#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket
sk = socket.socket()
sk.bind(('127.0.0.1',8000))
sk.listen()
while True:
    conn,_ = sk.accept()
    msg = conn.recv(1024)
    conn.send(b'HTTP/1.1 200 ok\r\n\r\n<div style="width:200px;height:200px;background-color:red;"></div>')
    conn.close()