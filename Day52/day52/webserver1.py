#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket
sk = socket.socket()
sk.bind(('127.0.0.1',8000))
sk.listen()
while True:
    conn,_ = sk.accept()
    msg = conn.recv(1024).decode('utf-8')
    url = msg.split()[1]
    if url == '/index/':
        conn.send(b'HTTP/1.1 200 ok\r\n\r\n<h1>index</h1>')
    elif url == '/home/':
        conn.send(b'HTTP/1.1 200 ok\r\n\r\n<h1>home</h1>')
    else:
        conn.send(b'HTTP/1.1 200 ok\r\n\r\n<h1>404 Not Found</h1>')
    conn.close()