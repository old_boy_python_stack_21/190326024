#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket
from datetime import datetime
def index(url):
    ret = b'<h1>{}</h1>'.decode('utf-8')
    return ret.format(url).encode('utf-8')

def home(url):
    ret = b'<h1>{}</h1>'.decode('utf-8')
    return ret.format(url).encode('utf-8')

def timer(url):
    with open('timer.html',mode='r',encoding='utf-8') as f:
        content = f.read()
    time = datetime.now().strftime('%H:%M:%S')
    content = content.replace('@@time@@',time)

    return content.encode('utf-8')

lst = [
    ('/index/',index),
    ('/home/',home),
    ('/time/',timer),
]

sk = socket.socket()
sk.bind(('127.0.0.1',8000))
sk.listen()
while True:
    conn,_ = sk.accept()
    msg = conn.recv(1024).decode('utf-8')
    url = msg.split()[1]
    conn.send(b'HTTP/1.1 200 OK\r\n\r\n')
    ret = None
    for i in lst:
        if url == i[0]:
            ret = i[1](url)
    if not ret:
        ret = b'<h1>404 Not Found</h1>'
    conn.send(ret)
    conn.close()