#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket

def index(url):
    ret = b'HTTP/1.1 200 ok\r\n\r\n<h1>{}</h1>'.decode('utf-8')
    return ret.format(url).encode('utf-8')

def home(url):
    ret = b'HTTP/1.1 200 ok\r\n\r\n<h1>{}</h1>'.decode('utf-8')
    return ret.format(url).encode('utf-8')

lst = [
    ('/index/',index),
    ('/home/',home),
]

sk = socket.socket()
sk.bind(('127.0.0.1',8000))
sk.listen()
while True:
    conn,_ = sk.accept()
    msg = conn.recv(1024).decode('utf-8')
    url = msg.split()[1]
    ret = None
    for i in lst:
        if url == i[0]:
            ret = i[1](url)
    if not ret:
        ret = b'HTTP/1.1 200 ok\r\n\r\n<h1>404 Not Found</h1>'
    conn.send(ret)
    conn.close()