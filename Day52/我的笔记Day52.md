# Day52

## 今日内容

1. web框架本质
2. django下载安装使用

## 内容详细

### 1.web框架本质

#### 1.1 本质

实现socket服务端。

#### 1.2 Http协议

超文本传输协议（英文：Hyper Text Transfer Protocol，HTTP）是一种用于分布式、协作式和超媒体信息系统的应用层协议。

- 工作在应用层
- 双工通信

#### 1.3 Http请求方法

- GET
- POST
- HEAD
- PUT
- DELETE
- TRACE
- OPTIONS
- CONNECT

#### 1.4 Http状态码

- 1xx：消息，请求已被服务器接收，继续处理。
- 2xx：成功，请求已成功被服务器接收、理解、返回。
- 3xx：重定向，需要后续操作才能完成这一请求。
- 4xx：请求错误，请求含有词法错误或无法被执行。
- 5xx：服务器错误，服务器在处理一个正确请求时发生错误。

#### 1.5 URL

超文本传输协议（HTTP）的统一资源定位符将从因特网获取信息的五个基本元素包括在一个简单的地址中：

- 传送协议
- 层级URL标记符号（为//，固定不变）
- 服务器（通常为域名，有时为IP地址）
- 端口号（以数字方式表示，若为Http的默认值":80"可以省略）
- 路径 （以"/"字符区别路径中的每一个目录名称）
- 查询（GET模式的窗体参数，以"?"字符为起点，每个参数以"&"隔开，再以"="分开参数名称和数据，通常以UTF-8的URL编码，避开字符冲突的问题）
- 片段 以"#"字符为起点

```
以http://www.luffycity.com:80/news/index.html?id=250&page=1 为例, 其中：

http，是协议；
www.luffycity.com，是服务器；
80，是服务器上的网络端口号；
/news/index.html，是路径；
?id=250&page=1，是查询。
```

#### 1.6 HTTP格式

1. 请求格式

   ![](E:\Python\11Django\http请求格式.jpg)

2. 响应格式

   ![](E:\Python\11Django\http响应格式.jpg)

#### 1.7 web框架的功能

1. socket收发消息 wsgireg   uwsgi

2. 根据不同的路径返回不同的页面
3. 返回动态页面（字符串的替换）jinja2

各web框架实现的功能：

- django：2,3
- flask：2
- tornado：1,2,3

### 2.django的下载安装使用

#### 2.1 下载安装

1. 命令行

   ```
   pip3 install django==1.11.21 -i https://pypi.tuna.tsinghua.edu.cn/simple
   ```

2. pycharm

   ![](E:\Python\11Django\django下载安装.png)

#### 2.2 创建项目

1. 命令行：

   找一个文件夹存放项目文件：

   打开终端：

   django-admin startproject 项目名称

   项目目录:

![](E:\Python\11Django\django项目目录.png)

2. pycharm

   ![](E:\Python\11Django\pycharm创建django项目.png)

#### 2.3 启动项目

1.命令行

切换到项目的根目录下  manage.py

`python36 manage.py runserver `  ——默认 <http://127.0.0.1:8000/>

`python36 manage.py runserver 80 `   在80端口启动

`python36 manage.py runserver 0.0.0.0:80`   0.0.0.0:80  指定IP和端口

通过修改配置文件settings.py

```
ALLOWED_HOSTS = ['*']  //允许所有ip连接
```

2.pycharm

点绿三角启动 可配置

![](E:\Python\11Django\项目配置.png)

![](E:\Python\11Django\python修改ip和端口号.png)

#### 2.4 简单使用

urls.py文件

```python
from django.conf.urls import url
from django.contrib import admin
from django.shortcuts import render,HttpResponse //引入模块

def index(request): 页面函数
    #return HttpResponse(b'hello world')
    return render(request,'index.html')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', index),
]
```

