#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第七天作业
王凯旋
'''

# 1.看代码写结果
'''
v1 = [1,2,3,4,5]
v2 = [v1,v1,v1]

v1.append(6)
print(v1)
print(v2)
'''

'''
结果：
[1,2,3,4,5,6]
[[1,2,3,4,5,6],[1,2,3,4,5,6],[1,2,3,4,5,6]]
'''

# 2.看代码写结果
'''
v1 = [1,2,3,4,5]
v2 = [v1,v1,v1]

v2[1][0] = 111
v2[2][0] = 222
print(v1)
print(v2)
'''

'''
结果：
[222,2,3,4,5]
[[222,2,3,4,5],[222,2,3,4,5],[222,2,3,4,5]]
'''

# 3.看代码写结果，并解释每一步的流程。
'''
v1 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
v2 = {}

for item in v1:    #遍历v1列表
    if item < 6:   #若v1元素小于6，继续循环
        continue
    if 'k1' in v2: #判断字典v2中是否存在键"k1"
        v2['k1'].append(item) #若存在，则向"k1"的值中追加元素
    else:
        v2['k1'] = [item]  #若不存在，则添加"k1"键值
print(v2) #遍历结束打印v2
'''

'''
结果：
{"k1":[6,7,8,9]}
'''


#4.简述深浅拷贝？
'''
浅拷贝：只拷贝最外层，内部元素的地址不变
深拷贝：会寻找所有的可变元素，然后拷贝
'''


#5.看代码写结果
'''
import copy

v1 = "alex"
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1 is v2)
print(v1 is v3)
'''

'''
结果：
True
True
'''


#6.看代码写结果
'''
import copy

v1 = [1,2,3,4,5]
v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1 is v2)
print(v1 is v3)
'''

'''
结果：
False
False
'''


#7.看代码写结果
'''
import copy

v1 = [1,2,3,4,5]

v2 = copy.copy(v1)
v3 = copy.deepcopy(v1)

print(v1[0] is v2[0])
print(v1[0] is v3[0])
print(v2[0] is v3[0])
'''

'''
结果：
True
True
True
'''


#9.看代码写结果
'''
import copy

v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]

v2 = copy.copy(v1)

print(v1 is v2)

print(v1[0] is v2[0])
print(v1[3] is v2[3])

print(v1[3]['name'] is v2[3]['name'])
print(v1[3]['numbers'] is v2[3]['numbers'])
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])
'''

'''
结果：
False
True
True
True
True
True
'''


#10.看代码写结果
'''
import copy

v1 = [1,2,3,{"name":'武沛齐',"numbers":[7,77,88]},4,5]

v2 = copy.deepcopy(v1)

print(v1 is v2)

print(v1[0] is v2[0])
print(v1[3] is v2[3])

print(v1[3]['name'] is v2[3]['name'])
print(v1[3]['numbers'] is v2[3]['numbers'])
print(v1[3]['numbers'][1] is v2[3]['numbers'][1])
'''

'''
结果：
False
True
False
True
False
True
'''


#11.简述文件操作的打开模式
'''
r模式：只读模式，文件不存在会报错，写也会报错
w模式：只写模式，在打开文件时会将文件清空，若文件不存在，则会创建。读会报错
a模式：追加模式，打开文件时光标在文件末尾，写入内容会追加到结尾，文件不存在会创建。读会报错
r+模式：可读可写，打开文件时光标位于文件开头。写入时会覆盖光标后面的文字
w+模式：可读可写，打开文件时会清空文件。
a+模式：可读可写，打开文件时光标位于文件末尾，可通过移动光标读取。写入时光标自动移动到末尾。
'''


#12.请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = ['骗子，','不是','说','只有',"10",'个题吗？']
lst = []
for i in info:
    lst.append(i)
content = "_".join(lst)
f = open("readme.txt",mode="w",encoding="utf-8")
f.write(content)
f.flush()
f.close()
'''


#13.请将info中的值使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = ['骗子，','不是','说','只有',10,'个题吗？']
lst = []
for i in info:
    if type(i) != str:
        i = str(i)
    lst.append(i)
content = "_".join(lst)
f = open("readme.txt",mode="w",encoding="utf-8")
f.write(content)
f.flush()
f.close()
'''


#14.请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
''''''
#info = {'name':'骗子','age':18,'gender':'性别'}

# 1. 请将info中的所有键 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = {'name':'骗子','age':18,'gender':'性别'}
lst = []
for i in info:
    if type(i) != str:
        i = str(i)
    lst.append(i)
content = "_".join(lst)
f = open("readme.txt",mode="w",encoding="utf-8")
f.write(content)
f.flush()
f.close()
'''

# 2. 请将info中的所有值 使用 "_" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = {'name':'骗子','age':18,'gender':'性别'}
lst = []
for i in info.values():
    if type(i) != str:
        i = str(i)
    lst.append(i)
content = "_".join(lst)
f = open("readme.txt",mode="w",encoding="utf-8")
f.write(content)
f.flush()
f.close()
'''

# 3. 请将info中的所有键和值按照 "键|值,键|值,键|值" 拼接起来并写入到文件 "readme.txt" 文件中。
'''
info = {'name':'骗子','age':18,'gender':'性别'}
lst = []
for k,v in info.items():
    if type(k) != str:
        k = str(k)
    if type(v) != str:
        v = str(v)
    lst.append(k + "|" + v)
content = ",".join(lst)
f = open("readme.txt",mode="w",encoding="utf-8")
f.write(content)
f.flush()
f.close()
'''



#15.写代码
'''
要求：
    如文件 data.txt 中有内容如下：

    wupeiqi|oldboy|wupeiqi@xx.com
    alex|oldboy|66s@xx.com
    xxxx|oldboy|yuy@xx.com

    请用代码实现读入文件的内容，并构造成如下格式：
    info = [
        {'name':'wupeiqi','pwd':'oldboy','email':'wupeiqi@xx.com'},
        {'name':'alex','pwd':'oldboy','email':'66s@xx.com'},
        {'name':'xxxx','pwd':'oldboy','email':'yuy@xx.com'},
    ]
'''

'''
lst = []
info = []
f = open("question15.txt",mode="r",encoding="utf-8")
for i in f:
    lst.append(i.strip())
f.close()
for item in lst:
    value = item.split("|")
    dic = {"name":value[0],"pwd":value[1],"email":value[2]}
    info.append(dic)
print(info)
'''