#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第一天作业
'''
# 1. 操作系统的作用?
'''
操作系统是用来操控计算机硬件完成某项任务的。
'''

# 2.列举你听过的操作系统及区别？
'''
- windows:图形界面很好。可以玩游戏。
     - window xp
     - window 7
     - window 10
     - window server 2012
   - linux:开源免费
     - centos：图形界面一般，多用于服务器。
     - ubuntu：图形界面比较友好。
     - redhat：用于企业。
   - mac：办公/装逼
'''

# 3. 列举你了解的编码及他们之间的区别？
'''
   - ascii编码：用单个字节（8位）表示一个字符，仅可以表示英文字母以及一些特殊符号。
   - Unicode编码（万国码）：用四个字节（32位）表示一个字符。可以表示所有字符。
   - utf-8编码：Unicode编码的压缩版。最少用一个字节表示，去除掉Unicode编码中未用到的字节。中文用三个字节表示。
'''

# 4. 列举你了解的Python2和Python3的区别？
'''
解释器默认编码：
   py2:ascii
   py3:utf-8
输出：
   py2:print ""
   py3:print()
输入：
   py2:print ""
   py3:print()
整数相除：
   py2:只保留整数位
   py3:正确
整型：
   py2:超过长度转换为long
   py3:无限制
'''

# 5.你了解的python都有那些数据类型？
'''
int
bool
str
list
tuple
dict
set
None
'''

# 6. 补充代码，实现以下功能。
'''
value = 'alex"烧饼'
print(value)  # 要求输出  alex"烧饼
'''

# 7.用print打印出下面内容：
'''
   ⽂能提笔安天下,
   武能上⻢定乾坤.
   ⼼存谋略何⼈胜,
   古今英雄唯是君。
'''
'''
print("""
   ⽂能提笔安天下,
   武能上⻢定乾坤.
   ⼼存谋略何⼈胜,
   古今英雄唯是君。
   """)
'''

# 8.变量名的命名规范和建议？
'''
变量名只能由数字，字母以及下划线组成。
变量名不能以数字开头。
变量名不能是python关键字。
建议：见名知意。多个单词用下划线连接。
'''

# 9. 如下那个变量名是正确的？
'''
   name = '武沛齐'   #正确
   _ = 'alex'       #正确
   _9 = "老男孩"     #正确
   9name = "景女神"  #错误
   oldboy(edu = 666 #错误
'''

# 10. 简述你了解if条件语句的基本结构。
'''
if 条件1:
    缩进
elif 条件2:
    缩进
else:
    缩进
'''

# 11. 设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确。
'''
value = input("请输入一个数字：")
value = int(value)
if value > 66:
    print("大了")
elif value < 66:
    print("小了")
else:
    print("结果正确")
'''

# 12. 提⽰⽤户输入⿇花藤. 判断⽤户输入的对不对。如果对, 提⽰真聪明, 如果不对, 提⽰你 是傻逼么。
'''
value = input("请输入“麻花藤”：")
if value == "麻花藤":
    print("真聪明")
else:
    print("你是傻逼么")
'''

'''
第二天作业
'''
# 1. 猜数字，设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;只有等于66，显示猜测结果正确，然后退出循环。
'''
while True:
    value = input("请输入一个数字：")
    value = int(value)
    if value > 66:
        print("大了")
    elif value < 66:
        print("小了")
    else:
        print("结果正确")
        break
'''

# 在上一题的基础，设置：给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘大笨蛋’。
'''
for i in range(0,3):
    value = input("请输入一个数字：")
    value = int(value)
    if value > 66:
        print("大了")
    elif value < 66:
        print("小了")
    else:
        print("结果正确")
        break
else:
    print("大笨蛋")
'''

# 3. 使用两种方法实现输出 1 2 3 4 5 6   8 9 10 。
'''
for i in range(1,11):
    if i == 7:
        continue
    print(i)
'''
'''
for i in range(1,11):
    if i != 7:
        print(i)
'''

# 4. 求1-100的所有数的和
'''
total = 0
for i in range(1,101):
    total += i
print(total)
'''

# 5. 输出 1-100 内的所有奇数
'''
for i in range(1,101):
    if i % 2 == 1:
        print(i)
'''

# 6. 输出 1-100 内的所有偶数
'''
for i in range(1,101):
    if i % 2 == 0:
        print(i)
'''

# 7. 求1-2+3-4+5 ... 99的所有数的和
'''
total = 0
for i in range(1,100):
    if i % 2 == 1:
        total += i
    else:
        total -= i
print(total)
'''

# 8. ⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）
'''
for i in range(0,3):
    name = input("请输入用户名：")
    pwd = input("请输入密码：")
    if name == "1" and pwd == "1":
        break
    if i < 2:
        print("您还有{}次机会".format(2-i))
'''

# 9. 简述ASCII、Unicode、utf-8编码
'''
    ASCII：一个字节表示一个字符，仅能表示英文字母，数字以及一些特殊符号
    Unicode：一般用于内存中的数据运算
      ecs2：两个字节表示一个字符
      ecs4：四个字节表示一个字符
    utf-8：Unicode编码的压缩版本，最少用一个字节表示一个字符，去除掉Unicode编码中无用的字节，一个中文字符用三个字节表示。
'''

# 10. 简述位和字节的关系？
'''
1字节等于8位        1byte = 8bit
'''

# 11. 猜年龄游戏
#    要求：允许用户最多尝试3次，3次都没猜对的话，就直接退出，如果猜对了，打印恭喜信息并退出
'''
for i in range(0,3):
    value = input("请输入：")
    value = int(value)
    if value == 18:
        print("恭喜")
        break
    if i < 2:
        print("你还有{}次机会".format(2-i))
'''

# 12. 猜年龄游戏升级版
# 要求：允许用户最多尝试3次，每尝试3次后，如果还没猜对，就问用户是否还想继续玩，
# 如果回答Y，就继续让其猜3次，以此往复，如果回答N，就退出程序，如何猜对了，就直接退出。
'''
count = 3
while True:
    value = input("请输入：")
    value = int(value)
    if value == 18:
        print("恭喜")
        break
    count -= 1
    if count > 0:
        print("你还剩{}次机会".format(count))
    else:
        result = input("是否继续玩？是（Y）/否（N）:")
        if result == "N":
            break
        else:
            count = 3
'''

# 13. 判断下列逻辑语句的True,False
'''
    1 > 1 or 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
      False or True or False and True and True or False
      False or True or False or False
      True
    not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6 
      False and True or False and True and True or False
      False or False or False
      False
'''

# 14. 求出下列逻辑语句的值。
'''
     8 or 3 and 4 or 2 and 0 or 9 and 7 
       8 or 4 or 0 or 7
       8
     0 or 2 and 3 and 4 or 6 and 0 or 3
       0 or 4 or 0 or 3
       4
'''

# 15. 下列结果是什么？
'''
     6 or 2 > 1
       6
     3 or 2 > 1
       3
     0 or 5 < 4
       False
     5 < 4 or 3
       3
     2 > 1 or 6
       True
     3 and 2 > 1
       True
     0 and 3 > 1
       0
     2 > 1 and 3
       3
     3 > 1 and 0
       0
     3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2
       True and 2 or Fasle and 3 and 4 or True
       2 or False or True
       2
'''

"""
第三天作业
"""
# 1.有变量name = "aleX leNb " 完成如下操作：

# 移除 name 变量对应的值两边的空格,并输出处理结果
'''
name = "aleX leNb "
name = name.strip()
print(name)
'''

# 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
'''
name = "aleX leNb "
print(name.startswith("al"))
'''

# 判断name变量是否以"Nb"结尾,并输出结果（用切片）
'''
name = "aleX leNb "
print(name.endswith("Nb"))
'''

# 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
'''
name = "aleX leNb "
result = name.replace("l", "p")
print(result)
'''

# 将name变量对应的值中的第一个"l"替换成"p",并输出结果
'''
name = "aleX leNb "
result = name.replace("l", "p", 1)
print(result)
'''

# 将 name 变量对应的值根据 所有的"l" 分割,并输出结果
'''
name = "aleX leNb "
result = name.split("l")
print(result)
'''

# 将name变量对应的值根据第一个"l"分割,并输出结果
'''
name = "aleX leNb "
result = name.split("l", 1)
print(result)
'''

# 将 name 变量对应的值变大写,并输出结果
'''
name = "aleX leNb "
result = name.upper()
print(result)
'''

# 将 name 变量对应的值变小写,并输出结果...
'''
name = "aleX leNb "
result = name.lower()
print(result)
'''

# 请输出 name 变量对应的值的第 2 个字符?
'''
name = "aleX leNb "
result = name[1]
print(result)
'''

# 请输出 name 变量对应的值的前 3 个字符?
'''
name = "aleX leNb "
result = name[0:3]
print(result)
'''

# 请输出 name 变量对应的值的后 2 个字符?
'''
name = "aleX leNb "
result = name[-2:]
print(result)
'''

# 2.有字符串s = "123a4b5c"
# 通过对s切片形成新的字符串 "123"
'''
s = "123a4b5c"
result = s[0:3]
print(result)
'''

# 通过对s切片形成新的字符串 "a4b"
'''
s = "123a4b5c"
result = s[3:6]
print(result)
'''

# 通过对s切片形成字符串s5,s5 = "c"
'''
s = "123a4b5c"
s5 = s[-1:]
print(s5)
'''

# 通过对s切片形成字符串s6,s6 = "ba2"
'''
s = "123a4b5c"
s6 = s[5:6]
s6 += s[3:4]
s6 += s[1:2]
print(s6)
'''

# 3.使用while循环字符串 s="asdfer" 中每个元素。
'''
s = "asdfer"
length = len(s)
index = 0
while index < length:
    print(s[index])
    index += 1
'''
'''
s = "asdfer"
for i in s:
    print(i)
'''

# 4.使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"
'''
s = "321"
length = len(s)
index = 0
while True:
    if index < length:
        count = s[index]
        print("倒计时%s秒" % (count,))
    else:
        print("出发！")
        break
    index += 1
'''
'''
s = "321"
for i in s:
    print("倒计时{}秒".format(i))
else:
    print("出发！")
'''

# 5.实现一个整数加法计算器(两个数相加)：
# 如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。
'''
message = input("请输入内容:")
value = message.split("+")
first_value = int(value[0].strip())
second_value = int(value[1].strip())
result = first_value + second_value
print(result)
'''

# 6.计算用户输入的内容中有几个 h 字符？
# 如：content = input("请输入内容：")   # 如fhdal234slfh98769fjdla
'''
content = input("请输入内容：")
length = len(content)
index = 0
total = 0
while index < length:
    if content[index] == "h":
        total += 1
    index += 1
print(total)
'''
'''
content = input("请输入内容：")
total = 0
for i in content:
    if i == "h":
        total += 1
print(total)
'''

# 7.计算用户输入的内容中有几个 h 或 H  字符？
# 如：content = input("请输入内容：")   # 如fhdal234slfH9H769fjdla
'''
content = input("请输入内容：")
content = content.lower()
length = len(content)
index = 0
total = 0
while index < length:
    if content[index] == "h":
        total += 1
    index += 1
print(total)
'''
'''
content = input("请输入内容：")
content = content.lower()
total = 0
for i in content:
    if i == "h":
        total += 1
print(total)
'''

# 8.使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。"。
'''
# 正向
message = "伤情最是晚凉天，憔悴厮人不堪言。"
length = len(message)
index = 0
while index < length:
    print(message[index])
    index += 1
'''

'''
# 反向
message = "伤情最是晚凉天，憔悴厮人不堪言。"
length = len(message)
index = -1
while index >= 0 - length:
    print(message[index])
    index -= 1
'''

# 9.获取用户输入的内容中 前4个字符中 有几个 A ？
# 如：content = input("请输入内容：")   # 如fAdal234slfH9H769fjdla
'''
content = input("请输入内容：")
length = len(content)
total = 0
index = 0
if length >= 4:
    while index < 4:
        if content[index] == "A":
            total += 1
        index += 1
else:
    while index < length:
        if content[index] == "A":
            total += 1
        index += 1
print(total)
'''
'''
content = input("请输入内容：")
total = 0
for i in range(0,4):
    if content[i] == "A":
        total += 1
print(total)
'''

# 10.如果判断name变量对应的值前四位"l"出现几次,并输出结果。
'''
name = input("请输入姓名：")
length = len(name)
total = 0
index = 0
if length >= 4:
    while index < 4:
        if name[index] == "l":
            total += 1
        index += 1
else:
    while index < length:
        if name[index] == "l":
            total += 1
        index += 1
print(total)
'''
'''
content = input("请输入内容：")
total = 0
for i in range(0,4):
    if content[i] == "l":
        total += 1
print(total)
'''

# 11.获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
'''
要求：
	将num1中的的所有数字拼接起来：1232312
	将num1中的的所有数字拼接起来：1218323
	然后将两个数字进行相加。
'''

'''
num1 = input("请输入：")
num2 = input("请输入：")
new_num1 = ""
new_num2 = ""
for i in num1:
    if i.isdigit():
        new_num1 += i
for i1 in num2:
    if i1.isdigit():
        new_num2 += i1
print(int(new_num1) + int(new_num2))
'''


'''
第四天作业
'''

#1.简述解释性语言和编译型语言的区别？
'''
解释型语言：代码编写完成后，交给解释器，解释器解释完一句执行一句
编译型语言：代码编写完成后，交给编译器编译生成新文件后，再交给计算机执行
'''


#2.列举你了解的Python的数据类型？
'''
1.整型
2.布尔类型
3.字符串
4.列表
5.元祖
6.字典
7.集合
8.None
'''


#3.写代码，有如下列表，按照要求实现每一个功能 li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]

#    计算列表的长度并输出
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
print(len(li))
'''

#    请通过步长获取索引为偶数的所有值，并打印出获取后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
print(li[0::2])
'''

#   列表中追加元素"seven",并输出添加后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.append("senven")
print(li)
'''

#    请在列表的第1个位置插入元素"Tony",并输出添加后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.insert(0,"Tony")
print(li)
'''

#   请修改列表第2个位置的元素为"Kelly",并输出修改后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li[1] = "Kelly"
print(li)
'''

#    请将列表的第3个位置的值改成 "太白"，并输出修改后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li[2] = "太白"
print(li)
'''

#    请将列表 l2=[1,"a",3,4,"heart"] 的每一个元素追加到列表li中，并输出添加后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
l2=[1, "a", 3, 4, "heart"]
for item in l2:
    li.append(item)
print(li)
'''

'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
l2 = [1, "a", 3, 4, "heart"]
li.extend(l2)
print(li)
'''

#    请将字符串 s = "qwert"的每一个元素添加到列表li中，一行代码实现，不允许循环添加。
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
s = "qwert"
li.extend(s)
print(li)
'''

#    请删除列表中的元素"ritian",并输出添加后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.remove("ritian")
print(li)
'''

#    请删除列表中的第2个元素，并输出删除元素后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
li.pop(1)
print(li)
'''

#    请删除列表中的第2至第4个元素，并输出删除元素后的列表
'''
li = ["alex", "WuSir", "ritian", "barry", "wenzhou"]
del li[1:4]
print(li)
'''


#4.请用三种方法实现字符串反转 name = "小黑半夜三点在被窝玩愤怒的小鸟"（步长、while、for）
#方法一
'''
name = "小黑半夜三点在被窝玩愤怒的小鸟"
print(name[::-1])
'''
#方法二
'''
name = "小黑半夜三点在被窝玩愤怒的小鸟"
length = len(name)
result = ""
while length > 0:
    result += name[length-1]
    length -= 1
print(result)
'''
#方法三
'''
name = "小黑半夜三点在被窝玩愤怒的小鸟"
result = ""
length = len(name)
for i in range(1, length+1):
    result += name[length - i]
print(result)
'''


#5.写代码，有如下列表，利用切片实现每一个功能 li = [1, 3, 2, "a", 4, "b", 5,"c"]

#通过对li列表的切片形成新的列表[1, 3, 2]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[0:3])
'''

#通过对li列表的切片形成新的列表["a", 4, "b"]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[3:6])
'''

#通过对li列表的切片形成新的列表[1, 2, 4, 5]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[0::2])
'''

#通过对li列表的切片形成新的列表[3, "a", "b"]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[1:7:2])
'''

#通过对li列表的切片形成新的列表[3, "a", "b", "c"]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[1::2])
'''

#通过对li列表的切片形成新的列表["c"]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[-1::])
'''

#通过对li列表的切片形成新的列表["b", "a", 3]
'''
li = [1, 3, 2, "a", 4, "b", 5,"c"]
print(li[-3::-2])
'''


# 6.请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
#0 武沛齐
#1 景女神
#2 肖大侠
'''
users = ["武沛齐","景女神","肖大侠"]
for index in range(0,len(users)):
    print(index,users[index])
'''


#7.请用代码实现循环输出元素和值：users = ["武沛齐","景女神","肖大侠"] ，如：
#1 武沛齐
#2 景女神
#3 肖大侠

'''
users = ["武沛齐","景女神","肖大侠"]
for index in range(0,len(users)):
    print(index + 1,users[index])
'''


#8.写代码，有如下列表，按照要求实现每一个功能。
# lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]

#将列表lis中的"k"变成大写，并打印列表。
'''
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[2] = "K"
lis[3][2][0] = lis[3][2][0].upper()
print(lis)
'''

#将列表中的数字3变成字符串"100"
'''
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[1] = "100"
lis[3][2][1][1] = "100"
print(lis)
'''

#将列表中的字符串"tt"变成数字101
'''
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[3][2][1][0] = 101
print(lis)
'''

#在"qwe"前面插入字符串："火车头"
'''
lis = [2, 3, "k", ["qwe", 20, ["k1", ["tt", 3, "1"]], 89], "ab", "adv"]
lis[3].insert(0,"火车头")
print(lis)
'''


#9.写代码实现以下功能
#如有变量 googs = ['汽车','飞机','火箭'] 提示用户可供选择的商品：
#0,汽车
#1,飞机
#2,火箭
'''
googs = ['汽车','飞机','火箭']
print("可供选择的商品：")
for item in googs:
    print(googs.index(item),",",item)
num = input("请选择商品：")
print("你选择的商品是{0}".format(googs[int(num)]))
'''


#10.请用代码实现 li = "alex"
#利用下划线将列表的每一个元素拼接成字符串"a_l_e_x"
'''
li = "alex"
print("_".join(li))
'''


#11.利用for循环和range找出 0 ~ 100 以内所有的偶数，并追加到一个列表。
'''
li = []
for item in range(0, 101):
    if item % 2 == 0:
        li.append(item)
print(li)
'''


#12.利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并追加到一个列表。
'''
li = []
for item in range(0, 51):
    if item % 3 == 0:
        li.append(item)
print(li)
'''


#13.利用for循环和range 找出 0 ~ 50 以内能被3整除的数，并插入到列表的第0个索引位置，最终结果如下：
#[48,45,42...]
'''
li = []
for item in range(0, 51):
    if item % 3 == 0:
        li.append(item)
li = li[::-1]
print(li)
'''


#14.查找列表li中的元素，移除每个元素的空格，并找出以"a"开头，并添加到一个新列表中,最后循环打印这个新列表。
#li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
'''
li = ["TaiBai ", "alexC", "AbC ", "egon", " riTiAn", "WuSir", "  aqc"]
new_li = []
for item in li:
    if item.strip().startswith("a"):
        new_li.append(item.strip())
for i in new_li:
    print(i)
'''


#15.判断是否可以实现，如果可以请写代码实现。
#li = ["alex",[11,22,(88,99,100,),33], "WuSir", ("ritian", "barry",), "wenzhou"]

#    请将 "WuSir" 修改成 "武沛齐"
'''
li = ["alex",[11,22,(88,99,100,),33], "WuSir", ("ritian", "barry",), "wenzhou"]
li[2] = "武沛齐"
print(li)
'''

#    请将 ("ritian", "barry",) 修改为 ['日天','日地']
'''
message = ("ritian", "barry",)
message = ['日天','日地']
print(message)
'''

#    请将 88 修改为 87
'''
无法实现
'''

#    请将 "wenzhou" 删除，然后再在列表第0个索引位置插入 "文周"
'''
li = ["alex",[11,22,(88,99,100,),33], "WuSir", ("ritian", "barry",), "wenzhou"]
li.remove("wenzhou")
li.insert(0,"文周")
print(li)
'''



'''
第五天作业
'''

# 1.请将列表中的每个元素通过 "_" 链接起来。
'''
users = ['李少奇', '李启航', '渣渣辉']
result = "_".join(users)
print(result)
'''

# 2.请将列表中的每个元素通过 "_" 链接起来。
'''
users = ['李少奇', '李启航', 666, '渣渣辉']
users[2] = str(users[2])
print("_".join(users))
'''

# 3.请将元组 v1 = (11,22,33) 中的所有元素追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33)
v2 = [44,55,66]
v2.extend(v1)
print(v2)
'''

# 4.请将元组 v1 = (11,22,33,44,55,66,77,88,99) 中的所有偶数索引位置的元素 追加到列表 v2 = [44,55,66] 中。
'''
v1 = (11,22,33,44,55,66,77,88,99)
v2 = [44,55,66]
v3 = v1[::2]
v2.extend(v3)
print(v2)
'''

# 5.将字典的键和值分别追加到 key_list 和 value_list 两个列表中，如：
# key_list = []
# value_list = []
# info = {'k1':'v1','k2':'v2','k3':'v3'}
'''
key_list = []
value_list = []
info = {'k1':'v1','k2':'v2','k3':'v3'}
for k,v in info.items():
    key_list.append(k)
    value_list.append(v)
print(key_list)
print(value_list)
'''

# 6.字典dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}

# a. 请循环输出所有的key
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
for i in dic.keys():
    print(i)
'''

# b. 请循环输出所有的value
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
for i in dic.values():
    print(i)
'''

# c. 请循环输出所有的key和value
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
for k,v in dic.items():
    print(k,v)
'''

# d. 请在字典中添加一个键值对，"k4": "v4"，输出添加后的字典
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
dic["k4"] = "v4"
print(dic)
'''

# e. 请在修改字典中 "k1" 对应的值为 "alex"，输出修改后的字典
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
dic["k1"] = "alex"
print(dic)
'''

# f. 请在k3对应的值中追加一个元素 44，输出修改后的字典
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
dic["k3"].append(44)
print(dic)
'''

# g. 请在k3对应的值的第 1 个位置插入个元素 18，输出修改后的字典
'''
dic = {'k1': "v1", "k2": "v2", "k3": [11,22,33]}
dic["k3"].insert(0,18)
print(dic)
'''

# 7.请循环打印k2对应的值中的每个元素。
# info = {
#     'k1':'v1',
#     'k2':[('alex'),('wupeiqi'),('oldboy')],
# }
'''
info = {
    'k1':'v1',
    'k2':[('alex'),('wupeiqi'),('oldboy')],
}
for i in info["k2"]:
    print(i)
'''

# 8.有字符串"k: 1|k1:2|k2:3 |k3 :4" 处理成字典 {'k':1,'k1':2....}
'''
message = "k: 1|k1:2|k2:3 |k3 :4"
dic = {}
for i in message.split("|"):
    value = i.split(":")
    dic[value[0].strip()] = int(value[1].strip())
print(dic)
'''

# 9.写代码
# 有如下值 li= [11,22,33,44,55,66,77,88,99,90] ,将所有大于 66 的值保存至字典的第一个key对应的列表中，
# 将小于 66 的值保存至第二个key对应的列表中。
# result = {'k1':[],'k2':[]}
'''
result = {'k1':[],'k2':[]}
li= [11,22,33,44,55,66,77,88,99,90]
for i in li:
    if i > 66:
        result["k1"].append(i)
    elif i < 66:
        result["k2"].append(i)
print(result)
'''

# 10.输出商品列表，用户输入序号，显示用户选中的商品
"""
商品列表：
  goods = [
		{"name": "电脑", "price": 1999},
		{"name": "鼠标", "price": 10},
		{"name": "游艇", "price": 20},
		{"name": "美女", "price": 998}
	]
要求:
1：页面显示 序号 + 商品名称 + 商品价格，如：
      1 电脑 1999 
      2 鼠标 10
	  ...
2：用户输入选择的商品序号，然后打印商品名称及商品价格
3：如果用户输入的商品序号有误，则提示输入有误，并重新输入。
4：用户输入Q或者q，退出程序。
"""
'''
goods = [
    {"name": "电脑", "price": 1999},
    {"name": "鼠标", "price": 10},
    {"name": "游艇", "price": 20},
    {"name": "美女", "price": 998}
]
for i in range(0, len(goods)):
    print(i + 1, goods[i]["name"], goods[i]["price"])
while True:
    index = input("请输入要选择的商品序号：")
    if index.isdigit():
        index = int(index) - 1
    elif index.upper() == "Q":
        break
    else:
        print("输入有误，请重新输入")
        continue
    if index in range(0, len(goods)):
        print("您选择的商品是：{0}，价格是：{1}".format(goods[i]["name"], goods[i]["price"]))
        break
    else:
        print("输入有误，请重新输入")
        continue
'''


'''
第六天作业
'''

# 1.列举你了解的字典中的功能（字典独有）。
'''
keys()  
values()
items()
get()
update()
pop()
'''

# 2.列举你了解的集合中的功能（集合独有）。
'''
add()
discard()
update()
intersection()
union()
difference()
'''

# 3.列举你了解的可以转换为 布尔值且为False的值。
'''
0,"",[],(),{},set(),None
'''

# 4.请用代码实现 info = {'name':'王刚蛋','hobby':'铁锤'}
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
'''
info = {'name':'王刚蛋','hobby':'铁锤'}
while True:
    content = input("请输入键：")
    print(info.get(content))
'''

# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
# 注意：无需考虑循环终止（写死循环即可）
'''
info = {'name': '王刚蛋', 'hobby': '铁锤'}
while True:
    content = input("请输入键：")
    print(info.get(content, "键不存在"))
'''

# 5.请用代码验证 "name" 是否在字典的键中？
'''
info = {'name':'王刚蛋','hobby':'铁锤','age':'18'}
if info.get("name") == None:
    print("不存在")
else:
    print("存在")
'''

# 6.请用代码验证 "alex" 是否在字典的值中？
'''
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18'}
if "alex" in list(info.values()):
    print("存在")
else:
    print("不存在")
'''

# 7.有如下：
# v1 = {'武沛齐','李杰','太白','景女神'}
# v2 = {'李杰','景女神'}

# 请得到 v1 和 v2 的交集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v1.intersection(v2))
'''

# 请得到 v1 和 v2 的并集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v1.union(v2))
'''

# 请得到 v1 和 v2 的 差集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v1.difference(v2))
'''

# 请得到 v2 和 v1 的 差集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v2.difference(v1))
'''

# 8.循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
'''
lst = []
while True:
    content = input("请输入：")
    if content.lower() == "n":
        break
    lst.append(content)
print(lst)
'''

# 9.循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
'''
value = set()
while True:
    content = input("请输入：")
    if content.lower() == "n":
        break
    value.add(content)
print(value)
'''

# 10.写代码实现
# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，
# 如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
# v1 = {'alex','武sir','肖大'}
# v2 = []
'''
v1 = {'alex', '武sir', '肖大'}
v2 = []
while True:
    content = input("请输入：")
    if content.lower() == "n":
        break
    if content in v1:
        v2.append(content)
    else:
        v1.add(content)
print(v1, v2)
'''

# 11.判断以下值那个能做字典的key ？那个能做集合的元素？
'''
    1     可以
    -1    可以
    ""    可以
    None  可以
    [1,2]   不可以
    (1,)    可以
    {11,22,33,4}   不可以
    {'name':'wupeiq','age':18} 不可以
'''

# 12.is 和 == 的区别？
'''
#is 用来判断两个变量的内存地址是否相等
#== 用来判断两个变量的值是否相等
'''

# 13.type使用方式及作用？
'''
#用法
v1 = 123
print(type(v1))

#作用：获取变量的数据类型
'''

# 14.id的使用方式及作用？
'''
#用法
v1 = 123
print(id(v1))

#作用：获取变量的内存地址
'''

# 15.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = {'k1':'v1','k2':[1,2,3]}

result1 = v1 == v2
result2 = v1 is v2
print(result1)
print(result2)
'''

'''
#结果
True
False
#原因
== 比较的是两个变量的值是否相等
is 比较的是两个变量的内存地址是否相等
v1和v2分别赋值，开辟了两块不同的内存来存放两个变量的值，他们的值相等，但内存地址不同
'''

# 16.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

result1 = v1 == v2
result2 = v1 is v2
print(result1)
print(result2)
'''

'''
#结果
True
True
#原因
v2 = v1 是将v1变量的内存地址赋给了v2变量。这时两个变量引用的同一块内存。所以他们的值和内存地址都是相等的
'''

# 17.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

v1['k1'] = 'wupeiqi'
print(v2)
'''

'''
#结果
v1 = {'k1':'wupeiqi','k2':[1,2,3]}
#原因
v2 = v1 是将v1变量的内存地址赋给了v2变量。这时两个变量引用的同一块内存。所以当修改了v1的值后，v2的值也改了。
'''

# 18.看代码写结果并解释原因
'''
v1 = '人生苦短，我用Python'
v2 = [1,2,3,4,v1]

v1 = "人生苦短，用毛线Python"

print(v2)
'''

'''
#结果
v2 = [1,2,3,4,'人生苦短，我用Python']
#原因
v2变量的第5个元素引用v1变量的内存地址，当v1变量重新赋值后，在内存中重新开辟了一块空间存放"人生苦短，用毛线Python"，
而这时v2的第五个元素引用的内存地址和内存中的值都没有改变。所以v2没变。
'''

# 19.看代码写结果并解释原因
'''
info = [1,2,3]
userinfo = {'account':info, 'num':info, 'money':info}

info.append(9)
print(userinfo)

info = "题怎么这么多"
print(userinfo)
'''

'''
#结果
userinfo = {'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
userinfo = {'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
#原因
第一次修改info变量内存地址中的值，而字典中的值的内存地址和info变量的内存地址相同，所以第一次打印时字典的值也变化了。
第二次给info变量重新赋值，这时重新开辟一块内存存放info的值，而原来内存中的值没有变化，所以第二次打印结果没变化
'''


# 20.看代码写结果并解释原因
'''
info = [1,2,3]
userinfo = [info,info,info,info,info]

info[0] = '不仅多，还特么难呢'
print(info,userinfo)
'''

'''
#结果
['不仅多，还特么难呢',2,3] [['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3]]
#原因
只是修改了info变量内存地址中元素的值，所以userinfo变量的元素也变化了
'''


# 21.看代码写结果并解释原因
'''
info = [1,2,3]
userinfo = [info,info,info,info,info]

userinfo[2][0] = '闭嘴'
print(info,userinfo)
'''

'''
#结果
['闭嘴',2,3] [['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3]]
#原因
只是修改了userinfo变量的第三个元素内存地址中内部的值，所以所有引用相同内存地址的变量都变了
'''


# 22.看代码写结果并解释原因
'''
info = [1, 2, 3]
user_list = []
for item in range(10):
    user_list.append(info)

info[1] = "是谁说Python好学的？"

print(user_list)
'''

'''
#结果
[[1, '是谁说Python好学的？', 3],...] （共有10个[1, '是谁说Python好学的？', 3]）
#原因
user_list变量在每次循环添加元素时，引用的都是info变量的内存地址，所以当info变量内存地址中的只改变后，user_list
的所有元素都变了
'''


# 23.看代码写结果并解释原因
'''
data = {}
for item in range(10):
    data['user'] = item
print(data)
'''

'''
#结果
{'user': 9}
#原因
在第一次向data字典中添加元素时，计算机开辟一块内存存放键对应的值的内存地址，之后每次循环键都是指向同一块内存，
内存中保存的值得内存地址没有变化，所以每次循环只是更新值。所以最终结果为最后一次循环的值
'''


# 24.看代码写结果并解释原因
'''
data_list = []
data = {}
for item in range(10):
    data['user'] = item
    data_list.append(data)
print(data_list)
'''

'''
#结果
[{'user':9},...] (10个{'user':9})
#原因
在第一次向data字典中添加元素时，计算机开辟一块内存存放键对应的值的内存地址，之后每次循环键都是指向同一块内存，
内存中保存的值得内存地址没有变化，所以每次循环只是更新值。所以data字典中的值是最后一次循环的值。而data_list
变量的所有元素都是引用的data字典的内存地址。
'''


# 25.看代码写结果并解释原因
'''
data_list = []
for item in range(10):
    data = {}
    data['user'] = item
    data_list.append(data)
print(data_list)
'''

'''
#结果
[{'user': 0}, {'user': 1}, {'user': 2}, {'user': 3}, {'user': 4}, {'user': 5}, {'user': 6}, {'user': 7}, {'user': 8}, {'user': 9}]
#原因
每次循环时都会给data变量重新开辟内存，存放值。所以最终列表中的所有元素指向的内存地址都不相同。
'''