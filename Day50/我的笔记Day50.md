# Day50

## 今日内容

1. 文档加载完成事件
2. 对属性的操作
3. 文档操作
4. 事件
5. AJAX

## 内容详细

### 1.文档加载完成事件

当文档未加载完成之前，对内部的元素进行操作，绑定事件是不生效的，因为节点还不存在。所以对节点元素的操作要放在文档加载完成后执行。

- jQuery中的文档加载完成事件，事件不会覆盖。

  ```js
  //完整
  $(document).ready(function(){
      console.log($('div'));
  })
  
  //简写
  $(function(){
      console.log($('div'));
  })
  ```

- js中的文档加载完成事件，js中的事件有覆盖效果，后面再写前面的就不生效了。

  ```js
  window.onload = function(){
      console.log($('div'));
  }
  ```

### 2.对属性的操作

#### 2.1 attr和removeAttr方法

操作的标签上的属性。

- 设置或读取属性 attr

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<ul>
  			<li class="a">mjj</li>
  			<li class="b">wusir</li>
  			<li class="c">alex</li>
  			<li class="d">sb</li>
  		</ul>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  		 	$('ul li').attr('index',1);//设置所有匹配节点元素的属性和值
              $('ul li').attr({'index':1,'b':2});//设置多个属性
              console.log($('ul li').attr('index'));//这能获取匹配到的第一个节点元素的属性值
  		</script>
  	</body>
  </html>
  ```

- 移除属性 removeAttr

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<ul>
  			<li index="1" class="a">mjj</li>
  			<li index="2" class="b">wusir</li>
  			<li index="3" class="c">alex</li>
  			<li index="4" class="d">sb</li>
  		</ul>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  		 	$('ul li:odd').removeAttr('index');//为每一个匹配的节点元素移除一个属性
              $('ul li:odd').removeAttr('index active');//为每一个匹配的节点元素移除多个属性
  		</script>
  	</body>
  </html>
  ```

#### 2.2 prop和removeProp方法

操作的是节点元素对象的属性

- 设置或读取属性 prop

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  			$(function(){
  				console.log($('input[type=radio]').eq(0).prop('checked'));
  			})
  		</script>
  	</head>
  	<body>
  		<p id="box">mjj</p>
  		<input type="radio" name='sex' checked='checked'>男
  		<input type="radio" name='sex'>女
  		
  	</body>
  </html>
  ```

- 移除属性 removeProp

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  			$(function(){
  				console.log($('input[type=radio]').eq(0).removeProp('checked'));
  			})
  		</script>
  	</head>
  	<body>
  		<p id="box">mjj</p>
  		<input type="radio" name='sex' checked='checked'>男
  		<input type="radio" name='sex'>女
  		
  	</body>
  </html>
  ```

### 3.文档操作

- 父.append(子)    向父元素内的后面追加元素，返回父元素对象

  ```js
  //参数为js对象
  var obj = document.createElement('h3');
  obj.innerText = 'MJJ';
  $('#box').append(obj);//添加一个js对象，若js节点元素在文档中已存在，则为改变节点元素位置
  
  //参数为jQuery对象
  $('#box').append($('h4'));//若参数为jQuery对象，则为改变节点元素位置
  
  //参数为html
  $('box').append('<h4>mjj</h4>');
  
  //参数为文本
  $('box').append('mjj');
  ```

- 子.appendTo(父)  将子元素追加到父元素内的后面，返回子元素对象。用起来更方便可以链式编程在后面继续调用方法，操作子元素。

  ```js
  $('<a href="#">百度一下</a>').appendTo('#box').css('color','yellow');
  ```

- 父.prepend(子)  向父元素内的前面追加元素，返回父元素对象

  ```js
  $('#box').prepend('<p>hello world</p>');
  ```

- 子.prependTo(父)  将子元素追加到父元素内的前面，返回子元素对象。用起来更方便可以链式编程在后面继续调用方法，操作子元素。

  ```js
  $('<p>hello world</p>').prependTo('#box');
  ```

- 元素1.before(元素2)  将元素2添加到元素1的前边

  ```js
  $('h2').before('<p>hello world</p>');
  ```

- 元素1.insertBefore(元素2)  将元素1添加到元素2的前边

  ```js
  $('<h3>女神</h3>').insertBefore('h2');
  ```

- 元素1.after(元素2)  将元素2添加到元素1的后边

  ```js
  $('h2').after('<p>hello world</p>');
  ```

- 元素1.insertAfter(元素2)  将元素1添加到元素2的后边

  ```js
  $('<h3>女神</h3>').insertAfter('h2');
  ```

- 被替换的元素.replaceWith(要替换成的元素) 用提供的内容替换集合中所有匹配的元素并且返回被删除元素的集合。 

  ```js
  <div class="container">
    <div class="inner first">Hello</div>
    <div class="inner second">And</div>
    <div class="inner third">Goodbye</div>
  </div>
  
  $('div.inner').replaceWith('<h2>New heading</h2>');
  
  <div class="container">
    <h2>New heading</h2>
    <h2>New heading</h2>
    <h2>New heading</h2>
  </div>
  ```

- 要替换成的元素.replaceAll(被替换的元素)   返回要替换的元素

  ```js
  <div class="container">
    <div class="inner first">Hello</div>
    <div class="inner second">And</div>
    <div class="inner third">Goodbye</div>
  </div>
  
  $('<h2>New heading</h2>').replaceAll('.inner');
  
  <div class="container">
    <h2>New heading</h2>
    <h2>New heading</h2>
    <h2>New heading</h2>
  </div>
  ```

- 元素.remove(选择器) 移除匹配的元素，元素内部绑定的事件也会被移除。返回移除的元素对象。

  ```js
  <div class="container">
    <div class="hello">Hello</div>
    <div class="goodbye">Goodbye</div>
  </div>
  //remove()参数为空
  $('.hello').remove();
  //结果
  <div class="container">
    <div class="goodbye">Goodbye</div>
  </div>
  
  //参数不为空
  $('div').remove('.hello');
  //结果
  <div class="container">
    <div class="goodbye">Goodbye</div>
  </div>
  ```

- 元素.detach(选择器)  删除所有匹配的元素，事件会被保留。返回删除的元素对象。

  ```js
  <p class="hello">Hello</p> how are <p>you?</p>
  
  $("p").detach(".hello");
  
  how are <p>you?</p>
  ```

- 匹配元素.empty()  移除所有匹配的元素节点的所有子节点，无参数，内部嵌套的元素也会被移除。

  ```js
  <div class="container">
    <div class="hello">Hello</div>
    <div class="goodbye">Goodbye</div>
  </div>
  
  $('.hello').empty();
  
  <div class="container">
    <div class="hello"></div>
    <div class="goodbye">Goodbye</div>
  </div>
  ```

### 4.事件

#### 4.1 鼠标键盘事件

- 单击事件click和双击事件dblclick

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  	<button>点击</button>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
              //解决一次双击击事件会执行两次单击事件问题
              //解决思路：我们在单击事件中添加一个延时，若在延时所在的时长内没有做其他操作，则执行单击事件，若在延时的时长内，再次点击控件，则取消延时程序。这样就可以在一定程度上解决这个冲突。当然这个延时的时长设置是需要斟酌的，若太长，则单击事件有种缓慢的感觉，若太短，则起不到分离单击事件和双击事件的效果。
  			var times = null;
  			$('button').click(function () {
  				// 取消上次延时未执行的方法
  				clearTimeout(times);
  				//执行延时
  				times = setTimeout(function(){
  				console.log('单击了');
  			},300);
  			});
  
  			$('button').dblclick(function () {
  				clearTimeout(times);
  				console.log('双击了')
  			});
  		</script>
  	</body>
  </html>
  ```

  

- mouseover  鼠标悬浮事件，鼠标穿过父级元素和子元素都会调用方法

  ```js
  $('#box').mouseover(function(){
      console.log('移入进来了');
  
      $('#child').slideDown(1000);
  })
  ```

- mouseout    鼠标离开事件，鼠标穿过父级元素和子元素都会调用方法

  ```js
  $('#box').mouseout(function(){
      console.log('移除了');
  
      $('#child').slideUp(1000);
  })
  ```

- mouseenter  鼠标悬浮事件，鼠标穿过父级元素会调用方法

  ```js
  $('#box').mouseenter(function(){
      console.log('进来了');
  
      $('#child').stop().slideDown(1000);
  })
  ```

- mouseleave  鼠标离开事件，鼠标穿过父级元素会调用方法

  ```js
  $('#box').mouseleave(function(){
      console.log('离开了');
  
      $('#child').stop().slideUp(1000);
  })
  ```

- hover   鼠标悬浮和离开事件，接收两个回调函数分别对应悬浮和离开动作要做的操作

  ```js
  $('#box').hover(function(){
      $('#child').stop().slideDown(1000);
  },function(){
      $('#child').stop().slideUp(1000);
  })
  ```

- focus  聚焦事件

  ```js
  //默认文档加载聚焦行为
  $('input[type=text]').focus();
  
  
  $('input[type=text]').focus(function(){
      //聚焦
      console.log('聚焦了');
  })
  ```

- blur  失去焦点事件

  ```js
  $('input[type=text]').blur(function(){
      //失焦
      console.log('失去焦点了');
  })
  ```

- keydown  当键盘的键按下时触发

  ```js
  //e为事件对象，内部封装了大量事件相关的信息
  $('input[type=text]').keydown(function(e){
      console.log(e.keyCode);
      switch (e.keyCode){ //通过事件对象的keyCode属性，获取按下了哪个键（对应的ascii码）
          case 8:
              $(this).val(' ')
              break;
          default:
              break;
      }	
  })
  ```

#### 4.2 表单事件

- change  可用于单选，复选，下拉列表，input输入框内容的改变

  ```js
  $('input').change(function(){
       console.log($(this).val());//通过val()方法获取input的value值。通过text()方法和html()方法获取双闭合标签内部的文本或html
  })
  ```

- submit  点击提交按钮式触发

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<a href="javascript:;">百度游戏啊</a>//还可以通过该方式阻止a标签的连接
  		<form action="javascript:void(0)"> //也可以通过该方式阻止表单的默认提交方式
  			<input type="text" placeholder="用户名">
  			<input type="password" placeholder="密码">
  			<input type="submit">
  		</form>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  			$('form').submit(function(e){
  				e.preventDefault();//通过事件对象的preventDefault方法，阻止表单的默认提交action方式。
  				console.log('11111');
  			})
  		</script>
  	</body>
  </html>
  ```

- select   当选中时触发

  ```html
  <form>
    <input id="target" type="text" value="Hello there" />
  </form>
  <div id="other">
    Trigger the handler
  </div>
  <script>
      $('#target').select(function() {
        alert('Handler for .select() called.');
      });
  </script>
  ```

### 5.Ajax

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<div id="box">
			
		</div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			$(function(){
				// 获取首页的数据
				$.ajax({
					url:'https://api.apeland.cn/api/banner/',
					methods:'get',
					success:function(res){ //请求成功的回调，res包括了服务器返回的数据
						console.log(res);
						if(res.code === 0){
							var cover = res.data[0].cover;
							var name = res.data[0].name;
							console.log(cover);
							$('#box').append(`<img src=${cover} alt=${name}>`)
						}
					},
					error:function(err){
						console.log(err);
					}
				})
			})
		</script>
	</body>
</html>
```

