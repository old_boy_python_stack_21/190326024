#!/usr/bin/env python
# -*- coding:utf-8 -*-
from redis import Redis

class Debug(object):
    DEBUG = True
    SECRET_KEY = '123456'
    SESSION_COOKIE_NAME = 'I am debug session'
    SESSION_TYPE = 'redis'
    SESSION_REDIS = Redis(host='127.0.0.1',port=6379,db=4)

class Testing(object):
    TESTING = True
    SESSION_COOKIE_NAME = 'I am Not session'
    SESSION_TYPE = 'redis'
    SESSION_REDIS = Redis(host='127.0.0.1',port=6379,db=4)
