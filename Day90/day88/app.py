#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask,request,session,redirect,render_template,url_for
from blue.students import student
from blue.login import login
from settings import Debug,Testing
from flask_session import Session

app = Flask(__name__)

app.config.from_object(Debug)
# app.config.from_object(Testing)

app.register_blueprint(student)
app.register_blueprint(login)

Session(app=app)

@app.before_request
def Auth():
    if request.path == '/login':
        return
    if not session.get('user'):
        return redirect(url_for('login.my_login'))

@app.errorhandler(404)
def error(errorMessage):
    return render_template('error.html')


app.run()
