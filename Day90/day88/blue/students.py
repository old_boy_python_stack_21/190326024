#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Blueprint,render_template,request,views

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

student = Blueprint('student',__name__,url_prefix='/student')

class Student(views.MethodView):
    def get(self,*args,**kwargs):
        return render_template('student_list.html', student_list=STUDENT_DICT)


class Detail(views.MethodView):
    def get(self,*args,**kwargs):
        id = request.args.get('id')
        return render_template('student.html', student=STUDENT_DICT[int(id)], id=id)

student.add_url_rule('/list',view_func=Student.as_view(name='student_list'))
student.add_url_rule('/detail',view_func=Detail.as_view(name='detail'))