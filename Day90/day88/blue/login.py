#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Blueprint,request,session,redirect,render_template,views

login = Blueprint('login',__name__)


class Login(views.MethodView):
    def get(self,*args,**kwargs):
        return render_template('login.html')

    def post(self,*args,**kwargs):
        username = request.form.get('username')
        password = request.form.get('password')
        if username == 'alex' and password == '123':
            session['user'] = username
            return redirect('/student/list')
        return render_template('login.html')

login.add_url_rule('/login',endpoint='my_login',view_func=Login.as_view(name='my_login'))