# flask知识整理

### 1.request

- request.method 获取请求方式
- request.url  请求的完整url,包括参数
- request.path  请求的路径
- request.heads  请求头
- request.host  主机地址
- request.cookies  获取浏览器的cookies
- request.form   获取FormData中的数据
- request.args  获取url中的参数
- request.json  请求头中Content-Type:applications/json,请求体中的被序列化成json的数据
- request.data  请求头的Content-Type中不包含Form或FormDate时，保留的原始数据
- request.file  上传的文件

### 2.response

flask中一共有5种响应

- 直接返回字符串
- 返回模板  render_template
- 返回重定向 redirect
- 返回文件 send_file
- 返回json   jsonify

### 3.session

- session.get()
- session['key'] = value

### 4.蓝图Blueprint

```python
from flask import Blueprint
bp = Blueprint('user',__name__，url_prefix='/car')

@bp.route('/')
def add():
    pass
#运行文件
from car import bp
app.register_blueprint(bp)
```

### 5.路由

#### 5.1 路由的参数

- rule 路由地址
- methods 被允许的请求方式
- endpoint   Mapping路由地址和endpoint,endpoint在同一个app中不能出现重复，默认值为函数名
- defaults  设置默认值
- strict_slashes   是否严格遵守路由匹配
- redirect_to   永久重定向

#### 5.2  动态路由

```python
@app.route('up/<string:filename>')
def upload(filename):
```

### 6.初始化配置

- template_folder  配置模板文件目录
- static_folder 静态文件目录
- static_url_path  静态文件的访问路径

### 7.Config配置

```python
'DEBUG' #DEBUG模式
'TESTING'  #测试模式
'SECRET_KEY' #session加密字符串
'SESSION_COOKIE_NAME' #session存放在cookie中的名字
'PREMANENT_SESSION_LIFETIME' #session的过期时间
'JSONIFY_MIMETYPE' #使用jsonify响应是返回的Content-Type
```

配置方式

```PYTHON
class FlaskSetting():
	DEBUG = True
	SERECT_KEY = '12223423423'
```

可执行文件中引用

```
app.config.from_object(FlaskSetting)
```

### 8.特殊装饰器

- @app.before_request 在请求进入视图函数之前执行。按注册顺序执行
- @app.after_request  在响应返回客户端之前，结束视图函数之后执行，按注册顺序倒序执行
- @app.errorhandler()  请求错误或者服务器错误执行

### 9.CBV

```python
from flask import Flask,views

class Login(views.MethodView):
    def get(self,*args,**kwargs):
        return 'get'
    def post(self,*args,**kwargs):
        retuen 'post'
      
app.add_url_rule('/login',view_func=Login.as_view(name='login'))
    
```

