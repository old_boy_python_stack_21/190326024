# Day90

## 今日内容

1. CBV
2. redis
3. flask-session

## 内容详细

### 1.CBV

```python
from flask import Flask,views
class Login(views.MethodView):
    def get(self,*args,**kwargs):
        return('get')

    def post(self,*args,**kwargs):
        return('post')

app.add_url_rule('/login',view_func=Login.as_view(name='login')) #name参数相当于调用的视图函数的__name__，所以未设置endpoint的情况下不能重复
```

### 2.redis

#### 2.1 redis的指令

```
set key name  设置值
get key  获取值
keys *   查看当前仓库的所有的键
select 1  切换到1仓库
get n*  获取以n开头的键
```

#### 2.2 python中使用redis

下载redis模块

pip install redis

```
from redis import Redis
import json

redis_cli = Redis('127.0.0.1',6379,db=5)

redis_cli.set('name','rose')
print(redis_cli.get('name').decode('utf-8')) #读取到的数据为bytes类型

redis_cli.set('user',json.dumps({'name':'李白','age':18,'gender':'男'})) #对象需要序列化后再写入
print(json.loads(redis_cli.get('user'))) # 获取到的值要反序列化
```

### 3.flask-session

flask-session是一个第三方模块，他可以改变flask的默认的session的存放位置。设置或者获取session依然使用flask的session

```python
from flask import Flask,session,redirect,render_template,request
from redis import Redis
from flask_session import Session

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'reids' #设置存储位置
app.config['SESSION_REDIS'] = Redis(port='127.0.0.1',port=6379,db=4)#配置redis配置
Session(app=app) #实例化Session

@app.route('/login',methods=['GET','POST'])
def login1():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username == 'alex' and password == '123':
            session['user'] = username #设置session
            return redirect('/student/list')
    return render_template('login.html')
```

