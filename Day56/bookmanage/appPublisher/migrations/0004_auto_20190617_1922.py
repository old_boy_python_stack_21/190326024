# -*- coding: utf-8 -*-
# Generated by Django 1.11.21 on 2019-06-17 11:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appPublisher', '0003_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='iden_num',
            field=models.CharField(default=1, max_length=18),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='author',
            name='phone',
            field=models.CharField(default=1, max_length=20),
            preserve_default=False,
        ),
    ]
