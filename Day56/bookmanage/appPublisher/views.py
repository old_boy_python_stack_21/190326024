from django.shortcuts import render,redirect
from appPublisher import models

#展示出版社信息
def publisher_list(request):
    publishers_list = models.Publisher.objects.all().order_by('-pid')
    return render(request, 'publisher/publisher_list.html', {'publishers_list':publishers_list})

#删除出版社信息
def del_publisher(request):
    pid = request.GET.get('pid')
    obj_list = models.Publisher.objects.filter(pid=pid)
    obj_list.delete()
    return redirect('/publisher_list/')

#添加出版社信息
def add_publisher(request):
    error = ''
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        publisher_tel = request.POST.get('publisher_tel')
        publisher_addr = request.POST.get('publisher_addr')

        if models.Publisher.objects.filter(name=publisher_name):
            error = '出版社名称已存在'
        if not publisher_name:
            error = '出版社名称不能为空'
        if not error:
            models.Publisher.objects.create(name=publisher_name,phone=publisher_tel,addr=publisher_addr)
            return redirect('/publisher_list/')
    return render(request, 'publisher/add_publisher.html', {'error':error})

#编辑出版社信息
def edit_publisher(request):
    error = ''
    pid = request.GET.get('pid')
    obj = models.Publisher.objects.get(pid=pid)
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        publisher_tel = request.POST.get('publisher_tel')
        publisher_addr = request.POST.get('publisher_addr')

        if models.Publisher.objects.filter(name=publisher_name):
            error = '出版社名称已存在'
        if obj.name == publisher_name:
            error = '出版社名称与之前相同'
        if not publisher_name:
            error = '出版社名称不能为空'
        if not error:
            obj.name = publisher_name
            obj.phone = publisher_tel
            obj.addr = publisher_addr
            obj.save()
            return redirect('/publisher_list/')
    return render(request, 'publisher/edit_publisher.html', {'error':error, 'publisher':obj})


#展示出版社信息
def book_list(request):
    book_list = models.Book.objects.all().order_by('-bid')
    return render(request, 'book/book_list.html', {'book_list':book_list})

#删除出版社信息
def del_book(request):
    bid = request.GET.get('bid')
    book_list = models.Book.objects.filter(bid=bid)
    book_list.delete()
    return redirect('/book_list/')

#添加出版社信息
def add_book(request):
    error = ''
    publisher_list = models.Publisher.objects.all().order_by('-pid')
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name,pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name,pub_id=publisher)
            return redirect('/book_list/')
    return render(request, 'book/add_book.html', {'error':error,'publisher_list':publisher_list})

#编辑出版社信息
def edit_book(request):
    error = ''
    bid = request.GET.get('bid')
    book = models.Book.objects.get(bid=bid)
    publisher_list = models.Publisher.objects.all().order_by('-pid')
    if request.method == 'POST':

        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name,pub_id=publisher):
            error = '图书信息已存在已存在'
        if not book_name:
            error = '出版社名称不能为空'
        if not error:
            book.name = book_name
            book.pub_id = publisher
            book.save()
            return redirect('/book_list/')
    return render(request, 'book/edit_book.html', {'error':error, 'book':book,'publisher_list':publisher_list})


#展示作者信息
def author_list(request):
    author_list = models.Author.objects.all().order_by('-pk')
    return render(request,'author/author_list.html',{'author_list':author_list})

#添加作者信息
def add_author(request):
    error = ''
    book_list = models.Book.objects.all().order_by('-pk')
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        iden_num = request.POST.get('iden_num')
        phone = request.POST.get('phone')
        books = request.POST.getlist('books')
        if models.Author.objects.filter(iden_num=iden_num):
            error = '身份证号已存在'
        if not error:
            author = models.Author.objects.create(name=author_name,iden_num=iden_num,phone=phone)
            author.books.set(books)
            return redirect('/author_list/')
    return render(request,'author/add_author.html',{'error':error,'book_list':book_list})

#删除作者信息
def del_author(request):
    pk = request.GET.get('pk')
    author_list = models.Author.objects.filter(pk=pk)
    author_list.delete()
    return redirect('/author_list')

#编辑作者信息
def edit_author(request):
    error = ''
    pk = request.GET.get('pk')
    book_list = models.Book.objects.all().order_by('-pk')
    author = models.Author.objects.get(pk=pk)
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        iden_num = request.POST.get('iden_num')
        phone = request.POST.get('phone')
        books = request.POST.getlist('books')
        if iden_num != author.iden_num:
            if models.Author.objects.filter(iden_num=iden_num):
                error = '身份证号已存在'
        if not error:
            author.name = author_name
            author.iden_num = iden_num
            author.phone = phone
            author.save()
            author.books.set(books)
            return redirect('/author_list/')
    return render(request,'author/edit_author.html',{'error':error,'author':author,'book_list':book_list})

#首页
def index(request):
    return render(request,'index.html')