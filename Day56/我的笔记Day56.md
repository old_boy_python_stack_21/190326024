# Day56

## 今日内容

1. 作者表的创建（作者与书籍多对多）
2. 作者信息的增删改查

## 内容详细

### 1.创建作者表

```python
#书籍表
class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE)
    
#作者表
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  #多对多，django会帮助我么创建第三张表，无on_delete参数，默认就是级联删除
```

### 2.作者信息的操作

#### 2.1 查

```python
author_list = models.Author.objects.all()
    for author in author_list:
        print(author.name) #作者姓名
        print(author.books,type(author.books)) #多对多管理对象
        print(author.books.all()) #与该作者关联的所有书籍对象,模板代码调用方法时不用加括号
```

#### 2.2 增

```python
def add_author(request):
    error = ''
    book_list = models.Book.objects.all().order_by('-pk')
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        books = request.POST.getlist('books') #通过getlist获取多选数据，返回一个集合
        if not error:
            author = models.Author.objects.create(name=author_name) #插入数据返回插入的对象
            author.books.set(books)  #通过set设置多对多外键数据
            return redirect('/author_list/')
    return render(request,'author/add_author.html',{'error':error,'book_list':book_list})
```

#### 2.3 改

```python
def edit_author(request):
    error = ''
    pk = request.GET.get('pk')
    book_list = models.Book.objects.all().order_by('-pk')
    author = models.Author.objects.get(pk=pk)
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        books = request.POST.getlist('books')
        if not author_name:
            error = '作者姓名不能为空'
        if not error:
            author.name = author_name
            author.save()  #先修改姓名
            author.books.set(books) #然后更新关系表
            return redirect('/author_list/')
    return render(request,'author/edit_author.html',{'error':error,'author':author,'book_list':book_list})
```

### 3.其他创建多对多关系方式

1.django帮我们生成第三张表

```
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book')  # 不在Author表中生产字段，生产第三张表
```

2.自己创建第三张表

```python
class AuthorBook(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()
```

3.自建的表和 ManyToManyField 联合使用

```python
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField('Book',through='AuthorBook')  # 不在Author表中生产字段，生产第三张表


class AuthorBook(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    date = models.DateField()
```

