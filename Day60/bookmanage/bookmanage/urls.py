"""bookmanage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from appPublic import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^apppublisher/', include('appPublisher.urls',namespace='appPublisher')),
    url(r'^appbook/', include('appBook.urls',namespace='appBook')),
    url(r'^appauthor/', include('appAuthor.urls',namespace='appAuthor')),
    url(r'^apppublic/', include('appPublic.urls',namespace='appPublic')),


    url(r'^$', views.index),
]
