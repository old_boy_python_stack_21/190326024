from django.apps import AppConfig


class ApppublisherConfig(AppConfig):
    name = 'appPublisher'
