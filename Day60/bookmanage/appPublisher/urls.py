#!/usr/bin/env python
# -*- coding:utf-8 -*-

from django.conf.urls import url
from appPublisher import views

urlpatterns = [
    url(r'^publisher_list/$', views.publisher_list,name='publisher'),
    url(r'^add_publisher/$', views.add_publisher,name='add'),
    url(r'^edit_publisher/$', views.edit_publisher,name='edit'),
]