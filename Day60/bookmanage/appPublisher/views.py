from django.shortcuts import render, redirect,reverse
from appPublisher import models


# 展示出版社信息
def publisher_list(request):
    publishers_list = models.Publisher.objects.all().order_by('-pid')
    return render(request, 'publisher/publisher_list.html', {'publishers_list': publishers_list})


# 添加出版社信息
def add_publisher(request):
    error = ''
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        publisher_tel = request.POST.get('publisher_tel')
        publisher_addr = request.POST.get('publisher_addr')

        if models.Publisher.objects.filter(name=publisher_name):
            error = '出版社名称已存在'
        if not publisher_name:
            error = '出版社名称不能为空'
        if not error:
            models.Publisher.objects.create(name=publisher_name, phone=publisher_tel, addr=publisher_addr)
            return redirect(reverse('appPublisher:publisher'))
    return render(request, 'publisher/add_publisher.html', {'error': error})


# 编辑出版社信息
def edit_publisher(request):
    error = ''
    pid = request.GET.get('pid')
    obj = models.Publisher.objects.get(pid=pid)
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        publisher_tel = request.POST.get('publisher_tel')
        publisher_addr = request.POST.get('publisher_addr')

        if models.Publisher.objects.filter(name=publisher_name):
            error = '出版社名称已存在'
        if obj.name == publisher_name:
            error = '出版社名称与之前相同'
        if not publisher_name:
            error = '出版社名称不能为空'
        if not error:
            obj.name = publisher_name
            obj.phone = publisher_tel
            obj.addr = publisher_addr
            obj.save()
            return redirect(reverse('appPublisher:publisher'))
    return render(request, 'publisher/edit_publisher.html', {'error': error, 'publisher': obj})



