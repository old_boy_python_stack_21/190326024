from django.db import models

class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE)

class Author(models.Model):
    name = models.CharField(max_length=32)
    iden_num = models.CharField(max_length=18)
    phone = models.CharField(max_length=20)
    books = models.ManyToManyField('Book')  #多对多，django会帮助我么创建第三张表，无on_delete参数，默认就是级联删除