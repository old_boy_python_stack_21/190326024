from django.shortcuts import render,redirect,reverse
from appPublisher import models

# 展示作者信息
def author_list(request):
    author_list = models.Author.objects.all().order_by('-pk')
    return render(request, 'author/author_list.html', {'author_list': author_list})


# 添加作者信息
def add_author(request):
    error = ''
    book_list = models.Book.objects.all().order_by('-pk')
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        iden_num = request.POST.get('iden_num')
        phone = request.POST.get('phone')
        books = request.POST.getlist('books')
        if models.Author.objects.filter(iden_num=iden_num):
            error = '身份证号已存在'
        if not error:
            author = models.Author.objects.create(name=author_name, iden_num=iden_num, phone=phone)
            author.books.set(books)
            return redirect(reverse('appAuthor:author'))
    return render(request, 'author/add_author.html', {'error': error, 'book_list': book_list})


# 编辑作者信息
def edit_author(request):
    error = ''
    pk = request.GET.get('pk')
    book_list = models.Book.objects.all().order_by('-pk')
    author = models.Author.objects.get(pk=pk)
    if request.method == 'POST':
        author_name = request.POST.get('author_name')
        iden_num = request.POST.get('iden_num')
        phone = request.POST.get('phone')
        books = request.POST.getlist('books')
        if iden_num != author.iden_num:
            if models.Author.objects.filter(iden_num=iden_num):
                error = '身份证号已存在'
        if not error:
            author.name = author_name
            author.iden_num = iden_num
            author.phone = phone
            author.save()
            author.books.set(books)
            return redirect(reverse('appAuthor:author'))
    return render(request, 'author/edit_author.html', {'error': error, 'author': author, 'book_list': book_list})
