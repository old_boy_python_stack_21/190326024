#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.conf.urls import url
from appAuthor import views

urlpatterns = [
    url(r'^author_list/$', views.author_list,name='author'),
    url(r'^add_author/$', views.add_author,name='add'),
    url(r'^edit_author/$', views.edit_author,name='edit'),
]