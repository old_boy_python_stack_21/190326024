#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.conf.urls import url
from appBook import views

urlpatterns = [
    url(r'^book_list/$', views.BookList.as_view(),name='book'),
    url(r'^add_book/$', views.AddBook.as_view(),name='add'),
    url(r'^edit_book/$', views.EditBook.as_view(),name='edit'),
]