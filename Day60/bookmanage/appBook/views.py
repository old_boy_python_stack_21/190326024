from django.shortcuts import render,redirect,reverse
from django.views import View
from appPublisher import models

# 展示书籍信息
class BookList(View):
    def get(self, request):
        book_list = models.Book.objects.all().order_by('-bid')
        return render(request, 'book/book_list.html', {'book_list': book_list})

# 添加书籍信息
class AddBook(View):

    def get(self, request):
        error = ''
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        return render(request, 'book/add_book.html', {'error': error, 'publisher_list': publisher_list})

    def post(self, request):
        error = ''
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name, pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name, pub_id=publisher)
            return redirect(reverse('appBook:book'))
        return render(request, 'book/add_book.html', {'error': error, 'publisher_list': publisher_list})


# 编辑书籍信息
class EditBook(View):
    book = None
    def get(self, request):
        error = ''
        bid = request.GET.get('bid')
        EditBook.book = models.Book.objects.get(bid=bid)
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        return render(request, 'book/edit_book.html', {'error': error, 'book': EditBook.book, 'publisher_list': publisher_list})

    def post(self, request):
        error = ''
        if not EditBook.book:
            bid = request.GET.get('bid')
            EditBook.book = models.Book.objects.get(bid=bid)
        publisher_list = models.Publisher.objects.all().order_by('-pid')

        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')
        if book_name != EditBook.book.name or publisher != EditBook.book.pub.name:
            if models.Book.objects.filter(name=book_name, pub_id=publisher):
                error = '图书信息已存在'
        if not book_name:
            error = '出版社名称不能为空'
        if not error:
            EditBook.book.name = book_name
            EditBook.book.pub_id = publisher
            EditBook.book.save()
            return redirect(reverse('appBook:book'))
        return render(request, 'book/edit_book.html', {'error': error, 'book': EditBook.book, 'publisher_list': publisher_list})
