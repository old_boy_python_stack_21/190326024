/**
 * Created by Administrator on 2017/12/12.
 * author 375361172@qq.com
 * 解释类
 */

(function(){
    /*使用闭包 防止用户在控制台对一些属性进行修改 */
    //创建一个绘制背景的canvas
    //var gs=0;
    //缓存对象
    var bufObj=new Object();
    //中心对象
    var centerObj=new Object();
    //地形动画对象
    centerObj.animateDx=new Array();
    //是否开始游戏
    centerObj.start=false;
    //中心动画
    function gameEnd(O){
        O.animate.stop();
        O.animate02.stop();
        O.animate03.stop();
        var txt= O.camera.canvas.txt;
        var c=O.camera.canvas;
        txt.clearRect(0,0, c.w, c.h);
        txt.beginPath();
        txt.fillStyle="#000000";
        txt.fillRect(0,0,c.w, c.h);
        txt.textAlign="center";
        txt.textBaseline="middle";
        txt.fillStyle="#ffffff";
        txt.fillText("鶸,就这样挂了! 刷新重新开始吧", c.w/2, c.h/2);
        txt.closePath();
    }

    centerObj.animate=new HGAME.animate({
        action:function(){
            //镜头对象着色
            centerObj.camera.render();
            centerObj.renderFun();

            //移动
            centerObj.user.moveLeftRight();
            centerObj.user.moveTopBottom();
            //用户判断地形
            centerObj.user.colFunDx(dxArr);
            //改变镜头
            centerObj.user.changeCamera(centerObj.camera);
            if( centerObj.user.y>600){
                gameEnd(centerObj);
            }
            if(centerObj.user.death==true){
                gameEnd(centerObj);
            }

            //怪物移动函数
            var q=0;
            for(var i = 0;i<monArr.length;i++){
                if(typeof monArr[i]=="undefined"){
                    continue;
                }
                for(q = 0;q<monArr[i].method.length;q++){
                    if(monArr[i].method[q]=="colFunDx"){
                        monArr[i].colFunDx(dxArr);
                    }else if(monArr[i].method[q]=="colHurt"){
                        monArr[i].colHurt(centerObj.user);
                    }else if(monArr[i].method[q]=="monA"){
                        if(monArr[i].wuqiImg.src==""){
                            monArr[i].wuqiImg.src=wuqiDataArr[monArr[i].wuqiPointer];
                        }
                        monArr[i].monA(wqArr,monArr[i].wuqiImg,4,centerObj.bgCanvas,
                            {
                                x: monArr[i].x+ monArr[i].w/2-10,
                                y: monArr[i].y+ monArr[i].h/2-10,
                                w:20,
                                h:20
                            },centerObj.user);
                    }else if(monArr[i].method[q]=="changeGs"){
                        if(monArr[i].changeGs.call(monArr[i],centerObj)=="1"){
                            return;
                        };
                    }else if(monArr[i].method[q]=="behavior"){
                        monArr[i].behavior.call(monArr[i],centerObj)
                    }else if(monArr[i].method[q]=="znHT"){
                        monArr[i].behavior.call(monArr[i],centerObj.user);
                    }
                    else{
                        monArr[i][monArr[i].method[q]]&& monArr[i][monArr[i].method[q]]();
                    }

                }
                if(monArr[i].removeThis==true){
                    //移出怪物
                    monArr[i].removeCarmeFun.call(monArr[i],centerObj);
                    centerObj.bgCanvas.remove(monArr[i]);
                    monArr.splice(i,1);
                }
            }
            //武器移动函数
            for( i = 0;i<wqArr.length;i++){
                if(typeof wqArr[i]=="undefined"){
                    continue;
                }
                for(q=0;q<wqArr[i].method.length;q++){
                    if(wqArr[i].method[q]=="colFun"){
                        wqArr[i][wqArr[i].method[q]](
                            {
                                user:centerObj.user,
                                monArr:monArr,
                                dxArr:dxArr
                            }
                        )
                    }else{
                        wqArr[i][wqArr[i].method[q]]();
                    }
                }
                if( wqArr[i].death==true){
                    centerObj.bgCanvas.remove(wqArr[i]);
                    wqArr.splice(i,1);
                }

            }
            for( i = 0;i<goodsArr.length;i++){
                if(typeof   goodsArr[i]=="undefined"){
                    continue;
                }
                if(goodsArr[i].method){
                    for(q=0; q<goodsArr[i].method.length;q++){
                        if(goodsArr[i].method[q]=="colMethod"){
                            goodsArr[i][goodsArr[i].method[q]](centerObj.user);
                            continue;
                        }
                        goodsArr[i][goodsArr[i].method[q]]();
                    }
                }

            }
            //地形移动函数
            for( i=0;i<centerObj.animateDx.length;i++){
                centerObj.animateDx[i].changeY();
            }

        },
        time:30//时间间隔
    });
    //中心动画
    centerObj.animate02=new HGAME.animate({//人物帧改变计时器
        action:function(){
            centerObj.user.changeFrame();

            for(var i = 0;i<monArr.length;i++){
                if(typeof monArr[i]=="undefined"){
                    continue
                }
                for(var q = 0;q<monArr[i].method02.length;q++){
                    if(monArr[i].method02[q]=="colFunDx"){
                        monArr[i].colFunDx(dxArr);
                    }else{
                        monArr[i][monArr[i].method02[q]]&& monArr[i][monArr[i].method02[q]]();
                    }

                }
            }
            for( i = 0;i<goodsArr.length;i++){
                if(typeof goodsArr[i]=="undefined"){
                    continue
                }
            if(goodsArr[i].method02){
                for(q=0; q<goodsArr[i].method02.length;q++){
                    goodsArr[i][goodsArr[i].method02[q]]();
                }
            }
            }
            if(centerObj.user.THJnowTime>0){
                centerObj.user.createTHJ(centerObj,bufObj.THJImg);
            }
        },
        time:150
    });
    //中心动画
    centerObj.animate03=new HGAME.animate({//人物帧改变计时器
        action:function(){

            for(var  i = 0;i<goodsArr.length;i++){
                if(goodsArr[i].method02){
                    for(var q=0; q<goodsArr[i].method02.length;q++){
                        goodsArr[i][goodsArr[i].method02[q]]();
                    }
                    if(goodsArr[i].death){
                        centerObj.bgCanvas.remove(goodsArr[i]);
                    }
                }
            }
        },
        aSecondAction:function(){
            if(centerObj.user.THJnowTime>0){
                centerObj.user.THJnowTime--;
            }
            if(centerObj.user.THJnowInterTime>0){
                centerObj.user.THJnowInterTime--;
            }
            if(centerObj.user.SXJnowInterTime>0){
                centerObj.user.SXJnowInterTime--;
            }
            if(centerObj.user.SXJnowTime>0){
                centerObj.user.SXJnowTime--;
                centerObj.user.hurtTnterval=120;
            }else{
                if( centerObj.user.isSXJ==true)
                {
                    centerObj.user.isSXJ=false;
                    centerObj.user.hurtTnterval=centerObj.user.bufHurt;
                }
            }
            centerObj.timeInt--;
            if(centerObj.user.isHurtInt<=0){
                centerObj.user.isHurtInt=0;
                centerObj.user.isHurt=true;
            }else{
                centerObj.user.isHurtInt--;
                centerObj.user.isHurt=false;
            }
        },
        time:120
    });
    centerObj.gs=0;
    //添加节点托管 重写等号运算符
    defineEx(centerObj,"gs",0,function(){
        changeGs(centerObj.gs);
    });
    /*创建一个镜头对象*/
    centerObj.camera=new HGAME.camera({
        w:800,
        h:480
    });
    centerObj.camera.y=40;
    //创建一个dx数组
    var dxArr=HGAME.dxArr=new Array();
    //创建一个怪物数组
    var monArr=HGAME.monArr=new Array();
    //
    var wqArr=HGAME.wqArr=new Array();
    //物品数组
    var goodsArr=HGAME.goodsArr=new Array();
    /*创建一个加载资源对象*/
    centerObj.source=new HGAME.source({
        data:[],
        loadCall:function(num,allNum){
            var txt=centerObj.camera.canvas.txt;
            var w=centerObj.camera.canvas.w,h=centerObj.camera.canvas.h;
            txt.clearRect(0,0,w,h);
            txt.fillStyle="#000000";
            txt.fillRect(0,0,w,h);
            txt.font="normal normal 20px/24px arial";
            txt.fillStyle="#ffffff";
            txt.textAlign="center";
            txt.textBaseline="middle";
            txt.fillText("加载资源:"+num+"/"+allNum,w/2,h/2);
        },
        loaded:function(THIS){

        }

    });
    //一些可以通过data.js配置的对象
    var local=new Object();
    bufObj.THJImg=new Image();

    centerObj.bgCanvas= new HGAME.canvas({
        x:0,
        y:0,
        w:6000,
        h:400
    });
    centerObj.bgObj=new HGAME.Object2D({
        img:null,
        w:centerObj.bgCanvas.w,
        h:centerObj.bgCanvas.h,
        x:0,
        y:0,
        repeatType:"repeat"
    });
    centerObj.user=new HGAME.userObj({
        img:null,
        w:32,
        h:48,
        x:0,
        y:0,
        H_INT:2
    });
    centerObj.camera.add(centerObj.bgCanvas);
    document.getElementById("demo").appendChild(centerObj.camera.canvas.dom);
    function changeGs(gs){
        //清空所有缓存
       empty(dxArr);
       empty(monArr);
       empty(wqArr);
       empty(goodsArr);
        //停止各个动画的计时器
       centerObj.animate.stop();
       centerObj.animate02.stop();
       centerObj.animate03.stop();
        //清空绘图缓存
       centerObj.bgCanvas.empty();
        //改变用户的x y
       centerObj.user.x=0;
       centerObj.user.y=0;
       //重置游戏总时间
        centerObj.timeInt=300;
     //  centerObj.user.isHurt=false;
        //加载资源
       centerObj.source.loadedSource(txDataArr[gs],function(THIS){
           //加载完成改变背景和用户图片
           centerObj.bgObj.img=THIS.data[0];
           centerObj.user.img=THIS.data[1];
           bufObj.THJImg.src=jnDataArr[2];
           //修改镜头对象的源canvas的宽高
           centerObj.bgCanvas.w=gsInfo[gs].w;
           centerObj.bgCanvas.h=gsInfo[gs].h;
           //添加绘制对象
           centerObj.bgCanvas.add(centerObj.bgObj);

           //解释data.js的各个配置项
           function getOption(objGet,Source){
               var pzArr=[];
               var option={};
               for(var q in objGet){
                   if(typeof objGet[q]=="string"){
                       pzArr= objGet[q].split("=>");
                       if(pzArr.length>1){
                           switch (pzArr[0]){
                               case "attr":{
                                   option[q]=centerObj[pzArr[1]][pzArr[2]]+parseFloat(pzArr[3]);
                                   break;
                               }
                               case "set":{
                                   if(pzArr[1]=="local"){
                                       local[pzArr[2]]=pzArr[3];
                                   }
                                   break;
                               }
                           }
                       }else{

                           option[q]=objGet[q];


                       }
                   }else{

                       if(q=="img"){
                           option[q]=THIS.data[objGet[q]];
                       }else{
                           option[q]=objGet[q];
                       }


                   }
               }
               return option;
           }
           function resetDx(objGet){
               var buf=new HGAME.Object2D(getOption(objGet));
               if(local["bool"]==false){
                   dxArr.push(buf);
               }
               centerObj.bgCanvas.add(buf);
               return buf;
           }
           this.loadedSource(dxArrData[gs],function(){
               var nowGs= dxArrExData[gs];

               for(var i =0;i<nowGs.length;i++){
                   local["bool"]=false;
                   if(typeof nowGs[i].type!="undefined"){

                       switch ( nowGs[i].type){
                           case "elevator":{
                               var arr=new Array();
                               for(var o = 0;o<nowGs[i].child.length;o++){
                                   arr.push(resetDx(nowGs[i].child[o]));
                               }
                               centerObj.animateDx.push(HGAME.dxElevator(arr,centerObj.camera,typeof nowGs[i].fx=="undefined"?"top":nowGs[i].fx));
                           }
                       }
                       continue;
                   }

                   resetDx(nowGs[i]);
               }
               this.loadedSource(monArrData[gs],function(){
                   var option=null;

                   function expOption(info){
                       var infoObj=new Object();
                       for(var i in info){
                           if(typeof info[i]=="string"){
                               var arrBuf=info[i].split("=>");
                               if(arrBuf[0]=="attr"){
                                   infoObj[i]=centerObj[arrBuf[1]][arrBuf[2]]+parseFloat(arrBuf[3]);
                               }else{
                                   infoObj[i]=info[i];
                               }
                           }else{
                               if(i=="img"){
                                   infoObj[i]=THIS.data[info[i]]
                               }else{
                                   infoObj[i]=info[i];
                               }

                           }
                       }

                       return infoObj;
                   }
                   for(var q = 0;q<monArrExData[gs].length;q++){
                       option=expOption(monArrExData[gs][q]);
                       var bufs=new HGAME.Object2DMonster(option);
                       centerObj.bgCanvas.add(bufs);
                       monArr.push(bufs);
                   }
                   for(q = 0;q<goodsArrExData[gs].length;q++ ){
                       option=expOption(goodsArrExData[gs][q]);
                       var bufss=new HGAME.Goods(option);
                       centerObj.bgCanvas.add(bufss);
                       goodsArr.push(bufss);
                   }
                   centerObj.bgCanvas.add(centerObj.user);
                    setTimeout(function(){
                        centerObj.animate.run();
                        centerObj.animate02.run();
                        centerObj.animate03.run();
                    },500)
               });


           });
           //  centerObj.animate.run();
       });
   }
//    changeGs(0);

    //开始游戏触发的背景
    var bgStart=new Image();
    bgStart.src="img/bg/start.jpg";
    function bgFun(O){
        //绘制背景
        var t =  centerObj.camera.canvas.txt;
        t.drawImage(O,0,0);
        //用户点击开始游戏区域
        var start={
            x:320,
            y:254,
            w:160,
            h:60
        };
        //用户点击继续游戏区域
        var joinStart={
            x:322,
            y:341,
            w:160,
            h:60
        };
        //为canvas添加点击事件 进行判断
        centerObj.camera.canvas.dom.addEventListener("click",function(e){
            //如果游戏已经开始了就return
            if(centerObj.start==true)return;
            //鼠标相对与canvas的x y
            var x = e.offsetX;
            var y = e.offsetY;
            //判断点的位置是否在设置的区域里面
            if(inRect({x:x,y:y},start)){
                //如果点击的是开始 就默认加载第一关数据并修改游戏状态为 true
                changeGs(0);
                centerObj.start=true;
            }else if(inRect({x:x,y:y},joinStart)){
                //如果是点击的继续游戏就读取数据 通过h5的localstorage
                var gs=localStorage.gs?localStorage.gs:0;
                if(typeof localStorage.user!="undefined"){
                    var userInfo=JSON.parse(localStorage.user);
                    centerObj.user.hurt=userInfo.hurt;
                    centerObj.user.fyl=userInfo.fyl;
                    centerObj.user.allPH=userInfo.allPH;
                    centerObj.user.PH=userInfo.PH;

                    centerObj.user.THJinfo=userInfo.THJinfo;
                    centerObj.user.hurtTnterval=userInfo.hurtTnterval;
                    centerObj.user.hurtDestance=userInfo.hurtDestance;
                }
                //读取数据通过缓存的获取
                centerObj.gs=gs;
                centerObj.start=true;
            }
        });

        bufObj.THJinfo=new HGAME.canvas(
            {
                w:200,
                h:100,
                x:0,
                y:340
            }
        );
        bufObj.SXJinfo=new HGAME.canvas(
            {
                w:200,
                h:100,
                x:20,
                y:340
            }
        );

        function jnSay(txt,name,say,cx,jg){
          txt.beginPath();
            txt.fillStyle="rgba(0,0,0,0.5)";
            txt.fillRect(0,0, bufObj.THJinfo.w, bufObj.THJinfo.h);
            txt.font="normal normal 16px/20px arial";
            txt.textAlign="left";
            txt.textBaseline="top";
            txt.fillStyle="#ffff00";
            txt.fillText(name,10,16);
            txt.fillStyle="#ffffff";
            txt.font="normal normal 12px/16px arial";
            txt.fillText(say,10,20+16,bufObj.THJinfo.w-20);
            txt.fillText("持续时间:"+cx+"秒",10,36+16,bufObj.THJinfo.w-20);
            txt.fillText("冷却时间:"+jg+"秒",10,52+16,bufObj.THJinfo.w-20);
            txt.closePath();
        }
        jnSay(bufObj.THJinfo.txt,"天火决","召唤天火对敌人进行伤害。",10,20);
        jnSay(bufObj.SXJinfo.txt,"碎心决","减少25%的生命值提高攻速到最大。",20,30);
        centerObj.camera.canvas.dom.addEventListener("mouseout",function(){
            centerObj.camera.canvas.remove(bufObj.THJinfo);
            centerObj.camera.canvas.remove(bufObj.SXJinfo);
        });
        centerObj.camera.canvas.dom.addEventListener("mousemove",function(e){
            //如果游戏已经开始了就return
            if(centerObj.start==false)return;
            var THJ={
                x:10,
                y:440,
                w:60,
                h:40
            };
            //用户点击继续游戏区域
            var SXJ={
                x:80,
                y:440,
                w:40,
                h:40
            };
            //鼠标相对与canvas的x y
            var x = e.offsetX;
            var y = e.offsetY;
            //判断点的位置是否在设置的区域里面
            if(inRect({x:x,y:y},THJ)){
                centerObj.camera.canvas.remove(bufObj.THJinfo);
                centerObj.camera.canvas.add(bufObj.THJinfo);
            }else{
                centerObj.camera.canvas.remove(bufObj.THJinfo);
            }
            if(inRect({x:x,y:y},SXJ)){
                centerObj.camera.canvas.remove(bufObj.SXJinfo);
                centerObj.camera.canvas.add(bufObj.SXJinfo);
            }else{
                centerObj.camera.canvas.remove(bufObj.SXJinfo);
            }
        });
    }
    if(bgStart.complete){
        bgFun(bgStart);
    }else{
        bgStart.onload=function(){
            bgFun(bgStart);
        }
    }
    //centerObj.user.isHurt=false;
   // centerObj.animate.showFPS();
    /*添加移动事件*/
    var funTbuf=0;
    //顶部ui
    function funT(txt){
        txt.font="normal normal 16px/20px arial";
        txt.textBaseline="top";
        txt.textAlign="left";
        txt.fillStyle="#ffffff";
        //攻击力
        txt.fillText("攻击力:"+centerObj.user.hurt,10,10);
        funTbuf=funTbuf+10+txt.measureText("攻击力:"+centerObj.user.hurt).width;
        txt.fillText("防御力:"+centerObj.user.fyl,30+funTbuf,10);
        funTbuf=funTbuf+30+txt.measureText("防御力:"+centerObj.user.fyl).width;
        //攻击距离
        txt.fillText("生命值:"+centerObj.user.PH+"/"+centerObj.user.allPH,30+funTbuf,10);
        funTbuf=funTbuf+30+txt.measureText("生命值:"+centerObj.user.PH+"/"+centerObj.user.allPH).width;
        //攻击间隔
        txt.fillText("攻击间隔:"+centerObj.user.hurtTnterval+"ms",30+funTbuf,10);
        funTbuf=funTbuf+30+txt.measureText("攻击间隔:"+centerObj.user.hurtTnterval+"ms").width;
        //攻击距离
        txt.fillText("攻击距离:"+centerObj.user.hurtDestance,30+funTbuf,10);
        funTbuf=funTbuf+30+txt.measureText("攻击距离:"+centerObj.user.hurtDestance).width;

        //time
        txt.fillText("时间:"+centerObj.timeInt,centerObj.camera.w-(txt.measureText("时间:"+centerObj.timeInt).width+10),10);
        funTbuf=0;
    }
    var funBbuf={
        intBuf:0
    };
    //底部ui
    function initBBuf(){
        funBbuf.THJ1=new Image();
        funBbuf.THJ1.src=jnDataArr[0];
        funBbuf.THJ2=new Image();
        funBbuf.THJ2.src=jnDataArr[1];
        funBbuf.SXJ=new Image();
        funBbuf.SXJ.src=jnDataArr[3];
        funBbuf.title=new Image();
        funBbuf.title.src="img/bg/title.png";
    }
    initBBuf();
    function funB(txt,y){
        if(centerObj.user.THJinfo==0){
            txt.drawImage(funBbuf.THJ2,10,y);
        }else if(centerObj.user.THJinfo>0){
            txt.drawImage(funBbuf.THJ1,10,y);
        }
        txt.fillStyle="rgba(0,150,255,0.5)";
        txt.beginPath();
        txt.moveTo(20+10,20+y);
        txt.arc(20+10,20+y,14,0,Math.PI*centerObj.user.THJnowInterTime/centerObj.user.THJInterTime*2);
        txt.fill();
        txt.closePath();
        funBbuf.intBuf+=50;
        txt.fillStyle="#ffffff";
        txt.fillText("× "+centerObj.user.THJinfo, funBbuf.intBuf,y+10);
        funBbuf.intBuf= funBbuf.intBuf+txt.measureText("× "+centerObj.user.THJinfo).width;
        txt.drawImage(funBbuf.SXJ, funBbuf.intBuf+10,y);
        txt.fillStyle="rgba(0,255,155,0.5)";
        txt.beginPath();
        txt.moveTo(funBbuf.intBuf+30,20+y);
        txt.arc(funBbuf.intBuf+30,20+y,14,0,Math.PI*centerObj.user.SXJnowInterTime/centerObj.user.SXJInterTime*2);
        txt.fill();
        txt.closePath();

        funBbuf.intBuf+=50;
        txt.drawImage( funBbuf.title, centerObj.camera.canvas.w-10-120,y);
        funBbuf.intBuf=0;
    }
    centerObj.renderFun=function(){
        centerObj.camera.canvas.txt.beginPath();
        centerObj.camera.canvas.txt.fillStyle="#000000";
        centerObj.camera.canvas.txt.fillRect(0,0,centerObj.camera.w,40);
        funT(centerObj.camera.canvas.txt);
        centerObj.camera.canvas.txt.fillStyle="#000000";
        centerObj.camera.canvas.txt.fillRect(0,centerObj.camera.h-40,centerObj.camera.w,40);
        funB(centerObj.camera.canvas.txt,centerObj.camera.h-40);
        centerObj.camera.canvas.txt.closePath();
    };


    window.onkeydown=function(e){
        switch (e.keyCode){
            case 68:{
                centerObj.user.moveType="left";
                centerObj.user.H_INT=2;
                break;
            }
            case 74:{
                if(centerObj.user.wuqiImg.src==""){
                    centerObj.user.wuqiImg.src=wuqiDataArr[centerObj.user.wuqiPointer];
                }

                centerObj.user.A(wqArr,centerObj.user.wuqiImg,4,centerObj.bgCanvas,{
                    x:centerObj.user.x+centerObj.user.w/2-10,
                    y:centerObj.user.y+centerObj.user.h/2-10,
                    w:20,
                    h:20
                });
                break;
            }
            case 65:{
                centerObj.user.moveType="right";
                centerObj.user.H_INT=1;
                break;
            }
            case 87:{
                if(centerObj.user.jumpNum>0){
                    centerObj.user.jumpNum--;
                    centerObj.user.speedT=centerObj.user.maxSpeedT;
                    centerObj.user.moveTypeT="top";
                }
                break;
            }
        }
    };
    window.onkeyup=function(e){
        switch (e.keyCode){
            case 50:{
                centerObj.user.SXJ();
                break;
            }
            case 65:{
                centerObj.user.moveType="stop";
                break;
            }
            case 68:{
                centerObj.user.moveType="stop";
                break;
            }
            case 49:{
                centerObj.user.isSkyfire();
                break;
            }
            case 87:{
                centerObj.user.moveTypeT="bottom";
            }
        }
    }
})();