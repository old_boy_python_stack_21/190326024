/**
 * Created by Administrator on 2017/12/16.
 * author 375361172@qq.com
 * 数据类
 */
//一些特殊的资源
var txDataArr=[
    [
        "img/bg/BG1.png",//bg
        "img/rw/USER.png",//用户
        "img/wuqi/wuqi01.png",//武器子弹
        "img/wp/attr/gs02.png",//攻速
        "img/wp/attr/a02.png",//攻击力
        "img/wp/attr/hx02.png",//增加生命值
        "img/wp/attr/smsx02.png",//生命上限
        "img/jn/THJ02.png",//可以释放技能1
        "img/jn/THJ03.png",//不可以释放技能
        "img/jn/THJ04.png",//技能投射物
        "img/bg/title.png"//作者代号
    ],
    [
        "img/bg/BG02.png",//bg
        "img/rw/USER.png",//用户
        "img/wuqi/wuqi01.png",//武器子弹
        "img/wuqi/wuqi02.png",
        "img/wp/attr/gs02.png",//攻速
        "img/wp/attr/a02.png",//攻击力
        "img/wp/attr/hx02.png",//增加生命值
        "img/wp/attr/smsx02.png",//生命上限
        "img/wp/attr/gjfw02.png",//攻击范围
        "img/wp/attr/yf02.png",//防御力
        "img/jn/THJ02.png",//可以释放技能1
        "img/jn/THJ03.png",//不可以释放技能
        "img/jn/THJ04.png",//技能投射物
        "img/bg/title.png"//作者代号
    ],
    [
        "img/bg/BG02.png",//bg
        "img/rw/USER.png",//用户
        "img/wuqi/wuqi01.png",//武器子弹
        "img/wuqi/wuqi02.png",
        "img/wp/attr/gs02.png",//攻速
        "img/wp/attr/a02.png",//攻击力
        "img/wp/attr/hx02.png",//增加生命值
        "img/wp/attr/smsx02.png",//生命上限
        "img/wp/attr/gjfw02.png",//攻击范围
        "img/wp/attr/yf02.png",//防御力
        "img/jn/THJ02.png",//可以释放技能1
        "img/jn/THJ03.png",//不可以释放技能
        "img/jn/THJ04.png",//技能投射物
        "img/bg/title.png"//作者代号
    ]
];

var wuqiDataArr=[
    "img/wuqi/wuqi01.png",
    "img/wuqi/wuqi02.png"
];

var jnDataArr=[
    //天火决0-2
    "img/jn/THJ02.png",//可以释放技能1
    "img/jn/THJ03.png",//不可以释放技能
    "img/jn/THJ04.png",//技能投射物

    //天火决3
    "img/jn/SXJ.png"//可以释放技能1
];
//地形资源数组
var dxArrData=[
    [
        "img/dx/sl/DX.png",
        "img/dx/sl/sl_zsw_1.png",
        "img/dx/sl/sl_zsw_2.png",
        "img/dx/sl/sl_zsw_3.png",
        "img/dx/sl/sl_zsw_4.png",
        "img/dx/sl/dxTool.png"
    ],
    [
        "img/dx/sl/02/DX_.png",
        "img/dx/sl/02/sl_zsw_1.png",
        "img/dx/sl/02/sl_zsw_2.png",
        "img/dx/sl/02/sl_zsw_3.png",
        "img/dx/sl/02/sl_zsw_4.png"
    ],
    [
        "img/dx/sl/02/DX_.png",
        "img/dx/sl/02/sl_zsw_1.png",
        "img/dx/sl/02/sl_zsw_2.png",
        "img/dx/sl/02/sl_zsw_3.png",
        "img/dx/sl/02/sl_zsw_4.png"
    ]
];
var dxArrExData=[
    [
        {
            img:0,//指向加载数据的第0个
            w:600,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:0,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
           // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:400,//高度 type=>ObjName=>attr=>pc
            x:-200,//x  type=>ObjName=>attr=>pc
            y:0,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:400,//高度 type=>ObjName=>attr=>pc
            x:6000,//x  type=>ObjName=>attr=>pc
            y:0,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,
            w:20,
            h:40,
            x:100,
            y:"attr=>bgCanvas=>h=>-101",
            repeatType:"repeat"
        },
        {
            img:1,
            w:20,
            h:80,
            x:162,
            y:"attr=>bgCanvas=>h=>-141",
            repeatType:"repeat"
        },
        {
            img:1,
            w:20,
            h:120,
            x:222,
            y:"attr=>bgCanvas=>h=>-181",
            repeatType:"repeat"
        },
        {
            img:1,
            w:20,
            h:80,
            x:282,
            y:"attr=>bgCanvas=>h=>-141",
            repeatType:"repeat"
        },
        {
            img:4,
            w:40,
            h:15,
            x:650,
            y:"attr=>bgCanvas=>h=>-85",
            repeatType:"repeat",
            set01:"set=>local=>bool=>true"
        },
        {
            img:2,
            w:40,
            h:70,
            x:650,
            y:"attr=>bgCanvas=>h=>-70",
            repeatType:"repeat"
        },
        {
            img:2,
            w:200,
            h:90,
            x:750,
            y:"attr=>bgCanvas=>h=>-90",
            repeatType:"repeat"
        },
        {
            img:4,
            w:200,
            h:15,
            x:750,
            y:"attr=>bgCanvas=>h=>-105",
            repeatType:"repeat",
            set01:"set=>local=>bool=>true"
        },
        {
            img:0,
            w:1000,
            h:60,
            x:1100,
            y:"attr=>bgCanvas=>h=>-60",
            repeatType:"repeat"
        },
        {
            img:3,
            w:40,
            h:40,
            x:1325,
            y:"attr=>bgCanvas=>h=>-100",
            repeatType:"repeat"
        },
        {
            img:3,
            w:40,
            h:40,
            x:1715,
            y:"attr=>bgCanvas=>h=>-100",
            repeatType:"repeat"
        },
        {
            img:3,
            w:100,
            h:20,
            x:1815,
            y:"attr=>bgCanvas=>h=>-140",
            repeatType:"repeat"
        },
        {
            img:3,
            w:100,
            h:20,
            x:1990,
            y:"attr=>bgCanvas=>h=>-180",
            repeatType:"repeat"
        },
        {
            img:3,
            w:200,
            h:20,
            x:2180,
            y:"attr=>bgCanvas=>h=>-180",
            repeatType:"repeat"
        },
        {
            img:3,
            w:200,
            h:20,
            x:2480,
            y:"attr=>bgCanvas=>h=>-220",
            repeatType:"repeat"
        },
        {
            img:3,
            w:200,
            h:20,
            x:2760,
            y:"attr=>bgCanvas=>h=>-220",
            repeatType:"repeat"
        },

		  {
            img:0,
            w:1000,
            h:60,
            x:2180,
            y:"attr=>bgCanvas=>h=>-60",
            repeatType:"repeat"
        },
        {
            img:0,//指向加载数据的第0个
            w:300,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:4950,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:60,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:5400,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:5,//指向加载数据的第0个
            w:320,//宽度 type=>ObjName=>attr=>pc
            h:40,//高度 type=>ObjName=>attr=>pc
            x:5150,//x  type=>ObjName=>attr=>pc
            y:200,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            set01:"set=>local=>bool=>true"
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:60,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:5620,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:5830,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            type:"elevator",
            child:[
                {
                    img:3,
                    w:240,
                    h:20,
                    x:3300,
                    y:"attr=>bgCanvas=>h=>-220",
                    repeatType:"repeat"
                },
                {
                    img:3,
                    w:240,
                    h:20,
                    x:3620,
                    yChangeSpeed:3,
                    y:"attr=>bgCanvas=>h=>-220",
                    repeatType:"repeat"
                },
                {
                    img:3,
                    w:240,
                    h:20,
                    x:4500,
                    yChangeSpeed:3,
                    y:"attr=>bgCanvas=>h=>-220",
                    repeatType:"repeat"
                }
            ]
        },
        {
            fx:"patrolType",
            type:"elevator",
            child:[
                {
                    xChangeSpeed:2,
                    patrolTypeArgu:{
                        start:{
                            x:3900,
                            y:200
                        },
                        end:{
                            x:4200,
                            y:200
                        }
                    },
                    bufPatrolType:0,
                    img:3,
                    w:200,
                    h:20,
                    x:3900,
                    y:200,
                    repeatType:"repeat"
                }
            ]
        }
    ],
    [
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:400,//高度 type=>ObjName=>attr=>pc
            x:-200,//x  type=>ObjName=>attr=>pc
            y:0,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:400,//高度 type=>ObjName=>attr=>pc
            x:6000,//x  type=>ObjName=>attr=>pc
            y:0,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:150,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:0,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:80,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:300,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:120,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:530,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:4,
            w:40,
            h:15,
            x:650,
            y:"attr=>bgCanvas=>h=>-105",
            repeatType:"repeat",
            set01:"set=>local=>bool=>true"
        },
        {
            img:2,
            w:40,
            h:90,
            x:650,
            y:"attr=>bgCanvas=>h=>-90",
            repeatType:"repeat"
        },
        {
            img:4,
            w:200,
            h:15,
            x:1290,
            y:"attr=>bgCanvas=>h=>-105",
            repeatType:"repeat",
            set01:"set=>local=>bool=>true"
        },
        {
            img:2,
            w:200,
            h:90,
            x:1290,
            y:"attr=>bgCanvas=>h=>-90",
            repeatType:"repeat"
        },
        {
            img:3,
            w:100,
            h:20,
            x:770,
            y:268,
            repeatType:"repeat"
        },
        {
            img:3,
            w:100,
            h:20,
            x:1020,
            y:268,
            repeatType:"repeat"
        },
        {
            img:0,//指向加载数据的第0个
            w:150,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:1640,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:160,//宽度 type=>ObjName=>attr=>pc
            h:20,//高度 type=>ObjName=>attr=>pc
            x:2140,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:160,//宽度 type=>ObjName=>attr=>pc
            h:20,//高度 type=>ObjName=>attr=>pc
            x:2380,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-120",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:160,//宽度 type=>ObjName=>attr=>pc
            h:20,//高度 type=>ObjName=>attr=>pc
            x:2140,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-180",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:160,//宽度 type=>ObjName=>attr=>pc
            h:20,//高度 type=>ObjName=>attr=>pc
            x:2380,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-240",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:3200,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:3300,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:3400,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },

        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:3500,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:150,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:3620,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:3850,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:3950,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:4050,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:4150,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:5680,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:1,//指向加载数据的第0个
            w:20,//宽度 type=>ObjName=>attr=>pc
            h:80,//高度 type=>ObjName=>attr=>pc
            x:5780,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-80",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:5880,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            fx:"patrolType",
            type:"elevator",
            child:[
                {
                    xChangeSpeed:2,
                    patrolTypeArgu:{
                        start:{
                            x:690,
                            y:356
                        },
                        end:{
                            x:1090,
                            y:356
                        }
                    },
                    bufPatrolType:0,
                    img:3,
                    w:200,
                    h:20,
                    x:690,
                    y:356,
                    repeatType:"repeat"
                },
                {
                    xChangeSpeed:2,
                    bufPatrolType:0,
                    img:3,
                    patrolTypeArgu:{
                        start:{
                            x:2600,
                            y:160
                        },
                        end:{
                            x:2900,
                            y:160
                        }
                    },
                    w:200,
                    h:20,
                    x:2600,
                    y:160,
                    repeatType:"repeat"
                },
                {
                    xChangeSpeed:2,
                    bufPatrolType:0,
                    img:3,
                    patrolTypeArgu:{
                        start:{
                            x:4750,
                            y:160
                        },
                        end:{
                            x:5100,
                            y:160
                        }
                    },
                    w:200,
                    h:20,
                    x:4750,
                    y:160,
                    repeatType:"repeat"
                }
            ]
        },
        {
            fx:"top",
            type:"elevator",
            child:[
                {
                    img:3,
                    w:100,
                    h:20,
                    x:1920,
                    y:"attr=>bgCanvas=>h=>-220",
                    repeatType:"repeat"
                },
                {
                    img:3,
                    w:100,
                    h:20,
                    x:4280,
                    y:"attr=>bgCanvas=>h=>-220",
                    repeatType:"repeat"
                },
                {
                    yChangeSpeed:1,
                    img:3,
                    w:200,
                    h:20,
                    x:4480,
                    y:"attr=>bgCanvas=>h=>-220",
                    repeatType:"repeat"
                },
                {
                    img:3,
                    w:200,
                    h:20,
                    x:5400,
                    y:"attr=>bgCanvas=>h=>-100",
                    repeatType:"repeat"
                }
            ]
        }
    ],
    [
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:400,//高度 type=>ObjName=>attr=>pc
            x:-200,//x  type=>ObjName=>attr=>pc
            y:0,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:200,//宽度 type=>ObjName=>attr=>pc
            h:400,//高度 type=>ObjName=>attr=>pc
            x:800,//x  type=>ObjName=>attr=>pc
            y:0,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:0,//指向加载数据的第0个
            w:800,//宽度 type=>ObjName=>attr=>pc
            h:60,//高度 type=>ObjName=>attr=>pc
            x:0,//x  type=>ObjName=>attr=>pc
            y:"attr=>bgCanvas=>h=>-60",//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:100,//宽度 type=>ObjName=>attr=>pc
            h:20,//高度 type=>ObjName=>attr=>pc
            x:360,//x  type=>ObjName=>attr=>pc
            y:200,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:100,//宽度 type=>ObjName=>attr=>pc
            h:40,//高度 type=>ObjName=>attr=>pc
            x:250,//x  type=>ObjName=>attr=>pc
            y:300,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:100,//宽度 type=>ObjName=>attr=>pc
            h:40,//高度 type=>ObjName=>attr=>pc
            x:600,//x  type=>ObjName=>attr=>pc
            y:300,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        },
        {
            img:3,//指向加载数据的第0个
            w:100,//宽度 type=>ObjName=>attr=>pc
            h:20,//高度 type=>ObjName=>attr=>pc
            x:100,//x  type=>ObjName=>attr=>pc
            y:260,//y type=>ObjName=>attr=>pc 类型=>对象名称=>属性=>偏差
            repeatType:"repeat"//是否重复绘制
            // set01:"set=>local=>bool=>true" 设置一些关键性的变量
        }
    ]
];


//怪物资源数组
var monArrData=[
    [
        "img/monster/mon01.png",
        "img/monster/next.png"
    ],
    [
        "img/monster/mon01.png",
        "img/monster/mon02.png",
        "img/monster/next.png"
    ],
    [
        "img/monster/mon03.png"
    ]
];
var monArrExData=[
    [
        {
            method:["moveTopBottom","moveLeftRight","colFunDx","colHurt","monA"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            x:1600,
            y:0,
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            patrolArguments:{
                startX:302,
                endX:570
            },
            x:302,
            y:"attr=>bgCanvas=>h=>-108",
            w:32,
            h:48
        },
        {
            method:["patrol","monA","colHurt"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            patrolArguments:{
                startX:745,
                endX:925
            },
            x:745,
            y:"attr=>bgCanvas=>h=>-138",
            w:32,
            h:48
        },
        {
            method:["patrol","monA","colHurt"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            patrolArguments:{
                startX:2174,
                endX:2352
            },
            x:2174,
            y:172,
            w:32,
            h:48
        },
        {
            method:["patrol","monA","colHurt"],
            method02:["changeFrame"],
            img:0,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            patrolArguments:{
                startX:2473,
                endX:2659
            },
            x:2473,
            y:132,
            w:32,
            h:48
        },
        {
            method:["colFunDx","patrol","moveTopBottom","monA","colHurt"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            patrolArguments:{
                startX:4500,
                endX:4710
            },
            x:4500,
            y:132,
            w:32,
            h:48
        },
        {
            method:["patrol","monA","colHurt"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            patrolArguments:{
                startX:2751,
                endX:2931
            },
            x:2751,
            y:132,
            w:32,
            h:48
        },
        {
            method:["colFunDx","patrol","moveTopBottom","monA","colHurt"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            patrolArguments:{
                startX:3296,
                endX:3514

            },
            img:0,
            x:3296,
            y:0,
            w:32,
            h:48
        },
        {
            method:["colFunDx","patrol","moveTopBottom","monA","colHurt"],
            method02:["changeFrame"],
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            patrolArguments:{
                startX:3611,
                endX:3835,speed:2
            },
            img:0,
            x:3611,
            y:0,
            w:32,
            h:48
        },
        {
            method:["changeGs"],
            method02:["changeFrame"],
            changeGs:function(O){
                //debugger;
                var str=O.user.colObj.test(this, O.user);
                if(str!="pzNO"){
                    O.gs++;
                    O.camera.x=0;
                    //储存过关的数据
                    localStorage.gs= O.gs;
                    localStorage.user= JSON.stringify({
                        hurt: O.user.hurt,
                        fyl: O.user.fyl,
                        allPH: O.user.allPH,
                        PH: O.user.PH,
                        THJinfo: O.user.THJinfo,
                        hurtTnterval: O.user.hurtTnterval,
                        hurtDestance:O.user.hurtDestance
                    });
                    return "1";
                }
            },
            img:1,
            x:5942,
            y:275,
            w:25,
            h:68,
            showPH:false
        }
    ],
    [
        {
            method:["changeGs"],
            method02:["changeFrame"],
            changeGs:function(O){
                //debugger;
                var str=O.user.colObj.test(this, O.user);
                if(str!="pzNO"){
                    O.gs++;
                    O.camera.x=0;
                    //储存过关的数据
                    localStorage.gs= O.gs;
                    localStorage.user= JSON.stringify({
                        hurt: O.user.hurt,
                        fyl: O.user.fyl,
                        allPH: O.user.allPH,
                        PH: O.user.PH,
                        THJinfo: O.user.THJinfo,
                        hurtTnterval: O.user.hurtTnterval,
                        hurtDestance:O.user.hurtDestance
                    });
                    return "1";
                }
            },
            img:2,
            x:5942,
            y:275,
            w:25,
            h:68,
            showPH:false
        },
        {
            method:["colFunDx","patrol","moveTopBottom","monA","colHurt"],
            method02:["changeFrame"],
            hurt:40,
            hurtDestance:250,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            dropNum:0.75,
            img:1,
            PH:250,
            fyl:10,
            wuqiPointer:1,
            patrolArguments:{
                startX:4481,
                endX:4652
            },
            x:4481,
            y:132,
            w:32,
            h:48
        },
        {
            method:["colFunDx","patrol","moveTopBottom","monA","colHurt"],
            method02:["changeFrame"],
            hurt:40,
            hurtDestance:250,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            dropNum:0.75,
            img:1,
            PH:250,
            fyl:10,
            wuqiPointer:1,
            patrolArguments:{
                startX:5391,
                endX:5570
            },
            x:5391,
            y:132,
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:40,
            hurtDestance:250,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>50",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            dropNum:0.75,
            img:1,
            PH:250,
            fyl:10,
            wuqiPointer:1,
            patrolArguments:{
                startX:521,
                endX:618
            },
            x:521,
            y:"attr=>bgCanvas=>h=>-108",
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:40,
            hurtDestance:250,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>50",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            dropNum:0.75,
            img:1,
            PH:250,
            fyl:10,
            wuqiPointer:1,
            patrolArguments:{
                startX:2371,
                endX:2510
            },
            x:2510,
            y:112,
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:40,
            hurtDestance:250,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>50",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            dropNum:0.75,
            img:1,
            PH:250,
            fyl:10,
            wuqiPointer:1,
            patrolArguments:{
                startX:3613,
                endX:3739
            },
            x:3613,
            y:292,
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:40,
            hurtDestance:250,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>50",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            dropNum:0.75,
            img:1,
            PH:250,
            fyl:10,
            wuqiPointer:1,
            patrolArguments:{
                startX:2132,
                endX:2268
            },
            x:2132,
            y:172,
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:25,
            hurtDestance:200,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            PH:100,
            fyl:5,
            wuqiPointer:0,
            patrolArguments:{
                startX:1284,
                endX:1450
            },
            x:1284,
            y:"attr=>bgCanvas=>h=>-135",
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:25,
            hurtDestance:200,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            PH:100,
            fyl:5,
            wuqiPointer:0,
            patrolArguments:{
                startX:2379,
                endX:2511
            },
            x:2379,
            y:"attr=>bgCanvas=>h=>-165",
            w:32,
            h:48
        },
        {
            method:["patrol","colHurt","monA"],
            method02:["changeFrame"],
            hurt:25,
            hurtDestance:200,
            deathGoods:[
                {
                    method:["colMethod"],
                    method02:["animate"],
                    x:0,
                    attr:"PH=>30",
                    y:0,
                    w:36,
                    h:36,
                    imgSrc:"img/wp/attr/hx02.png"
                }
            ],
            img:0,
            PH:100,
            fyl:5,
            wuqiPointer:0,
            patrolArguments:{
                startX:1636,
                endX:1763
            },
            x:1636,
            y:296,
            w:32,
            h:48
        }
    ],
    [
        {
            method:["moveTopBottom","colFunDx","colHurt","behavior","monA","znHT"],
            method02:["changeFrame"],
            img:0,
            x:500,
            y:0,
            w:31,
            PH:2000,
            hurt:80,
            h:32,
            removeCarmeFun:function(O){
                O.animate.stop();
                O.animate02.stop();
                O.animate03.stop();
                var txt= O.camera.canvas.txt;
                var c=O.camera.canvas;
                txt.clearRect(0,0, c.w, c.h);
                txt.beginPath();
                txt.fillStyle="#000000";
                txt.fillRect(0,0,c.w, c.h);
                txt.textAlign="center";
                txt.textBaseline="middle";
                txt.fillStyle="#ffffff";
                txt.fillText("厉害了! 我的哥", c.w/2, c.h/2);
                txt.closePath();
            }
        }
    ]
];


//物品资源数组
//img/wp/attr/gs.png 40*40
var goodsArrExData=[
    [
        {
            method:["colMethod"],
            method02:["animate"],
            x:90,
            attr:"hurt=>5",
            y:"attr=>bgCanvas=>h=>-140",
            w:36,
            h:36,
            imgSrc: "img/wp/attr/a02.png"
        }
        ,
        {
            method:["colMethod"],
            method02:["animate"],
            x:654,
            attr:"hurtTnterval=>-120",
            y:"attr=>bgCanvas=>h=>-110",
            w:36,
            h:36,
            imgSrc:"img/wp/attr/gs02.png"
        },
        {
            method:["colMethod"],
            method02:["animate"],
            x:3100,
            attr:"allPH=>50&PH=>50",
            y:150,
            w:36,
            h:36,
            imgSrc:"img/wp/attr/smsx02.png"
        }
    ],
    [
        {
            method:["colMethod"],
            method02:["animate"],
            x:3100,
            attr:"allPH=>50&PH=>50",
            y:150,
            w:36,
            h:36,
            imgSrc:"img/wp/attr/smsx02.png"
        },
        {
            method:["colMethod"],
            method02:["animate"],
            x:5050,
            attr:"allPH=>50&PH=>50",
            y:50,
            w:36,
            h:36,
            imgSrc:"img/wp/attr/smsx02.png"
        },
        {
            method:["colMethod"],
            method02:["animate"],
            x:1090,
            attr:"fyl=>10",
            y:220,
            w:36,
            h:36,
            imgSrc:"img/wp/attr/yf02.png"
        },
        {
            method:["colMethod"],
            method02:["animate"],
            attr:"hurt=>10",
            x:90,
            y:"attr=>bgCanvas=>h=>-180",
            w:36,
            h:36,
            imgSrc:"img/wp/attr/a02.png"
        },
        {
            method:["colMethod"],
            method02:["animate"],
            attr:"hurt=>15",
            x:2507,
            y:112,
            w:36,
            h:36,
            imgSrc:"img/wp/attr/a02.png"
        },
        {
            imgSrc:"img/wp/attr/gjfw02.png",
            method:["colMethod"],
            method02:["animate"],
            attr:"hurtDestance=>50",
            x:90,
            y:"attr=>bgCanvas=>h=>-140",
            w:36,
            h:36
        }
    ],
    []
];
//关数info
var gsInfo=[
    {
        w:6000,
        h:400
    },
    {
        w:6000,
        h:400
    },
    {
        w:800,
        h:400
    }
];