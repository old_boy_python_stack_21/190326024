#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.conf.urls import url
from appPublic import views

urlpatterns = [
    url(r'^del_(book|author|publisher)/(\d+)$', views.delete,name='del'),
]