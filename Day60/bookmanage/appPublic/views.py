from django.shortcuts import render,redirect,reverse
from appPublisher import models
# 首页
def index(request):
    return render(request, 'index.html')

#公共删除
def delete(request,table,pk):
    cls = getattr(models,table.capitalize())
    obj_list = cls.objects.filter(pk=pk)
    obj_list.delete()
    url = 'app{}:{}'.format(table.capitalize(),table)
    return redirect(reverse(url))
