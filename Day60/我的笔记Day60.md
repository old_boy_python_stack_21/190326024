# Day60

## 今日内容

1. urlconf配置参数
2. url分组命名
3. 传递额外参数
4. 命名url和url反向解析
5. 命名空间模式

## 内容详细

### 1.urlconf配置参数

1. 基本格式

   ```python
   '''
   from django.conf.urls import url
   urlpatterns = [
       url(正则表达式,views视图，参数，别名)
   ]
   '''
   urlpatterns = [
       url(r'^articles/2003/$', views.special_case_2003),
       url(r'^articles/([0-9]{4})/$', views.year_archive),
       url(r'^articles/([0-9]{4})/([0-9]{2})/$', views.month_archive),
       url(r'^articles/([0-9]{4})/([0-9]{2})/([0-9]+)/$', views.article_detail),
   ]
   ```

   参数说明:

   - 正则表达式：一个正则表达式字符串，用来匹配地址
   - views视图：一个可调用对象，通常为一个视图函数
   - 参数：可选的要传递给视图函数的默认参数（字典形式）
   - 别名：一个可选的name参数

    正则表达式注意事项：

   - urlpatterns中的元素按照书写的顺序从上往下逐一匹配正则表达式，一但匹配成功则不再继续。
   - 若要从url中捕获一个值只需要在它周围放置一对圆括号（分组匹配）
   - 不需要添加一个前导的反斜杠，因为每个url都有。如：'^demo',而不是'^/demo'
   - 每个正则表达式前面的'r'是可选的，但建议加上。取消转义

2. APPEND_SLASH配置

   ```python
   # 是否开启URL访问地址后面不为/跳转至带有/的路径的配置项
   APPEND_SLASH=True
   ```

   若该配置项为true，我们在地址栏输入末尾不带'/'的url地址，则django会重定向末尾带'/'的地址。

   默认没有 APPEND_SLASH 这个参数，但 Django 默认这个参数为 APPEND_SLASH = True。 其作用就是自动在网址结尾加'/'。

### 2.url命名分组

#### 2.1 分组

分组会将括号内匹配到的 内容按照位置传参的方式，传入视图函数中

```python
#url代码
urlpatterns = [
    url(r'^demo/(/d+)/$', views.demo),
]

#视图函数代码
def demo(request,pk):
    pass
```

#### 2.2 命名分组

命名分组会将分组匹配到的内容，以命名为关键字按关键字传参的方式传到后台函数中

```python
#url代码
urlpatterns = [
    url(r'^demo/(?P<pk>/d+)/$', views.demo),
]

#视图函数代码  针对于url'/demo/12' 会按以下方式调用视图函数
# demo(pk='12')
def demo(request,pk):
    pass
```

**注意：urlconf中捕获的参数都是字符串格式。**

#### 2.3 为视图函数参数指定默认值

如下：两个url指向同一个视图函数。

```python
#urls.py
urlpatterns = [
    url(r'^demo/$', views.demo),
    url(r'^demo/(?P<pk>/d+)/$', views.demo),
]

#视图函数
def demo(request,pk=12):
    pass
```

#### 2.4 include其他urlconfs

- 在业务越来越复杂的情况下，项目下的urls.py也会越来越复杂。因此我们可以将不同业务的路由放到自己对应的app中。
- 在多人协作开发项目的过程中，为了避免多人操作urls.py文件的冲突问题。所以需要需要路由分发。

在对应的业务功能app中创建自己的urls.py文件，内部写上自己相关的路由配置。然后在项目下的urls.py中引用即可。

```	python
#app01下的urls.py
from django.conf.urls import url
urlpatterns = [
    url(r'^demo/$', views.demo),
    url(r'^demo1/(?P<pk>/d+)/$', views.demo1),
]

#项目下的urls.py
from django.conf.urls import url,include
urlpatterns = [
    url(r'^app01/$',include('app01.urls')), #包含其他文件的urlconfs文件
]
```

### 3.传递额外参数

`django.conf.urls.url()` 可以接收一个可选的第三个参数，它是一个字典，表示想要传递给视图函数的额外关键字参数。

```python
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^blog/(?P<year>[0-9]{4})/$', views.year_archive, {'foo': 'bar'}),
]
#对于/blog/2005/请求，将会这样调用视图函数：year_archive(year='2005',foo='bar')
```

**注：若命名分组和额外参数键相同，传入视图中的是额外参数。**

### 4.命名url和url反向解析

为了提高项目的可扩展和易维护性。我们应该避免使用固定写死的url来发送请求。

Django 提供一个办法是让URL 映射是URL 设计唯一的地方。你填充你的URLconf，然后可以双向使用它：

- 根据用户/浏览器发起的URL 请求，它调用正确的Django 视图，并从URL 中提取它的参数需要的值。
- 根据Django 视图的标识和将要传递给它的参数的值，获取与之关联的URL。

通俗来讲，就是为urlconf起一个名称，当我们在视图函数中要重定向一个路由或者在html的a标签或表单提交请求的时，通过这个别名来设置路由地址。

```python
#urls.py
from django.conf.urls import url

urlpatterns = [
    url(r'^home/',views.func,name='home'),
    url(r'^demo/(\d+)/',views.func,name='demo'), #分组
    url(r'^demo/(?P<pk>\d+)/',views.func,name='index'), #命名分组
]

```

```html
<!--html文件-->
<a href="{% url 'home' %}">home</a> 
<a href="{% url 'demo' 12 %}">demo</a> <!--分组-->
<a href="{% url 'index' pk=12 %}">index</a> <!--命名分组-->
```

```python
#视图函数
#可以通过下列两种方式导入reverse
from django.shortcuts import reverse,redirect
from django.urls import reverse

def func(request):
    redirect(reverse('home'))
    redirect(reverse('demo',args=(12,))) #分组
    redirect(reverse('index',kwargs={'pk':12}))#命名分组
```

### 5.命名空间模式

如果两个app下的urlconf的别名相同，可以通过命名空间来区分调用。

项目下的urls.py文件

```python
from django.conf.urls import url
urlpatterns = [
    url(r'^app01/',include('app01.urls')),
    url(r'^app02/',include('app02.urls')),
]
```

app01下的urls.py文件

```python
from django.conf.urls import url
urlpatterns = [
    url(r'book/(?P<pk>\d+)$',views.book,name='demo')
]
```

app02下的urls.py文件

```python
from django.conf.urls import url
urlpatterns = [
    url(r'author/(?P<pk>\d+)$',views.book,name='demo')
]
```

当调用时便无法确定调用哪一个路由了

```python
reverse('demo',kwargs={'pk':12})
```

```html
<a href="{% url 'demo' pk=12 %}">demo</a>
```

所以可以使用命名空间namespase解决该问题。修改项目下的urls.py文件如下：

```python
from django.conf.urls import url
urlpatterns = [
    url(r'^app01/',include('app01.urls',namespase = 'app01')),
    url(r'^app02/',include('app02.urls',namespase = 'app02')),
]
```

然后用以下方式调用：

```python
reverse('app01:demo',kwargs={'pk':12})
```

```html
<a href="{% url 'app02:demo' pk=12 %}">demo</a>
```

