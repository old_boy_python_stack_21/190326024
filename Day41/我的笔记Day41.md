# Day41

## 补充

- having 后面的条件要么是select的字段，要么是分组的字段。

## 今日内容

1. 索引原理
2. 正确的使用索引
3. 数据库的备份、恢复，表的备份、恢复
4. 事务和锁
5. sql注入和pymysql

## 内容详细

### 1.索引原理

#### 1.1 定义

一个建立在存储表阶段的一个存储结构，能在查询的时候加快速度。（相当于目录）

重要性：

- 在实际场景中读写的比例大概10:1，所以要加快读的速度。

#### 1.2 索引的原理

##### 1.2.1 磁盘预读原理 block

每次委托操作系统去硬盘读取内容时，操作系统为减少io操作，总是多读出来一部分数据到内存中，以便于下次读取。

- 读硬盘的io操作的时间非常长，比CPU执行指令的时间长很多。
- 尽量的减少io次数才是读写数据的主要要解决的问题。

##### 1.2.2 数据库的存储方式

1. 存储结构-树
2. 平衡树 balance tree   --b树
3. 在b树的基础上进行改良，b+树。
   - 分支节点和根节点都不再存储实际的数据
     - 让分支和根节点能存储更多的索引的信息
     - 降低了树的高度
     - 所有的实际数据都存储在叶子节点中
   - 在叶子节点之间加入了双向的链式结构
     - 方便在查询中范围条件查找
4. mysql当中的所有的b+树索引的高度都基本控制在3层
   - io操作的次数非常稳定
   - 有利于通过范围查询

5. 树的高度会影响索引的效率
   - 尽量选择短的列创建索引
   - 对区分度高的列创建索引，重复率超过了10%那么就不适合做索引。

##### 1.2.3 索引的种类

- 聚集索引：数据直接存储在树结构的叶子节点
- 辅助索引：数据不直接存储在树中

在innodb中既有聚集索引，又有辅助索引。

在mysiam中，只有辅助索引，没有聚集索引。

- primary key  主键   聚集索引    约束的作用：非空 + 唯一
- unique  自带索引  辅助索引   约束的作用：唯一
- index 辅助索引 没有约束作用

##### 1.2.4 创建索引语法

```sql
create index 索引名字 on 表(字段);
drop index 索引名字 on 表名字;
```

```sql
select * from 表名 where id = xxxx;
--在id字段没有索引的时候，效率低
--在id字段有索引的之后，效率高
```

### 2.正确的使用索引

#### 2.1 索引不生效

- 要查询的数据范围大

  - < , >,>=,<=,!=

  - between  and

    ```sql
    select * from 表 order by age limit 1000000,5;--越到后面效率越低，每次都要把前面的数据全查出来，再从1000001条开始取5条
    
    select * from 表 where id between 1000000 and 1000005;--效率快，根据索引只取中间需要的5条数据
    ```

  - like

    - 结果的范围大，索引不生效
    - 如果xxx% 索引生效，%xxx索引不生效

- 如果一列内容的区分度不高，索引不生效

- 索引不能在条件中参与计算，否则不生效

  ```sql
  select * from 表 where id * 10 = 1000000;--不生效，数据库内部会将每个树结构的索引值乘以10 后，再与1000000比较。
  ```

- 对两列内容进行条件查询

  - and 条件两端的内容，优先选择一个有索引的，并且树形结构更好的，来进行查询。and条件需要所有条件都满足，先完成范围小速度快的（带索引的）以减小后面的压力

    ```sql
    select * from s1 where id = 1000000 and email = 'eva1000000@oldboy';
    ```

  - or 条件不会进行优化，只是根据条件从左到右依次筛选。带or的想要命中索引，这些条件的列必须都是索引列。

- 联合索引

  ```sql
  create index ind_mix on s1(id,email);
  ```

  - 在联合索引中如果使用了or条件，则索引就不能生效

    ```sql
    select * from s1 where id = 1000000 or email = 'eva1000000@oldboy';
    ```

  - 最左前缀原则：在联合索引中，条件必须含有在创建索引的时候的第一个索引列，索引才会命中

    ```sql
    select * from s1 where id = 1000000; --可以命中
    select * from s1 where email = 'eva1000000@oldboy'; --不能命中
    ```

  - 在整个条件中，从开始出现模糊匹配,大范围查询的那一刻，索引就失效了。

    ```sql
    select * from s1 where id > 1000000 and email = 'eva1000000@oldboy'; --不生效
    select * from s1 where id = 1000000 and email like 'eva%'; --生效
    ```

#### 2.2 应用场景

##### 2.2.1 联合索引

- 只对a 或者abc条件进行筛选，而不会对b,c进行单列的条件筛选，可以创建联合索引。

  ```sql
  create index ind_abc on s1(a,b,c);
  ```

##### 2.2.2 单列索引

- 选择一个区分度高的列建立索引
- 使用or来连接多个条件
  - 在满足上述条件的基础上，对or相关的所有列分别创建索引

#### 2.3  其他名词

##### 2.3.1 覆盖索引

如果我们使用索引作为条件查询，查询完毕后，不需要回表查，那么这就是覆盖索引。

```sql
select id from s1 where id = 1000000;
select count(id) from s1 where id >1000000;
```

##### 2.3.2 合并索引

对两个字段分别创建索引，由于sql的条件，使两个索引同时生效了，那么这时候和两个索引就成为了合并索引。

##### 2.3.3 执行计划

如果你想在执行sql之前就知道sql语句的执行情况，那么可以使用执行计划。

```sql
explain sql语句;
```

### 3.数据库的备份

```sql
mysqldump -uroot -p123 数据库名 > 保存的文件路径 --只备份数据库中的表结构和数据
mysqldump -uroot -p123 database 数据库名 > 保存的文件路径 --备份整个数据库


--数据库还原
source 文件路径
```

### 4.事务和锁

```sql
begin;  -- 开启事务
select * from emp where id = 1 for update;  -- 查询id值，for update添加行锁；
update emp set salary=10000 where id = 1; -- 完成更新
commit; -- 提交事务
```

### 5.sql注入和pymysql

示例：

```sql
--用户名和密码到数据库里查询数据
--如果能查到数据 说明用户名和密码正确
--如果查不到，说明用户名和密码不对
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = '%s' and password = '%s'"%(username,password)
print(sql)
-- 注释掉--之后的sql语句
select * from userinfo where name = 'alex' ;-- and password = '792164987034';
select * from userinfo where name = 219879 or 1=1 ;-- and password = 792164987034;
select * from userinfo where name = '219879' or 1=1 ;-- and password = '792164987034';
```

```python
import pymysql

conn = pymysql.connect(host = '127.0.0.1',user = 'root',
                       password = '123',database='day41')
cur = conn.cursor()
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = %s and password = %s"
cur.execute(sql,(username,password))#让pymysql模块帮助我们做sql语句拼接
print(cur.fetchone())
cur.close()
conn.close()
```

