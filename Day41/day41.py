#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第41天作业
王凯旋
'''

# 1、查询男生、女生的人数；
'''
select gender,count(*) from student group by gender;
'''

# 2、查询姓“张”的学生名单；
'''
select * from student where sname like '张%';
'''

# 3、课程平均分从高到低显示
'''
select course_id,avg(num) as n from score group by course_id order by n desc;
'''

# 4、查询有课程成绩小于60分的同学的学号、姓名；
'''
select stu.sid,sname from student as stu inner join score as s on stu.sid = s.student_id where num < 60 group by stu.sid;
'''

# 5、查询至少有一门课与学号为1的同学所学课程相同的同学的学号和姓名；
'''
select stu.sid,sname from student as stu inner join score as sc on stu.sid = sc.student_id where course_id in(
select course_id from student as stu inner join score as sc on stu.sid = sc.student_id where stu.sid = 1) and 
stu.sid != 1 group by stu.sid;
'''

# 6、查询出只选修了一门课程的全部学生的学号和姓名；
'''
select stu.sid,sname from student as stu inner join score as s on stu.sid = student_id group by sid having count(course_id) = 1;
'''

# 7、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
'''
select course_id as '课程ID',max(num) as '最高分',min(num) as '最低分' from score group by course_id;
'''

# 8、查询课程编号“2”的成绩比课程编号“1”课程低的所有同学的学号、姓名；
'''
select p.pid as '学号',p.sname as '姓名' from
(select stu.sid as pid,sname,num from student as stu inner join score as s on stu.sid = student_id where course_id = 1) as p inner join
(select stu.sid as tid,num from student as stu inner join score as s on stu.sid = student_id where course_id = 2) as t on p.pid = t.tid
where p.num > t.num;
'''

# 9、查询“生物”课程比“物理”课程成绩高的所有学生的学号；
'''
select s.student_id from
(select student_id,num from score inner join course on score.course_id = course.cid where cname = '生物') as s inner join
(select student_id,num from score inner join course on score.course_id = course.cid where cname = '物理') as w 
on s.student_id = w.student_id where s.num > w.num;
'''

# 10、查询平均成绩大于60分的同学的学号和平均成绩;
'''
select std.sid,avg(num) from student as std inner join score as s on std.sid = student_id group by std.sid having avg(num) > 60; 
'''

# 11、查询所有同学的学号、姓名、选课数、总成绩；
'''
select std.sid,sname,count(*),sum(num) from student as std inner join score as s on std.sid = student_id group by std.sid;
'''

# 12、查询姓“李”的老师的个数；
'''
select count(*) from teacher where tname like '李%';
'''

# 13、查询没学过“张磊老师”课的同学的学号、姓名；
'''
select std.sid,sname from student as std inner join score on std.sid = student_id where course_id in 
(select cid from teacher as ter inner join course as c on ter.tid = teacher_id where tname = '张磊老师');
'''

# 14、查询学过“1”并且也学过编号“2”课程的同学的学号、姓名；
'''
select sid,sname from student where sid in (
select t1.student_id from (select student_id from score where course_id = 1) as t1 inner join
(select student_id from score where course_id = 2) as t2 on t1.student_id = t2.student_id);
'''

# 15、查询学过“李平老师”所教的所有课的同学的学号、姓名；
'''
select std.sid,sname from student as std inner join score on std.sid = student_id where course_id in(
select cid from teacher as ter inner join course as c on ter.tid = teacher_id where tname = '李平老师') group by std.sid;
'''




# 1、查询没有学全所有课的同学的学号、姓名；
'''
select std.sid,sname from student as std inner join score as s on std.sid = student_id group by std.sid 
having count(*) < (select count(*) from course);

select sid,sname from student where sid in(
select student_id from score group by student_id having count(*)<(select count(*) from course));
'''

# 2、查询和“002”号的同学学习的课程完全相同的其他同学学号和姓名；
'''
select sid,sname from student where sid in(
select student_id from score where student_id != 2 and student_id not in (
select student_id from score where course_id not in (select course_id from score where student_id = 2))
group by student_id having count(*) = (select count(*) from score where student_id = 2));
'''

# 3、删除学习“叶平”老师课的SC表记录；
'''
select cid from teacher as ter inner join course as c on ter.tid = c.teacher_id where tname = '叶平';
delete from score where course_id in ('上条语句的查询结果')
'''

# 4、向SC表中插入一些记录，这些记录要求符合以下条件：①没有上过编号“002”课程的同学学号；②插入“002”号课程的平均成绩；
'''

'''
# 5、按平均成绩从低到高显示所有学生的“语文”、“数学”、“英语”三门的课程成绩，按如下形式显示： 学生ID,语文,数学,英语,有效课程数,有效平均分；
'''
select student_id,cid from score inner join course
select cid from course where cname = '语文';
select cid from course where cname = '数学';
select cid from course where cname = '英语';
'''

# 6、查询各科成绩最高和最低的分：以如下形式显示：课程ID，最高分，最低分；
'''
select course_id as '课程ID',max(num) as '最高分',min(num) as '最低分' from score group by course_id;
'''

# 7、按各科平均成绩从低到高和及格率的百分数从高到低顺序；
'''
select course_id,avg(num) as avg_num from score group by course_id order by avg_num;

select a.course_id,b.cou/a.cou*100 as pre from (select course_id,count(*) as cou from score group by course_id) as a inner join (
select course_id,count(*) as cou from score where num >= 60 group by course_id) as b on a.course_id = b.course_id
order by pre desc;
'''

# 8、查询各科成绩前三名的记录:(不考虑成绩并列情况)
'''

'''

# 9、查询每门课程被选修的学生数；
'''
select course_id,count(*) from score group by course_id;
'''

# 10、查询同名同姓学生名单，并统计同名人数；
'''
select a.cou - b.cou as '同名人数' from (select count(*) as cou from student) as a inner join 
(select count(*) as cou from (select distinct sname from student) as c) as b;
'''

# 11、查询每门课程的平均成绩，结果按平均成绩升序排列，平均成绩相同时，按课程号降序排列；
'''
select course_id,avg(num) as avg_num from score group by course_id order by avg_num,course_id desc;
'''

# 12、查询平均成绩大于85的所有学生的学号、姓名和平均成绩；
'''
select std.sid,std.sname,avg(num) from student as std inner join score as s on std.sid = student_id
group by std.sid having avg(num) > 85;
'''

# 13、查询课程名称为“数学”，且分数低于60的学生姓名和分数；
'''
select std.sid,std.sname,a.snum from student as std inner join 
(select student_id,s.num as snum from course as c inner join score as s on c.cid = s.course_id where c.cname = '数学' and s.num < 60) 
as a on std.sid = a.student_id;
'''

# 14、查询课程编号为003且课程成绩在80分以上的学生的学号和姓名；
'''
select std.sid,std.sname from student as std inner join score as s on std.sid = student_id where s.course_id = 3 and s.num > 80;
'''

# 15、求选了课程的学生人数
'''
select count(*) from (select student_id from score group by student_id) as a;
'''

# 16、查询选修“杨艳”老师所授课程的学生中，成绩最高的学生姓名及其成绩；
'''
select std.sname,s.num from student as std inner join score as s on std.sid = student_id where s.course_id in 
(select cid from course as c inner join teacher as t on c.teacher_id = t.tid where tname = '杨艳') order by s.num desc limit 1;
'''

# 17、查询各个课程及相应的选修人数；
'''
select course_id,count(*) from score group by course_id;
'''

# 18、查询不同课程但成绩相同的学生的学号、课程号、学生成绩；
'''

'''

# 19、查询每门课程成绩最好的前两名；
'''

'''

# 20、检索至少选修两门课程的学生学号；
'''
select student_id from score group by student_id having count(*) >= 2;
'''

# 21、查询全部学生都选修的课程的课程号和课程名；
'''
select c.cid,c.cname from course as c inner join score as s on c.cid = s.course_id 
group by c.cid having count(*) = (select count(*) from student);
'''

# 22、查询没学过“叶平”老师讲授的任一门课程的学生姓名；
'''
select sname from student where sid not in(
select std.sid from student as std inner join score as s on std.sid = student_id where s.course_id in
(select cid from course as c inner join teacher as t on c.teacher_id = t.tid where tname = '叶平'));
'''

# 23、查询两门以上不及格课程的同学的学号及其平均成绩；
'''
select a.student_id,a.avg_num from (select student_id,avg(num) as avg_num from score group by student_id) as a inner join 
(select student_id,count(*) as cou from score where num < 60 group by student_id) as b on a.student_id = b.student_id
where b.cou > 2;
'''

# 24、检索“004”课程分数小于60，按分数降序排列的同学学号；
'''
select student_id from score where course_id = 4 and num < 60 order by num desc;
'''

# 25、删除“002”同学的“001”课程的成绩；
'''
delete from score where student_id = 2 and course_id = 1;
'''

