# Day91

## 今日内容

1. 轮询、长轮询、长连接
2. 使用flask和geventWebsocket实现一个WebSocket

## 内容详细

### 1.轮询、长轮询、长连接

- 轮询：无限循环和server对话，询问是否有消息。不能保证数据的实时性。
- 长轮询：向server发送请求，询问是否有消息，等待十几秒。收到消息返回或时间到了主动断开连接。
- 长连接：建立连接，并保持不断开。可以及时准确的收到消息。

#### 2.使用flask和geventWebsocket实现一个WebSocket

使用websocket建立一个单点聊天

app.py文件

```python
import json
from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler #处理请求  WSGI HTTP
from geventwebsocket.server import WSGIServer #替换Flask原来的WSGI服务
from geventwebsocket.websocket import WebSocket 

app = Flask(__name__)

ws_dic = {}#存放各个websocket的连接


@app.route('/sws/<username>')
def my_ws(username): #建立websocket的请求
    ws = request.environ.get('wsgi.websocket')  # type:WebSocket  指定ws类型 使ws有语法提示
    ws_dic[username] = ws  #将连接添加到字典
    print(ws_dic)
    while True:
        msg = ws.receive()
        receiver = json.loads(msg).get('receiver') #获取要发送给的那个人的连接
        print(receiver)
        ws_socket = ws_dic.get(receiver)
        ws_socket.send(msg)#发送数据


@app.route('/schat')
def wechat():
    return render_template('simple_chat.html')


if __name__ == '__main__':
    ws_server = WSGIServer(('0.0.0.0', 9527), app, handler_class=WebSocketHandler) #指定监听的ip和端口;处理业务的app;以及处理请求的handler,WebSocketHandler可以处理http也可以处理websocket
    ws_server.serve_forever() 
```

html文件

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<div>
    <input type="text" id="username">
    <button onclick="login()">登录</button>
</div>
<div>
    发送至<input type="text" id="receiver">
    内容<input type="text" id="msg">
    <button onclick="sendmsg()">发送</button>
</div>
<div id="content" style="width: 500px;"></div>
</body>
<script>
    ws = null;
    div = document.getElementById('content');

    function login() {
        username = document.getElementById('username').value;
        ws = new WebSocket('ws://192.168.12.39:9527/sws/' + username); //创建websocket连接
        ws.onmessage = function (info) { //绑定接收数据事件
            console.log(info.data);
            dic = JSON.parse(info.data);
            sender = dic['sender'];
            msg = dic['msg'];
            p = document.createElement('p');
            p.innerText = sender +'：'+ msg;
            div.appendChild(p);
        }
    }

    function sendmsg() {
        username = document.getElementById('username').value;
        recevier = document.getElementById('receiver').value;
        msg = document.getElementById('msg').value;
        sendstr = {
            'receiver': recevier,
            'sender': username,
            'msg': msg
        }; //封装发送者，接受者，发送数据
        ws.send(JSON.stringify(sendstr));//发送数据
        p = document.createElement('p');
        p.innerText = msg + '：我';
        p.style.cssText = 'text-align: right';
        div.appendChild(p);

    }


</script>
</html>
```

