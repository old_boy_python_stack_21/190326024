#!/usr/bin/env python
# -*- coding:utf-8 -*-
import json
from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket

app = Flask(__name__)

ws_dic = {}


@app.route('/sws/<username>')
def my_ws(username):
    ws = request.environ.get('wsgi.websocket')  # type:WebSocket
    ws_dic[username] = ws
    print(ws_dic)
    while True:
        msg = ws.receive()
        receiver = json.loads(msg).get('receiver')
        print(receiver)
        ws_socket = ws_dic.get(receiver)
        ws_socket.send(msg)


@app.route('/schat')
def wechat():
    return render_template('simple_chat.html')


if __name__ == '__main__':
    ws_server = WSGIServer(('0.0.0.0', 9527), app, handler_class=WebSocketHandler)
    ws_server.serve_forever()
