#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask, request,render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket

app = Flask(__name__)

ws_list = []


@app.route('/ws')
def my_ws():
    ws = request.environ.get('wsgi.websocket')  # type:WebSocket
    ws_list.append(ws)
    while True:
        msg = ws.receive()
        for i in ws_list:
            try:
                if i == ws:
                    continue
                i.send(msg)
            except:
                continue

@app.route('/wechat')
def wechat():
    return render_template('wechat.html')

if __name__ == '__main__':
    ws_server = WSGIServer(('0.0.0.0', 9527), app, handler_class=WebSocketHandler)
    ws_server.serve_forever()
