# Day54

## 今日内容

1. ORM操作
2. 模板语法

## 内容详细

### 1.ORM操作

#### 1.1 表约束

```python
class User(models.Model):
    uid = models.AutoField(primary_key=True) #自增主键
    name = models.CharField(max_length=32,unique=True) #唯一
    gender = models.CharField(max_length=2,default='男') #默认值
```

#### 1.2 添加数据

```python
models.User.objects.create(name='alex',gender='女')
```

#### 1.3 删除数据

```python
obj_list = models.User.objects.filter(gender='男') 
obj_list.delete()#删除一条或多条数据

obj = models.User.objects.get(pk=1) #pk代表主键字段
obj.delete()#删除一条数据
```

#### 1.4 修改数据

```python
obj_list = models.User.objects.filter(uid=1)
obj_list[0].name = 'wusir'
obj_list.save()
```

### 2.模板语法

#### 2.1 view函数向模板发送数据

view函数：

```python
from django.shortcuts import render
from app01 import models

def user_list(request):
    obj_list = models.User.objects.all()
    return render(request,'user_list.html',{'obj_list':obj_list})#模板根据键取数据
```

#### 2.2 模板语法

- {{    }}  内部为后台的变量
- {% %}  写一些逻辑相关的代码

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h2>用户列表</h2>
<table border="1px" cellspacing="0">
    <tr>
        <th>序号</th>
        <th>ID</th>
        <th>姓名</th>
        <th>性别</th>
    </tr>
    {% for user in obj_list %} #for循环
        <tr>
            <td>{{ forloop.counter }}</td>  #for循环内部对象forloop,counter属性表示循环的次数
            <td>{{ user.pk }}</td> #pk代表主键
            <td>{{ user.name }}</td>
            <td>{{ user.gender }}</td>
        </tr>
    {% endfor %} #结束for循环
</table>

</body>
</html>
```

