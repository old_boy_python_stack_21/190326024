from django.db import models

class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

