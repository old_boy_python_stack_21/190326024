from django.shortcuts import render,redirect
from appPublisher import models

def publisher_list(request):
    publishers_list = models.Publisher.objects.all().order_by('-pid')
    return render(request,'publisher_list.html',{'publishers_list':publishers_list})

def del_publisher(request):
    pid = request.GET.get('pid')
    obj_list = models.Publisher.objects.filter(pid=pid)
    obj_list.delete()
    return redirect('/publisher_list/')

def add_publisher(request):
    error = ''
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        publisher_tel = request.POST.get('publisher_tel')
        publisher_addr = request.POST.get('publisher_addr')

        if models.Publisher.objects.filter(name=publisher_name):
            error = '出版社名称已存在'
        if not publisher_name:
            error = '出版社名称不能为空'
        if not error:
            models.Publisher.objects.create(name=publisher_name,phone=publisher_tel,addr=publisher_addr)
            return redirect('/publisher_list/')
    return render(request,'add_publisher.html',{'error':error})

def edit_publisher(request):
    error = ''
    pid = request.GET.get('pid')
    obj = models.Publisher.objects.get(pid=pid)
    if request.method == 'POST':
        publisher_name = request.POST.get('publisher_name')
        publisher_tel = request.POST.get('publisher_tel')
        publisher_addr = request.POST.get('publisher_addr')

        if models.Publisher.objects.filter(name=publisher_name):
            error = '出版社名称已存在'
        if obj.name == publisher_name:
            error = '出版社名称与之前相同'
        if not publisher_name:
            error = '出版社名称不能为空'
        if not error:
            obj.name = publisher_name
            obj.phone = publisher_tel
            obj.addr = publisher_addr
            obj.save()
            return redirect('/publisher_list/')
    return render(request,'edit_publisher.html',{'error':error,'publisher':obj})