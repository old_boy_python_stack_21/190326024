# Day17

## 今日内容

1. 迭代器
2. 生成器

## 补充

- 模块导入

  ```python
  #导入的是包下面的__init__.py文件
  import 包
  from 包 import * 
  ```

## 内容详细

### 1.迭代器

1. 自己不会写迭代器，只会用。

2. 迭代器：对可迭代对象（序列）中的元素进行逐一获取。

3. 列表转换成迭代器

   ```python
   lst = [11,22,33,44]
   v1= iter(lst)
   v2 = lst.__iter__()
   print(v2.__next__())
   print(v2.__next__())
   print(v2.__next__())
   print(v2.__next__()) #反复调用__next__()方法来获取每一个值，直到报错：StopIteration
   ```

4. 判断一个对象是否是迭代器：内部是否有`__next__()`方法。

   ```python
   lst = [11,22,33,44]
   for i in lst:   #1.内部将lst转换成迭代器，内部反复执行迭代器的__next__()方法，直到报错结束
       print(i) 
   ```

5. 可迭代对象
   - 具有`__iter__()`方法，且`__iter__()` 方法返回一个迭代器或生成器
   - 或者可以被for循环

### 2.生成器（函数的变异）

```python
#函数
def func1():
    return 123
func()

#生成器函数（内部是否包含yield）
def func2():
	yield 1
    yield 2
    yield 3
    
v1 = func2()  #函数内部代码不会执行，返回一个生成器对象

#生成器可以被for循环，一旦开始循环，函数内部代码就会开始执行
for i in v1:
    print(i) #遇到yield就停止返回yield后的值
    		 #下一次继续从上次结束的位置开始，遇到yield就停止返回
        	 #直到生成器函数执行完毕终止循环
```

总结：

- 函数中如果存在yield，那么该函数就是一个生成器函数
- 执行生成器函数会返回一个生成器，只有for循环生成器的时候生成器函数内部的代码才会执行
- 每次循环都会获取yield返回的值，直到函数执行完成

```python
CURSOR = 0
FLAG = False

def func():
    global CURSOR
    global FLAG
    while True:
        f = open('goods.txt',mode='r',encoding='utf-8')
        f.seek(CURSOR)
        content_list = []
        for i in range(2):
            line = f.readline().strip()
            if not line:
                FLAG = True
            content_list.append(line)
        CURSOR = f.tell()
        f.close()

        for item in content_list:
            yield item
        if FLAG:
            return

v = func()
for item in v:
    print(item)
```

### 3.其他

1. yield from关键字（欠） py3.3后才有
2. 生成器推导式（欠）

## 总结

1. 迭代器：对可迭代对象中的元素进行逐一获取。迭代器对象内部都有一个`__next()__` 方法，用于逐个获取数据。
2. 可迭代对象：可以被for循环，此类对象内部都有`__iter()__` 方法，且返回一个迭代器（生成器）
3. 生成器：函数内部有yield就是生成器函数，调用该函数返回生成器，for循环生成器函数内部代码才会执行
   - 生成器用来生成数据，也可以迭代(被for循环)，并且内部也有`__next__()` 方法
   - 生成器是一种特殊的迭代器，和可迭代对象

