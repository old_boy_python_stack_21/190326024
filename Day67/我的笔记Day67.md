# Day67

### 1.展示客户信息

#### 1.1 普通字段

对象.字段名

#### 1.2 有choice参数

- 对象.字段名    --》得到数据库中的数据

- `对象.get_字段名_display()`     --》得到数据库对应的中文提示

  ```python
  #Custome的class_type字段
  class_type_choices = (('fulltime', '脱产班',),
                        ('online', '网络班'),
                        ('weekend', '周末班',),)
  ```

  ```html
  <td>{{ customer.get_class_type_display }}</td>
  ```

#### 1.3 外键

- 对象.外键  ——》  外键对象   定义`__str__`

- 对象.外键.字段

#### 1.4 自定义方法

多对多：

```python
def show_class(self):
    return ' '.join([str(i) for i in self.class_list.all()])
```

自定义需求：

```python
    def get_status(self):
        dic = {
            'signed': "blue",
            'unregistered': "gray",
            'studying': 'green',
            'paid_in_full': "red"
        }
        return '<span style="color:white; background-color:{};padding: 5px; border-radius: 8%">{}</span>'.format(
            dic.get(self.status), self.get_status_display())
```

### 2.分页

分页功能封装成类

```python
import math

class Pagination:
    def __init__(self,page,total_count,show_page=11,pre_count=10):
        '''

        :param page: 当前页码
        :param total_count:数据总量
        :param show_page: 展示的页码个数
        :param pre_count: 每页展示的数据个数
        '''
        try:
            self.page = int(page)
            if self.page <= 0:
                self.page = 1
        except Exception:
            self.page = 1

        total_page = math.ceil(total_count / pre_count)
        half_page = show_page // 2
        if total_page <= show_page:
            start_page = 1
            end_page = total_page
        else:
            if self.page <= half_page:
                start_page = 1
                end_page = show_page
            elif self.page + half_page > total_page:
                start_page = total_page - show_page + 1
                end_page = total_page
            else:
                start_page = self.page - half_page
                end_page = self.page + half_page
        self.total_page = total_page
        self.start_page = start_page
        self.end_page = end_page
        self.start = (self.page - 1) * 10
        self.end = self.page * 10

    @property
    def page_html(self):
        page_list = []
        if self.page == 1:
            page_list.append(
                '<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>')
        else:
            page_list.append(
                '<li><a href="?page={}" aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>'.format(
                    self.page - 1))
        for i in range(self.start_page, self.end_page + 1):
            if i == self.page:
                page_list.append('<li class="active"><a href="?page={}">{}</a></li>'.format(i, i))
            else:
                page_list.append('<li><a href="?page={}">{}</a></li>'.format(i, i))
        if self.page == self.total_page:
            page_list.append(
                '<li class="disabled"><a aria-label="Next"><span aria-hidden="true">下一页</span></a></li>')
        else:
            page_list.append(
                '<li><a href="?page={}" aria-label="Next"><span aria-hidden="true">下一页</span></a></li>'.format(
                    self.page + 1))
        return page_list
```

### 3.git操作

```python
#查看历史版本
git log
git reflog

#版本回退
git reset --hard 版本号

#克隆远程仓库
git clone https://gitee.com/old_boy_python_stack_21/teaching_plan.git  克隆远程仓库

#拉取远程仓库代码到本地仓库
git pull origin master
```

### 4.django后台管理（admin）

1. 创建超级用户

   ```
   python manage.py createsuperuser
   ```

2. 在app下的admin.py文件中注册要使用的model

   ```python
   from django.contrib import admin
   from crm import models
   
   admin.site.register(models.Customer)
   admin.site.register(models.Department)
   admin.site.register(models.Campus)
   admin.site.register(models.ClassList)
   ```

   

