﻿#!/usr/bin/env python
# -*- coding:utf-8 -*-


'''
第十五天作业
王凯旋
'''
# 1.sys.path.append("/root/mods")的作用？
'''
添加一个python导入模块时的寻找路径
'''

# 2.字符串如何进行反转？
'''
v = '12345'
print(v[::-1])
'''

# 3.不用中间变量交换a和b的值
'''
a = 1
b = 2
a,b = b,a
'''

# 4.*args和**kwargs这俩参数是什么意思？我们为什么要用它。
'''
*args:接收任意个位置参数
**kwargs:接收任意个关键字参数
作用：可以接收任意个，任意类型的参数。让函数通用
'''

# 5.函数的参数传递是地址还是新值？
'''
地址
'''

# 6.看代码写结果
'''
my_dict = {'a':0,'b':1}
def func(d):
    d['a'] = 1
    return d

func(my_dict)
my_dict['c'] = 2
print(my_dict)
'''

'''
结果：{'a':1,'b':1,'c':2}
'''

# 7.什么是lambda表达式
'''
匿名函数，用来表示一些简单的函数
'''

# 8.range和xrang有什么不同？
'''
range:在内存中一次生成所有元素
xrange:生成一个生成器，循环一次生成一个数
'''

# 9."1,2,3" 如何变成 ['1','2','3',]
'''
v = '1,2,3'
print(v.split(','))
'''

# 10.['1','2','3'] 如何变成 [1,2,3]
'''
lst = ['1','2','3']
print(list(map(lambda x:int(x),lst)))
'''

# 11.def f(a,b=[]) 这种写法有什么陷阱？
'''
b形参的空列表在函数加载的时候就在内存中生成了，后面所有用默认的函数调用，都操作的是同一个列表。所以在多次执行后，结果可能
和预期不同
'''

# 12.如何生成列表 [1,4,9,16,25,36,49,64,81,100] ，尽量用一行实现。
'''
v = [i * i for i in range(1, 11)]
print(v)
'''

# 13.python一行print出1~100偶数的列表, (列表推导式, filter均可)
'''
print(list(filter(lambda i: i % 2 == 0, list(range(1, 101)))))
'''

# 14.把下面函数改成lambda表达式形式
'''


def func():
    result = []
    for i in range(10):
        if i % 3 == 0:
            result.append(i)
    return result
'''

'''
v = lambda: [i for i in range(10) if i % 3 == 0]
'''

# 15.如何用Python删除一个文件？
'''
import os
os.remove('data.txt')
'''

# 16.如何对一个文件进行重命名？
'''
import os
os.rename('test2.py','test1.py')
'''

# 17.python如何生成随机数？
'''
import random
print(random.randint(1,4))
'''

# 18.从0-99这100个数中随机取出10个数，要求不能重复，可以自己设计数据结构。
'''
import random
def get_num():
    value = set()
    while True:
        value.add(random.randint(0,99))
        if len(value) >= 10:
            break
    return value

print(get_num())
'''

# 19.用Python实现 9*9 乘法表 （两种方式）
# 第一种方式
'''
for i in range(1, 10):
    for j in range(1, i + 1):
        print('%s*%s' % (i, j,), end=' ')
    print()
'''

'''
[print('%s*%s' % (i, j,), end=' ') if j < i else print('%s*%s' % (i, j,)) for i in range(1, 10) for j in
 range(1, i + 1)]
'''

# 20.请给出下面代码片段的输出并阐述涉及的python相关机制
'''


def dict_updater(k, v, dic={}):
    dic[k] = v
    print(dic)


dict_updater('one', 1) #使用的是在函数加载时创建的dic默认值地址
dict_updater('two', 2) #和上面使用的都是在函数加载时创建的dic默认值地址 所以结果为{'one':1,'two':2}
dict_updater('three', 3, {}) #在执行函数时为dic形参传入了一个新的地址，所以结果为{'three':3}
'''

'''
结果：{'one':1}
     {'one':1,'two':2}
     {'three':3}
'''

# 21.写一个装饰器出来。
'''


def func(arg):
    def inner(*args, **kwargs):
        data = arg(*args, **kwargs)
        return data

    return inner
'''

# 22.用装饰器给一个方法增加打印的功能。
'''


def func(arg):
    def inner(*args, **kwargs):
        print("before")
        data = arg(*args, **kwargs)
        print("after")
        return data

    return inner

@func
def bar():
    return 123

bar()
'''

# 23.as 请写出log实现(主要功能时打印函数名)
'''
@log  
	def now():  
	    print "2013-12-25"  
	  
	now()  

    # 输出  
	# call now()  
	# 2013-12-25
'''

'''


def log(arg):
    def inner(*args, **kwargs):
        print('call %s()' % arg.__name__)
        return arg(*args, **kwargs)

    return inner


@log
def now():
    print("2013-12-25")


now()
'''

# 24.向指定地址发送请求，将获取到的值写入到文件中。
'''
import requests
import json

response = requests.get('https://www.luffycity.com/api/v1/course_sub/category/list/')
obj = json.loads(response.text)
lst = []
for i in obj.get('data'):
    lst.append(i.get('name'))
with open('catelog.txt', mode='w', encoding='utf-8') as f:
    f.write(','.join(lst))
    f.flush()
'''


#25.请列举经常访问的技术网站和博客
'''
w3c school
csdn
'''
#26.请列举最近在关注的技术
'''
oracle数据库,微信小程序
'''
#27.请列举你认为不错的技术书籍和最近在看的书（不限于技术）
'''
数据结构与算法
'''