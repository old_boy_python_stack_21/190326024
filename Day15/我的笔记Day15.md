# Day15

## 今日内容

1. 模块相关知识
2. 内置模块

## 补充

1. 代码编写建议
   - 如果有if...else...先处理简单业务
   - 尽量减少层级
2. py2和py3区别
   - py2
     - range()  立即在内存中生成所有元素
     - xrange()  得到一个生成器，在循环的时候边循环边创建，节省内存
   - py3
     - range() 得到一个生成器，在循环的时候边循环边创建，节省内存

## 内容详细

### 1.模块基本知识

- 内置模块

  python内部提供的功能

- 第三方模块

  需要下载、安装、使用

  pip install 要安装的模块名称

- 自定义模块

### 2.内置模块

#### 2.1 os模块

- os.mkdir()  在当前工作路径下创建目录，只有一层

  ```python
  import os
  os.mkdir('aaa')
  ```

- os.makedirs()  在当前工作目录下创建目录及子目录，如果存在会报错

  ```python
  import os
  os.makedirs(r'dd\bb\cc')
  ```

- os.rename() 重命名

  ```python
  import os
  os.rename('test1.py','test2.py')
  os.rename('aaa','bbb')
  ```

#### 2.2 sys模块

- sys.path 默认python在导入模块时会按照sys.path中的路径逐个查找。可以通过sys.path.append()添加一个路径

  ```python
  for i in sys.path:
      print(i)
  #E:\code\day10
  #C:\Python36\python36.zip
  #C:\Python36\DLLs
  #C:\Python36\lib
  #C:\Python36
  #C:\Python36\lib\site-packages
  ```

- sys.exit() 退出程序

#### 2.3  json模块

##### 2.3.1基本知识

- json是一个特殊的字符串。

- 序列化：对象转换成json。

- 反序列化：json转换成对象
- json格式要求
  - 只包含int,str,list,dict,bool,tuple(会转换成列表)
  - 最外层必须是列表或字典
  - 如果包含字符串必须是双引号

##### 2.3.2 json.dumps()  序列化

```python
import json

lst = [1,2,3,{'k1':'v1'},(1,2,3),'alex',True]
print(json.dumps(lst))#[1, 2, 3, {"k1": "v1"}, [1, 2, 3], "alex", true]
```

##### 2.3.3 json.load() 反序列化

  ```python
import json

val = '["alex",{"k2":2,"k1":44},true]'
print(json.loads(val))#['alex', {'k2': 2, 'k1': 44}, True]
  ```

