#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket
import gevent
from gevent import monkey
monkey.patch_all()

def func():
    sk = socket.socket()
    sk.connect(('127.0.0.1',8066))
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)

g_list = []
for i in range(10):
    g = gevent.spawn(func)
    g_list.append(g)
gevent.joinall(g_list)