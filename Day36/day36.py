#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第36天作业
王凯旋
'''

# 线程部分
# 1.程序从flag a执行到falg b的时间大致是多少秒？
'''
import threading
import time
def _wait():
	time.sleep(60)
# flag a
t = threading.Thread(target=_wait,daemon = False)
t.start()
# flag b
'''
'''
非常短的时间
'''

# 2.程序从flag a执行到falg b的时间大致是多少秒？
'''
import threading
import time
def _wait():
	time.sleep(60)
# flag a
t = threading.Thread(target=_wait,daemon = True)
t.start()
# flag b
'''
'''
非常短的时间
'''

# 3.程序从flag a执行到falg b的时间大致是多少秒？
'''
import threading
import time
def _wait():
	time.sleep(60)
# flag a
t = threading.Thread(target=_wait,daemon = True)
t.start()
t.join()
# flag b
'''
'''
60s
'''

# 4.读程序，请确认执行到最后number是否一定为0
'''
import threading
loop = int(1E7)
def _add(loop:int = 1):
	global number
	for _ in range(loop):
		number += 1
def _sub(loop:int = 1):
	global number
	for _ in range(loop):
		number -= 1
number = 0
ta = threading.Thread(target=_add,args=(loop,))
ts = threading.Thread(target=_sub,args=(loop,))
ta.start()
ts.start()
ta.join()
ts.join()
'''
'''
不一定
'''

# 5.读程序，请确认执行到最后number是否一定为0
'''
import threading
loop = int(1E7)
def _add(loop:int = 1):
	global number
	for _ in range(loop):
		number += 1
def _sub(loop:int = 1):
	global number
	for _ in range(loop):
		number -= 1
number = 0
ta = threading.Thread(target=_add,args=(loop,))
ts = threading.Thread(target=_sub,args=(loop,))
ta.start()
ta.join()
ts.start()
ts.join()
'''
'''
一定为0
'''

# 6.读程序，请确认执行到最后number的长度是否一定为1
'''
import threading
loop = int(1E7)
def _add(loop:int = 1):
	global numbers
	for _ in range(loop):
		numbers.append(0)
def _sub(loop:int = 1):
	global number
	while not numbers:
		time.sleep(1E-8)
	numbers.pop()
number = [0]
ta = threading.Thread(target=_add,args=(loop,))
ts = threading.Thread(target=_sub,args=(loop,))
ta.start()
ta.join()
ts.start()
ts.join()
'''
'''
是
'''

# 7.读程序，请确认执行到最后number的长度是否一定为1
'''
import threading
loop = int(1E7)
def _add(loop:int = 1):
	global numbers
	for _ in range(loop):
		numbers.append(0)
def _sub(loop:int = 1):
	global number
	while not numbers:
		time.sleep(1E-8)
	numbers.pop()
number = [0]
ta = threading.Thread(target=_add,args=(loop,))
ts = threading.Thread(target=_sub,args=(loop,))
ta.start()
ts.start()
ta.join()
ts.join()
'''
'''
是
'''

# 协程
# 1、什么是协程？常用的协程模块有哪些？
'''
能够在一个线程下的多个任务下来回切换，那么每一个任务都是一个协程。
gevent,asyncio
'''

# 2、协程中的join是用来做什么用的？它是如何发挥作用的？
'''
阻塞，直到协程任务执行完毕。当遇到阻塞时会在多个任务间切换，而join阻塞到协程任务执行完毕，所以cpu只能执行协程任务
'''

# 3、使用协程实现并发的tcp server端


# 4、在一个列表中有多个url，请使用协程访问所有url，将对应的网页内容写入文件保存
'''
import gevent
import requests
from gevent import monkey
monkey.patch_all()

url_list = [
    'http://www.baidu.com',
    'http://tool.chinaz.com/'
]

def func(url,file_name):
    ret = requests.get(url)
    with open(file_name,mode='w',encoding='utf-8') as f:
        f.write(ret.text)
        f.flush()

g_list = []
for i in range(len(url_list)):
    g = gevent.spawn(func,url_list[i],'%s.txt'%(i,))
    g_list.append(g)

gevent.joinall(g_list)
'''


#综合
# 1、进程和线程的区别
'''
进程开销大，可以利用多核，数据隔离，是最小的资源分配单位
线程开销小，在Cpython解释器下不能利用多核，数据共享，是最小的cpu调度单位
'''


# 2、进程池、线程池的优势和特点
'''
进程池开销较大，但可以实现并行
线程池开销较小，但不能实现并行
'''
# 3、线程和协程的异同?
'''
线程和协程都不能实现并行，都是在轮转使用同一个cpu
但是线程的切换是由操作系统来执行的，对操作系统压力大，但对io操作的感知更灵敏
协程的切换是由用户来执行的，不会增加操作系统的压力，但是对io操作不如线程灵敏
'''


# 4、请简述一下互斥锁和递归锁的异同？
'''
相同点：都可以解决线程的数据安全问题
不同点：
互斥锁：在同一个线程中，同一把互斥锁不能连续acquire多次，开销较小，效率较高
递归锁：在同一个线程中，同一把递归锁可以连续acquire多次，但也需要release同样的次数，开销较大，效率较慢
'''


# 5、请列举一个python中数据安全的数据类型？
'''
元组
'''


# 6、Python中如何使用线程池和进程池
'''
#线程池
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print(i)

pool = ThreadPoolExecutor(20)
for i in range(10):
    pool.submit(func,i)
'''
'''
#进程池
from concurrent.futures import ProcessPoolExecutor
def func(i):
    print(i)
if __name__ == '__main__':
    pool = ProcessPoolExecutor(5)
    for i in range(10):
        pool.submit(func,i)
'''


# 7、简述 进程、线程、协程的区别 以及应用场景？
'''
进程：开销大，数据隔离，可以实现并行。用于高计算的场景
线程：开销小，数据共享，不能实现并行，切换由操作系统完成，对操作系统压力大，但对io操作更灵敏。用于多io操作的场景
协程：开销最小，不能实现并行，由用户完成任务切换，对操作系统无压力，对io操作不如线程灵敏。用于多io操作场景
'''


# 8、什么是并行，什么是并发？
'''
并行：多个任务分别在不同的cpu上执行
并发：多个任务在同一个cpu上轮流执行
'''
