# Day36

## 今日内容

1. 协程的概念
2. 模块操作协程
   - gevent模块
   - asyncio模块

## 内容详细

### 1.协程的概念

1. 用户级别的，由我们自己写的python代码来控制切换，并且是操作系统不可见的。
2. 在Cpython解释器下，由于多线程本身不能利用多核，所以即便开启了多线程，也只能轮流在一个CPU上行。协程如果把所有任务的IO操作都规避掉，只剩下要使用CPU的操作，就意味着协程就可以做到提高CPU利用率的效果。
3. 多线程和协程都不能利用多核，只能在一个cpu上来回切换。
4. 线程和协程对比：
   - 线程：切换需要操作系统完成，开销大，操作系统控制切换的过程不可控，给操作系统的压力大。但操作系统对IO操作感知更加敏感。
   - 协程：切换由python代码完成，开销小，用户控制切换过程可控，完全不会增加操作系统的压力。但用户级别对IO操作的感知灵敏度较低。

## 2.切换问题

1. 协程：能够在一个线程下的多个任务之间来回切换，那么每个任务都是一个协程。(只有在一个协程遇到io操作时，才会切换到另一个协程)

2. 两种切换方式

   - 原生python完成

     - 使用yield
     - asyncio模块（内置模块，原生靠近底层，使用不方便）

   - C语言完成的python模块

     - greenlet模块（只能完成任务切换，不能解决IO操作问题）

       ```python
       import time
       from greenlet import greenlet
       
       def eat():
           print('start eat')
           time.sleep(0.5)
           g2.switch() #切换到sleep任务
           print('end eat')
       
       def sleep():
           print('start sleep')
           time.sleep(0.5) #并不能解决io阻塞
           print('end sleep')
           g1.switch() # 若想eat继续执行，还要切回去
       
       g1 = greenlet(eat)
       g2 = greenlet(sleep)
       g1.switch() #相当于一个开关
       ```

     - gevent模块（第三方模块，使用方便）

### 3.gevent模块

```python
import time
import gevent
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def eat():
    print('start eat')
    time.sleep(0.5)
    print('end eat')

def sleep():
    print('start sleep')
    time.sleep(1)
    print('end sleep')

g1 = gevent.spawn(eat) #创建一个协程任务
g2 = gevent.spawn(sleep)
g1.join() # 阻塞直到g1协程执行完成，若无阻塞则协程任务g1和g2不会执行(有阻塞才会任务切换)
g2.join()
```

带参数的协程

```python
import time
import gevent
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def eat(name):
    print('%s start eat'%(name,))
    time.sleep(0.5)
    print('%s end eat'%(name,))

def sleep(name):
    print('%s start sleep'%(name,))
    time.sleep(1)
    print('%s end sleep'%(name,))

g1 = gevent.spawn(eat,'wusir') #创建一个协程任务
g2 = gevent.spawn(sleep,'alex')
gevent.joinall([g1,g2]) #阻塞直到两个协程任务都执行完成
```

带返回值的协程

```python
import time
import gevent
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def eat(name):
    print('%s start eat'%(name,))
    time.sleep(0.5)
    print('%s end eat'%(name,))
    return name

def sleep(name):
    print('%s start sleep'%(name,))
    time.sleep(1)
    print('%s end sleep'%(name,))
    return name

g1 = gevent.spawn(eat,'wusir') #创建一个协程任务
g2 = gevent.spawn(sleep,'alex')
gevent.joinall([g1,g2]) #阻塞直到两个协程任务都执行完成
print(g1.value,g2.value) #wusir alex
```

协程实现并发socket

```python
#服务端
import gevent
import socket
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def func(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        conn.send(msg.upper().encode('utf-8'))

sk = socket.socket()
sk.bind(('127.0.0.1',8066))
sk.listen()

while True:
    conn,addr = sk.accept()
    gt = gevent.spawn(func,conn)

#多线程客户端
import socket,time
from threading import Thread

def func():
    sk = socket.socket()
    sk.connect(('127.0.0.1',8066))
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)


for i in range(10):
    Thread(target=func).start()
 
#协程客户端
import socket,time
import gevent
from gevent import monkey
monkey.patch_all()

def func():
    sk = socket.socket()
    sk.connect(('127.0.0.1',8066))
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)

g_list = []
for i in range(10):
    g = gevent.spawn(func)
    g_list.append(g)
gevent.joinall(g_list)
```

### 4.asyncio模块

创建一个任务

```python
import asyncio

async def func(): #定义了一个协程函数
    print('start func')
    await asyncio.sleep(0.5)  #await关键字必须放在协程函数中 阻塞
    print('end func')

loop = asyncio.get_event_loop() #创建一个事件循环
loop.run_until_complete(func()) #把func函数放入事件循环中执行(函数需要带括号) 阻塞
```

创建多个任务

```python
import asyncio

async def func(): #定义了一个协程函数
    print('start func')
    await asyncio.sleep(0.5)  #await关键字必须放在协程函数中 阻塞
    print('end func')

loop = asyncio.get_event_loop() #创建一个事件循环
obj = asyncio.wait([func(),func(),func()])#创建一个对象，内部封装几个任务
loop.run_until_complete(obj) #把对象放入事件循环中执行 阻塞
```

启动多个任务且有返回值

```python
import asyncio

async def func(): #定义了一个协程函数
    print('start func')
    await asyncio.sleep(0.5)  #await关键字必须放在协程函数中 阻塞
    print('end func')
    return 123

loop = asyncio.get_event_loop() #创建一个事件循环
t1 = loop.create_task(func()) #创建任务
t2 = loop.create_task(func())
task_list = [t1,t2]
obj = asyncio.wait(task_list)
loop.run_until_complete(obj) #把对象放入事件循环中执行 阻塞
for i in task_list:
    print(i.result()) #会阻塞

```

先执行完的先去结果

```python
import asyncio
async def demo(i):   # 协程方法
    print('start')
    await asyncio.sleep(10-i)  # 阻塞
    print('end')
    return i,123

async def main():
    task_l = []
    for i in range(10):
        task = asyncio.ensure_future(demo(i))
        task_l.append(task)
    for ret in asyncio.as_completed(task_l):
        res = await ret
        print(res)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
```

- await 阻塞 协程函数这里要切换出去，还能保证一会儿再切回来
- await 必须写在async函数里，async函数是协程函数
- loop 事件循环,所有的协程的执行 调度 都离不开这个loop