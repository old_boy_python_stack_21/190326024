#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import shutil
import hashlib
import socketserver
from lib.encrypt import encrypt
from config import settings
from lib.file_oper import read_file,get_res_disk_size,is_has_file,get_size
from src.tcpbase import TcpBase
from lib.jsonhelper import json_from_file,json_to_file

class Tcpsvr(TcpBase):

    def __init__(self):
        self.conn = None
        self.user_name = ''
        self.msg = None
        self.total_size = 200,
        self.res_size = 0

    def base(arg):
        def inner(self):
            if self.user_name:
                self.base_send({'method':self.msg['method'],'code':1,'info':''})
                arg(self)
            else:
                self.base_send({'method':self.msg['method'],'code':0,'info':'请先进行登录'})
        return inner

    def create_dir(self):
        '''
        创建文件夹
        :return:
        '''
        create_path = self.msg['create_path'].strip()
        dir_name = self.msg['dir_name'].strip()
        path = os.path.join(settings.BASE_PATH, 'filedb', create_path, dir_name)
        os.makedirs(path)

    def to_up(self):
        '''
        返回上一层
        :return:
        '''
        pass

    def del_dir_file(self):
        '''
        删除文件或文件夹
        :return:
        '''
        del_path = self.msg['del_path']
        del_path = os.path.join(settings.BASE_PATH, 'filedb', del_path)
        size = get_size(del_path)
        res_size = get_res_disk_size(self.user_name,self.total_size)
        if os.path.isfile(del_path):
            os.remove(del_path)
        else:
            shutil.rmtree(del_path)
        res_size = res_size + size
        self.res_size = int(res_size/1024/1024)

    def to_down(self):
        '''
        进入下一层目录
        :return:
        '''
        pass

    def login(self):
        '''
        登录
        :return:
        '''
        dic = self.msg
        pwd = encrypt(dic['user_pwd'])
        user_dic = json_from_file(settings.USER_INFO_PATH)
        if not user_dic.get(dic['user_name']):
            self.base_send({'method': 'login', 'code': 0, 'info': '用户名不存在，请重新输入'})
        else:
            svr_dic = user_dic.get(dic['user_name'])
            if dic['user_name'] == svr_dic['user_name'] and pwd == svr_dic['user_pwd']:

                self.total_size = svr_dic['total_disk'] #获取硬盘总大小
                self.res_size = svr_dic['res_disk'] #获取硬盘剩余大小
                self.base_send({'method': 'login', 'code': 1,'total_disk':self.total_size,
                                'res_disk': self.res_size, 'info': '登录成功'})
                self.user_name = dic['user_name']
            else:
                self.base_send({'method': 'login', 'code': 0,'info': '密码错误，请重新输入'})

    def register(self):
        '''
        注册
        :return:
        '''
        user_dic = json_from_file(settings.USER_INFO_PATH)

        if user_dic.get(self.msg['user_name']):
            self.base_send({'method':'register','code':0,'info':'用户名已存在，请重新输入'})

        else:
            user_dic[self.msg['user_name']] = {'user_name':self.msg['user_name'],
                                               'user_pwd':encrypt(self.msg['user_pwd']),
                                               'total_disk':200,
                                               'res_disk':200
                                               }
            json_to_file(user_dic,settings.USER_INFO_PATH)
            os.makedirs(os.path.join(settings.BASE_PATH,'filedb',self.msg['user_name'])) #创建用户根目录
            self.base_send({'method':'register','code':1,'info':'注册成功'})

    def flie_list(self):
        '''
        获取文件列表
        :return:
        '''
        path = os.path.join(settings.BASE_PATH,'filedb',self.msg['path'])
        if os.path.isfile(path):
            dic = {'method': 'flie_list', 'code': 0, 'dir_dict': {self.msg['path']: ''},
                   'total_disk': self.total_size, 'res_disk': self.res_size, 'is_null': 1}
        else:
            dir_list = os.listdir(path)
            if dir_list:
                dic = {'method':'flie_list','code':1,'dir_dict':{self.msg['path']:dir_list},
                       'total_disk':self.total_size,'res_disk':self.res_size,'is_null':0}
            else:
                dic = {'method':'flie_list','code':1,'dir_dict':{self.msg['path']:dir_list},
                       'total_disk':self.total_size,'res_disk':self.res_size,'is_null':1}
        self.base_send(dic)

    def upload(self):
        '''
        上传文件
        :return:
        '''
        if self.msg['is_return']:
            return

        res_size = get_res_disk_size(self.user_name,self.total_size)
        file_size = self.msg['file_size'] #上传的文件大小
        file_name = self.msg['file_name']  # 上传的文件名
        file_path = self.msg['save_path']  # 上传的文件路径
        save_path = os.path.join(settings.BASE_PATH, 'filedb', file_path, file_name)

        if is_has_file(save_path): #判断上传的文件是否已存在
            self.base_send({'method': 'upload', 'code': 0, 'total_disk': self.total_size,
                            'res_disk': self.res_size, 'info': '文件已存在！'})
            return

        if res_size < file_size:
            self.base_send({'method': 'upload', 'code': 0,'total_disk':self.total_size,
                                'res_disk': self.res_size, 'info': '网盘空间已满！'})
            return
        self.base_send({'method': 'upload', 'code': 1, 'total_disk': self.total_size,
                        'res_disk': self.res_size, 'info': ''})

        f = open(save_path, mode='wb')
        read_size = 0
        obj = hashlib.md5()
        while read_size < file_size:
            msg = self.conn.recv(1024 * 1024)
            obj.update(msg)
            read_size += len(msg)
            f.write(msg)
            f.flush()
        f.close()
        msg = self.base_recv()['info']
        if msg == obj.hexdigest():
            res_size = res_size - file_size
            self.res_size = int(res_size/1024/1024)
            if self.res_size < 0:
                self.res_size = 0
            self.base_send({'method': 'upload', 'code': 1,'total_disk':self.total_size,
                                'res_disk': self.res_size, 'info': '文件上传成功！'})
        else:
            os.remove(save_path)
            self.base_send({'method': 'upload', 'code': 0,'total_disk':self.total_size,
                                'res_disk': self.res_size, 'info': '文件上传失败！'})

    def download(self):
        '''
        下载文件
        :return:
        '''
        file_path = os.path.join(settings.BASE_PATH, 'filedb', self.msg['file_path'])
        if not os.path.isfile(file_path):
            self.base_send({'code': 0, 'file_name': '', 'file_size': ''})
            return
        else:
            self.base_send({'code': 1, 'file_name': '', 'file_size': ''})

        self.msg = self.base_recv()
        if self.msg['is_return']:
            return
        file_path = os.path.join(settings.BASE_PATH,'filedb',self.msg['file_path'])
        file_name = os.path.basename(file_path)
        file_size = os.path.getsize(file_path)
        self.base_send({'file_name':file_name,'file_size':file_size})
        f = read_file(file_path)
        obj = hashlib.md5()
        for i in f:
            obj.update(i)
            self.conn.send(i)
        msg = self.base_recv()['info']
        if msg == obj.hexdigest():
            self.base_send({'method':'download','code':1,'info':'文件下载成功！'})
        else:
            self.base_send({'method':'download','code':0,'info':'文件下载失败！'})

    def exit(self):
        '''
        退出
        :return:
        '''
        if self.user_name:
            dic = json_from_file(settings.USER_INFO_PATH)
            dic[self.user_name]['total_disk'] = self.total_size
            dic[self.user_name]['res_disk'] = self.res_size
            json_to_file(dic,settings.USER_INFO_PATH) #用户退出时将磁盘大小更新到文件
        self.conn.close()
        self.user_name = ''
        self.conn = None

    def start(self,conn):
        '''
        启动
        :param conn: 客户端连接对象
        :return:
        '''
        self.conn = conn
        while self.conn:
            dic = self.base_recv()
            self.msg = dic
            getattr(self,dic['method'])()



class MyServer(socketserver.BaseRequestHandler):
    def handle(self):
        tcp_cli = Tcpsvr()
        tcp_cli.start(self.request)

def run():
    sk = socketserver.ThreadingTCPServer((settings.SERVER_IP, settings.SERVER_PORT), MyServer)
    sk.serve_forever()