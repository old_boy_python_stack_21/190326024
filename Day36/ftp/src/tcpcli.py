#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket, sys, os, hashlib
from lib.file_oper import read_file
from src.tcpbase import TcpBase
from lib.tools import processbar
from config import settings


class Tcpcli(TcpBase):

    def __init__(self, ip, port):
        self.dic1 = {'1': 'login','2':'register', '3': 'exit'}
        self.dic2 = {'a': 'upload','b':'create_dir','c':'to_up','d':'exit'}#对当前目录的操作
        self.dic3 = {'a': 'download','b':'del_dir_file','c':'to_down','d':'exit'} #对当前目录文件或文件夹的操作
        self.file_dict = {} #文件目录缓存，减少网络请求
        self.now_dir = '' #当前目录
        self.now_select = '' #当前选择的文件或文件夹
        self.total_size = 0 #网盘总容量
        self.res_size = 0 #剩余网盘容量
        self.ip = ip
        self.port = port
        self.conn = None
        self.user_name = None

    def create_dir(self):
        '''
        创建文件夹
        :return:
        '''
        while True:
            dir_name = input('请输入要创建的文件夹名称(输入Q/q返回)：')
            if dir_name.upper() == 'Q':
                return
            if dir_name in self.file_dict[self.now_dir]:
                print('文件夹已存在，请重新输入')
                continue
            self.base_send({'method':'create_dir',
                            'create_path':self.now_dir,'dir_name':dir_name})
            self.base_send({'method': 'flie_list', 'path': self.now_dir})
            dic = self.base_recv()
            self.file_dict[self.now_dir] = dic['dir_dict'][self.now_dir]   #创建后更新文件缓存字典
            break

    def to_up(self):
        '''
        返回上一层
        :return:
        '''
        self.now_dir = os.path.dirname(self.now_dir)

    def del_dir_file(self):
        '''
        删除文件或文件夹
        :return:
        '''
        inp = input('您确认要删除该文件(夹)及其子文件吗(Y/N)：')
        if inp.upper() == 'N':
            self.now_select = ''
            self.now_dir = os.path.dirname(self.now_dir)
            return
        self.base_send({'method':'del_dir_file','del_path':self.now_dir})
        self.now_select = ''
        self.now_dir = os.path.dirname(self.now_dir)
        self.base_send({'method': 'flie_list', 'path': self.now_dir})
        dic = self.base_recv()
        self.total_size = dic['total_disk'] #删除文件后更新磁盘大小
        self.res_size = dic['res_disk']
        self.file_dict[self.now_dir] = dic['dir_dict'][self.now_dir] #删除后更新文件缓存字典

    def to_down(self):
        '''
        进入下一层目录
        :return:
        '''
        if not self.file_dict.get(self.now_dir):
            self.base_send({'method':'flie_list','path':self.now_dir})
            dic = self.base_recv()
            if not dic['code']:
                print('选择的不是一个文件夹，请重新选择!')
                self.now_dir = os.path.dirname(self.now_dir)
                self.now_select = ''
                return
            self.file_dict[self.now_dir] = dic['dir_dict'][self.now_dir]

    def login(self):
        '''
        登录
        :return:
        '''
        if self.user_name:
            print('您已经登录过了')
            return
        while True:
            print('用户登录'.center(16, '*'))
            user_name = input('请输入用户名(输入Q/q返回上一层)：')
            if user_name.upper() == 'Q':
                return
            user_pwd = input('请输入密码：')
            self.base_send({'method':'login','user_name':user_name,'user_pwd':user_pwd})
            msg = self.base_recv()
            if msg['code'] == 1:
                self.user_name = user_name
                self.total_size = msg['total_disk']
                self.res_size = msg['res_disk']
                print(msg['info'])
                self.flie_list()
                break
            else:
                print(msg['info'])

    def register(self):
        '''
        注册
        :return:
        '''
        while True:
            print('用户注册'.center(20,'*'))
            user_name = input('请输入用户名(输入Q/q返回)：')
            if user_name.upper() == 'Q':
                return
            user_pwd = input('请输入密码：')
            self.base_send({'method':'register','user_name':user_name,'user_pwd':user_pwd})
            msg = self.base_recv()
            print(msg['info'])

    def flie_list(self):
        '''
        获取文件列表
        :return:
        '''
        self.base_send({'method':'flie_list','path':self.user_name})
        self.now_dir = self.user_name
        dic = self.base_recv()
        self.file_dict[self.now_dir] = dic['dir_dict'][self.now_dir]
        while True:
            print(('文件列表(容量:%s/%s(M))'%(self.res_size,self.total_size,)).center(30,'*'))
            num_list = []
            if not self.file_dict[self.now_dir]:
                print('当前目录无文件')
            else:
                for i in range(len(self.file_dict[self.now_dir])): #打印当前目录下的文件
                    num_list.append(str(i+1))
                    print('%s. %s'%(i+1,self.file_dict[self.now_dir][i],))
            print(('操作选择(%s)'%(self.now_dir,)).center(30,'*'))
            print('a.上传文件到当前文件夹\nb.创建文件夹\nc.返回上一级目录\nd.退出系统')
            index = input('请输入要选择的文件(夹)或操作：')
            if index in num_list:
                while True:
                    self.now_select = self.file_dict[self.now_dir][int(index)-1] #当前选择的文件或文件夹
                    self.now_dir = os.path.join(self.now_dir,self.now_select) #当前目录
                    print(('操作选择(%s)'%(self.now_dir,)).center(30, '*'))
                    print('a.下载该文件\nb.删除该文件(夹)\nc.进入下一级目录\nd.退出系统')
                    inp = input('请输入要选择的操作(Q/q返回上一层)：')
                    if inp.upper() == 'Q':
                        self.now_select = ''
                        self.now_dir = os.path.dirname(self.now_dir)
                        break
                    if inp not in self.dic3:
                        print('输入错误，请重新输入')
                        continue
                    getattr(self, self.dic3[inp])()
                    break
            elif index in self.dic2:
                if index =='c' and self.now_dir == self.user_name:
                    print('当前目录已经是最上层目录了。')
                    continue
                getattr(self,self.dic2[index])()
            else:
                print('输入错误，请重新输入')

    def upload(self):
        '''
        上传文件
        :return:
        '''
        print('文件上传'.center(20, '*'))
        while True:
            file_path = input('请输入要上传的文件路径(输入Q/q退出)：')
            if file_path.upper() == 'Q':
                self.base_send({'method':'upload','file_name': '', 'file_size': '','save_path':'', 'is_return':1})
                return
            if not os.path.isfile(file_path):
                print('输入的不是一个文件，请重新输入')
                continue
            file_name = os.path.basename(file_path)
            file_size = os.path.getsize(file_path)
            self.base_send({'method':'upload','file_name': file_name,
                            'file_size': file_size,'save_path':self.now_dir,
                            'is_return':0})
            msg = self.base_recv()
            if not msg['code']:
                print(msg['info'])
                return

            f = read_file(file_path)
            print('文件上传中...')
            obj = hashlib.md5()
            read_size = 0
            for i in f:
                obj.update(i)
                self.conn.send(i)
                read_size += len(i)
                processbar(read_size,file_size)
            self.base_send({'info':obj.hexdigest()})
            msg = self.base_recv()
            print(msg['info'])
            self.total_size = msg['total_disk'] #上传文件后更新云盘大小
            self.res_size = msg['res_disk']

            self.base_send({'method': 'flie_list', 'path': self.now_dir})
            dic = self.base_recv()
            self.file_dict[self.now_dir] = dic['dir_dict'][self.now_dir] #上传文件后更新缓存
            return

    def download(self):
        '''
        下载文件
        :return:
        '''
        print('文件下载'.center(20, '*'))
        while True:
            self.base_send({'method': 'download', 'file_path': self.now_dir})
            msg = self.base_recv()
            if not msg['code']:
                print('选择的不是一个文件，请重新选择！')
                self.now_dir = os.path.dirname(self.now_dir)
                self.now_select = ''
                return

            file_path = input('请输入存储路径(输入Q/q退出)：')
            if file_path.upper() == 'Q':
                self.base_send({'method':'download','file_path':'', 'is_return':1})
                return
            if not os.path.isdir(file_path):
                print('输入的不是一个路径，请重新输入')
                continue
            self.base_send({'method':'download','file_path':self.now_dir, 'is_return':0})
            msg = self.base_recv()
            file_name = msg['file_name']
            file_size = msg['file_size']
            full_path = os.path.join(file_path, file_name)
            f = open(full_path, mode='wb')
            read_size = 0
            print('文件下载中...')
            obj = hashlib.md5()
            while read_size < file_size:
                info = self.conn.recv(1024 * 1024)
                obj.update(info)
                read_size += len(info)
                processbar(read_size,file_size)
                f.write(info)
                f.flush()
            f.close()
            self.base_send({'info': obj.hexdigest()})
            msg = self.base_recv()
            print(msg['info'])
            self.now_dir = os.path.dirname(self.now_dir)
            self.now_select = ''
            return

    def exit(self):
        '''
        退出
        :return:
        '''
        content = {'method': 'exit'}
        self.base_send(content)
        self.conn.close()
        print('退出系统成功！')
        sys.exit(0)

    def start(self):
        '''
        启动
        :return:
        '''
        self.conn = socket.socket()
        self.conn.connect((self.ip, self.port))
        while True:
            print('欢迎使用云盘系统'.center(20, '*'))
            print('1.用户登录\n2.用户注册\n3.退出系统')
            inp = input('请选择：')
            if inp not in self.dic1:
                print('输入有误，请重新输入')
                continue
            getattr(self, self.dic1[inp])()

def run():
    tcp_cli = Tcpcli(settings.SERVER_IP, settings.SERVER_PORT)
    tcp_cli.start()
