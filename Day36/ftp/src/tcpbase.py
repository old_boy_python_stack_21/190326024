#!/usr/bin/env python
# -*- coding:utf-8 -*-
import struct,json,os,hashlib

class TcpBase(object):

    def read_file(self,path):
        total_size = os.path.getsize(path)
        read_size = 0
        with open(path, mode='rb') as f:
            while read_size < total_size:
                con = f.read(1024 * 1024)
                read_size += len(con)
                yield con

    def base_recv(self):
        length = struct.unpack('i',self.conn.recv(4))[0]
        recv_info = b''
        recv_size = 0
        while recv_size < length:
            recv_info += self.conn.recv(length-recv_size)
            recv_size = len(recv_info)
        return json.loads(recv_info.decode('utf-8'))

    def base_send(self,content):
        body_dict = json.dumps(content).encode('utf-8')
        head = struct.pack('i', len(body_dict))
        self.conn.send(head)
        self.conn.send(body_dict)

    def encrypt(self,pwd):
        obj = hashlib.md5('123'.encode('utf-8'))
        obj.update(pwd.encode('utf-8'))
        return obj.hexdigest()
