#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys

BATH_PATH = os.path.dirname(os.path.dirname(__file__))
sys.path.append(BATH_PATH)

from src.tcpsvr import run

if __name__ == '__main__':
    run()