#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
from config import settings

def read_file(path):
    total_size = os.path.getsize(path)
    read_size = 0
    with open(path, mode='rb') as f:
        while read_size < total_size:
            con = f.read(1024 * 1024)
            read_size += len(con)
            yield con

def get_res_disk_size(user_name,total_size):
    path = os.path.join(settings.BASE_PATH,'filedb',user_name)
    size = total_size * 1024 * 1024
    for a,b,c in os.walk(path):
        for i in c:
            size -= os.path.getsize(os.path.join(a,i))
    if size < 0:
        size = 0
    return size

def is_has_file(file_path):
    return os.path.exists(file_path)

def get_size(path):
    size = 0
    if os.path.isfile(path):
        size = os.path.getsize(path)
    else:
        for a,b,c in os.walk(path):
            for i in c:
                size += os.path.getsize(os.path.join(a,i))

    return size