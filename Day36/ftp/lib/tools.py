#!/usr/bin/env python
# -*- coding:utf-8 -*-

def processbar(size,total):
    ret = ''
    if size < total:
        ret = '\r%s>%s%%'%('='*int(size/total*50),int(size/total*100),)
    if size == total:
        ret = '\r%s>%s%%\n'%('='*50,100,)

    print(ret,end='')

