#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket
import gevent
from gevent import monkey
monkey.patch_all()

def func(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        conn.send(msg.upper().encode('utf-8'))

sk = socket.socket()
sk.bind(('127.0.0.1',8066))
sk.listen()

while True:
    conn,addr = sk.accept()
    gevent.spawn(func,conn)