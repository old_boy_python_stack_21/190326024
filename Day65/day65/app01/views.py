from django.shortcuts import render,redirect
from django import forms
from app01 import models
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError


class RegForm(forms.Form):
    username = forms.CharField(
        label='用户名',
        required=True,
        min_length=8,
        error_messages={
            'required': '用户名不能为空',
            'min_length': '用户名不能少于8位',
        },
    )
    phone = forms.CharField(
        label='手机号',
        required=True,
        validators=[RegexValidator('^1[3-9]\d{9}$','手机号格式不正确')],
        error_messages={
            'required':'手机号不能为空',
        }
    )
    pwd = forms.CharField(
        label='密码',
        required=True,
        min_length=8,
        max_length=12,
        widget= forms.PasswordInput,
        error_messages={
            'required':'密码不能为空',
            'min_length':'密码长度在8~12之间',
            'max_length':'密码长度在8~12之间',
        },
    )
    re_pwd = forms.CharField(
        label='确认密码',
        widget=forms.PasswordInput,
        max_length=12,
        required=True,
        error_messages={
            'required':'确认密码不能为空',
        }
    )
    def clean(self):
        pwd = self.cleaned_data.get('pwd')
        re_pwd = self.cleaned_data.get('re_pwd')
        if pwd == re_pwd:
            return self.cleaned_data
        else:
            self.add_error('re_pwd','两次密码输入不一致')
            raise ValidationError('两次密码输入不一致')

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if models.UserInfo.objects.filter(username=username).exists():
            raise ValidationError('用户名已存在')
        else:
            return username


def register(request):
    reg_form = RegForm()
    if request.method == 'POST':
        reg_form = RegForm(request.POST)
        if reg_form.is_valid():
            username = request.POST.get('username')
            phone = request.POST.get('phone')
            pwd = request.POST.get('pwd')
            models.UserInfo.objects.create(username=username,password=pwd,phone=phone)
            return redirect('/login/')
    return render(request, 'register.html', {'reg_form': reg_form})


def login(request):
    return render(request,'login.html')