from django.db import models

# Create your models here.

class UserInfo(models.Model):
    username = models.CharField(max_length=24)
    password = models.CharField(max_length=12)
    phone = models.CharField(max_length=12)


class Hobby(models.Model):
    hobby = models.CharField(max_length=10)

    def __str__(self):
        return self.hobby