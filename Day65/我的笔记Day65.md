# Day65

## 今日内容

1. form组件

## 内容详细

### 1.form组件

#### 1.1 form组件的功能

- 生产input标签
- 对提交的数据进行校验
- 提供错误提示

#### 1.2 form组件的定义

后台视图代码

```python
from django.shortcuts import render, HttpResponse
from django import forms
from app01 import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

#自定义验证
def checkname(value):
    # 通过校验规则 不做任何操作
    # 不通过校验规则   抛出异常
    if 'alex' in value:
        raise ValidationError('不符合社会主义核心价值观')

class RegForm(forms.Form):
    username = forms.CharField(
        label='用户名',
        min_length=6, #最小长度验证
        initial='张三', #默认值
        required=True , #是否为必须字段
        validators=[checkname],  #自定义验证 自动将值传入checkname函数
        error_messages={  #错误提示信息
            'required': '用户名是必填项', 
            'min_length': '用户名长度不能小于6位'
        }
    )
    pwd = forms.CharField(label='密码', widget=forms.PasswordInput) #widget html插件
    
    re_pwd = forms.CharField(label='确认密码', widget=forms.PasswordInput)
    
    gender = forms.ChoiceField(
        choices=((1, '男'), (2, '女')), #key value
        widget=forms.RadioSelect  #单选框  不设置为单选下拉列表
    )
    
    # hobby = forms.MultipleChoiceField(  #多选下拉框
    #     # choices=((1, "篮球"), (2, "足球"), (3, "双色球"),),
    #     choices=models.Hobby.objects.values_list('id', 'name'), 
    # )

    hobby = forms.ModelMultipleChoiceField(  #另一种多选下拉框
        # choices=((1, "篮球"), (2, "足球"), (3, "双色球"),),
        queryset=models.Hobby.objects.all(), #通过该方式，在对应的表类中添加__str__方法即可
    )

    phone = forms.CharField(
        validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')] #内置的校验器正则
    )

    def clean_username(self):
        # 局部钩子
        # 通过校验规则  必须返回当前字段的值
        # 不通过校验规则   抛出异常
        v = self.cleaned_data.get('username')
        if 'alex' in v:
            raise ValidationError('不符合社会主义核心价值观')
        else:
            return v

    def clean(self):
        # 全局钩子
        # 通过校验规则  必须返回当前所有字段的值
        # 不通过校验规则   抛出异常   模板通过'__all__.errors'显示错误信息
        pwd = self.cleaned_data.get('pwd')
        re_pwd = self.cleaned_data.get('re_pwd')
        if pwd == re_pwd:
            return self.cleaned_data
        else:
            self.add_error('re_pwd','两次密码不一致!!!!!') #为re_pwd添加错误信息，可以再模板中使用re_pwd来显示该错误信息
            raise ValidationError('两次密码不一致')

    # def __init__(self, *args, **kwargs):
    #     super(RegForm, self).__init__(*args, **kwargs)
    #     self.fields['hobby'].choices = models.Hobby.objects.values_list('id', 'name') #在类的初始化方法中设置选项，可以解决数据库改变而网页未改变的情况

def reg2(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():  # 对数据进行校验
            # 插入数据
            print(form_obj.cleaned_data)  # 通过校验的数据
            return HttpResponse('注册成功')

    return render(request, 'reg2.html', {'form_obj': form_obj})    

```

前端代码

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>

<form action="" method="post" novalidate>
    {% csrf_token %}
    <p>
        <label for="{{ form_obj.username.id_for_label }}">{{ form_obj.username.label }}</label>
        {{ form_obj.username }}
        {{ form_obj.username.errors }}
    </p>

    <p>
        <label for="{{ form_obj.pwd.id_for_label }}">{{ form_obj.pwd.label }}</label>
        {{ form_obj.pwd }}
        {{ form_obj.pwd.errors.0 }}
    </p>

    <p>
        <label for="{{ form_obj.re_pwd.id_for_label }}">{{ form_obj.re_pwd.label }}</label>
        {{ form_obj.re_pwd }}
        {{ form_obj.re_pwd.errors.0 }}
    </p>
{##}
{#    <p>#}
{#        <label for="{{ form_obj.gender.id_for_label }}">{{ form_obj.gender.label }}</label>#}
{#        {{ form_obj.gender }}#}
{#        {{ form_obj.gender.errors.0 }}#}
{#    </p>#}
{##}
{#    <p>#}
{#        <label for="{{ form_obj.hobby.id_for_label }}">{{ form_obj.hobby.label }}</label>#}
{#        {{ form_obj.hobby }}#}
{#        {{ form_obj.hobby.errors.0 }}#}
{#    </p>#}
{##}
{#    <p>#}
{#        <label for="{{ form_obj.phone.id_for_label }}">{{ form_obj.phone.label }}</label>#}
{#        {{ form_obj.phone }}#}
{#        {{ form_obj.phone.errors.0 }}#}
{#    </p>#}

    {{ form_obj.errors }}
    <button>注册</button>
</form>
</body>
</html>
```

模板使用总结：

```
{{ form_obj.as_p }}    __>   生产一个个P标签  input  label
{{ form_obj.errors }}    ——》   form表单中所有字段的错误
{{ form_obj.username }}     ——》 一个字段对应的input框
{{ form_obj.username.label }}    ——》  该字段的中午提示
{{ form_obj.username.id_for_label }}    ——》 该字段input框的id
{{ form_obj.username.errors }}   ——》 该字段的所有的错误
{{ form_obj.username.errors.0 }}   ——》 该字段的第一个错误的错误
```

#### 1.3 常用字段

```
CharField    
ChoiceField 
MultipleChoiceField
```

#### 1.4 字段参数

```
required=True,               是否允许为空
widget=None,                 HTML插件
label=None,                  用于生成Label标签或显示内容
initial=None,                初始值
error_messages=None,         错误信息 {'required': '不能为空', 'invalid': '格式错误'}  
validators=[],               自定义验证规则
disabled=False,              是否可以编辑
```

#### 1.5 验证

1. 内置验证

   ```
   required=True 必须字段
   min_length   最小长度
   max_length   最大长度
   ```

2. 自定义验证

   - 自己写函数

     ```python
     def checkname(value):
         # 通过校验规则 不做任何操作
         # 不通过校验规则   抛出异常
         if 'alex' in value:
             raise ValidationError('不符合社会主义核心价值观')
     ```

   - 使用内置校验器

     ```python
     from django.core.validators import RegexValidator
     phone = forms.CharField(
             validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')] #内置的校验器正则
         )
     ```

#### 1.6 钩子函数

1. 局部钩子

   ```
   def clean_字段名(self):
       # 局部钩子
       # 通过校验规则  必须返回当前字段的值
       # 不通过校验规则   抛出异常
       raise ValidationError('不符合社会主义核心价值观')
   ```

2. 全局钩子

   ```
   def clean(self):
       # 全局钩子
       # 通过校验规则  必须返回当前所有字段的值
       # 不通过校验规则   抛出异常   '__all__'
       pass
   ```

#### 1.7 is_valid流程

1.执行full_clean()的方法：

定义错误字典

定义存放清洗数据的字典

2.执行_clean_fields方法：

​	循环所有的字段

​	获取当前的值

​    对进行校验 （ 内置校验规则   自定义校验器）

1. 通过校验

   self.cleaned_data[name] = value 

   如果有局部钩子，执行进行校验：

   通过校验——》 self.cleaned_data[name] = value 

   不通过校验——》     self._errors  添加当前字段的错误 并且 self.cleaned_data中当前字段的值删除掉

2. 没有通过校验

   self._errors  添加当前字段的错误

3.执行全局钩子clean方法



