#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第二十五天作业
王凯旋
'''

# 一、正则表达式练习
# 1. 匹配整数或者小数（包括正数和负数）
'''
-?\d+(\.\d+)?
'''

# 2.匹配年月日日期 格式2018-12-6
'''
\d{4}-[01]?\d-[0-3]?\d
'''

# 3.匹配qq号
'''
\d{5,11}
'''

# 4.11位的电话号码
'''
1[3-9]\d{9}
'''

# 5.长度为8-10位的用户密码 ： 包含数字字母下划线
'''
\w{8,10}
'''

# 6.匹配验证码：4位数字字母组成的
'''
[0-9A-Za-z]{4}
'''

# 7.匹配邮箱地址
'''
[\w\.]{4,18}@[\da-z]{2,4}\.[a-zA-Z]{2,3}
'''

# 8. 1-2*((60-30+(-40/5)(9-25/3+7/399/42998+10568/14))-(-43)/(16-32))
# 从上面算式中匹配出最内层小括号以及小括号内的表达式
'''
\([^\(\)]+\)
'''

# 9.从类似9-25/3+7/399/42998+10*568/14的表达式中匹配出乘法或除法
'''
\d+((/|\*)\d+)+
'''

# 10.从类似
# <a>wahaha</a>
# <b>banana</b>
# <h1>qqxing</h1>
# 这样的字符串中，
# 1）匹配出<a>,<b>,<h1>这样的内容
# 2）匹配出wahaha，banana，qqxing内容。(思考题)
# 自学一下内容，完成10、2)
# ret = re.search("<(?P<tag_name>\w+)>\w+</w+>","<h1>hello</h1>")
# #还可以在分组中利用?的形式给分组起名字
# #获取的匹配结果可以直接用group('名字')拿到对应的值
# print(ret.group('tag_name')) #结果 ：h1
# print(ret.group()) #结果 ：<h1>hello</h1>
'''
1)
<.+?>
'''
'''
2)

import re
ret = re.search('<\w+>(?P<a>\w+)</\w+>','<a>wahaha</a>')
print(ret.group('a'))
ret = re.search('<\w+>(?P<a>\w+)</\w+>','<b>banana</b>')
print(ret.group('a'))
ret = re.search('<\w+>(?P<a>\w+)</\w+>','<h1>qqxing</h1>')
print(ret.group('a'))
'''


# 二、使用listdir完成计算文件夹大小
'''
import os

def get_size(path):
    total_size = 0
    temp_list = []
    l = [os.listdir(path)]
    abspath_list = [path]
    while l:
        for key in l[-1]:
            temp_path = os.path.join(abspath_list[-1], key)
            if os.path.isfile(temp_path):
                total_size += os.path.getsize(temp_path)
            else:
                temp_list.append(temp_path)
        l.pop()
        abspath_list.pop()
        for i in temp_list:
            l.append(os.listdir(i))
            abspath_list.append(i)
        temp_list.clear()
    return total_size

print(get_size(r'E:\Python'))

'''
