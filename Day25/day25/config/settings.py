#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

BASE_PATH = os.path.dirname(os.path.dirname(__file__))

OPER_LOG_PATH = os.path.join(BASE_PATH,'log','operlog.log')

ERROR_LOG_PATH = os.path.join(BASE_PATH,'log','errorlog.log')

ACCOUNT_PATH = os.path.join(BASE_PATH,'db','accountinfo.txt')

COURSE_PATH = os.path.join(BASE_PATH,'db','courseinfo.txt')

SELECT_COURSE_PATH = os.path.join(BASE_PATH,'db','selectcourse.txt')

OPER_LOG_WHEN = 'd'

ERROR_LOG_WHEN = 'd'

OPER_INTERVAL = 1

ERROR_INTERVAL = 1