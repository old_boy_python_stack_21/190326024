#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import pickle
from config import settings

def write_pickle(path,content):
    with open(path,mode='wb') as f:
        pickle.dump(content,f)

def read_pickle(path):
    if not os.path.exists(path):
        write_pickle(path,[])
    with open(path,mode='rb') as f:
        result = pickle.load(f)
        return result