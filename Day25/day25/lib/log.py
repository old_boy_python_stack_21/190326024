#!/usr/bin/env python
# -*- coding:utf-8 -*-
import logging
from config import settings
from logging import handlers


def get_oper_logger():
    file_handler = handlers.TimedRotatingFileHandler(filename=settings.OPER_LOG_PATH,
                                                     when=settings.OPER_LOG_WHEN,
                                                     interval=settings.OPER_INTERVAL,
                                                     encoding='utf-8')
    logging.basicConfig(
        level=logging.INFO,
        format= '%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S %p',
        handlers=[file_handler,]
    )
    return logging


def get_error_logger():
    file_handler = handlers.TimedRotatingFileHandler(filename=settings.ERROR_LOG_PATH,
                                                     when=settings.ERROR_LOG_WHEN,
                                                     interval=settings.ERROR_INTERVAL,
                                                     encoding='utf-8')
    logging.basicConfig(
        level=logging.ERROR,
        format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S %p',
        handlers=[file_handler, ]
    )
    return logging

oper_logger = get_oper_logger()
error_logger = get_error_logger()