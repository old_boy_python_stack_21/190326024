#!/usr/bin/env python
# -*- coding:utf-8 -*-

from lib import log
from src import base
from lib import oper_pickle
from config import settings
from src.course import Course
from src.student import Student


class administrator(base.Base):

    def __init__(self, user_name, pwd, name):
        self.user_name = user_name
        self.pwd = pwd
        self.name = name
        self.account_type = 0

    def add_course(self):
        try:
            lst = oper_pickle.read_pickle(settings.COURSE_PATH)
            while True:
                print('添加课程'.center(20, '*'))
                # print('课程名称 课程价格 课程周期 任课教师')
                # for item in lst:
                #     print('%s   %s   %s   %s' % (item.name, item.price, item.cycle, item.teacher,))
                name = input('请输入课程名称(输入N/n返回上一级)：')
                if name.upper() == 'N':
                    return
                price = input('请输入课程价格：')
                cycle = input('请输入课程周期：')
                teacher = input('请输入任课教师：')
                lst.append(Course(name, price, cycle, teacher))
                log.oper_logger.info('%s添加了%s课程，价格:%s,周期:%s,任课老师:%s' %
                                     (self.name, name, price, cycle, teacher,))
                print('添加课程成功！')
        except Exception as e:
            log.error_logger.error(str(e), exc_info=False)
        finally:
            oper_pickle.write_pickle(settings.COURSE_PATH, lst)

    def add_student_account(self):
        try:
            lst = oper_pickle.read_pickle(settings.ACCOUNT_PATH)
            while True:
                print('添加学生账号'.center(20, '*'))
                name = input('请输入学生姓名(输入N/n返回上一级)：')
                if name.upper() == 'N':
                    return
                user_name = input('请输入学生账号：')
                lst.append(Student(user_name, '123', name))
                log.oper_logger.info('%s添加了学生信息，姓名:%s,账号:%s' %
                                     (self.name, name, user_name))
                print('添加学生信息成功！')
        except Exception as e:
            log.error_logger.error(str(e), exc_info=False)
        finally:
            oper_pickle.write_pickle(settings.ACCOUNT_PATH, lst)

    def query_student(self):
        try:
            lst = oper_pickle.read_pickle(settings.ACCOUNT_PATH)
            print('学生姓名 账号')
            for item in lst:
                if item.account_type == 1:
                    print('%s   %s' % (item.name, item.user_name))
            input('输入任意字符返回上一级:')
            return
        except Exception as e:
            log.error_logger.error(str(e), exc_info=False)

    def query_select_course(self):
        try:
            select_list = oper_pickle.read_pickle(settings.SELECT_COURSE_PATH)
            if select_list:
                for item in select_list:
                    for i in item.courses:
                        print(('%s选择课程如下' % (item.name)).center(20, '*'))
                        print('课程名称 课程价格 课程周期 任课教师')
                        print('%s   %s   %s   %s' % (i.name, i.price, i.cycle, i.teacher,))
            else:
                print('无选课信息')

            input('输入任意字符返回上一级：')
            return
        except Exception as e:
            log.error_logger.error(str(e), exc_info=False)

    def main(self):
        try:
            log.oper_logger.info('%s登录本系统' % (self.name))
            while True:
                dic = {'1': 'add_course', '2': 'add_student_account', '3': 'query_course',
                       '4': 'query_student', '5': 'query_select_course', '6': 'exit'}
                print(('欢迎%s登录本系统' % self.name).center(20, '*'))
                print('1、创建课程\n2、创建学生账号\n3、查看所有课程\n4、查看所有学生\n5、查看所有学生的选课情况\n6、退出程序')
                try:
                    index = input('请选择:')
                    if index not in dic.keys():
                        print('输入错误，请重新输入')
                        continue
                    if hasattr(self, dic.get(index)):
                        getattr(self, dic.get(index))()
                except Exception as e:
                    log.error_logger.error(str(e), exc_info=False)
        except Exception as e:
            log.error_logger.error(str(e), exc_info=False)

# def func():
#     admin = administrator('alex', '123', '李杰')
#     oper_pickle.write_pickle(settings.ACCOUNT_PATH, [admin])
