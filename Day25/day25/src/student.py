#!/usr/bin/env python
# -*- coding:utf-8 -*-

from src import base
from lib import log
from lib import oper_pickle
from config import settings


class Student(base.Base):
    def __init__(self, user_name, pwd, name):
        self.user_name = user_name
        self.pwd = pwd
        self.name = name
        self.account_type = 1
        self.courses = []

    def main(self):
        log.oper_logger.info('%s登录本系统' % (self.name))
        dic = {'1': 'query_all_course', '2': 'select_course', '3': 'query_selected_course', '4': 'exit'}
        while True:
            print(('欢迎%s登录本系统' % self.name).center(20, '*'))
            print('1、查看所有课程\n2、选择课程\n3、查看所选课程\n4、退出程序')
            try:
                index = input('请选择:')
                if index not in dic.keys():
                    print('输入错误，请重新输入')
                    continue
                if hasattr(self, dic.get(index)):
                    getattr(self, dic.get(index))()
            except Exception as e:
                log.error_logger.error(str(e), exc_info=False)

    def select_course(self):
        lst = oper_pickle.read_pickle(settings.COURSE_PATH)
        select_list = oper_pickle.read_pickle(settings.SELECT_COURSE_PATH)
        student = self
        for item in select_list:
            if item.user_name == student.user_name:
                student = item
        else:
            select_list.append(student)
        while True:
            try:
                index_list = []
                print('序号 课程名称 课程价格 课程周期 任课教师')
                for i in range(len(lst)):
                    index_list.append(str(i + 1))
                    print(' %s  %s   %s   %s   %s' % (i + 1, lst[i].name, lst[i].price, lst[i].cycle,
                                                      lst[i].teacher,))
                index = input('输入N/n返回上一级:')
                if index.upper() == 'N':
                    return
                if index not in index_list:
                    print('输入错误，请重新输入')
                    continue
                cor = lst[int(index) - 1]
                student.courses.append(cor)
                print('选课成功')
                log.oper_logger.info('%s选择了%s课程，价格:%s,周期:%s,任课教师:%s' % (student.name, cor.name,
                                                                        cor.price, cor.cycle,
                                                                        cor.teacher))
            except Exception as e:
                log.error_logger.error(str(e), exc_info=False)
            finally:
                oper_pickle.write_pickle(settings.SELECT_COURSE_PATH, select_list)

    def query_selected_course(self):
        try:
            select_list = oper_pickle.read_pickle(settings.SELECT_COURSE_PATH)
            student = None
            for item in select_list:
                if item.user_name == self.user_name:
                    student = item

            if student:
                for i in student.courses:
                    print(('%s选择课程如下' % (student.name)).center(20, '*'))
                    print('课程名称 课程价格 课程周期 任课教师')
                    print('%s   %s   %s   %s' % (i.name, i.price, i.cycle, i.teacher,))
            else:
                print('该账户无选课信息')
            input('输入任意字符返回上一级：')
            return
        except Exception as e:
            log.error_logger.error(str(e),exc_info=False)
