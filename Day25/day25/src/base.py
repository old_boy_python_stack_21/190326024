#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
from lib import log
from lib import oper_pickle
from config import settings

class Base(object):

    def query_all_course(self):
        try:
            lst = oper_pickle.read_pickle(settings.COURSE_PATH)
            print('课程名称 课程价格 课程周期 任课教师')
            for item in lst:
                print('%s   %s   %s   %s' % (item.name, item.price, item.cycle, item.teacher,))
            while True:
                msg = input('输入N/n返回上一级:')
                if msg.upper() == 'N':
                    return
                print('输入错误，请重新输入')
        except Exception as e:
            log.error_logger.error(str(e),exc_info=False)

    def exit(self):
        log.oper_logger.info('%s退出本系统' % (self.name))
        sys.exit(0)