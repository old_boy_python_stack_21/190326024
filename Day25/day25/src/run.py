#!/usr/bin/env python
# -*- coding:utf-8 -*-

from config import settings
from lib import oper_pickle as pic

def login():
    account_list = pic.read_pickle(settings.ACCOUNT_PATH)
    while True:
        print('欢迎使用选课系统'.center(20,'*'))
        name = input('请输入用户名：')
        pwd = input('请输入密码：')
        for item in account_list:
            if name == item.user_name and pwd == item.pwd:
                print('登录成功')
                item.main()
        else:
            print('用户名或密码输入错误，请重新输入')