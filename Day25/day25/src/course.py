#!/usr/bin/env python
# -*- coding:utf-8 -*-

class Course:
    def __init__(self,name,price,cycle,teacher):
        self.name = name
        self.price = price
        self.cycle = cycle
        self.teacher = teacher
