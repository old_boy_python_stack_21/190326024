# Day47

## 今日内容

1. js的引入方式
2. js变量
3. js数据类型
4. 算术运算符
5. 赋值运算符
6. 数值和字符串转化
7. 数组Array

## 内容详细

### 1.js的引入方式

#### 1.1 内部js

只要放入Html文档中即可，一般放在head中或body中。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript">
        //js代码
    </script>
</head>
<body>
</body>
</html>
```

#### 1.2 外部js

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript" src="js/index.js"></script>
</head>
<body>
</body>
</html>
```

#### 1.3 js的注释

```js
//单行注释
/*
多行注释
*/
```

### 2.js的变量

#### 2.1 通过var关键字声明变量

```js
//声明并初始化变量
var a =12;
//声明变量
var b;
//变量赋值
b = 12;
```

#### 2.2 变量命名规范

- 必须以字母，下划线或者$符开头。
- 驼峰命名法
- 不能是js中的关键字he保留字

### 3.js的数据类型

#### 3.1 基本数据类型

1. number 数值类型

   ```js
   var a = 12;
   var b =1.234;
   var c = -1.2;
   alert(a);
   alert(typeof a);//typeof 获取变量的类型 
   ```

2. string 字符串类型

   字符串可以用双引号也可以用单引号

   ```js
   var a = '123';
   var b = "I'm MJJ";
   var c = "I'm \"MJJ\"";//转义
   ```

3. boolean 布尔类型

   - 0 假 false
   - 1 真 true

   ```js
   var c = 3 < 4;
   alert(c); // true
   ```

4. undefined 未定义

   当一个变量只是声明，但是没有赋值时，这时值为undefined,类型也为undefined。

   ```js
   var a;
   alert(a); //undefined
   alert(typeof a);//undefined
   ```

5. null 空

   ```js
   var a = null;
   alert(a); //null
   alert(typeof a) //object
   ```

#### 3.2 引用数据类型

1. object 类型

   我们看到的大多数的像Array、String、Number等这些类型的，他们统统属于对象，而且，object也是ECMAScript中使用最多的一个类型。并且每个对象都有属性和方法。

   - **对象的属性**：它是属于这个对象的某个变量。比如字符串的长度、数组的长度和索引、图像的宽高等
   - **对象的方法**：只有某个特定属性才能调用的函数。表单的提交、时间的获取等。

   创建对象:

   方式一：使用new操作符后跟Object构造函数

   ```js
   //创建对象
   var person = new Object();//等价于 var person = {};
   //给对象添加name和age属性
   person.name = 'jack';
   person.age = 28;
   //给对象添加fav的方法
   person.fav = function(){
       console.log('泡妹子');
   }
   ```

   方式二：使用**对象字面量**表示法。

   ```js
   var person = {
       name : 'jack',
       age : 28,
       fav : function(){
           console.log(123);
       }   
   }
   ```

   对象属性的调用和修改

   ```js
   //调用
   var a = person.name;
   var b = person['name'];
   //修改
   person.name = '123';
   person['name'] = '123';
   ```

2. Array 数组类型

   ```js
   var a = [1,2,'123',4];//可以放置不同类型的数据
   var b = [1,2,3,['1','2']];//可以嵌套
   console.log(a[2])//取值  console.log通过浏览器的控制台打印
   a[1] = 10; //修改值
   console.log(typeof a)//object  数组属于object类型
   //获取数组长度
   a.length; //4
   ```

3. Function 方法

### 4.算数运算符

```js
/*
+ 加
- 减
* 乘
/ 除
% 取余
*/
```

### 5.赋值运算符

```js
/*
++  
--
+=
-=
*=
/=
*/
```

### 6.数值和字符串的转换

#### 6.1 数值转字符串

```js
//方式一 隐式转换
var a = 123;
var b = a + '';
//方式二 方法转换
var a = 123;
var c = a.toString();
//方式三
var a = 123;
var b = String(a);
```

#### 6.2 字符串转数值

```js
var a = '123';
var b = Number(a);

//转换错误
var a = '123asd';
var b = Number(a);//得到结果NaN not a Number 数值运算错误
alert(typeof b); // number 

//parseInt
var a = '123asd';
var b = parseInt(a); //123

//parseFloat
var a = 1234.333;
var b = parseFloat(a);//1234.333

```

### 7.流程控制

#### 7.1 条件判断

```js
var score = 200;
if(score > 200){
    console.log('大于200');
}else if(score == 200){
    console.log('等于200');
}else{
    console.log('小于200');
}
// == 和 ===
var a = 2;
var b = '2';
console.log(a == b);//比较的是值
console.log(a === b); //比较是值和数据类型
```

#### 7.2 switch语句

```js
var weather = prompt('请输入今天天气');
switch(weather){
    case '晴天':
        console.log('123');
    case '阴天':
        console.log('456');
    default:
        console.log('789');       
}
```

### 8.循环

#### 8.1 for循环

```js
for(var i=0;i<10;i++){
    console.log(i);
}
```

#### 8.2 while循环

```js
var a = 1;
while(a<100){
    console.log(a);
    a++;
}
```

#### 8.3 do while循环

先执行再判断，至少执行一次。

```js
var a = 0;
do{
    console.log(a);
    a++;
}while(a<100);
```

### 9.js中的常用对象

#### 9.1 String 字符串

1. charAt和charCodeAt

   ```js
   var str = 'hello world';
   var b = str.charAt(3);
   console.log(b); //返回位于索引3处的字符
   
   var C = str.charCodeAt(3);
   console.log(C);//返回位于索引3处的字符编码
   ```

2. concat方法

   字符串拼接

   ```js
   var stringValue = "hello ";
   var result = stringValue.concat("world", "!");
   alert(result); //"hello world!" 
   
   //另一种拼接方式
   var name = 'alex';
   var age = 73;
   var str = `我叫${name},年龄是${age}`;
   alert(str);
   ```

3. 字符串截取

   - slice 参数：截取开始的位置上 ，截取结束的位置（不包括）

     ```js
     ar stringValue = "hello world";
     alert(stringValue.slice(3));//"lo world"
     alert(stringValue.slice(3, 7));//"lo w"
     ```

   - substring 参数：截取开始的位置上 ，截取结束的位置（不包括）

     ```js
     ar stringValue = "hello world";
     alert(stringValue.substring(3));//"lo world"
     alert(stringValue.substring(3,7));//"lo w"
     ```

   - substr 参数：截取开始的位置，截取的字符个数

     ```js
     ar stringValue = "hello world";
     alert(stringValue.substr(3));//"lo world"
     alert(stringValue.substr(3, 7));//"lo worl"
     ```

4. indexOf和lastIndexOf

   `indexOf()`方法从字符串的开头向后搜索子字符串，而 `lastIndexOf()`方法 是从字符串的末尾向前搜索子字符串 

   ```js
   var stringValue = "hello world";
   alert(stringValue.indexOf("o"));             //4
   alert(stringValue.lastIndexOf("o"));         //7
   alert(stringValue.indexOf("o", 6));         //7
   alert(stringValue.lastIndexOf("o", 6));     //4
   ```

5. trim方法

   去掉字符串两边的空格。

   ```js
   var stringValue = "   hello world   ";
   var trimmedStringValue = stringValue.trim();//hello world
   ```

#### 9.2 Array数组

1. isArray() 判断是否为一个数组

   ```js
   var ary = [1,2,3,4];
   console.log(Array.isArray(ary));
   ```

2. join连接方法

   ```js
   var colors = ['red','blue','green'];
   colors.join('||'); //red||blue||green
   ```

3. 栈方法

   - push方法：可以接收任意数量的参数，把它们逐个添加到数组末尾，并返回修改后数组的长度。

     ```js
     var ary = [1,2,3,4];
     var len = ary.push('23',44,66);
     console.log(len)
     ```

   - pop方法：从数组末尾移除最后一项，减少数组的 length 值，然后返回移除的项 

     ```js
     var ary = [1,2,3,4];
     var val = ary.pop();
     console.log(val)
     ```

4. concat方法

   - 参数为一个或多个数组，则该方法会将这些数组中每一项都添加到结果数组中。

   - 参数不为数组，则只是被简单地添加到结果数组的末尾

     ```js
     var ary = [1,2,3,4];
     var a = ary.concat({'123':123});//[1,2,3,4,{'123':123}]
     var b = ary.concat([5,6],[7,8]);//[1,2,3,4,5,6,7,8]
     ```

5. slice方法

   对数组进行切片。

   - 一个参数的情况下，slice()方法会返回从该参数指定位置开始到当前数组默认的所有项
   - 两个参数的情况下，该方法返回起始和结束位置之间的项——但不包括结束位置的项。

   ```js
   var ary = [1,2,3,4];
   var b = ary.slice(1); //[2,3,4]
   var c = ary.slice(1,3);//[2,3]
   var d = ary.slice(-2,-1);//使用数组长度分别和两个参数相加，得到结果再切片
   ```

6. splice方法

   - **删除**：可以删除任意数量的项，只需指定2个参数：要删除的第一项的位置和要删除的个数。例如`splice(0,2)`会删除数组中的前两项

     ```js
     var ary = [1,2,3,4,5,6];
     var b = ary.splice(1,2);
     console.log(b);// 结果为返回的值[2,3]
     ```

   - **插入**：可以向指定位置插入任意数量的项，只需提供3个参数：**起始位置**、**0（要删除的个数）**和**要插入的项**。如果要插入多个项，可以再传入第四、第五、以至任意多个项。例如，`splice(2,0,'red','green')`会从当前数组的位置2开始插入字符串`'red'`和`'green'`。

     ```js
     var ary = [1,2,3,4,5,6];
     ary.splice(1,0,'green','red');
     console.log(ary);//[1, "green", "red", 2, 3, 4, 5, 6]
     ```

   - **替换**：可以向指定位置插入任意数量的项，且同时删除任意数量的项，只需指定 3 个参数:**起始位置**、**要删除的项数**和**要插入的任意数量的项**。插入的项数不必与删除的项数相等。例如，`splice (2,1,"green")`会删除当前数组位置 2 的项，然后再从位置 2 开始插入字符串`"green"`。 

     ```js
     var ary = [1,2,3,4,5,6];
     var b = ary.splice(2,1,'green');
     console.log(ary);//[1,2,'red',4,5,6]
     console.log(b); //[3]返回值为删除的值
     ```

   7. indexof和lastIndexOf

      indexOf()方法从数组的开头(位置 0)开始向后查找，lastIndexOf()方法则从数组的末尾开始向前查找。

      两个方法都返回要查找的那项在数组中的位置，或者在没找到的情况下返回-1。

      ```js
      var numbers = [1,2,3,4,5,4,3,2,1];
      alert(numbers.indexOf(4)); //3
      alert(numbers.lastIndexOf(4));//5
      
      //可以查找对象
      var person = {name:"mjj"};
      var people = [{name:'mjj'}];
      var morePeople = [person];
      alert(people.indexOf(person)); //-1
      alert(morePeople.indexOf(person));// 0
      ```

   8. forEach方法

      它只是对数组中的每一项运行传入的函数。这个方法没有返回值。

      ```js
      var numbers = [1,2,3,4,5,4,3,2,1];
      numbers.forEach(function(item, index, array){
      });//数组的项  数组的索引  数组
      ```

#### 9.3 date日期

1. 创建日期对象

   ```js
   var now = new Date();
   console.log(now);//Mon Jun 03 2019 21:15:52 GMT+0800
   ```

2. toLocalSting 格式化

   ```js
   var now = new Date();
   var s = now.toLocaleString();
   console.log(s);//2019/6/3 下午9:15:52
   ```

3. getDate方法

   根据本地时间返回指定日期对象的月份中的第几天(1-31)

   ```js
   var now = new Date();
   console.log(now.getDate());
   ```

4. getMonth方法

5. 根据本地时间返回指定日期对象的月份(0-11)

   ```js
   var now = new Date();
   console.log(now.getMonth());
   ```

6. getFullYear方法

   根据本地时间返回指定日期对象的年份

   ```js
   var now = new Date();
   console.log(now.getFullYear());
   ```

7. getDay方法

   根据本地时间返回指定的日期对象的星期中的第几天(0-6) ,0代表星期日

   ```js
   var now = new Date();
   console.log(now.getDay());
   ```

8. getHours方法

   根据本地时间返回指定的日期对象的小时(0-23)

   ```js
   var now = new Date();
   console.log(now.getHours());
   ```

9. 根据本地时间返回指定的日期对象的分钟(0-59)

   ```js
   var now = new Date();
   console.log(now.getMinutes());
   ```

10. 根据本地时间返回指定的日期对象的秒数(0-59)

    ```js
    var now = new Date();
    console.log(now.getSeconds());
    ```

#### 9.4 Math数学对象

1. 舍入方法

   - `Math.ceil()`执行向上舍入，即它总是将数值向上舍入为最接近的整数;
   - `Math.floor()`执行向下舍入，即它总是将数值向下舍入为最接近的整数;  
   - `Math.round()`执行标准舍入，即它总是将数值四舍五入为最接近的整数(这也是我们在数学课上学到的舍入规则)。 

   ```js
   var num = 25.7;
   var num2 = 25.2;
   alert(Math.ceil(num));//26
   alert(Math.floor(num));//25
   alert(Math.round(num));//26
   alert(Math.round(num2));//25
   ```

2. random方法

   返回大于等于 0 小于 1 的一个随机数 

   ```js
   //获取min到max的范围的整数
   function random(lower, upper) {
       return Math.floor(Math.random() * (upper - lower)) + lower;
   }
   ```

   

