#!/usr/bin/env python
# -*- coding:utf-8 -*-
import math
from django.http.request import QueryDict

class Pagination:
    def __init__(self,page,total_count,param=None,show_page=11,pre_count=10):
        '''

        :param page: 当前页码
        :param total_count:数据总量
        :param show_page: 展示的页码个数
        :param pre_count: 每页展示的数据个数
        '''
        try:
            self.page = int(page)
            if self.page <= 0:
                self.page = 1
        except Exception:
            self.page = 1

        self.param = param
        if not self.param:
            self.param = QueryDict(mutable=True)
        total_page = math.ceil(total_count / pre_count)
        half_page = show_page // 2
        if total_page <= show_page:
            start_page = 1
            end_page = total_page
        else:
            if self.page <= half_page:
                start_page = 1
                end_page = show_page
            elif self.page + half_page > total_page:
                start_page = total_page - show_page + 1
                end_page = total_page
            else:
                start_page = self.page - half_page
                end_page = self.page + half_page
        self.total_page = total_page
        self.start_page = start_page
        self.end_page = end_page
        self.start = (self.page - 1) * pre_count
        self.end = self.page * pre_count

    @property
    def page_html(self):
        page_list = []
        if self.page == 1:
            page_list.append(
                '<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>')
        else:
            self.param['page'] = self.page - 1
            page_list.append(
                '<li><a href="?{}" aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>'.format(
                    self.param.urlencode()))
        for i in range(self.start_page, self.end_page + 1):
            self.param['page'] = i
            if i == self.page:
                page_list.append('<li class="active"><a href="?{}">{}</a></li>'.format(self.param.urlencode(), i))
            else:
                page_list.append('<li><a href="?{}">{}</a></li>'.format(self.param.urlencode(), i))
        if self.page == self.total_page:
            page_list.append(
                '<li class="disabled"><a aria-label="Next"><span aria-hidden="true">下一页</span></a></li>')
        else:
            self.param['page'] = self.page + 1
            page_list.append(
                '<li><a href="?{}" aria-label="Next"><span aria-hidden="true">下一页</span></a></li>'.format(
                    self.param.urlencode()))
        return page_list
