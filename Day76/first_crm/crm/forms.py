#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django import forms
from crm import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from comm.encrypt import encrypt_pwd


class BSModelForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if name == 'birthday' or name == 'next_date':
                field.widget.attrs['class'] = 'form-control pull-right datepicker'
            else:
                field.widget.attrs['class'] = 'form-control'


class RegForm(forms.ModelForm):
    password = forms.CharField(
        min_length=8,
        widget=forms.PasswordInput(attrs={'placeholder': '请输入密码', 'autocomplete': 'off'})
    )
    re_password = forms.CharField(
        min_length=8,
        widget=forms.PasswordInput(attrs={'placeholder': '请再次输入密码', 'autocomplete': 'off'})
    )
    mobile = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': '请输入手机号', 'autocomplete': 'off'}),
        validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')]
    )

    class Meta:

        model = models.UserProfile
        fields = '__all__'
        exclude = ['is_active']
        widgets = {
            'username': forms.EmailInput(attrs={'placeholder': '请输入邮箱地址', 'autocomplete': 'off'}),
            'name': forms.TextInput(attrs={'placeholder': '请输入真实姓名', 'autocomplete': 'off'}),
        }

    def clean_username(self):
        username = self.cleaned_data.get('username')
        obj = models.UserProfile.objects.filter(username=username).first()
        if obj:
            raise ValidationError('用户名已存在')
        else:
            return username

    def clean(self):
        self._validate_unique = True
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if password == re_password:
            if password:
                self.cleaned_data['password'] = encrypt_pwd(password)
            return self.cleaned_data
        else:
            self.add_error('re_password', '两次密码输入不一致')
            raise ValidationError('两次密码输入不一致')


class CustomerForm(BSModelForm):
    course = forms.MultipleChoiceField(label='咨询课程',choices=(('Linux', 'Linux中高级'),
                                                ('PythonFullStack', 'Python高级全栈开发'),))

    class Meta:
        model = models.Customer
        fields = '__all__'
        widgets = {

        }

    # def __init__(self, *args, **kwargs):
    #     super().__init__(*args, **kwargs)
    #     for name, field in self.fields.items():
    #         if name == 'birthday' or name == 'next_date':
    #             field.widget.attrs['class'] = 'form-control pull-right datepicker'
    #         else:
    #             field.widget.attrs['class'] = 'form-control'


class ConsultRecordForm(BSModelForm):
    class Meta:
        model = models.ConsultRecord
        fields = '__all__'
        exclude = ['delete_status']

    def __init__(self, request=None, customer_pk=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if request:
            if customer_pk and customer_pk != '0':
                self.fields['customer'].choices = [(i.pk, str(i)) for i in
                                                   request.user_obj.customers.filter(pk=customer_pk)]
            elif not customer_pk:
                self.fields['customer'].choices = [(self.instance.customer_id, self.instance.customer)]
            else:
                self.fields['customer'].choices = [('', '-----------'), ] + [(i.pk, str(i)) for i in
                                                                             request.user_obj.customers.all()]
            self.fields['consultant'].choices = [(request.user_obj.pk, request.user_obj)]


class EnrollmentForm(BSModelForm):
    class Meta:
        model = models.Enrollment
        fields = '__all__'
        exclude = ['delete_status']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['customer'].choices = [(self.instance.customer.pk, self.instance.customer)]
        # self.fields['enrolment_class'].choices = [(i.pk,i)for i in self.instance.customer.class_list.all()]


class ClassForm(BSModelForm):

    class Meta:
        model = models.ClassList
        fields = '__all__'

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['start_date'].widget.attrs['class'] = 'form-control pull-right datepicker'
        self.fields['graduate_date'].widget.attrs['class'] = 'form-control pull-right datepicker'
        self.fields['teachers'].choices = [(i.pk,i) for i in models.UserProfile.objects.filter(department_id=2)]


class CourseRecordForm(BSModelForm):

    class Meta:
        model = models.CourseRecord
        fields = '__all__'

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['re_class'].choices = [(self.instance.re_class.pk,self.instance.re_class)]
        self.fields['teacher'].choices = [(i.pk,i) for i in self.instance.re_class.teachers.all()]
        self.fields['recorder'].choices = [(self.instance.recorder.pk,self.instance.recorder)]


class StudyRecordForm(BSModelForm):

    class Meta:
        model = models.StudyRecord
        fields = '__all__'

