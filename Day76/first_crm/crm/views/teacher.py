#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.shortcuts import render, redirect, reverse
from django.http.response import JsonResponse
from crm.forms import ClassForm, CourseRecordForm,StudyRecordForm
from crm import models
from comm.pagination import Pagination
from django.views import View
from django.forms import modelformset_factory
import json


def class_list(request):
    page = request.GET.get('page', 1)
    obj_list = models.ClassList.objects.all()
    pagination = Pagination(page, obj_list.count())
    return render(request, 'teacher/class_list.html',
                  {'obj_list': obj_list[pagination.start:pagination.end], 'page_list': pagination.page_html})


class OperClass(View):
    def get(self, request, pk=None, *args, **kwargs):
        class_obj = models.ClassList.objects.filter(pk=pk).first()
        form_obj = ClassForm(instance=class_obj)
        title = '编辑信息' if pk else '添加信息'
        return render(request, 'teacher/oper_class.html', {'form_obj': form_obj, 'title': title})

    def post(self, request, pk=None, *args, **kwargs):
        class_obj = models.ClassList.objects.filter(pk=pk).first()
        form_obj = ClassForm(data=request.POST, instance=class_obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect('class')
        title = '编辑信息' if pk else '添加信息'
        return render(request, 'teacher/oper_class.html', {'form_obj': form_obj, 'title': title})


def course_record_list(request, class_pk):
    page = request.GET.get('page', 1)
    obj_list = models.CourseRecord.objects.filter(re_class_id=class_pk).order_by('-pk')
    pagination = Pagination(page, obj_list.count())
    request.session['return_url'] = request.get_full_path()
    return render(request, 'teacher/course_record_list.html',
                      {'obj_list': obj_list[pagination.start:pagination.end], 'page_list': pagination.page_html,
                       'class_pk': class_pk})


def oper_course_record(request, pk=None, class_pk=None):
    course_record = models.CourseRecord(re_class_id=class_pk,
                                        recorder=request.user_obj) if class_pk else models.CourseRecord.objects.filter(
        pk=pk).first()
    form_obj = CourseRecordForm(instance=course_record)
    if request.method == 'POST':
        form_obj = CourseRecordForm(data=request.POST, instance=course_record)
        return_url = request.session.get('return_url')
        if form_obj.is_valid():
            form_obj.save()
            return redirect(return_url)
    title = '编辑信息' if pk else '添加信息'
    return render(request, 'teacher/oper_course_record.html', {'form_obj': form_obj, 'title': title})


def add_study_record(request):
    pk_list = json.loads(request.POST.get('pk_list'))
    course_records = models.CourseRecord.objects.filter(pk__in=pk_list)
    for course_record in course_records:
        enrollments = course_record.re_class.enrollment_set.filter(customer__status='studying')
        student_list = []
        for enrollment in enrollments:
            student_list.append(models.StudyRecord(course_record=course_record,student=enrollment.customer))
        models.StudyRecord.objects.bulk_create(student_list)
    return JsonResponse({'info':'添加成功'})


def study_record_list(request,course_pk):
    ModelFormSet = modelformset_factory(models.StudyRecord,StudyRecordForm,extra=0)
    formset_obj = ModelFormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_pk))
    if request.method == 'POST':
        formset_obj = ModelFormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_pk),data=request.POST)
        return_url = request.session.get('return_url')
        if formset_obj.is_valid():
            formset_obj.save()
            if return_url:
                return redirect(return_url)

    return render(request,'teacher/study_record_list.html',{'form_set_obj':formset_obj})
