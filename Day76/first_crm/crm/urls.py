from django.conf.urls import url
from crm.views import auth,consultant,teacher

urlpatterns = [
    # 登录，注册，注销，主页
    url(r'^login/$', auth.login, name='login'),
    url(r'^reg/$', auth.reg, name='reg'),
    url(r'^index/$', auth.index, name='index'),
    url(r'^logout/$', auth.logout, name='logout'),

    # 客户管理
    url(r'^customer_list/$', consultant.customer_list, name='my_customer'),
    url(r'^public_customer_list/$', consultant.customer_list, name='public_customer'),
    url(r'^add_customer/$', consultant.oper_customer, name='add_customer'),
    url(r'^edit_customer/(\d+)/$', consultant.oper_customer, name='edit_customer'),
    url(r'^details/(\d+)/(\w+)/(\w+)/$', consultant.details, name='details'),
    url(r'^customer_change/$', consultant.customer_change, name='change'),

    # 跟进管理
    url(r'^consult_record_list/$', consultant.consult_record_list, name='consult_record'),
    url(r'^cus_consult_record_list/(\d+)/$', consultant.consult_record_list, name='cus_consult_record'),
    url(r'^add_consult_record/(?P<customer_pk>\d+)/$', consultant.oper_consult_record, name='add_consult_record'),
    url(r'^edit_consult_record/(\d+)/$', consultant.oper_consult_record, name='edit_consult_record'),

    # 报名管理
    url(r'^enrollment_list/$', consultant.enrollment_list, name='enrollment'),
    url(r'^add_enrollment/(?P<customer_pk>\d+)/$', consultant.oper_enrollment, name='add_enrollment'),
    url(r'^edit_enrollment/(\d+)/$', consultant.oper_enrollment, name='edit_enrollment'),

    # 班级管理
    url(r'^class_list/$', teacher.class_list, name='class'),
    url(r'^add_class/$', teacher.OperClass.as_view(), name='add_class'),
    url(r'^edit_class/(\d+)/$', teacher.OperClass.as_view(), name='edit_class'),

    #课程记录管理
    url(r'^course_record_list/(\d+)/$', teacher.course_record_list, name='course_record'),
    url(r'^add_course_record/(?P<class_pk>\d+)/$', teacher.oper_course_record, name='add_course_record'),
    url(r'^edit_course_record/(\d+)/$', teacher.oper_course_record, name='edit_course_record'),

    #学习记录管理
    url(r'^add_study_record/$', teacher.add_study_record, name='add_study_record'),
    url(r'^study_record_list/(\d+)/$', teacher.study_record_list, name='study_record_list'),

]
