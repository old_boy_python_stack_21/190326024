#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect,reverse
from crm import models
from comm.encrypt import encrypt_pwd

class LoginMiddleware(MiddlewareMixin):
    def process_request(self,request):
        lst = [reverse('login'),reverse('reg')]
        if request.path_info not in lst and not request.path_info.startswith('/admin'):
            is_login = request.COOKIES.get('is_login')
            if not is_login:
               return redirect('login')
        obj = models.UserProfile.objects.filter(pk=request.session.get('pk')).first()
        if obj:
            request.user_obj = obj