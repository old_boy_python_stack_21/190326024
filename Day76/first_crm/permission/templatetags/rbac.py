#!/usr/bin/env python
# -*- coding:utf-8 -*-
import re
from django import template
from django.conf import settings
from collections import OrderedDict
register = template.Library()

@register.inclusion_tag('rbac/menu.html')
def meun(request):
    menu_id = request.menu_id
    od = OrderedDict()
    menu_dic = request.session.get(settings.MENU_SESSION_KEY)
    key_list = sorted(menu_dic,key=lambda x:menu_dic[x]['weight'],reverse=True)
    for key in key_list:
        od[key] = menu_dic[key]
    for i in od.values():
        i['class'] = 'hide'
        for m in i['children']:
            if menu_id == m['id']:
                m['class'] = 'active'
                i['class'] = ''
    return{'menu_list':od.values()}


@register.inclusion_tag('rbac/breadcrumb.html')
def breadcrumb(request):
    return {'breadcrumb_list':request.breadcrumb_list}


@register.filter
def has_permission(request,arg):
    dic = request.session.get(settings.PERMISSION_SESSION_KEY)
    if arg in dic:
        return True

@register.simple_tag
def gen_role_url(request, rid):
    params = request.GET.copy()
    params['rid'] = rid
    return params.urlencode()