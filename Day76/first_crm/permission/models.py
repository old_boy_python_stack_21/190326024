from django.db import models


class Role(models.Model):
    name = models.CharField(verbose_name='角色名称',max_length=10)
    permissions = models.ManyToManyField('Permission',blank=True)

    def __str__(self):
        return self.name


class User(models.Model):
    roles = models.ManyToManyField(Role,blank=True)
    class Meta:
        abstract = True



class Permission(models.Model):
    url = models.CharField(verbose_name='路由地址',max_length=100)
    title = models.CharField(verbose_name='标题',max_length=20)
    name = models.CharField(verbose_name='别名',max_length=50,unique=True)
    menu = models.ForeignKey('Menu',verbose_name='一级菜单',blank=True,null=True)
    p_id = models.ForeignKey('self',verbose_name='父权限id',blank=True,null=True)


    def __str__(self):
        return self.title




class Menu(models.Model):
    title = models.CharField(verbose_name='标题',max_length=10)
    icon =  models.CharField(verbose_name='图标',max_length=50)
    weight = models.IntegerField(verbose_name='权重',default=1)

    def __str__(self):
        return self.title
