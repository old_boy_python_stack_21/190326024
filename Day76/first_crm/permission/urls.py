#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.conf.urls import url
from permission import views

urlpatterns = [
    url(r'role_list/$', views.role_list, name='role_list'),
    url(r'role_add/$', views.role_change, name='role_add'),
    url(r'role_edit/(\d+)/$', views.role_change, name='role_edit'),

    url(r'menu_list/$', views.menu_list, name='menu_list'),
    url(r'menu_add/$', views.menu_change, name='menu_add'),
    url(r'menu_edit/(\d+)/$', views.menu_change, name='menu_edit'),

    url(r'permission_add/$', views.permission_change, name='permission_add'),
    url(r'permission_edit/(\d+)/$', views.permission_change, name='permission_edit'),

    url(r'^(role|menu|permission)/del/(\d+)/$', views.delete, name='delete'),

    url(r'^multi/permissions/$', views.multi_permissions, name='multi_permissions'),

    url(r'^distribute/permissions/$', views.distribute_permissions, name='distribute_permissions'),
]
