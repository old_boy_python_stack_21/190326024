#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect, reverse, HttpResponse
from django.conf import settings
import re


class PermissionMiddleware(MiddlewareMixin):

    def process_request(self, request):

        url = request.path_info
        for i in settings.WHITE_LIST:
            if re.match(r'{}$'.format(i), url):
                return
        if not request.session.get('is_login'):
            return redirect(reverse('login'))

        request.menu_id = None
        request.breadcrumb_list = [
            {'url': '/index/', 'title': '首页'}
        ]
        for i in settings.NO_PERMISSION:
            if re.match(r'{}$'.format(i), url):
                return
        permission_dic = request.session.get(settings.PERMISSION_SESSION_KEY)
        for i in permission_dic.values():
            if re.match(r'{}$'.format(i.get('url')), url):
                name = i['name']
                pname = i['pname']
                if i['p_id']:
                    request.menu_id = i['p_id']
                    request.breadcrumb_list.append(
                        {'url': permission_dic[pname]['url'], 'title': permission_dic[pname]['title']})
                    request.breadcrumb_list.append(
                        {'url': permission_dic[name]['url'], 'title': permission_dic[name]['title']})
                else:
                    request.menu_id = i['id']
                    request.breadcrumb_list.append(
                        {'url': permission_dic[name]['url'], 'title': permission_dic[name]['title']})
                return
        return HttpResponse('您没有权限访问该页面！')
