#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.conf import settings


def permission_init(request, obj):
    request.session['is_login'] = 1
    permissions = obj.roles.filter(permissions__isnull=False).values(
        'permissions__url',
        'permissions__title',
        'permissions__menu_id',
        'permissions__menu__title',
        'permissions__menu__icon',
        'permissions__menu__weight',
        'permissions__id',
        'permissions__p_id',
        'permissions__name',
        'permissions__p_id__name',

    ).distinct()

    permission_dic = {}
    menu_dic = {}
    for i in permissions:
        permission_dic[i['permissions__name']] = (
            {'url': i['permissions__url'], 'id': i['permissions__id'], 'p_id': i['permissions__p_id'],
             'title': i['permissions__title'],'name':i['permissions__name'],'pname':i['permissions__p_id__name']})

        menu_id = i.get('permissions__menu_id')
        if menu_id:
            temp_dic = {'title': i.get('permissions__title'), 'url': i.get('permissions__url'),
                        'id': i.get('permissions__id')}
            if not menu_dic.get(menu_id):
                temp = {
                    'title': i.get('permissions__menu__title'),
                    'icon': i.get('permissions__menu__icon'),
                    'weight': i.get('permissions__menu__weight'),
                    'children': [temp_dic]
                }
                menu_dic[menu_id] = temp
            else:
                menu_dic[menu_id]['children'].append(temp_dic)
    for i in permission_dic:
        print(i)
    request.session[settings.PERMISSION_SESSION_KEY] = permission_dic
    request.session[settings.MENU_SESSION_KEY] = menu_dic
