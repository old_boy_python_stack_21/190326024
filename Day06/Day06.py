#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第六天作业
王凯旋
'''

# 1.列举你了解的字典中的功能（字典独有）。
'''
keys()  
values()
items()
get()
update()
'''

# 2.列举你了解的集合中的功能（集合独有）。
'''
append()
insert()
pop()
remove()
clear()
extend()
reverse()
sort()
'''

# 3.列举你了解的可以转换为 布尔值且为False的值。
'''
0,"",[],(),{},set(),None
'''

# 4.请用代码实现 info = {'name':'王刚蛋','hobby':'铁锤'}
# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出。
'''
info = {'name':'王刚蛋','hobby':'铁锤'}
while True:
    content = input("请输入键：")
    print(info.get(content))
'''

# 循环提示用户输入，根据用户输入的值为键去字典中获取对应的值并输出（如果key不存在，则获取默认“键不存在”，并输出）。
# 注意：无需考虑循环终止（写死循环即可）
'''
info = {'name': '王刚蛋', 'hobby': '铁锤'}
while True:
    content = input("请输入键：")
    print(info.get(content, "键不存在"))
'''

# 5.请用代码验证 "name" 是否在字典的键中？
'''
info = {'name':'王刚蛋','hobby':'铁锤','age':'18'}
if info.get("name") == None:
    print("不存在")
else:
    print("存在")
'''

# 6.请用代码验证 "alex" 是否在字典的值中？
'''
info = {'name': '王刚蛋', 'hobby': '铁锤', 'age': '18'}
if "alex" in list(info.values()):
    print("存在")
else:
    print("不存在")
'''

# 7.有如下：
# v1 = {'武沛齐','李杰','太白','景女神'}
# v2 = {'李杰','景女神'}

# 请得到 v1 和 v2 的交集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v1.intersection(v2))
'''

# 请得到 v1 和 v2 的并集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v1.union(v2))
'''

# 请得到 v1 和 v2 的 差集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v1.difference(v2))
'''

# 请得到 v2 和 v1 的 差集并输出
'''
v1 = {'武沛齐','李杰','太白','景女神'}
v2 = {'李杰','景女神'}
print(v2.difference(v1))
'''

# 8.循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
'''
lst = []
while True:
    content = input("请输入：")
    if content.lower() == "n":
        break
    lst.append(content)
print(lst)
'''

# 9.循环提示用户输入，并将输入内容添加到集合中（如果输入N或n则停止循环）
'''
value = set()
while True:
    content = input("请输入：")
    if content.lower() == "n":
        break
    value.add(content)
print(value)
'''

# 10.写代码实现
# 循环提示用户输入，如果输入值在v1中存在，则追加到v2中，
# 如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）
# v1 = {'alex','武sir','肖大'}
# v2 = []
'''
v1 = {'alex', '武sir', '肖大'}
v2 = []
while True:
    content = input("请输入：")
    if content.lower() == "n":
        break
    if content in v1:
        v2.append(content)
    else:
        v1.add(content)
print(v1, v2)
'''

# 11.判断以下值那个能做字典的key ？那个能做集合的元素？
'''
    1     可以
    -1    可以
    ""    可以
    None  可以
    [1,2]   不可以
    (1,)    可以
    {11,22,33,4}   不可以
    {'name':'wupeiq','age':18} 不可以
'''

# 12.is 和 == 的区别？
'''
#is 用来判断两个变量的内存地址是否相等
#== 用来判断两个变量的值是否相等
'''

# 13.type使用方式及作用？
'''
#用法
v1 = 123
print(type(v1))

#作用：获取变量的数据类型
'''

# 14.id的使用方式及作用？
'''
#用法
v1 = 123
print(id(v1))

#作用：获取变量的内存地址
'''

# 15.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = {'k1':'v1','k2':[1,2,3]}

result1 = v1 == v2
result2 = v1 is v2
print(result1)
print(result2)
'''

'''
#结果
True
False
#原因
== 比较的是两个变量的值是否相等
is 比较的是两个变量的内存地址是否相等
v1和v2分别赋值，开辟了两块不同的内存来存放两个变量的值，他们的值相等，但内存地址不同
'''

# 16.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

result1 = v1 == v2
result2 = v1 is v2
print(result1)
print(result2)
'''

'''
#结果
True
True
#原因
v2 = v1 是将v1变量的内存地址赋给了v2变量。这时两个变量引用的同一块内存。所以他们的值和内存地址都是相等的
'''

# 17.看代码写结果并解释原因
'''
v1 = {'k1':'v1','k2':[1,2,3]}
v2 = v1

v1['k1'] = 'wupeiqi'
print(v2)
'''

'''
#结果
v1 = {'k1':'wupeiqi','k2':[1,2,3]}
#原因
v2 = v1 是将v1变量的内存地址赋给了v2变量。这时两个变量引用的同一块内存。所以当修改了v1的值后，v2的值也改了。
'''

# 18.看代码写结果并解释原因
'''
v1 = '人生苦短，我用Python'
v2 = [1,2,3,4,v1]

v1 = "人生苦短，用毛线Python"

print(v2)
'''

'''
#结果
v2 = [1,2,3,4,'人生苦短，我用Python']
#原因
v2变量的第5个元素引用v1变量的内存地址，当v1变量重新赋值后，在内存中重新开辟了一块空间存放"人生苦短，用毛线Python"，
而这时v2的第五个元素引用的内存地址和内存中的值都没有改变。所以v2没变。
'''

# 19.看代码写结果并解释原因
'''
info = [1,2,3]
userinfo = {'account':info, 'num':info, 'money':info}

info.append(9)
print(userinfo)

info = "题怎么这么多"
print(userinfo)
'''

'''
#结果
userinfo = {'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
userinfo = {'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
#原因
第一次修改info变量内存地址中的值，而字典中的值的内存地址和info变量的内存地址相同，所以第一次打印时字典的值也变化了。
第二次给info变量重新赋值，这时重新开辟一块内存存放info的值，而原来内存中的值没有变化，所以第二次打印结果没变化
'''


# 20.看代码写结果并解释原因
'''
info = [1,2,3]
userinfo = [info,info,info,info,info]

info[0] = '不仅多，还特么难呢'
print(info,userinfo)
'''

'''
#结果
['不仅多，还特么难呢',2,3] [['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3],['不仅多，还特么难呢',2,3]]
#原因
只是修改了info变量内存地址中元素的值，所以userinfo变量的元素也变化了
'''


# 21.看代码写结果并解释原因
'''
info = [1,2,3]
userinfo = [info,info,info,info,info]

userinfo[2][0] = '闭嘴'
print(info,userinfo)
'''

'''
#结果
['闭嘴',2,3] [['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3],['闭嘴',2,3]]
#原因
只是修改了userinfo变量的第三个元素内存地址中内部的值，所以所有引用相同内存地址的变量都变了
'''


# 22.看代码写结果并解释原因
'''
info = [1, 2, 3]
user_list = []
for item in range(10):
    user_list.append(info)

info[1] = "是谁说Python好学的？"

print(user_list)
'''

'''
#结果
[[1, '是谁说Python好学的？', 3],...] （共有10个[1, '是谁说Python好学的？', 3]）
#原因
user_list变量在每次循环添加元素时，引用的都是info变量的内存地址，所以当info变量内存地址中的只改变后，user_list
的所有元素都变了
'''


# 23.看代码写结果并解释原因
'''
data = {}
for item in range(10):
    data['user'] = item
print(data)
'''

'''
#结果
{'user': 9}
#原因
在第一次向data字典中添加元素时，计算机开辟一块内存存放键对应的值的内存地址，之后每次循环键都是指向同一块内存，
内存中保存的值得内存地址没有变化，所以每次循环只是更新值。所以最终结果为最后一次循环的值
'''


# 24.看代码写结果并解释原因
'''
data_list = []
data = {}
for item in range(10):
    data['user'] = item
    data_list.append(data)
print(data_list)
'''

'''
#结果
[{'user':9},...] (10个{'user':9})
#原因
在第一次向data字典中添加元素时，计算机开辟一块内存存放键对应的值的内存地址，之后每次循环键都是指向同一块内存，
内存中保存的值得内存地址没有变化，所以每次循环只是更新值。所以data字典中的值是最后一次循环的值。而data_list
变量的所有元素都是引用的data字典的内存地址。
'''


# 25.看代码写结果并解释原因
'''
data_list = []
for item in range(10):
    data = {}
    data['user'] = item
    data_list.append(data)
print(data_list)
'''

'''
#结果
[{'user': 0}, {'user': 1}, {'user': 2}, {'user': 3}, {'user': 4}, {'user': 5}, {'user': 6}, {'user': 7}, {'user': 8}, {'user': 9}]
#原因
每次循环时都会给data变量重新开辟内存，存放值。所以最终列表中的所有元素指向的内存地址都不相同。
'''