# Day37

## 内容回顾

1. 协程是数据安全的。

2. gevent模块时基于greenlet进行切换的。asyncio是基于yield

3. 在哪些地方用到了线程和协程。

   - 初期自己线程和协程完成爬虫任务
   - 后来有了比较丰富的爬虫框架
     - 了解scrapy/beautyful soup/ainhttp爬虫框架那些用了协程，那些用了线程。

   - web框架中的并发是如何实现的
     - 传统框架：
       - django:多线程
       - flask:优先使用协程，其次使用线程。
       - socketserver：多线程
     - 异步框架：
       - tornado,sanic底层都是协程

4. IPC 进程间通信
   - 内置模块（基于文件）：Queue,Pipe
   - 第三方工具（基于网络）：redis,memcache,kafka,rabbitmq  发挥的都是消息中间件的功能。

## 今日内容

1. 认识数据库，在开发项目中的位置
2. 数据库中的一些基础概念
3. 安装数据库
4. 最基础的一些操作
   - 和用户相关
   - 和数据相关

## 内容详细

### 1.数据库基础概念

#### 1.1 自己管理数据

- 很多功能如果只是通过操作文件来改变数据是非常繁琐的，程序员需要做很多事情。
- 对于多台机器或者多个进程操作一份数据，程序员自己解决并发和安全问题比较麻烦
- 自己处理一些数据备份，容错的措施。

#### 1.2 数据库

1. C/S架构的，操作数据文件的一个管理软件

  - 帮助我们解决并发问题
  - 能够帮助我们用更简单更快速的方式完成数据的增删改查
  - 能够给我们提供一些容错、高可用的机制
  - 权限的认证

2. 数据库管理系统（DBMS），专门用来管理数据文件，帮助用户更简洁的操作数据的软件。
   - 关系型数据库
     - sql server
     - oracle 收费、比较严谨、安全性比较高
       - 国企、事业单位
       - 银行、金融行业
     - mysql 开源的
       - 小公司
       - 互联网公司
     - sqlite
   - 非关系型数据库
     - redis
     - mongodb

3. 数据库管理员（DBA）

### 2.数据库的安装

1. 解压安装包，将my-default.ini文件复制一份，重命名为my.ini，修改my.ini配置文件内容

   ```python
   [mysql] #客户端的配置
   # 设置mysql客户端默认字符集
   default-character-set=utf8 
   
   [mysqld]   #服务端的配置
   #设置3306端口
   port = 3306 
   # 设置mysql的安装目录
   basedir=D:\mysql\mysql-5.6.44-winx64 
   # 设置mysql数据库的数据的存放目录
   datadir=D:\mysql\mysql-5.6.44-winx64\data 
   # 允许最大连接数
   max_connections=200
   # 服务端使用的字符集默认为8比特编码的latin1字符集
   character-set-server=utf8
   # 创建新表时将使用的默认存储引擎
   default-storage-engine=INNODB
   ```

2. 添加环境变量，将安装文件夹下的bin文件夹路径添加到Path环境变量。

3. 以管理员身份运行cmd,分别执行如下指令

   ```python
   mysqld install #安装mysql服务
   net start mysql #启动mysql服务
   mysql -u root -p #登录数据库，用户为root，后面让输入密码，直接回车即可。
   
   net stop mysql #mysql服务没有没有重启命令，只能关闭再重新打开
   
   ```

### 3.数据库语句

数据库语句分为三种：

- 数据库定义语句(DDL)：创建库、创建表等。
- 数据库操纵语句(DML)：数据的增删改查。
- 数据库控制语句(DCL)：数据库权限的控制。

1. 登录数据库

   数据库在安装完成之后，有一个最高权限的用户root。

   ```sql
   --登录本地数据库
   mysql -uroot -p  --mysql -u用户名 -p
   
   --登录远程数据库
   mysql -uroot -h192.168.12.39 -p  --mysql -u用户名 -h远程IP -p
   ```

2. 查看当前用户

   ```sql
   select user();
   ```

3. 设置密码

   ```sql
   set password = password();
   ```

4. 创建用户

   ```sql
   --创建用户wkx，密码为123，只有ip在192.168.12段的ip可以登录该用户
   create user 'wkx'@'192.168.12.%' identified by '123';
   --创建用户wkx,任何ip都可以访问（只要可以访问到服务器），不设置密码
   create user 'wkx'@'%';
   ```

5. 授权

   ```sql
   --设置wkx的权限为所有数据库所有权限
   grant all on * to 'wkx'@'192.168.12.%'; 
   --设置wkx的权限为test数据库下的所有操作权限
   grant all on test.* 'wkx'@'192.168.12.%';
   --设置wkx的权限为test数据库下的查询权限
   grant select on test.* to 'wkx'@'192.168.12.%';
   --设置权限的同时也可以创建用户
   grant all on test.* to 'wkx'@'192.168.12.%' identified by '123';
   ```

6. 查看数据库

   ```sql
   show databases;
   ```

7. 创建数据库

   ```sql
   --创建数据库test
   create database test;
   ```

8. 查看当前使用的数据库

   ```sql
   select database();
   ```

9. 切换数据库

   ```sql
   use test; -- use 数据库  切换到test数据库
   ```

10. 查看当前数据库的表

    ```sql
    show tables ;
    ```

11. 创建表

    ```sql
    --创建student表，表内有两个字段id(int型)和name(char类型最大长度为4)
    create table student(id int, name char(4));
    ```

12. 删除表

    ```sql
    drop table student; --删除student表
    ```

13. 查看表结构

    ```sql
    desc student; --查看student表的表结构
    ```

14. 添加数据

    ```sql
    insert into student values(1,'wusir');
    insert into student (id,name)values(1,'wusir');
    ```

15. 查询数据

    ```python
    select * from student;
    ```

16. 修改数据

    ``` sql
    update student set name='alex' where id=1;
    ```

17. 删除数据

    ```sql
    delete from student where id=1;
    ```

    