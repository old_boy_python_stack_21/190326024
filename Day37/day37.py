#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
第37天作业
王凯旋
'''



# 数据库练习
# 1.创建一个数据库，ftp
'''
create database ftp;
'''


# 2.创建一张表，userinfo表 存储三个字段：id,username,password
'''
create table userinfo(id int, username char(10),password char(30));
'''


# 3.向这张表中插入3条数据
# | 1 | alex | alex3714 |
# | 2 | wusir | 6666 |
# | 3 | yuan | 9999 |
'''
insert into userinfo values(1,'alex','alex3714');
insert into userinfo values(2,'wusir','6666');
insert into userinfo values(3,'yuan','9999');
'''


# 4.查看表中的所有数据
'''
select * from userinfo;
'''


# 5.修改wusir的密码,变成666
'''
update userinfo set password='666' where username='wusir';
'''


# 6.删除yuan这一行信息
'''
delete from userinfo where id=3;
'''


# python练习
# 1.有一个文件，这个文件中有20000行数据，开启一个线程池，为每100行创建一个任务，打印这100行数据。
''''''
from multiprocessing import Lock
from concurrent.futures import ThreadPoolExecutor

def func(f,lock):
     with lock:
        for i in range(100):
            res = f.readline()
            print(res)

f = open('test.txt',mode='r',encoding='utf-8')
lock = Lock()
pool = ThreadPoolExecutor(200)
for i in range(200):
    pool.submit(func,f,lock)
pool.shutdown()

f.close()



# 2.写一个socket 的udp代码，client端发送一个时间格式，server端按照client的格式返回当前时间
# 例如 client发送'%Y-%m-%d'，server端返回'2019-5-20'
'''
import socket,time
sk = socket.socket(type=socket.SOCK_DGRAM)

while True:
    sk.sendto('%Y-%m-%d %H:%M:%S'.encode('utf-8'),('127.0.0.1',8066))
    msg = sk.recv(1024).decode('utf-8')
    print(msg)
    time.sleep(1)
'''

'''
import socket
from datetime import datetime

sk = socket.socket(type=socket.SOCK_DGRAM)
sk.bind(('127.0.0.1',8066))

while True:
    fmt,addr = sk.recvfrom(1024)
    dt = datetime.now()
    msg = dt.strftime(fmt.decode('utf-8'))
    sk.sendto(msg.encode('utf-8'),addr)
'''


# 3.请总结进程、线程、协程的区别。和应用场景。
'''
进程：开销大，数据隔离，可以利用多核，数据不安全。 用于高计算。
线程：开销较小，数据共享，Cpython解释器下不能利用多核，数据不安全，由操作系统进行切换，对操作系统压力大，
对io操作敏感。 django框架，socketserver模块，flask框架
协程：开销最小，数据共享，不能利用多核，数据安全，由用户进行切换，对操作系统无压力，对io操作不如线程敏感。
flask框架，异步框架：sanic,tornado。
'''