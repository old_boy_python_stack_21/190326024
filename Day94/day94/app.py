from flask import Flask
from myblue.users import user

app = Flask(__name__)

app.register_blueprint(user)

if __name__ == '__main__':
    app.run('0.0.0.0', 9527)
