from flask import Blueprint, jsonify, request
from settings import MONGODB

user = Blueprint('user', __name__)


@user.route('/reg', methods=["post"])
def reg():
    try:
        userinfo = request.form.to_dict()
        MONGODB.users.insert_one(userinfo)
        return jsonify({'code': 1, 'msg': '注册成功'})
    except Exception as e:
        return jsonify({'code': 0, 'msg': e})


@user.route('/login', methods=["post"])
def login():
    try:
        userinfo = request.form.to_dict()
        if MONGODB.users.find_one(userinfo):
            return jsonify({'code': 1, 'msg': '登录成功'})
        else:
            return jsonify({'code': 0, 'msg': '用户名或密码错误'})
    except Exception as e:
        return jsonify({'code': 0, 'msg': e})
