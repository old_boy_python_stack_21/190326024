# Day55

## 今日内容

1. 外键的创建
2. 模板语法的条件判断

## 内容详细

### 1.ORM外键创建

```python
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE) #默认关联Publisher表的主键
    #pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)  也可以通过字符串的方式创建 
```

on_delete  在django2.0 版本之后是必填的参数    1.11  之前的可以不填 

on_delete 的参数  

- models.CASCADE   级联删除

- models.SET()   设置指定的值

- models.SET_DEFAULT    设置默认值

  ```python
  pub = models.ForeignKey(Publisher,on_delete=models.SET_DEFAULT,default=0)
  ```

- models.SET_NULL 设置为空

**注意：**

数据库的表在创建时会在model中的外键字段后加上`_id`，在django代码中查询model的外键名得到的是一个关联的表对应类的对象，可以用`外键名_id`来获取数据库中外键的值。

```python
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE) #数据库中外键字段名为pub_id
    
    
book = models.Book.objects.get(bid=1) 
print(book.pub) #得到Publisher对象
print(book.pub_id) #得到外键值
print(book.pub.pid) #得到外键值

  
```



###  2.模板条件判断语法

```html
{% for publisher in publisher_list %}
	{% if book.pub == publisher %}
		<option selected value="{{ publisher.pk }}">{{ publisher.name }}</option>
	{% else %}
		<option value="{{ publisher.pk }}">{{ publisher.name }}</option>
	{% endif %}
{% endfor %}
```

```python
{% if 条件 %}
	操作
{% elif 条件 %} 
	操作
{% else %}
	操作
{% endfor %}
```

