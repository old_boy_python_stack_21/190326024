from django.db import models

class Classes(models.Model):
    cid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    std_num = models.IntegerField()
    start_date = models.DateField()

class Student(models.Model):
    sid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)
    age = models.IntegerField()
    gender = models.CharField(max_length=1)
    cls = models.ForeignKey(Classes,on_delete=models.CASCADE)

