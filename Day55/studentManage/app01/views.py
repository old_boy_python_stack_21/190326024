from django.shortcuts import render,redirect
from app01 import models


#主页
def index(request):
    return render(request,'index.html')

#班级列表
def classes_list(request):
    classes_list = models.Classes.objects.all().order_by('-cid')
    return render(request,'classes/classes_list.html',{'classes_list':classes_list})

#删除班级
def del_classes(request):
    cid = request.GET.get('cid')
    classes_list = models.Classes.objects.filter(cid=cid)
    classes_list.delete()
    return redirect('/classes_list/')

#添加班级
def add_classes(request):
    error = ''
    if request.method == 'POST':
        classes_name = request.POST.get('classes_name')
        std_num = request.POST.get('std_num')
        start_date = request.POST.get('start_date')
        if models.Classes.objects.filter(name=classes_name):
            error = '班级名称已存在'
        if not classes_name:
            error = '班级名称不能为空'
        if not error:
            models.Classes.objects.create(name=classes_name,std_num=std_num,start_date=start_date)
            return redirect('/classes_list/')

    return render(request,'classes/add_classes.html',{'error':error})

#编辑班级
def edit_classes(request):
    error = ''
    cid = request.GET.get('cid')
    clesses = models.Classes.objects.get(cid=cid)
    if request.method == 'POST':
        classes_name = request.POST.get('classes_name')
        std_num = request.POST.get('std_num')
        start_date = request.POST.get('start_date')
        if models.Classes.objects.filter(name=classes_name):
            error = '班级名称已存在'
        if not classes_name:
            error = '班级名称不能为空'
        if not error:
            clesses.name = classes_name
            clesses.std_num = std_num
            clesses.start_date = start_date
            clesses.save()
            return redirect('/classes_list/')
    return render(request,'classes/edit_classes.html',{'error':error,'classes':clesses})


#学生列表
def student_list(request):
    student_list = models.Student.objects.all().order_by('-sid')
    return render(request,'student/student_list.html',{'student_list':student_list})

#删除学生
def del_student(request):
    sid = request.GET.get('sid')
    student = models.Student.objects.filter(sid=sid)
    student.delete()
    return redirect('/student_list/')

#添加学生
def add_student(request):
    error =''
    classes_list = models.Classes.objects.all().order_by('-cid')
    if request.method == 'POST':
        student_name = request.POST.get('student_name')
        student_age = request.POST.get('student_age')
        student_gender = request.POST.get('student_gender')
        classes = request.POST.get('classes')
        if not student_name:
            error = '学生姓名不能为空'
        if not student_age:
            error = '学生年龄不能为空'
        if not error:
            models.Student.objects.create(name=student_name,age=student_age,gender=student_gender,cls_id=classes)
            return redirect('/student_list/')
    return render(request,'student/add_student.html',{'error':error,'classes_list':classes_list})

#编辑学生
def edit_student(request):
    error = ''
    sid = request.GET.get('sid')
    student = models.Student.objects.get(sid=sid)
    classes_list = models.Classes.objects.all().order_by('-cid')
    if request.method == 'POST':
        student_name = request.POST.get('student_name')
        student_age = request.POST.get('student_age')
        student_gender = request.POST.get('student_gender')
        classes = request.POST.get('classes')
        if student_name == student.name:
            error = '学生姓名与之前相同'
        if not student_name:
            error = '学生姓名不能为空'
        if not student_age:
            error = '学生年龄不能为空'
        if not error:
            student.name = student_name
            student.age = student_age
            student.gender = student_gender
            student.cls_id = classes
            student.save()
            return redirect('/student_list/')
    return render(request,'student/edit_student.html',{'error':error,'student':student,'classes_list':classes_list})
