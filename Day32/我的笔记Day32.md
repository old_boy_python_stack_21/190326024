# Day32

## 今日内容

1. 线程和进程
2. muitiprocessing包
   - Process类，完成进程的开启，销毁，基础操作。

## 内容详细

### 1.线程和进程

#### 1.1 线程

- 线程是计算机中能被CPU调度的最小单位（负责执行代码）。

- 线程是进程的一部分，一个进程至少含有一个线程
- 线程的数据共享
- 线程的创建，销毁，切换开销小

#### 1.2 进程

- 进程是计算机资源分配的最小单位（负责圈资源）。
- 进程的数据是隔离的
- 进程的开销

### 2.多进程

#### 2.1 基础知识

1. 子进程

   在父进程中创建子进程。在pycharm中启动的所有py程序都是pycharm的子进程。

2. 获取进程pid

   ```python
   import os
   os.getpid()#获取当前进程的pid
   os.getppid()#获取父进程的pid
   ```

3. 主进程的结束逻辑

- 主进程的代码结束
- 所有的子进程的代码结束
- 主进程回收子进程资源
- 主进程结束

4. 主进程如何知道子进程结束？

   基于网络、文件。

#### 2.2 Process类

Process类在multiprocessing包中。

```python
import os
import time
from multiprocessing import Process
def func():
    print('start',os.getpid())
    time.sleep(1)
    print('end',os.getpid())

if __name__ == '__main__':
    p = Process(target=func)
    p.start()#异步调用开启进程的方法，但并不等待这个进程真的开启(异步非阻塞)
    print('main:',os.getpid())
```

不同操作系统创建进程的方式不同

- windows操作系统开启进程的代码
  - 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作。
  - 所以有一些内容我们只希望在父进程中完成，就写在`if __name__ == '__main__':`中。
- ios、linux操作系统创建进程 fork, 不执行代码,直接执行调用的func函数

join放法：阻塞父线程，直到子进程结束。

例：等10个子进程全部执行完毕

```python
import time
from multiprocessing import Process

def func(i):
    time.sleep(1)
    print(i)

if __name__ == '__main__':
    p_l = []
    for i in range(10):
        p = Process(target=func,args=(i,))
        p.start()
        p_l.append(p)

    for i in p_l:
        i.join()
    print('执行完毕')
```





