#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import shutil
import hashlib
import socketserver
from lib.encrypt import encrypt
from config import settings
from lib.file_oper import read_file
from src.tcpbase import TcpBase
from lib.jsonhelper import json_from_file,json_to_file

class Tcpsvr(TcpBase):

    def __init__(self):
        self.conn = None
        self.user_name = ''
        self.msg = None

    def base(arg):
        def inner(self):
            if self.user_name:
                self.base_send({'method':self.msg['method'],'code':1,'info':''})
                arg(self)
            else:
                self.base_send({'method':self.msg['method'],'code':0,'info':'请先进行登录'})
        return inner

    def create_dir(self):
        '''
        创建文件夹
        :return:
        '''
        create_path = self.msg['create_path'].strip()
        dir_name = self.msg['dir_name'].strip()
        path = os.path.join(settings.BASE_PATH, 'filedb', create_path, dir_name)
        os.makedirs(path)

    def to_up(self):
        '''
        返回上一层
        :return:
        '''
        pass

    def del_dir_file(self):
        '''
        删除文件或文件夹
        :return:
        '''
        del_path = self.msg['del_path']
        del_path = os.path.join(settings.BASE_PATH, 'filedb', del_path)
        if os.path.isfile(del_path):
            os.remove(del_path)
        else:
            shutil.rmtree(del_path)

    def to_down(self):
        '''
        进入下一层目录
        :return:
        '''
        pass

    def login(self):
        '''
        登录
        :return:
        '''
        dic = self.msg
        pwd = encrypt(dic['user_pwd'])
        user_list = json_from_file(settings.USER_INFO_PATH)
        for i in user_list:
            if dic['user_name'] == i['user_name'] and pwd == i['user_pwd']:
                self.base_send({'method':'login','code':1,'info':'登录成功'})
                self.user_name = dic['user_name']
                break
        else:
            self.base_send({'method':'login','code':0,'info':'用户名或密码错误，请重新输入'})

    def register(self):
        '''
        注册
        :return:
        '''
        user_list = json_from_file(settings.USER_INFO_PATH)
        for i in user_list:
            if i['user_name'] == self.msg['user_name']:
                self.base_send({'method':'register','code':0,'info':'用户名已存在，请重新输入'})
                break
        else:
            user_list.append({'user_name':self.msg['user_name'],'user_pwd':encrypt(self.msg['user_pwd'])})
            json_to_file(user_list,settings.USER_INFO_PATH)
            os.makedirs(os.path.join(settings.BASE_PATH,'filedb',self.msg['user_name']))
            self.base_send({'method':'register','code':1,'info':'注册成功'})

    def flie_list(self):
        '''
        获取文件列表
        :return:
        '''
        path = os.path.join(settings.BASE_PATH,'filedb',self.msg['path'])
        dir_list = os.listdir(path)
        if dir_list:
            dic = {'method':'flie_list','dir_dict':{self.msg['path']:dir_list},'is_null':0}
        else:
            dic = {'method':'flie_list','dir_dict':{self.msg['path']:dir_list},'is_null':1}
        self.base_send(dic)

    def upload(self):
        '''
        上传文件
        :return:
        '''
        if self.msg['is_return']:
            return
        file_size = self.msg['file_size']
        file_name = self.msg['file_name']
        file_path = self.msg['save_path']
        save_path = os.path.join(settings.BASE_PATH, 'filedb', file_path, file_name)
        f = open(save_path, mode='wb')
        read_size = 0
        obj = hashlib.md5()
        while read_size < file_size:
            msg = self.conn.recv(1024 * 1024)
            obj.update(msg)
            read_size += len(msg)
            f.write(msg)
            f.flush()
        f.close()
        msg = self.base_recv()['info']
        if msg == obj.hexdigest():
            self.base_send({'method': 'upload', 'code': 1, 'info': '文件上传成功！'})
        else:
            self.base_send({'method': 'upload', 'code': 0, 'info': '文件上传失败！'})

    def download(self):
        '''
        下载文件
        :return:
        '''
        if self.msg['is_return']:
            return
        file_path = os.path.join(settings.BASE_PATH,'filedb',self.msg['file_path'])
        file_name = os.path.basename(file_path)
        file_size = os.path.getsize(file_path)
        self.base_send({'file_name':file_name,'file_size':file_size})
        f = read_file(file_path)
        obj = hashlib.md5()
        for i in f:
            obj.update(i)
            self.conn.send(i)
        msg = self.base_recv()['info']
        if msg == obj.hexdigest():
            self.base_send({'method':'download','code':1,'info':'文件下载成功！'})
        else:
            self.base_send({'method':'download','code':0,'info':'文件下载失败！'})

    def exit(self):
        '''
        退出
        :return:
        '''
        self.conn.close()
        self.user_name = ''
        self.conn = None

    def start(self,conn):
        '''
        启动
        :param conn: 客户端连接对象
        :return:
        '''
        self.conn = conn
        while self.conn:
            dic = self.base_recv()
            self.msg = dic
            getattr(self,dic['method'])()



class MyServer(socketserver.BaseRequestHandler):
    def handle(self):
        tcp_cli = Tcpsvr()
        tcp_cli.start(self.request)

def run():
    sk = socketserver.ThreadingTCPServer((settings.SERVER_IP, settings.SERVER_PORT), MyServer)
    sk.serve_forever()