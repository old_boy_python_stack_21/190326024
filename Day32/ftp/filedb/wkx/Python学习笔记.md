# Python学习笔记

## 一、计算机基础

### 1.1 硬件

​	主板、CPU、硬盘、显卡、网卡、内存...

### 1.2 操作系统

- windows 图形界面好
  - window 7
  - window 10
- linux  开源免费
  - centos(公司线上使用版本)
  - redhat
  - ubuntu
- mac 办公方便

### 1.3 解释器和编译器

语言开发者编写的一种软件，将用户写的代码转换成二进制，交给计算机执行

#### 1.3.1 解释型语言和编译型语言的区别

- 解释型语言：将编写好的代码交给解释器，解释器解释一句就执行语句。如：Python、PHP
- 编译型语言：将编写好的代码交给编译器编译成新的文件，然后交给计算机执行。如：C/C++、Java

### 1.4 软件

应用程序

### 1.5 进制

- 二进制：文件存储，网络传输都是二进制
- 八进制：
- 十进制：人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。
- 十六进制：多用来表示复杂的东西。\x

## 二、Python入门

### 2.1 环境

#### 2.1.1 解释器环境

​	python2、python3

#### 2.1.2 代码开发工具（IDE）

​	PyCharm

### 2.2 编码

- ascii:一个字节表示一个字符
- unicode：万国码可以表示所有字符
  - ecs2:2个字节表示一个字符
  - ecs4:4个字节表示一个字符
- utf-8:Unicode的压缩版，3个字节表示一个中文字符
- gbk:亚洲字符编码，两个字节表示一个字符
- gb2312:2个字节表示一个字符

### 2.3 变量

2.3.1 为什么要定义变量？

​	方便以后代码调用

2.3.2 变量命名规范

- 只能包括数字、字母以及下划线。
- 不能以数字开头
- 不能是python的关键字
- 建议：见名知意，多个单词用下划线隔开

### 2.4 条件判断

```python
if 1>2:
    print()
elif 1<2:
    print()
else:
	print()
```

### 2.5 while循环

```python
while True:
    print()
```

- while
- break:结束当前循环
- continue:结束本次循环，回到循环条件判断处
- pass:跳过
- while  else:当while的条件不满足时，进入else。break掉的循环不会进入else。

### 2.6 运算符

- 算数运算符

  ```python
  +,-,*,/,%,**,//
  ```

- 比较运算符

  ```python
  ==,>=,<=,!=,>,<
  ```

- 赋值运算符

  ```python
  =,+=,-=,*=,/=,**=,//=,%=
  ```

- 逻辑运算符

  - not

  - and

    ```python
    #当第一值为假，取第一值
    #当第一值为真，取第二值
    #有多个判断时，按照上述规则，从左到右依次比较
    1 and 2
    ```

  - or

    ```python
    #当第一值为假，取第二值
    #当第一值为真，取第一值
    #有多个判断时，按照上述规则，从左到右依次比较
    1 or 2
    ```

  优先级：not>and>or

- 成员运算符

  - in 
  - not in

### 2.7 注释

```python
#单行注释

'''
多行注释
'''
```

### 2.8 单位

```python 
1 byte = 8 bit
1 kb = 1024 byte
1 Mb = 1024 kb
...
```

### 2.9 range()

```python
#产生一组数字（高仿列表）
value = range(0,9) #不包括9
```



### 2.10 py2和py3的区别

- 类型

  - py2:unicode类型（以unicode编码），str类型（其他编码）	

    ```python
    v = u"alex" #定义了unicode类型的变量
    v2 = "alex" #定义str类型的变量
    ```

  - py3：str类型(unicode编码) ，bytes类型（其他编码）

    ```python
    v1 = 'alex' #定义了一个str类型的变量，在内存中unicode编码
    v2 = b'alex' #定义了一个bytes类型变量
    ```

- 字典的keys,values,items

  - py2：返回列表
  - py3：返回迭代器

- map和filter

  - py2：返回列表
  - py3：返回迭代器

- 默认解释器编码

  - py2:ascii
  - py3:utf-8

- 输入
  - py2: raw_input()
  - py3: input()

- 输出
  - py2: print ""
  - py3: print()

- 整数相除
  - py2: 只保留整数位
  - py3: 全部保留

- 整型
  - py2: int/long
  - py3:只有int

- range()和xrange()
  - py2
    - range()  立即在内存中生成所有元素
    - xrange()  得到一个生成器，在循环的时候边循环边创建，节省内存
  - py3
    - range() 得到一个生成器，在循环的时候边循环边创建，节省内存

- 创建包
  - py2:文件夹下面必须有`__init__.py`文件才能算包
  - py3:不必有`__init__.py`文件

- 内置函数变化，如
  - py2：reduce是内置函数
  - py3：reduce被放入functools模块中，需导入模块后使用

- 类定义

  ```python
  class Foo:
  	pass
  
  class Foo(object):
  	pass
  # 在python3中这俩的写法是一样，因为所有的类默认都会继承object类，全部都是新式类。
  # 如果在python2中这样定义，则称其为：经典类
  class Foo:
  	pass
  # 如果在python2中这样定义，则称其为：新式类
  class Foo(object):
  	pass
  ```

  

### 2.11 三元运算符（三目运算符）

前面 if 表达式 else 后面

```python
#表达式为True取前面的值，否则取后面的值
v =  a if a > b else b
```

### 2.12 推导式

#### 2.12.1 列表推导式

目的：更加便捷的生成一个列表

```python
v1 = [i for i in 'alex'] #['a','l','e','x']
v2 = [66 if i > 5 else 77 for i in range(10)] #[77,77,77,77,77,77,66,66,66,66]
v3 = [i for i in range(10) if i > 5] #[6,7,8,9]  条件成立添加到列表
```

基本格式

```python
v1 = [i for i in 可迭代对象]
v2 = [i for i in 可迭代对象 if 条件] #条件成立添加到列表中
```

面试题

```python
def num():
    return [lambda x:x*i for i in range(4)]
print([m(2) for m in num()]) #[6,6,6,6]
```

#### 2.12.2 集合推导式

基本格式

```python
v1 = {i for i in 可迭代对象}
v2 = {i for i in 可迭代对象 if 条件} #条件成立添加到列表中
```

#### 2.12.3 字典推导式

基本格式

```python
v1 = {'k'+str(i):i for i in 可迭代对象}
v2 = {'k'+str(i):i for i in 可迭代对象 if 条件} #条件成立添加到列表中
```

#### 2.12.4 生成器推导式

```python
#基本格式
v = (i for i in range(10))
for item in v:
    print(v)
    
# 示例一
# def func():
#     result = []
#     for i in range(10):
#         result.append(i)
#     return result
# v1 = func()
# for item in v1:
#    print(item)

# 示例二
# def func():
#     for i in range(10):
#         def f():
#             return i
#         yield f
#
# v1 = func()
# for item in v1:
#     print(item())

# 示例三：
v1 = [i for i in range(10)] # 列表推导式，立即循环创建所有元素。
v2 = (lambda :i for i in range(10))
for item in v2:
    print(item())
```

面试题：请比较 [i for i in range(10)] 和 (i for i in range(10)) 的区别？

## 三、数据类型

### 3.1 整型（int）

### 3.2 布尔类型（bool）

0, "", [], (), {}, set(), None转换成为布尔值时为False,其他全部为True.

### 3.3  字符串（str）

- upper() 转大写

  ```python
  #upper()转换大写
  v1 = "alex"
  v2 = v1.upper()
  print(v2) #ALEX
  ```

- lower() 转小写

  ```python
  #lower()转换小写
  v1 = "ALEX"
  v2 = v1.upper()
  print(v2)  #alex
  ```

- isdecimal() 是否为十进制数

  ```python
  v1 = "12345"
  print(v1.isdecimal()) #True
  ```

- strip() 去空格

  ```python
  v1 = " alex "
  print(v1.strip()) #alex
  ```

- replace() 替换

  ```python
  v1 = "aaaa111"
  print(v1.replace("111","bbb")) #aaaabbb
  ```

- split() 分割

  ```python
  v1 = "aaaa|bbbb"
  print(v1.split("|")) #['aaaa', 'bbbb']
  ```

- format() 格式化

  ```python
  v1 = "他叫{0}，今年{1}岁"
  print(v1.format("张三",18)) #他叫张三，今年18岁
  ```

- startswith() 以...开头

  ```python
  #startswith()
  v1 = "abc"
  print(v1.startswith("a"))#True
  ```

- endswith()  以...结尾

  ```python
  #endswith()
  v1 = "abc"
  print(v1.endswith("a"))#False
  ```

- encode()  编码

  ```python
  v1 = "你好"
  print(v1.encode("utf-8"))# b'\xe4\xbd\xa0\xe5\xa5\xbd'
  ```

- join() 连接

  ```python
  v1 = "你好"
  print("_".join(v1))# 你_好
  ```

- 字符串格式化

  ```python
  v = '姓名是：%(name)s,年龄：%(age)s'%{'name':'alex','age':19}
  print(v) #姓名是：alex,年龄：19
  
  v = '姓名是：{0},年龄：{1}'.format('alex',19)
  print(v) #姓名是：alex,年龄：19
  
  v = '姓名是：{name},年龄：{age}'.format(name='alex',age=19)
  print(v) #姓名是：alex,年龄：19
  ```

### 3.4 列表（list）

- append()  追加

  ```python
  v1 = ["111","222","333","444","555"]
  v1.append("666")
  print(v1)# ['111', '222', '333', '444', '555', '666']
  ```

- insert() 插入

  ```python
  v1 = ["111","222","333","444","555"]
  v1.insert(1,"666")
  print(v1)# ['111', '666', '222', '333', '444', '555']
  ```

- extend()  扩展

  ```python
  v1 = ["111","222","333","444","555"]
  v1.extend(['777','888'])
  print(v1) #['111', '222', '333', '444', '555', '777', '888']
  ```

- pop() 删除

  ```python
  v1 = ["111","222","333","444","555"]
  v1.pop(1)
  print(v1) #['111', '333', '444', '555']
  ```

- remove() 移除

  ```python
  v1 = ["111","222","333","444","555"]
  v1.remove("333")
  print(v1)#['111', '222', '444', '555']
  ```

- clear() 清空

  ```python
  v1 = ["111","222","333","444","555"]
  v1.clear()
  print(v1) #[]
  ```

- reverse() 反转

  ```python
  v1 = ["111","222","333","444","555"]
  v1.reverse()
  print(v1)#['555', '444', '333', '222', '111']
  ```

- sort() 排序

  ```python
  v1 = [1,3,2,6,4]
  v1.sort(reverse=False)
  print(v1)#[1, 2, 3, 4, 6]
  ```

### 3.5 元组（tuple）

​	无

### 3.6 字典（dict）

- keys() 获取所有键

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.keys():
      print(i)
  #name
  #age
  ```

- values() 获取所有值

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.values():
      print(i)
  #alex
  #18
  ```

- items() 获取所有键值对

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.items():
      print(i)
  
  #('name', 'alex')
  #('age', 18)
  ```

- get() 根据键获取值

  ```python
  v1 = {"name":"alex","age":18}
  print(v1.get("name"))#alex
  ```

- update() 更新

  ```python
  v1 = {"name":"alex","age":18}
  v1.update({"gender":"男"})
  print(v1)# {'name': 'alex', 'age': 18, 'gender': '男'}
  ```

- pop() 根据键删除

  ```python
  v1 = {"name":"alex","age":18}
  v1.pop("name")
  print(v1)# {'age': 18}
  ```

- 有序字典

  ```python
  from collections import OrderedDict
  info = OrderedDict()
  info['k1'] = 1
  info['k2'] = 2
  ```

### 3.7 集合（set）

集合元素为可哈希类型（不可变），但集合本身不可哈希。

- add() 添加

  ```python
  v1 = {1,2,3,4}
  v1.add(5)
  print(v1)# {1, 2, 3, 4, 5}
  ```

- discard() 删除

  ```python
  v1 = {1,2,3,4}
  v1.discard(4)
  print(v1)#{1, 2, 3}
  ```

- update() 批量添加

  ```python
  v1 = {1,2,3,4}
  v1.update((6,7,8))
  print(v1)#{1, 2, 3, 4, 6, 7, 8}
  ```

- intersection() 交集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.intersection(v2)
  print(v3)#{3, 4}
  ```

- union() 并集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.union(v2)
  print(v3)#{1, 2, 3, 4, 5, 6}
  ```

- difference() 差集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.difference(v2)
  print(v3)# {1, 2}
  ```

### 3.8 公共功能

- len() 获取长度

  - str
  - list
  - tuple
  - dict
  - set

- 索引

  - str
  - list
  - tuple

- 切片

  - str
  - list
  - tuple

- 步长

  - str
  - list
  - tuple

- for循环

  - str
  - list
  - tuple
  - dict
  - set

- 删除

  ```python
  del v[1]
  ```

  - list
  - dict (根据键删除)

- 修改

  - list
  - dict (根据键删除)

### 3.9 内存及深浅拷贝

#### 3.9.1 内存

- 为变量重新赋值时，计算机会为变量重新开辟内存。
- 修改变量内部元素时，不会重新开辟内存。

python的小数据池：

- 常用数字：-5~256，重新赋值时，不会开辟内存
- 字符串：只有带特殊字符，且*num，num>1，重新赋值会开辟内存

#### 3.9.2 深浅拷贝

```python
import copy
copy.copy() #浅拷贝 只拷贝第一层，里面元素地址相同
copy.deepcopy() #深拷贝 寻找所有的可变类型，拷贝
```

- 对于int，bool，str深浅拷贝相同
- 对于list，dict，set
  - 当无嵌套时，深浅拷贝相同。最外层地址不同
  - 当有嵌套时，深拷贝的内部嵌套地址不再相同

- 对于元组
  - 无嵌套，深浅拷贝元组的最外层地址相同
  - 当嵌套有可变类型时，深拷贝的地址不再相同

#### 3.9.3 type()

获取变量的类型

#### 3.9.4 id()

获取变量的内存地址

#### 3.9.5 == 和 is

- == 比较两个变量的值是否相等
- is 比较两个变量的内存是否相同

## 四、文件操作

### 4.1 文件操作步骤

- 打开文件

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  ```

- 操作文件

  ```python
  f.read()
  ```

- 关闭文件

  ```python
  f.close()
  ```

### 4.2 文件打开模式

读和写都会在光标位置开始。

- r/w/a
  - r：只读，文件不存在报错
  - w：只写，文件不存在会创建文件，在文件打开时会清空文件
  - a：追加，文件不存在会创建
- r+/w+/a+
  - r+：可读可写，打开文件光标在文件开头，写入会覆盖光标后面的字符。
  - w+：可读可写，打开文件时会清空文件，可通过移动光标来读取。
  - a+：可读可写，打开文件时光标在文件末尾，可通过移动光标读取，但只要写入光标就会移动到末尾。
- rb/wb/ab ：以二进制打开，不能指定编码，否则报错。多用于图片、视频、音频的读取。
- r+b/w+b/a+b

### 4.3 文件操作方法

- read() 

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  f.read()#将文件的所有内容一次读入内存
  f.read(1)#读取一个字符
  ```

- readlines()

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  lst = f.readlines()#将文件的所有内容一次读入内存,并按行放入列表中
  ```

- seek() 移动光标

  ```python
  seek(0) #将光标移动到开头
  seek(1) #将光标向后移动一个字节
  ```

- tell()  获取光标位置

- flush() 将内存中的数据强制写入硬盘文件

- 修改特别大的文件

  ```python
  with open("test.txt",mode="r",encoding="utf-8") as f1,open("test1.txt",mode="r",encoding="utf-8") as f2:
      for line in f1:
          content = line.replace("123","456")
          f2.write(content)
  ```

  

### 4.4 自动释放文件句柄

- with ... as....

  ```python
  with open("test.txt",mode="r",encoding="utf-8") as f:
      f.read
  #待缩进内的代码执行完成后，文件句柄会自动被释放
  ```

## 五、函数

截止目前，都是面向过程编程（可读性差/可重用性差）

对函数编程：

- 本质：将N行代码那到别处起个名字，以后通过名字就可以找到执行这段代码。
- 场景：
  - 代码重复执行
  - 代码量超过一屏，可以通过函数对代码进行分割。

函数执行：

- 函数和函数的执行不会相互影响，函数内部执行相互之间不会混乱
- 函数的销毁条件
  - 函数执行完成
  - 函数内部数据无人使用

### 5.1 基本结构

```python
def 函数名():
    #函数内容

#函数调用
函数名()
```

注意：函数如果不调用，将永远不会执行。

### 5.2 参数

#### 5.2.1 形参和实参

```python
def get_sum(a,b)  #这里ab为形参
	pass

v1 = 3
v2 = 4
get_sum(v1,v2)#这里的v1和v2为实参
```

#### 5.2.2 参数基本知识

- 任意类型
- 任意个数

#### 5.2.3 位置传参（调用函数时）

```python
def func(a1,a2,a3)：
	print(a1,a2,a3)
func(1,2,3)#按参数位置传递，一一对应
```

#### 5.2.4 关键字传参（调用函数时）

```python
def func(a1,a2,a3)：
	print(a1,a2,a3)
func(a1=1,a3=2,a2=3)#按参数名赋值
```

- 位置传参必须在关键字传参之前

  ```python
  def func(a1,a2,a3)：
  	print(a1,a2,a3)
  func(1,a3=2,a2=3)#正确
  func(a1=1,a2=2,3)#报错
  ```

#### 5.2.5 默认参数（定义函数时）

```python
def func(a1,a2,a3=3)：  #非默认参数必须在默认参数前
	print(a1,a2,a3)

func(a1=1,a2=2)#默认参数可以不传，不传的话使用默认值
```

默认参数的默认值慎用可变类型

```python
def func(data,value=[]):   #不推荐
    pass

def func(data,value=None): #推荐
    if not value:
        value = []
```

```python
def func(data,value=[]): #value在函数加载时就被定义出来 
    value.append(data)
    return value

v1 = func(1) 
v2 = func(1,[11,22,33])
v3 = func(2)
print(v1,v2,v3)  #[1,2]  [11,22,33,1] [1,2]
```

#### 5.2.6 万能参数（定义函数时）

- 第一种

  ```python
  def func(*args): #不支持关键字传参，只能用位置传参，可以传入任意个位置参数，会转换成元组
  	print(args)
  
  func(1,2,3,4,5) #转换成元组(1,2,3,4,5)
  func((1,2,3,4,5))#会转换成元组((1,2,3,4,5),)
  func(*(1,2,3,4,5))#会循环形成元组(1,2,3,4,5)， 也可以是列表、集合
  ```

  ```python
  def func(a1,*args,a2):
      print(a1,args,a2)
  func(1,2,3,4,5,a2=10)#会将1传给a1,2,3,4,5传给args,a2必须用关键字传参	
  ```

- 第二种

  ```python
  def func(**kwargs): #不支持位置传参，只支持关键字传参，可以传入任意个关键字参数，会转换成字典
      print(kwargs)
      
  func(k1=123,k2=456)#转换为{'k1':123,'k2':456}
  func(**{'k1':123,'k2':456}) #将字典复制给kwargs
  ```

#### 5.2.7 综合应用

```python
def func(*args,**kwargs):
    print(args,kwargs)
    
func(1,2,3,4,k1='alex',k2=123)#(1,2,3,4)  {'k1':'alex','k2':123}
```

#### 5.2.8参数重点

```python
def func(a1,a2):
    pass

def func(a1,a2=None):
    pass

def func(*args,**kwargs):
	pass
```

### 5.3 返回值

- 在函数里，遇到return后后面的代码不再执行
- 不写的话，默认返回None

```python
def get_sum(a,b):
	return a+b

get_sum(2,3)
```

### 5.4 作用域

1. 在python中：
   - py文件：全局作用域
   - 函数：局部作用域，一个函数一个作用域

2. 作用域内的数据归自己所有，别人访问不到。局部作用域可以访问父级作用域的数据。

3. 作用域中数据查找规则：首先在自己的作用域内查找，自己的作用域内不存在就去父级作用域查找，直到全局作用域。

4. 子作用域只能找到父级作用域内的变量，默认不能为父级作用域的变量重新赋值。但可以修改可变类型（列表，字典，集合）内部的值。

5. 可以使用global关键字为全局作用域的变量重新赋值

   ```python
   name = 'oldboy'
   def func():
       global name
       name = 'alex'
   func()
   print(name) #alex
   ```

6. 可以使用nonlocal关键字为父级的变量重新赋值

   ```python
   name = 'oldboy'
   def func():
       name = 'alex'
       def func1():
   		nonlocal name
           name = "wusir"
       func1()
       print(name)
   func() #打印wusir
   ```

### 5.5 函数高级知识

#### 5.5.1 函数赋值

- 函数名当做变量使用

- 函数名可以放入集合中，也可当做字典的键

  ```python
  def func():
      return 123
  v = func
  print(v()) #123
  ```

  ```python
  def func():
      print(123)
  func_list = [func,func,func]
  func_list[0]() #123
  func_list[1]() #123
  func_list[2]() #123
  ```

#### 5.5.2 函数作参数传递

```python
def func(arg):
	arg()
def func1():
    print(123)
func(func1) #123
```

#### 5.5.3 函数作返回值

```python
def func()
	print(123)
def bar():
    return func
result = bar()
result() #123
```

```python
name = "oldboy"
def func():
	print(name)

def bar():
    return func

result = bar()
result() #oldboy
```

```python
def func()
	def inner():
        print(123)
    return inner

result = func()
result() #123
```

```python
name = 'oldboy'
def func():
    name = 'eric'
    def inner():
        print(name)
    return inner
v = func()
v() #eric
```

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

#### 5.5.4 闭包

为函数开辟一块内存空间（内部变量供自己使用），为它以后执行时提供数据。

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

练习题

```python
name = 'oldboy'
def func():
    print(name)
def bar()
	name = 'alex'
    return func
v = bar()
v() #oldboy
```

```python
name = 'oldboy'
def func():
	name = 'alex'
    def inner():
        print(name)
    return inner
v = func()
v() #alex     
#注意：看函数是在哪里定义的
```

面试题

```python
info = []

def func(name):
	def inner():
		print(name)
    return inner

for item in range(10):
    info.append(func(item)

info[0]() #0 闭包{name：0 inner}
info[2]() #2 闭包{name：2 inner}
info[4]() #4 闭包{name：4 inner}
```

闭包应用场景：装饰器、SQLAlchemy源码

注意：闭包：封装值+内层函数使用

```python
#不是闭包
def func(name):
    def inner():
        return 123
    return inner

#是闭包
def func(name):
    def inner():
        return name
    return inner
```

#### 5.5.5 高阶函数

- 把函数当做参数传递
- 把函数当做返回值返回

### 5.6 lambda表达式(匿名函数)

为解决简单函数的情况，如：

```python
def func(a1,a2):
	return a1+a2
#等同于
v = lambda a1,a2:a1+a2
v(1,2)#3
```

```python
lst = []
func = lambda a1:lst.append(a1)
print(func(666)) #None
print(lst)#[666]
```

- lambda表达式的几种形式

  ```python
  func1 = lambda : 100
  
  func2 = lambda a1,a2:a1+a2
  
  func3 = lambda *args,**kwargs: print(args)
  
  DATA = 100
  func4 = lambda a1: a1+DATA  #只有一行代码，不能自己创建变量，只能去父级寻找
  ```

  

### 5.7 内置函数

#### 5.7.1 类型强转

- int()
- bool()
- str()
- list()
- tuple()
- dict()
- set()

#### 5.7.2 输入输出

- print()
- input()

#### 5.7.3 数学相关

- abs() 取绝对值

- max()  取最大值

  ```python
  print(max([11,22,66,754,78]))#754
  print(max(2,3))#3
  ```

- min()  取最小值

- sum()  求和

  ```python
  print(sum([1,2,3,4]))#10
  ```

- float()   转浮点类型

- divmod()  取两数相除的商和余数(可用于分页)

  ```python
  print(divmod(10,3)) #(3,1)
  print(divmod(3.5,2)) #(1.0,1.5)
  ```

- pow() 求指数

  ```python
  print(pow(2,3)) #2**3 = 8
  ```

  

- round()  取保留小数位

  取距离自身最近的整数，如果最后一位为5

  - 未指定第二个参数（保留位数），则取距离最近的偶数

    ```python
    print(round(1.5))#2
    print(round(2.5))#2
    ```

  - 若指定了第二个参数，若取舍位前一位为奇数，则直接舍弃取舍位；若为偶数则向上取舍

    ```python
    print(round(2.675,2))#2.67
    print(round(2.665,2))#2.67
    ```

#### 5.7.4 进制转换

- bin()   将十进制转换成二进制

  ```python
  print(bin(15))  #0b1111
  ```

- oct()   将十进制转换成八进制

  ```python
  print(oct(15))  #0o17
  ```

- hex()  将十进制转换成十六进制

  ```python
  print(hex(15)) #0xf
  ```

- int()    将其他进制转换成十进制

  ```python
  v1 = '0b11111111'
  print(int(v1,base=2)) #255
  
  v2 = '77'
  print(int(v2,base=8)) #63
  
  v3 = 'ff'
  print(int(v3,base=16))#255
  ```

  - base默认值为10

#### 5.7.5 编码相关

- chr()  将十进制整数转换成unicode编码的字符

  ```python
  v = 65
  print(chr(v)) #A
  ```

- ord() 将字符转换成Unicode编码对应的十进制整数

  ```python
  v = 'a'
  print(ord(v))#97
  ```

#### 5.7.6 高级内置函数

- map(函数,可迭代对象)    映射    输入元素数量 = 输出元素数量 ，且一一对应。

  ```python
  def func(a):
      return a+100
  
  lst = [1,2,3,4]
  result = map(func,lst)  #内部会遍历lst,将每个元素传给函数，然后将返回值放入新列表。不会影响原列表
  print(result)  # [101,102,103,104] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

  ```python
  #计算两个列表对应元素相加
  lst1 = [1,2,3,4,5]
  lst2 = [10,9,8,7,6]
  result = map(lambda x,y:x+y,lst1,lst2)
  print(list(result)) #[11, 11, 11, 11, 11]
  ```

- fliter(函数,可迭代对象)    筛选    输入元素数量 >= 输出元素数量

  ```python
  def func(a):
      return a > 3
  lst = [1,2,3,4,5]
  result = filter(func,lst) #内部会遍历lst,将每个元素传给函数，然后根据函数返回的真假确定是否添加到新列表。不会影响原列表
  print(result)  # [4,5] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

- reduce(函数,可迭代对象)    多对一  （py3把该函数放入functools模块中，需导入模块后使用）

  ```python
  import functools
  def func(x,y):       #函数参数必须为两个
      return x + y
  lst = [1,2,3,4,5]
  result = functools.reduce(func,lst)#内部会遍历lst，第一次循环时传入前两个元素，后面每次循环传入上次循环函数的返回值和下一个元素
  print(result) #15
  ```

#### 5.7.7 其他

- len()
- open()
- range()
- id()
- type()

#### 5.7.8 isinstance(obj,class)

查看obj是否是class或class的基类的实例

    ```python
    class Base(object):
        pass
    
    class Foo(Base):
        pass
    
    obj = Foo()
    
    print(type(obj) == Base) #False
    print(isinstance(obj,Base)) #True
    ```

#### 5.7.9 issubclass(class,class)  

查看一个类是否是另一个类的子类

    ```python
    class Base(object):
        pass
    
    class Foo(Base):
        pass
    
    class Bar(Foo):
        pass
    
    print(issubclass(Bar,Base))#True
    ```

### 5.8 装饰器

#### 5.8.1 原理示例

```python
def func():
    print(1)
    
def bar():
    print(2)
    
bar = func
bar()  #将func函数的地址赋值给了bar  结果：1
```

```python
def func(arg):
    def inner():
        print(arg)
    return inner
v1 = func(1)
v2 = func(2)
v1() #1
v2() #2
```

```python
def func(arg):
    def inner():
        return arg()
    return inner
def index():
    print(123)
    return 666
v1 = func(index) #将inner函数地址赋给v1
result = v1() #执行inner函数，内部执行index函数，打印123，并将返回值赋给result
print(result)
```

```python
def func(arg):
    def inner():
        return arg()
    return inner
def index():
    print(123)
    return 666
index = func(index) #将inner函数地址赋给index,此时index变量不再指向原来函数地址
result = index() #执行index也就是inner函数，由于闭包，内部参数依旧执行index原地址，打印123，并将返回值赋给result
print(result) #666
```

```python
def func(arg):
    def inner():
        print('before')
        v = arg()
        print('after')
        return v
    return inner
def index():
    print(123)
    return 666
index = func(index) #将inner函数地址赋给index,此时index变量不再指向原来函数地址
result = index() #执行index也就是inner函数，由于闭包，内部参数依旧执行index原地址，先打印before,再执行原函数打印123，将返回值赋给v，然后打印after，最后返回v
print(result) #666
```

```python
def func(arg):
    def inner():
        print('before')
        v = arg()
        print('after')
        return v
    return inner

@func #第一步将index函数作为参数传入func函数 v = func(index)。第二部将func函数返回值赋值给index index = v
def index():
    print(123)
    return 666

index() 
```

#### 5.8.2 装饰器目的

在不改变原函数内部代码的情况下，在函数执行前和函数执行后自定义一些功能。

#### 5.8.3 装饰器应用场景

- 大多数函数要增加相同的功能

#### 5.8.4 装饰器格式

```python
# 装饰器的编写
def func(arg):
    def inner(*args,**kwargs):
        return arg(*args,**kwargs)
    return inner

# 装饰器的使用
@func
def index():
    print(123)
    
@func
def base(a):
    return a+100  
```

#### 5.8.5 带参数的装饰器

应用：flask框架 + django缓存 + 面试题

```python
####################普通装饰器###################
def func(arg):
    def inner(*args,**kwargs):
        print('before')
        data = arg(*args,**kwargs)
        print('after')
        return data
    return inner

#第一步执行 v1 = func(index)
#第二步 index = v1
@func
def index():
    print(123)
    
####################带参数的装饰器###################
def base(value):
    def func(arg):
        def inner(*args,**kwargs):
            print(value)
            data = arg(*args,**kwargs)
            print('after')
            return data
        return inner
    return func

#第一步 执行v1 = base(5)
#第二步 ret = v1(index)
#第三步 index = ret
@base(5)
def index():
    print(123)
```



### 5.9 迭代器

1. 自己不会写迭代器，只会用。

2. 迭代器：对可迭代对象（序列）中的元素进行逐一获取。

3. 列表转换成迭代器

   ```python
   lst = [11,22,33,44]
   v1= iter(lst)
   v2 = lst.__iter__()
   print(v2.__next__())
   print(v2.__next__())
   print(v2.__next__())
   print(v2.__next__()) #反复调用__next__()方法来获取每一个值，直到报错：StopIteration
   ```

4. 判断一个对象是否是迭代器：内部是否有`__next__()`方法。

   ```python
   lst = [11,22,33,44]
   for i in lst:   #1.内部将lst转换成迭代器，内部反复执行迭代器的__next__()方法，直到报错结束
       print(i)
   ```

5. 可迭代对象
   - 具有`__iter__()`方法，且`__iter__()` 方法返回一个迭代器或生成器

   - 或者可以被for循环

   - 如何将一个对象变为可迭代对象？

     在其类内部实现`__iter__()`方法，且返回一个迭代器或生成器

     ```python
     #方式一
     class Foo(object):
         def __iter__(self):
             return iter([1,2,3,4])
         
     #方式二
     class Foo(object):
         def __iter__(self):
             yield 1
             yield 2
             yield 4
     ```

     

### 5.10 生成器

```python
#函数
def func1():
    return 123
func()

#生成器函数（内部是否包含yield）
def func2():
	yield 1
    yield 2
    yield 3
    
v1 = func2()  #函数内部代码不会执行，返回一个生成器对象

#生成器可以被for循环，一旦开始循环，函数内部代码就会开始执行
for i in v1:
    print(i) #遇到yield就停止返回yield后的值
    		 #下一次继续从上次结束的位置开始，遇到yield就停止返回
        	 #直到生成器函数执行完毕终止循环
```

总结：

- 函数中如果存在yield，那么该函数就是一个生成器函数
- 执行生成器函数会返回一个生成器，只有for循环生成器的时候生成器函数内部的代码才会执行
- 每次循环都会获取yield返回的值，直到函数执行完成

```python
CURSOR = 0
FLAG = False

def func():
    global CURSOR
    global FLAG
    while True:
        f = open('goods.txt',mode='r',encoding='utf-8')
        f.seek(CURSOR)
        content_list = []
        for i in range(2):
            line = f.readline().strip()
            if not line:
                FLAG = True
            content_list.append(line)
        CURSOR = f.tell()
        f.close()

        for item in content_list:
            yield item
        if FLAG:
            return

v = func()
for item in v:
    print(item)
```

生成器：函数内部有yield就是生成器函数，调用该函数返回生成器，for循环生成器函数内部代码才会执行

- 生成器用来生成数据，也可以迭代(被for循环)，并且内部也有`__next__()` 方法
- 生成器是一种特殊的迭代器，和可迭代对象

yield from

```python
def base():
    yield 11
    yield 22

def func():
    yield 1
    yield from base()
    yield 3

for item in func():
    print(item)     #1 11 22 3
```

生成器的send方法会将参数赋给上一次执行的yield的变量，然后在执行下一次yield

可以通过close()方法手动关闭生成器

### 5.11 递归函数

递归函数：函数自己调用自己，内存无法释放，效率低。python递归调用默认最大次数为1000次。

```python
def func():
    print(123)
    func()
func()
```



## 六、模块

### 6.1 基本概念

- 内置模块

  python内部提供的功能

- 第三方模块

  需要下载、安装、使用

  pip install 要安装的模块名称

- 自定义模块

- 常用模块：json，time，datetime

- 模块的概念：

  - 模块是一个py文件或者一个文件夹（包），方便以后其他py文件调用。

- 对于包的定义：
  - py2：在文件夹下必须加上  __ init  __ .py文件。
  - py3：不必要加上__ init  __ .py文件。

- 导入模块
  - 导入模块时会将模块中的所有值加载到内存。
  - 导入模块的几种方式

  ```python
  import 模块
  from 模块 import 函数，函数
  from 模块 import *  #导入模块内所有函数
  from 模块 import 函数 as 别名 #起别名
  ```

- 导入包中的文件

  导入包相当于执行了`__init__.py`文件

  直接只导入一个包，包内的模块默认是不能用的

  ```python
  import 包  #内部模块不能使用
  ```

  

  ```python
  from 包 import 文件
  from xxx.xx import x 
  
  #导入的是包下面的__init__.py文件
  import 包
  from 包 import * 
  ```

- __ file __ :命令行执行脚本时，传入的该脚本路径参数。

- 注意：文件和文件夹的命名不能是导入的模块名称相同，否则就会直接在当前目录中查找。

### 6.2 随机数 random

```python
import random
v = random.randint(1,3)
print(v) #随机生成一个1~3的整数
v1 = random.choice([1,2,3,4,5]) #随机选取一个
print(v1) #1
v2 = random.sample([1,2,3,4,5],3) #随机选取多个
print(v2) #[2, 5, 3]
v3 = random.uniform(1,5) #随机获取一个小数
print(v3) #2.906728453875117
lst = [1,2,3,4,5]
random.shuffle(lst) #打乱顺序
print(lst) #[1, 4, 5, 3, 2]
```

可以和内置函数chr()内置函数搭配使用生成随机验证码

```python
import random
def func(count):
    lst = []
    for i in range(0,count):
        val = random.randint(65,90)
        lst.append(chr(val))
    return ''.join(lst)
result = func(4)
print(result)
```

### 6.3 摘要算法模块  hashlib

#### 6.3.1 md5算法

- 加密

```python
import hashlib

def func(pwd):
    key = "asdw3221" #秘钥
    obj = hashlib.md5(key.encode("utf-8")) #在内存中字符串编码是unicode。在py3中需要转换成utf-8编码。否则报错
    obj.update(pwd.encode('utf-8')) #加密字符串
    result = obj.hexdigest() #得到密文
    return result

print(func('123'))
```

- md5校验文件一致性

```python
import hashlib

obj = hashlib.md5()
obj.update('123'.encode('utf-8'))  #可以一次update完，也可以分多次update，结果相同
print(obj.hexdigest()) #比较最终的结果是否相同验证文件一致性
```

#### 6.3.2 sha算法 

用法和md5相同，通常使用sha1

### 6.4 密码不显示   getpass

只有在终端中运行有效果

```python
import getpass

pwd = getpass.getpass('请输入密码：')
print(pwd)
```

### 6.5 时间  time

#### 6.5.1 关于时间

1. UTC/GMT  世界时间
2. 本地时间

#### 6.5.2 time.time() 获取时间戳（秒）

```python
import time

v = time.time() #获取时间戳（秒）
time.sleep(2) #休眠2秒
```

#### 6.5.3 time.sleep() 休眠

```python
import time

time.sleep(2) #休眠2秒
```

#### 6.5.4 time.timezone 获取和格林威治时间的差值

```python
import time

v = time.timezone
print(v) #-28800
```

### 6.6 datetime

#### 6.6.1 datetime.now() 获取本地当前时间   得到时间类型

```python
from datetime import datetime

v = datetime.now()
print(v,type(v)) #2019-04-18 16:59:34.829613 <class 'datetime.datetime'>
```

#### 6.6.2 datetime.utcnow() 获取UTC当前时间

```python
from datetime import datetime

v = datetime.utcnow()
print(v,type(v))#2019-04-18 09:00:16.636312 <class 'datetime.datetime'>
```

#### 6.6.3 获取其他时区时间

```python
from datetime import datetime,timezone,timedelta
tz = timezone(timedelta(hours=7)) #东七区的时间   -7为西七区的时间
date = datetime.now(tz)
print(date)
```

#### 6.6.4 时间增加或减少

```python
from datetime import datetime,timedelta
v1 = datetime.now()#2019-04-18 17:07:35.080733
print(v1)
v2 = v1 + timedelta(days=10)
print(v2)#2019-04-28 17:07:35.080733
```

#### 6.6.5 datetime转成字符串（strftime(格式)）

```python
from datetime import datetime

v1 = datetime.now()
print(v1,type(v1)) #2019-04-18 17:13:16.579031 <class 'datetime.datetime'>
v2 = v1.strftime('%Y-%m-%d %H:%M:%S')
print(v2,type(v2))#2019-04-18 17:13:16 <class 'str'>
```

#### 6.6.6 字符串转成datetime  (strptime(字符串，格式))

```python
from datetime import datetime

v1 = '2018-12-14'
v2 = datetime.strptime(v1,'%Y-%m-%d')
print(v2)#2018-12-14 00:00:00
```

#### 6.6.7 时间戳转datetime  (fromtimestamp(时间戳))

```python
import time
from datetime import datetime

v1 = time.time()
date = datetime.fromtimestamp(v1)
print(date) #2019-04-18 17:20:42.438591
```

#### 6.6.8 datetime转时间戳  (timestamp())

```python
from datetime import datetime

v1 = datetime.now()
date = v1.timestamp()
print(date)#1555579302.702781
```

### 6.7 python解释器相关 sys

- sys.getrefcount() 获取变量被引用的次数

  ```python
  import sys
  value = []
  print(sys.getrefcount(value)) #2
  ```

- sys.getrecursionlimit()   获取python默认递归最大次数

  ```python
  import sys
  print(sys.getrecursionlimit()) #1000
  ```

- sys.stdout.write()    输出（不换行）

  ```python
  import sys
  sys.stdout.write("123")
  sys.stdout.write("123") #123123
  ```

  注：\r  将光标移动到本行的开头，可用于打印进度百分比

  ```python
  import time
  
  for i in range(1, 101):
      msg = "%s%%" % i
      print(msg, end='')
      time.sleep(0.1)
  ```

- sys.argv   获取执行脚本传入的参数

  ```python
  # C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
  # sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
  #print(sys.argv[1])  #D:/test
  ```

- sys.path  默认python在导入模块时会按照sys.path中的路径逐个查找。可以通过sys.path.append()添加一个路径

  ```python
  for i in sys.path:
      print(i)
  #E:\code\day10   当前执行文件所在的路径
  #C:\Python36\python36.zip
  #C:\Python36\DLLs
  #C:\Python36\lib
  #C:\Python36
  #C:\Python36\lib\site-packages
  ```

- sys.exit()  退出程序

- sys.module   存储了当前程序用到的所有模块

### 6.8 操作系统相关 os

- os.path.exists()    判断路径是否存在

  ```python
  import os
  print(os.path.exists('data.py')) #false
  ```

- os.stat(文件).st_size   获取文件大小

  ```python
  import os
  print(os.stat('goods.txt').st_size)
  ```

- os.path.abspath()  获取文件的绝对路径

  ```python
  import os
  print(os.path.abspath('goods.txt')) #E:\code\day10\goods.txt
  ```

- os.path.dirname()  获取路径的上级目录

  ```python
  import os
  v = r'E:\code\day10\goods.txt'  #r转义
  print(os.path.dirname(v)) #E:\code\day10
  ```

- os.path.join(*args)  将路径拼接起来

  ```python
  import os
  
  v = os.path.join('E:\code\day10', '123', '34')
  print(v) #E:\code\day10\123\34
  ```

- os.listdir()   获取目录下的所有文件，只能获取当前层

  ```python
  import os
  
  v = os.listdir('E:\code\day10')
  print(v) #['.idea', '123', 'day10.py', 'day11.py', 'day12.py', 'day13.py', 'goods.txt', 'test.py', 'test1.py', '__pycache__', '面试题.py']
  ```

- os.walk()  获取目录下的所有文件，所有层，返回生成器

  - for循环生成器 每次循环得到三个元素，第一个元素：正在查看的目录  第二个元素：此目录下的文件夹

    第三个元素：此目录下的文件

  ```python
  result = os.walk(r'D:\code\s21day14')
  for a,b,c in result:
  # a,正在查看的目录 b,此目录下的文件夹 c,此目录下的文件
  	for item in c:
  		path = os.path.join(a,item)
  		print(path)
  ```

- os.mkdir()  在当前工作路径下创建目录，只有一层

  ```python
  import os
  os.mkdir('aaa')
  ```

- os.makedirs()  在当前工作目录下创建目录及子目录，如果存在会报错

  ```python
  import os
  os.makedirs(r'dd\bb\cc')
  ```

- os.rename()  重命名

  ```python
  import os
  os.rename('test1.py','test2.py')
  os.rename('aaa','bbb')
  ```

### 6.9 shutil

#### 6.9.1 shutil.rmtree() 

删除目录下的所有文件和目录

```python
import shutil

shutil.rmtree('E:/code/day10')
```

#### 6.9.2 shutil.move() 重命名

```python
import shutil

shutil.move('ass','aaa')
shutil.move('aaa.txt','abc.txt')
```

#### 6.9.3 shutil.make_archive(压缩包名称,压缩的格式,要压缩的文件(夹))  

压缩文件或文件夹

```python
import shutil

shutil.make_archive('abc','zip','aaa')
```

#### 6.9.4 shutil.unpack_archive(压缩包名称,解压路径(默认当前路径),格式)  解压

```python
import shutil

shutil.unpack_archive('abc.zip',extract_dir=r'E:\test',format='zip')
```

### 6.10 json

#### 6.9.1 json基本知识

- json是一个特殊的字符串。
- 序列化：对象转换成json。
- 反序列化：json转换成对象
- json格式要求
  - 只包含int,str,list,dict,bool,tuple(会转换成列表)
  - 最外层必须是列表或字典
  - 如果包含字符串必须是双引号
- json的优缺点
  - 优点：所有语言通用
  - 缺点：只支持基本的数据类型（int,bool,str,list,tuple(会转换成list),dict）

#### 6.9.2 json.dumps() 序列化

```python
import json

lst = [1,2,3,{'k1':'v1'},(1,2,3),'alex',True]
print(json.dumps(lst))#[1, 2, 3, {"k1": "v1"}, [1, 2, 3], "alex", true]

#保留中文
import json
lst = ['alex',123,'李杰'] 
print(json.dumps(lst))#["alex", 123, "\u674e\u6770"]  中文默认序列化为unicode编码

print(json.dumps(lst,ensure_ascii=False))#["alex", 123, "李杰"]
```

#### 6.9.3 json.loads() 反序列化

```python
import json

val = '["alex",{"k2":2,"k1":44},true]'
print(json.loads(val))#['alex', {'k2': 2, 'k1': 44}, True]
```

#### 6.9.4 json.dump(obj,文件句柄,ensure_ascii = False) 

将对象序列化后写入文件

```python
import json

lst = ['alex', 123, '李杰']
f = open('abc.txt', mode='w', encoding='utf-8')
json.dump(lst, f, ensure_ascii=False)
f.close()
```

#### 6.9.5 json.load(文件内容)  

将文件内容反序列化成对象

```python
import json

f = open('abc.txt', mode='r', encoding='utf-8')
content = f.read()
data = json.load(content)
f.close()
print(data)
```

### 6.11 pickle

#### 6.11.1 pickle基本知识

- pickle优缺点
  - 优点：可以序列化几乎所有类型（socket除外）
  - 缺点：只有python自己认识

#### 6.11.2 pickle.dumps() 

将对象序列化成字节类型

#### 6.11.3 pickle.loads()

将字节类型反序列化成对象

```python
import pickle

lst = ['alex',123,'李杰']
v1 = pickle.dumps(lst)
print(v1) #b'\x80\x03]q\x00(X\x04\x00\x00\x00alexq\x01K{X\x06\x00\x00\x00\xe6\x9d\x8e\xe6\x9d\xb0q\x02e.'
data = pickle.loads(v1)
print(data)#['alex', 123, '李杰']

#操作函数
import pickle

def func():
    print(123)

v1 = pickle.dumps(func)
print(v1) #b'\x80\x03c__main__\nfunc\nq\x00.'
data = pickle.loads(v1)
data()#123
```

#### 6.11.4 pickle.dump()

将对象序列化成bytes并写入文件

```python
import pickle

def func():
    print(123)

f = open('abc.txt',mode='wb')
pickle.dump(func,f)
f.close()
```

#### 6.11.5 pickle.load()

读取文件内容，反序列化成对象

```python
import pickle

def func():
    print(123)

f = open('abc.txt',mode='rb')
data = pickle.load(f)
data()
f.close()
```

### 6.12 importlib模块

根据字符串的形式导入一个字符串

```python
import importlib
redis = importlib.import_module('utils.tests')
print(redis.X)


path = 'utils.tests.func'

v1,v2 = path.rsplit('.',maxsplit=1)
test = importlib.import_module(v1)
getattr(test,v2)()
```

### 6.13 logging模块

目的：记录日志

用户：记录用户的一些操作

程序员：

- 用于统计
- 用来做故障排除的debug
- 用来记录错误完成代码优化

应用场景：对异常处理捕获到的内容，通过logging模块写入到日志文件中。

#### 6.13.1 基本使用

```python
import logging
logging.basicConfig(filename='x.log',level=logging.ERROR)#使用系统默认编码
#level登记  一般写ERROR
'''
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
'''
logging.error('错误',exc_info=True)# 只有大于等于配置的登记才能写入日志文件,exc_info为True保留堆栈信息
```

#### 6.13.2 内部原理

```python
import logging
#涉及3个对象FileHandler、Formatter、Logger
file_handler = logging.FileHandler('x1.log',mode='a',encoding='utf-8')#创建FileHandler对象
fmt_handler = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s') #创建Formatter对象
file_handler.setFormatter(fmt_handler)#把Formatter对象添加到FileHandler对象中

logger = logging.Logger('xxx',level=logging.ERROR)#创建Logger对象，第一个参数为logger对象名称，即Formatter格式化时的name
logger.addHandler(file_handler)#把FileHandler对象添加到Logger对象中


logger.error('错误',exc_info=True)
```

#### 6.13.3 通过logger对象实现日志

```python
#创建logger对象
logger = logging.getLogger('mylog') 
#设置日志等级
logger.setLevel(logging.ERROR)
#创建文件操作符
fh = logging.FileHandler(filename='log.log',mode='a',encoding='utf-8')
#创建屏幕操作符
sh = logging.StreamHandler()
#创建格式化对象
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s")
#将格式化对象绑定到文件操作符
fh.setFormatter(formatter)
#将格式化对象绑定到屏幕操作符
sh.setFormatter(formatter)
#将文件操作符添加到logger对象
logger.addHandler(fh)
#将屏幕操作符添加到logger对象
logger.addHandler(sh)

logger.error('123')#2019-04-29 15:19:25,594 - mylog - ERROR - test: 123
```



#### 6.13.4 推荐使用格式

- 写入到一个日志文件中

  ```python
  import logging
  file_handler = logging.FileHandler('x1.log',mode='a',encoding='utf-8')
  
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      level=logging.ERROR,
      datefmt='%Y-%m-%d %H:%M:%S %p',#日期时间格式化
      handlers=[file_handler,]
  )
  
  
  logging.error('错误',exc_info=True)
  ```

- 日志分割（按时间）

  ```python
  import logging
  from logging import handlers
  file_handler = handlers.TimedRotatingFileHandler(filename='x2.log',when='h',interval=1,encoding='utf-8')#when参数单位为小时，interval参数按几个小时来分割
  
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      level=logging.ERROR,
      datefmt='%Y-%m-%d %H:%M:%S %p',
      handlers=[file_handler,]
  )
  
  try:
      int('wwqe')
  except Exception as e:
      logging.error(str(e),exc_info=True)
  ```

### 6.14 collections模块

#### 6.14.1 OrderedDict 有序字典

```python
from collections import OrderedDict

dic = OrderedDict([('a',1),('b',2),('c',3)])
for k,v in dic.items():
    print(k,v) #a 1   b 2    c 3
```

#### 6.14.2 namedtuple  可命名元组

创建一个类这个类没有方法，所有属性都不能修改

```python
from collections import namedtuple

Course = namedtuple('Course',['name','price','teacher'])
python = Course('python',19800,'alex')
print(python) #Course(name='python', price=19800, teacher='alex')
print(python.name) #python 
print(python.teacher) #alex
```

#### 6.14.3 deque 双端队列

#### 6.14.4 defaultDict 默认字典

可以给字典的value设置一个默认值

### 6.15 re模块

#### 6.15.1 转义符

正则表达式的转义符在python的字符串中也有转义的作用，但正则表达式中的转义符和字符串中的转义符并没有关系，且还容易产生冲突。为了避免冲突我们所有的正则都以在工具中的测试结果为结果，然后只需要在正则和待匹配的字符串前面加r即可。

```python
import re
ret = re.search(r'\\n',r'\n')
print(ret.group())
```

#### 6.15.2 模块方法

1. findall()

   ```python
   import re
   ret = re.findall('\d','alex83') #第一个参数正则表达式，第二个参数待匹配的字符串，返回一个列表，未匹配到返回空列表
   print(ret) #['8','3']
   ```

2. search()

   ```python
   import re
   ret = re.search('\d','alex83')#若匹配到则返回一个对象，否则返回None,只取第一个符合条件的项
   print(ret) #<_sre.SRE_Match object; span=(4, 5), match='8'>
   if ret:
   	print(ret.group())#8
   ```

3. match()

   ```python
   #只能从头开始匹配，相当于search的正则表达式加上一个^
   import re
   ret = re.match('\d','1alex83')#若匹配到则返回一个对象，否则返回None,只取第一个符合条件的项
   print(ret) #<_sre.SRE_Match object; span=(4, 5), match='8'>
   if ret:
   	print(ret.group())#8
   ```

4. finditer()

   ```python
   import re
   ret = re.finditer('\d','alex83') #返回一个迭代器
   for i in ret: #循环迭代器返回一个对象
       print(i.group())
   ```

5. compite()

   将正则表达式提前编译好，后面重复调用该正则规则时可以节省时间和空间。

   ```python
   import re
   ret = re.compile('\d')
   res = ret.findall('alex83')
   print(res)
   
   res = ret.search('alex83')
   print(res.group())
   
   res = ret.finditer('alex83')
   for i in res:
       print(i.group())
   ```

   compile的flags参数：

   ```python
   flags = re.S  #.可以匹配任意字符
   ```

6. split()

   ```python
   import re
   ret = re.split('\d+','alex83wusir74taibai40') #以正则表达式匹配的内容进行分割
   print(ret)#['alex', 'wusir', 'taibai', '']
   
   #split 默认保留分组中的内容
   ret = re.split('(\d+)','alex83wusir74taibai40')
   print(ret)#['alex', '83', 'wusir', '74', 'taibai', '40', '']
   ```

7. sub()和subn()

   ```python
   import re
   ret = re.sub('\d','D','alex83wusir74taibai40',3) #替换，默认全部替换，返回替换后的字符
   print(ret)# alexDDwusirD4taibai40
   
   ret = re.subn('\d','D','alex83wusir74taibai40',3)#替换，默认全部替换，返回替换后的字符和替换了几个字符
   print(ret)# ('alexDDwusirD4taibai40', 3)
   ```

#### 6.15.3 分组

无分组

```python
import re
s = '<h1>wahaha</h1>'
ret = re.search('<\w+>.*?</\w+>',s)
print(ret.group())
```

分组

```python
import re
s = '<h1>wahaha</h1>'
ret = re.search('<(\w+)>(.*?)</\w+>',s)
print(ret.group(0)) #<h1>wahaha</h1>  等同于ret.group()默认参数为0，表示取整个匹配项
print(ret.group(1)) #h1  取第一个分组内的内容
print(ret.group(2)) #wahaha 取第二个分组内的内容
```

对于findall()遇到正则表达式中的分组，会优先显示分组中的内容

```python
import re

ret = re.findall('\d+(\.\d+)?','1.234+2')
print(ret) #['.234', ''] 优先显示分组中的内容，所以2不会显示
```

取消分组优先显示 ?:

```python
import re

ret = re.findall('\d+(?:\.\d+)?','1.234+2')
print(ret)
```

应用：取字符串里的所有整数

```python
import re

ret = re.findall('\d+\.\d+|(\d+)','1-2*(60+(-40.35/5)-(-4*3))')#首先把整数和小数都取出来，优先显示整数
print(ret) #['1', '2', '60', '', '5', '4', '3']
ret.remove('') #移除''
print(ret)#['1', '2', '60', '5', '4', '3']
```

#### 6.15.4 分组命名

（?P<名字>正则表达式）

```python
import re

s = '<h1>wahaha</h1>'
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s)
print(ret.group('tag'))
print(ret.group('cont'))
```

#### 6.15.5 引用分组

这个组的内容必须和引用的分组的内容完全一致

```python
import re
s = '<h1>wahaha</h1>'
s1 = '<h1>wahaha</h2>'
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s)
ret1 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s1)
print(ret.group())#<h1>wahaha</h1>
print(ret1.group())#报错
```

#### 6.15.6 正则的？都能做什么？

- ? 量词，0个或一个

- 用于量词后，非贪婪匹配
- ?P<> 用于分组命名
- ?P=   用于引用分组
- ?： 取消分组优先显示

#### 6.15.7 爬虫小例子

```python
import re
import json
import requests


def get_req(url):
    ret = requests.get(url)
    return ret.text


def get_con(ret, content):
    res = ret.finditer(content)
    for i in res:
        yield {
            'id':i.group('id'),
            'title':i.group('title'),
            'score':i.group('score'),
            'comment':i.group('comment')
        }




def write_file():
    with open('moviefile',mode='w',encoding='utf-8') as f:
        while True:
            dic = yield
            f.write(dic+'\n')


pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>\w+)' \
          '</span>.*?<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment>\d+)人评价'

res = re.compile(pattern, flags=re.S)

num = 0

f = write_file()

next(f)

for i in range(10):
    num = 25 * i
    con = get_req('https://movie.douban.com/top250?start=%s&filter=' % num)
    gc = get_con(res, con)
    for j in gc:
        f.send(json.dumps(j,ensure_ascii=False)#生成器的send方法会将参数赋给上一次执行的yield的变量，然后在执行下一次yield
        
f.close()#手动关闭生成器
```

### 6.16正则表达式

#### 6.16.1 定义

正则表达式是一种匹配字符串的规则

#### 6.16.2 应用场景

- 匹配字符串
  - 电话号码
  - 身份证
  - IP地址

- 表单验证
  - 验证用户输入的信息是否正确

- 爬虫
  - 从网页源码中获取一些链接，重要数据

#### 6.16.3 正则规则（元字符，量词）

1. 第一匹配规则：是哪一个字符，就匹配字符串中的哪个字符

2. 第二匹配规则：字符组[字符1字符2]，一个字符组就代表匹配一个字符，只要这个字符出现在字符组中，那就说明这个字符符合规则。

   ```python
   #[0-9][0-9]匹配一个两位数
   #[A-z]必须根据ascii码从小到大指定范围。
   #[a-zA-Z]匹配所有大小写字母
   ```

##### 6.16.3.1 元字符

```python
# \d: [0-9] 表示所有数字（digit）
# \w: 表示大小写字母，数字，下划线（word）
# \s: 表示空白，即空格，制表符，换行符（space）
# \t: 表示制表符（tab）
# \n: 表示换行符（next）
# \D: 表示除数字以外的所有字符
# \W: 表示除大小写字母，数字，下划线以外的所有字符
# \S: 表示除空白以外的所有字符      例：[\d\D]匹配所有字符
# .: 表示除换行符之外的所有字符
# []: 字符组，只要在中括号内的字符都是符合规则的字符 例：[\d] \d [0-9] 表示的都是匹配一个数字
# [^]: 非字符组，只要不在中括号内的字符都是符合规则的字符
# ^: 表示字符串的开始
# $: 表示字符串的结尾
#例：
# abc abc abc  ^a 只能匹配到第一个a   $c 只能匹配到最后一个c  
# ^abc$ 无匹配项 ：^和$之间有多长，匹配到的字符串就有多长

# |: 表示或   注：abc|ab如果两个规则有重合，则长的在前短的在后
# (): 表示分组，给一部分正则分组， 如：www\.(baidu|luffy)\.com,|的作用域就变小了
```

##### 6.16.3.2 量词

```python
# {n}: 表示只能出现n次
# {n,}: 表示至少出现n次
# {n,m}: 表示至少出现n次，至多出现m次
# ?: 匹配0次或1次，表示可有可无但又只能有一个
# +: 匹配一次或多次
# *: 匹配0次或多次，表示可有可无但有可以有多个
#例：表示一个小数或整数
#\d+(\.\d+)?
```

##### 6.16.3.3 贪婪匹配和非贪婪匹配

- 贪婪匹配（默认）

  总是在符合量词条件的范围内尽量多的匹配

- 非贪婪匹配

  总是匹配符合量词条件范围内尽量小的字符串

在**量词**后面加一个?，就变成了非贪婪匹配。

常用：

```python
#元字符 量词 ？x    表示按照元字符规则在量词范围内匹配，一旦遇到x就停止
#例： .*?x  匹配除换行符外的所有字符遇到x停止
```

带有特殊意义的元字符和量词到了字符组或非字符组内，有些会取消他的特殊意义。如：`[()+*.]`

[-],-的位置决定了他的意义，写在字符组的第一个位置或最后一个位置就表示一个普通的横杠，写在字符组的其他任何位置都表示一个范围

### 6.17 struct模块

```python
import struct

ret = struct.pack('i',10000) #i代表int，得到4个字节
print(ret)#b"\x10'\x00\x00"

res = struct.unpack('i',ret)
print(res)#(10000,)
```

### 6.18 socketserver模块

详见 8.7.2

### 6.19 hmac模块

```python
import hmac,os

secret_key = '123456'
random_msg = os.urandom(16) #得到16位随机字节串
obj = hmac.new(secret_key.encode('utf-8'),random_msg)
print(obj.digest())# 得到加密后的字节串 b': \x98`\xa6\xdb\xcdF\\ \xae\xbfR\x0e\x1a\xc1'
```

## 七、面向对象

- 什么是类：具有相同属性和方法的一类事物。
- 什么是对象或实例：一个具有具体属性和动作的具体个体。

### 7.1面向对象的基本形式

#### 7.1.1 基本格式

```python
#定义类
class Account:
    def func(self):
        print(123)

#调用类中的方法
a = Account() #创建（实例化）了一个Account类的对象
a.func() #使用对象调用类中的方法
```

应用场景：以后遇到很多函数，要对函数进行归类和划分（封装）

#### 7.1.2 对象的作用

self参数：哪个对象调用的方法，self就是哪个对象

对象的作用：存储一些值，供自己以后使用

```python
class Person:
    def func(self):
        print('我叫%s，今年%s岁，性别%s'%(self.name,self.age,self.gender))

p = Person()
p.name = 'alex'
p.age = 18
p.gender = '男'
p.func()

#__init__() 初始化方法（构造方法）
class Person:
    def __init__(self,name,age,gender): #可以加参数
        self.name = name
        self.age = age
        self.gender = gender
        
    def func(self):
        print('我叫%s，今年%s岁，性别%s'%(self.name,self.age,self.gender))
        
p = Person('张三',18,'男') #实例化时，会调用__init__()方法
p.func()
p1 = Person('李四',19,'男') 
p1.func()  #将数据放入__init__()方法中，可以更方便的为每个对象保存自己的数据
```

#### 7.1.3 应用场景

- 业务功能复杂时，可以用面向对象进行分类
- 想要封装有限个数据时

### 7.2 面向对象三大特性

#### 7.2.1 封装性

- 将函数归类放到一个类中
- 函数中有反复使用的值，可以放到对象中

#### 7.2.2 继承性

广义：将数据和方法放入类中

狭义：私有成员，不允许外部访问

```python
#基类
class Base:
    def func1():
        print('f1')

#派生类
class Foo(Base):  
    def func2():
        print('f2')
        
f = Foo()
f.func2() #首先在对象自己的类中查找，找不到就去父类中查找
f.func1()
```

什么时候会用到继承？

- 多个类中有公共方法时，可以将它们放到基类中，避免重复编写。

继承关系中查找方法的顺序：

- self是到底是谁
- self是由哪个类创建的，就在哪个类中查找，找不到就去父类中查找

```python
# 示例一
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1() #base.f1
obj.f2() #foo.f2

# 示例二
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
        
obj = Foo() 
obj.f2() #base.f1  foo.f2

# 示例三
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
	def f1(self):
        print('foo.f1')
        
obj = Foo()
obj.f2()#foo.f1  foo.f2

# 示例四
class Base:
    def f1(self):
        self.f2()
        print('base.f1')
	def f2(self):
        print('base.f2')
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1() #foo.f2  base.f1
```

对于多继承，从左到右依次查找

```python
class BaseServer:
    def serve_forever(self, poll_interval=0.5):
        self._handle_request_noblock()
	def _handle_request_noblock(self):
        self.process_request(request, client_address)
        
	def process_request(self, request, client_address):
        pass
    
class TCPServer(BaseServer):
    pass

class ThreadingMixIn:
    def process_request(self, request, client_address):
        pass
    
class ThreadingTCPServer(ThreadingMixIn, TCPServer): 
    pass

obj = ThreadingTCPServer()
obj.serve_forever()
```

多继承查找方式：

- py2：深度优先
- py3：广度优先   同过C3算法计算查找顺序

#### 7.2.3 多态性(鸭子模型)

```python
def func(arg):
    v = arg[-1]
    print(v)
```

面试题：什么是鸭子模型

答：对于函数而言，Python对参数的类型不会限制，那么在传入参数时就可以是任意类型，在函数中如果有如：arg[-1],那么就是对传入类型的限制（类型必须有索引）。这就是鸭子模型，类似于上述函数，我们认为只要是能嘎嘎叫的就是鸭子（只要有索引就是我们要的参数类型）

### 7.3 成员

#### 7.3.1 类变量 和 实例变量(属于对象)

写在类中与方法同级别，可以通过类名.变量，对象.变量调用

```python
class Base:
    x = 1

obj = Base()
print(obj.name) #打印实例变量
print(Base.name) #报错，类名无法访问实例变量
print(obj.x) #对象调用类变量
print(Base.x) #类名调用类变量
```

只能修改或设置自己的值，不能修改其他值

```python
class Base:
    x = 1

obj = Base()
obj.y = 123 #为对象obj添加了一个y变量
obj.x = 666 #为对象obj添加了一个x变量
print(Base.x) #1
Base.x = 666 #修改类变量x
print(Base.x) #666
```

```python
class Base:
    x = 1
    
class Foo(Base):
    pass
print(Base.x)#1
print(Foo.x)#1
Base.x = 666
print(Base.x)#666
print(Foo.x)#666
Foo.x = 999
print(Base.x)#666
print(Foo.x)#999
```

#### 7.3.2 绑定方法

定义：至少有一个self参数

调用：先创建对象，由对象调用

```python
class Base:
    def func(self):
        pass
obj = Base()
obj.func()
```

#### 7.3.3 静态方法

定义：

- 由装饰器@staticmethod装饰
- 参数任意

调用：

- 可以由类名或对象名（不推荐）调用

```python
class Base:
    @staticmethod
    def func():
		pass
Base.func()
```

#### 7.3.4 类方法

定义：

- 由装饰器@classmethod装饰
- 至少一个参数

调用：

- 可以由类名或对象名（不推荐）调用

```python
class Base:
    @classmetnod
    def func(cls): #cls类名
        pass
Base.func()
```

面试题：@staticmethod和@classmethod的区别？

一个是静态方法，一个是类方法

定义：

- 类方法：用@classmethod做装饰器，且至少一个参数
- 静态方法：用@staticmethod做装饰器，且参数无限制

调用：

- 类名调用，对象也可调用。

#### 7.3.5 属性

定义：

- 用@property装饰器
- 只有一个self参数

调用：

- 对象.func   不加括号

```python
class Base:
    @property
    def func(self):
        return 123
 
obj = Base()
obj.func
```

### 7.4 成员修饰符

公有：任何地方都可以访问

私有：只有类内部可以访问

```python
class Foo:
    def __init__(self,name):
        self.__name = name #__name为私有实例变量
    def func(self):
        print(self.__name)
        
obj = Foo('alex')
obj.__name #外部访问不到（继承也无法访问）
obj.func() #内部可以访问
```

可以通过obj._Foo.__x强制访问

### 7.5 特殊成员

#### 7.5.1 `__init__(self)`

初始化函数,为对象的变量赋值

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
        
obj = Foo('alex')
```

#### 7.5.2 `__new__(cls,*args,**kwargs)`

构造方法，在实例化一个对象时会有两步`obj = Foo('alex')`

- 第一步调用`__new__()`方法
- 第二步调用`__init__()`方法初始化变量

```python
class Foo(object):
    def __init__(self, name):
        self.name = name

    def __new__(cls, *args, **kwargs):
        print(args[0])
        return object.__new__(cls) #返回什么obj就是什么

obj = Foo('alex')
print(obj.name)
```

若类内部无`__new__()`方法，则会调用object的`__new__()`方法

#### 7.5.3 `__call__(self)`

对象后面加括号会执行`__call__()`方法

```python
class Foo(object):
    def __call__(self, *args, **kwargs):
        print('执行了__call__方法')

Foo()()
```

#### 7.5.4 `__getitem__(self,item)`

```python
class Foo(object):
    def __getitem__(self, item):
        return item

obj = Foo()
print(obj['1']) #1
```

#### 7.5.5 `__setitem__(self,key,value)`

```python
class Foo(object):
    def __setitem__(self, key, value):
        print(key,value)

obj = Foo()
obj['k1'] = 123 #k1 123
```

#### 7.5.6 `__delitem__(self,key)`

```python
class Foo(object):
    def __delitem__(self, key):
        print(key)
        
obj = Foo()
del obj['k1']  #k1
```

#### 7.5.7 `__str__(self)`

打印对象时执行，打印内容为返回值

```python
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def __str__(self):
        return '姓名：%s,年龄：%s'%(self.name,self.age,)


obj = Foo('alex',18)
print(obj) #姓名：alex,年龄：18
```

#### 7.5.8 `__dict__`

寻找对象的所有变量，并转换成字典

```python
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age


obj = Foo('alex',18)
print(obj.__dict__) #{'name': 'alex', 'age': 18}
```

#### 7.5.9 上下文管理

```python
class Foo(object):

    def __enter__(self):
        print('开始')
        return Foo()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('结束')


with Foo() as obj:   #with后面的Foo()会执行__enter__()方法,并将返回值赋给obj
    print(123)       #缩进代码执行完成后会执行__exit__()方法
                     #开始
        			 #123
            		 #结束
```

#### 7.5.10 对象相加

```python
class Foo(object):
    def __init__(self, name):
        self.name = name

    def __add__(self, other):
        return '%s + %s'%(self.name,other.name,)

    def __sub__(self, other):
        return '%s - %s' % (self.name, other.name,)

    def __mul__(self, other):
        return '%s * %s' % (self.name, other.name,)

    def __divmod__(self, other):
        return '%s / %s' % (self.name, other.name,)

    def __mod__(self, other):
        return '%s %% %s' % (self.name, other.name,)

obj1 = Foo('alex')
obj2 = Foo('eric')
print(obj1 + obj2) #调用obj1对象的类的__add__(self,other)方法，将obj1赋给self,obj2赋给other，返回值是表达式结果
print(obj1 - obj2)
print(obj1 * obj2)
print(divmod(obj1,obj2))
print(obj1 % obj2)
```

7.5.11 `__iter__(self)`

```python
#将对象变成可迭代对象
class Foo(object):
    def __iter__():
        return iter([1,2,3,4])
    
obj = Foo()
```

### 7.6 super

super().func() 根据self对象所属的类的继承关系,按照顺序挨个查找func()函数，找到第一个开始执行，后面不再找。

```python
class Base(object):
	def func(self):
		print('base.func')
		return 123
class Foo(Base):
	def func(self):
		v1 = super().func()
		print('foo.func',v1)
obj = Foo()
obj.func()
# super().func() 去父类中找func方法并执行
```

```python
class Bar(object):
	def func(self):
		print('bar.func')
		return 123
    
class Base(Bar):
	pass

class Foo(Base):
	def func(self):
		v1 = super().func()
		print('foo.func',v1)
obj = Foo()
obj.func()
# super().func() 根据类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

```python
class Base(object): # Base -> object
	def func(self):
		super().func()
		print('base.func')

class Bar(object):
	def func(self):
		print('bar.func')
class Foo(Base,Bar): # Foo -> Base -> Bar
	pass
obj = Foo()
obj.func()
# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

### 7.7 异常处理

#### 7.7.1 基本格式

```python
def func():
    try:
        pass
    except IndexError as e: 
        pass
    except ValueError as e:
        pass
    except Exception as e: #e为Exception类的一个对象
        pass
    finally:   #finally内的代码，上面遇到return也会被执行
        pass
```

#### 7.7.2 抛出异常(raise)

```python
def func():
	result = True
	try:
		with open('x.log',mode='r',encoding='utf-8') as f:
			data = f.read()
			if 'alex' not in data:
			raise Exception()
	except Exception as e:
		result = False
	return result
```

#### 7.7.3 自定义异常

```python
class MyException(Exception):
    pass

def func():
    try:
        raise MyException()
    except MyException as e:
        print(e)
```

### 7.8 约束

是一种规范，约束子类必须实现某方法

```python
class Base(object):

    def send(self):
        raise NotImplementedError()  #基类约束所有的子类必须实现send方法


class Bar(Base):

    def send(self):  #子类实现send方法
        print('Bar的send方法') 


class Foo(Base):

    def send(self):   #子类实现send方法
        print('Foo的send方法')
```

### 7.9 反射

以字符串的形式去某个对象中操作他的成员

通过本文件来获取本文件的任意变量

```python
getattr(sys.module[__name__],'变量名')
```

#### 7.9.1 getattr(对象,'成员')

以字符串的形式去对象中获取对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')
v = getattr(obj,'name') #等同于obj.name
print(v)
```

#### 7.9.2 setattr(对象,'成员',值)

以字符串的形式设置对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')
setattr(obj,'name',123) #等同于obj.name = 123
print(getattr(obj,'name'))
```

#### 7.9.3 hasattr(对象,'成员')

以字符串的形式判断对象中是否包含某成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')

if hasattr(obj,'name'):
    print(getattr(obj, 'name'))
```

#### 7.9.4 delattr(对象,'成员')

以字符串形式删除对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')

delattr(obj,'name')
```

#### 7.9.5 python中一切皆对象

因此以后想要通过字符串形式操作内部成员，均可以通过反射来实现

- py文件（模块）
- 包
- 类
- 对象

```python
import utils.tests
print(getattr(utils.tests,'X'))
```

通过本文件来获取本文件的任意变量

```python
getattr(sys.module[__name__],'变量名')
```

sys.module 存储了当前程序用到的所有模块

### 7.10 单例模式

无论实例化多少次使用的都是第一次创建的那个对象

```python
class Singleton(object):
    instance = None
    def __new__(cls,*args,**kwargs):
        if not cls.instance:
            cls.instance = object.__new__(cls)
        return cls.instance

obj1 = Singleton()
obj2 = Singleton()

print(obj1 is obj2) #True
```

多次导入，不会重新加载。模块在第一次导入时，会加载到内存中，其他文件再次导入时不会重新加载。

通过模块的导入特性也可以实现单例模式。

```python
#test.py文件
class Singleton(object):
    pass

OBJ = Singleton()

#其他文件
import test
obj = test.OBJ
```

### 7.11 项目目录结构

#### 7.11.1 对于一个脚本

只有一个文件，在文件内导入模块编写代码即可。

#### 7.11.2 对于但可执行文件

- config文件夹  存放配置文件
- db文件夹  存放数据文件
- lib文件夹  存放公共功能，类库
- src文件夹 存放业务相关代码
- log文件夹  存放日志文件
- app.py  可执行文件

#### 7.11.3 对于多可执行文件

- config文件夹  存放配置文件

  - setting.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    import os
    
    BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    LOG_FILE_PATH = os.path.join(BASE_PATH,'log','debug.log')
    LOG_WHEN = 'd'
    LOG_INTERVAL = 1
    ```

- db文件夹  存放数据文件

- lib文件夹  存放公共功能，类库

  - log.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    import logging
    from logging import handlers
    from config import settings
    def get_logger():
        file_handler = handlers.TimedRotatingFileHandler(filename=settings.LOG_FILE_PATH,
                                                         when=settings.LOG_WHEN,			                                                                  interval=settings.LOG_INTERVAL, 
                                                         encoding='utf-8')
        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(module)s： %(message)s',
            level=logging.ERROR,
            handlers=[file_handler,]
        )
    
        return logging
    
    logger = get_logger()
    ```

- src文件夹 存放业务相关代码

  - run.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    
    from src import account
    def start():
        dic = {'1':account.login,'2':account.reg}
        print('1.登录\n2.注册')
    
        index = input('请选择')
        dic.get(index)()
    ```

  - account.py

    ```python
    from lib.log import logger
    def login():
        print('登录')
        logger.error('登录错误',exc_info=1)
    
    def reg():
        print('注册')
        logger.error('登录错误', exc_info=1)
    ```

- log文件夹  存放日志文件

- bin文件夹  存放可执行文件

  - student.py

    ```python
    import os
    import sys
    
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(PROJECT_PATH)
    from src import run
    
    if __name__ == '__main__':
        run.start()
    ```

  - teacher.py

    ```python
    import os
    import sys
    
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(PROJECT_PATH)
    from src import run
    
    if __name__ == '__main__':
        run.start()
    ```


### 7.12 新式类和经典类

- 新式类：
  - 支持super
  - 广度优先
  - 有mro方法
- 经典类：
  - 不支持super
  - 深度优先
  - 没有mro方法

继承顺序可以通过mro方法获得

```python
class A(object):
    def func(self):
        print('A')

class B(A):
    def func(self):
        print('B')

class C(A):
    def func(self):
        print('C')

class D(B,C):
    def func(self):
        print('D')

print(D.mro())#[<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>]
```

super的查找顺序和mro方法得到的顺序相同

面试题：

```python
class A(object):
    def __init__(self):
        self.__func()   #私有成员会变式成_A__func()
        
    def func(self):
        print('A')
        
class B(object):
    def func(self):
        print('B')
        
B().func() #打印A
```



## 八、网络编程

### 8.1 网络基础

#### 8.1.1 网络应用开发架构

- C/S架构：client-server
- B/S架构：browser-server

- B/S架构是特殊的C/S架构

#### 8.1.2 网络基础概念

- 网卡：一个实际存在的计算机硬件
- mac地址：网卡在出厂时设置好的一个地址，全球唯一。普通交换机只认识mac地址。
- 协议：server和client得到的内容都是二进制，所以每一位代表什么内容，对于计算机来说必须先约定好，再按照约定进行发送和解析。
- ip地址
  - IPv4协议: 4位的点分十进制，32位2进制表示。范围：0.0.0.0-255.255.255.255
  - IPv6协议：8位的冒分十六进制，128位二进制表示。范围：0:0:0:0:0:0:0:0-FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF

- 公网ip：每个IP地址想要被所有人访问到，那么这个ip地址必须是你申请的。
- 内网ip：
  - 192.168.0.0-192.168.255.255
  - 172.16.0.0-172.31.255.255
  - 10.0.0.0-10.255.255.255

- 交换机负责进行局域网内通信，路由器负责局域网间通信。
- 交换机：广播、单播、组播
- arp协议：地址解析协议。已知一个机器的ip地址，获取该机器的mac地址（会用到广播、单播）
- 网关IP：是一个局域网的网络出口，访问局域网之外的区域都需要经过路由器和网关。
- 网段：指的一个ip地址段。
- 子网掩码：用来计算两台机器是否在同一网段内。将两个ip转换成二进制，分别和子网掩码按位与，得到的结果相同则在同一网段内。
- 端口：0-65535。自己写的程序一般设置8000以后的端口。

### 8.2 TCP协议

tcp协议是一种全双工通信，是基于连接的协议。

#### 8.2.1 建立和断开连接

建立连接：三次握手，只能由客户端发起连接请求

断开连接：四次挥手，双方都可以发起断开请求。因为客户端或服务端主动发起断开请求后，对方同意后但对方可能还有信息要发送，所以断开连接请求中间的通过和请求断开连接不能合并。

![](E:\Python\2019-05-07\day28\TCP.png)

#### 8.2.2 tcp特点

- 面向连接，
- 流式的，
- 可靠的，慢
- 全双工通信

在建立连接后：

- 发送的每一条信息都有回执
- 为了保证数据的完整性，还有重传机制

IO操作(in/out)：输入和输出是相对与内存来说的。

- write/send 输出
- read/recv 输入

#### 8.2.3 应用场景

文件的上传下载（发邮件、网盘、缓存电影）

### 8.3 UDP协议

#### 8.3.1 udp特点

- 无连接速度快
- 传输不可靠，可能会丢消息
- 能够传递的长度有限，和数据传递设备的设置有关（一般1500byte）

补充：是一个面向数据报，不可靠，快的，能完成一对一，一对多，多对多的高效通讯协议。（视频的在线观看，即时聊天）

#### 8.3.2 应用场景

即时通讯类（QQ、微信、飞秋的聊天系统）

### 8.4 osi七层模型

应用层、表示层、会话层、传输层、网络层、数据链路层、物理层。

有时把应用层、表示层、会话层统一称为应用层。所以有了以下osi五层模型

| 层号 |    名称    |              协议               |        物理设备        |
| :--: | :--------: | :-----------------------------: | :--------------------: |
|  5   |   应用层   | http,https,ftp,smtp(python代码) |                        |
|  4   |   传输层   |          tcp,udp,端口           | 四层路由器、四层交换机 |
|  3   |   网络层   |            ipv4,ipv6            |   三层交换机、路由器   |
|  2   | 数据链路层 |        mac地址，arp协议         |    网卡、二层交换机    |
|  1   |   物理层   |             二进制              |                        |

### 8.5 socket实现TCP、UDP

什么是socket?

socket(套接字)是工作在应用层和传输层之间的抽象层，帮助我们完成所有信息的组织和拼接。socket对程序员来说已经是网络操作的底层了。

#### 8.5.1 socket实现TCP

```python
#服务端
import socket

sk = socket.socket()#默认tcp协议（type=socket.SOCK_STREAM）
sk.bind(('192.168.12.39',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        recv_msg = conn.recv(1024)
        if recv_msg.decode().upper() == 'Q':
            conn.close()
            break
        print(recv_msg.decode('utf-8'))
        send_msg = input('请输入：')
        conn.send(send_msg.encode('utf-8'))
        if send_msg.upper() == 'Q':
            conn.close()
            break

sk.close()
```

```python
#客户端
import socket

sk = socket.socket()#默认tcp协议（type=socket.SOCK_STREAM）
sk.connect(('192.168.12.39',8066))
while True:
    send_msg = input('请输入：')
    sk.send(send_msg.encode('utf-8'))
    if send_msg.upper() == 'Q':
        break
    recv_msg = sk.recv(1024)
    if recv_msg.decode().upper() == 'Q':
        break
    print(recv_msg.decode('utf-8'))
sk.close()
```

#### 8.5.2 socket实现UDP

```python
#服务端
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)#udp协议
sk.bind(('127.0.0.1',8066))

while True:
    msg,addr = sk.recvfrom(1024)
    print(msg.decode('utf-8'))
    send_msg = input('>>>').encode('utf-8')
    sk.sendto(send_msg,addr)

sk.close()
```

```python
#客户端
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)#udp协议
while True:
    send_msg = input('>>>')
    if send_msg.upper() == 'Q':
        sk.close()
        break
    sk.sendto(send_msg.encode('utf-8'),('127.0.0.1',8066))
    recv_msg = sk.recv(1024)
    print(recv_msg.decode('utf-8'))
```

### 8.6 粘包

#### 8.6.1 tcp粘包问题

tcp传输特点

- 无边界
- 流式传输

##### 8.6.1.1 什么是粘包

同时执行多条命令之后，得到的结果很可能只有一部分，在执行其他命令的时候又接收到之前执行的另外一部分结果，这种现象就是黏包。

##### 8.6.1.2 tcp的分包机制

当发送端缓冲区的长度大于网卡的MTU时，tcp会将这次发送的数据拆成几个数据包发送出去。 
MTU是Maximum Transmission Unit的缩写。意思是网络上传送的最大数据包。MTU的单位是字节。 大部分网络设备的MTU都是1500。如果本机的MTU比网关的MTU大，大的数据包就会被拆开来传送，这样会产生很多数据包碎片，增加丢包率，降低网络速度。

##### 8.6.1.3 tcp粘包原因

- 发送方的缓存机制
  - 发送端需要等缓冲区满才发送出去，造成粘包（发送数据时间间隔很短，数据了很小，会合到一起，产生粘包）
- 接收方的缓存机制
  - 接收方不及时接收缓冲区的包，造成多个包接收（客户端发送了一段数据，服务端只收了一小部分，服务端下次再收的时候还是从缓冲区拿上次遗留的数据，产生粘包） 

##### 8.6.1.4 总结

黏包现象只发生在tcp协议中：

1.从表面上看，黏包问题主要是因为发送方和接收方的缓存机制、tcp协议面向流通信的特点。

2.实际上，**主要还是因为接收方不知道消息之间的界限，不知道一次性提取多少字节的数据所造成的**

#### 8.6.2 解决粘包问题

##### 8.6.2.1 解决粘包问题

借助struct模块，我们知道长度数字可以被转换成一个标准大小的4字节数字。因此可以利用这个特点来预先发送数据长度。

我们还可以把报头做成字典，字典里包含将要发送的真实数据的详细信息，然后json序列化，然后用struck将序列化后的数据长度打包成4个字节（4个字节足够用了）

```python
#服务端
import struct,json,socket

sk = socket.socket(type=socket.SOCK_STREAM)
sk.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)#防止连接关闭后，端口没有马上被释放。可以复用端口
sk.bind(('127.0.0.1',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        head_length = conn.recv(4) #接收包头长度
        head_length = struct.unpack('i',head_length)[0]
        body_length = json.loads(conn.recv(head_length).decode('utf-8'))['body_length']

        recv_length = 0
        recv_info = b''
        while recv_length < body_length:
            recv_info += conn.recv(body_length-recv_length)# 循环接收包未接收完的数据
            recv_length = len(recv_info)

        print(recv_info.decode('utf-8'))
    
    conn.close()
sk.close()
```

```python
#客户端
import json,struct,socket

sk = socket.socket(type=socket.SOCK_STREAM)
sk.connect(('127.0.0.1',8066))
while True:
    inp = input('>>>')
    if not inp:
        continue
    body_info = inp.encode('utf-8')
    head = {'body_length':len(body_info)}
    head_info = json.dumps(head).encode('utf-8')
    head_length = struct.pack('i',len(head_info))
    sk.send(head_length)
    sk.send(head_info)
    sk.send(body_info)
sk.close()
```

### 8.7 并发问题

#### 8.7.1 IO模型

1. 阻塞IO模型

2. 非阻塞IO模型

   非阻塞模式解决了多个客户端同时连接的问题。但提高了CPU的占用率，耗费了大量时间在异常处理。

   ```python
   #服务端
   import socket
   import time
   
   lst = [] #连接客户端列表
   del_lst = []#关闭连接的客户端列表
   
   sk = socket.socket()
   sk.setblocking(False)#设置socket为非阻塞模式，默认True
   sk.bind(('127.0.0.1',8066))
   sk.listen()
   while True:
       try:
           conn,addr = sk.accept()#非阻塞，无客户端连接会报错
           lst.append(conn)
       except BlockingIOError:
           for i in lst:
               try:
                   msg = i.recv(1024).decode('utf-8')#非阻塞，客户端无消息发送来，会报错
                   if not msg:  #在window操作系统下，客户端关闭后仍然会接收空消息
                       del_lst.append(i)
                       continue
                   print('>>>',msg)
                   i.send(msg.upper().encode('utf-8'))
                   time.sleep(1)
               except BlockingIOError:pass
           for i in del_lst:
               i.close()
               lst.remove(i) #移除掉关闭的客户端
           del_lst.clear()
   
   sk.close()
   ```

   ```python
   #客户端
   import time
   import socket
   sk = socket.socket()
   sk.connect(('127.0.0.1',8066))
   for i in range(5):
       sk.send(b'abc')
       msg = sk.recv(1024).decode('utf-8')
       print(msg)
       time.sleep(1)
   sk.close()
   ```

3. 事件驱动IO模型

4. IO多路复用模型

5. 异步IO模型

#### 8.7.2 socketserver模块

socketserver模块底层调用了socket模块，解决了客户端的并发连接问题。

```python
#服务端
import time
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):
        #print(self.request) #conn对象
        while True:
            msg = self.request.recv(1024).decode('utf-8')
            if not msg: #客户端连接关闭后，msg为空
                self.request.close()
                break
            print('>>>',msg)
            self.request.send(msg.upper().encode('utf-8'))
            time.sleep(1)

sk = socketserver.ThreadingTCPServer(('127.0.0.1',8066),Myserver)
sk.serve_forever()#启动服务端，有客户端连接，会执行handle方法
```

```python
#客户端
import time
import socket

sk = socket.socket()
sk.connect(('127.0.0.1',8066))
for i in range(5):
    sk.send('sss'.encode('utf-8'))
    msg = sk.recv(1024).decode('utf-8')
    print(msg)
    time.sleep(1)
sk.close()
```

### 8.8 验证客户端的合法性

- 如果客户端是给客户用的，那么就没有必要验证客户端的合法性，只需要登录验证即可。

- 如果客户端是提供给机器使用的话，就需要验证客户端的合法性

  ```python
  #服务端
  import socket,os,hashlib
  
  sk = socket.socket()
  sk.bind(('127.0.0.1',8066))
  sk.listen()
  
  conn,addr = sk.accept()
  key = '123456' #和客户端使用相同的秘钥
  s = os.urandom(16) #服务端随机生成字符串，并发送给客户端
  conn.send(s)
  msg = conn.recv(1024).decode('utf-8') #接收客户端发来的验证串，比较是否和服务端一致
  obj = hashlib.md5(key.encode('utf-8'))
  obj.update(s)
  info = obj.hexdigest()
  if msg == info:
      conn.send('验证成功'.encode('utf-8'))
  else:
      conn.send('验证失败'.encode('utf-8'))
  conn.close()
  sk.close()
  ```

  ```python
  #客户端
  import socket,hashlib
  
  sk = socket.socket()
  sk.connect(('127.0.0.1',8066))
  
  msg = sk.recv(16)
  key = '123456'
  obj = hashlib.md5(key.encode('utf-8'))
  obj.update(msg)
  info = obj.hexdigest()
  sk.send(info.encode('utf-8'))
  msg = sk.recv(1024)
  print(msg.decode('utf-8'))
  sk.close()
  ```

  

## 九、并发编程

### 9.1 基础知识

#### 9.1.1 操作系统发展史

##### 9.1.1.1 人机矛盾

- cpu利用率低

##### 9.1.1.2 磁带存储+批处理

- 降低数据的读取时间
- 提高cpu利用率

##### 9.1.1.3 多道操作系统

- 数据隔离
- 时空复用
- 能够在一个任务遇到io操作时，主动把cpu让出来给其他任务使用。
- 任务切换过程也占用时间（操作系统来进行切换）

##### 9.1.1.4 分时操作系统

给时间分片，让多个任务轮流使用cpu

- 时间分片
  - cpu的轮转
  - 每个程序分配一个时间片
- 切换占用了时间，降低了cpu的利用率
- 提高了用户体验

##### 9.1.1.5 分时操作系统+多道操作系统

多个程序一起在计算机中执行

- 一个程序如果遇到了io操作，就切出去让出cpu
- 一个程序没有遇到io操作，但时间片到了，就让出cpu

补充：python中的分布式框架celery

#### 9.1.2 进程

##### 9.1.2.1 定义

运行中的程序。

##### 9.1.2.2 程序和进程的区别

程序：是一个文件。

进程：这个文件被cpu运行起来了

##### 9.1.2.3 基础知识

- 进程是计算机中最小的资源分配单位
- 进程在操作系统中的唯一标识符：pid
- 操作系统调度进程的算法
  - 短作业优先算法
  - 先来先服务算法
  - 时间片轮转算法
  - 多级反馈算法

##### 9.1.2.4 并发和并行区别

并行：两个程序，两个cpu，每个程序分别占用一个cpu，自己执行自己的。

并发：多个程序，一个cpu，多个程序交替在一个cpu上执行，看起来在同时执行，但实际上仍然是串行。

#### 9.1.3 同步和异步

同步：一个动作执行时，要想执行下一个动作，必须将上一个动作停止。

异步：一个动作执行的同时，也可以执行另一个动作

#### 9.1.4 阻塞和非阻塞

阻塞：cpu不工作

非阻塞：cpu工作

同步阻塞：如conn.revc()

同步非阻塞：调用一个func()函数，函数内没有io操作

异步非阻塞：把func()函数(无io操作)放到其他任务里去执行，自己执行自己的任务。

异步阻塞：

#### 9.1.5 进程的三状态图

![](E:\Python\2019-05-10\day31\进程三状态图.png)





## 十、数据库

## 十一、前端开发

## 十二、Django框架

## 附录 常见错误和单词

### 单词

| 单词        | 解释   | 备注 |
| ----------- | ------ | ---- |
| preview     | 预览   |      |
| iterable    | 迭代   |      |
| refactor    | 重构   |      |
| indentation | 缩进   |      |
| generator   | 生成器 |      |
| recursion   | 递归   |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |



### 常见错误

