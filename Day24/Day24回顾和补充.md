# Day24回顾和补充

## 1.logging模块

目的：记录日志

用户：记录用户的一些操作

程序员：

- 用于统计
- 用来做故障排除的debug
- 用来记录错误完成代码优化

通过logger对象实现日志

```python
#创建logger对象
logger = logging.getLogger('mylog') 
#设置日志等级
logger.setLevel(logging.ERROR)
#创建文件操作符
fh = logging.FileHandler(filename='log.log',mode='a',encoding='utf-8')
#创建屏幕操作符
sh = logging.StreamHandler()
#创建格式化对象
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s")
#将格式化对象绑定到文件操作符
fh.setFormatter(formatter)
#将格式化对象绑定到屏幕操作符
sh.setFormatter(formatter)
#将文件操作符添加到logger对象
logger.addHandler(fh)
#将屏幕操作符添加到logger对象
logger.addHandler(sh)

logger.error('123')#2019-04-29 15:19:25,594 - mylog - ERROR - test: 123
```

## 2.collections模块

### 2.1 OrderedDict 有序字典

```python
from collections import OrderedDict

dic = OrderedDict([('a',1),('b',2),('c',3)])
for k,v in dic.items():
    print(k,v) #a 1   b 2    c 3
```

### 2.2 namedtuple  可命名元组

创建一个类这个类没有方法，所有属性都不能修改

```python
from collections import namedtuple

Course = namedtuple('Course',['name','price','teacher'])
python = Course('python',19800,'alex')
print(python) #Course(name='python', price=19800, teacher='alex')
print(python.name) #python 
print(python.teacher) #alex
```

### 2.3 deque 双端队列

### 2.4 defaultDict 默认字典

可以给字典的value设置一个默认值

## 3.反射

通过本文件来获取本文件的任意变量

```python
getattr(sys.module[__name__],'变量名')
```

sys.module 存储了当前程序用到的所有模块

## 4.hashlib 摘要算法模块

### 4.1 md5校验文件一致性

```python
import hashlib

obj = hashlib.md5()
obj.update('123'.encode('utf-8'))  #可以一次update完，也可以分多次update，结果相同
print(obj.hexdigest()) #比较最终的结果是否相同验证文件一致性
```

### 4.2 sha算法 

用法和md5相同，通常使用sha1

## 5.random模块

```python
import random
v = random.choice([1,2,3,4,5]) #随机选取一个
print(v) #1
v1 = random.sample([1,2,3,4,5],3) #随机选取多个
print(v1) #[2, 5, 3]
v2 = random.uniform(1,5) #随机获取一个小数
print(v2) #2.906728453875117
lst = [1,2,3,4,5]
random.shuffle(lst) #打乱顺序
print(lst) #[1, 4, 5, 3, 2]
```

## 6.面向对象

### 6.1 基本概念

- 什么是类：具有相同属性和方法的一类事物。
- 什么是对象或实例：一个具有具体属性和动作的具体个体。

### 6.2 多继承

查找方式：

- py2：深度优先
- py3：广度优先   同过C3算法计算查找顺序

### 6.3 封装

广义：将数据和方法放入类中

狭义：私有成员，不允许外部访问

### 6.4 新式类和经典类

- 新式类：
  - 支持super
  - 广度优先
  - 有mro方法
- 经典类：
  - 不支持super
  - 深度优先
  - 没有mro方法

继承顺序可以通过mro方法获得

```python
class A(object):
    def func(self):
        print('A')

class B(A):
    def func(self):
        print('B')

class C(A):
    def func(self):
        print('C')

class D(B,C):
    def func(self):
        print('D')

print(D.mro())#[<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>]
```

super的查找顺序和mro方法得到的顺序相同

### 6.5 面试题

```python
class A(object):
    def __init__(self):
        self.__func()   #私有成员会变式成_A__func()
        
    def func(self):
        print('A')
        
class B(object):
    def func(self):
        print('B')
        
B().func() #打印A
```

