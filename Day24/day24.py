#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第二十四天作业
王凯旋
'''

# 1.计算任意一个文件夹的大小（考虑绝对路径的问题）
#     # 基础需求
#         # 这个文件夹中只有文件
#     # 进阶需求
#         # 这个文件夹中可能有文件夹，并且文件夹中还可能有文件夹...不知道有多少层
'''
import os
def get_dir_size(abspath):
    result = 0
    for a,b,c in os.walk(abspath):
        for i in c:
            path = os.path.join(a,i)
            result += os.stat(path).st_size

    return result

print(get_dir_size(r'E:\Oracle学习'))
'''

# 2.校验大文件的一致性
#     # 基础需求
#         # 普通的文字文件
#     # 进阶需求
#         # 视频或者是图片

# 普通文本文件
'''
import hashlib
def check_agreed(filepath1,filepath2):
    with open(filepath1,mode='r',encoding='utf-8') as f1,open(filepath2,mode='r',encoding='utf-8') as f2:
        obj1 = hashlib.md5()
        obj2 = hashlib.md5()
        
        for line in f1:
            obj1.update(line.encode())

        code1 = obj1.hexdigest()

        for line in f2:
            obj2.update(line.encode())

        code2 = obj2.hexdigest()

        return code1 == code2

print(check_agreed(r'E:\Oracle学习\test.txt',r'E:\test.txt'))
'''

# 视频或者是图片
'''
import os
import hashlib
def check_agreed(filepath1,filepath2):
    with open(filepath1,mode='rb') as f1,open(filepath2,mode='rb') as f2:
        pre_size = 1024*1024
        obj1 = hashlib.md5()
        obj2 = hashlib.md5()
        file_size1 = os.stat(filepath1).st_size
        file_size2 = os.stat(filepath2).st_size
        read_size1 = 0
        read_size2 = 0
        while read_size1 < file_size1:
            content1 = f1.read(pre_size)
            read_size1 += len(content1)
            obj1.update(content1)

        code1 = obj1.hexdigest()

        while read_size2 < file_size2:
            content2 = f2.read(pre_size)
            read_size2 += len(content2)
            obj2.update(content2)

        code2 = obj2.hexdigest()

        return code1 == code2

print(check_agreed(r'E:\Oracle学习\test.mp4',r'E:\test.mp4'))
'''

# # 3.发红包
#     # 每一个人能够抢到的金额的概率都是平均的
#     # 小数的不准确性的问题
'''
import random
def func(num,count):
    lst = []
    re_num = int(num * 100)
    re_count = count
    for i in range(count-1):
        end = int(re_num * 2 / re_count)
        now_num = random.randint(0,end)
        if now_num == 0:
            now_num = 1
        lst.append(now_num)
        re_num -= now_num
        re_count -= 1
    lst.append(re_num)
    random.shuffle(lst)
    return lst

def run():
    lst = func(100,10)
    for i in lst:
        print('%.2f'%(i/100))
run()
'''



# 4.根据代码研究super和mro的关系
'''
class A:
    def func(self):
        print('in a')

class B(A):
    def func(self):
        print('in b')
        super().func()

class C(A):
    def func(self):
        print('in c')
        super().func()

class D(B,C):
    def func(self):
        print('in d')
        super().func()

d = D()
d.func()
print(D.mro())

#super的查找方式和mro方法得到的继承顺序相同
'''
