from django.shortcuts import render,redirect,HttpResponse
from django.views import View

class Login(View):

    def get(self,request,*args,**kwargs):
        return render(request,'login.html')

    def post(self,request,*args,**kwargs):
        url = request.GET.get('return_url')
        username = request.POST.get('username')
        pwd = request.POST.get('pwd')
        if username == 'alex' and pwd == '123':
            if url:
                response = redirect(url)
            else:
                response = redirect('/cookie_demo/index/')
            # response.set_cookie('is_login','1')
            response.set_signed_cookie('is_login','1',salt='s21')
            return response

        return render(request,'login.html',{'error':'用户名或密码错误'})

def wrapper(func):
    def inner(request,*args,**kwargs):
        # is_login = request.COOKIES.get('is_login')
        is_login = request.get_signed_cookie('is_login', default='', salt='s21')
        if not is_login:
            return redirect('/cookie_demo/login/?return_url={}'.format(request.path_info))
        ret = func(request,*args,**kwargs)
        return ret
    return inner


@wrapper
def index(request):
    return HttpResponse('index')

@wrapper
def home(request):
    return HttpResponse('home')


def logout(request):
    ret = redirect('/cookie_demo/login/')
    ret.delete_cookie('is_login')
    return ret