#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.conf.urls import url
from cookie_demo import views

urlpatterns = [
    url('login/$',views.Login.as_view()),
    url('index/$',views.index),
    url('home/$',views.home),
    url('logout/$',views.logout),
]