# Day63

## 今日内容

1. cookie
2. session

## 内容详细

### 1.cookie

#### 1.1 定义

保存在浏览器本地的一组组键值对

#### 1.2 特点

- 是服务器让浏览器进行设置的
- 保存在浏览器本地
- 下次访问自动携带

#### 1.3 应用

- 登录验证
- 保存浏览习惯
- 简单的投票

**注：默认在浏览器关闭后cookie就失效了。**

#### 1.4 django使用cookie

1. 获取cookie

   ```python
   #方式一
   value = request.COOKIES['key']   #key不存在会报错
   value = request.COOKIES.get('key')  #key不存在返回None
   #方式二
   value = request.get_signed_cookie('key',salt='盐',default='',max_age='20') #获取设置时加密的cookie
   '''
   参数：
   salt:设置cookie时的盐
   default:获取不到返回的默认值
   max_age:过期时间
   '''
   ```

2. 设置cookie

   ```python
   #方式一
   response.set_cookie(key,value,...)
   #方式二
   response.set_signed_cookie(key,value,salt='加密盐',...)
   ```

   set_cookie()参数：

   - key, 键
   - value='', 值
   - max_age=None, 超时时间
   - expires=None, 超时时间(IE requires expires, so set it if hasn't been already.)
   - path='/', Cookie生效的路径，/ 表示根路径，特殊的：根路径的cookie可以被任何url的页面访问
   - domain=None, Cookie生效的域名
   - secure=False, https传输
   - httponly=False 只能http协议传输，无法被JavaScript获取（不是绝对，底层抓包可以获取到也可以被覆盖）

3. 删除cookie

   ```python
   response.delete_cookie("user")  # 删除用户浏览器上之前设置的user的cookie值
   ```

### 2.session

#### 2.1 定义

保存在服务器上的一组组键值对，必须依赖cookie使用。

#### 2.2 为什么要有session？

- cookie保存在浏览器本地，不安全
- 大小受限制

#### 2.3 session原理

设置session时，会生成一个sessionid并把它放到cookie中返回给浏览器，sessionid对应的数据存放在服务器上，下次请求会携带sessionid，然后找到服务器上对应的session，取出里面的值。

#### 2.4 django使用session

```python
#获取session
value = request.session['key']
value = request.session.get('key')

#设置session
request.session['key'] = value
request.session.setdefault('key',value) #存在则不设置

#获取会话session的key(sessionid)
request.session.session_key

#删除所有数据库中过期的session 
request.session.clear_expired()

#检查session的session_key在数据库中是否存在
request.session.exists("session_key")

#删除数据库中的数据，不会删除cookie
request.session.delete()

#删除数据库和cookie数据，一般在注销时使用
request.session.flush()

#设置过期时间
request.session.set_expiry(value)
'''
 * 如果value是个整数，session会在些秒数后失效。
 * 如果value是个datatime或timedelta，session就会在这个时间后失效。
 * 如果value是0,用户关闭浏览器session就会失效。
 * 如果value是None,session会依赖全局session失效策略。
'''
```

**注：默认两星期过期**