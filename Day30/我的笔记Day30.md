# Day30

## 今日内容

1. 阻塞和非阻塞
2. 验证客户端的合法性
3. socketserver

## 回顾

tcp特点：面向连接，流式的，可靠的，慢，全双工通信。

udp特点：是一个面向数据报，不可靠，快的，能完成一对一，一对多，多对多的高效通讯协议。（视频的在线观看）

## 内容详细

### 1.IO模型

1. 阻塞IO模型

2. 非阻塞IO模型

   非阻塞模式解决了多个客户端同时连接的问题。但提高了CPU的占用率，耗费了大量时间在异常处理。

   ```python
   #服务端
   import socket
   import time
   
   lst = [] #连接客户端列表
   del_lst = []#关闭连接的客户端列表
   
   sk = socket.socket()
   sk.setblocking(False)#设置socket为非阻塞模式，默认True
   sk.bind(('127.0.0.1',8066))
   sk.listen()
   while True:
       try:
           conn,addr = sk.accept()#非阻塞，无客户端连接会报错
           lst.append(conn)
       except BlockingIOError:
           for i in lst:
               try:
                   msg = i.recv(1024).decode('utf-8')#非阻塞，客户端无消息发送来，会报错
                   if not msg:  #在window操作系统下，客户端关闭后仍然会接收空消息
                       del_lst.append(i)
                       continue
                   print('>>>',msg)
                   i.send(msg.upper().encode('utf-8'))
                   time.sleep(1)
               except BlockingIOError:pass
           for i in del_lst:
               i.close()
               lst.remove(i) #移除掉关闭的客户端
           del_lst.clear()
   
   sk.close()
   ```

   ```python
   #客户端
   import time
   import socket
   sk = socket.socket()
   sk.connect(('127.0.0.1',8066))
   for i in range(5):
       sk.send(b'abc')
       msg = sk.recv(1024).decode('utf-8')
       print(msg)
       time.sleep(1)
   sk.close()
   ```

3. 事件驱动IO模型

4. IO多路复用模型

5. 异步IO模型

### 2.验证客户端的合法性

- 如果客户端是给客户用的，那么就没有必要验证客户端的合法性，只需要登录验证即可。

- 如果客户端是提供给机器使用的话，就需要验证客户端的合法性

  ```python
  #服务端
  import socket,os,hashlib
  
  sk = socket.socket()
  sk.bind(('127.0.0.1',8066))
  sk.listen()
  
  conn,addr = sk.accept()
  key = '123456' #和客户端使用相同的秘钥
  s = os.urandom(16) #服务端随机生成字符串，并发送给客户端
  conn.send(s)
  msg = conn.recv(1024).decode('utf-8') #接收客户端发来的验证串，比较是否和服务端一致
  obj = hashlib.md5(key.encode('utf-8'))
  obj.update(s)
  info = obj.hexdigest()
  if msg == info:
      conn.send('验证成功'.encode('utf-8'))
  else:
      conn.send('验证失败'.encode('utf-8'))
  conn.close()
  sk.close()
  ```

  ```python
  #客户端
  import socket,hashlib
  
  sk = socket.socket()
  sk.connect(('127.0.0.1',8066))
  
  msg = sk.recv(16)
  key = '123456'
  obj = hashlib.md5(key.encode('utf-8'))
  obj.update(msg)
  info = obj.hexdigest()
  sk.send(info.encode('utf-8'))
  msg = sk.recv(1024)
  print(msg.decode('utf-8'))
  sk.close()
  ```

#### 2.1 hmac模块

```python
import hmac,os

secret_key = '123456'
random_msg = os.urandom(16) #得到16位随机字节串
obj = hmac.new(secret_key.encode('utf-8'),random_msg)
print(obj.digest())# 得到加密后的字节串 b': \x98`\xa6\xdb\xcdF\\ \xae\xbfR\x0e\x1a\xc1'
```

### 3.socketserver模块

socketserver模块底层调用了socket模块，解决了客户端的并发连接问题。

```python
#服务端
import time
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):
        #print(self.request) #conn对象
        while True:
            msg = self.request.recv(1024).decode('utf-8')
            if not msg: #客户端连接关闭后，msg为空
                self.request.close()
                break
            print('>>>',msg)
            self.request.send(msg.upper().encode('utf-8'))
            time.sleep(1)

sk = socketserver.ThreadingTCPServer(('127.0.0.1',8066),Myserver)
sk.serve_forever()#启动服务端，有客户端连接，会执行handle方法
```

```python
#客户端
import time
import socket

sk = socket.socket()
sk.connect(('127.0.0.1',8066))
for i in range(5):
    sk.send('sss'.encode('utf-8'))
    msg = sk.recv(1024).decode('utf-8')
    print(msg)
    time.sleep(1)
sk.close()
```



