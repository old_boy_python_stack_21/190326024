#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

BASE_PATH = os.path.dirname(os.path.dirname(__file__))
SERVER_IP = '127.0.0.1'
SERVER_PORT = 8066
OPER_LOG_PATH = os.path.join(BASE_PATH,'log','operlog.log')
ERROR_LOG_PATH = os.path.join(BASE_PATH,'log','errorlog.log')
OPER_LOG_WHEN = 'd'
ERROR_LOG_WHEN = 'd'
OPER_INTERVAL = 1
ERROR_INTERVAL = 1
USER_INFO_PATH = os.path.join(BASE_PATH,'db','userinfo.txt')