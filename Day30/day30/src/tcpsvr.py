#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import hashlib
import socketserver
from lib.encrypt import encrypt
from config import settings
from lib.file_oper import read_file
from src.tcpbase import TcpBase
from lib.jsonhelper import json_from_file,json_to_file

class Tcpsvr(TcpBase):
    def __init__(self):
        self.conn = None
        self.user_name = ''
        self.msg = None

    def base(arg):
        def inner(self):
            if self.user_name:
                self.base_send({'method':self.msg['method'],'code':1,'info':''})
                arg(self)
            else:
                self.base_send({'method':self.msg['method'],'code':0,'info':'请先进行登录'})
        return inner

    def login(self):
        dic = self.msg
        pwd = encrypt(dic['user_pwd'])
        user_list = json_from_file(settings.USER_INFO_PATH)
        for i in user_list:
            if dic['user_name'] == i['user_name'] and pwd == i['user_pwd']:
                self.base_send({'method':'login','code':1,'info':'登录成功'})
                self.user_name = dic['user_name']
                break
        else:
            self.base_send({'method':'login','code':0,'info':'用户名或密码错误，请重新输入'})

    def register(self):
        user_list = json_from_file(settings.USER_INFO_PATH)
        for i in user_list:
            if i['user_name'] == self.msg['user_name']:
                self.base_send({'method':'register','code':0,'info':'用户名已存在，请重新输入'})
                break
        else:
            user_list.append({'user_name':self.msg['user_name'],'user_pwd':encrypt(self.msg['user_pwd'])})
            json_to_file(user_list,settings.USER_INFO_PATH)
            self.base_send({'method':'register','code':1,'info':'注册成功'})


    @base
    def download(self):
        dic = self.base_recv()
        if dic['is_return']:
            return
        file_path = r'E:\Python\2019-05-08\day29\1.内容回顾.mp4'
        file_name = os.path.basename(file_path)
        file_size = os.path.getsize(file_path)
        self.base_send({'file_name':file_name,'file_size':file_size})
        f = read_file(file_path)
        obj = hashlib.md5()
        for i in f:
            obj.update(i)
            self.conn.send(i)
        msg = self.base_recv()['info']
        if msg == obj.hexdigest():
            self.base_send({'method':'download','code':1,'info':'文件下载成功！'})
        else:
            self.base_send({'method':'download','code':0,'info':'文件下载失败！'})

    @base
    def upload(self):
        dic = self.base_recv()
        if dic['is_return']:
            return
        file_size = dic['file_size']
        file_name = dic['file_name']
        file_path = os.path.join(r'E:\svrdownload',file_name)
        f = open(file_path,mode='wb')
        read_size = 0
        obj = hashlib.md5()
        while read_size < file_size:
            msg = self.conn.recv(1024 * 1024)
            obj.update(msg)
            read_size += len(msg)
            f.write(msg)
            f.flush()
        f.close()
        msg = self.base_recv()['info']
        if msg == obj.hexdigest():
            self.base_send({'method':'upload','code':1,'info':'文件上传成功！'})
        else:
            self.base_send({'method':'upload','code':0,'info':'文件上传失败！'})

    def exit(self):
        self.conn.close()
        self.user_name = ''
        self.conn = None

    def start(self,conn):
        self.conn = conn
        while self.conn:
            dic = self.base_recv()
            self.msg = dic
            getattr(self,dic['method'])()



class MyServer(socketserver.BaseRequestHandler):
    def handle(self):
        tcp_cli = Tcpsvr()
        tcp_cli.start(self.request)

def run():
    sk = socketserver.ThreadingTCPServer((settings.SERVER_IP, settings.SERVER_PORT), MyServer)
    sk.serve_forever()