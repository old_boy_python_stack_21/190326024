#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket, sys, os, hashlib
from lib.file_oper import read_file
from src.tcpbase import TcpBase
from lib.tools import processbar
from config import settings


class Tcpcli(TcpBase):

    def __init__(self, ip, port):
        self.dic = {'1': 'login','2':'register', '3': 'upload', '4': 'download', '5': 'exit'}
        self.ip = ip
        self.port = port
        self.conn = None
        self.is_login = False

    def login(self):
        if self.is_login:
            print('您已经登录过了')
            return
        while True:
            print('用户登录'.center(16, '*'))
            user_name = input('请输入用户名(输入Q/q返回上一层)：')
            if user_name.upper() == 'Q':
                return
            user_pwd = input('请输入密码：')
            self.base_send({'method':'login','user_name':user_name,'user_pwd':user_pwd})
            msg = self.base_recv()
            if msg['code'] == 1:
                self.is_login = True
                print(msg['info'])
                break
            else:
                print(msg['info'])

    def register(self):
        while True:
            print('用户注册'.center(20,'*'))
            user_name = input('请输入用户名(输入Q/q返回)：')
            if user_name.upper() == 'Q':
                return
            user_pwd = input('请输入密码：')
            self.base_send({'method':'register','user_name':user_name,'user_pwd':user_pwd})
            msg = self.base_recv()
            print(msg['info'])

    def upload(self):
        dic = {'method': 'upload'}
        self.base_send(dic)
        msg = self.base_recv()
        if not msg['code']:
            print(msg['info'])
            return
        print('文件上传'.center(20, '*'))
        while True:
            file_path = input('请输入要上传的文件路径(输入Q/q退出)：')
            if file_path.upper() == 'Q':
                self.base_send({'method':'upload','file_name': '', 'file_size': '', 'is_return':1})
                return
            if not os.path.isfile(file_path):
                print('输入的不是一个文件，请重新输入')
                continue
            file_name = os.path.basename(file_path)
            file_size = os.path.getsize(file_path)
            self.base_send({'method':'upload','file_name': file_name, 'file_size': file_size,'is_return':0})
            f = read_file(file_path)
            print('文件上传中...')
            obj = hashlib.md5()
            read_size = 0
            for i in f:
                obj.update(i)
                self.conn.send(i)
                read_size += len(i)
                processbar(read_size,file_size)
            self.base_send({'info':obj.hexdigest()})
            msg = self.base_recv()
            print(msg['info'])
            return

    def download(self):
        dic = {'method': 'download'}
        self.base_send(dic)
        msg = self.base_recv()
        if not msg['code']:
            print(msg['info'])
            return
        print('文件下载'.center(20, '*'))
        while True:
            file_path = input('请输入存储路径(输入Q/q退出)：')
            if file_path.upper() == 'Q':
                self.base_send({'method':'download', 'is_return':1})
                return
            if not os.path.isdir(file_path):
                print('输入的不是一个路径，请重新输入')
                continue
            self.base_send({'method':'download', 'is_return':0})
            msg = self.base_recv()
            file_name = msg['file_name']
            file_size = msg['file_size']
            full_path = os.path.join(file_path, file_name)
            f = open(full_path, mode='wb')
            read_size = 0
            print('文件下载中...')
            obj = hashlib.md5()
            while read_size < file_size:
                info = self.conn.recv(1024 * 1024)
                obj.update(info)
                read_size += len(info)
                processbar(read_size,file_size)
                f.write(info)
                f.flush()
            f.close()
            self.base_send({'info': obj.hexdigest()})
            msg = self.base_recv()
            print(msg['info'])
            return

    def exit(self):
        content = {'method': 'exit'}
        self.base_send(content)
        self.conn.close()
        sys.exit(0)

    def start(self):
        self.conn = socket.socket()
        self.conn.connect((self.ip, self.port))
        while True:
            print('欢迎使用文件上传下载系统'.center(20, '*'))
            print('1.用户登录\n2.用户注册\n3.上传文件\n4.下载文件\n5.退出系统')
            inp = input('请选择：')
            if inp not in self.dic:
                print('输入有误，请重新输入')
                continue
            getattr(self, self.dic[inp])()

def run():
    tcp_cli = Tcpcli(settings.SERVER_IP, settings.SERVER_PORT)
    tcp_cli.start()
