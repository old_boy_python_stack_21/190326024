#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

def read_file(path):
    total_size = os.path.getsize(path)
    read_size = 0
    with open(path, mode='rb') as f:
        while read_size < total_size:
            con = f.read(1024 * 1024)
            read_size += len(con)
            yield con
