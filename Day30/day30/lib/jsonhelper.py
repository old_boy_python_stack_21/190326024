#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import json
from config.settings import BASE_PATH

def json_to_file(obj,path):
    with open(path,mode='w',encoding='utf-8') as f:
        json.dump(obj,f,ensure_ascii=False)

def json_from_file(path):
    with open(path, mode='r', encoding='utf-8') as f:
        return json.load(f)