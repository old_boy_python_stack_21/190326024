# Day14

## 今日内容

1. 带参数的装饰器
   - 应用：flask框架 + django缓存 + 面试题
2. 模块

## 补充

1. 写代码的方式
   - 面向过程编程
   - 面向函数编程
   - 面向对象编程

2. 对函数的默认值慎用可变类型

   ```python
   def func(data,value=[]):   #不推荐
       pass
   
   def func(data,value=None): #推荐
       if not value:
           value = []
   ```

   ```python
   def func(data,value=[]): #value在函数加载时就被定义出来 
       value.append(data)
       return value
   
   v1 = func(1) 
   v2 = func(1,[11,22,33])
   v3 = func(2)
   print(v1,v2,v3)  #[1,2]  [11,22,33,1] [1,2]
   ```

3. 闭包

   闭包：封装值+内层函数使用

   ```python
   #不是闭包
   def func(name):
       def inner():
           return 123
       return inner
   
   #是闭包
   def func(name):
       def inner():
           return name
       return inner
   ```

4. 递归函数

   递归函数：函数自己调用自己，内存无法释放，效率低。python递归调用默认最大次数为1000次

   ```python
   def func():
       print(123)
       func()
   func()
   ```

## 内容详细

### 1.带参数的装饰器

```python
####################普通装饰器###################
def func(arg):
    def inner(*args,**kwargs):
        print('before')
        data = arg(*args,**kwargs)
        print('after')
        return data
    return inner

#第一步执行 v1 = func(index)
#第二步 index = v1
@func
def index():
    print(123)
    
####################带参数的装饰器###################
def base(value):
    def func(arg):
        def inner(*args,**kwargs):
            print(value)
            data = arg(*args,**kwargs)
            print('after')
            return data
        return inner
    return func

#第一步 执行v1 = base(5)
#第二步 ret = v1(index)
#第三步 index = ret
@base(5)
def index():
    print(123)
    
```

### 2.元数据和多个装饰器（欠）

### 3.模块

#### 3.1 sys 模块    

python解释器相关的内容。

- sys.getrefcount()   获取变量被引用的次数

  ```python
  import sys
  value = []
  print(sys.getrefcount(value)) #2
  ```

  

- sys.getrecursionlimit()     获取python默认支持的递归次数

  ```python
  import sys
  print(sys.getrecursionlimit()) #1000
  ```

- sys.stdout.write()    输出（不换行）

  ```python
  import sys
  sys.stdout.write("123")
  sys.stdout.write("123") #123123
  ```

  注：\r  将光标移动到本行的开头，可用于打印进度百分比

  ```python
  import time
  
  for i in range(1, 101):
      msg = "%s%%" % i
      print(msg, end='')
      time.sleep(0.1)
  ```

- sys.argv   获取用户执行脚本时输入的参数，用于命令行操作脚本

  ```python
  # C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
  # sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
  #print(sys.argv[1])  #D:/test
  ```

  

- sys.path （欠）

#### 3.2 os 模块

和操作系统相关

- os.path.exists()    判断路径是否存在

  ```python
  import os
  print(os.path.exists('data.py')) #false
  ```

- os.stat(文件名).st_size  获取文件大小

  ```python
  import os
  print(os.stat('goods.txt').st_size)
  ```

- os.path.abspath(文件名)     获取文件的绝对路径

  ```python
  import os
  print(os.path.abspath('goods.txt')) #E:\code\day10\goods.txt
  ```

- os.path.dirname(路径)  获取路径的上级目录

  ```python
  import os
  v = r'E:\code\day10\goods.txt'  #r转义
  print(os.path.dirname(v)) #E:\code\day10
  ```

- os.path.join(*args)  连接多个路径

  ```python
  import os
  
  v = os.path.join('E:\code\day10', '123', '34')
  print(v) #E:\code\day10\123\34
  ```

- os.listdir(路径)    获取该路径下的所有文件，只包括当前层

  ```python
  import os
  
  v = os.listdir('E:\code\day10')
  print(v) #['.idea', '123', 'day10.py', 'day11.py', 'day12.py', 'day13.py', 'goods.txt', 'test.py', 'test1.py', '__pycache__', '面试题.py']
  ```

- os.walk(路径)   获取路径下的所有文件，遍历所有层 ，返回生成器

  - for循环生成器 每次循环得到三个元素，第一个元素：正在查看的目录  第二个元素：此目录下的文件夹

    第三个元素：此目录下的文件

  ```python
  result = os.walk(r'D:\code\s21day14')
  for a,b,c in result:
  # a,正在查看的目录 b,此目录下的文件夹 c,此目录下的文件
  	for item in c:
  		path = os.path.join(a,item)
  		print(path)
  ```

3.3 shutil 模块

- shutil.rmtree(路径)   删除目录下的所有文件和目录

  ```python
  import shutil
  
  shutil.rmtree('E:/code/day10')
  ```

## 总结

1. 普通装饰器
   - 参数
   - 返回值
   - 执行前后
2. 带参数的装饰器
3. 模块
   - random
   - hashlib
   - getpass
   - os(重要)
   - time(重要)
   - sys
   - shutil

 