#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第十四天作业
王凯旋
'''

# 1.为函数写一个装饰器，在函数执行之后输入 after
'''


def base(arg):
    def inner(*args, **kwargs):
        data = arg(*args, **kwargs)
        print("after")
        return data

    return inner


@base
def func():
    pass
'''

# 2.为函数写一个装饰器，把函数的返回值 +100 然后再返回。
'''


def base(arg):
    def inner(*args, **kwargs):
        data = arg(*args, **kwargs)
        return data + 100

    return inner


@base
def func():
    pass
'''

# 3.为函数写一个装饰器，根据参数不同做不同操作。
# flag为True，则 让原函数执行后返回值加100，并返回。
# flag为False，则 让原函数执行后返回值减100，并返回
'''


def x(flag):
    def base(arg):
        def inner(*args, **kwargs):
            if flag:
                return arg(*args, **kwargs) + 100
            return arg(*args, **kwargs) - 100

        return inner

    return base


@x(True)
def f1():
    return 11


@x(False)
def f2():
    return 22


r1 = f1()
'''

# 4.写一个脚本，接收两个参数。
#
#     第一个参数：文件
#     第二个参数：内容
#
# 请将第二个参数中的内容写入到 文件（第一个参数）中。
'''
import sys

v1 = sys.argv[0]
v2 = sys.argv[1]
with open(v1, mode='a', encoding='utf-8') as obj:
    obj.write(v2)
    obj.flush()
'''

# 5.递归的最大次数是多少？
'''
1000
'''

# 6.看代码写结果
'''
print("你\n好")
print("你\\n好")
print(r"你\n好")
'''

'''
结果：你
     好
     你\n好
     你\n好
'''

# 7.写函数实现，查看一个路径下所有的文件【所有】。
'''
import os


def func(path):
    value = os.walk(path)
    for a, b, c in value:
        for item in c:
            temp = os.path.join(a, item)
            print(temp)
'''

# 8.写代码 请根据path找到code目录下所有的文件【单层】，并打印出来
# path = r"D:\code\test.pdf"
'''
import os
path = r"D:\code\test.pdf"
val = os.path.dirname(path)
for item in os.listdir(val):
    print(item)
'''
# 9.写代码实现【题目1】和【题目2】
# 题目一
'''


def func():
    a = 0
    b = 1
    index = 0
    while b < 4000000:
        a, b = b, a + b
        index += 1
    print(index)
    print(b)
'''

# 题目二
'''
dicta = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'f': 'hello'}
dictb = {'b': 3, 'd': 5, 'e': 7, 'm': 9, 'k': 'world'}
v1 = set(dicta.keys())
v2 = set(dictb.keys())
v3 = v1.union(v2)
dic = {}
for i in v3:
    vala = dicta.get(i)
    valb = dictb.get(i)
    if type(vala) == type(valb):
        dic[i] = vala + valb
    elif not vala:
        dic[i] = valb
    else:
        dic[i] =vala 

print(dic)
'''

# 10.看代码写结果
'''
结果：[10,'a']  [123,]  [10,'a']

'''

# 11.实现如下面试题
# 1.
'''
ABC
'''

# 2.
'''
tupleA = ('a', 'b', 'c', 'd', 'e')
tupleB = (1, 2, 3, 4, 5)
res = {}
for i in range(len(tupleA)):
    res[tupleA[i]] = tupleB[i]
print(res)
'''

# 3.
'''
import sys
sys.argv
'''

# 4.
'''
ip = '192.168.0.100'
print([int(i) for i in ip.split('.')])
'''

# 5.
'''
AList = ['a','b','c']
print(','.join(AList))
'''

# 6.
'''
StrA = '1234A6FASKKSJJLSKWLM<SJKL90'
print(StrA[-2:])
print(StrA[1])
print(StrA[2])
'''

# 7.
'''
Alist = [1,2,3,1,3,1,2,1,3]
print(Alist[0:3])
'''

# 编程题
# 1.
''''''
import os

'''
def func(path):
    value = os.walk(path)
    for a, b, c in value:
        for i in c:
            temp = os.path.join(a, i)
            print(temp)

'''

# 2.
'''

for i in range(1, 1000):
    lst = []
    for item in range(1, i + 1):
        if i % item == 0:
            lst.append(item)
    lst.remove(i)
    if sum(lst) == i:
        print(i)
'''

# 4.
'''


def func(lst1, lst2):
    for i in range(0, len(lst1)):
        if lst1[i] < lst2[i]:
            return lst2
        return lst1
'''

# 5.

'''

with open('etl_log.txt', mode='r', encoding='utf-8') as f:
    for line in f:
        print(line)
'''
