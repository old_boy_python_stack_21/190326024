# Day61

## 今日内容

1. ORM字段和参数
2. 表的参数
3. 数据操作
4. 单表的双下划线
5. 外键操作

## 内容详细

### 1.ORM字段和参数

#### 1.1 字段

- AutoField，自增字段，设置primary_key=True后代表主键字段。一张表里只能有一个AutoField字段。

  ```python
  id = models.AutoField(primary_key = True)
  ```

- IntegerField，整型

  ```
  age = models.IntegerField()
  ```

- DateField  日期类型    DateTimeField 日期时间类型

  ```python
  birth = models.DateField(auto_now_add = True)
  #auto_now_add  为True，新增数据时自动添加当前日期（UTC时间）
  #auto_now   为True，新增或修改数据时都会更新该字段
  ```

- DecimalField  十进制小数

  ```python
  price = models.DecimalField(max_digits=5,decimal_places=2)
  #max_digits  小数总长度
  #decimal_places  小数部分长度
  ```

- BooleanField  布尔值字段，只有0和1两个值

- TextField  文本字段

- 自定义字段

- 在models.py文件中添加类：

  ```python
  #定义一个Char类型字段
  class MyCharField(models.Field):
      """
      自定义的char类型的字段类
      """
      def __init__(self, max_length, *args, **kwargs):
          self.max_length = max_length
          super(MyCharField, self).__init__(max_length=max_length, *args, **kwargs)
   
      def db_type(self, connection):
          """
          限定生成数据库表的字段类型为char，长度为max_length指定的值
          """
          return 'char(%s)' % self.max_length
      
  #创建表，使用自定义字段   
  class Person(models.Model):
      name = MyCharField(max_length = 10)
  ```

#### 1.2 参数

- null 表示一个字段是否可以为空，默认为False
- db_column  数据库中字段的名称
- unique  是否唯一
- db_index  是否创建索引
- defalut  默认值
- verbose_name      Admin中显示的字段名称
- blank          Admin中是否允许用户输入为空
- editable            Admin中是否可以编辑
- help_text           Admin中该字段的提示信息
- choices             Admin中显示选择框的内容，用不变动的数据放在内存中从而避免跨表操作

### 2.表的参数

```python
class Person(models.Model):
    name = models.CharField(max_length=12)index
    age = models.IntegerField()
    
    #配置表信息
    class Meta:
        db_table = '表名'  #表名
        index_together = [('name','age')] #联合索引
        unique_together = [('name','age')] #联合唯一索引
        
```

### 3.数据操作

则pyCharm中通过脚本文件操作数据

```python
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings") #可以按照manage.py中写

import django
django.setup()

from appPublisher import models
#数据操作
```



- all()  获取所有数据

- get()  获取满足条件的一条数据，没有或多条数据会报错

- filter()  获取满足条件的所有数据

- exclude()  获取不满足条件的所有数据

- values()  拿到对象的所有字段和字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values()
  print(obj)
  #<QuerySet [{'pid': 2, 'name': '北京理工出版社', 'phone': '18836325896', 'addr': '北京市房山区良乡'}, {'pid': 5, 'name': '清华大学出版社', 'phone': '13399900888', 'addr': '北京市海淀区'}, {'pid': 7, 'name': '路飞出版社', 'phone': '13589456983', 'addr': 'ad请问'}]>
  obj_list = models.Publisher.objects.filter(name='出版社').values()
  ```

- values('字段名称',...)  拿到对象指定的字段和字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values('name')
  print(obj)
  #<QuerySet [{'name': '北京理工出版社'}, {'name': '清华大学出版社'}, {'name': '路飞出版社'}]>
  ```

- values_list()  拿到对象的所有字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values_list()
  print(obj)
  #<QuerySet [(2, '北京理工出版社', '18836325896', '北京市房山区良乡'), (5, '清华大学出版社', '13399900888', '北京市海淀区'), (7, '路飞出版社', '13589456983', 'ad请问')]>
  ```

- values_list('字段名称',...)   拿到对象的指定字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values_list('name','phone')
  print(obj)
  #<QuerySet [('北京理工出版社', '18836325896'), ('清华大学出版社', '13399900888'), ('路飞出版社', '13589456983')]>
  ```

- order_by()  排序，默认升序。降序要在字段前面加负号。也可以按照多个字段排序

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj_list = models.Publisher.objects.all().order_by('-pk')
  obj_list = models.Publisher.objects.all().order_by('name','-pk')
  ```

- reverse()  反向排序，必须对已排序的QuerySet使用

  ```python
  ...
  obj_list = models.Publisher.objects.values().order_by('-pk').reverse()
  ```

- distinct()  去重，必须完全相同的内容才能去重

  ```python
  ...
  obj_list = models.Publisher.objects.values('name').distinct
  ```

- count()  计算结果集元素个数

  ```python
  num = models.Publisher.objects.all().count()
  ```

- first() 取查询结果集的第一个元素，没有则返回None

  ```python
  ...
  obj = models.Publisher.objects.values('name').first()
  ```

- last()  取查询结果集的最后一个元素，没有则返回None

- exists()  判断查询的数据是否存在

  ```python
  ...
  obj = models.Publisher.objects.filter(pk=1).exists()#True
  ```

### 4.单表的双下划线

- gt 大于

  ```python
  ret = models.Person.objects.filter(pk__gt=1)
  ```

- lt 小于

  ```python
  ret = models.Person.objects.filter(pk__lt=1)
  ```

- gte 大于等于

- lte  小于等于

- range = [m,n]  范围 m=< 值<=n

  ```python
  ret = models.Person.objects.filter(pk__range=[2,5])
  ```

- in = [1,2,3]   在...中，成员判断

  ```python
  ret = models.Person.objects.filter(pk__in=[1,2,3])
  ```

- contains  包含，相当于like 区分大小写

  ```python
  ret = models.Person.objects.filter(name__contains='a')
  ```

- icontains  包含，不区分大小写

  ```python
  ret = models.Person.objects.filter(name__icontains='a')
  ```

- startswith   以...开头 区分大小写

  ```python
  ret = models.Person.objects.filter(name__startswith='a')
  ```

- istartswith   以...开头 不区分大小写

  ```python
  ret = models.Person.objects.filter(name__istartswith='a')
  ```

- endswith   以...结尾 区分大小写

- iendswith   以...结尾 不区分大小写

- year  筛选年

  ```python
  ret = models.Person.objects.filter(birth__year='2019')
  ```

- isnull   是否为空

  ```python
  ret = models.Person.objects.filter(name__isnull=True)
  ```

### 5.外键操作

#### 5.1 基于对象的查询

表关系(多对一)：

```python
#出版社（一）
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

#书籍（多）
class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE)
```

- 正向查询

  ```python
  book_obj = models.Book.object.get(pk=1)
  book_obj.pub.pid
  ```

- 反向查询

  ```python
  pub_obj = models.Publisher.objects.get(pk=1)
  pub_obj.book_set.all()  #book_set多对一关系管理对象，可以在建立外键字段时设置related_name属性来设置管理对象名称
  ```

```python
class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE,related_name='books')

#使用
pub_obj = models.Publisher.objects.get(pk=1)
pub_obj.books.all()
```

#### 5.2 基于字段查询

```python
#  查询老男孩出版的书
ret = models.Book.objects.filter(pub__name='老男孩出版社') 


# 查询出版菊花怪大战MJJ的出版社
# related_name='books' 指定了管理对象名
ret= models.Publisher.objects.filter(books__title='菊花怪大战MJJ')

#没有指定related_name   类名的小写
ret= models.Publisher.objects.filter(book__title='菊花怪大战MJJ')
```







