#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第二十六天作业
王凯旋
'''

# 1.匹配一篇英文文章的标题 类似 The Voice Of China
'''
([A-Z][a-z]{0,} ?)+
'''

# 2.匹配一个网址
# 类似 https://www.baidu.com http://www.cnblogs.com
'''
https?://www.[a-zA-Z]+?.com
'''

# 3.匹配年月日日期 类似 2018-12-06 2018/12/06 2018.12.06
'''
\d{4}[-//.]\d{2}[-//.]\d{2}
'''

# 4.匹配15位或者18位身份证号
'''
[1-9]\d{14}(\d{2}[\dx])?
'''

# 5.从lianjia.html中匹配出标题，户型和面积，结果如下：
# [('金台路交通部部委楼南北大三居带客厅   单位自持物业', '3室1厅', '91.22平米'), ('西山枫林 高楼层南向两居 户型方正 采光好', '2室1厅', '94.14平米')]
"""
import re

# f = open(r'C:\\Users\wkx\Desktop\day26作业\day26作业\lianjia.html',mode='r',encoding='utf-8')
content = f.read()
f.close()

pattern = '<div class="title">.*?data-sl="">' \
          '(?P<title>.*?)</a>.*?<span class="divide">/</span>(?P<house>.*?)' \
          '<.*?</span>(?P<area>.*?)<'
ret = re.finditer(pattern,content,flags=re.S)
lst = []
for i in ret:
    lst.append((i.group('title'),i.group('house'),i.group('area')))

print(lst)
"""

# 6. 1-2*((60-30+(-40/5)*(9-2*5/3+7/3*99/4*2998+10*568/14))-(-4*3)/(16-3*2))
# # 从上面算式中匹配出最内层小括号以及小括号内的表达式
'''
\([^\(\)]+\)
'''

# 7.从类似9-2*5/3+7/3*99/4*2998+10*568/14的表达式中匹配出乘法或除法
'''
-?\d+((/|\*)-?\d+)+
'''

# 8.通读博客，完成三级菜单
'''
menu = {
    '北京': {
        '海淀': {
            '五道口': {
                'soho': {},
                '网易': {},
                'google': {}
            },
            '中关村': {
                '爱奇艺': {},
                '汽车之家': {},
                'youku': {},
            },
            '上地': {
                '百度': {},
            },
        },
        '昌平': {
            '沙河': {
                '老男孩': {},
                '北航': {},
            },
            '天通苑': {},
            '回龙观': {},
        },
        '朝阳': {},
        '东城': {},
    },
    '上海': {
        '闵行': {
            "人民广场": {
                '炸鸡店': {}
            }
        },
        '闸北': {
            '火车战': {
                '携程': {}
            }
        },
        '浦东': {},
    },
    '山东': {},
}

lst = [menu]

while lst:
    for i in lst[-1]:
        print(i)
    con = input('请输入：')
    if con.upper() == 'Q':
        break
    if con.upper() == 'B':
        lst.pop()
    elif lst[-1].get(con):
        lst.append(lst[-1][con])
'''
# 9、大作业：计算器
# 1)如何匹配最内层括号内的表达式
# 2)如何匹配乘除法
# 3)考虑整数和小数
# 4)写函数，计算‘2*3’ ‘10/5’
# 5)引用4)中写好的函数，完成'2*3/4'计算
'''
1)
\([^\(\)]+\)

2)
-?\d+((/|\*)-?\d+)+

3)
-?\d+(\.\d+)?((/|\*)-?\d+(\.\d+)?)+

4)

import re
def func1(content):
    lst = re.split('([*/])',content)
    if lst[1].strip() == '*':
        return str(float(lst[0].strip())*float(lst[2].strip()))
    elif lst[1].strip() == '/':
        return str(float(lst[0].strip())/float(lst[2].strip()))

#5)
def func2(content):
    ret = re.search('-?\d+(\.\d+)?((/|\*)-?\d+(\.\d+)?)+',content)
    while ret:
        print(ret.group())
        content = re.sub('-?\d+(\.\d+)?((/|\*)-?\d+(\.\d+)?)+?',func1(ret.group()),content,1)
        ret = re.search('-?\d+(\.\d+)?((/|\*)-?\d+(\.\d+)?)+', content)
    print(content)

func2('-2*3/-4')
'''

