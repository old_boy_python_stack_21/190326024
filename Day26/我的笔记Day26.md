# Day26

## 今日内容

1. 转义符
2. re模块：python中的方法+正则表达式新内容
   - 分组
   - 分组命名
   - 引用分组

3. 爬虫小例子

## 内容详细

### 1.转义符

正则表达式的转义符在python的字符串中也有转义的作用，但正则表达式中的转义符和字符串中的转义符并没有关系，且还容易产生冲突。为了避免冲突我们所有的正则都以在工具中的测试结果为结果，然后只需要在正则和待匹配的字符串前面加r即可。

```python
import re
ret = re.search(r'\\n',r'\n')
print(ret.group())
```

### 2.正则模块

#### 2.1 几个方法

1. findall()

   ```python
   import re
   ret = re.findall('\d','alex83') #第一个参数正则表达式，第二个参数待匹配的字符串，返回一个列表，未匹配到返回空列表
   print(ret) #['8','3']
   ```

2. search()

   ```python
   import re
   ret = re.search('\d','alex83')#若匹配到则返回一个对象，否则返回None,只取第一个符合条件的项
   print(ret) #<_sre.SRE_Match object; span=(4, 5), match='8'>
   if ret:
   	print(ret.group())#8
   ```

3. match()

   ```python
   #只能从头开始匹配，相当于search的正则表达式加上一个^
   import re
   ret = re.match('\d','1alex83')#若匹配到则返回一个对象，否则返回None,只取第一个符合条件的项
   print(ret) #<_sre.SRE_Match object; span=(0, 1), match='1'>
   if ret:
   	print(ret.group())#1
   ```

4. finditer()

   ```python
   import re
   ret = re.finditer('\d','alex83') #返回一个迭代器
   for i in ret: #循环迭代器返回一个对象
       print(i.group())
   ```

5. compite()

   将正则表达式提前编译好，后面重复调用该正则规则时可以节省时间和空间。

   ```python
   import re
   ret = re.compile('\d')
   res = ret.findall('alex83')
   print(res)
   
   res = ret.search('alex83')
   print(res.group())
   
   res = ret.finditer('alex83')
   for i in res:
       print(i.group())
   ```

   compile的flags参数：

   ```python
   flags = re.S  #.可以匹配任意字符
   ```

6. split()

   ```python
   import re
   ret = re.split('\d+','alex83wusir74taibai40') #以正则表达式匹配的内容进行分割
   print(ret)#['alex', 'wusir', 'taibai', '']
   
   #split 默认保留分组中的内容
   ret = re.split('(\d+)','alex83wusir74taibai40')
   print(ret)#['alex', '83', 'wusir', '74', 'taibai', '40', '']
   ```

7. sub()和subn()

   ```python
   import re
   ret = re.sub('\d','D','alex83wusir74taibai40',3) #替换，默认全部替换，返回替换后的字符
   print(ret)# alexDDwusirD4taibai40
   
   ret = re.subn('\d','D','alex83wusir74taibai40',3)#替换，默认全部替换，返回替换后的字符和替换了几个字符
   print(ret)# ('alexDDwusirD4taibai40', 3)
   ```

#### 2.2 分组

无分组

```python
import re
s = '<h1>wahaha</h1>'
ret = re.search('<\w+>.*?</\w+>',s)
print(ret.group())
```

分组

```python
import re
s = '<h1>wahaha</h1>'
ret = re.search('<(\w+)>(.*?)</\w+>',s)
print(ret.group(0)) #<h1>wahaha</h1>  等同于ret.group()默认参数为0，表示取整个匹配项
print(ret.group(1)) #h1  取第一个分组内的内容
print(ret.group(2)) #wahaha 取第二个分组内的内容
```

对于findall()遇到正则表达式中的分组，会优先显示分组中的内容

```python
import re

ret = re.findall('\d+(\.\d+)?','1.234+2')
print(ret) #['.234', ''] 优先显示分组中的内容，所以2不会显示
```

取消分组优先显示 ?:

```python
import re

ret = re.findall('\d+(?:\.\d+)?','1.234+2')
print(ret)
```

应用：取字符串里的所有整数

```python
import re

ret = re.findall('\d+\.\d+|(\d+)','1-2*(60+(-40.35/5)-(-4*3))')#首先把整数和小数都取出来，优先显示整数
print(ret) #['1', '2', '60', '', '5', '4', '3']
ret.remove('') #移除''
print(ret)#['1', '2', '60', '5', '4', '3']
```

#### 2.3 分组命名

（?P<名字>正则表达式）

```python
import re

s = '<h1>wahaha</h1>'
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s)
print(ret.group('tag'))
print(ret.group('cont'))
```

#### 2.4 引用分组

这个组的内容必须和引用的分组的内容完全一致

```python
import re
s = '<h1>wahaha</h1>'
s1 = '<h1>wahaha</h2>'
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s)
ret1 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s1)
print(ret.group())#<h1>wahaha</h1>
print(ret1.group())#报错
```

### 3.爬虫小例子

```python
import re
import json
import requests


def get_req(url):
    ret = requests.get(url)
    return ret.text


def get_con(ret, content):
    res = ret.finditer(content)
    for i in res:
        yield {
            'id':i.group('id'),
            'title':i.group('title'),
            'score':i.group('score'),
            'comment':i.group('comment')
        }




def write_file():
    with open('moviefile',mode='w',encoding='utf-8') as f:
        while True:
            dic = yield
            f.write(dic+'\n')


pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>\w+)' \
          '</span>.*?<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment>\d+)人评价'

res = re.compile(pattern, flags=re.S)

num = 0

f = write_file()

next(f)

for i in range(10):
    num = 25 * i
    con = get_req('https://movie.douban.com/top250?start=%s&filter=' % num)
    gc = get_con(res, con)
    for j in gc:
        f.send(json.dumps(j,ensure_ascii=False)#生成器的send方法会将参数赋给上一次执行的yield的变量，然后在执行下一次yield
        
f.close()#手动关闭生成器
```

## 总结

模块中的方法：

- findall()
- search()
- match()
- finditer()
- compile()
- split()
- sub()
- subn()

分组：

- 分组
- 分组命名
- 引用分组

正则的？都能做什么？

- ? 量词，0个或一个

- 用于量词后，非贪婪匹配
- ?P<> 用于分组命名
- ?P=   用于引用分组
- ?： 取消分组优先显示

