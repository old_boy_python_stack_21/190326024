# Day49

## 今日内容

1. jquery简介
2. 基础选择器
3. 事件
4. 设置样式
5. 设置类
6. 设置属性
7. 基本过滤选择器
8. 筛选选择器
9. 动画

## 内容详细

### 1.jQuery简介

jQuery是一个快速，小巧，功能丰富的JavaScript库。

它通过易于使用的API在大量浏览器中运行，使得Html文档遍历和操作，事件处理，动画和AJAX变得更加简单。通过多功能性和可扩展性的结合，jQuery改变了数百万人编写JavaScript的方式。

操作：获取节点元素对象，属性操作，样式操作，类名，节点的创建，删除，添加和替换。

核心：write less,do more

### 2.基础选择器

#### 2.1 基础选择器

- id选择器

  ```js
  $('#box')//得到一个jquery对象
  ```

- 类选择器

  ```js
  $('.box')
  ```

- 标签选择器

  ```js
  $('div')
  ```

- 后代选择器

  ```js
  $('#box .active')
  ```

- 子代选择器

  ```js
  $('#id>.active')
  ```

- 属性选择器

  ```js
  $('input[type=submit]')
  ```

#### 2.2 jquery对象和js对象的相互转换

- jquery对象转化js对象

  ```js
  $('#id')[0]//通过对jquery对象加索引，取到的每一个元素就是js对象
  ```

- js对象转化成jquery对象

  ```js
  var div = document.getElementById('box');
  $(div)//使用$可以将js对象转换成jquery对象
  ```

### 3.事件

- 单击事件

  ```js
  //自动遍历所有选择的节点元素，为它们绑定事件
  $('.box').click(function(){
      console.log($(this));//回调函数内部的this指向js节点元素本身
  })
  ```

- 鼠标悬浮事件

  ```js
  $('.box').mouseover(function(){
      console.log('悬浮');
  })
  ```

- 鼠标离开事件

  ```js
  $('.box').mouseout(function(){
      console.log('离开');
  })
  ```

- 聚焦事件

  ```js
  $('input').focus(function(){
       console.log('聚焦');
  })
  ```

- 失焦事件

  ```js
  $('input').blur(function(){
       console.log('失焦');
  })
  ```

- 内容改变事件

  可用于单选，复选，下拉列表，input输入框内容的改变

  ```js
  $('input').change(function(){
       console.log($(this).val());//通过val()方法获取input的value值。通过text()方法和html()方法获取双闭合标签内部的文本或html
  })
  ```

### 4.设置样式

```js
//设置单个样式
$(this).css('color,red')
$(this).css('font-size,12px')
//设置多个样式
$(this).css({'color':'red','font-size':12})
//获取样式
var ret = $(this).css('color')
```

### 5.设置类

```js
//为节点元素添加类
$(this).addClass('active')
//为节点元素移除类
$(this).removeClass('active')
```

### 6.设置属性

- 设置属性

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<ul>
  			<li class="a">mjj</li>
  			<li class="b">wusir</li>
  			<li class="c">alex</li>
  			<li class="d">sb</li>
  		</ul>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  		 	$('ul li').attr('index',1);//设置所有匹配节点元素的属性和值
              console.log($('ul li').attr('index'));//这能获取匹配到的第一个节点元素的属性值
  		</script>
  	</body>
  </html>
  ```

- 移除属性

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<ul>
  			<li index="1" class="a">mjj</li>
  			<li index="2" class="b">wusir</li>
  			<li index="3" class="c">alex</li>
  			<li index="4" class="d">sb</li>
  		</ul>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  		 	$('ul li:odd').removeAttr('index');//为每一个匹配的节点元素移除一个属性
  		</script>
  	</body>
  </html>
  ```

### 7.基本过滤选择器

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<ul>
			<li class="a">
				<a href="#">
					mjj
				</a>
			</li>
			<li class="b">wusir</li>
			<li class="c">alex</li>
			<li class="d">sb</li>
		</ul>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
		 	console.log($('ul li:eq(1)'));//过滤出索引等于1的li标签
		 	console.log($('ul li:gt(1)'));//过滤出索引大于1的li标签
			console.log($('ul li:lt(1)'));//过滤出索引小于1的li标签
            console.log($('ul li:odd'));//过滤出所有索引为奇数的li标签
            console.log($('ul li:even'));//过滤出所有索引为偶数的li标签
			console.log($('ul li:first'));//过滤出第一个li标签
			console.log($('ul li:last'));//过滤出最后一个li标签
		</script>
	</body>
</html>
```

### 8.筛选选择器

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<ul>
			<li id="first" class="a">
				<a href="#">
					mjj
				</a>
			</li>
			<li class="b">wusir</li>
			<li class="c">alex</li>
			<li class="d">sb</li>
            <a href=""></a>
		</ul>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			// 筛选方法
			// 即选择儿子又选择孙子....
			console.log($('ul').find('a'));//后代筛选器
			// 只选择亲儿子
			console.log($('ul').children());//子代选择器
			console.log($('li a').parent());//父代选择器
            console.log($('ul #first').siblings('li'));//兄弟选择器(不包括自己)，选择li的所有兄弟li节点元素，
		</script>
	</body>
</html>
```

**补充：**

```js
console.log(document);//获取文档对象
console.log(document.body);//获取body对象
console.log(document.documentElement);//获取html对象
```

### 9.动画

#### 9.1 显示和隐藏

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<style>
			#box{
				width: 200px;
				height: 200px;
				background-color: red;
				display: none;
			}
		</style>
	</head>
	<body>
		<button id="btn">显示</button>
		<div id="box"></div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$('#btn').click(function(){
                if($(this).text() === '显示'){
				 	$('#box').show(1000,function(){
						$('#btn').text('隐藏');
					});//显示动画，回调函数在动画执行完成后会执行
				}else{
					$('#box').hide(1000,function(){
						$('#btn').text('显示');
					});//显示动画，回调函数在动画执行完成后会执行

				}
				//$('#box').stop().toggle(2000);//toggle相当于一个开关在显示和隐藏之间来回切换，但快速切换，动画响应不及时会产生bug，所以在每次切换前调用stop方法结束上次未结束的动画效果。
			})
		</script>
	</body>
</html>
```

#### 9.2 滑入和滑出

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<style>
			#box{
				width: 200px;
				height: 200px;
				background-color: red;
				display: none;
			}
		</style>
	</head>
	<body>
		<button id="btn">显示</button>
		<div id="box"></div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$('#btn').click(function(){
                if($(this).text() === '显示'){
				 	$('#box').slideDown(1000,function(){
						$('#btn').text('隐藏');
					});

				}else{

					$('#box').slideUp(1000,function(){
						$('#btn').text('显示');
					});

				}

				//$('#box').stop().slideToggle(2000);

			})
		</script>
	</body>
</html>
```

#### 9.3 淡入和淡出

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<style>
			#box{
				width: 200px;
				height: 200px;
				background-color: red;
				display: none;
			}
		</style>
	</head>
	<body>
		<button id="btn">显示</button>
		<div id="box"></div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$('#btn').click(function(){
                if($(this).text() === '显示'){
				 	$('#box').fadeIn(1000,function(){
						$('#btn').text('隐藏');
					});

				}else{

					$('#box').fadeOut(1000,function(){
						$('#btn').text('显示');
					});

				}

				//$('#box').stop().fadeToggle(2000);

			})
		</script>
	</body>
</html>
```

#### 9.4 自定义动画

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <style>
        div {
            position: absolute;
            left: 20px;
            top: 30px;
            width: 100px;
            height: 100px;
            background-color: green;
        }
    </style>
    <script src="js/jquery.js"></script>
    <script>
        jQuery(function () {
            $("button").click(function () {
                var json = {"width": 500, "height": 500, "left": 300, "top": 300, "border-radius": 100};
                var json2 = {
                    "width": 100,
                    "height": 100,
                    "left": 100,
                    "top": 100,
                    "border-radius": 100,
                    "background-color": "red"//颜色对动画不起作用，需要使用插件
                };

                //自定义动画
                $("div").animate(json, 5000, function () {
                    $("div").animate(json2, 1000, function () {
                        alert("动画执行完毕！");
                    });
                });//第一个参数为动画执行完整后的样式，第二个参数为执行时间，第三个参数为动画执行完成后的回调函数

            })
        })
    </script>
</head>
<body>
<button>自定义动画</button>
<div></div>
</body>
</html>
```

