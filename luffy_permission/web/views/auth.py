#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.shortcuts import redirect, render, reverse
from permission import models
from permission.services.permission_init import permission_init


def login(request):
    error = ''
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        obj = models.User.objects.filter(username=username, password=password).first()
        if obj:
            permission_init(request,obj)
            return redirect(reverse('index'))
        else:
            error = '用户名或密码错误'
    return render(request, 'login.html', {'error': error})


def index(request):
    return render(request, 'index.html')
