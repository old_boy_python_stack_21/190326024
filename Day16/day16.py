#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第十六天作业
王凯旋
'''

# 1.列举你常见的内置函数。
'''
len(),int(),list(),set(),print(),input()
'''

# 2.列举你常见的内置模块？
'''
time(),datetime(),json(),os(),sys()
'''

# 3.json序列化时，如何保留中文？
'''
import json
lst = [123,'中国']
print(json.dumps(lst,ensure_ascii=False))
'''

# 4.程序设计：用户管理系统
"""
功能：
	1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
	2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。
"""
'''
from datetime import datetime
import json


def main():
    dic = {'1': user_reg, '2': user_login}
    lst = ['********用户管理系统********', '1.用户注册', '2.用户登录']
    while True:
        for item in lst:
            print(item)
        index = input('请选择,输入N/n退出程序:')
        if index.upper() == 'N':
            return
        ret = dic.get(index)
        if not ret:
            print('输入错误,请重新输入')
            continue
        ret()


def user_reg():
    dic = {}
    old_content = read_file('user_info.txt').strip()
    if old_content:
        dic = json.loads(old_content)
    while True:
        user_name = input('请输入用户名(输入N/n返回上一层)：')
        if user_name.upper() == 'N':
            new_content = json.dumps(dic, ensure_ascii=False)
            write_file(new_content, 'user_info.txt')
            return
        user_pwd = input('请输入密码：')
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        dic[user_name] = {'password':user_pwd,'date':date}
        print('注册成功！')



def user_login():
    temp_account_dic = {}
    user_info_dic = {}
    account_info_dic = {}
    user_info_content = read_file('user_info.txt').strip()
    account_info_content = read_file('account_info.txt').strip()
    if user_info_content:
        user_info_dic = json.loads(user_info_content)
    if  account_info_content:
        account_info_dic = json.loads(account_info_content)

    while True:
        user_name = input('请输入用户名(输入N/n返回上一层)：')
        if user_name.upper() == 'N':
            write_file(json.dumps(account_info_dic, ensure_ascii=False), 'account_info.txt')
            return
        if account_info_dic.get(user_name) == 0:
            print('该账户已被冻结')
            continue
        if not user_info_dic.get(user_name):
            print('该用户不存在，请重新输入')
            continue
        user_pwd = input('请输入密码：')
        if user_info_dic.get(user_name).get('password') == user_pwd:
            print('登录成功')
            write_file(json.dumps(account_info_dic, ensure_ascii=False), 'account_info.txt')
            return
        temp_account_dic[user_name] = temp_account_dic.get(user_name, 3) - 1
        if temp_account_dic.get(user_name) == 0:
            print('三次机会已全部用完，账户被冻结')
            account_info_dic[user_name] = 0
            continue
        print('用户名或密码错误，请重新输入（还有%s次机会）' % (temp_account_dic.get(user_name),))



def write_file(content, file_name='user_info.txt'):
    try:
        with open(file_name, mode='w', encoding='utf-8') as f:
            f.write('%s\n' % content)
            f.flush()
    except Exception as e:
        return False
    return True


def read_file(file_name='user_info.txt'):
    try:
        with open(file_name, mode='r', encoding='utf-8') as f:
            content = f.read()
        return content
    except Exception as e:
        return None


main()
'''


# 5.有如下文件，请通过分页的形式将数据展示出来。【文件非常小】
'''
商品|价格
飞机|1000
大炮|2000
迫击炮|1000
手枪|123
'''
'''


def main():
    while True:
        ret = input('请输入要查看的页码（输入N/n退出）：')
        if ret.upper() == 'N':
            return
        if not ret.isdecimal():
            print('输入错误，请重新输入')
            continue
        val = query_goods(int(ret))
        if not val:
            print('页码超出范围，请重新输入')


def query_goods(page):
    pre_page_count = 2
    with open('goods.txt', mode='r', encoding='utf-8') as f:
        title = f.readline()
        content = f.read()
        title_list = title.split('|')
        content_list = []
        for item in content.split('\n'):
            content_list.append(item)
        total, v = divmod(len(content_list), pre_page_count)
        if v > 0:
            total += 1
        if page < 1 or page > total:
            return False
        start = (page - 1) * pre_page_count
        end = page * pre_page_count
        lst = content_list[start:end]
        print(title_list[0]+"  "+title_list[1])
        for i in lst:
            temp = i.split('|')
            print(temp[0] + "  " + temp[1])
        return True

main()

'''

# 6.有如下文件，请通过分页的形式将数据展示出来。【文件非常大】
'''
商品|价格
飞机|1000
大炮|2000
迫击炮|1000
手枪|123
'''

'''
import sys

LOCATION = 0

def main():
    query_goods()
    while True:
        input("输入N/n查看下一页:")
        query_goods()

def query_goods():
    with open('goods.txt', mode='r', encoding='utf-8') as f:
        title = f.readline().strip()
        title_list = title.split('|')
        temp = f.tell()
        content = ''
        global LOCATION
        if not LOCATION:
            f.seek(temp)
        else:
            f.seek(LOCATION,0)
        for i in range(2):
            content += f.readline()
        LOCATION = f.tell()
        content_list = []
        if not content :
            print('全部查看完毕')
            sys.exit()
        for item in content.strip().split('\n'):
            content_list.append(item)
        print(title_list[0]+"  "+title_list[1])
        for i in content_list:
            temp = i.split('|')
            print(temp[0] + "  " + temp[1])

main()
'''

# 7.程序设计：购物车
'''
有如下商品列表 GOODS_LIST，用户可以选择进行购买商品并加入到购物车 SHOPPING_CAR 中且可以选择要购买数量，
购买完成之后将购买的所有商品写入到文件中【文件格式为：年_月_日.txt】。

注意：重复购买同一件商品时，只更改购物车中的数量。
"""
# 购物车
SHOPPING_CAR = {}

# 商品列表
GOODS_LIST = [
    {'id':1,'title':'飞机','price':1000},
	{'id':3,'title':'大炮','price':1000},
	{'id':8,'title':'迫击炮','price':1000},
	{'id':9,'title':'手枪','price':1000},
]
'''

'''
import json
from datetime import datetime

GOODS_LIST = [
    {'id': 1, 'title': '飞机', 'price': 1000},
    {'id': 3, 'title': '大炮', 'price': 1000},
    {'id': 8, 'title': '迫击炮', 'price': 1000},
    {'id': 9, 'title': '手枪', 'price': 1000},
]

SHOPPING_CAR = {}

def main():
    while True:
        print('**********欢迎光临**********')
        message = '%s  %s  %s'
        print(message%('id','名称','价格'))
        id_list=[]
        for item in GOODS_LIST:
            id_list.append(str(item.get('id')))
            print(message%(item.get('id'),item.get('title'),item.get('price')))
        id = input('请输入产品ID(输入N/n购买完成)：')
        if id.upper() == 'N':
            if SHOPPING_CAR != {}:
                writr_file()
            return
        if id not in id_list:
            print('产品不存在，请重新输入')
            continue
        count = input('请输入要购买的数量：')
        if not count.isdecimal():
            print('购买数量有误,请重新输入')
            continue
        SHOPPING_CAR[str(id)] = SHOPPING_CAR.get(str(id),0) + int(count)

def writr_file():
    file_name = datetime.now().strftime('%Y-%m-%d') + '.txt'
    with open(file_name,mode='w',encoding='utf-8') as f:
        content = json.dumps(SHOPPING_CAR,ensure_ascii=False)
        f.write(content)
        f.flush()

main()
'''

# 8.程序设计：沙河商城
'''
功能：
	1.用户注册，提示用户输入用户名和密码，然后获取当前注册时间，最后将用户名、密码、注册时间写入到文件。
	2.用户登录，只有三次错误机会，一旦错误则冻结账户（下次启动也无法登录，提示：用户已经冻结）。
	3.商品浏览，分页显示商品（小文件）； 用户可以选择商品且可以选择数量然后加入购物车（在全局变量操作），
	  不再购买之后，需要讲购物车信息写入到文件，文件要写入到指定目录：
		shopping_car(文件夹)
			- 用户名A(文件夹)
				2019-11-11-09-59.txt
				2019-11-12-11-56.txt
				2019-12-11-11-47.txt
			- 用户B(文件夹)
				2019-11-11-11-11.txt
				2019-11-12-11-15.txt
				2019-12-11-11-22.txt
	  注意：重复购买同一件商品时，只更改购物车中的数量。
	4.我的购物车，查看用户所有的购物车记录，即：找到shopping_car目录下当前用户所有的购买信息，并显示：
		2019-11-11-09-59
			飞机|1000|10个
			大炮|2000|3个
		2019-11-12-11-56.txt
			迫击炮|10000|10个
			手枪|123|3个
			
	5.用户未登录的情况下，如果访问 商品流程 、我的购物车 时，提示登录之后才能访问，让用户先去选择登录（装饰器实现）。
'''

'''
from datetime import datetime
import json
import os

INDEX_LIST = []
SHOPPING_CAR = {}
NOW_SHOW = {}
USER_NAME = ''
IS_LOGIN = False


def func(arg):
    def inner(*args, **kwargs):
        if not IS_LOGIN:
            print('未登录，请登录')
            return
        arg(*args, **kwargs)

    return inner


def main():
    dic = {'1': user_reg, '2': user_login, '3': show_goods, '4': show_shopping_car}
    lst = ['********用户管理系统********', '1.用户注册', '2.用户登录', '3.商品浏览', '4.我的购物车']
    while True:
        for item in lst:
            print(item)
        index = input('请选择,输入N/n退出程序:')
        if index.upper() == 'N':
            return
        ret = dic.get(index)
        if not ret:
            print('输入错误,请重新输入')
            continue
        ret()


def user_reg():
    dic = {}
    old_content = read_file('user_info.txt').strip()
    if old_content:
        dic = json.loads(old_content)
    while True:
        user_name = input('请输入用户名(输入N/n返回上一层)：')
        if user_name.upper() == 'N':
            new_content = json.dumps(dic, ensure_ascii=False)
            write_file(new_content,'w', 'user_info.txt')
            return
        user_pwd = input('请输入密码：')
        date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        dic[user_name] = {'password':user_pwd,'date':date}
        print('注册成功！')


def user_login():
    temp_account_dic = {}
    user_info_dic = {}
    account_info_dic = {}
    user_info_content = read_file('user_info.txt').strip()
    account_info_content = read_file('account_info.txt').strip()
    if user_info_content:
        user_info_dic = json.loads(user_info_content)
    if  account_info_content:
        account_info_dic = json.loads(account_info_content)

    while True:
        user_name = input('请输入用户名(输入N/n返回上一层)：')
        if user_name.upper() == 'N':
            write_file(json.dumps(account_info_dic, ensure_ascii=False),'w', 'account_info.txt')
            return
        if account_info_dic.get(user_name) == 0:
            print('该账户已被冻结')
            continue
        if not user_info_dic.get(user_name):
            print('该用户不存在，请重新输入')
            continue
        user_pwd = input('请输入密码：')
        if user_info_dic.get(user_name).get('password') == user_pwd:
            print('登录成功')
            global USER_NAME
            global IS_LOGIN
            USER_NAME = user_name
            IS_LOGIN = True
            write_file(json.dumps(account_info_dic, ensure_ascii=False),'w', 'account_info.txt')
            return
        temp_account_dic[user_name] = temp_account_dic.get(user_name, 3) - 1
        if temp_account_dic.get(user_name) == 0:
            print('三次机会已全部用完，账户被冻结')
            account_info_dic[user_name] = 0
            continue
        print('用户名或密码错误，请重新输入（还有%s次机会）' % (temp_account_dic.get(user_name),))


@func
def show_goods():
    while True:

        ret = input('请输入要查看的页码（输入N/n退出,输入B/b进行购买）：')
        if ret.upper() == 'N':
            if SHOPPING_CAR != {}:
                file_name = datetime.now().strftime('%Y-%m-%d-%H-%M') + '.txt'
                if not os.path.exists(USER_NAME):
                    os.makedirs(USER_NAME)
                dir = os.path.join(USER_NAME, file_name)
                write_file(json.dumps(SHOPPING_CAR, ensure_ascii=False),'w', dir)
            return
        if ret.upper() == 'B':
            shopping()
            continue
        if not ret.isdecimal():
            print('输入错误，请重新输入')
            continue
        val = query_goods(int(ret))
        if not val:
            print('页码超出范围，请重新输入')


def query_goods(page):
    pre_page_count = 2
    content = read_file('shahegoods.txt')
    if not content:
        print('无商品')
        return
    content_list = json.loads(content)
    total, v = divmod(len(content_list), pre_page_count)
    if v > 0:
        total += 1
    if page < 1 or page > total:
        return False
    start = (page - 1) * pre_page_count
    end = page * pre_page_count
    lst = content_list[start:end]
    print('id   名称   价格')
    INDEX_LIST.clear()
    NOW_SHOW.clear()
    for i in lst:
        INDEX_LIST.append(str(i.get('id')))
        NOW_SHOW[str(i.get('id'))] = {'title': i.get('title'), 'price': str(i.get('price'))}
        print(str(i.get('id')) + "   " + i.get('title') + "   " + str(i.get('price')))
    return True


def shopping():
    while True:
        id = input('请输入产品ID(输入N/n购买完成)：')
        if id.upper() == 'N':

            return
        if id not in INDEX_LIST:
            print('产品不存在，请重新输入')
            continue
        count = input('请输入要购买的数量：')
        if not count.isdecimal():
            print('购买数量有误,请重新输入')
            continue

        if not SHOPPING_CAR.get(id):
            SHOPPING_CAR[id] = {}
        SHOPPING_CAR[id]['title'] = NOW_SHOW[id]['title']
        SHOPPING_CAR[id]['price'] = NOW_SHOW[id]['price']
        SHOPPING_CAR[id]['count'] = SHOPPING_CAR[id].get('count', 0) + int(count)


@func
def show_shopping_car():
    print('***********我的购物车***********')
    for i in os.listdir(USER_NAME):
        print(i)
        path = os.path.join(USER_NAME, i)
        content = read_file(path)
        dic = json.loads(content)
        for item in dic.values():
            print('%s|%s|%s个'%(item.get('title'),item.get('price'),item.get('count'),))


def write_file(content,md= 'a', file_name='user_info.txt'):
    try:
        with open(file_name, mode=md, encoding='utf-8') as f:
            f.write('%s\n' % content)
            f.flush()
    except Exception as e:
        return False
    return True


def read_file(file_name='user_info.txt'):
    try:
        with open(file_name, mode='r', encoding='utf-8') as f:
            content = f.read()
        return content
    except Exception as e:
        return None


main()
'''

# 9.请使用第三方模块xlrd读取一个excel文件中的内容。【课外】
'''
import xlrd
# 打开Excel文件读取数据
data = xlrd.open_workbook('my.xls')
# 获取一个工作表
table = data.sheets()[0] #通过索引顺序获取
# 获取行数和列数
nrows = table.nrows
ncols = table.ncols
# 循环行列表数据
for i in range(nrows):
    print(table.row_values(i))
'''
