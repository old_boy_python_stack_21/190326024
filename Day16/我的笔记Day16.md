# Day16

## 今日内容

模块：

- time
- datetime
- pickle
- shutil
- logging

## 补充

1. 常用模块：json，time，datetime
2. 模块的概念：

   - 模块是一个py文件或者一个文件夹（包），方便以后其他py文件调用。

3. 对于包的定义：
   - py2：在文件夹下必须加上  _  _init _  _ .py文件。
   - py3：不必要加上_  _init _  _ .py文件。

4. 导入模块

   - 导入模块时会将模块中的所有值加载到内存。

   - 导入模块的几种方式

     ```python
     import 模块
     from 模块 import 函数，函数
     from 模块 import *  #导入模块内所有函数
     from 模块 import 函数 as 别名 #起别名
     ```

   - 导入包中的文件

     ```python
     from 包 import 文件
     from xxx.xx import x 
     ```

5. __ file __ :命令行执行脚本时，传入的该脚本路径参数。

6. json序列化时保留中文

   ```python
   import json
   lst = ['alex',123,'李杰'] 
   print(json.dumps(lst))#["alex", 123, "\u674e\u6770"]  中文默认序列化为unicode编码
   
   print(json.dumps(lst,ensure_ascii=False))#["alex", 123, "李杰"]
   ```

## 内容详细

### 1.json和pickle模块

#### 1.1 json模块

1. json的优缺点

   - 优点：所有语言通用
   - 缺点：只支持基本的数据类型（int,bool,str,list,tuple(会转换成list),dict）

2. json.dump() 将对象序列化成json串，并写入文件

   ```python
   import json
   
   lst = ['alex', 123, '李杰']
   f = open('abc.txt', mode='w', encoding='utf-8')
   json.dump(lst, f, ensure_ascii=False)
   f.close()
   ```

3. json.load() 读取文件，将内容反序列化成对象

   ```python
   import json
   
   f = open('abc.txt', mode='r', encoding='utf-8')
   data = json.load(f)
   f.close()
   print(data)
   ```

#### 1.2 pickle模块

1. pickle优缺点

   - 优点：可以序列化几乎所有类型（socket除外）
   - 缺点：只有python自己认识

2. pickle.dumps() 将对象序列化成字节类型

3. pickle.loads() 将字节类型反序列化成对象

   ```python
   import pickle
   
   lst = ['alex',123,'李杰']
   v1 = pickle.dumps(lst)
   print(v1) #b'\x80\x03]q\x00(X\x04\x00\x00\x00alexq\x01K{X\x06\x00\x00\x00\xe6\x9d\x8e\xe6\x9d\xb0q\x02e.'
   data = pickle.loads(v1)
   print(data)#['alex', 123, '李杰']
   
   #操作函数
   import pickle
   
   def func():
       print(123)
   
   v1 = pickle.dumps(func)
   print(v1) #b'\x80\x03c__main__\nfunc\nq\x00.'
   data = pickle.loads(v1)
   data()#123
   
   ```

4. pickle.dump()  将对象序列化成bytes并写入文件

   ```python
   import pickle
   
   def func():
       print(123)
   
   f = open('abc.txt',mode='wb')
   pickle.dump(func,f)
   f.close()
   ```

5. pickle.load() 读取文件内容，反序列化成对象

   ```python
   import pickle
   
   def func():
       print(123)
   
   f = open('abc.txt',mode='rb')
   data = pickle.load(f)
   data()
   f.close()
   ```

### 2.shutil模块

1. shutil.rmtree() 删除目录和其内部

   ```python
   import shutil
   
   shutil.rmtree('ass')
   ```

2. shutil.move(old,new)  重命名文件或文件夹

   ```python
   import shutil
   
   shutil.move('ass','aaa')
   shutil.move('aaa.txt','abc.txt')
   ```

3. shutil.make_archive('压缩包名称','压缩后的格式','要压缩的文件、文件夹')     压缩文件

   ```python
   import shutil
   
   shutil.make_archive('abc','zip','aaa')
   ```

4. shutil.unpack_archive('压缩包名称','解压到的路径（默认当前路径）'，‘压缩包类型’)   解压文件 ，路径不存在会创建

   ```python
   import shutil
   
   shutil.unpack_archive('abc.zip',extract_dir=r'E:\test',format='zip')
   ```

### 3.time和datetime模块

#### 3.1 关于时间

1. UTC/GMT  世界时间
2. 本地时间

#### 3.2 time模块

1. time.time()  获取时间戳   1970年1月1日 0时0分0秒 至今的时间 

2. time.sleep()   休眠

3. time.timezone   和格林威治时间所差的秒数

   ```python
   import time
   
   v = time.timezone
   print(v) #-28800
   ```

#### 3.3datetime模块

1. datetime.now()   获取本地当前时间   得到时间类型

   ```python
   from datetime import datetime
   
   v = datetime.now()
   print(v,type(v)) #2019-04-18 16:59:34.829613 <class 'datetime.datetime'>
   ```

2. datetime.utcnow() 获取UTC当前时间

   ```python
   from datetime import datetime
   
   v = datetime.utcnow()
   print(v,type(v))#2019-04-18 09:00:16.636312 <class 'datetime.datetime'>
   ```

3. 计算某时区的时间

   ```python
   from datetime import datetime,timezone,timedelta
   
   tz = timezone(timedelta(hours=7)) #东七区的时间   -7为西七区的时间
   v = datetime.now(tz)
   print(v)
   ```

4. 时间增加或减少

   ```python
   from datetime import datetime,timedelta
   
   v1 = datetime.now()
   print(v1) #2019-04-18 17:07:35.080733
   v2 = v1 + timedelta(days=10)
   print(v2) #2019-04-28 17:07:35.080733
   ```

5. datetime转成字符串（strftime(格式)）

   ```python
   from datetime import datetime
   
   v1 = datetime.now()
   print(v1,type(v1)) #2019-04-18 17:13:16.579031 <class 'datetime.datetime'>
   v2 = v1.strftime('%Y-%m-%d %H:%M:%S')
   print(v2,type(v2))#2019-04-18 17:13:16 <class 'str'>
   ```

6. 字符串转成datetime  (strptime(字符串，格式))

   ```python
   from datetime import datetime
   
   v1 = '2018-12-14'
   v2 = datetime.strptime(v1,'%Y-%m-%d')
   print(v2)#2018-12-14 00:00:00
   ```

7. 时间戳转datetime  (fromtimestamp(时间戳))

   ```python
   import time
   from datetime import datetime
   
   v1 = time.time()
   date = datetime.fromtimestamp(v1)
   print(date) #2019-04-18 17:20:42.438591
   ```

8. datetime转时间戳  (timestamp())

   ```python
   from datetime import datetime
   
   v1 = datetime.now()
   date = v1.timestamp()
   print(date)#1555579302.702781
   ```

### 4.异常处理

1. 格式

   ```python
   try:
       pass
   except Exception as e:
       pass
   
   ####示例#####
   def func():
       try:
           f = open('a.txt', mode='r',encoding='utf-8')
           print(f.read())
           return True
       except Exception as e:
           print('文件不存在')
           return False
   
   func() 
           
   ```

   