#!/usr/bin/env python
# -*- coding:utf-8 -*-

import json, struct, socket, sys, os
from tcpbase import TcpBase



class Tcpcli(TcpBase):

    def __init__(self, ip, port):
        self.dic = {'1': 'login', '2': 'upload', '3': 'download', '4': 'exit'}
        self.ip = ip
        self.port = port
        self.conn = None
        self.is_login = False

    def base(arg):
        def inner(self):
            if self.is_login:
                arg(self)
            else:
                print('请先登录')
        return inner

    def login(self):
        content = {'method': 'login'}
        while True:
            print('用户登录'.center(16, '*'))
            user_name = input('请输入用户名：')
            user_pwd = input('请输入密码：')
            self.base_send(content)
            user_dic = {'user_name': user_name, 'user_pwd': user_pwd}
            self.base_send(user_dic)
            msg = self.base_recv()
            if msg['result'] == '1':
                self.is_login = True
                print('登录成功！')
                break
            else:
                print('用户名或密码错误，请重新输入')

    @base
    def upload(self):
        print('文件上传'.center(20, '*'))
        dic = {'method': 'upload'}
        self.base_send(dic)
        while True:
            file_path = input('请输入要上传的文件路径：')
            if not os.path.isfile(file_path):
                print('输入的不是一个文件，请重新输入')
                continue
            file_name = os.path.basename(file_path)
            file_size = os.path.getsize(file_path)
            self.base_send({'file_name': file_name, 'file_size': file_size})
            f = self.read_file(file_path)
            print('文件上传中...')
            for i in f:
                self.conn.send(i)
            msg = self.base_recv()
            print(msg['result'])
            break

    @base
    def download(self):
        print('文件下载'.center(20, '*'))
        while True:
            file_path = input('请输入存储路径：')
            if not os.path.isdir(file_path):
                print('输入的不是一个路径，请重新输入')
                continue
            dic = {'method': 'download'}
            self.base_send(dic)
            msg = self.base_recv()
            file_name = msg['file_name']
            file_size = msg['file_size']
            full_path = os.path.join(file_path, file_name)
            f = open(full_path, mode='wb')
            read_size = 0
            print('文件下载中...')
            while read_size < file_size:
                info = self.conn.recv(1024 * 1024)
                read_size += len(info)
                f.write(info)
                f.flush()
            f.close()
            print('下载完成')
            break

    def exit(self):
        content = {'method': 'exit'}
        self.base_send(content)
        self.conn.close()
        sys.exit(0)

    def start(self):
        self.conn = socket.socket()
        self.conn.connect((self.ip, self.port))
        while True:
            print('欢迎使用文件上传下载系统'.center(20, '*'))
            print('1.用户登录\n2.上传文件\n3.下载文件\n4.退出系统')
            inp = input('请选择：')
            if inp not in self.dic:
                continue
            getattr(self, self.dic[inp])()


tcp_cli = Tcpcli('127.0.0.1', 8066)
tcp_cli.start()
