#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket,os
from tcpbase import TcpBase

user_list = [{'user_name':'wkx','user_pwd':'4297f44b13955235245b2497399d7a93'}] #密码123

class Tcpsvr(TcpBase):
    def __init__(self,ip,port):
        self.ip = ip
        self.port = port

        self.sk = None
        self.conn = None
        self.connected = False

    def login(self):
        dic = self.base_recv()
        pwd = self.encrypt(dic['user_pwd'])
        for i in user_list:
            if dic['user_name'] == i['user_name'] and pwd == i['user_pwd']:
                self.base_send({'result':'1'})
                break
        else:
            self.base_send({'result':'0'})

    def download(self):
        file_path = r'E:\Python\2019-05-08\day29\1.内容回顾.mp4'
        file_name = os.path.basename(file_path)
        file_size = os.path.getsize(file_path)
        self.base_send({'file_name':file_name,'file_size':file_size})
        f = self.read_file(file_path)
        for i in f:
            self.conn.send(i)

    def upload(self):
        dic = self.base_recv()
        file_size = dic['file_size']
        file_name = dic['file_name']
        file_path = os.path.join(r'E:\svrdownload',file_name)
        f = open(file_path,mode='wb')
        read_size = 0
        while read_size < file_size:
            msg = self.conn.recv(1024 * 1024)
            read_size += len(msg)
            f.write(msg)
            f.flush()
        f.close()
        self.base_send({'result':'上传完成'})

    def exit(self):
        self.conn.close()
        self.conn = None


    def start(self):
        self.sk = socket.socket()
        self.sk.bind((self.ip,self.port))
        self.sk.listen()
        while True:
            self.conn, _ = self.sk.accept()
            while self.conn:
                dic = self.base_recv()
                getattr(self,dic['method'])()


tcp_svr = Tcpsvr('127.0.0.1',8066)
tcp_svr.start()