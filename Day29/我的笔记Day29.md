# Day29

## 今日内容

1. TCP的粘包问题
2. 如何解决粘包问题

## 内容详细

### 1.tcp粘包问题

tcp传输特点

- 无边界
- 流式传输

#### 1.1 什么是粘包

同时执行多条命令之后，得到的结果很可能只有一部分，在执行其他命令的时候又接收到之前执行的另外一部分结果，这种现象就是黏包。

#### 1.2 tcp的分包机制

当发送端缓冲区的长度大于网卡的MTU时，tcp会将这次发送的数据拆成几个数据包发送出去。 
MTU是Maximum Transmission Unit的缩写。意思是网络上传送的最大数据包。MTU的单位是字节。 大部分网络设备的MTU都是1500。如果本机的MTU比网关的MTU大，大的数据包就会被拆开来传送，这样会产生很多数据包碎片，增加丢包率，降低网络速度。

#### 1.3 tcp粘包原因

- 发送方的缓存机制
  - 发送端需要等缓冲区满才发送出去，造成粘包（发送数据时间间隔很短，数据了很小，会合到一起，产生粘包）
- 接收方的缓存机制
  - 接收方不及时接收缓冲区的包，造成多个包接收（客户端发送了一段数据，服务端只收了一小部分，服务端下次再收的时候还是从缓冲区拿上次遗留的数据，产生粘包） 

#### 1.4 总结

黏包现象只发生在tcp协议中：

1.从表面上看，黏包问题主要是因为发送方和接收方的缓存机制、tcp协议面向流通信的特点。

2.实际上，**主要还是因为接收方不知道消息之间的界限，不知道一次性提取多少字节的数据所造成的**

### 2.解决粘包问题

#### 2.1 struct模块

```python
import struct

ret = struct.pack('i',10000) #i代表int，得到4个字节
print(ret)#b"\x10'\x00\x00"

res = struct.unpack('i',ret)
print(res)#(10000,)
```

#### 2.2 解决粘包问题

借助struct模块，我们知道长度数字可以被转换成一个标准大小的4字节数字。因此可以利用这个特点来预先发送数据长度。

我们还可以把报头做成字典，字典里包含将要发送的真实数据的详细信息，然后json序列化，然后用struck将序列化后的数据长度打包成4个字节（4个字节足够用了）

```python
#服务端
import struct,json,socket

sk = socket.socket(type=socket.SOCK_STREAM)
sk.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)#防止连接关闭后，端口没有马上被释放。可以复用端口
sk.bind(('127.0.0.1',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        head_length = conn.recv(4) #接收包头长度
        head_length = struct.unpack('i',head_length)[0]
        body_length = json.loads(conn.recv(head_length).decode('utf-8'))['body_length']

        recv_length = 0
        recv_info = b''
        while recv_length < body_length:
            recv_info += conn.recv(body_length-recv_length)# 循环接收包未接收完的数据
            recv_length = len(recv_info)

        print(recv_info.decode('utf-8'))
    
    conn.close()
sk.close()
```

```python
#客户端
import json,struct,socket

sk = socket.socket(type=socket.SOCK_STREAM)
sk.connect(('127.0.0.1',8066))
while True:
    inp = input('>>>')
    if not inp:
        continue
    body_info = inp.encode('utf-8')
    head = {'body_length':len(body_info)}
    head_info = json.dumps(head).encode('utf-8')
    head_length = struct.pack('i',len(head_info))
    sk.send(head_length)
    sk.send(head_info)
    sk.send(body_info)
sk.close()
```

