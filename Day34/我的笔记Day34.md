# Day34

## 今日内容

1. 生产者消费者模型
2. 进程之间的数据共享
3. 初识线程

## 补充

### 1.IPC机制补充

除了内置的Queue和Pipe外，还有第三方的工具（软件）提供给了我们IPC机制。

- redis
- memcache
- kafka
- rabbitmq

使用第三方工具的好处：

- 集群（高可用）
- 并发需求
- 断电保存数据
- 解耦

### 2.生成器

- 取过一次就没有了

- 惰性运算：不取不执行(list,`__next__`,for循环)

  ```python
  def demo():
       for i in range(4):
          yield i
  g=demo()
  g1=(i for i in g)
  g2=(i for i in g1)
  print(list(g1))#[0,1,2,3]
  print(list(g2))#[]
  ```

  ```python
  def add(n,i):
      return n+i
  def test():
      for i in range(4):
          yield i
  g=test()
  for n in [1,10]:
      g=(add(n,i) for i in g)
  print(list(g))#[20,21,22,23]
  ```

## 内容详细

### 1.生产者消费者模型

#### 1.1 程序解耦

把写在一起的大的功能分开成多个小的功能处理。

优点：修改、复用方便，代码的可读性高。

#### 1.2 生产者消费者模型

生产者消费者模型利用了程序解耦的思想，将获取(生产)数据和处理数据分开，提高程序效率。

生产者（生产数据）--> 中间容器 -->消费者（处理数据）

生产者消费者模型小示例：

```python
import time
import random
from multiprocessing import Process, Queue


def producer(q, name):
    for i in range(10):
        time.sleep(random.random())
        goods = '产品%s' % (i,)
        s = '生产者%s生产了%s' % (name, goods,)
        q.put(goods)
        print(s)


def consumer(q, name):
    while True:
        time.sleep(random.random())
        goods = q.get()
        if not goods: break
        print('消费者%s购买了%s' % (name, goods,))


def cp(c_count, p_count):
    p_list = []
    q = Queue(10)
    for i in range(p_count):
        p = Process(target=producer, args=(q, i))
        p.start()
        p_list.append(p)
    for i in range(c_count):
        Process(target=consumer, args=(q, i)).start()
    for i in p_list:
        i.join()
    for i in range(c_count):
        q.put(None)


if __name__ == '__main__':
    cp(2, 3)
```



爬虫实例：

```python
import re
import requests
from multiprocessing import Process,Queue

def producer(q,url):
    response = requests.get(url)
    q.put(response.text)


def consumer(q):
    while True:
        s = q.get()
        if not s: break
        com = re.compile(
            '<div class="item">.*?<div class="pic">.*?<em .*?>(?P<id>\d+).*?<span class="title">(?P<title>.*?)</span>'
            '.*?<span class="rating_num" .*?>(?P<rating_num>.*?)</span>.*?<span>(?P<comment_num>.*?)评价</span>', re.S)
        ret = com.finditer(s)
        for i in ret:
            print({
                "id": i.group("id"),
                "title": i.group("title"),
                "rating_num": i.group("rating_num"),
                "comment_num": i.group("comment_num")}
            )


if __name__ == '__main__':
    count = 0
    q = Queue(10)
    p_list = []
    for i in range(10):
        url = 'https://movie.douban.com/top250?start=%s&filter=' % count
        count += 25
        p = Process(target=producer, args=(q, url,))
        p.start()
        p_list.append(p)
    Process(target=consumer, args=(q,)).start()
    for i in p_list:
        i.join()
    q.put(None)
```

进程

- 一个进程就是一个生产者
- 一个进程就是一个消费者

队列：中间容器

#### 1.3 JoinableQueue

原理：

![](E:\Python\2019-05-15\day34\JoinableQueue原理.png)

```python
import time
import random
from multiprocessing import Process, Queue,JoinableQueue


def producer(q, name):
    for i in range(10):
        time.sleep(random.random())
        goods = '产品%s' % (i,)
        s = '生产者%s生产了%s' % (name, goods,)
        q.put(goods)
        print(s)
    q.join()

def consumer(q, name):
    while True:
        time.sleep(random.random())
        goods = q.get()
        print('消费者%s购买了%s' % (name, goods,))
        q.task_done()


def cp(c_count, p_count):
    p_list = []
    q = JoinableQueue(10)
    for i in range(p_count):
        p = Process(target=producer, args=(q, i))
        p_list.append(p)
        p.start()
    for i in range(c_count):
        c = Process(target=consumer, args=(q, i))
        c.daemon = True
        c.start()
    for i in p_list:
        i.join()

if __name__ == '__main__':
    cp(2, 3)
```

### 2.进程之间共享数据

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)
```

### 3.初识线程

1. 一个进程中的多个线程可以利用多核，但Cpython解释器不能实现多线程利用多核。
2. 锁：全局解释器锁GIL(global interpreter lock)
   - 保证了整个python程序中，同一时刻只能有一个线程被CPU执行
   - 原因：Cpython解释器中特殊的垃圾回收机制

3. pypy解释器有锁，Jpython无锁。
4. GIL锁导致了线程不能并行，可以并发。
   - 使用多线程并不影响高IO型的操作
   - 对高计算型的程序有效率上的影响（遇到高计算可以使用多进程+多线程，或者分布式）

### 4.Thread类

#### 4.1 启动线程

multiprocessing是完全仿照threading的类写的。所以它们的操作类似

```python
import os
from threading import Thread

def func():
    print(os.getpid())#

t = Thread(target=func)
t.start()
print(os.getpid())#两个pid相同，它们属于同一进程
```

```python
#创建多个线程
from threading import Thread

def func(i):
    print('thread',i)


for i in range(10):
    t = Thread(target=func,args=(i,))
    t.start()
```

#### 4.2 join方法

阻塞，直到子线程执行结束

主线程等待所有子线程结束后才结束，主线程结束后，主进程也就结束了。

#### 4.3 面向对象方式启动线程

```python
from threading import Thread

class MyThread(Thread):
    def __init__(self,count):
        self.count = count
        super().__init__()

    def run(self):
        print('thread',self.count)
        print(self.ident) #ident线程id


for i in range(10):
    t = MyThread(i)
    t.start()
```

#### 4.4 线程中的其他方法

```python
from threading import Thread,enumerate,current_thread,active_count

def func():
    print(current_thread().ident) #current_thread()获取当前线程对象

t = Thread(target=func)
t.start()

print(enumerate()) #获取当前活着的线程对象，得到列表
print(active_count())#获取当前活着的线程数，等价于len(enumerate())
```

注：在线程中，不能在主线程结束一个子线程（没有terminate方法）

#### 4.5 守护线程

守护线程一直等到所有非守护线程都结束后才结束，除了守护主线程的代码之外，也会守护子线程。

```python
import time
from threading import Thread

def func1():
    while True:
        time.sleep(0.5)
        print('thread1')

def func2():
    for i in range(5):
        time.sleep(1)
        print('thread2')


t1 = Thread(target=func1)
t2 = Thread(target=func2)
t1.daemon = True
t1.start()
t2.start()
time.sleep(3)
#结果
'''
thread1
thread2
thread1
thread1
thread2
thread1
thread1
thread2
thread1
thread1
thread2
thread1
thread1
thread2
'''
```



