#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第34天作业
王凯旋
'''

#1.你了解生产者模型消费者模型么？如何实现？
'''
生产者：只负责生产数据。消费者：只负责处理数据。
可以通过multiprocessing包中的JoinableQueue类队列来实现，生产者向队列中放入数据，消费者取出数据
'''


#2.GIL锁是怎么回事?
'''
GIL锁
因为Cpython解释器的垃圾回收机制，所以产生了GIL锁
GIL锁保证了在整个程序中，同一时刻只能有一个线程被CPU执行
'''


#3.请简述进程和线程的区别？
'''
线程是进程的一部分，一个进程中至少包括一个线程
进程可以利用多核，线程不可以利用多核
'''


#4.多进程之间是否能实现数据共享？用哪个模块实现？
'''
可以实现数据共享
利用multiprocessing包中的Manager类实现
'''


#生成器
'''
0 0 0
'''
'''
[6,6,6,6]
'''
'''
def multipliers():
    return (lambda x:i*x for i in range(4))
print([m(2) for m in multipliers()])
'''