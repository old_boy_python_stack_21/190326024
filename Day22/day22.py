#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第二十二天作业
王凯旋
'''

# 1.请使用面向对象实现栈（后进先出）
'''
class Stack(object):

    def __init__(self):
        self.stack_list = []

    def push(self,val):
        self.stack_list.append(val)

    def pop(self):
        if self.queue_list:
            return self.stack_list.pop()

'''

# 2.请是用面向对象实现队列（先进先出）
'''
class Queue(object):
    def __init__(self):
        self.queue_list = []

    def push(self,val):
        self.queue_list.append(val)

    def pop(self):
        if self.queue_list:
            return self.queue_list.pop(0)
'''

# 3.如何实现一个可迭代对象？
'''
class Foo(object):
    def __iter__(self):
        return iter([1,2,3,4])

'''

# 4.看代码写结果
'''
class Foo(object):

    def __init__(self):
        self.name = '武沛齐'
        self.age = 100


obj = Foo()
setattr(Foo, 'email', 'wupeiqi@xx.com')

v1 = getattr(obj, 'email')
v2 = getattr(Foo, 'email')

print(v1, v2)
'''

'''
结果：wupeiqi@xx.com  wupeiqi@xx.com
'''

# 5.请补充代码（提：循环的列表过程中如果删除列表元素，会影响后续的循环，推荐：可以尝试从后向前找）
'''
li = ['李杰','女神','金鑫','武沛齐']

name = input('请输入要删除的姓氏：') # 如输入“李”，则删除所有姓李的人。
length = len(li)
for i in range(0,length):
    if name in li[length-i-1]:
        li.pop(length-i-1)
'''

# 6.有如下字典，请删除指定数据。
'''
class User(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age


info = [User('武沛齐', 19), User('李杰', 73), User('景女神', 16)]

name = input('请输入要删除的用户姓名：')
# 请补充代码将指定用户对象再info列表中删除。
length = len(info)
for i in range(0,length):
    if name in info[length-i-1].name:
        info.pop(length-i-1)

'''

# 7.补充代码实现：校园管理系统。
'''
class User(object):
    def __init__(self, name, email, age):
        self.name = name
        self.email = email
        self.age = age

    def __str__(self):
        return self.name


class School(object):
    """学校"""

    def __init__(self):
        # 员工字典，格式为：{"销售部": [用户对象,用户对象,] }
        self.user_dict = {}

    def invite(self, department, user_object):
        """
        招聘，到用户信息之后，将用户按照指定结构添加到 user_dict结构中。
        :param department: 部门名称，字符串类型。
        :param user_object: 用户对象，包含用户信息。
        :return:
        """
        if department in self.user_dict.keys():
            self.user_dict[department].append(user_object)
            print('招聘成功')
            return
        self.user_dict[department] = [user_object]
        print('招聘成功')

    def dimission(self, username, department=None):
        """
        离职，讲用户在员工字典中移除。
        :param username: 员工姓名
        :param department: 部门名称，如果未指定部门名称，则遍历找到所有员工信息，并将在员工字典中移除。
        :return:
        """
        if department in self.user_dict.keys():
            for i in self.user_dict.get(department):
                if i.name == username:
                    self.user_dict[department].remove(i)
                    print("离职成功")
                    return
        for k, i in self.user_dict.items():
            for j in i:
                if j.name == username:
                    self.user_dict[k].remove(j)
                    print("离职成功")
                    return

    def run(self):
        """
        主程序
        :return:
        """
        dic = {'1':self.invite,'2':self.dimission}
        while True:
            print('校园管理系统'.center(16,'*'))
            print('1.招聘\n2.离职')
            index = input('请选择（N/n退出）：')
            if index.upper() == 'N':
                return
            if not index in dic.keys():
                print("输入错误，请重新输入")
                continue
            if index == '1':
                while True:
                    print('招聘'.center(16,'*'))
                    name = input('请输入姓名(N/n退出)：')
                    if name.upper() == 'N':
                        break
                    email = input('请输入邮箱：')
                    age = input('请输入年龄：')
                    dep = input('请输入部门：')
                    user = User(name,email,age)
                    dic.get(index)(dep,user)
            if index == '2':
                while True:
                    print('离职'.center(16, '*'))
                    name = input('请输入离职人员姓名(N/n退出)：')
                    if name.upper() == 'N':
                        break
                    dmp = input('请输入离职人员部门(输入*跳过)：')
                    dic.get(index)(name,dmp)

if __name__ == '__main__':
    obj = School()
    obj.run()

'''

# 8.请编写网站实现如下功能。
'''
需求：

    实现 MIDDLEWARE_CLASSES 中的所有类，并约束每个类中必须有process方法。

    用户访问时，使用importlib和反射 让 MIDDLEWARE_CLASSES 中的每个类对 login、logout、index 方法的返回值进行包装，最终让用户看到包装后的结果。
    如：

        用户访问 : http://127.0.0.1:8000/login/ ，
        页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】

        用户访问 : http://127.0.0.1:8000/index/ ，
        页面显示： 【csrf】【auth】【session】 登录 【session】 【auth】 【csrf】

        即：每个类都是对view中返回返回值的内容进行包装。

'''

'''
MIDDLEWARE_CLASSES = [
    'utils.session.SessionMiddleware',
    'utils.auth.AuthMiddleware',
    'utils.csrf.CrsfMiddleware',
]

from wsgiref.simple_server import make_server
import importlib

class View(object):
    def login(self):
        return '登陆'

    def logout(self):
        return '等处'

    def index(self):
        return '首页'


def func(environ,start_response):
    start_response("200 OK", [('Content-Type', 'text/plain; charset=utf-8')])
    obj = View()
    method_name = environ.get('PATH_INFO').strip('/')
    if not hasattr(obj,method_name):
        return ["".encode("utf-8"),]

    response = getattr(obj,method_name)()
    for item in MIDDLEWARE_CLASSES:
        package_path,class_path = item.rsplit('.',maxsplit=1)
        v = importlib.import_module(package_path)
        if hasattr(v,class_path):
            obj1 = getattr(v,class_path)()
            response = obj1.process(response)

    return [response.encode("utf-8")  ]


server = make_server('127.0.0.1', 8000, func)
server.serve_forever()
'''
