# Day22

## 今日内容

1. 可迭代对象
2. 约束
3. 反射
4. 模块补充

## 内容详细

### 1.可迭代对象

表象：可以被for循环的对象被称为可迭代对象

如何将一个对象变为可迭代对象？

在其类内部实现`__iter__()`方法，且返回一个迭代器或生成器

```python
#方式一
class Foo(object):
    def __iter__(self):
        return iter([1,2,3,4])
    
#方式二
class Foo(object):
    def __iter__(self):
        yield 1
        yield 2
        yield 4
```

### 2.约束

是一种规范，约束子类必须实现某方法

```python
class Base(object):

    def send(self):
        raise NotImplementedError()  #基类约束所有的子类必须实现send方法


class Bar(Base):

    def send(self):  #子类实现send方法
        print('Bar的send方法') 


class Foo(Base):

    def send(self):   #子类实现send方法
        print('Foo的send方法')
```

### 3.反射

以字符串的形式去某个对象中操作他的成员

#### 3.1 getattr(对象,'成员')

以字符串的形式去对象中获取对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')
v = getattr(obj,'name') #等同于obj.name
print(v)
```

#### 3.2 setattr(对象,'成员',值)

以字符串的形式设置对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')
setattr(obj,'name',123) #等同于obj.name = 123
print(getattr(obj,'name'))
```

#### 3.3 hasattr(对象,'成员')

以字符串的形式判断对象中是否包含某成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')

if hasattr(obj,'name'):
    print(getattr(obj, 'name'))
```

#### 3.4 delattr(对象,'成员')

以字符串形式删除对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')

delattr(obj,'name')
```

#### 3.5 python中一切皆对象

因此以后想要通过字符串形式操作内部成员，均可以通过反射来实现

- py文件（模块）
- 包
- 类
- 对象

```python
import utils.tests
print(getattr(utils.tests,'X'))
```

### 4.模块补充

#### 4.1 importlib模块

根据字符串的形式导入一个字符串

```python
import importlib
redis = importlib.import_module('utils.tests')
print(redis.X)


path = 'utils.tests.func'

v1,v2 = path.rsplit('.',maxsplit=1)
test = importlib.import_module(v1)
getattr(test,v2)()
```

