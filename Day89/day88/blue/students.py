#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Blueprint,render_template,request

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

student = Blueprint('student',__name__,url_prefix='/student')

@student.route('/list')
def student_list():
    return render_template('student_list.html', student_list=STUDENT_DICT)

@student.route('/detail')
def detail():
    id = request.args.get('id')
    return render_template('student.html', student=STUDENT_DICT[int(id)], id=id)