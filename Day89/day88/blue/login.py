#!/usr/bin/env python
# -*- coding:utf-8 -*-

from flask import Blueprint,request,session,redirect,render_template

login = Blueprint('login',__name__)

@login.route('/login',methods=['GET','POST'])
def login1():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username == 'alex' and password == '123':
            session['user'] = username
            return redirect('/student/list')
    return render_template('login.html')