#!/usr/bin/env python
# -*- coding:utf-8 -*-

class Debug(object):
    DEBUG = True
    SESSION_COOKIE_NAME = 'I am debug session'

class Testing(object):
    TESTING = True
    SESSION_COOKIE_NAME = 'I am Not session'
