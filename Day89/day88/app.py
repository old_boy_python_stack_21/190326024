#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask,request,session,redirect,render_template
from blue.students import student
from blue.login import login
from settings import Debug,Testing

app = Flask(__name__)
app.debug = True
app.secret_key = '123456'
# app.config.from_object(Debug)
app.config.from_object(Testing)

app.register_blueprint(student)
app.register_blueprint(login)

@app.before_request
def Auth():
    if request.path == '/login':
        return
    if not session.get('user'):
        return redirect('/login')

@app.errorhandler(404)
def error(errorMessage):
    return render_template('error.html')


app.run()
