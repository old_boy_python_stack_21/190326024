# Day89

## 今日内容

1. flask的路由

2. flask初始化配置
3. flask实例配置（app的配置）
4. flask中的蓝图Blueprint(app01)
5. 特殊装饰器

## 内容详细

### 1.flask的路由

@app.route()

参数：

- rule  "/login"路由地址

- methods 允许进入视图函数的请求方式

- endpoint Mapping路由地址和endpoint,endpoint在同一个app中不能出现重复，默认值为函数名

- defaults   默认路由参数 

  ```python
  @app.route('/',defaults={'page':'20'})
  def home(page):
      count = request.args.get('page',page)
      return count
  ```

- strict_slashes  是否严格遵守路由匹配，默认为True
- reditect_to  永久重定向308,301

动态路由的设置：

可以分页，获取文件，解决分类，解决正则路由问题

```python
@app.route('/func/<int:type>_<ind>') #默认是string类型

def func(type,ind):
    print(type,ind)
    return "123"

```

路由反向解析：

```python
from flask import Flask,request,redirect,url_for

@app.route('/',defaults={'page':'20'})
def home(page):
    count = request.args.get('page',page)
    return redirect(url_for(func,type=10,ind="12"))#需要把需要的参数赋值

@app.route('/func/<int:type>_<ind>',endpoint='func')
def func(type,ind):
    print(type,ind)
    return "123"
```

### 2.flask初始化配置

```python
app = Flask(__name__,template_folder="templates",static_folder="static",static_url_path='static')
```

- template_folder   模板文件的存放路径
- static_folder   静态文件存放目录，默认值是static
- static_url_path   静态文件访问的路径 默认值是 / + static_folder

### 3.flask实例化配置 (app的配置)

app的常用配置项：

```python
'DEBUG':False  #是否开启Debug模式
'TESTING':False #是否开启测试模式
'SECRET_KEY': None  #加密session的串
'PREMANENT_SESSION_LIFETIME':31 #session的过期时间，单位秒，默认31天
'SESSION_COOKIE_NAME':'session' #在cookies中存放的session加密串的key
'SESSION_COOKIE_DOMAIN': None,  # 在哪个域名下会产生session记录在cookies中
'SESSION_COOKIE_PATH': None,  #在哪个路径下产生的session记录在cookies中
'JSONIFY_MIMETYPE': 'application/json', #使用jsonify时，返回给浏览器的Content-Type
```

配置方式：

- 直接对设置

  ```python
  app.DEBUG = True
  app.config['TESTING'] = True
  ```

- 使用类的方式导入

  新建settings.py文件

  ```python
  class FlaskSetting(object):
      DEBUG = True
      SECRET_KEY = '1234'
  ```

  然后在flask的启动文件中这样写：

  ```python
  from flask import Flask
  
  app = Flask(__name__)
  app.config.from_object("settings.FlaskSettings")
  ```

### 4.Flask中的蓝图

蓝图用于项目中进行功能划分，是不能被run的Flask实例，不存在Config

蓝图文件user.py

```python
from flask import Blueprint

user = Blueprint('user',__name__)

@user.route('/add')
def add():
    return 'add user'

@user.route('/delete')
def delete():
    return 'delete user'
```

蓝图文件car.py

```python
from flask import Blueprint

car = Blueprint('car',__name__,url_prefix='/car')#url_prefix，会在路由前面加上/car


@car.route('/add')
def add():
    return 'add car'

@car.route('/delete')
def delete():
    return 'delete car'
```

flask启动文件

```python
from flask import Flask
from user import user
from car import car

app = Flask(__name__)

app.register_blueprint(car)#注册蓝图
app.register_blueprint(user)

app.run()
```

**注：蓝图中的路由名称不能相同，否则会按照注册顺序调用，后面的不能调用**

### 5.特殊装饰器

#### 5.1 before_request

在请求进入视图函数之前，做的处理。作用于所有的路由。按照在代码中编写的顺序执行。如果中间有一个返回了一个response，则后面的before_request不执行，直接从最后定义的after_request开始向前执行。

```python
@app.before_request
def bf1():
    print('bf1')
    
@app.before_request
def bf2():
    print('bf2')
```

#### 5.2 after_request

在响应返回客户端之前，结束视图函数会后。会按照编写的顺序倒序执行

```python
@app.after_request
def af1():
    print('af1')
    
@app.after_request
def af2():
    print('af2')
```

请求生命周期：bf1-bf2-视图函数-af2-af1

#### 5.3 errorhandler(404)

当发生请求错误或者服务器错误(40x或50x)是会执行。

```python
@app.errorhandler(404)
def error404(errorMessage):
    return '页面不存在' #5种响应
```



