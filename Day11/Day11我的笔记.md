# Day11

## 今日内容

1. 函数小高级
2. lambda表达式
3. 内置函数

## 内容详细

### 1.函数小高级

#### 1.1 函数的赋值

- 函数名当做变量使用

- 函数名可以放入集合中，也可当做字典的键

  ```python
  def func():
      return 123
  v = func
  print(v()) #123
  ```

  ```python
  def func():
      print(123)
  func_list = [func,func,func]
  func_list[0]() #123
  func_list[1]() #123
  func_list[2]() #123
  ```

#### 1.2 函数当参数传递

```python
def func(arg):
	arg()
def func1():
    print(123)
func(func1) #123
```

#### 1.3 面试题

- 有十个函数，根据用户的不同输入，执行不同的函数。
  - 将函数名和对应输入构造成字典

### 2.lambda表达式

为解决简单函数的情况，如：

```python
def func(a1,a2):
	return a1+a2
#等同于
v = lambda a1,a2:a1+a2
v(1,2)#3
```

```python
lst = []
func = lambda a1:lst.append(a1)
print(func(666)) #None
print(lst)#[666]
```

- lambda表达式的几种形式

  ```python
  func1 = lambda : 100
  
  func2 = lambda a1,a2:a1+a2
  
  func3 = lambda *args,**kwargs: print(args)
  
  DATA = 100
  func4 = lambda a1: a1+DATA  #只有一行代码，不能自己创建变量，只能去父级寻找
  ```

### 3.内置函数

#### 3.1 强制转换函数

- int()
- bool()
- str()
- list()
- tuple()
- dict()
- set()

#### 3.2 输入/输出

- print()
- input()

#### 3.3 数学相关

- max()   取最大值

  ```python
  print(max([11,22,66,754,78]))#754
  print(max(2,3))#3
  ```

- min()   取最小值

- abs()    取绝对值

- sum()   计算总和

  ```python
  print(sum([1,2,3,4]))#10
  ```

- float()  转换为浮点类型

- divmod()  取两数相除的商和余数(可用于分页)

  ```python
  print(divmod(10,3)) #(3,1)
  print(divmod(3.5,2)) #(1.0,1.5)
  ```

#### 3.4 进制转换

- bin()   将十进制转换成二进制

  ```python
  print(bin(15))  #0b1111
  ```

- oct()   将十进制转换成八进制

  ```python
  print(oct(15))  #0o17
  ```

- hex()  将十进制转换成十六进制

  ```python
  print(hex(15)) #0xf
  ```

- int()    将其他进制转换成十进制（可作为中转，完成任意进制间的转换）

  ```python
  v1 = '0b11111111'
  print(int(v1,base=2)) #255
  
  v2 = '77'
  print(int(v2,base=8)) #63
  
  v3 = 'ff'
  print(int(v3,base=16))#255
  ```

  - base默认值为10

#### 3.5 面试题

- 将ip:192.168.12.79,按’.‘分割转换成8位的二进制数，然后将其拼接起来，再转换成十进制数

  ```python
  IP = '192.168.12.79'
  LST = IP.split('.')
  NEW_LIST = []
  for item in LST:
      TEMP = bin(int(item))[2:]
      TEMP = "0" * (8 - len(TEMP)) + TEMP
      NEW_LIST.append(TEMP)
  print(int("".join(NEW_LIST), base=2))
  
  
  #列表生成器
  print(int(''.join(bin(int(item))[2:].zfill(8) for item in IP.split('.')),base=2))
  ```

  

## 总结

1. 函数当做一个变量：参数传值、当元素嵌套到字典中
2. lambda表达式
3. 内置函数
   - 类型强转
   - 输入输出
   - 数学相关
   - 进制转换