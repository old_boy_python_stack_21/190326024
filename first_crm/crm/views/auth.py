#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.shortcuts import render, redirect
from crm.forms import RegForm
from comm.encrypt import encrypt_pwd
from crm import models



def login(request):
    error = ''
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        obj = models.UserProfile.objects.filter(username=username, password=encrypt_pwd(password),
                                                is_active=True).first()
        if obj:
            response = redirect('index')
            response.set_cookie('is_login', '1')
            request.session['pk'] = obj.pk
            return response
        error = '用户名或密码不正确'
    return render(request, 'auth/login.html', {'error': error})


def logout(request):
    request.session.flush()
    response = redirect('login')
    response.delete_cookie('is_login')
    return response


def reg(request):
    reg_obj = RegForm()
    if request.method == 'POST':
        reg_obj = RegForm(request.POST)
        if reg_obj.is_valid():
            obj = reg_obj.save()
            dep = obj.department
            dep.count += 1
            dep.save()
            return redirect('login')

    return render(request, 'auth/reg.html', {'reg_obj': reg_obj})


def index(request):
    return render(request, 'auth/index.html')