#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.shortcuts import render, redirect, reverse
from django.http.response import JsonResponse
from crm.forms import CustomerForm, ConsultRecordForm, EnrollmentForm
from crm import models
from comm.pagination import Pagination
from django.db.models import Q
import sys
import json
from django.db import transaction
from django.conf import settings


def customer_list(request):
    path = request.get_full_path()
    query = request.GET.get('query', '')
    q = Q(Q(qq__contains=query) | Q(name__contains=query) | Q(phone__contains=query))
    if request.path_info == reverse('my_customer'):
        u_pk = request.session.get('pk')
        customer_list = models.Customer.objects.filter(q, consultant_id=u_pk)
        type = 1
    else:
        customer_list = models.Customer.objects.filter(q, consultant__isnull=True)
        type = 0
    page = Pagination(request.GET.get('page', 1), len(customer_list), request.GET.copy())
    response = render(request, 'consultant/customer_list.html',
                      {'customer_list': customer_list[page.start:page.end], 'page_list': page.page_html, 'type': type})

    # 修改或添加后要返回的页面
    response.set_cookie('return_url', path)
    return response


def oper_customer(request, pk=None):
    customer = models.Customer.objects.filter(pk=pk).first()
    form_obj = CustomerForm(instance=customer)
    if request.method == 'POST':
        return_url = request.COOKIES.get('return_url')
        form_obj = CustomerForm(data=request.POST, instance=customer)
        if form_obj.is_valid():
            form_obj.save()
            if return_url:
                return redirect(return_url)
            return redirect('my_customer')
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'consultant/oper_customer.html', {'form_obj': form_obj, 'title': title})


def details(request, pk, model, form):
    model_obj = getattr(models, model)
    form_model = getattr(sys.modules['crm.forms'], form)
    obj = model_obj.objects.filter(pk=pk).first()
    form_obj = form_model(instance=obj)
    return render(request, 'consultant/details.html', {'form_obj': form_obj})


def customer_change(request):
    '''

    :param request:
    :param type: 0 公户转私户   1 私户转公户
    :return:
    '''
    pk_list = json.loads(request.POST.get('pk_list'))
    type = request.POST.get('type')
    if type == '0':
        try:
            with transaction.atomic():
                customer_list = models.Customer.objects.filter(pk__in=pk_list,
                                                               consultant__isnull=True).select_for_update()
                if len(pk_list) == customer_list.count():
                    count = models.Customer.objects.filter(consultant=request.user_obj).count()
                    count += customer_list.count()
                    if count > settings.MAX_CUSTOMER_COUNT:
                        return JsonResponse({'url': reverse('public_customer'), 'info': '客户已达上限!'})
                    customer_list.update(consultant=request.user_obj)
                    return JsonResponse({'url': reverse('public_customer'), 'info': '转换成功!'})
                return JsonResponse({'url': reverse('public_customer'),'info':'来晚了，该客户已给抢走了!'})

        except Exception as e:
            print(e)

    else:
        models.Customer.objects.filter(pk__in=pk_list).update(consultant_id=None)
        return JsonResponse({'url': reverse('my_customer'),'info': '转换成功!'})


def consult_record_list(request, customer_pk=0):
    url = request.get_full_path()
    query = request.GET.get('query', '')
    q = Q(Q(customer__name__contains=query) | Q(customer__qq__contains=query))
    page = request.GET.get('page', 1)
    u_pk = request.session.get('pk')
    if customer_pk:
        obj_list = models.ConsultRecord.objects.filter(q, consultant_id=u_pk, customer__pk=customer_pk,
                                                       delete_status=False)
    else:
        obj_list = models.ConsultRecord.objects.filter(q, delete_status=False)

    obj = Pagination(page, obj_list.count(), request.GET.copy())
    response = render(request, 'consultant/consult_record_list.html',
                      {'obj_list': obj_list.order_by('-date')[obj.start:obj.end], 'page_list': obj.page_html,
                       'customer_pk': customer_pk})
    response.set_cookie('return_url', url)
    return response


def oper_consult_record(request, pk=None, customer_pk=None):
    consult_obj = models.ConsultRecord.objects.filter(pk=pk).first()
    form_obj = ConsultRecordForm(request, customer_pk, instance=consult_obj)
    if request.method == 'POST':
        return_url = request.COOKIES.get('return_url')
        form_obj = ConsultRecordForm(request, customer_pk, data=request.POST, instance=consult_obj)
        if form_obj.is_valid():
            form_obj.save()
            if return_url:
                return redirect(return_url)
            return redirect('consult_record')
    title = '编辑信息' if pk else '添加信息'
    return render(request, 'consultant/oper_consult_record.html', {'form_obj': form_obj, 'title': title})


def enrollment_list(request):
    url = request.get_full_path()
    # query = request.GET.get('query', '')
    # q = Q(Q(customer__name__contains=query) | Q(customer__qq__contains=query))
    page = request.GET.get('page', 1)
    obj_list = models.Enrollment.objects.filter(customer__consultant=request.user_obj, delete_status=False)
    obj = Pagination(page, obj_list.count(), request.GET.copy())
    response = render(request, 'consultant/enrollment_list.html',
                      {'obj_list': obj_list.order_by('-enrolled_date')[obj.start:obj.end], 'page_list': obj.page_html})
    response.set_cookie('return_url', url)
    return response


def oper_enrollment(request, pk=None, customer_pk=None):
    enrollment = models.Enrollment(customer_id=customer_pk) if customer_pk else models.Enrollment.objects.filter(
        pk=pk).first()
    form_obj = EnrollmentForm(instance=enrollment)
    if request.method == 'POST':
        form_obj = EnrollmentForm(data=request.POST, instance=enrollment)
        return_url = request.COOKIES.get('return_url')
        if form_obj.is_valid():
            form_obj.save()
            if return_url:
                return redirect(return_url)
            return redirect('consult_record')
    title = '编辑信息' if pk else '添加信息'
    return render(request, 'consultant/oper_enrollment.html', {'form_obj': form_obj, 'title': title})
