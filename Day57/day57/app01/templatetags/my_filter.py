#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django import template
from django.utils.safestring import SafeData, mark_for_escaping, mark_safe

register = template.Library()


@register.filter
def sub(value, arg):
    try:
        return int(value) - int(arg)
    except (ValueError, TypeError):
        try:
            return value - arg
        except Exception:
            return ''


@register.filter
def mul(value, arg):
    try:
        return int(value) * int(arg)
    except (ValueError, TypeError):
        try:
            return value * arg
        except Exception:
            return ''

@register.filter
def div(value, arg):
    try:
        return int(value) / int(arg)
    except (ValueError, TypeError):
        try:
            return value / arg
        except Exception:
            return ''

@register.filter(name='get_a')
def get_atag(value,arg):
    return mark_safe('<a href={} target="_blank">{}</a>'.format(value,arg,))