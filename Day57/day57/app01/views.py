from django.shortcuts import render
from datetime import datetime

# Create your views here.

def template(request):
    return render(request,'template.html',
                  {
                      'num1':6,
                      'num2':2,
                      'url':'https://www.baidu.com',
                      'name':'百度',
                  })