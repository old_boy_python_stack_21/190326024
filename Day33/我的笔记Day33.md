# Day33

## 今日内容

1. 守护进程的概念
2. 面向对象方式开启进程
3. 锁的概念
4. 进程之间的通信

## 补充

进程对象和进程之间的关系？`p = Process(target=函数名,args=(参数,))`

- 进程对象和进程之间没有直接的关系，进程对象只是存储了一些和进程相关的内容，此时此刻操作系统还没有接到开启操作进程的指令
- ·`p.start()`向操作系统发送一个开启子进程的指令，发送完之后父进程继续执行自己的任务（异步非阻塞）

join() 同步阻塞，主进程阻塞，等待子进程执行结束。

## 内容详细

### 1.守护进程

守护进程在父进程的**代码结束**时也跟着结束。

通过设置daemon参数为True设置一个线程为守护线程

```python
import time
from multiprocessing import Process

def func():
    while True:
        print('is_alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=func)
    p.daemon = True
    p.start()
    time.sleep(2)
    #结果：
    #is_alive
    #is_alive
    #is_alive
    #is_alive
```

```python
import time
from multiprocessing import Process

def func():
    while True:
        print('is_alive')
        time.sleep(0.5)

def func1():
    for i in range(5):
        print('123')
        time.sleep(1)

if __name__ == '__main__':
    p = Process(target=func)
    p1 = Process(target=func1)
    p.daemon = True
    p.start()
    p1.start()
    time.sleep(2)
    #结果：
    #is_alive
    #123
    #is_alive
    #is_alive
    #123
    #is_alive
    #123
    #123
    #123
```

应用场景：

- 生产者消费者模型
- 和守护线程做对比

Process类的其他方法：

- is_alive() 判断进程是否活着
- terminate() 强制结束进程（异步非阻塞）

### 2.面向对象方式开启进程

```python
import os
import time
from multiprocessing import Process

class Myprocess(Process):
    def __init__(self,name,age):
        super().__init__()
        self.name = name
        self.age = age


    def run(self):
        while True:
            print(os.getpid(),'姓名:%s,年龄:%s'%(self.name,self.age,))
            time.sleep(0.5)


if __name__ == '__main__':
    mp = Myprocess('alex',18)
    mp.daemon = True
    mp.start()
    time.sleep(5)
```

### 3.锁的概念

如果在一个并发的场景下，涉及到某部分所有进程共享的数据资源需要修改，那就需要加锁来维护数据安全。

```python
import json
import time
from multiprocessing import Process,Lock

def buy_ticket(i,lock):
    lock.acquire()#加锁
    time.sleep(0.02)
    with open('ticket.txt',mode='r',encoding='utf-8') as f:
        dic = json.load(f)
        count = dic['count']
    if count > 0:
        print('%s买到票了'%(i,))
        count -= 1
    else:
        print('%s没买到票'%(i,))
    time.sleep(0.02)
    with open('ticket.txt',mode='w',encoding='utf-8') as f:
        dic['count'] = count
        json.dump(dic,f)
    lock.release()#解锁


if __name__ == '__main__':
    lock = Lock()#在父进程中创建锁对象，用参数传到子进程中，以确保所有的子进程都使用一把锁
    for i in range(10):
        p = Process(target=buy_ticket,args=(i,lock))
        p.start()

#可以通过 with lock： 代替lock.acquire()和lock.release()。好处：with内部实现了异常处理，不会形成死锁。
import json
import time
from multiprocessing import Process,Lock

def buy_ticket(i,lock):
    with lock:
        time.sleep(0.02)
        with open('ticket.txt',mode='r',encoding='utf-8') as f:
            dic = json.load(f)
            count = dic['count']
        if count > 0:
            print('%s买到票了'%(i,))
            count -= 1
        else:
            print('%s没买到票'%(i,))
        time.sleep(0.02)
        with open('ticket.txt',mode='w',encoding='utf-8') as f:
            dic['count'] = count
            json.dump(dic,f)


if __name__ == '__main__':
    lock = Lock()#在父进程中创建锁对象，用参数传到子进程中，以确保所有的子进程都使用一把锁
    for i in range(10):
        p = Process(target=buy_ticket,args=(i,lock))
        p.start()

```

加锁保证了数据的安全，但也降低了执行效率。

在数据安全的基础上，才考虑效率问题。

应用场景：

- 共享数据资源（文件，数据库），且对资源进行修改删除操作。

### 4.进程间的通信IPC(inter process communication)

#### 4.1 Queue

- 基于文件家族的socket实现
- 写文件基于pickle
- 基于Lock,天生数据安全

```python
from multiprocessing import Process,Queue

def func(queue):
    queue.put(1) #向队列中推入数据
    queue.put(2)
    queue.put(3)

if __name__ == '__main__':
    queue =  Queue()
    p = Process(target=func,args=(queue,))
    p.start()
    print(queue.get())#从队列中取出数据，先进先出，和推入的顺序一样，不会混乱
    print(queue.get())
    print(queue.get())
    print(queue.get())#当队列中无数据时会阻塞，直到另一个进程在向其中推入一个数据
```

```python
from multiprocessing import Process,Queue

def func(queue):
    queue.put(1)
    queue.put(2)
    queue.put(3)
    queue.put(4)
    queue.put(5) #当队列满了，再向其中推入数据时会阻塞，直到其他进程取走数据


if __name__ == '__main__':
    queue =  Queue(4) #可以设置队列的大小
    p = Process(target=func,args=(queue,))
    p.start()
```

put_nowait()和get_nowait()。当队列满了或队列空了不会阻塞，会报错(queue.Full，queue.Emply)。put_nowait()报错会丢失最后推入导致报错的数据。

```python
from multiprocessing import Process,Queue
import queue

def func(qu):
    try:
        qu.put_nowait(1)
        qu.put_nowait(2)
        qu.put_nowait(3)
        qu.put_nowait(4)
        qu.put_nowait(5)
    except queue.Full:
        pass

if __name__ == '__main__':
    qu =  Queue(4)
    p = Process(target=func,args=(qu,))
    p.start()
    print(qu.get())#1
    print(qu.get())#2
    print(qu.get())#3
    print(qu.get())#4
    print(qu.get())#阻塞，5丢失了
```

```python
from multiprocessing import Process,Queue
import queue,time

def func(qu):
    try:
        qu.put_nowait(1)
        qu.put_nowait(2)
        qu.put_nowait(3)
        qu.put_nowait(4)
    except queue.Full:
        pass

if __name__ == '__main__':
    qu =  Queue()
    p = Process(target=func,args=(qu,))
    p.start()
    time.sleep(2)
    try:
        print(qu.get_nowait())
        print(qu.get_nowait())
        print(qu.get_nowait())
        print(qu.get_nowait())
        print(qu.get_nowait())
    except queue.Empty:
        pass
```

#### 4.2 Pipe管道

- 基于文件家族的socket,pickle,天生不安全。

```python
from multiprocessing import Pipe
pip = Pipe()
pip.send()
pip.recv()
```

Queue相当于Pipe加上异常处理。



