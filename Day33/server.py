#!/usr/bin/env python
# -*- coding:utf-8 -*-

import socket
from multiprocessing import Process

class MyServer(Process):
    def __init__(self,conn):
        super().__init__()
        self.conn = conn

    def run(self):
        msg = self.conn.recv(1024).decode('utf-8')
        print(msg)
        self.conn.send(msg.upper().encode('utf-8'))


if __name__ == '__main__':
    sk = socket.socket()
    sk.bind(('127.0.0.1',8066))
    sk.listen() 

    while True:
        conn,addr = sk.accept()
        ms = MyServer(conn)
        ms.start()