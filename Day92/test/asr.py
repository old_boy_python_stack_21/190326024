from aip import AipSpeech,AipNlp
import os,requests

""" 你的 APPID AK SK """
APP_ID = '16981797'
API_KEY = 'gONt3gYX6bc5yRRGfgEvRG9G'
SECRET_KEY = 'xBKa0yW6UFYgu5Xntpwy2XP1j4dx5zZL'

sclient = AipSpeech(APP_ID, API_KEY, SECRET_KEY)
client = AipNlp(APP_ID, API_KEY, SECRET_KEY)

# 读取音频文件
def get_file_content(filePath):
    cmd = f'ffmpeg -y  -i {filePath}  -acodec pcm_s16le -f s16le -ac 1 -ar 16000 {filePath}.pcm'
    os.system(cmd)
    with open(f'{filePath}.pcm', 'rb') as fp:
        return fp.read()

# 识别音频文件内容
def asr(file_name):
    result = sclient.asr(get_file_content(file_name), 'pcm', 16000, {
        'dev_pid': 1536,
    })
    return result.get('result')[0]

#短文本相似度
def nlp(q,q_std):
    res = client.simnet(q, q_std)
    return res.get('score')

#图灵机器人
def tuling(content):
    tl_url = 'http://openapi.tuling123.com/openapi/api/v2'
    data = {
        "perception": {
            "inputText": {
                "text": ""
            },
            "selfInfo": {
                "location": {
                    "city": "北京",
                    "province": "北京",
                    "street": "信息路"
                }
            },
        },
        "userInfo": {
            "apiKey": "2213889293634c759484cac88a91c170",
            "userId": "1561322asd223"
        }
    }
    data['perception']['inputText']['text'] = content
    res = requests.post(tl_url,json=data)
    return res.json().get('results')[0].get('values').get('text')

#语音合成
def tts(ans):
    result = sclient.synthesis(ans, 'zh', 1, {
        'vol': 5,
        'per': 4
    })

    # 识别正确返回语音二进制 错误则返回dict 参照下面错误码
    if not isinstance(result, dict):
        with open('answer.mp3', 'wb') as f:
            f.write(result)

    os.system('answer.mp3')

#入口
def run():
    Q = asr('weather.m4a')
    print(Q)

    A = '我不知道你在说什么'
    if nlp(Q,'你是谁') > 0.6:
        A = '我是全英雄联盟最骚的骚猪'
    else:
        A = tuling(Q)

    tts(A)

if __name__ == '__main__':
    run()
