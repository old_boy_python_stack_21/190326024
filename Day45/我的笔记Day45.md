# Day45

## 今日内容

1. 浮动
   - 浮动的现象
   - 浮动带来的问题
   - 清除浮动

2. 定位

## 内容详细

### 1.浮动

浮动作为布局方案，实现了元素并排效果。

#### 1.1 浮动的现象

- 脱离了标准文档流，不在页面上占位置（脱标）。
- 文字环绕，设置浮动的初衷。
- 贴边现象，给盒子设置了浮动之后，找浮动盒子的边贴靠，如果找不到浮动盒子的边，就找父元素的边贴靠。

- 若没有为设置盒子的宽度，给盒子设置浮动后盒子宽度会收缩，大小为内容宽度。

#### 1.2 浮动带来的问题

若父元素未设置固定高度，则浮动元素不能撑起父元素的高度

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father{
            width: 800px;
            border: 1px solid #000;
        }

        .child1{
            width: 200px;
            height: 200px;
            background-color: red;
            float: left;
        }
        .child2{
            width: 200px;
            height: 200px;
            background-color: green;
            float: left;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

#### 1.3 清除浮动方法

1. 为父元素设置固定高度。

   - 缺点：不灵活，后期不易维护。
   - 应用场景：固定的导航栏上。

2. 内墙法

   给最后一个浮动元素的后面添加一个空的块级标签，并设置该标签的样式属性clear：both;

   缺点：一个网页会有很多浮动，每次都加空的块级标签会冗余，消耗带宽。

   解决方案：利用伪元素选择器。

   标准写法：

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <style>
           .father{
               width: 800px;
               border: 1px solid #000;
           }
   
           .child1{
               width: 200px;
               height: 200px;
               background-color: red;
               float: left;
           }
           .child2{
               width: 200px;
               height: 200px;
               background-color: green;
               float: left;
           }
           /*标准写法*/
           .clearfix::after{
               content: '.';
               display: block;
               clear: both;
               visibility: hidden;
               height: 0;
           }
   
       </style>
   </head>
   <body>
       <div class="father clearfix">
           <div class="child1"></div>
           <div class="child2"></div>
       </div>
   </body>
   </html>
   ```

   简便写法：

   ```css
   .clearfix::after{
       content:'';
       display:block;
       clear:both;
   }
   ```

3. 通过overflow属性

   overflow:设置超出盒子部分的状态。

   - visible  默认值，超出部分可见。
   - hidden 超出部分隐藏。
   - scroll   使用滚动条。
   - inherit   可继承，设置为该项后，当前元素会继承父元素的overflow属性

   设置overflow属性为hidden可以清除浮动。

   一旦设置了一个box盒子的overflow为visible之外的任意值，那么该box会变成一个BFC（Block Format Context）区域，形成的BFC区域有它自己的规则，计算BFC的高度时，浮动的元素也参与计算。

   **注意：使用overflow清除浮动时应该注意，flowover本身的特性。**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father{
            width: 800px;
            border: 1px solid #000;
            overflow: hidden;
        }

        .child1{
            width: 200px;
            height: 200px;
            background-color: red;
            float: left;
        }
        .child2{
            width: 200px;
            height: 200px;
            background-color: green;
            float: left;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

**补充：BFC区域**

1. BFC定义

      BFC(Block formatting context)直译为”块级格式化上下文”。它是一个独立的渲染区域，只有Block-level box参与， 它规定了内部的Block-level Box如何布局，并且与这个区域外部毫不相干。

2. BFC的一些布局规则

   ```
   1.内部的Box会在垂直方向，一个接一个地放置。
   2.Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠
   3.每个元素的margin box的左边， 与包含块border box的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此。
   4.BFC的区域不会与float 元素重叠。
   5.BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
   6.计算BFC的高度时，浮动元素也参与计算
   ```

3. 哪些元素会生成BFC

   ```
   1.根元素
   2.float属性不为none
   3.position为absolute或fixed
   4.display为inline-block
   5.overflow不为visible
   ```

### 2.定位

position  定位

- static  默认值，静态的
- relative 相对定位
- absolute  绝对定位
- fixed  固定定位

只要设置了position属性为除了static之外的其他值，就可以设置该元素的top,bottom,left,right属性值为该元素定位。

#### 2.1 relative 相对定位

参考点：以原来的盒子为参考点。

特征：

- 设置相对定位与标准文档流下的盒子没有任何区别，但是会覆盖标准文档流元素。
- 修改相对位置后，盒子原来的位置还会被占，影响页面布局

应用场景：一般用来做“子绝父相”布局方案的参考。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father{
            width: 800px;
            border: 1px solid #000;
        }

        .child1{
            width: 200px;
            height: 200px;
            background-color: red;
            position: relative;
            top: 20px;
            left: 30px;
        }
        .child2{
            width: 200px;
            height: 200px;
            background-color: green;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

#### 2.2 absolute 绝对定位

参考点：

- 如果单独设置一个盒子为绝对定位，以top来描述，它的参考点是以body的(0,0)为参考点。
- 如果单独设置一个盒子为绝对定位，以bottom来描述，它的参考点为浏览器的左下角。
- "子绝父相"，若父元素设置了相对定位，子元素设置了绝对定位，它会以最近的设置了相对定位的父辈元素的左上角为参考点。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father {
            width: 800px;
            border: 1px solid #000;
            position: relative;
        }

        .child1 {
            width: 200px;
            height: 200px;
            background-color: red;
            position: absolute;
            top: 20px;
            left: 20px;
        }

        .child2 {
            width: 200px;
            height: 200px;
            background-color: green;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

绝对定位特征：

- 脱离标准流，不占位置
- 会压盖标准流元素
- 子绝父相

### 3.其他

1. 行内标签设置了浮动就可以设置宽高。

2. vertical-align  调整文本垂直方向的位置

   - center
   - top
   - bottom
   - 也可以设置数值

   同样可以设置input标签的位置，默认为center。

3. list-style  
   - none 清除ul的样式

