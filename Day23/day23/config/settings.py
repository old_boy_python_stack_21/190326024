#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
LOG_FILE_PATH = os.path.join(BASE_PATH,'log','debug.log')
LOG_WHEN = 'd'
LOG_INTERNAL = 1