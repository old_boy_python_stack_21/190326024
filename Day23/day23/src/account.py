#!/usr/bin/env python
# -*- coding:utf-8 -*-

from db import data
from lib import encrypt
from lib.log import logger

def register():
    """
    用户注册
    :return:
    """
    print('用户注册')
    while True:
        try:
            user = input('请输入用户名(N返回上一级)：')
            if user.upper() == 'N':
                return
            pwd = input('请输入密码：')
            if user in data.USER_DICT:
                print('用户已经存在，请重新输入。')
                continue
            data.USER_DICT[user] = encrypt.encrypt_md5(pwd)
            print('%s 注册成功' % user)
            print(data.USER_DICT)
        except Exception as e:
            logger.error(str(e),exc_info=1)

def login():
    """
    用户登录
    :return:
    """
    print('用户登录')
    while True:
        try:
            user = input('请输入用户名(N返回上一级)：')
            if user.upper() == 'N':
                return
            pwd = input('请输入密码：')
            if user not in data.USER_DICT:
                print('用户名不存在')
                continue

            encrypt_password = data.USER_DICT.get(user)
            if encrypt.encrypt_md5(pwd) != encrypt_password:
                print('密码错误')
                continue

            print('登录成功')
            data.CURRENT_USER = user
            return
        except Exception as e:
            logger.error(str(e),exc_info=1)