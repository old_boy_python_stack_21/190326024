#!/usr/bin/env python
# -*- coding:utf-8 -*-

from src import news
from src import account
from lib.log import logger

def run():
    """
    主函数
    :return:
    """
    print('=================== 系统首页 ===================')
    func_dict = {'1': account.register, '2': account.login, '3': news.article_list}
    while True:
        try:
            print('1.注册；2.登录；3.文章列表')
            choice = input('请选择序号：')
            if choice.upper() == 'N':
                return
            func = func_dict.get(choice)
            if not func:
                print('序号选择错误')
                continue
            func()
        except Exception as e:
            logger.error(str(e),exc_info=1)