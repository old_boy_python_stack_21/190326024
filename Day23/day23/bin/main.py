#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import sys
from src.run import run

BASE_PASH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PASH)

if __name__ == "__main__":
    run()