#!/usr/bin/env python
# -*- coding:utf-8 -*-

import logging
from config import settings
from logging import handlers

def get_logger():
    file_handler = handlers.TimedRotatingFileHandler(
        filename=settings.LOG_FILE_PATH,
        when=settings.LOG_WHEN,
        interval=settings.LOG_INTERNAL,
        encoding='utf-8'
    )
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S %p',
        level=logging.ERROR,
        handlers = [file_handler,]
    )
    return logging

logger = get_logger()