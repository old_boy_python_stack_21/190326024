#!/usr/bin/env python
# -*- coding:utf-8 -*-

from db import data

def auth(func):
    def inner(*args, **kwargs):
        if data.CURRENT_USER:
            return func(*args, **kwargs)
        return False

    return inner