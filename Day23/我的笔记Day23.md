# Day23

## 今日内容

1. 单例模式
2. 模块导入
3. logging模块
4. 项目目录结构

## 补充

1. 字符串格式化

   ```python
   v = '姓名是：%(name)s,年龄：%(age)s'%{'name':'alex','age':19}
   print(v) #姓名是：alex,年龄：19
   
   v = '姓名是：{0},年龄：{1}'.format('alex',19)
   print(v) #姓名是：alex,年龄：19
   
   v = '姓名是：{name},年龄：{age}'.format(name='alex',age=19)
   print(v) #姓名是：alex,年龄：19
   ```

2. 有序字典

   ```python
   from collections import OrderedDict
   info = OrderedDict()
   info['k1'] = 1
   info['k2'] = 2
   ```

3. getattr(obj,func,None)替换hasattr(obj,func)

## 内容详细

### 1.单例模式

无论实例化多少次使用的都是第一次创建的那个对象

```python
class Singleton(object):
    instance = None
    def __new__(cls,*args,**kwargs):
        if not cls.instance:
            cls.instance = object.__new__(cls)
        return cls.instance

obj1 = Singleton()
obj2 = Singleton()

print(obj1 is obj2) #True
```

### 2.模块导入

多次导入，不会重新加载。模块在第一次导入时，会加载到内存中，其他文件再次导入时不会重新加载。

通过模块的导入特性也可以实现单例模式。

```python
#test.py文件
class Singleton(object):
    pass

OBJ = Singleton()

#其他文件
import test
obj = test.OBJ
```

### 3.logging模块

应用场景：对异常处理捕获到的内容，通过logging模块写入到日志文件中。

#### 3.1 基本使用

```python
import logging
logging.basicConfig(filename='x.log',level=logging.ERROR)#使用系统默认编码
#level登记  一般写ERROR
'''
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
'''
logging.error('错误',exc_info=True)# 只有大于等于配置的登记才能写入日志文件,exc_info为True保留堆栈信息
```

#### 3.2 内部原理

```python
import logging
#涉及3个对象FileHandler、Formatter、Logger
file_handler = logging.FileHandler('x1.log',mode='a',encoding='utf-8')#创建FileHandler对象
fmt_handler = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s') #创建Formatter对象
file_handler.setFormatter(fmt_handler)#把Formatter对象添加到FileHandler对象中

logger = logging.Logger('xxx',level=logging.ERROR)#创建Logger对象，第一个参数为logger对象名称，即Formatter格式化时的name
logger.addHandler(file_handler)#把FileHandler对象添加到Logger对象中


logger.error('错误',exc_info=True)
```

#### 3.3 推荐使用格式

- 写入到一个日志文件中

  ```python
  import logging
  file_handler = logging.FileHandler('x1.log',mode='a',encoding='utf-8')
  
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      level=logging.ERROR,
      datefmt='%Y-%m-%d %H:%M:%S %p',#日期时间格式化
      handlers=[file_handler,]
  )
  
  
  logging.error('错误',exc_info=True)
  ```

- 日志分割（按时间）

  ```python
  import logging
  from logging import handlers
  file_handler = handlers.TimedRotatingFileHandler(filename='x2.log',when='h',interval=1,encoding='utf-8')#when参数单位为小时，interval参数按几个小时来分割
  
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      level=logging.ERROR,
      datefmt='%Y-%m-%d %H:%M:%S %p',
      handlers=[file_handler,]
  )
  
  try:
      int('wwqe')
  except Exception as e:
      logging.error(str(e),exc_info=True)
  ```



### 4.项目目录结构

#### 4.1 对于一个脚本

只有一个文件，在文件内导入模块编写代码即可。

#### 4.2 对于但可执行文件

- config文件夹  存放配置文件
- db文件夹  存放数据文件
- lib文件夹  存放公共功能，类库
- src文件夹 存放业务相关代码
- log文件夹  存放日志文件
- app.py  可执行文件

#### 4.3 对于多可执行文件

- config文件夹  存放配置文件

  - setting.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    import os
    
    BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    LOG_FILE_PATH = os.path.join(BASE_PATH,'log','debug.log')
    LOG_WHEN = 'd'
    LOG_INTERVAL = 1
    ```

- db文件夹  存放数据文件

- lib文件夹  存放公共功能，类库

  - log.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    import logging
    from logging import handlers
    from config import settings
    def get_logger():
        file_handler = handlers.TimedRotatingFileHandler(filename=settings.LOG_FILE_PATH,
                                                         when=settings.LOG_WHEN,			                                                                  interval=settings.LOG_INTERVAL, 
                                                         encoding='utf-8')
        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s',
            level=logging.ERROR,
            handlers=[file_handler,]
        )
    
        return logging
    
    logger = get_logger()
    ```

- src文件夹 存放业务相关代码

  - run.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    
    from src import account
    def start():
        dic = {'1':account.login,'2':account.reg}
        print('1.登录\n2.注册')
    
        index = input('请选择')
        dic.get(index)()
    ```

  - account.py

    ```python
    from lib.log import logger
    def login():
        print('登录')
        logger.error('登录错误',exc_info=1)
    
    def reg():
        print('注册')
        logger.error('登录错误', exc_info=1)
    ```

- log文件夹  存放日志文件

- bin文件夹  存放可执行文件

  - student.py

    ```python
    import os
    import sys
    
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(PROJECT_PATH)
    from src import run
    
    if __name__ == '__main__':
        run.start()
    ```

  - teacher.py

    ```python
    import os
    import sys
    
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(PROJECT_PATH)
    from src import run
    
    if __name__ == '__main__':
        run.start()
    ```

    