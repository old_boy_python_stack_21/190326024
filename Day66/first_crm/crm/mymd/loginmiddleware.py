#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import redirect

class LoginMiddleware(MiddlewareMixin):
    def process_view(self,request,view_func,*args,**kwargs):
        lst = ['/crm/reg/','/crm/login/']
        print(request.path_info)
        if request.path_info not in lst:
            is_login = request.COOKIES.get('is_login')
            if not is_login:
               return redirect('login')