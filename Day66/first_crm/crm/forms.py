#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django import forms
from crm import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from comm.encrypt import encrypt_pwd

class RegForm(forms.ModelForm):
    password = forms.CharField(
        min_length=8,
        widget= forms.PasswordInput(attrs={'placeholder':'请输入密码','autocomplete':'off'})
    )
    re_password = forms.CharField(
        min_length=8,
        widget= forms.PasswordInput(attrs={'placeholder':'请再次输入密码','autocomplete':'off'})
    )
    mobile = forms.CharField(
        widget= forms.TextInput(attrs={'placeholder':'请输入手机号','autocomplete':'off'}),
        validators=[RegexValidator(r'^1[3-9]\d{9}$','手机号格式不正确')]
    )

    class Meta:

        model = models.UserProfile
        fields = '__all__'
        exclude = ['is_active']
        widgets = {
            'username': forms.EmailInput(attrs={'placeholder':'请输入邮箱地址','autocomplete':'off'}),
            'name': forms.TextInput(attrs={'placeholder':'请输入真实姓名','autocomplete':'off'}),
        }

    def clean_username(self):
        username = self.cleaned_data.get('username')
        obj = models.UserProfile.objects.filter(username=username).first()
        if obj:
            raise ValidationError('用户名已存在')
        else:
            return username

    def clean(self):
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if password == re_password:
            if password:
                self.cleaned_data['password'] = encrypt_pwd(password)
            return self.cleaned_data
        else:
            self.add_error('re_password','两次密码输入不一致')
            raise ValidationError('两次密码输入不一致')