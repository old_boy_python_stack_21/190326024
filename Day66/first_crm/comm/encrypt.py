#!/usr/bin/env python
# -*- coding:utf-8 -*-
import hashlib

def encrypt_pwd(pwd):
    key = 'admin123'
    obj = hashlib.md5(key.encode('utf-8'))
    obj.update(pwd.encode('utf-8'))
    return obj.hexdigest()