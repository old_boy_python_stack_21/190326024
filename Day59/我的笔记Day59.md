# Day59

## 今日内容

1. CBV和FBV
2. as_view的流程
3. 视图加装饰器
4. request对象
5. response对象

## 内容详细

### 1.CBV和FBV

#### 1.1 FBV

FBV：function based view   基于函数实现view逻辑代码

定义：

```python
from django.shortcuts import render,redirect
from appPublisher import models

def add_book(request):
    error = ''
    publisher_list = models.Publisher.objects.all().order_by('-pid')
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name,pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name,pub_id=publisher)
            return redirect('/book_list/')
    return render(request, 'book/add_book.html', {'error':error,'publisher_list':publisher_list})
```

使用：

```python
from django.conf.urls import url
from appPublisher import views

urlpatterns = [
    url(r'^add_book/', views.add_book),
]
```

#### 1.2 CBV

CBV：class based view  基于类实现的view代码

定义：

```python
from django.shortcuts import render,redirect
from appPublisher import models
from django.views import View

class AddBook(View):

    def get(self,request): #get请求逻辑代码
        error = ''
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        return render(request, 'book/add_book.html', {'error': error, 'publisher_list': publisher_list})

    def post(self,request):  #post请求逻辑代码
        error = ''
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name, pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name, pub_id=publisher)
            return redirect('/book_list/')
        return render(request, 'book/add_book.html', {'error': error, 'publisher_list': publisher_list})
```

使用：

```python
from django.conf.urls import url
from appPublisher import views

urlpatterns = [
    url(r'^add_book/', views.AddBook.as_view()),
]
```

### 2.as_view的流程

1.项目启动 加载ur.py时，执行类.as_view()    ——》  view函数

2.请求到来的时候执行view函数：

1. 实例化类  ——》 self 

   ​	self.request = request 

   2. 执行self.dispatch(request, *args, **kwargs)

      1. 判断请求方式是否被允许：
      2. 允许    通过反射获取到对应请求方式的方法   ——》 handler
      3. 不允许   self.http_method_not_allowed  ——》handler
      4. 执行handler（request，*args,**kwargs）

       返回响应   —— 》 浏览器

### 3.视图加装饰器

#### 3.1 FBV

直接加装饰器即可

```python
#定义装饰器
def timer(func):
    def inner(*args,**kwargs):
        start = time.time()
        ret = func(*args,**kwargs)
        print(time.time()-start)
        return ret
    return inner

@timer
def add_book(request):
    error = ''
    publisher_list = models.Publisher.objects.all().order_by('-pid')
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name,pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name,pub_id=publisher)
            return redirect('/book_list/')
    return render(request, 'book/add_book.html', {'error':error,'publisher_list':publisher_list})
```

#### 3.2 CBV

首先导入一个模块

```python
from django.utils.decorators import method_decorator
```

方式一：加在方法上，可以为特定的请求方法加装饰器

```python
@method_decorator(timer)
def get(self, request, *args, **kwargs):
    """处理get请求"""
```

方式二：加在自己重写的dispatch方法上，相当于给所有的请求方法都加装饰器

```python
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
    ret = super().dispatch(request, *args, **kwargs)
    return ret
```

方式三：加在类上

```python
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
class AddPublisher(View):
    
 
@method_decorator(timer,name='dispatch')
class AddPublisher(View):
```

注意：方法也可以直接加装饰器。

直接加装饰器和使用method_decorator的区别：

- 直接使用装饰器

  ```python
  '''
  func   ——》 <function AddPublisher.get at 0x000001FC8C358598>
  
  args  ——》 (<app01.views.AddPublisher object at 0x000001FC8C432C50>, <WSGIRequest: GET '/add_publisher/'>)
  '''
  ```

- 使用method_decorator

  ```python
  '''
  func ——》 <function method_decorator.<locals>._dec.<locals>._wrapper.<locals>.bound_func at 0x0000015185F7C0D0>
  
  args ——》 (<WSGIRequest: GET '/add_publisher/'>,)
  '''
  ```

### 4.request对象

#### 4.1 属性

- request.method   请求方式

- request.GET    url上携带的参数

- request.POST   post请求提交的数据

- request.path_info   URL路径  不包括ip和端口(域名)  不包含参数

- request.body   请求体    b''

- request.FILES    上传的文件

  html代码

  ```python
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta http-equiv="x-ua-compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Title</title>
  </head>
  <body>
  
  <form action="" method="post" enctype="multipart/form-data"> <!--上传文件需要改为multipart/form-data-->
      {% csrf_token %}
      <input type="file"  name="f1">
      <button>上传</button>
  </form>
  ```

  后台代码：

  ```python
  def upload(request):
      if request.method == 'POST':
          # 获取文件
          f1 = request.FILES.get('f1')
          # 保存文件
          with open(f1.name, 'wb') as f:
              for i in f1.chunks():
                  f.write(i)
      return render(request, 'upload.html')
  ```

- request.META    请求头信息
- request.COOKIES     cookie
- request.session   session

#### 4.2 方法

- request.get_full_path()   URL的路径   不包含ip和端口   包含参数
- request.is_ajax()    判断是否为ajax请求

### 5.response对象

- HttpResponse  

  ```python
  HttpResponse('字符串')
  ```

- render  返回完整页面

  ```python
  render(request,'模板的文件名',{k1:v1})
  ```

- redirect  重定向

  ```python
  redirect('重定向地址')  #请求头Localtion:地址
  ```

- JsonResponse   返回一个json对象

  ```python
  from django.http.response import JsonResponse
  
  #默认只能返回最外层为字段形式的json
  JsonResponse({})  #自动序列化为json串
  #若想响应字典对象，需要把safe参数设为False
  JsonResponse([],safe=False)
  ```

  

