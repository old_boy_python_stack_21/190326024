from django.shortcuts import render
from django.views import View

# Create your views here.
class UpLoad(View):
    def get(self,request):
        return render(request,'upload.html')

    def post(self,request):
        info = '上传失败'
        obj = request.FILES.get('up_file')
        with open(obj.name,'wb') as f:
            for i in obj.chunks():
                f.write(i)
        info = '上传成功'
        return render(request,'upload.html',{'info':info})

