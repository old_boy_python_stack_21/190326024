# Day18

## 今日内容

1. 回顾和补充
2. Day16作业讲解

## 内容详细

### 1.py2和py3的区别

1. 类型

    - py2:unicode类型（以unicode编码），str类型（其他编码）	

      ```python
      v = u"alex" #定义了unicode类型的变量
      v2 = "alex" #定义str类型的变量
      ```

    - py3：str类型(unicode编码) ，bytes类型（其他编码）

      ```python
      v1 = 'alex' #定义了一个str类型的变量，在内存中unicode编码
      v2 = b'alex' #定义了一个bytes类型变量
      ```

2. 字典的keys,values,items

    - py2：返回列表
    - py3：返回迭代器

3. map和filter

   - py2：返回列表
   - py3：返回迭代器

### 2.yield from 

```python
def base():
    yield 11
    yield 22

def func():
    yield 1
    yield from base()
    yield 3

for item in func():
    print(item)     #1 11 22 3
```

### 3.生成器推导式

```python
v = (i for i in range(10))
for item in v:
    print(v)
    
# 示例一
# def func():
#     result = []
#     for i in range(10):
#         result.append(i)
#     return result
# v1 = func()
# for item in v1:
#    print(item)

# 示例二
# def func():
#     for i in range(10):
#         def f():
#             return i
#         yield f
#
# v1 = func()
# for item in v1:
#     print(item())

# 示例三：
v1 = [i for i in range(10)] # 列表推导式，立即循环创建所有元素。
v2 = (lambda :i for i in range(10))
for item in v2:
    print(item())
```

面试题：请比较 [i for i in range(10)] 和 (i for i in range(10)) 的区别？

