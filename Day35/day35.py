#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
第35天作业
王凯旋
'''

# 3.使用多线程 实现一个请求网页 并且把网页写到文件中
'''
import re
import json
import requests
from threading import Thread
from queue import PriorityQueue

def producer(q,url,count):
    response = requests.get(url)
    q.put((count,response.text))


def consumer(q,f):
    while True:
        s = q.get()
        if not s: break
        com = re.compile(
            '<div class="item">.*?<div class="pic">.*?<em .*?>(?P<id>\d+).*?<span class="title">(?P<title>.*?)</span>'
            '.*?<span class="rating_num" .*?>(?P<rating_num>.*?)</span>.*?<span>(?P<comment_num>.*?)评价</span>', re.S)
        ret = com.finditer(s)
        for i in ret:
            dic = {
                "id": i.group("id"),
                "title": i.group("title"),
                "rating_num": i.group("rating_num"),
                "comment_num": i.group("comment_num")}
            f.write('%s\n'%(json.dumps(dic,ensure_ascii=False),))
            f.flush()


def cp(c_count,p_count):
    f1 = open('thread1_file.txt',mode='w',encoding='utf-8')
    f2 = open('thread2_file.txt',mode='w',encoding='utf-8')
    count = 0
    q = PriorityQueue(10)
    p_list = []
    for i in range(p_count):
        url = 'https://movie.douban.com/top250?start=%s&filter=' % count
        count += 25
        p = Thread(target=producer, args=(q, url,count))
        p.start()
        p_list.append(p)

    c1 = Thread(target=consumer, args=(q,f1))
    c1.start()
    c2 = Thread(target=consumer, args=(q,f2))
    c2.start()

    for i in p_list:
        i.join()
    for i in range(c_count):
        q.put(None)

    f1.close()
    f2.close()
'''


# 6.使用线程池，随意访问n个网址，并把网页内容记录在文件中
'''
import requests
from concurrent.futures import ThreadPoolExecutor

def get_content(url):
    res = requests.get(url)
    return {'file_name':url.rsplit('.')[1]+'.txt','content':res.text}

def write_file(ret):
    dic = ret.result()
    with open(dic['file_name'],mode='w',encoding='utf-8') as f:
        f.write(dic['content'])
        f.flush()

url_list = [
            'http://www.baidu.com',
            'http://www.tencent.com',
]

pool = ThreadPoolExecutor(5)
for url in url_list:
    ret = pool.submit(get_content,url)
    ret.add_done_callback(write_file)
'''


# 7.请简述什么是死锁现象？如何产生？如何解决
'''
两个线程分别拿到了两个资源的两把锁，但是两把所内部的逻辑都和另一把锁的资源有关联，
导致两个线程在各自的锁内执行等待获取另一把锁的使用权，无法结束。这种现象就是死锁现象。
原因：使用了多把锁（一把以上），多把锁交替使用
解决方法：优化代码，使用递归锁
'''


# 8.请说说你知道的并发编程中的哪些锁？各有什么特点？
'''
互斥锁：只能acquire一次，效率高
递归锁：可以acquire多次，但也要release同样的次数。效率低
'''


# 9.cpython解释器下的线程是否数据安全？
'''
数据不安全
当对全局变量执行 +=，-+ 等先计算后赋值的操作时，可能会产生数据错乱问题。
'''


# 10.尝试：在多进程中开启多线程。在进程池中开启线程池
'''
import time,random,os
from threading import Thread
from concurrent.futures import ThreadPoolExecutor,ProcessPoolExecutor
def tfunc(i):
    print('%sstart'%(i,),os.getpid())
    time.sleep(random.random())
    print('%send'%(i,),os.getpid())

def pfunc():
    tpool = ThreadPoolExecutor(5)
    for i in range(10):
        tpool.submit(tfunc,i)

if __name__ == '__main__':
    ppool = ProcessPoolExecutor(4)
    for i in range(5):
        ppool.submit(pfunc)
    ppool.shutdown()
'''
