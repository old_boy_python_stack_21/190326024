#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket
from threading import Thread


class Mythread(Thread):
    def __init__(self, conn):
        self.conn = conn
        super().__init__()

    def run(self):
        while True:
            msg = self.conn.recv(1024).decode('utf-8')
            print(msg)
            inp = input('>>>').encode('utf-8')
            self.conn.send(inp)


sk = socket.socket()
sk.bind(('127.0.0.1', 8066))
sk.listen()
while True:
    conn, addr = sk.accept()
    print('%s连接成功'%(addr,))
    Mythread(conn).start()