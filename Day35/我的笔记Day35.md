# Day35

## 今日内容

1. 锁
   - 互斥锁
   - 递归锁

2. 线程队列
3. 池

## 内容详细

### 1.锁

即使是线程，即使有GIL锁，也会出现数据不安全的问题。

如：+=，-=，*=，/=之类的先运算后赋值的操作。

发生数据安全的问题的条件：

- 操作的是全局变量
- 做了以下操作：+=，-+，*=，/=，还包括lst[0]+=1,dict['key'] +=1等。

线程加锁也会影响程序的执行效率，但保证了数据的安全。

多线程下的单例模式

```python
import time
from threading import Lock,Thread

class Singleton(object):
    __instance = None
    lock = Lock()

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __new__(cls, *args, **kwargs):
        with cls.lock:
            if not cls.__instance:
                time.sleep(0.1)
                cls.__instance = super().__new__(cls)
        return cls.__instance

def func():
    obj = Singleton('wusir', 18)
    print(obj)

for i in range(10):
    Thread(target=func).start()
```

#### 1.1 互斥锁

互斥锁是锁的一种，在同一个线程中，不能连续acquire多次。

```python
from threading import Lock
lock = Lock()
lock.acquire()
print('*'*20)  #***********************
lock.acquire() #阻塞，等待lock.release()
print('-'*20)
```

#### 1.2 死锁现象

```python
import time
from threading import Thread,Lock

def eat1(noodle_lock,fork_lock,name):
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    fork_lock.acquire()
    print('%s拿到了叉子'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下了面条'%(name))
    fork_lock.release()
    print('%s放下了叉子'%(name))

def eat2(noodle_lock,fork_lock,name):
    fork_lock.acquire()
    print('%s拿到了叉子' % (name))
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下了叉子' % (name))
    noodle_lock.release()
    print('%s放下了面条'%(name))

user_list = ['alex','wusir','taibai','yuan']
noodle_lock = Lock()
fork_lock = Lock()
Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[0])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[1])).start()
Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[2])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[3])).start()

#alex拿到了面条
#alex拿到了叉子
#alex吃了口面条
#alex放下了面条
#taibai拿到了面条
#alex放下了叉子
#wusir拿到了叉子
```

死锁现象是如何发生的？

- 有多把锁（一把以上）
- 多把锁交替使用

解决办法：

- 使用递归锁（解决死锁问题的根本原因：将多把互斥锁变成一把递归锁）
  - 使用递归锁可以快速解决死锁现象，但是效率差

- 优化代码
  - 通过代码优化还是用互斥锁解决死锁现象，执行效率高，但解决死锁现象的效率低（尤其代码复杂的情况）

通过代码优化解决死锁现象

```python
import time
from threading import Thread,Lock

def eat1(lock,name):
    lock.acquire()
    print('%s拿到了面条' % (name))
    print('%s拿到了叉子'%(name))
    print('%s吃了口面条' % (name))
    time.sleep(0.1)
    print('%s放下了面条'%(name))
    print('%s放下了叉子'%(name))
    lock.release()

def eat2(lock,name):
    lock.acquire()
    print('%s拿到了叉子' % (name))
    print('%s拿到了面条'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    print('%s放下了叉子' % (name))
    print('%s放下了面条' % (name))
    lock.release()

user_list = ['alex','wusir','taibai','yuan']
lock = Lock()
Thread(target=eat1,args=(lock,user_list[0])).start()
Thread(target=eat2,args=(lock,user_list[1])).start()
Thread(target=eat1,args=(lock,user_list[2])).start()
Thread(target=eat2,args=(lock,user_list[3])).start()
```

#### 1.3  递归锁

在同一个线程中，可以连续acquire多次，不会阻塞。

优点：在同一个线程中多次acquire多次，不会阻塞，但也要release同样的次数。

缺点：占用了更多地资源

递归锁也会发生死锁现象，多把递归锁交替使用也会产生死锁现象。

```python
import time
from threading import Thread,RLock

def eat1(noodle_lock,fork_lock,name):
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    fork_lock.acquire()
    print('%s拿到了叉子'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下了面条'%(name))
    fork_lock.release()
    print('%s放下了叉子'%(name))

def eat2(noodle_lock,fork_lock,name):
    fork_lock.acquire()
    print('%s拿到了叉子' % (name))
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下了叉子' % (name))
    noodle_lock.release()
    print('%s放下了面条'%(name))

user_list = ['alex','wusir','taibai','yuan']
noodle_lock = fork_lock = RLock()#定义了同一把递归锁

Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[0])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[1])).start()
Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[2])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[3])).start()
```

### 2.线程队列

queue模块，不能进行进程间通信，可以进行线程间通信。

#### 2.1 先进先出队列

通常用于用户请求，写一个server，所有的用户的请求放到队列中。先来先服务思想。

```python
from queue import Queue

q = Queue()
q.put(1)
q.put(2)
q.put(3)
print(q.get())#1
print(q.get())#2
print(q.get())#3

```

#### 2.2 后进先出队列(lifo)

last in first out 栈，多用于算法。

```python
from queue import LifoQueue

q = LifoQueue()
q.put(1)
q.put(2)
q.put(3)
print(q.get())#3
print(q.get())#2
print(q.get())#1
```

#### 2.3 优先级队列

多用于自动排序

```python
from queue import PriorityQueue

q = PriorityQueue()
q.put((10,'alex'))
q.put((4,'wusir'))
q.put((100,'taibai'))
print(q.get()) #(4,'wusir')
print(q.get()) #(10,'alex')
print(q.get()) #(100,'taibai')
```

### 3.池

预先开启固定个数的进程数/线程数，当任务来了，直接提交给已经开好的进程/线程去执行就可以了。

优点：

- 节省了进程/线程开启，关闭，切换的时间
- 减轻了操作系统的调度负担

#### 3.1 进程池

```python
import random,os,time
from concurrent.futures import ProcessPoolExecutor

def func():
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())

if __name__ == '__main__':
    pool = ProcessPoolExecutor(5)
    for i in range(10):
        pool.submit(func)
    pool.shutdown() #关闭池，不能再继续提交任务，并且会阻塞，直到所有已经提交的任务全部执行完成
    print('main')
```

```python
#参数和返回值
import random,os,time
from concurrent.futures import ProcessPoolExecutor

def func(i):
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
    return '%s+%s'%(os.getpid(),i,)

if __name__ == '__main__':
    pool = ProcessPoolExecutor(5)
    ret_list = []
    for i in range(10):
        ret = pool.submit(func,i) #返回一个对象
        ret_list.append(ret)
    for ret in ret_list:
        print(ret.result()) #调用result()方法等到返回值，会阻塞直到返回结果
    print('main')
```

#### 3.2 线程池

用法和进程池类似

```python
import random,os,time
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
    return '%s+%s'%(os.getpid(),i,)


pool = ThreadPoolExecutor(20)
ret_list = []
for i in range(10):
    ret = pool.submit(func,i) #返回一个对象
    ret_list.append(ret)
for ret in ret_list:
    print(ret.result()) #调用result()方法等到返回值，会阻塞直到返回结果
print('main')
```

池的map方法：

```python
import random,os,time
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
    return '%s+%s'%(os.getpid(),i,)


pool = ThreadPoolExecutor(20)
ret = pool.map(func,range(10)) #将可迭代对象的每个元素当做参数传入函数中，返回一个生成器，生成器内部是函数返回值
pool.shutdown()
print('main')
for i in ret:
    print(i)
```

池的回调函数add_down_callback()

```python
import random,os,time,requests
from concurrent.futures import ThreadPoolExecutor

def func(url):
    res = requests.get(url)
    return {'url':url,'content':res.text}

def func2(ret):
    dic = ret.result()
    print(dic['url'])

url_list = [
    'http://www.baidu.com',
    'http://www.cnblogs.com',
    'http://www.douban.com',
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]

pool = ThreadPoolExecutor(20)
for url in url_list:
    ret = pool.submit(func,url)
    ret.add_done_callback(func2)#绑定回调函数，哪个任务先执行完，哪个就先调用回调函数，提高了效率
```

















