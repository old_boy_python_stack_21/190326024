# Day12

## 今日内容

1. 函数中高级
2. 内置函数
3. 模块

## 补充

- 函数和函数的执行不会相互影响，函数内部执行相互之间不会混乱
- 函数的销毁条件
  - 函数执行完成
  - 函数内部数据无人使用

## 内容详细

### 1.函数中高级

#### 1.1 函数作为返回值

```python
def func()
	print(123)
def bar():
    return func
result = bar()
result() #123
```

```python
name = "oldboy"
def func():
	print(name)

def bar():
    return func

result = bar()
result() #oldboy
```

```python
def func()
	def inner():
        print(123)
    return inner

result = func()
result() #123
```

```python
name = 'oldboy'
def func():
    name = 'eric'
    def inner():
        print(name)
    return inner
v = func()
v() #eric
```

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```



#### 1.2 闭包

为函数开辟一块内存空间（内部变量供自己使用），为他以后执行提供数据。

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

练习题

```python
name = 'oldboy'
def func():
    print(name)
def bar()
	name = 'alex'
    return func
v = bar()
v() #oldboy
```

```python
name = 'oldboy'
def func():
	name = 'alex'
    def inner():
        print(name)
    return inner
v = func()
v() #alex     
#注意：看函数是在哪里定义的
```

面试题

```python
info = []

def func(name):
	def inner():
		print(name)
    return inner

for item in range(10):
    info.append(func(item)

info[0]() #0 闭包{name：0 inner}
info[2]() #2 闭包{name：2 inner}
info[4]() #4 闭包{name：4 inner}
```

#### 1.3 高阶函数

- 把函数当做参数传递
- 把函数当做返回值返回

注意：函数的赋值

#### 1.4 总结

- 函数执行流程分析（函数到底是谁创建的）
- 闭包概念：为函数开辟一块内存空间（变量供自己使用），为它以后执行时提供数据。
- 闭包应用场景：装饰器、SQLAlchemy

### 2.内置函数

#### 2.1 编码相关

- chr()  将十进制整数转换成unicode编码的字符

  ```python
  v = 65
  print(chr(v)) #A
  ```

- ord() 将字符转换成Unicode编码对应的十进制整数

  ```python
  v = 'a'
  print(ord(v))#97
  ```

#### 2.2 高级内置函数

- map(函数,可迭代对象)    映射    输入元素数量 = 输出元素数量 ，且一一对应。

  ```python
  def func(a):
      return a+100
  
  lst = [1,2,3,4]
  result = map(func,lst)  #内部会遍历lst,将每个元素传给函数，然后将返回值放入新列表。不会影响原列表
  print(result)  # [101,102,103,104] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

  ```python
  #计算两个列表对应元素相加
  lst1 = [1,2,3,4,5]
  lst2 = [10,9,8,7,6]
  result = map(lambda x,y:x+y,lst1,lst2)
  print(list(result)) #[11, 11, 11, 11, 11]
  ```

  

- fliter(函数,可迭代对象)    筛选    输入元素数量 >= 输出元素数量

  ```python
  def func(a):
      return a > 3
  lst = [1,2,3,4,5]
  result = filter(func,lst) #内部会遍历lst,将每个元素传给函数，然后根据函数返回的真假确定是否添加到新列表。不会影响原列表
  print(result)  # [4,5] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

- reduce(函数,可迭代对象)    多对一  （py3把该函数放入functools模块中，需导入模块后使用）

  ```python
  import functools
  def func(x,y):       #函数参数必须为两个
      return x + y
  lst = [1,2,3,4,5]
  result = functools.reduce(func,lst)#内部会遍历lst，第一次循环时传入前两个元素，后面每次循环传入上次循环函数的返回值和下一个元素
  print(result) #15
  ```

### 3.模块

#### 3.1 随机数   random

```python
import random
v = random(1,3)
print(v) #随机生成一个1~3的整数
```

可以和内置函数chr()内置函数搭配使用生成随机验证码

```python
import random
def func(count):
    lst = []
    for i in range(0,count):
        val = random.randint(65,90)
        lst.append(chr(val))
    return ''.join(lst)
result = func(4)
print(result)
```

#### 3.2 字符串加密   hashlib

```python
import hashlib

def func(pwd):
    key = "asdw3221" #秘钥
    obj = hashlib.md5(key.encode("utf-8")) #在内存中字符串编码是unicode。在py3中需要转换成utf-8编码。否则报错
    obj.update(pwd.encode('utf-8')) #加密字符串
    result = obj.hexdigest() #得到密文
    return result

print(func('123'))
```

#### 3.3 密码不显示   getpass(只在终端运行时有效果)

```python
import getpass

pwd = getpass.getpass('请输入密码：')
print(pwd)
```

## 总结

1. 函数

   - 函数基本结构
   - 高级
     - 参数
     - 返回值
     - 闭包

2. 内置函数

   - 编码相关
     - chr()
     - ord()

   - 高级内置函数
     - map()
     - filter()
     - reduce()

3. 模块

   - random
   - hashlib
   - getpass