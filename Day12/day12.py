#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第十二天作业
王凯旋
'''

# 1.写出三元运算的基本格式及作用？
'''
'123' if 2 > 3 else '456'

#作用：
简化代码
'''

# 2.什么是匿名函数？
'''
# 没有名称的函数lambda表达式
lambda a, b: a + b
'''

# 3.尽量多的列举你了解的内置函数？【默写】
'''
a.类型强转
int()
bool()
str()
list()
tuple()
dict()
set()
b.输入输出
input()
print()
c.数学相关
abs()
max()
min()
sum()
float()
divmod()
pow()
round()
d.进制转换
int()
bin()
oct()
hex()
e.编码相关
chr()
ord()
f.高级内置函数
map()
filter()
reduce()
g.其他
len()
open()
range()
id()
type()
'''

# 4.filter/map/reduce函数的作用分别是什么？
'''
filter:筛选一个可迭代对象
map:对一个可迭代对象的所有元素进行相同的操作，得到一个与原对象元素个数相同的新的可迭代对象
reduce:对一个可迭代对象的所有元素进行操作（例如累加，拼接），得到一个值
'''

# 5.看代码写结果
'''


def func(*args, **kwargs):
    print(args, kwargs)
'''

# a. 执行 func(12,3,*[11,22]) ，输出什么？
'''
结果：(12,3,11,22) {}
'''
# b. 执行 func(('alex','武沛齐',),name='eric')
'''
结果：(('alex','武沛齐',),) {'name':'eric'}
'''

# 6.看代码分析结果
'''
def func(arg):
    return arg.pop(1)

result = func([11,22,33,44])
print(result)
'''

'''
结果：22
分析：列表的pop()方法返回删除的值
'''

# 7.看代码分析结果
'''
func_list = []

for i in range(10):
    func_list.append(lambda :i)

v1 = func_list[0]()
v2 = func_list[5]()
print(v1,v2)
'''

'''
结果：9 9
分析：循环中向列表中添加了十个相同的函数，循环结束i的值为9，这时执行两个函数返回i的值都是9
'''

# 8.看代码分析结果
'''
func_list = []

for i in range(10):
    func_list.append(lambda x:x+i)

v1 = func_list[0](2)
v2 = func_list[5](1)
print(v1,v2)
'''

'''
结果：11 10
分析：循环中向列表中添加了十个相同的函数，循环结束i的值为9，这时执行两个函数返回值分别是9+2=11 和9+1=10
'''

# 9.看代码分析结果
'''
func_list = []

for i in range(10):
    func_list.append(lambda x:x+i)

for i in range(0,len(func_list)):
    result = func_list[i](i)
    print(result)
'''

'''
结果：0,2,4,6,8,10,12,14,16,18
分析：第一个循环结束后列表中添加了10个方法，此时i的值为9，当第二个循环开始后，第一次i的值重新赋为0所以结果为0+0=0，
以此类推
'''

# 10.看代码分析结果
'''
def f1():
    print('f1')

def f2():
    print('f2')
    return f1

func = f2()   #执行f2函数打印"f2",并将f1函数赋值给func
result = func()  #执行f1函数打印"f1",f1函数无返回值，所以result为None
print(result)
'''

'''
结果： f2
       f1
       None
'''

# 11.看代码分析结果【面试题】
'''
def f1():
    print('f1')
    return f3()

def f2():
    print('f2')
    return f1

def f3():
    print('f3')

func = f2()   #执行f2函数打印"f2"，将f1函数赋值给func
result = func() #执行f1函数打印"f1"，f1函数内执行函数f3打印"f3",同时f3函数无返回值 所以result=None
print(result) #打印"f3"
'''

'''
结果：f2  f1  f3 None
'''

# 12.看代码分析结果
'''
name = '景女神'

def func():
    def inner():
        print(name)
    return inner()

v = func()  #执行func函数，func函数中执行inner函数,打印name,找到全局变量name打印，inner函数无返回值，所以v=None
print(v) #打印None
'''

'''
结果：景女神 None
'''

# 13.看代码分析结果
'''
name = '景女神'

def func():
    def inner():
        print(name)
        return "老男孩"
    return inner()

v = func()#执行func函数，func函数中执行inner函数,打印name,找到全局变量name打印，inner函数返回"老男孩"，所以v="老男孩"
print(v) #打印"老男孩"
'''

'''
结果：景女神 老男孩
'''

# 14.看代码分析结果
'''
name = '景女神'

def func():
    def inner():
        print(name)
        return '老男孩'
    return inner

v = func() #执行func函数，将inner函数赋值给v
result = v()#执行inner函数，打印name，name的值为全局变量"景女神",同时将inner函数返回值"老男孩"赋值给result
print(result)#打印"老男孩"
'''

'''
结果：景女神 老男孩
'''

# 15.看代码分析结果
'''


def func():
    name = '武沛齐'

    def inner():
        print(name)
        return '老男孩'

    return inner


v1 = func()
v2 = func()
print(v1, v2)
'''

'''
结果：函数地址
'''

# 16.看代码写结果
'''
def func(name):
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func('金老板')
v2 = func('alex')
print(v1,v2)
'''

'''
结果：函数地址
'''

# 17.看代码写结果
'''
def func(name=None):
    if not name:
        name= '武沛齐'
    def inner():
        print(name)
        return '老男孩'
    return inner

v1 = func()
v2 = func('alex')
print(v1,v2)
'''

'''
结果：函数地址 
'''

# 18.看代码写结果【面试题】
'''
def func(name):
    v = lambda x:x+name
    return v

v1 = func('武沛齐')
v2 = func('alex')
v3 = v1('银角')
v4 = v2('金角')
print(v1,v2,v3,v4)
'''

'''
结果：函数地址 银角武沛齐 金角alex
'''
# 19.看代码写结果
'''
NUM = 100
result = []
for i in range(10):
    func = lambda : NUM      # 注意：函数不执行，内部代码不会执行。
    result.append(func)

print(i)
print(result)
v1 = result[0]()
v2 = result[9]()
print(v1,v2)
'''

'''
结果：9
     列表内存放10个函数地址
     100 100
'''

# 20.看代码写结果【面试题】
'''
result = []
for i in range(10):
    func = lambda : i      # 注意：函数不执行，内部代码不会执行。
    result.append(func)

print(i)
print(result)
v1 = result[0]()
v2 = result[9]()
print(v1,v2)
'''

'''
结果：9
     列表内存放10个函数地址
     9 9
'''

# 21.看代码分析结果【面试题】
'''


def func(num):
    def inner():
        print(num)

    return inner


result = []
for i in range(10):
    f = func(i)
    result.append(f)

print(i)  # 循环结束后i值为9
print(result)  # 循环结束后将func函数的返回值inner函数追加到了列表，所以打印这10个函数的地址
v1 = result[0]()  # 在循环的为列表赋值时形成了闭包，每个函数保留了自己的数据供自己使用所以执行inner函数打印各自的num
v2 = result[9]()
print(v1, v2)  # inner函数无返回值，所以打印两个None
'''

'''
结果：9
     列表内存放10个函数地址
     0 
     9
     None None
'''
# 22.程序设计题
'''


# 主函数
def main():
    print("******欢迎使用老子的购物商城******")
    print("1.商品管理")
    print("2.会员管理")
    while True:
        index = input("请选择：").strip()
        if not index.isdecimal():
            print("输入有误，请重新输入")
            continue
        index = int(index)
        if index == 1:
            goods_manage()  # 进入商品管理界面
        elif index == 2:
            print("此功能不可用，正在开发中，请重新选择。")
            continue
        else:
            print("输入有误，请重新输入")
            continue


# 商品管理界面
def goods_manage():
    index_dict = {1: query_goods_list, 2: query_goods, 3: add_goods}  # 商品管理功能字典
    print("******欢迎使用老子的购物商城【商品管理】******")
    print("1.查看商品列表")
    print("2.根据关键字搜索指定商品")
    print("3.录入商品")
    while True:
        index = input("请选择（输入N返回上一级）：").strip()
        if index == "N":
            main()
        if not index.isdecimal():
            print("输入有误，请重新输入")
            continue
        index = int(index)
        if index in index_dict:
            index_dict.get(index)()  # 根据输入进入不同的功能
        else:
            print("输入有误，请重新输入")
            continue


# 查找所有商品
def query_goods_list():
    lst = read_file("goods.txt")
    page = 1
    per_page_count = 6
    total_page, value = divmod(len(lst), per_page_count)
    if value > 0:
        total_page += 1
    goods_page(page, lst)  # 展示第一页商品
    while True:
        info = input("请输入P/L查看上一页/下一页或输入页码查看，输入N返回上一级：")
        if info == "P":
            if page > 1:
                page -= 1
        elif info == "L":
            if page < total_page:
                page += 1
        elif info == "N":
            goods_manage()  # 返回商品管理界面
        elif info.isdecimal():
            info = int(info)
            if 1 <= info <= total_page:
                page = info
            else:
                print("输入页码范围有误，请重新输入")
                continue
        else:
            print("输入有误，请重新输入")
            continue
        goods_page(page, lst)  # 展示商品


# 搜索商品
def query_goods():
    lst = read_file("goods.txt")
    show_list = []
    while True:
        show_list.clear()
        keyword = input("请输入关键字（输入N返回上一级）：")
        if keyword == "N":
            goods_manage()  # 返回商品管理界面
        for item in lst:
            if keyword in item.split("|")[0]:
                show_list.append(item)  # 获取满足条件的商品
        show_goods(show_list)  # 展示商品信息


# 录入商品
def add_goods():
    while True:
        goods_name = input("请输入商品名称（输入N返回上一级）：")
        if goods_name == "N":
            goods_manage()  # 返回商品管理界面
            break
        goods_price = input("请输入商品价格：")
        goods_count = input("请输入商品数量：")
        temp = "%s|%s|%s\n" % (goods_name, goods_price, goods_count)
        write_file("goods.txt", temp)  # 将商品信息写入文件


# 商品信息分页
def goods_page(now_page, goods_list):
    pre_page_count = 6
    total_page, value = divmod(len(goods_list), pre_page_count)
    if value > 0:
        total_page += 1
    if now_page < 1:
        now_page = 1
    if now_page > total_page:
        now_page = total_page
    start = (now_page - 1) * pre_page_count
    end = now_page * pre_page_count
    show_goods(goods_list[start:end])  # 展示商品信息


# 展示商品信息
def show_goods(lst):
    print("查询结果如下：")
    if not lst:
        print("无满足条件的商品")
        return
    for i in lst:
        temp = i.strip()
        print(temp.replace("|", "  "))


# 读取商品文件
def read_file(file_name):
    lst = []
    with open(file_name, mode="r", encoding="utf-8") as goods_file:
        content = goods_file.read()
        lst = content.split("\n")
    return lst


# 写入商品文件
def write_file(file_name, content):
    with open(file_name, mode="a", encoding="utf-8") as goods_file:
        goods_file.write(content + "\n")
        goods_file.flush()


main()  # 执行主函数

'''
