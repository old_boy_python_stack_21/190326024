# Python学习笔记

## 一、计算机基础

### 1.1 硬件

​	主板、CPU、硬盘、显卡、网卡、内存...

### 1.2 操作系统

- windows 图形界面好
  - window 7
  - window 10
- linux  开源免费
  - centos(公司线上使用版本)
  - redhat
  - ubuntu
- mac 办公方便

### 1.3 解释器和编译器

语言开发者编写的一种软件，将用户写的代码转换成二进制，交给计算机执行

#### 1.3.1 解释型语言和编译型语言的区别

- 解释型语言：将编写好的代码交给解释器，解释器解释一句就执行语句。如：Python、PHP
- 编译型语言：将编写好的代码交给编译器编译成新的文件，然后交给计算机执行。如：C/C++、Java

### 1.4 软件

应用程序

### 1.5 进制

- 二进制：文件存储，网络传输都是二进制
- 八进制：
- 十进制：人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。
- 十六进制：多用来表示复杂的东西。\x

## 二、Python入门

### 2.1 环境

#### 2.1.1 解释器环境

​	python2、python3

#### 2.1.2 代码开发工具（IDE）

​	PyCharm

### 2.2 编码

- ascii:一个字节表示一个字符
- unicode：万国码可以表示所有字符
  - ecs2:2个字节表示一个字符
  - ecs4:4个字节表示一个字符
- utf-8:Unicode的压缩版，3个字节表示一个中文字符
- gbk:亚洲字符编码，两个字节表示一个字符
- gb2312:2个字节表示一个字符

### 2.3 变量

2.3.1 为什么要定义变量？

​	方便以后代码调用

2.3.2 变量命名规范

- 只能包括数字、字母以及下划线。
- 不能以数字开头
- 不能是python的关键字
- 建议：见名知意，多个单词用下划线隔开

### 2.4 条件判断

```python
if 1>2:
    print()
elif 1<2:
    print()
else:
	print()
```

### 2.5 while循环

```python
while True:
    print()
```

- while
- break:结束当前循环
- continue:结束本次循环，回到循环条件判断处
- pass:跳过
- while  else:当while的条件不满足时，进入else。break掉的循环不会进入else。

### 2.6 运算符

- 算数运算符

  ```python
  +,-,*,/,%,**,//
  ```

- 比较运算符

  ```python
  ==,>=,<=,!=,>,<
  ```

- 赋值运算符

  ```python
  =,+=,-=,*=,/=,**=,//=,%=
  ```

- 逻辑运算符

  - not

  - and

    ```python
    #当第一值为假，取第一值
    #当第一值为真，取第二值
    #有多个判断时，按照上述规则，从左到右依次比较
    1 and 2
    ```

  - or

    ```python
    #当第一值为假，取第二值
    #当第一值为真，取第一值
    #有多个判断时，按照上述规则，从左到右依次比较
    1 or 2
    ```

  优先级：not>and>or

- 成员运算符

  - in 
  - not in

### 2.7 注释

```python
#单行注释

'''
多行注释
'''
```

### 2.8 单位

```python 
1 byte = 8 bit
1 kb = 1024 byte
1 Mb = 1024 kb
...
```

### 2.9 range()

```python
#产生一组数字（高仿列表）
value = range(0,9) #不包括9
```



### 2.10 py2和py3的区别

- 默认解释器编码
  - py2:ascii
  - py3:utf-8
- 输入
  - py2: raw_input()
  - py3: input()
- 输出
  - py2: print ""
  - py3: print()
- 整数相除
  - py2: 只保留整数位
  - py3: 全部保留
- 整型
  - py2: int/long
  - py3:只有int

### 2.11 三元运算符（三目运算符）

前面 if 表达式 else 后面

```python
#表达式为True取前面的值，否则取后面的值
v =  a if a > b else b
```

## 三、数据类型

### 3.1 整型（int）

### 3.2 布尔类型（bool）

0, "", [], (), {}, set(), None转换成为布尔值时为False,其他全部为True.

### 3.3  字符串（str）

- upper() 转大写

  ```python
  #upper()转换大写
  v1 = "alex"
  v2 = v1.upper()
  print(v2) #ALEX
  ```

- lower() 转小写

  ```python
  #lower()转换小写
  v1 = "ALEX"
  v2 = v1.upper()
  print(v2)  #alex
  ```

- isdecimal() 是否为十进制数

  ```python
  v1 = "12345"
  print(v1.isdecimal()) #True
  ```

- strip() 去空格

  ```python
  v1 = " alex "
  print(v1.strip()) #alex
  ```

- replace() 替换

  ```python
  v1 = "aaaa111"
  print(v1.replace("111","bbb")) #aaaabbb
  ```

- split() 分割

  ```python
  v1 = "aaaa|bbbb"
  print(v1.split("|")) #['aaaa', 'bbbb']
  ```

- format() 格式化

  ```python
  v1 = "他叫{0}，今年{1}岁"
  print(v1.format("张三",18)) #他叫张三，今年18岁
  ```

- startswith() 以...开头

  ```python
  #startswith()
  v1 = "abc"
  print(v1.startswith("a"))#True
  ```

- endswith()  以...结尾

  ```python
  #endswith()
  v1 = "abc"
  print(v1.endswith("a"))#False
  ```

- encode()  编码

  ```python
  v1 = "你好"
  print(v1.encode("utf-8"))# b'\xe4\xbd\xa0\xe5\xa5\xbd'
  ```

- join() 连接

  ```python
  v1 = "你好"
  print("_".join(v1))# 你_好
  ```

### 3.4 列表（list）

- append()  追加

  ```python
  v1 = ["111","222","333","444","555"]
  v1.append("666")
  print(v1)# ['111', '222', '333', '444', '555', '666']
  ```

- insert() 插入

  ```python
  v1 = ["111","222","333","444","555"]
  v1.insert(1,"666")
  print(v1)# ['111', '666', '222', '333', '444', '555']
  ```

- extend()  扩展

  ```python
  v1 = ["111","222","333","444","555"]
  v1.extend(['777','888'])
  print(v1) #['111', '222', '333', '444', '555', '777', '888']
  ```

- pop() 删除

  ```python
  v1 = ["111","222","333","444","555"]
  v1.pop(1)
  print(v1) #['111', '333', '444', '555']
  ```

- remove() 移除

  ```python
  v1 = ["111","222","333","444","555"]
  v1.remove("333")
  print(v1)#['111', '222', '444', '555']
  ```

- clear() 清空

  ```python
  v1 = ["111","222","333","444","555"]
  v1.clear()
  print(v1) #[]
  ```

- reverse() 反转

  ```python
  v1 = ["111","222","333","444","555"]
  v1.reverse()
  print(v1)#['555', '444', '333', '222', '111']
  ```

- sort() 排序

  ```python
  v1 = [1,3,2,6,4]
  v1.sort(reverse=False)
  print(v1)#[1, 2, 3, 4, 6]
  ```

### 3.5 元组（tuple）

​	无

### 3.6 字典（dict）

- keys() 获取所有键

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.keys():
      print(i)
  #name
  #age
  ```

- values() 获取所有值

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.values():
      print(i)
  #alex
  #18
  ```

- items() 获取所有键值对

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.items():
      print(i)
  
  #('name', 'alex')
  #('age', 18)
  ```

- get() 根据键获取值

  ```python
  v1 = {"name":"alex","age":18}
  print(v1.get("name"))#alex
  ```

- update() 更新

  ```python
  v1 = {"name":"alex","age":18}
  v1.update({"gender":"男"})
  print(v1)# {'name': 'alex', 'age': 18, 'gender': '男'}
  ```

- pop() 根据键删除

  ```python
  v1 = {"name":"alex","age":18}
  v1.pop("name")
  print(v1)# {'age': 18}
  ```

### 3.7 集合（set）

集合元素为可哈希类型（不可变），但集合本身不可哈希。

- add() 添加

  ```python
  v1 = {1,2,3,4}
  v1.add(5)
  print(v1)# {1, 2, 3, 4, 5}
  ```

- discard() 删除

  ```python
  v1 = {1,2,3,4}
  v1.discard(4)
  print(v1)#{1, 2, 3}
  ```

- update() 批量添加

  ```python
  v1 = {1,2,3,4}
  v1.update((6,7,8))
  print(v1)#{1, 2, 3, 4, 6, 7, 8}
  ```

- intersection() 交集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.intersection(v2)
  print(v3)#{3, 4}
  ```

- union() 并集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.union(v2)
  print(v3)#{1, 2, 3, 4, 5, 6}
  ```

- difference() 差集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.difference(v2)
  print(v3)# {1, 2}
  ```

### 3.8 公共功能

- len() 获取长度

  - str
  - list
  - tuple
  - dict
  - set

- 索引

  - str
  - list
  - tuple

- 切片

  - str
  - list
  - tuple

- 步长

  - str
  - list
  - tuple

- for循环

  - str
  - list
  - tuple
  - dict
  - set

- 删除

  ```python
  del v[1]
  ```

  - list
  - dict (根据键删除)

- 修改

  - list
  - dict (根据键删除)

### 3.9 内存及深浅拷贝

#### 3.9.1 内存

- 为变量重新赋值时，计算机会为变量重新开辟内存。
- 修改变量内部元素时，不会重新开辟内存。

python的小数据池：

- 常用数字：-5~256，重新赋值时，不会开辟内存
- 字符串：只有带特殊字符，且*num，num>1，重新赋值会开辟内存

#### 3.9.2 深浅拷贝

```python
import copy
copy.copy() #浅拷贝 只拷贝第一层，里面元素地址相同
copy.deepcopy() #深拷贝 寻找所有的可变类型，拷贝
```

- 对于int，bool，str深浅拷贝相同
- 对于list，dict，set
  - 当无嵌套时，深浅拷贝相同。最外层地址不同
  - 当有嵌套时，深拷贝的内部嵌套地址不再相同

- 对于元组
  - 无嵌套，深浅拷贝元组的最外层地址相同
  - 当嵌套有可变类型时，深拷贝的地址不再相同

#### 3.9.3 type()

获取变量的类型

#### 3.9.4 id()

获取变量的内存地址

#### 3.9.5 == 和 is

- == 比较两个变量的值是否相等
- is 比较两个变量的内存是否相同

## 四、文件操作

### 4.1 文件操作步骤

- 打开文件

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  ```

- 操作文件

  ```python
  f.read()
  ```

- 关闭文件

  ```python
  f.close()
  ```

### 4.2 文件打开模式

读和写都会在光标位置开始。

- r/w/a
  - r：只读，文件不存在报错
  - w：只写，文件不存在会创建文件，在文件打开时会清空文件
  - a：追加，文件不存在会创建
- r+/w+/a+
  - r+：可读可写，打开文件光标在文件开头，写入会覆盖光标后面的字符。
  - w+：可读可写，打开文件时会清空文件，可通过移动光标来读取。
  - a+：可读可写，打开文件时光标在文件末尾，可通过移动光标读取，但只要写入光标就会移动到末尾。
- rb/wb/ab ：以二进制打开，不能指定编码，否则报错。多用于图片、视频、音频的读取。
- r+b/w+b/a+b

### 4.3 文件操作方法

- read() 

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  f.read()#将文件的所有内容一次读入内存
  f.read(1)#读取一个字符
  ```

- readlines()

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  lst = f.readlines()#将文件的所有内容一次读入内存,并按行放入列表中
  ```

- seek() 移动光标

  ```python
  seek(0) #将光标移动到开头
  seek(1) #将光标向后移动一个字节
  ```

- tell()  获取光标位置

- flush() 将内存中的数据强制写入硬盘文件

- 修改特别大的文件

  ```python
  with open("test.txt",mode="r",encoding="utf-8") as f1,open("test1.txt",mode="r",encoding="utf-8") as f2:
      for line in f1:
          content = line.replace("123","456")
          f2.write(content)
  ```

  

### 4.4 自动释放文件句柄

- with ... as....

  ```python
  with open("test.txt",mode="r",encoding="utf-8") as f:
      f.read
  #待缩进内的代码执行完成后，文件句柄会自动被释放
  ```

## 五、函数

截止目前，都是面向过程编程（可读性差/可重用性差）

对函数编程：

- 本质：将N行代码那到别处起个名字，以后通过名字就可以找到执行这段代码。
- 场景：
  - 代码重复执行
  - 代码量超过一屏，可以通过函数对代码进行分割。

函数执行：

- 函数和函数的执行不会相互影响，函数内部执行相互之间不会混乱
- 函数的销毁条件
  - 函数执行完成
  - 函数内部数据无人使用

### 5.1 基本结构

```python
def 函数名():
    #函数内容

#函数调用
函数名()
```

注意：函数如果不调用，将永远不会执行。

### 5.2 参数

#### 5.2.1 形参和实参

```python
def get_sum(a,b)  #这里ab为形参
	pass

v1 = 3
v2 = 4
get_sum(v1,v2)#这里的v1和v2为实参
```

#### 5.2.2 参数基本知识

- 任意类型
- 任意个数

#### 5.2.3 位置传参（调用函数时）

```python
def func(a1,a2,a3)：
	print(a1,a2,a3)
func(1,2,3)#按参数位置传递，一一对应
```

#### 5.2.4 关键字传参（调用函数时）

```python
def func(a1,a2,a3)：
	print(a1,a2,a3)
func(a1=1,a3=2,a2=3)#按参数名赋值
```

- 位置传参必须在关键字传参之前

  ```python
  def func(a1,a2,a3)：
  	print(a1,a2,a3)
  func(1,a3=2,a2=3)#正确
  func(a1=1,a2=2,3)#报错
  ```

#### 5.2.5 默认参数（定义函数时）

```python
def func(a1,a2,a3=3)：  #非默认参数必须在默认参数前
	print(a1,a2,a3)

func(a1=1,a2=2)#默认参数可以不传，不传的话使用默认值
```

#### 5.2.6 万能参数（定义函数时）

- 第一种

  ```python
  def func(*args): #不支持关键字传参，只能用位置传参，可以传入任意个位置参数，会转换成元组
  	print(args)
  
  func(1,2,3,4,5) #转换成元组(1,2,3,4,5)
  func((1,2,3,4,5))#会转换成元组((1,2,3,4,5),)
  func(*(1,2,3,4,5))#会循环形成元组(1,2,3,4,5)， 也可以是列表、集合
  ```

  ```python
  def func(a1,*args,a2):
      print(a1,args,a2)
  func(1,2,3,4,5,a2=10)#会将1传给a1,2,3,4,5传给args,a2必须用关键字传参	
  ```

- 第二种

  ```python
  def func(**kwargs): #不支持位置传参，只支持关键字传参，可以传入任意个关键字参数，会转换成字典
      print(kwargs)
      
  func(k1=123,k2=456)#转换为{'k1':123,'k2':456}
  func(**{'k1':123,'k2':456}) #将字典复制给kwargs
  ```

#### 5.2.7 综合应用

```python
def func(*args,**kwargs):
    print(args,kwargs)
    
func(1,2,3,4,k1='alex',k2=123)#(1,2,3,4)  {'k1':'alex','k2':123}
```

#### 5.2.8参数重点

```python
def func(a1,a2):
    pass

def func(a1,a2=None):
    pass

def func(*args,**kwargs):
	pass
```

### 5.3 返回值

- 在函数里，遇到return后后面的代码不再执行
- 不写的话，默认返回None

```python
def get_sum(a,b):
	return a+b

get_sum(2,3)
```

### 5.4 作用域

1. 在python中：
   - py文件：全局作用域
   - 函数：局部作用域，一个函数一个作用域

2. 作用域内的数据归自己所有，别人访问不到。局部作用域可以访问父级作用域的数据。

3. 作用域中数据查找规则：首先在自己的作用域内查找，自己的作用域内不存在就去父级作用域查找，直到全局作用域。

4. 子作用域只能找到父级作用域内的变量，默认不能为父级作用域的变量重新赋值。但可以修改可变类型（列表，字典，集合）内部的值。

5. 可以使用global关键字为全局作用域的变量重新赋值

   ```python
   name = 'oldboy'
   def func():
       global name
       name = 'alex'
   func()
   print(name) #alex
   ```

6. 可以使用nonlocal关键字为父级的变量重新赋值

   ```python
   name = 'oldboy'
   def func():
       name = 'alex'
       def func1():
   		nonlocal name
           name = "wusir"
       func1()
       print(name)
   func() #打印wusir
   ```

### 5.5 函数高级知识

#### 5.5.1 函数赋值

- 函数名当做变量使用

- 函数名可以放入集合中，也可当做字典的键

  ```python
  def func():
      return 123
  v = func
  print(v()) #123
  ```

  ```python
  def func():
      print(123)
  func_list = [func,func,func]
  func_list[0]() #123
  func_list[1]() #123
  func_list[2]() #123
  ```

#### 5.5.2 函数作参数传递

```python
def func(arg):
	arg()
def func1():
    print(123)
func(func1) #123
```

#### 5.5.3 函数作返回值

```python
def func()
	print(123)
def bar():
    return func
result = bar()
result() #123
```

```python
name = "oldboy"
def func():
	print(name)

def bar():
    return func

result = bar()
result() #oldboy
```

```python
def func()
	def inner():
        print(123)
    return inner

result = func()
result() #123
```

```python
name = 'oldboy'
def func():
    name = 'eric'
    def inner():
        print(name)
    return inner
v = func()
v() #eric
```

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

#### 5.5.4 闭包

为函数开辟一块内存空间（内部变量供自己使用），为它以后执行时提供数据。

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

练习题

```python
name = 'oldboy'
def func():
    print(name)
def bar()
	name = 'alex'
    return func
v = bar()
v() #oldboy
```

```python
name = 'oldboy'
def func():
	name = 'alex'
    def inner():
        print(name)
    return inner
v = func()
v() #alex     
#注意：看函数是在哪里定义的
```

面试题

```python
info = []

def func(name):
	def inner():
		print(name)
    return inner

for item in range(10):
    info.append(func(item)

info[0]() #0 闭包{name：0 inner}
info[2]() #2 闭包{name：2 inner}
info[4]() #4 闭包{name：4 inner}
```

闭包应用场景：装饰器、SQLAlchemy源码

#### 5.5.5 高阶函数

- 把函数当做参数传递
- 把函数当做返回值返回

### 5.6 lambda表达式(匿名函数)

为解决简单函数的情况，如：

```python
def func(a1,a2):
	return a1+a2
#等同于
v = lambda a1,a2:a1+a2
v(1,2)#3
```

```python
lst = []
func = lambda a1:lst.append(a1)
print(func(666)) #None
print(lst)#[666]
```

- lambda表达式的几种形式

  ```python
  func1 = lambda : 100
  
  func2 = lambda a1,a2:a1+a2
  
  func3 = lambda *args,**kwargs: print(args)
  
  DATA = 100
  func4 = lambda a1: a1+DATA  #只有一行代码，不能自己创建变量，只能去父级寻找
  ```

  

### 5.7 内置函数

#### 5.7.1 类型强转

- int()
- bool()
- str()
- list()
- tuple()
- dict()
- set()

#### 5.7.2 输入输出

- print()
- input()

#### 5.7.3 数学相关

- abs() 取绝对值

- max()  取最大值

  ```python
  print(max([11,22,66,754,78]))#754
  print(max(2,3))#3
  ```

- min()  取最小值

- sum()  求和

  ```python
  print(sum([1,2,3,4]))#10
  ```

- float()   转浮点类型

- divmod()  取两数相除的商和余数(可用于分页)

  ```python
  print(divmod(10,3)) #(3,1)
  print(divmod(3.5,2)) #(1.0,1.5)
  ```

- pow() 求指数

  ```python
  print(pow(2,3)) #2**3 = 8
  ```

  

- round()  取保留小数位（五舍六入）

  ```python
  v = 1.2345
  print(round(v,3)) #1.234
  ```

#### 5.7.4 进制转换

- bin()   将十进制转换成二进制

  ```python
  print(bin(15))  #0b1111
  ```

- oct()   将十进制转换成八进制

  ```python
  print(oct(15))  #0o17
  ```

- hex()  将十进制转换成十六进制

  ```python
  print(hex(15)) #0xf
  ```

- int()    将其他进制转换成十进制

  ```python
  v1 = '0b11111111'
  print(int(v1,base=2)) #255
  
  v2 = '77'
  print(int(v2,base=8)) #63
  
  v3 = 'ff'
  print(int(v3,base=16))#255
  ```

  - base默认值为10

#### 5.7.5 编码相关

- chr()  将十进制整数转换成unicode编码的字符

  ```python
  v = 65
  print(chr(v)) #A
  ```

- ord() 将字符转换成Unicode编码对应的十进制整数

  ```python
  v = 'a'
  print(ord(v))#97
  ```

#### 5.7.6 高级内置函数

- map(函数,可迭代对象)    映射    输入元素数量 = 输出元素数量 ，且一一对应。

  ```python
  def func(a):
      return a+100
  
  lst = [1,2,3,4]
  result = map(func,lst)  #内部会遍历lst,将每个元素传给函数，然后将返回值放入新列表。不会影响原列表
  print(result)  # [101,102,103,104] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

  ```python
  #计算两个列表对应元素相加
  lst1 = [1,2,3,4,5]
  lst2 = [10,9,8,7,6]
  result = map(lambda x,y:x+y,lst1,lst2)
  print(list(result)) #[11, 11, 11, 11, 11]
  ```

- fliter(函数,可迭代对象)    筛选    输入元素数量 >= 输出元素数量

  ```python
  def func(a):
      return a > 3
  lst = [1,2,3,4,5]
  result = filter(func,lst) #内部会遍历lst,将每个元素传给函数，然后根据函数返回的真假确定是否添加到新列表。不会影响原列表
  print(result)  # [4,5] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

- reduce(函数,可迭代对象)    多对一  （py3把该函数放入functools模块中，需导入模块后使用）

  ```python
  import functools
  def func(x,y):       #函数参数必须为两个
      return x + y
  lst = [1,2,3,4,5]
  result = functools.reduce(func,lst)#内部会遍历lst，第一次循环时传入前两个元素，后面每次循环传入上次循环函数的返回值和下一个元素
  print(result) #15
  ```

#### 5.7.7 其他

- len()
- open()
- range()
- id()
- type()

## 六、模块

### 6.1 基本概念

### 6.2 随机数 random

```python
import random
v = random(1,3)
print(v) #随机生成一个1~3的整数
```

可以和内置函数chr()内置函数搭配使用生成随机验证码

```python
import random
def func(count):
    lst = []
    for i in range(0,count):
        val = random.randint(65,90)
        lst.append(chr(val))
    return ''.join(lst)
result = func(4)
print(result)
```

### 6.3 字符串加密  hashlib

```python
import hashlib

def func(pwd):
    key = "asdw3221" #秘钥
    obj = hashlib.md5(key.encode("utf-8")) #在内存中字符串编码是unicode。在py3中需要转换成utf-8编码。否则报错
    obj.update(pwd.encode('utf-8')) #加密字符串
    result = obj.hexdigest() #得到密文
    return result

print(func('123'))
```

### 6.4 密码不显示   getpass

只有在终端中运行有效果

```python
import getpass

pwd = getpass.getpass('请输入密码：')
print(pwd)
```

## 七、面向对象

## 八、网络编程

## 九、并发编程

## 十、数据库

## 十一、前端开发

## 十二、Django框架

## 附录 常见错误和单词

### 单词

| 单词        | 解释   | 备注 |
| ----------- | ------ | ---- |
| preview     | 预览   |      |
| iterable    | 迭代   |      |
| refactor    | 重构   |      |
| indentation | 缩进   |      |
| generator   | 生成器 |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |
|             |        |      |



### 常见错误

