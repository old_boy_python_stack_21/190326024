# Day68

### 1.ModelForm组件的循环生成

循环ModelForm对象得到的是经过处理的ModelForm对象的字段

```html
{% for field in form_obj %}
   <div class="form-group {% if field.errors %}has-error{% endif %}">
       <label style="{% if not field.field.required %}color: #999{% endif %}"
        for="{{ field.id_for_label }}" class="col-md-3 control-label">{{ field.label }}		   </label>
       <div class="col-md-7">{{ field }}</div>
            <span class="help-block">{{ field.errors.0 }}</span>
       </div>
{% endfor %}
```

后台为ModelForm循环设置属性

```python
class CustomerForm(forms.ModelForm):
    class Meta:
        model = models.Customer
        fields = "__all__"
        # widgets = {
            # 'qq': forms.TextInput(attrs={'class': 'form-control'}),
        # }
    
    #在初始化ModelForm对象时，循环为每个字段添加属性
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for filed in self.fields.values():
            if isinstance(filed,MultiSelectFormField):
                continue
            filed.widget.attrs['class'] = 'form-control'
```

### 2.ModelForm组件编辑数据

```python
def oper_customer(request,pk=None):
    #查询要编辑的数据
    customer = models.Customer.objects.filter(pk=pk).first()
    #用要编辑的数据实例化ModelForm对象
    form_obj = CustomerForm(instance=customer)
    if request.method == 'POST':
        return_url = request.COOKIES.get('customer_url')
        #修改完成后，用修改后的数据和原始数据对象实例化ModelForm对象
        form_obj = CustomerForm(data=request.POST,instance=customer)
        if form_obj.is_valid():
            #保存ModelForm对象，完成数据修改
            form_obj.save()
            if return_url:
                return redirect(return_url)
            return redirect('my_customer')
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'oper_customer.html', {'form_obj': form_obj, 'title':title})
```

