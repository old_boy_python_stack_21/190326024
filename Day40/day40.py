#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第40天作业
王凯旋
'''


#单表练习
'''
#0.建表book，并向表中插入数据
create table book(
id int primary key auto_increment,
name char(12) not null,
author char(12),
press char(15),
price float(5,2),
publish_date date
);

insert into book (name,author,press,price,publish_date) values
('倚天屠龙记','egon','北京工业地雷出版社',70,20190701),
('九阳神功','alex','人民音乐不好听出版社',5,20180704),
('九阴真经','yuan','北京工业地雷出版社',62,20170712),
('九阴白骨爪','jin','人民音乐不好听出版社',40,20190807),
('独孤九剑','alex','北京工业地雷出版社',12,20170901),
('降龙十巴掌','egon','知识产权没有用出版社',20,20190705),
('葵花宝典','yuan','知识产权没有用出版社',33,20190802);

#1.查询egon写的所有书和价格
select name,price from book where author = 'egon';

#2.找出最贵的图书的价格
select max(price) from book;

#3.求所有图书的均价
select avg(price) from book;

#4.将所有图书按照出版日期排序
select * from book order by publish_date;

#5.查询alex写的所有书的平均价格
select avg(price) from book group by author having author = 'alex';

#6.查询人民音乐不好听出版社出版的所有图书
select * from book where press = '人民音乐不好听出版社';

#7.查询人民音乐出版社出版的alex写的所有图书和价格
select name,price from book where author = 'alex' and press = '人民音乐不好听出版社';

#8.找出出版图书均价最高的作者
select temp.author, max(temp.avg_price) from (select author,avg(price) as 'avg_price' from book group by author order by avg_price desc) as temp;
select author,avg(price) from book group by author order by avg(price) desc limit 0,1;

#9.找出最新出版的图书的作者和出版社
select author,press from book order by publish_date desc limit 1;

#10.显示各出版社出版的所有图书
select press,group_concat(name) from book group by press;

#11.查找价格最高的图书，并将它的价格修改为50元
update book set price = 50 where price = (select * from(select max(price) from book) as b);

#12.删除价格最低的那本书对应的数据
delete from book where price = (select * from (select min(price) from book) as b);

#13.将所有alex写的书作业修改成alexsb
update book set author = 'alexsb' where author = 'alex';

#14.将所有2017年出版的图书从数据库中删除
delete from book where publish_date in (select * from(select publish_date from book having year(publish_date) = 2017) as b);

#15.15.有文件如下，请使用pymysql写代码讲文件中的数据写入数据库
#学python从开始到放弃|alex|人民大学出版社|50|2018-7-1
#学mysql从开始到放弃|egon|机械工业出版社|60|2018-6-3
#学html从开始到放弃|alex|机械工业出版社|20|2018-4-1
#学css从开始到放弃|wusir|机械工业出版社|120|2018-5-2
#学js从开始到放弃|wusir|机械工业出版社|100|2018-7-30

import pymysql

conn = pymysql.connect(host='127.0.0.1', user='root', password="123",
                 database='day40')

cur = conn.cursor()
with open('data.txt',mode='r',encoding='utf-8') as f:
    for line in f:
        lst = line.strip().split('|')
        cur.execute('insert into book (name,author,press,price,publish_date) values (%s,%s,%s,%s,%s)',lst)

conn.commit()
conn.close()
'''



#查询每个部门最新入职的那位员工
'''
select emp_name,post from employee where hire_date in (select max(hire_date) from employee group by post);
'''