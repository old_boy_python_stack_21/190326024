# Day40

## 今日内容

1. 单表查询
2. pymysql模块
3. 多表查询

## 内容详情

### 1.单表查询

#### 1.1 where 

##### 1.1.1 比较运算符

```sql
-- > < = >= <= != <>
select * from 表名 where 字段 > 值;
```

##### 1.1.2 范围筛选

1. 多选一   in

   ```sql
   select * from 表名 where 字段名 in (值1,值2,值3,...);
   ```

2. 范围筛选 between   and

   - 表示一个范围

   ```sql
   select * from 表名 where 字段名 between 值1 and 值2;
   ```

3. 模糊查询 like

   - 通配符 %，匹配任意长度的任意字符

     ```sql
     select * from 表名 where 字段名 like 'a%';--匹配所有以a开头的行
     ```

   - 通配符 _，匹配一个字符长度的任意字符

     ```sql
     select * from 表名 where 字段名 like 'a_';--匹配所有以a开头，且长度为2的行
     ```

4. 正则匹配

   - 更加细粒度的匹配的时候

     ```sql
     select * from 表名 where 字段名 regexp '正则表达式';
     -- 查找所有以j开头，长度为5 后四位为小写字母的用户名
     # select * from employee where emp_name regexp '^j[a-z]{5}'
     ```

##### 1.1.3 逻辑运算

- and，or，not

  ```sql
  -- and 筛选满足所有条件的项
  select * from 表名 where 字段名1 = 值1 and 字段名2 = 值2;
  -- or 筛选满足任意一个条件的项
  select * from 表名 where 字段名1 = 值1 or 字段名2 = 值2;
  -- not 不满足条件的项
  select * from 表名 where 字段名 not in (值1,值2,...);
  ```

##### 1.1.4 身份运算符

- is， null永远不等于null

  ```sql
  select * from 表名 where 字段名 is null;
  ```

#### 1.2 group by 分组

```sql
--会把group by后面的这个字段，也就是post字段中的每一个不同的项都保留下来，有为post列去重的效果。并且把值是这一项的所有行归为一组。
select * from employee group by post;
```

- 聚合：把多行的同一个字段进行一些统计，最终得到一个结果。
  - count(字段)   统计这个字段有多少项。
  - sum(字段)   统计这个字段对应的数值的和
  - avg(字段)   统计这个字段对应的数值的平均值
  - min(字段)  统计这个字段对应数值的最小值
  - max(字段)  统计这个字段对应数值的最大值
  - group_concat(字段)  分组聚合

- 分组聚合

  - 求各个部门的人数

    ```sql
    select post,count(*) from employee group by post;
    ```

  - 求公司里 男生 和女生的人数

    ```sql
    select sex,count(*) from employee group by sex;
    ```

  - 求各部门的平均薪资

    ```sql
    select post,avg(salary) from employee group by post;
    ```

  - 求各部门的平均年龄

    ```sql
    select post,avg(age) from employee group by post;
    ```

  - 求各部门年龄最小的

    ```sql
    select post,min(age) from employee group by post;
    ```

  - 求各部门年龄最大的

    ```sql
    select post,max(age) from employee group by post;
    ```

  - 求最晚入职的

    ```sql
    select max(hire_date) from employee;
    ```

  - 求各部门最晚入职的

    ```sql
    select post,max(hire_date) from employee group by post;
    ```

  - 求各部门的所有人员

    ```sql
    select post,group_concat(emp_name) from employee group by post;
    ```

#### 1.3 having

- 用于过滤，后面可以跟聚合函数（where不可以），一般和group by搭配使用。

  - 求部门人数大于3的部门

    ``` sql
    select post,count(*) from employee group by post having count(*) > 3;
    ```

#### 1.4 order by

- 根据某些字段排序

  ```sql
  select * from 表名 order by 字段1;--根据字段1排序，默认升序
  select * from 表名 order by 字段1 desc;--降序排序
  select * from 表名 order by 字段1,字段2 desc;--先根据字段1排序，得到结果再根据字段2排序
  ```

#### 1.5 limit

- limit n 取前n个 ，等价于 limit 0,n

  ```sql
  select * from 表名 limit 5;--取查询结果的前五条数据
  ```

- 分页 limit m,n 从m+1开始取n条数据,等价于limit n offset m

  ```sql
  select * from 表名 limit 5,5;--从第六条开始取5条数据
  ```

#### 1.6 sql语句执行顺序

```sql
select 字段1,字段2,... from 表名
where 条件
group by 字段1
having 条件
order by 字段2
limit n
-- 1.from 表名
-- 2.where 条件
-- 3.group by 字段1
-- 4.having 条件
-- 5.select 字段1,字段2
-- 6.order by 字段2
-- 7.limit n
```

### 2.pymysql模块

```python
import pymysql
#连接数据库
conn = pymysql.connect(host='127.0.0.1',user='root',password='123',database='day40')
cur = conn,cursor() #数据库操作符 游标。可以加参数cursor = pymysql.cursors.DictCursor 查询到的结果会按字典形式返回

# 添加数据
cur.execute('insert into employee(emp_name,sex,age,hire_date) '
             'values ("郭凯丰","male",40,20190808)')
cur.execute('delete from employee where id = 18')
conn.commit()#提交，将内存中的修改写入数据库文件
conn.close() #关闭连接

#查询数据
cur.execute('select * from employee '
            'where id > 10')
ret = cur.fetchone() #取结果集中的一条数据
print(ret['emp_name']) 
ret = cur.fetchmany(5) #取结果集中的5条数据，前面取过一条，再取只能接着取
ret = cur.fetchall() #取出所有结果
print(ret)
conn.close()
```

### 3.多表查询

#### 3.1 连表查询

##### 3.1.1 内连接 inner join

- 两张表条件不匹配的项不会出现再结果中

  ```sql
  select * from emp inner join department as dep on emp.dep_id = dep.id;
  ```

##### 3.1.2 外连接

- 左外连接 left join：永远显示全量的左表中的数据

  ```sql
  select * from emp left join department as dep on emp.dep_id = dep.id;
  ```

- 右外连接 right join：永远显示全量的右表中的数据

  ```sql
  select * from emp right join department as dep on emp.dep_id = dep.id;
  ```

- 全外连接 left join union right join

  ```sql
  select * from emp right join department as dep on emp.dep_id = dep.id 
  union 
  select * from emp left join department as dep on emp.dep_id = dep.id;
  ```

- 常用 inner join 和 left join

- 连接语法

  ```sql
  select 字段 from 表1 xxx join 表2 on 表1.字段 = 表2.字段;
  ```

#### 3.2 子查询

```sql
select * from emp where dep_id = (select id from department where name = '技术'); 
```

- sql查询优化，既可以用连表查询实现又可以用子查询实现的查询，尽量使用连表查询，连表查询效率高。

#### 3.3 练习

1. 查询平均年龄在25岁以上的部门名

   ```sql
   -- 连表查询
   select dep.name,avg(age) from emp inner join department as dep on emp.dep_id = dep.id group by dep.name having avg(age) > 25;
   --子查询
   select name from department where id in (select dep_id from emp group by dep_id having avg(age) > 25);
   ```

2. 查询大于部门内平均年龄的员工名、年龄

	```sql
	-- 连表查询+子查询
	select emp.name,age from emp inner join (select dep_id,avg(age) as avg_age from emp group by dep_id) as dep on emp.dep_id = dep.dep_id where emp.age > dep.avg_age;
	```

