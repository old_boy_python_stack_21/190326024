# Day58

## 今日内容

标签{%  %}

1. for循环和if判断
2. csrf_token
3. 母版页
4. 组件
5. 静态文件
6. simpletag
7. inclusion_tags

## 内容详细

### 1.模板标签{% %}

#### 1.1 for循环和if判断

1. for循环

   - forloop对象
     - forloop.counter 当前循环的索引,从1开始
     - forloop.counter0  当前循环的索引，从0开始
     - forloop.revcounter 当前循环的倒叙索引值，从1开始
     - forloop.revcounter0 当前循环的倒叙索引值，从0开始
     - forloop.parentloop 父循环的forloop对象

   - for  empty   当循环为空时，执行empty

     ```html
     {% for i in book_list%}
     	{{i}}
     {% empty%}
     	没有相关数据
     {% endfor%}
     ```

2. if判断

   - 模板中不支持连续判断

     ```python
     #python中：5>4>1 等价于 5>4 and 4>1   结果：True
     #js中： 5>4>1 等价于 5>4 true  等价于 1>1  结果：false
     ```

   - 模板中不支持算数运算（可以使用过滤器代替）

     ```python
     {% if num|add:2 > 4 %}
     	xxxxxxx
     {% endif %}
     
     {% if num|divisibleby:2 %}
     	可以被2整除               
     {% else %}
         不能被2整除            
     {% endif %}
     ```

#### 1.2 csrf_token

csrf 跨站请求伪造。加上{% csrf_token %}后，django会在页面自动加上一个隐藏的input,然后提交post请求时会通过该input的value值来判断是不是本站的post请求。

```html
<!--模板代码代码-->
<form action="">
    {% csrf_token %}
    <label for="">用户名</label>
    <input type="text" name="name" id="">
    <input type="submit" name="name" id="">
</form>

<!--浏览器html代码-->
<form action="">
    <input type="hidden" name="csrfmiddlewaretoken" value="urZC99wxQ26eF3u9hwIyRLbZYZqH6ukLqeWSvk8ixIiKuo6YCDw1wgRJnJDq0dCQ">
    <label for="">用户名</label>
    <input type="text" name="name" id="">
    <input type="submit" name="name" id="">
</form>
```

#### 1.3 母版页

将重复用到的内容写成一个母版页，独有的内容定义成block块。子页面在编写时只需要继承母版页，然后将自己的内容包裹在对应的block块中即可。

母版页 base.html：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="/static/css/dsb.css">
    {% block css %}

    {% endblock %}
</head>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="{% block pub_active %}active{% endblock %}"><a 									href="/publisher_list/">出版社列表 <span
                        class="sr-only">(current)</span></a></li>
                <li class="{% block book_active %}{% endblock %}"><a href="/book_list/">						书籍列表 <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            {% block content %}

            {% endblock %}
        </div>
    </div>
</div>
</body>
{% block js %}

{% endblock %}
</html>
```

子版页

```html
{% extends 'base.html' %} <!-- 继承母版页 -->
{% block content %}  <!-- 会替代母版页中对应的块 -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">书籍列表</h3>
        </div>
        <div class="panel-body">
            <a class="btn btn-success btn-sm" href="/add_book/">添加</a>
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>序号</th>
                    <th>ID</th>
                    <th>书名</th>
                    <th>出版社名称</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                {% for book in all_books %}
                    <tr>
                        <td>{{ forloop.counter }}</td>
                        <td>{{ book.pk }}</td>
                        <td>{{ book.title }}</td>
                        <td>{{ book.pub.name }}</td>
                        <td>
                            <a href="/del_book/?id={{ book.pk }}">删除</a>
                            <a href="/edit_book/?id={{ book.pk }}">编辑</a>
                        </td>
                    </tr>
                {% empty %}
                    <td colspan="5" style="text-align: center">没有相关的数据</td>
                {% endfor %}
                </tbody>
            </table>
        </div>
    </div>
{% load my_tags %}
```

**注意：**

- {% extends 'base.html' %} 要写在第一行前面不能有内容，有内容会显示
- {% extends 'base.html' %}  的base.html要加上引号，不然会当做变量来处理。利用这一点可以根据从views返回的变量值，动态改变母版页
- 把要显示的内容写在block块中
- 可以定义js和css的块，使不同页面只引入自己的js和css

#### 1.4 组件

把常用的，不改变的部分html代码片段，单独封装成一个组件（一个html文件），如导航栏。

使用方法：

```python
...
{% include ‘nav.html ’  %}
...
```

#### 1.5 静态文件

当settings.py文件中的STATIC_URL改变后，原来的静态文件写法就需要把所有的地方都改一遍。通过下列两种方法，STATIC_URL改变后，引用也会动态改变。

```html
{% load static %}

 <link rel="stylesheet" href="{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}">
 <link rel="stylesheet" href="{% static '/css/dsb.css' %}">
 <link rel="stylesheet" href="{% get_static_prefix %}css/dsb.css">

{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}  
{% get_static_prefix %}   ——》 获取别名
```

#### 1.6 simpletag

类似于过滤器，但simpletag是标签，可以传任意个参数。

my_tags.py :

```python
@register.simple_tag
def join_str(*args, **kwargs):
    return '{} - {} '.format('*'.join(args), '$'.join(kwargs.values()))
```

使用：

```html
{% load my_tags %}
{% join_str '1' '2' k1='3' k2='4' %}
```

#### 1.7 inclusion_tag

有点类似于组件，也是返回一段html代码段，但是inclusion_tag可以动态的改变代码段。

流程如下：

![](E:\Python\11Django\day58\inclusion_tag.png)



