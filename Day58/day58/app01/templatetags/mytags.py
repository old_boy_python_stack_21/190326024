#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django import template

register = template.Library()

@register.inclusion_tag('dropdown_list.html')
def sqr_list(value):
    return {'value_list':range(1,value+1)}

@register.filter
def sqr(value):
    try:
        return int(value) * int(value)
    except (ValueError, TypeError):
        try:
            return value * value
        except Exception:
            return ''