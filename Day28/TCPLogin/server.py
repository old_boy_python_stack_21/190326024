#!/usr/bin/env python
# -*- coding:utf-8 -*-
import json
import socket

DIC = {'wkx':'4297f44b13955235245b2497399d7a93'}

sk = socket.socket()
sk.bind(('192.168.12.39',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        recv_msg = json.loads(conn.recv(1024).decode('utf-8'))
        if recv_msg[0].upper() == 'Q':
            conn.close()
            break
        if DIC.get(recv_msg[0]) == recv_msg[1]:
            conn.send('t'.encode('utf-8'))
            conn.close()
            break
        else:
            conn.send('f'.encode('utf-8'))
sk.close()