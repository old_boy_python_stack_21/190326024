#!/usr/bin/env python
# -*- coding:utf-8 -*-
import json
import socket
import hashlib

def encrypt(pwd):
    obj = hashlib.md5('123'.encode('utf-8'))
    obj.update(pwd.encode('utf-8'))
    return obj.hexdigest()

sk = socket.socket()
sk.connect(('192.168.12.39',8066))
while True:
    user_name = input('请输入用户名(Q/q退出)：')
    if user_name.upper() == 'Q':
        temp = [user_name,'']
        sk.send(json.dumps(temp).encode('utf-8'))
        break
    user_pwd = input('请输入密码：')
    user_pwd = encrypt(user_pwd)
    temp = [user_name,user_pwd]
    sk.send(json.dumps(temp).encode('utf-8'))
    recv_msg = sk.recv(1024)
    if recv_msg.decode().upper() == 'T':
        print('登录成功')
        break
    print('用户名或密码错误，请重新输入')
sk.close()