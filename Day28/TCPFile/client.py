#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
import json
import socket

def read_file(path):
    total_size = os.path.getsize(path)
    read_size = 0
    with open(path,mode='rb') as f:
        while read_size < total_size:
            con = f.read(1024*1024)
            read_size += len(con)
            yield con

sk = socket.socket()
sk.connect(('192.168.12.39',8066))
path = r'E:\Python\2019-05-07\day28\2.今日内容.mp4'
f = read_file(path)
sk.send(str(os.path.getsize(path)).encode('utf-8'))
sk.recv(1024)
for i in f:
    sk.send(i)
    sk.recv(1024)
print(sk.recv(1024).decode('utf-8'))
sk.close()