#!/usr/bin/env python
# -*- coding:utf-8 -*-
import json
import socket

sk = socket.socket()
sk.bind(('192.168.12.39',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    total_size = int(conn.recv(1024).decode('utf-8'))
    conn.send('ok'.encode('utf-8'))
    write_size = 0
    f = open('123.mp4',mode='wb')
    while True:
        recv_msg = conn.recv(1024*1024)
        conn.send('ok'.encode('utf-8'))
        f.write(recv_msg)
        f.flush()
        write_size += len(recv_msg)
        if write_size >= total_size:
            f.close()
            conn.send('上传完成'.encode('utf-8'))
            conn.close()
            break

sk.close()