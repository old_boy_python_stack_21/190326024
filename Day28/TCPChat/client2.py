#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket

CODE = '2995759557'

sk = socket.socket()
sk.connect(('127.0.0.1',8066))
sk.send(CODE.encode('utf-8'))
sk.recv(1024)
while True:
    inp = input('>>>')
    sk.send(inp.encode('utf-8'))
    if inp.upper() == 'Q':
        break
    msg = sk.recv(1024).decode('utf-8')
    if msg.upper() == 'Q':
        break
    print('\033[1;32m%s\033[0m'%msg)

sk.close()