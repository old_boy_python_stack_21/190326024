#!/usr/bin/env python
# -*- coding:utf-8 -*-
import socket

CLIENT_DICT = {
    '819717343':{'fmt':'\033[1;36m','name':'小王'},
    '2995759557':{'fmt':'\033[1;31m','name':'小李'}
               }

sk = socket.socket()
sk.bind(('127.0.0.1',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    code = conn.recv(1024).decode('utf-8')
    conn.send('yes'.encode('utf-8'))
    info = CLIENT_DICT.get(code)
    fmt = info.get('fmt')
    name = info.get('name')
    while True:
        msg = conn.recv(1024).decode('utf-8')
        if msg.upper() == 'Q':
            conn.close()
            break
        print('%s:%s%s\033[0m'%(name,fmt,msg,))
        inp = input('>>>')
        conn.send(inp.encode('utf-8'))
        if inp.upper() == 'Q':
            conn.close()
            break


sk.close()