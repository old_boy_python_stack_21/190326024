# Day28

## 今日内容

1. TCP协议
2. UDP协议
3. osi七层模型
4. socket实现TCP
5. socket实现UDP

## 内容详细

### 1.TCP协议

tcp协议是一种全双工通信，是基于连接的协议。

#### 1.1 建立和断开连接

建立连接：三次握手，只能由客户端发起连接请求

断开连接：四次挥手，双方都可以发起断开请求。因为客户端或服务端主动发起断开请求后，对方同意后但对方可能还有信息要发送，所以断开连接请求中间的通过和请求断开连接不能合并。

![](E:\Python\2019-05-07\day28\TCP.png)

#### 1.2 tcp特点

- 传输可靠
- 传输的数据长度几乎没有限制
- 传输效率低
- 长连接：建立连接后会一直占用双方的端口

在建立连接后：

- 发送的每一条信息都有回执
- 为了保证数据的完整性，还有重传机制

IO操作(in/out)：输入和输出是相对与内存来说的。

- write/send 输出
- read/recv 输入

#### 1.3 应用场景

文件的上传下载（发邮件、网盘、缓存电影）

### 2.UDP协议

#### 2.1 udp特点

- 无连接速度快
- 传输不可靠，可能会丢消息
- 能够传递的长度有限，和数据传递设备的设置有关（一般1500byte）

#### 2.2 应用场景

即时通讯类（QQ、微信、飞秋的聊天系统）

### 3.osi七层模型

应用层、表示层、会话层、传输层、网络层、数据链路层、物理层。

有时把应用层、表示层、会话层统一称为应用层。所以有了以下osi五层模型

| 层号 |    名称    |              协议               |        物理设备        |
| :--: | :--------: | :-----------------------------: | :--------------------: |
|  5   |   应用层   | http,https,ftp,smtp(python代码) |                        |
|  4   |   传输层   |          tcp,udp,端口           | 四层路由器、四层交换机 |
|  3   |   网络层   |            ipv4,ipv6            |   三层交换机、路由器   |
|  2   | 数据链路层 |        mac地址，arp协议         |    网卡、二层交换机    |
|  1   |   物理层   |             二进制              |                        |

### 4.socket实现TCP、UDP

什么是socket?

socket(套接字)是工作在应用层和传输层之间的抽象层，帮助我们完成所有信息的组织和拼接。socket对程序员来说已经是网络操作的底层了。

#### 4.1 socket实现TCP

```python
#服务端
import socket

sk = socket.socket()#默认tcp协议（type=socket.SOCK_STREAM）
sk.bind(('192.168.12.39',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        recv_msg = conn.recv(1024)
        if recv_msg.decode().upper() == 'Q':
            conn.close()
            break
        print(recv_msg.decode('utf-8'))
        send_msg = input('请输入：')
        conn.send(send_msg.encode('utf-8'))
        if send_msg.upper() == 'Q':
            conn.close()
            break

sk.close()
```

```python
#客户端
import socket

sk = socket.socket()#默认tcp协议（type=socket.SOCK_STREAM）
sk.connect(('192.168.12.39',8066))
while True:
    send_msg = input('请输入：')
    sk.send(send_msg.encode('utf-8'))
    if send_msg.upper() == 'Q':
        break
    recv_msg = sk.recv(1024)
    if recv_msg.decode().upper() == 'Q':
        break
    print(recv_msg.decode('utf-8'))
sk.close()
```

#### 4.2 socket实现UDP

```python
#服务端
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)#udp协议
sk.bind(('127.0.0.1',8066))

while True:
    msg,addr = sk.recvfrom(1024)
    print(msg.decode('utf-8'))
    send_msg = input('>>>').encode('utf-8')
    sk.sendto(send_msg,addr)

sk.close()
```

```python
#客户端
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)#udp协议
while True:
    send_msg = input('>>>')
    if send_msg.upper() == 'Q':
        sk.close()
        break
    sk.sendto(send_msg.encode('utf-8'),('127.0.0.1',8066))
    recv_msg = sk.recv(1024)
    print(recv_msg.decode('utf-8'))
```

