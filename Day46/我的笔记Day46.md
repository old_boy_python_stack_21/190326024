# Day46

## 今日内容

1. 固定定位
2. z-index
3. background背景图
4. border-radius
5. 阴影
6. 水平和垂直

## 内容详细

### 1.固定定位

特征：

- 脱标
- 位置固定不变
- 提高层级，压盖

参考点：以浏览器的左上角为参考点。

```html
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 500px;
            height: 2000px;
            border: 1px solid #000;
        }

        .fix{
            width: 50px;
            height: 200px;
            background-color: yellow;
            position: fixed;
            top:260px;
            right: 0;
        }
    </style>
</head>
<body>
    <div class="box"></div>
    <div class="fix"></div>
</body>
</html>
```

### 2.z-index

只是用于定位的元素（相对定位，绝对定位，固定定位），z-index越大表示显示层级越高。

### 3.background背景图

- background-image  设置图片背景

  ```css
  div{
      width: 200px;
      height: 200px;
      border: 1px solid #000;
      background-image: url("图片地址");
  }
  ```

- background-repeat  设置背景图平铺样式
  - repeat  默认，平铺。
  - repeat-x  横向平铺。
  - repeat-y  纵向平铺。
  - no-repeat 不平铺。

- background-position  设置背景图位置

  - 第一个值为横向坐标，第二个为纵向坐标，默认为0,0。
  - 横向还有三个写好的参数：left(靠左)，center(居中)，right(靠右)。
  - 纵向三个参数：top(靠上)，center(居中)，bottom(靠下)。

- background   综合属性

  ```css
  .box {
      width: 200px;
      height: 200px;
      border: 1px solid #000;
      background: url("图片地址") no-repeat center top;
  }
  ```

### 4.border-radius  设置边框圆角

```css
/*值越大，圆角越大，50%为整个圆*/
.box {
    width: 200px;
    height: 200px;
    background-color: red;
    border-radius: 50%;
}
```

### 5.box-shadow 盒子阴影

box-shadow属性值：

- 第一个参数 水平距离
- 第二个，垂直距离
- 第三个，模糊距离
- 第四个，模糊颜色
- 第五个，阴影样式（默认为外阴影），inset设置为内阴影。

```css
.box {
    width: 200px;
    height: 200px;
    margin: 0 auto;
    background-color: red;
    box-shadow: 2px 3px 10px yellow;
}
```

### 6.水平和垂直

#### 6.1 行内元素水平垂直居中

1. 设置text-align属性为center，行高等于容器高度。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 200px;
            height: 200px;
            border: 1px solid #000;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
    </style>
</head>
    <body>
        <div class="box">
           <span>mjj</span>
        </div>
    </body>
</html>
```

2. 设置元素display属性为table-cell，vertical-align属性为middle，text-align属性为center。(了解)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 200px;
            height: 200px;
            border: 1px solid #000;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="box">
       <span>mjj</span>
    </div>
</body>
</html>
```

#### 6.2 块级元素水平垂直居中

1. 方式一

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <style>
           .box{
               position: relative;
               width: 300px;
               height: 300px;
               border: 1px solid #000;
               margin: auto 0;
           }
           .box div{
               position: absolute;
               width: 100px;
               height: 100px;
               background-color: green;
               left: 0;
               right: 0;
               top:0;
               bottom: 0;
               margin: auto;
           }
       </style>
   </head>
   <body>
       <div class="box">
          <div></div>
       </div>
   </body>
   </html>
   ```

   2.方式2

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <style>
           .father{
               width: 500px;
               height: 300px;
               background-color: red;
               position: relative;
           }
           .child{
               /*如何让一个绝对定位的垂直居中： top设置50%，margin-top设置当前盒子的一半，并且是负*/
               position: absolute;
               width: 100px;
               height: 140px;
               background-color: green;
               /*父元素的50%*/
               left: 50%;
               margin-left: -50px;
               top: 50%;
               margin-top: -70px;
   
           }
       </style>
   </head>
   <body>
       <div class="father">
           <div class="child">我是个居中的盒子</div>
       </div>
   </body>
   </html>
   ```

   

### 7.其他

- curcor属性设置为pointer，可以使鼠标悬浮到元素上时变成小手。