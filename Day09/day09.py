#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
第九天作业
王凯旋
'''
# 2.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回。
'''
def get_new_list(lst):
    return lst[1::2]
'''

# 3.写函数，判断用户传入的一个对象（字符串或列表或元组任意）长度是否大于5，并返回真假。
'''
def get_len(obj):
    return len(obj) > 5
'''

# 4.写函数，接收两个数字参数，返回比较大的那个数字。
'''
def get_bigger(a,b):
    return a if a > b else b
'''

# 5.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，
# 然后将这四个内容传入到函数中，此函数接收到这四个内容，将内容根据"*"拼接起来并追加到一个student_msg文件中。
'''


def get_message(a, b, c, d):
    return a + "*" + b + "*" + c + "*" + d12


def write_to_file(message):
    with open("student_msg.txt", mode="w", encoding="utf-8") as f:
        f.write(message)


name = input("请输入姓名：")
gender = input("请输入性别：")
age = input("请输入年龄：")
edu = input("请输入学历：")
write_to_file(get_message(name, gender, age, edu))
'''

# 6.写函数，在函数内部生成如下规则的列表 [1,1,2,3,5,8,13,21,34,55…]（斐波那契数列），并返回。
# 注意：函数可接收一个参数用于指定列表中元素最大不可以超过的范围。
'''


def get_list(max_num):
    if max_num < 1:
        return []
    else:
        lst = [1, 1]
    index = 2
    while True:
        num = lst[index - 1] + lst[index - 2]
        if num > max_num:
            break
        else:
            lst.append(num)
        index += 1
    return lst
'''


# 7.写函数，验证用户名在文件 data.txt 中是否存在，如果存在则返回True，否则返回False。
# （函数有一个参数，用于接收用户输入的用户名）
'''


def name_exist(arg_name):
    with open("data.txt", mode="r", encoding="utf-8") as f:
        for line in f:
            if arg_name in line:
                return True
        else:
            return False


name = input("请输入用户名：")
print(name_exist(name))

'''
