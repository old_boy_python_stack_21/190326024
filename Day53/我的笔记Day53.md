# Day53

## 今日内容

1. 静态文件的配置
2. 简单的登录实例
3. app
4. ROM

## 内容详细

### 1.静态文件的配置

```python
STATIC_URL = '/static/' #别名
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'), #配置静态文件夹的名称
    os.path.join(BASE_DIR, 'static1'),
    os.path.join(BASE_DIR, 'static2'),
]
```

使用

```python
<link rel="stylesheet" href="/static/css/login.css">   # 别名开头,django会在配置的文件夹内寻找资源，找到后不再向后
```

### 2.简单的登录实现

form表单提交数据注意的问题：

- 提交的地址action="",请求方式method="post"
- 所有input框的type有name属性
- 有一个input框的type="submit"或者有一个button按钮
- 提交POST请求时，把settings.py里的MIDDLEWAER配置中的"django.middleware.csrf.CsrfViewMiddleware"注释掉。

```python
from django.conf.urls import url
from django.contrib import admin
from django.shortcuts import render,HttpResponse,redirect

def index(request):
    #return HttpResponse(b'hello world')
    return render(request,'index.html')

def login(request):
    if request.method == 'POST':
        username = request.POST['username'] #获取浏览器发来的数据
        password = request.POST['password'] #获取浏览器发来的数据
        if username == '123' and password == '123':
            # return redirect('https://www.baidu.com') #重定向，返回给浏览器，浏览器再向该地址发送一次请求
            return redirect('/index/') #本地资源前面加/  表示根目录
    return render(request,'login.html')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', index),
    url(r'^login/', login),
]
```

### 3.app

每次都把页面对应的函数写在urls.py文件中，会很冗余。所以创建app来存放一类功能类似的页面函数。使项目结构更清晰合理。

#### 3.1 创建app

1. 命令行创建

   ```python
   python36 manage.py startapp app名称
   ```

2. pycharm创建

   tools-->run manage.py task

   ```python
   startapp app名称
   ```

#### 3.2 注册app

在settings文件中

```python
INSTALLED_APPS = [
    ...
    'app01',
    'app01.apps.App01Config', #推荐写法
]
```

#### 3.3 app目录结构

```python
app
 -- migrations  #ORM数据库迁移记录文件夹
 -- __init__.py  
 -- admin.py  #Django后台管理工具
 -- apps.py   #app相关
 -- models.py #ORM相关
 -- tests.py  
 -- views.py  #写函数的文件
```

### 4.ORM

#### 4.1 简介

- 对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术。
- 简单的说，ORM是通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。
- ORM在业务逻辑层和数据库层之间充当了桥梁的作用。

#### 4.2 ORM优缺点

优点：

- ORM解决的主要问题是对象和关系的映射。它通常将一个类和一张表一一对应，类的每个实例对应表中的一条记录，类的每个属性对应表中的每个字段。 
- ORM提供了对数据库的映射，不用直接编写SQL代码，只需操作对象就能对数据库操作数据。
- 让软件开发人员专注于业务逻辑的处理，提高了开发效率。

缺点：

- ORM的缺点是会在一定程度上牺牲程序的执行效率。
- ORM定义好的操作是可以完成的，一些复杂的查询操作是完成不了。
- 关系数据库相关技能退化...

#### 4.3 Django中使用Mysql数据库的流程

1. 创建一个MySQL数据库。

2. 在settings中配置，django连接Mysql数据库：

   ```python
   DATABASES = {
       'default':{
          'ENGINE':'django.db.backends.mysql', #引擎
           'NAME' : 'day53', #数据库名称
           'HOST' : '127.0.0.1', #ip地址
           'PORT' : 3306, #端口号
           'USER' : 'root', #用户名
           'PASSWORD' : '123' #密码
       }
   }
   ```

3. 在与settings同级目录下的init文件中写：

   ```python
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 创建表（在app下的models.py中写类）

   - 类----表
   - 对象---一行数据
   - 属性----字段值

   ```python
   class User(models.Model):
       username = models.CharField(max_length=32) #varchar(32)
       password = models.CharField(max_length=32)
   ```

5. 执行数据库迁移的命令

   ```python
   python manage.py makemigrations #检测每个注册的app下的model.py 记录model的变更记录
   python manage.py migrate #同步变更记录到数据中
   ```

4.4 ORM操作

```python
# 获取表中所有的数据
ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
# 获取一个对象（有且唯一）
obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
# 获取满足条件的对象
ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
```

