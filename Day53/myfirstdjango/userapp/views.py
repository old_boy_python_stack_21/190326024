from django.shortcuts import render,HttpResponse,redirect
from userapp import models

def index(request):
    #return HttpResponse(b'hello world')
    return render(request,'index.html')

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if models.User.objects.filter(username=username,password=password):
            # return redirect('https://www.baidu.com') #重定向，返回给浏览器，浏览器再向该地址发送一次请求
            return redirect('/index/') #本地资源前面加/  表示根目录
    return render(request, 'login.html')

# def login1(request):
#     return render(request,'login.html')