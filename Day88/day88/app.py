#!/usr/bin/env python
# -*- coding:utf-8 -*-
from flask import Flask, render_template,redirect,request,session
import functools

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

# 要求:
# 1.登录页面
# 2.学生概况页面 ID name 点击详情
# 3.学生详情页面 ID name age gender

app = Flask(__name__)
app.debug = True

def wapper(func):
    @functools.wraps(func)
    def inner(*args,**kwargs):
        if not session.get('user'):
            return redirect('/login')
        ret = func(*args,**kwargs)
        return ret
    return inner


@app.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username == 'alex' and password == '123':
            session['user'] = username
            return redirect('/student_list')
    return render_template('login.html')


@app.route('/student_list')
@wapper
def student_list():
    return render_template('student_list.html',student_list=STUDENT_DICT)

@app.route('/student')
@wapper
def student():
    id = request.args.get('id')
    return render_template('student.html',student=STUDENT_DICT[int(id)],id=id)

app.run()
