# Day88

## 今日内容

1. 框架对比
2. 入门flask
3. flask的Response
4. flask的Request
5. Jinja2
6. session

## 内容详细

### 1.框架对比

| Django  | Flask           |
| ------- | --------------- |
| admin   | 原生无          |
| Model   | 原生无          |
| Form    | 原生无          |
| Session | 有-保存在客户端 |

- django的有缺点
  - 优点：组件全，功能全，教科书式框架
  - 缺点：占用资源，创建复杂度高

- Flask的优缺点
  - 优点：轻量级，块
  - 缺点：第三方组件稳定性差

### 2.入门Flask

#### 2.1 下载安装Flask

pip3 install Flask

#### 2.2 flask依赖组件

- Jinja2:模板语言，用于模板的渲染
- MarkupSafe用于处理后端返回给前台的字符串，防xss攻击
- Werkzeug:用来承载flask服务的

#### 2.3 创建启动一个flask项目

```python
from flask import Flask
app = Flask(__name__)
app.run()
```

### 3.Flask中的Response

#### 3.1 返回字符串

```python
@app.route('/')
def home():
    return 'hello world'
```

#### 3.2 render_template 返回模板

```python
from flask import Flask, render_template

@app.route('/')
def home():
    return render_template('home.html')
```

#### 3.3 redirect 返回重定向

```python
from flask import Flask,redirect
@app.route('/login')
def login():
    return redirect('/')
```

#### 3.4 send_file 

返回文件内容，自动识别文件类型，Content-type中添加文件类型，Content-type:文件类型

浏览器：识别Content-type,自动渲染；不识别Content-type,自动下载

```python
from flask import Flask,send_file

@app.route('/file')
def file():
    return send_file('img.bmp')
```

#### 3.5 jsonify

返回标准格式的json字符串

**注：flask 1.1.1版本中可以直接返回字典格式，无需jsonify**

```python
from flask import Flask,jsonify

@app.route('/to_json')
def to_json():
    dic = {'name':'alex','age':19}
    # return dic
    return jsonify(dic)
```

### 4.Flask中的Request

request在flask中是全局变量，需要导入模块

```python
from flask import request
```

#### 4.1 request的属性

- request.method  获取请求方式
- request.path   请求的路径  /login
- request.url   请求的完整url，包括参数
- request.host   主机地址 127.0.0.1:5000
- request.cookies  获取浏览器的cookies

- request.form 获取FormData中的数据，也就是form表单中的标签数据。也可以获取ajax发送的数据。

  ```python
  username = request.form.get('username')
  request.form.to_dict() #转换成字典
  ```

- request.args 获取url中的参数，to_dict

  ```python
  id = request.args.get('id')
  ```

- request.json 请求头中Content-Type:appliction/json,请求体中的数据被序列化到，request.json中，以字典的形式存放。

- request.data 请求头中Content-Type中不包含Form或者FormData保留请求体中的原始数据b""

- request.files 获取上传的文件

  ```python
  @app.route('/upfile',methods=['GET','POST'])
  def upfile():
      if request.method == 'POST':
          file = request.files.get('my_file')
          name = file.filename
          file.save(name)
      return render_template('file.html')
  ```

### 5.Jinja2 模板语法

#### 5.1 {{ }} 引用或执行

```python
STUDENT = {'name': 'Old', 'age': 38, 'gender': '中'}
@app.route('/student')
def std_info():
    return render_template('student.html',stu=STUDENT)
```

```html
<table>
        <tr>
            <th>name</th>
            <th>age</th>
            <th>gender</th>
        </tr>
        <tr>
            <td>{{ stu.name }}</td>
            <td>{{ stu['age'] }}</td>
            <td>{{ stu.get('gender') }}</td>
        </tr>
    </table>
```

#### 5.2 {% %} 逻辑引用

```python
STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}
@app.route('/student')
def std_info():
    return render_template('student.html',stu=STUDENT,stu_d=STUDENT_DICT)
```

```html
<table>
    <tr>
        <th>ID</th>
        <th>name</th>
        <th>age</th>
        <th>gender</th>
    </tr>
    {% for key,value in stu_d.items() %}
        <tr>
            <td>{{ key }}</td>
            <td>{{ value.name }}</td>
            <td>{{ value['age'] }}</td>
            <td>
            {% if value.get('gender') != '男' %}
                女
            {% endif %}
            </td>
        </tr>
    {% endfor %}
</table>
```

### 6.session

session会根据secret_key加密存放到客户端。

```python
from flask import session,request
app.secret_key = "asdfasdfaw^&%&^(())"

@app.route('/login')
def login():
    session['user'] = '123'
    if request.method == 'POST':
        user = session.get('user')
    return render_templates('login.html')
    
```







- 