#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第二十一天作业
王凯旋
'''

# 1.列举你了解的面向对象中的特殊成员，并为每个写代码示例。
# __init__()初始化方法
'''
class Foo(object):
    def __init__(self,name):
        self.name = name

obj = Foo('alex')
print(obj.name)
'''

# __new__()构造方法
'''
class Foo(object):
    def __new__(cls, *args, **kwargs):
        return 123

print(Foo())
'''

# __call__()对象加括号会调用该方法
'''
class Foo(object):
    def __call__(self, *args, **kwargs):
        print('__call__')

Foo()()
'''

# __getitem__  __setitem__  __delitem__
'''
class Foo(object):
    def __getitem__(self, item):
        print(item)

    def __setitem__(self, key, value):
        print(key,value)

    def __delitem__(self, key):
        print(key)

obj = Foo()
obj[123]
obj[1] = 'alex'
del obj['k1']
'''

# __str__ 打印对象时会执行对象对应的类的__str__方法,并打印返回值
'''
class Foo(object):
    def __str__(self):
        return '123'

obj = Foo()
print(obj)
'''

# __dict__ 寻找对象所有变量并组成字典
'''
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age

obj = Foo('alex',18)
print(obj.__dict__)
'''

# 上下文管理
'''
class Foo(object):
    def __enter__(self):
        print("开始")
        return 123

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("结束")

with Foo() as f:
    print(f)
'''

# 两对象相加
'''
class Foo(object):
    def __add__(self, other):
        return '%s + %s'%(1,2)

obj1 = Foo()
obj2 = Foo()
print(obj1 + obj2)
'''

# 2.看代码写结果
'''
class Foo(object):

    def __init__(self, age):
        self.age = age

    def display(self):
        print(self.age)


data_list = [Foo(8), Foo(9)]
for item in data_list:
    print(item.age, item.display())
'''

'''
结果：8 8 None 9 9 None
'''

# 3.看代码写结果
'''
class Base(object):
    def __init__(self, a1):
        self.a1 = a1

    def f2(self, arg):
        print(self.a1, arg)


class Foo(Base):
    def f2(self, arg):
        print('666')


obj_list = [Base(1), Foo(2), Foo(3)]
for obj in obj_list:
    obj.f2(4)
'''

'''
结果:  1 4
      666
      666     
'''

# 4.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print('666')

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
for item in config_obj_list:
    print(item.num)
'''

'''
结果： 1
      2
      3
'''

# 5.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

class RoleConfig(StarkConfig):
    pass

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
for item in config_obj_list:
    item.changelist(168)
'''

'''
结果： 1 168
      2 168
      3 168
'''

# 6.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
for item in config_obj_list:
    item.changelist(168)
'''

'''
结果：1 168
     2 168
     666 3
'''

# 7.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)

config_obj_list = [StarkConfig(1),StarkConfig(2),RoleConfig(3)]
config_obj_list[1].run()
config_obj_list[2].run()
'''

'''
结果：2 999
     666 3
'''

# 8.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)


class AdminSite(object):
    def __init__(self):
        self._registry = {}

    def register(self,k,v):
        self._registry[k] = v

site = AdminSite()
print(len(site._registry))
site.register('range',666)
site.register('shilei',438)
print(len(site._registry))

site.register('lyd',StarkConfig(19))
site.register('yjl',StarkConfig(20))
site.register('fgz',RoleConfig(33))

print(len(site._registry))
print(site._registry)
'''

'''
结果：0
     2
     5
     {'range':666,'shilei':438,'lyd':StarkConfig对象,'yjl':StarkConfig对象,'fgz':RoleConfig对象}
'''

# 9.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)

class AdminSite(object):
    def __init__(self):
        self._registry = {}

    def register(self,k,v):
        self._registry[k] = v

site = AdminSite()
site.register('lyd',StarkConfig(19))
site.register('yjl',StarkConfig(20))
site.register('fgz',RoleConfig(33))
print(len(site._registry)) # 3

for k,row in site._registry.items():
    row.changelist(5)
'''

'''
结果： 3
      19 5
      20 5
      666 33
'''

# 10.看代码写结果
'''
class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)

class AdminSite(object):
    def __init__(self):
        self._registry = {}

    def register(self,k,v):
        self._registry[k] = v

site = AdminSite()
site.register('lyd',StarkConfig(19))
site.register('yjl',StarkConfig(20))
site.register('fgz',RoleConfig(33))
print(len(site._registry)) # 3

for k,row in site._registry.items():
    row.run()
'''

'''
结果：3
     19 999
     20 999
     666 33
'''

# 11.看代码写结果
'''
class UserInfo(object):
    pass

class Department(object):
    pass

class StarkConfig(object):

    def __init__(self,num):
        self.num = num

    def changelist(self,request):
        print(self.num,request)

    def run(self):
        self.changelist(999)

class RoleConfig(StarkConfig):

    def changelist(self,request):
        print(666,self.num)

class AdminSite(object):
    def __init__(self):
        self._registry = {}

    def register(self,k,v):
        self._registry[k] = v(k)

site = AdminSite()
site.register(UserInfo,StarkConfig)
site.register(Department,StarkConfig)
print(len(site._registry))
for k,row in site._registry.items():
    row.run()
'''

'''
结果：2
     UserInfo类 999
     Department类 999
'''

# 12.看代码写结果
'''
class F3(object):
    def f1(self):
        ret = super().f1()
        print(ret)
        return 123


class F2(object):
    def f1(self):
        print('123')


class F1(F3, F2):
    pass


obj = F1()
obj.f1()
'''

'''
结果：123
     None
'''

# 13.看代码写结果
'''
class Base(object):
    def __init__(self, name):
        self.name = name


class Foo(Base):
    def __init__(self, name):
        super().__init__(name)
        self.name = "于大爷"


obj1 = Foo(123)
print(obj1.name)

obj2 = Base(123)
print(obj2.name)
'''

'''
结果：于大爷
     123
'''

# 14.看代码写结果
'''
class Base(object):
    pass

class Foo(Base):
    pass


obj = Foo()

print(type(obj) == Foo)
print(type(obj) == Base)
print(isinstance(obj,Foo))
print(isinstance(obj,Base))
'''

'''
结果：True
     False
     True
     True
'''

# 15.看代码写结果
'''
class StarkConfig(object):
    def __init__(self, num):
        self.num = num
    def __call__(self, *args, **kwargs):
        print(self.num)
class RoleConfig(StarkConfig):
    def __call__(self, *args, **kwargs):
        print(self.num)

v1 = StarkConfig(1)
v2 = RoleConfig(11)

v1()
v2()
'''

'''
结果：1
     11
'''

# 16.看代码写结果
'''
class StarkConfig(object):
    def __init__(self, num):
        self.num = num

    def run(self):
        self()

    def __call__(self, *args, **kwargs):
        print(self.num)


class RoleConfig(StarkConfig):
    def __call__(self, *args, **kwargs):
        print(345)

    def __getitem__(self, item):
        return self.num[item]


v1 = RoleConfig('alex')
v2 = StarkConfig("wupeiqi")

print(v1[1])
print(v2[2])
'''

'''
结果：l
     报错
'''

# 17.补全代码
'''
class Context:
    def do_something(self):
        pass
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

with Context() as ctx:
    ctx.do_something()
'''

# 18.补全代码
'''


class Stack(object):
    def __init__(self):
        self.data_list = []

    def push(self, val):
        self.data_list.append(val)

    def pop(self):
        length = len(self.data_list)
        if length >= 1:
            return self.data_list.pop(length - 1)


obj = Stack()
# 调用push方法，将数据加入到data_list中。
obj.push('alex')
obj.push('武沛齐')
obj.push('金老板')

# 调用pop讲数据从data_list获取并删掉，注意顺序(按照后进先出的格式)
v1 = obj.pop()  # 金老板
v2 = obj.pop()  # 武沛齐
v3 = obj.pop()  # alex

# 请补全Stack类中的push和pop方法，将obj的对象维护成 后进先出 的结构。
'''


# 19.如何主动触发一个异常？
'''
raise Exception()
'''


# 20.看代码写结果
'''
def func(arg):
    try:
        int(arg)
    except Exception as e:
        print('异常')
    finally:
        print('哦')


func('123')
func('二货')
'''

'''
结果：哦
     异常
     哦
'''
