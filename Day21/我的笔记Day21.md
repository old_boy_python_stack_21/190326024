# Day21

## 今日内容

1. 嵌套
2. 特殊成员
3. type、isinstance、issubclass
4. super
5. 异常处理

## 内容详细

### 1.嵌套

1. 函数：参数可以是任意类型
2. 字典：对象和类都可以当做字典的key和value

### 2.特殊成员

#### 2.1 `__init__(self)`

初始化函数,为对象的变量赋值

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
        
obj = Foo('alex')
```

#### 2.2 `__new__(cls,*args,**kwargs)`

构造方法，在实例化一个对象时会有两步`obj = Foo('alex')`

- 第一步调用`__new__()`方法
- 第二步调用`__init__()`方法初始化变量

```python
class Foo(object):
    def __init__(self, name):
        self.name = name

    def __new__(cls, *args, **kwargs):
        print(args[0])
        return object.__new__(cls) #返回什么obj就是什么

obj = Foo('alex')
print(obj.name)
```

若类内部无`__new__()`方法，则会调用object的`__new__()`方法

#### 2.3 `__call__(self)`

对象后面加括号会执行`__call__()`方法

```python
class Foo(object):
    def __call__(self, *args, **kwargs):
        print('执行了__call__方法')

Foo()()
```

#### 2.4 `__getitem__(self,item)`

```python
class Foo(object):
    def __getitem__(self, item):
        return item

obj = Foo()
print(obj['1']) #1
```

#### 2.5 `__setitem__(self,key,value)`

```python
class Foo(object):
    def __setitem__(self, key, value):
        print(key,value)

obj = Foo()
obj['k1'] = 123 #k1 123
```

#### 2.6 `__delitem__(self,key)`

```python
class Foo(object):
    def __delitem__(self, key):
        print(key)
        
obj = Foo()
del obj['k1']  #k1
```

#### 2.7 `__str__(self)`

打印对象时执行，打印内容为返回值

```python
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def __str__(self):
        return '姓名：%s,年龄：%s'%(self.name,self.age,)


obj = Foo('alex',18)
print(obj) #姓名：alex,年龄：18
```

#### 2.8 `__dict__`

寻找对象的所有变量，并转换成字典

```python
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age


obj = Foo('alex',18)
print(obj.__dict__) #{'name': 'alex', 'age': 18}
```

#### 2.9 上下文管理

```python
class Foo(object):

    def __enter__(self):
        print('开始')
        return Foo()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('结束')


with Foo() as obj:   #with后面的Foo()会执行__enter__()方法,并将返回值赋给obj
    print(123)       #缩进代码执行完成后会执行__exit__()方法
                     #开始
        			 #123
            		 #结束
```

#### 2.10 对象相加

```python
class Foo(object):
    def __init__(self, name):
        self.name = name

    def __add__(self, other):
        return '%s + %s'%(self.name,other.name,)

    def __sub__(self, other):
        return '%s - %s' % (self.name, other.name,)

    def __mul__(self, other):
        return '%s * %s' % (self.name, other.name,)

    def __divmod__(self, other):
        return '%s / %s' % (self.name, other.name,)

    def __mod__(self, other):
        return '%s %% %s' % (self.name, other.name,)

obj1 = Foo('alex')
obj2 = Foo('eric')
print(obj1 + obj2) #调用obj1对象的类的__add__(self,other)方法，将obj1赋给self,obj2赋给other，返回值是表达式结果
print(obj1 - obj2)
print(obj1 * obj2)
print(divmod(obj1,obj2))
print(obj1 % obj2)
```

### 3.内置函数补充

#### 3.1 type()

查看对象的类型

```python
v = 123
print(type(123)) #int
```

#### 3.2 isinstance(obj,class)

查看obj是否是class或class的基类的实例

```python
class Base(object):
    pass

class Foo(Base):
    pass

obj = Foo()

print(type(obj) == Base) #False
print(isinstance(obj,Base)) #True
```

3.3 issubclass(class,class)  

查看一个类是否是另一个类的子类

```python
class Base(object):
    pass

class Foo(Base):
    pass

class Bar(Foo):
    pass

print(issubclass(Bar,Base))#True
```

### 4.super关键字

super().func() 根据self对象所属的类的继承关系,按照顺序挨个查找func()函数，找到第一个开始执行，后面不再找。

```python
class Base(object):
	def func(self):
		print('base.func')
		return 123
class Foo(Base):
	def func(self):
		v1 = super().func()
		print('foo.func',v1)
obj = Foo()
obj.func()
# super().func() 去父类中找func方法并执行
```

```python
class Bar(object):
	def func(self):
		print('bar.func')
		return 123
    
class Base(Bar):
	pass

class Foo(Base):
	def func(self):
		v1 = super().func()
		print('foo.func',v1)
obj = Foo()
obj.func()
# super().func() 根据类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

```python
class Base(object): # Base -> object
	def func(self):
		super().func()
		print('base.func')

class Bar(object):
	def func(self):
		print('bar.func')
class Foo(Base,Bar): # Foo -> Base -> Bar
	pass
obj = Foo()
obj.func()
# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

### 5.异常处理

#### 5.1 基本格式

```python
def func():
    try:
        pass
    except IndexError as e: 
        pass
    except ValueError as e:
        pass
    except Exception as e: #e为Exception类的一个对象
        pass
    finally:   #finally内的代码，上面遇到return也会被执行
        pass
```

#### 5.2 抛出异常(raise)

```python
def func():
	result = True
	try:
		with open('x.log',mode='r',encoding='utf-8') as f:
			data = f.read()
			if 'alex' not in data:
			raise Exception()
	except Exception as e:
		result = False
	return result
```

#### 5.3 自定义异常

```python
class MyException(Exception):
    pass

def func():
    try:
        raise MyException()
    except MyException as e:
        print(e)
```

