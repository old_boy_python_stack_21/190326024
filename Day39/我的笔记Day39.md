# Day39

## 今日内容

1. 约束
2. 修改表结构
3. 表关系
4. 操作数据

## 内容详细

### 1.约束

#### 1.1 无符号 unsigned

```sql
create table t1(id int unsigned);
```

#### 1.2 不为空 not null

```sql
create table t2(id int not null);
```

#### 1.3 默认值 default

```sql
 create table t3(id int not null,age int default 18,
gender enum('male','female') default 'male');
```

#### 1.4 唯一 unique

设置某一个字段不能重复

```sql
create table t4(id int not null unique,name char(4)); 

--联合唯一
create table t4(id int not null,name char(4) default 'alex',unique(id,name));--id和name联合唯一
```

#### 1.5 自增 auto_increment

设置某一个int类型的字段 自动增加,auto_increment自带not null效果,设置条件int，unique

```sql
create table t5 (id int auto_increment unique,name char(4));
```

#### 1.6 主键 primary key

- 设置某一个字段非空且不能重复

- 约束力相当于 not null + unique

- 一张表只能由一个主键

- 你指定的第一个非空且唯一的字段会被定义成主键

  ```sql
  create table t6 (id int not null unique,name char(4) not null unique);--你指定的第一个非空且唯一的字段会被定义成主键
  
  create table t7(id int primary key,name char(4));
  
  --联合主键
  create table t8(id int,ip char(15),server char(10),port int,primary key(ip,port));--两个组合唯一
  ```

#### 1.7 外键 foreign key

- 涉及到两张表

  ```sql
  --员工表
  create table staff(id int primary key, age int default 18, gender enum('male','female') default 'male', post_id int,foreign key (post_id) references post(pid));
      
  --部门表
  create table post(pid int primary key,name char(5));
  
  
  --级联删除和级联更新（操作的都是从表）
  create table staff2(
  id  int primary key auto_increment,
  age int,
  gender  enum('male','female'),
  post_id int,
  foreign key(post_id) references post(pid) on update cascade on delete set null
  )--删除时，将外键置为空
  
  create table staff2(
  id  int primary key auto_increment,
  age int,
  gender  enum('male','female'),
  post_id int,
  foreign key(post_id) references post(pid) on update cascade on delete cascade
  )--级联更新删除
  
  
  update post set pid=2 where pid = 1;--级联更新会把staff的外键一起更新，更新staff的外键值，若post中不存在，则报错
  delete from post where pid = 1;--会把主表的外键变为null
  ```

### 2.修改表结构

#### 2.1 添加字段

```sql
--alter table 表名 add 添加字段
alter table staff add(name char(4),sno int);

--alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
```

#### 2.2 删除字段

```sql
--alter table 表名 drop 删除字段
alter table staff drop name;
```

#### 2.3 修改字段

- 修改已经存在的字段  的类型 宽度 约束

```sql
--alter table 表名 modify 修改已经存在的字段  的类型 宽度 约束
alter table staff modify sno char(4);
```

- 修改已经存在的字段  的类型 宽度 约束 和 字段名字

```sql
--alter table 表名 change 修改已经存在的字段  的类型 宽度 约束 和 字段名字
alter table staff change nnno name char(4);
```

```sql
--alter table 表名 modify age int not null after id;
--alter table 表名 modify age int not null first;
```

### 3.表关系

book 书：id,name,price,author_id

author 作者：aid,name,birthday,gender

#### 3.1 一对多

```sql
--作者表（一）
create table author(
aid int primary key,
name char(12) not null,
birthday date,
gender enum('male','female') default 'male'
)

--书表（多）
create table book(
id int primary key,
name char(12) not null,
price float(5,2),
author_id int,
foreign key(author_id) references author(aid)
)
```

#### 3.2 一对一

```sql
--作者表（一）
create table author(
aid int primary key,
name char(12) not null,
birthday date,
gender enum('male','female') default 'male'
)

--书表（一）
create table book(
id int primary key,
name char(12) not null,
price float(5,2),
author_id int unique,
foreign key(author_id) references author(aid)
)
```

#### 3.3 多对多

```sql
--作者表（多）
create table author(
aid int primary key,
name char(12) not null,
birthday date,
gender enum('male','female') default 'male'
)

--书表（多）
create table book(
id int primary key,
name char(12) not null,
price float(5,2)
)

--中间表，存储两表关系
create table book_author(
id int primary key,
bid int not null,
author_id int not null,
foreign key(bid) references book(id),
foreign key(author_key) references author(bid)
)
```

### 4.数据操作

#### 4.1 增

```sql
--插入一条数据
insert into 表名 values (值1,值2,...);
--插入多条数据
insert into 表名 values (值1,值2,...),(值1,值2,...),(值1,值2,...);
--指定插入的字段
insert into 表名 (字段1,字段2) values (值1,值2);
```

#### 4.2 删

```sql
delete from 表名 where 条件;
```

#### 4.3 改

```sql
update 表名 set 字段 = 新值 where 条件;
```

#### 4.4 查

```sql
--查询所有
select * from 表名 where 条件;
-- 查询某些字段
select 字段1，字段2,... from 表名 where 条件;
--按照查出来的字段去重
select distanct 字段1,字段2,... from 表名 where 条件;
--查询字段可以包括表达式
select 字段*5 from 表名 where 条件;
--起别名
select 字段1 as 别名 from 表名 where 条件;
```





















