from django.views import View
from app01 import models
from django.utils.decorators import method_decorator
from django.shortcuts import render,HttpResponse
from django.views.decorators.csrf import csrf_exempt,csrf_protect,ensure_csrf_cookie


class Register(View):

    def get(self,request,*args,**kwargs):
        return render(request,'register.html')

    def post(self,request,*args,**kwargs):
        pass

def username(request):
    username = request.POST.get('username')
    if models.UserInfo.objects.filter(username=username):
        return HttpResponse('用户名已存在')
    return HttpResponse('')