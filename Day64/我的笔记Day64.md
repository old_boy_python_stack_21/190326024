# Day64

## 今日内容

1. csrf装饰器
2. csrf功能
3. ajax

## 内容详细

### 1.csrf装饰器

- csrf_exempt装饰器      某个视图不需要进行csrf校验

  ```python
  from django.views import View
  from django.utils.decorators import method_decorator
  from django.shortcuts import render,HttpResponse
  from django.views.decorators.csrf import csrf_exempt,csrf_protect,ensure_csrf_cookie
  
  #在CBV中必须要加在CBV的dispatch方法上。
  @method_decorator(csrf_exempt,name='dispatch')
  class Index(View):
      def get(self,request,*args,**kwargs):
          return render(request, 'index.html')
  
      def post(self,request,*args,**kwargs):
          print(request.POST.get('user'))
          return render(request,'index.html')
      
  #加在FBV上
  @csrf_exempt
  def index(request):
      if request.method == 'POST':
          print(request.POST.get('user'))
      return render(request,'index.html')
  ```

- csrf_protect   某个视图需要进行csrf校验。

  ```python
  #csrf_protect  某个视图需要进行csrf校验。若没有在settings中设置'django.middleware.csrf.CsrfViewMiddleware' 全局校验，则可以使用该装饰器为单个视图函数设置校验
  
  #CBV
  class Index(View):
      def get(self,request,*args,**kwargs):
          return render(request, 'index.html')
  
      @method_decorator(csrf_protect)
      def post(self,request,*args,**kwargs):
          print(request.POST.get('user'))
          return render(request,'index.html')
  
  #FBV
  @csrf_protect
  def index(request):
      if request.method == 'POST':
          print(request.POST.get('user'))
      return render(request,'index.html')
  ```

- ensure_csrf_cookie  确保生成csrf的cookie，（不会在模板生成一个隐藏的模板标签（在post的数据中并没有csrfmiddlewaretoken））

  ```python
  #CBV
  class Index(View):
      @method_decorator(ensure_csrf_cookie)
      def get(self,request,*args,**kwargs):
          return render(request, 'index.html')
  
      def post(self,request,*args,**kwargs):
          print(request.POST.get('user'))
          return render(request,'index.html')
      
  #FBV
  @ensure_csrf_cookie
  def index(request):
      if request.method == 'POST':
          print(request.POST.get('user'))
      return render(request,'index.html')
  ```

### 2.csrf功能

1.csrf中间件中执行process_request：

​	1. 从cookie中获取到csrftoken的值

```
2. csrftoken的值放入到 request.META
```

2.执行process_view

1. 查询视图函数是否使用csrf_exempt装饰器，使用了就不进行csrf的校验

2. 判断请求方式：

   1. 如果是GET', 'HEAD', 'OPTIONS', 'TRACE'

      不进行csrf校验

      2. 其他的请求方式（post，put）

      进行csrf校验：

      1.获取cookie中csrftoken的值

      获取csrfmiddlewaretoken的值

      ​	能获取到  ——》 request_csrf_token

      ​	获取不到   ——》 获取请求头中X-csrftoken的值 ——》request_csrf_token

      比较上述request_csrf_token和cookie中csrftoken的值，比较成功接收请求，比较不成功拒绝请求。

**注：使请求中含有csrftoken的cookie的两种方法：**

- 在setting.py文件的中间件项中放开'django.middleware.csrf.CsrfViewMiddleware'（全局csrf校验），并且在模板表单中添加{% csrf_token %}
- 为返回页面的视图函数添加ensure_csrf_cookie装饰器。

**注：校验生效的两种情况：**

- 把模板{% csrf_token %}标签生成的csrfmiddlewaretoken的值传到后台和csrftoken(cookie)作比较。比较通过，验证通过。 
- 未向后台传csrfmiddlewaretoken的值，但是在请求头中设置了X-csrftoken的值，csrftoken(cookie)和X-csrftoken的值作比较，比较通过，则通过验证。

### 3.ajax

#### 3.1 定义

就是使用js的技术发请求和接收响应的。

#### 3.2 特点

- 异步

- 局部刷新

- 传输的数据量少

#### 3.3 jqery发ajax请求

```html
<script>
	$.ajax({
        url: '/calc/',
        type: 'post',
        data: {
            a: $("[name='i1']").val(),
            b: $("[name='i2']").val(),
            hobby: JSON.stringify(['唱', '跳', 'rap']),
        },
        success: function (res) {
            //json格式字符串反序列化
            //res = JSON.parse(res)
            //res.xxx
            $("[name='i3']").val(res)
        },
        error:function (error) {
            console.log(error)
        }
	})
</script>
```

#### 3.4 上传文件

前端代码：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form action="">
    <input type="file" id="f1">
    <button id="b1" type="button">上传</button>
</form>
<script src="/static/jquery.js"></script>
<script>
    $('#b1').click(function () {
       var formobj = new FormData();
        //formobj.append('file', $('#f1')[0].files[0]); //js对象的files[0]获取文件对象
        formobj.append('file',document.getElementById('f1').files[0])
        formobj.append('name', '123'); //添加一些其他数据
        $.ajax({
            url: '/upload/',
            type: 'post',
            processData: false,
            contentType: false,
            data: formobj,
            success: function (res) {
                console.log(res)
            },
        });
    })
</script>
</body>
</html>
```

后台视图函数代码

```python
def upload(request):
    print(request.FILES)
    name = request.POST.get('name')
    f1 = request.FILES.get('file')
    with open(f1.name,mode='wb') as f:
        for i in f1.chunks():
            f.write(i)
    return HttpResponse('ok')
```

#### 3.5 ajax通过csrf的校验

首先确保有cookie csrftoken：

- 添加装饰器ensure_csrf_cookie
- 设置全局csrf校验，添加标签{% csrf_token %}

然后确保有和cookie比较校验的csrfmiddlewaretoken或x-csrftoken：

- 在data中添加csrfmiddlewaretoken

  ```js
  data: {
      'csrfmiddlewaretoken':$('[name="csrfmiddlewaretoken"]').val(),
      a: $("[name='i1']").val(),
      b: $("[name='i2']").val(),
  },
  ```

- 在请求头中添加x-csrftoken

  ```js
  $.ajax({
              url: '/calc2/',
              type: 'post',
              headers: {
                  'x-csrftoken':$('[name="csrfmiddlewaretoken"]').val(),
              },
              data: {
                  a: $("[name='ii1']").val(),
                  b: $("[name='ii2']").val(),
              },
              success: function (res) {
                  $("[name='ii3']").val(res)
              },
          })
  ```

  