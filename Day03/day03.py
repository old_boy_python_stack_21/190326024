#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
第三天作业
王凯旋
"""
# 1.有变量name = "aleX leNb " 完成如下操作：

# 移除 name 变量对应的值两边的空格,并输出处理结果
'''
name = "aleX leNb "
name = name.strip()
print(name)
'''

# 判断 name 变量是否以 "al" 开头,并输出结果（用切片）
'''
name = "aleX leNb "
begin = name[0:2]
result = begin == "al"
print(result)
'''

# 判断name变量是否以"Nb"结尾,并输出结果（用切片）
'''
name = "aleX leNb "
end = name[-2:]
result = end == "Nb"
print(result)
'''

# 将 name 变量对应的值中的 所有的"l" 替换为 "p",并输出结果
'''
name = "aleX leNb "
result = name.replace("l", "p")
print(result)
'''

# 将name变量对应的值中的第一个"l"替换成"p",并输出结果
'''
name = "aleX leNb "
result = name.replace("l", "p", 1)
print(result)
'''

# 将 name 变量对应的值根据 所有的"l" 分割,并输出结果
'''
name = "aleX leNb "
result = name.split("l")
print(result)
'''

# 将name变量对应的值根据第一个"l"分割,并输出结果
'''
name = "aleX leNb "
result = name.split("l", 1)
print(result)
'''

# 将 name 变量对应的值变大写,并输出结果
'''
name = "aleX leNb "
result = name.upper()
print(result)
'''

# 将 name 变量对应的值变小写,并输出结果...
'''
name = "aleX leNb "
result = name.lower()
print(result)
'''

# 请输出 name 变量对应的值的第 2 个字符?
'''
name = "aleX leNb "
result = name[1]
print(result)
'''

# 请输出 name 变量对应的值的前 3 个字符?
'''
name = "aleX leNb "
result = name[0:3]
print(result)
'''

# 请输出 name 变量对应的值的后 2 个字符?
'''
name = "aleX leNb "
result = name[-2:]
print(result)
'''

# 2.有字符串s = "123a4b5c"
# 通过对s切片形成新的字符串 "123"
'''
s = "123a4b5c"
result = s[0:3]
print(result)
'''

# 通过对s切片形成新的字符串 "a4b"
'''
s = "123a4b5c"
result = s[3:6]
print(result)
'''

# 通过对s切片形成字符串s5,s5 = "c"
'''
s = "123a4b5c"
s5 = s[-1:]
print(s5)
'''

# 通过对s切片形成字符串s6,s6 = "ba2"
'''
s = "123a4b5c"
s6 = s[5:6]
s6 += s[3:4]
s6 += s[1:2]
print(s6)
'''

# 3.使用while循环字符串 s="asdfer" 中每个元素。
'''
s = "asdfer"
length = len(s)
index = 0
while index < length:
    print(s[index])
    index += 1
'''

# 4.使用while循环对s="321"进行循环，打印的内容依次是："倒计时3秒"，"倒计时2秒"，"倒计时1秒"，"出发！"
'''
s = "321"
length = len(s)
index = 0
while True:
    if index < length:
        count = s[index]
        print("倒计时%s秒" % (count,))
    else:
        print("出发！")
        break
    index += 1
'''

# 5.实现一个整数加法计算器(两个数相加)：
# 如：content = input("请输入内容:") 用户输入：5+9或5+ 9或5 + 9（含空白），然后进行分割转换最终进行整数的计算得到结果。
'''
message = input("请输入内容:")
value = message.split("+")
first_value = int(value[0].strip())
second_value = int(value[1].strip())
result = first_value + second_value
print(result)
'''

# 6.计算用户输入的内容中有几个 h 字符？
# 如：content = input("请输入内容：")   # 如fhdal234slfh98769fjdla
'''
content = input("请输入内容：")
length = len(content)
index = 0
total = 0
while index < length:
    if content[index] == "h":
        total += 1
    index += 1
print(total)
'''

# 7.计算用户输入的内容中有几个 h 或 H  字符？
# 如：content = input("请输入内容：")   # 如fhdal234slfH9H769fjdla
'''
content = input("请输入内容：")
content = content.lower()
length = len(content)
index = 0
total = 0
while index < length:
    if content[index] == "h":
        total += 1
    index += 1
print(total)
'''

# 8.使用while循环分别正向和反向对字符串 message = "伤情最是晚凉天，憔悴厮人不堪言。"。
'''
# 正向
message = "伤情最是晚凉天，憔悴厮人不堪言。"
length = len(message)
index = 0
while index < length:
    print(message[index])
    index += 1
'''

'''
# 反向
message = "伤情最是晚凉天，憔悴厮人不堪言。"
length = len(message)
index = -1
while index >= 0 - length:
    print(message[index])
    index -= 1
'''

# 9.获取用户输入的内容中 前4个字符中 有几个 A ？
# 如：content = input("请输入内容：")   # 如fAdal234slfH9H769fjdla
'''
content = input("请输入内容：")
length = len(content)
total = 0
index = 0
if length >= 4:
    while index < 4:
        if content[index] == "A":
            total += 1
        index += 1
else:
    while index < length:
        if content[index] == "A":
            total += 1
        index += 1
print(total)
'''

# 10.如果判断name变量对应的值前四位"l"出现几次,并输出结果。
'''
name = input("请输入姓名：")
length = len(name)
total = 0
index = 0
if length >= 4:
    while index < 4:
        if name[index] == "l":
            total += 1
        index += 1
else:
    while index < length:
        if name[index] == "l":
            total += 1
        index += 1
print(total)
'''

# 11.获取用户两次输入的内容，并将所有的数据获取并进行相加，如：
'''
要求：
	将num1中的的所有数字倒找并拼接起来：1232312
	将num1中的的所有数字倒找并拼接起来：1218323
	然后将两个数字进行相加。
'''

'''
num1 = input("请输入：")  # asdfd123sf2312
num2 = input("请输入：")  # a12dfd183sf23
length1 = len(num1)
length2 = len(num2)
index1 = -1
index2 = -1
result1 = ""
result2 = ""
while index1 >= 0 - length1:
    if num1[index1].isdigit():
        result1 += num1[index1]
    index1 -= 1
while index2 >= 0 - length2:
    if num2[index2].isdigit():
        result2 += num2[index2]
    index2 -= 1
print(int(result1) + int(result2))
'''
