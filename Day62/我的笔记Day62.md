# Day62

## 今日内容

1. 多对多查询
2. 分组聚合
3. F和Q查询
4. 事务

## 内容详细

### 1.多对多查询

表关系

```python
class Book(models.Model):
    title = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=6, decimal_places=2)  # 9999.99
    sale = models.IntegerField()
    kucun = models.IntegerField()
    pub = models.ForeignKey(Publisher, null=True,
                            on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Author(models.Model):
    name = models.CharField(max_length=32, )
    books = models.ManyToManyField(Book)

    def __str__(self):
        return self.name
```

#### 1.1 基于对象查询

```python
#从author表查关联的book,多对多字段写在author类中，正向查询
author_obj = models.Author.objects.get(pk=1)
book_list = author_obj.books.all() #获取与该作者关联的所有书籍对象 books关系管理对象

#从book表查关联的author,多对多字段写在author类中，反向查询
#未设置related_name
book_obj = models.Book.objects.get(pk=1)
author_list = book_obj.author_set.all() #获取与该书籍关联的所有作者对象 author_set关系管理对象
#设置related_name = authors
book_obj = models.Book.objects.get(pk=1)
author_list = book_obj.authors.all() #获取与该书籍关联的所有作者对象 authors关系管理对象
```

#### 1.2 基于字段查询

```python
#查与书籍相关的作者
author_list = models.Author.objects.filter(books__title = '菊花怪大战MJJ')

#查询作者相关的书籍
#未设置related_name和related_query_name
book_list = models.Book.objects.filter(author__pk = 1)
#设置related_name='authors',未设置related_query_name
book_list = models.Book.objects.filter(authors__pk = 1)
#同时设置了related_name='authors'和related_query_name='xxx'(字段查询时使用的值)
book_list = models.Book.objects.filter(xxx__pk = 1)
```

#### 1.3 关系管理对象的方法

- all()  获取关联的所有对象

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.all()
  ```

- set() 设置多对多关系，会覆盖之前的数据

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.set([1,2,3]) #可以设置关联的主键id
  author_obj.books.set(models.Book.objects.filter(pk__in=[1,2,3])) #也可以设置对象集合
  ```

- add() 添加多对多数据，不会覆盖之前的数据

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.add(1,2) #不接受列表参数
  author_obj.books.add(*models.Book.objects.filter(pk__in=[1,2]))#列表前加*把列表拆开打散
  ```

- remove() 删除多对多关系 

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.remove(1,2) #不接受列表参数
  author_obj.books.remove(*models.Book.objects.filter(pk__in=[1,2]))#列表前加*把列表拆开打散
  ```

- clear()   清空多对多关系

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.clear()
  ```

- create() 创建对象并关联

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.create(title='跟MJJ学前端',pub_id=1) #创建书籍对象（增加一条书籍数据）并关联作者
  ```

#### 1.4 多对一关系的方法

多对一关系中，关系管理对象的方法和多对多关系管理对象的方法有些差别

- 只能使用对象，不能使用id
- 要使用remove和clear方法，必须把外键设置为可为null

```python
pub_obj = models.Publisher.objects.get(pk=1)
#set
pub_obj.book_set.set(*models.Book.objects.filter(pk__in=[2,3]))#只能是对象
#add
pub_obj.book_set.add(*models.Book.objects.filter(pk__in=[2,3]))#只能是对象
#remove pub = models.ForeignKey(Publisher, null=True,on_delete=models.CASCADE)
pub_obj.book_set.remove(*models.Book.objects.filter(pk__in=[2,3]))#只能是对象
pub_obj.book_set.clear()
```

### 2.分组聚合

#### 2.1 聚合

```python
from django.db.models import Max, Min, Avg, Sum, Count
ret = models.Book.objects.filter(pk__gt=3).aggregate(Max('price'),avg=Avg('price')) #聚合方法返回一个列表，是一个终止分句，后面不能继续使用ORM操作方法
```

#### 2.2 分组聚合

```python
from django.db.models import Max, Min, Avg, Sum, Count
# 统计每一本书的作者个数
ret = models.Book.objects.annotate(count = Count('author')) #根据书籍分组，通过作者聚合
# 统计出每个出版社的最便宜的书的价格
#方式一
ret = models.Publisher.objects.annotate(min=Min(book__price)).values()
#方式二
ret = models.Book.objects.values('pub_id').annotate(min=Min('price')).values('pub_id','min')#按照出版社id分组，后边的values只能取前面出现的字段
#统计不止一个作者的图书
ret = models.Book.objects.annotate(count=Count('author')).filter(count__gt=1)
# 查询各个作者出的书的总价格
ret = models.Author.objects.annotate(sum = Sum('books__price')).values()
```

### 3.F和Q查询

#### 3.1 F查询

```python
from django.db.models import F
# 比较两个字段的值  查找销售量大于库存的书籍
ret = models.Book.objects.filter(sale__gt=F('kucun'))
```

更新数据的两种方式：

```python
#方式一
obj = models.Book.objects.get(pk=1)
obj.title = '123'
obj.save()
#方式二
models.Book.objects.filter(pk__in=[1,2,3]).update(title='123') #将所有满足条件的都更新
```

```python
from django.db.models import F
# 比较两个字段的值  将销量翻倍
ret = models.Book.objects.all().update(sale=F('sale')*2)
```

#### 3.2 Q查询

```python
from django.db.models import Q
#获取id大于3或小于2，且价格大于50的书籍
ret = models.Book.objects.filter(Q(Q(pk__gt=3) | Q(pk__lt=2)) & Q(price__gt=50))
# & 与  |或  ~非
```

### 4.事务

```python
from django.db import transaction
try: #异常捕捉要放到事务外边
    with transaction.atomic():
        #进行orm操作，若中间报错，则自动回滚
        models.Publisher.object.create(name='123')
        models.Publisher.object.create(name='123')
except Exception as e:
    print(e)
```

