#!/usr/bin/env python
# -*- coding:utf-8 -*-
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "day61.settings")

import django
django.setup()

from app01 import models
from django.db.models import Max,Min,Sum,Count,Avg,Q

# 查找所有书名里包含金老板的书
'''
obj_list = models.Book.objects.filter(title__contains='金老板')
sql = "select * from app01_book where title like '%金老板%'"
'''


# 查找出版日期是2018年的书
'''
obj_list = models.Book.objects.filter(publish_date__year='2018')
sql = "select * from app01_book where publish_date betwwen '2018-01-01' and '2018-12-31'"
'''

# 查找出版日期是2017年的书名
'''
obj_list = models.Book.objects.filter(publish_date__year='2017').values('title')
sql = "select title from app01_book where publish_date betwwen '2017-01-01' and '2017-12-31'"
'''

# 查找价格大于10元的书
'''
obj_list = models.Book.objects.filter(price__gt=10)
sql = "select * from app01_book where price > 10"
'''

# 查找价格大于10元的书名和价格
'''
obj_list = models.Book.objects.filter(price__gt=10).values('title','price')
sql = "select title,price from app01_book where price > 10"
'''

# 查找memo字段是空的书
'''
obj_list = models.Book.objects.filter(memo__isnull=True)
sql = "select * from app01_book where memo is null"
'''

# 查找在北京的出版社
'''
obj_list = models.Publisher.objects.filter(city='北京')
sql = "select * from app01_publisher where city = '北京'"
'''

# 查找名字以沙河开头的出版社
'''
obj_list = models.Publisher.objects.filter(name__startswith='沙河')
sql = "select * from app01_publisher where name like '沙河%'"
'''

# 查找“沙河出版社”出版的所有书籍
'''
obj_list = models.Book.objects.filter(publisher__name='沙河出版社')
sql = "select book.id,title,publish_date,price,memo from app01_book as book inner join app01_publisher as p on book.publisher_id = p.id where p.name = '沙河出版社'"
'''

# 查找每个出版社出版价格最高的书籍价格
'''
ret = models.Publisher.objects.annotate(max=Max('book__price')).values('max')
sql = "select max(b.price) from app01_publisher as p left join app01_book as b on p.id = b.publisher_id group by p.id"
'''

# 查找每个出版社的名字以及出的书籍数量
'''
ret = models.Publisher.objects.annotate(count = Count('book')).values('name','count')
print(ret)
sql = "select p.id,p.name,count(b.id) from app01_publisher as p left join app01_book as b on p.id = b.publisher_id group by p.id"
'''

# 查找作者名字里面带“小”字的作者
'''
obj_list = models.Author.objects.filter(name__contains='小')
sql = "select * from app01_author where name like '%小%'"
'''

# 查找年龄大于30岁的作者
'''
obj_list = models.Author.objects.filter(age__gt=30)
sql = "select * from app01_author where age > 30"
'''

# 查找手机号是155开头的作者
'''
obj_list = models.Author.objects.filter(phone__startswith='155')
sql = "select * from app01_author where phone like '155%'"
'''

# 查找手机号是155开头的作者的姓名和年龄
'''
obj_list = models.Author.objects.filter(phone__startswith='155').values('name','age')
sql = "select name,age from app01_author where phone like '155%'"
'''

# 查找每个作者写的价格最高的书籍价格
'''
ret = models.Author.objects.annotate(max=Max('book__price')).values('max')
sql = "select a2.id,a2.name,a1.price from (select ba.author_id as id, max(b.price) as price from app01_book_author as ba left join app01_book as b on ba.book_id = b.id group by ba.author_id) as a1 inner join app01_author as a2 on a1.id = a2.id"
'''

# 查找每个作者的姓名以及出的书籍数量
'''
ret = models.Author.objects.annotate(count = Count('book')).values('name','count')
sql = "select a.name, count(b.book_id) as num from app01_book_author as b left join app01_author as a on b.author_id = a.id group by b.author_id"
'''

# 查找书名是“跟金老板学开车”的书的出版社
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('publisher__id','publisher__name','publisher__city')
sql = "select p.id,p.name,p.city from app01_book as b left join app01_publisher as p on b.publisher_id = p.id where b.title = '跟金老板学开车'"
'''

# 查找书名是“跟金老板学开车”的书的出版社所在的城市
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('publisher__city')
sql = "select p.city from app01_book as b left join app01_publisher as p on b.publisher_id = p.id where b.title = '跟金老板学开车'"
'''

# 查找书名是“跟金老板学开车”的书的出版社的名称
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('publisher__name')
sql = "select p.name from app01_book as b left join app01_publisher as p on b.publisher_id = p.id where b.title = '跟金老板学开车'"
'''

# 查找书名是“跟金老板学开车”的书的出版社出版的其他书籍的名字和价格
'''
obj_list = models.Book.objects.filter(Q(publisher__in=models.Publisher.objects.filter(book__title='跟金老板学开车'))&~Q(title='跟金老板学开车')).values('title','price')
sql = "select * from (select p.id as id from app01_book as b left join app01_publisher as p on b.publisher_id = p.id where b.title = '跟金老板学开车') as a1 left join app01_book as a2 on a1.id = a2.publisher_id where a2.title != '跟金老板学开车'"
'''

# 查找书名是“跟金老板学开车”的书的所有作者
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('author__id','author__name','author__age','author__phone')
sql = "select a2.id,a2.name,a2.age,a2.phone from (select author_id from app01_book as a left join app01_book_author as ba on a.id = ba.book_id where title = '跟金老板学开车') as a1 left join app01_author as a2 on a1.author_id = a2.id"
'''

# 查找书名是“跟金老板学开车”的书的作者的年龄
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('author__age')
sql = "select a2.age from (select author_id from app01_book as a left join app01_book_author as ba on a.id = ba.book_id where title = '跟金老板学开车') as a1 left join app01_author as a2 on a1.author_id = a2.id"
'''

# 查找书名是“跟金老板学开车”的书的作者的手机号码
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('author__phone')
sql = "select a2.phone from (select author_id from app01_book as a left join app01_book_author as ba on a.id = ba.book_id where title = '跟金老板学开车') as a1 left join app01_author as a2 on a1.author_id = a2.id"
'''

# 查找书名是“跟金老板学开车”的书的作者们的姓名以及出版的所有书籍名称和价钱
'''
obj_list = models.Book.objects.filter(title='跟金老板学开车').values('author__name','author__book__title','author__book__price')
print(obj_list)

sql = "select a3.name,a4.title,a4.price from (select a1.name as name,a2.book_id as id from (select id,name from app01_author where id in (select author_id from app01_book as b left join app01_book_author as ba on b.id = ba.book_id where b.title = '跟金老板学开车')) as a1 left join app01_book_author as a2 on a1.id = a2.author_id) as a3 left join app01_book as a4 on a3.id = a4.id"
'''