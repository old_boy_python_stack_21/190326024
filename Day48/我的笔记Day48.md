# Day48

## 今日内容

1. BOM模型

2. DOM模型

## 内容详细

### 1.BOM模型

浏览器对象模型，核心对象是window对象。

#### 1.1 BOM模型的对话框

##### 1.1.1 弹出框

```js
window.alert('mjj');//window可以省略不写
```

##### 1.1.2 提示框

```js
var ret = window.confirm('您确实要删除吗？');
console.log(ret);//点击确定按钮返回true,点击取消按钮返回false
```

##### 1.1.3 输入框

```js
var val = window.prompt('请输入姓名');
console.log(val);//返回输入的值
```

#### 1.2 定时方法

##### 1.2.1 设置延时 setTimeout

```js
//window.setTimeout(回调函数,延迟的时间)  非阻塞
window.setTimeout(fn,2000);
function fn(){
    console.log(111);
}
console.log(222);//先打印222，后打印111
```

##### 1.2.2 定时器 setInterval

```js
//window.setInterval(回调函数,执行间隔)
num = 0;
window.setInterval(function(){
    console.log(num);
    num++;
},1000);

//停止定时器
num = 0;
var timer = window.setInterval(func,1000);
function func(){
    console.log(num);
    num++;
    if(num === 5){
        window.clearInterval(timer);
    }
}
```

#### 1.3 location对象

location对象是window对象的一个属性。

```js
//通过console.dir(对象)查看对象的所有属性和方法
console.dir(window.location);
//reload方法 刷新整个网页（一般用来做测试）
window.location.reload();
//局部刷新，ajax技术：在不重载页面的情况下，对网页进行操作。
```

### 2.DOM模型

文档对象模型。

javascript中的对象分为三种：

- 用户自定义对象：用户自己创建的对象。
- 内建对象：Array,Math,Function
- 宿主对象：window,document

DOM中的节点分类：

```html
<!--元素节点-->
<p></p>  
<!--属性节点-->
<p title=''></p>
<!--文本节点-->
<p>.....</p>
```

#### 2.1 获取元素节点

- getElementById 根据元素id获取元素节点

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box">123</div>
      <script type="text/javascript">
          var box = document.getElementById('box');
          console.log(box);
      </script>
  </body>
  </html>
  ```

- getElementsByTagName 根据元素名获取元素节点，返回一个伪数组。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
      </ul>
      <script type="text/javascript">
          var lis = document.getElementsByTagName('li');
          console.log(lis);
      </script>
  </body>
  </html>
  ```

- getElementsByClassName 通过类名获取元素节点,返回一个伪数组。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <ul>
          <li class="active">1</li>
          <li>2</li>
          <li class="active">3</li>
          <li>4</li>
      </ul>
      <script type="text/javascript">
          var lis = document.getElementsByClassName('active');
          console.log(lis);
      </script>
  </body>
  </html>
  ```

#### 2.2 事件

##### 2.2.1 单击事件

```js
var box = document.getElementById('box');
box.onclick = function(){
    console.log('点击');
}
```

##### 2.2.2 鼠标悬浮事件

```js
var box = document.getElementById('box');
box.onmouseover = function(){
    console.log('悬浮');
}
```

##### 2.2.3 鼠标离开事件

```js
var box = document.getElementById('box');
box.onmouseout = function(){
    console.log('离开');
}
```

#### 2.3 对样式进行操作

```js
var box = document.getElementById('box');
box.style.backgroundColor = 'red'//css中所有的带有-的属性在js中都改为驼峰命名法
```

#### 2.4 对属性进行设置

- 方式一

  ```js
  //getAttribute(name) 获取属性值
  var box = document.getElementById('box');
  var val = box.getAttribute('class');
  
  //setAttribute(name,value) 设置属性值
  var box = document.getElementById('box');
  box.setAttribute('class','a1 a2'); //会覆盖原来的属性值
  ```

- 方式二

  ```js
  //获取属性值
  var box = document.getElementById('box');
  var val =  box.className;//获取class属性值
  //设置属性值
  var box = document.getElementById('box');
  box.abc = '123';//自定义属性
  box.className += ' active'//多添加一个属性 
  ```

  **注：如果是自定义的属性，想要在文档上可以看到需要使用setAttribute,否则只能在对象属性中看到。**

#### 2.5 节点的创建和添加

```js
var li = document.createElement('li');//创建一个节点
var ul = document.getElementById('box');
li.innerText = '123'; //只设置节点文本
li.innerHtml = '<a>123</a>'; //设置节点内html
ul.appendChild(li);


//在一个节点前添加一个新节点insertBefore(新节点,旧节点)
ul.insertBefore(li2,li1);
```

#### 2.6 遍历数据操作页面

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <ul id="box">
    </ul>
    <script type="text/javascript">
        var data = [
            {id:'1',name:'小米5',price:99},
            {id:'2',name:'小米6',price:199},
            {id:'3',name:'小米7',price:299},
            {id:'4',name:'小米8',price:399}
        ];
        var ul = document.getElementById('box');
        for(var i=0;i<data.length;i++){
            var li = document.createElement('li');
            li.innerHTML = `<p>${data[i].name}</p><p>价格:${data[i].price}</p>`;
            ul.appendChild(li);
        }
    </script>
</body>
</html>
```

#### 2.7 轮播图实现

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<div id="box">
			<img src="images/1.jpg" alt="" id="imgBox">
		</div>
		<button id="prev">上一张</button>
		<button id="next">下一张</button>

		<script type="text/javascript">
			var imgBox = document.getElementById('imgBox');
			var next = document.getElementById('next');
			var prev = document.getElementById('prev');
			var num = 1;
			next.onclick = function (){
				nextImg();
			};
			prev.onclick = function () {
                prevImg();
            };
			function prevImg() {
			    if(num === 1){
			        num = 4
                }
			    imgBox.src = `images/${num}.jpg`;
                num--;
            }
			function nextImg(){
				num++;
				if(num === 5){
					num = 1;
				}
				imgBox.src = `images/${num}.jpg`;
			}
			setInterval(nextImg,3000);
		</script>
	</body>
</html>
```



