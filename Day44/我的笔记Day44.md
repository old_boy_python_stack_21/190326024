# Day44

## 今日内容

1. 选择器
   - 伪类选择器
   - 属性选择器
   - 伪元素选择器
2. 常用格式排版
   - 字体属性
   - 文本属性
3. 盒子模型补充
4. 布局-浮动

## 内容详细

### 1.选择器

#### 1.1 伪类选择器

对于a标签，有“爱恨准则”，LoVe HAte。

- L:  link  未链接之前
- V：visited 链接之后
- H：hover 鼠标悬浮上之后 （hover可用于任何标签）
- A：active  鼠标点中之后

对于a标签，如果想要设置a标签的样式，要作用于a标签上，继承不起作用。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        a:link{
            color: aquamarine;
        }
        a:visited{
            color: chartreuse;
        }
        a:hover{
            color: yellow;
        }
        a:active{
            color: darkgreen;
        }
    </style>
</head>
<body>
    <a href="#">百度一下</a>
</body>
</html>
```

利用hover属性实现一些动态效果

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #box{
            width: 200px;
            height:200px;
            background-color: red;
        }
        #box:hover div{
            width: 200px;
            height: 100px;
            background-color: yellow;
        }
        a{
            text-decoration: none;
        }
        a:hover{
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div id="box">
        <div></div>
    </div>

    <a href="">百度一下</a>
</body>
</html>
```

#### 1.2 属性选择器

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        input[type='text']{
            background-color: red;
        }
        input[type='checkbox']{

        }
        input[type='submit']{

        }
    </style>
</head>
<body>
<form action="">
    <input type="text">
    <input type="password">
    <input type="radio">
    <input type="checkbox">
    <input type="file">
</form>
</body>
</html>
```

#### 1.3 伪元素选择器

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        /*可以省略一个冒号*/
        p::first-letter{
            color: red;
        }
        p::before{
            content: '123';
        }
        /*解决浮动布局常用的一种方法*/
        p::after{
             /*通过伪元素添加的内容为行内元素*/
            content: '@';
        }
    </style>
</head>
<body>
    <p>好好看，好好学</p>
</body>
</html>
```

#### 1.4 通配符选择器

将html文档中的所有标签都选中，包括head中的标签，效率较低（不推荐使用）。

```css
*{
    color:red;
}
```

补充：

```css
/*css3中的一种高级选择器。取第一个span*/
span:nth-child(1){
    color:red;
}
```



### 2.常用排版格式

#### 2.1 字体属性

1. font-family  设置字体

   ```css
   /*从左向右解析，如果前面的字体系统没有安装，则一次向后查找*/
   body{
       font-family:'微软雅黑','宋体','楷体';
   }
   ```

2. font-size 设置字体大小

   ```css
   body{
       font-size:20px;
   }
   ```

   单位：

   - px 绝对单位，也叫固定单位

   - em 相对单位，相对于当前盒子的font-size。只需要动态改变字体大小，就可以实现响应式布局。

     ```css
     div{
         font-size:20px;
         width:10em; /*相当于200px*/
         height:10em;
     }
     ```

   - rem 相对单位，相对于根元素

     ```css
     /*font-size可以被继承，所以相对font-size容易设置*/
     html{
     	font-size:20px;
     }
     div{
        width:10rem; /*相当于200px*/
        height:10rem;
     }
     ```

3. font-style 字体样式

   - normal 普通字体
   - italic 斜体

   ```css
   div{
       font-style:italic;
   }
   ```

4. font-weight  设置字体的粗细 （范围100~900）

   - normal  普通字体     400
   - bold  加粗   700
   - bolder  更粗  900
   - lighter   比普通更细    100

   ```css
   div{
       font-weight:bold;
   }
   ```

#### 2.2 文本属性

1. text-decoration 

   - none 什么都不设置
   - underline 下划线
   - line-through 删除线
   - overline 上划线

   ```css
   div{
       text-decoration:underline;
   }
   ```

2. text-indent  设置缩进

   若字体大小为16px，则32px为缩进两个字符

   ```css
   div{
       font-size:16px;
       text-indent:32px;
   }
   
   .box{
   	font-size:16px;
       text-indent:2em;
   }
   ```

3. line-height 行高、行间距

   ```css
   div{
       line-height:20px;
   }
   ```

4. letter-spacing  文字间距（针对中文）

   ```css
   div{
       letter-spacing:2px;
   }
   ```

5. word-spacing   字母间距（针对英文）

   ```css
   div{
       word-spacing:2px;
   }
   ```

6. text-align 文本对齐

   - center居中
   - left 靠左
   - right 靠右

   ```css
   div{
       text-align:center;
   }
   ```

### 3.盒子模型补充

#### 3.1 边框 border

边框三要素：粗细，线性样式，颜色。

```css
/*整体设置4个方向的粗细，线性样式，颜色*/
div{
    border:1px,solid,red;
}
```

设置粗细 border-width

- border-left-width  设置左侧边框
- border-right-width
- border-top-width
- border-bottom-width

```css
div{
    border-width:2px 3px; /*设置上下，左右*/
    border-width:1px 3px 2px;/*设置上，左右，下边框*/
    border-width:1px,2px,3px,4px;/*设置上，右，下，左（顺时针）*/
}
```

设置线性样式  border-style

- border-left-style  设置左边框线性样式 
- 。。。

线性样式：

- solid 实线
- dotted 点状线
- dashed  虚线
- double 双线

```css
div{
    border-style:solid;
}
```

设置颜色  border-color

- border-left-color  设置左边框线性样式
- 。。。

```css
div{
    border-color: transparent;
}
```

transparent  透明色

#### 3.2 内边距 padding

- padding-left
- padding-right
- padding-top
- padding-bottom

#### 3.3 外边距 margin

1. 水平方向，两个盒子的距离为各自所设定的外边距之和。
2. 垂直方向，会出现外边距的合并（塌陷现象），以大的外边距值为基准。设置盒子内部的元素的外边距时，会改变父盒子的位置上。

```css
/*盒子水平居中*/
div{
    margin-left:auto;
    margin-right:auto;
}
```



### 4.布局-浮动 float

#### 4.1 文字环绕现象

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        img{
            width: 400px;

        }
        /*设置浮动属性后，盒子会浮动起来，传统的盒子会往上走补到浮动盒子的位置，但盒子内的文字不会被浮动盒子覆盖，产生环绕现象*/
        .box{
            float: left;
        }
        p{
            border: 1px solid red;
        }
    </style>
</head>
<body>

	<div class="box">
		<img src="https://i1.mifile.cn/a4/xmad_15590487108559_JOpcA.jpg" alt="">
	</div>
	<p>
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
            　央视网消息（新闻联播）：中共中央总书记、国家主席习近平28日上午在北京人民大会堂亲切会见出席第九届世界华侨华人社团联谊大会和中华海外联谊会五届一次理事大会的全体代表，代表党中央、国务院向大家表示热烈欢迎和衷心祝贺，向世界各地华侨华人致以诚挚问候。
	</p>
</body>
</html>
```

#### 4.2 浮动

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        div{
            width: 200px;
            height: 200px;

        }
        .father{
            width: 1200px;
            height: 200px;
        }
        .box1{
            background-color: red;
            float: left;
        }
        .box2{
            width: 500px;
            background-color: black;
            float: left;
        }
    </style>
</head>
<body>
    <div class="father">
        <div class="box1"></div>
        <div class="box2"></div>
    </div>

</body>
</html>
```

注：向右浮动的话，要从右开始数。



