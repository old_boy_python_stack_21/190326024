#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第38天作业
王凯旋
'''

#数据库创建表
'''
#创建数据库
create database school;

#切换数据库
use school;

#创建班级表
create table class(cid int,caption char(4));

#插入数据
insert into class values('1','三年二班');
insert into class values('2','一年三班');
insert into class values('3','三年一班');

#创建学生表
create table student(sid int,sname char(4),gender char(1),class_id int);

#插入数据
insert into student values(1,'钢弹','女',1);
insert into student values(2,'铁锤','女',1);
insert into student values(3,'山炮','男',2);

#创建老师表
create table teacher (tid int,tname char(2));

#插入数据
insert into teacher values(1,'波多');
insert into teacher values(2,'苍空');
insert into teacher values(3,'饭岛');

#创建课程表
create table course(cid int,cname char(2),teacher_id int);

#插入数据
insert into course values(1,'生物',1);
insert into course values(2,'体育',1);
insert into course values(3,'物理',2);

#创建成绩表
create table score(sid int,student_id int,course_id int,number int);

#插入数据
insert into score values(1,1,1,60);
insert into score values(2,1,2,59);
insert into score values(3,2,2,100);

'''