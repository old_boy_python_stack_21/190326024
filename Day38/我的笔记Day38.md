# Day38

## 今日内容

1. 表的创建方式—存储引擎
2. 基础数据类型
3. 表的约束

## 内容详细

### 1.存储引擎

#### 1.1 MyISAM

mysql 5.5及以下的版本的默认存储方式。

- 存储文件个数：表结构，表中的数据，索引
- 支持表级锁
- 不支持行级锁，不支持事务，不支持外键

#### 1.2  InnoDB

mysql 5.6及以上版本默认的存储方式

- 存储文件个数：表结构，表中的数据

- 支持行级锁和表级锁，默认为行级锁
- 支持事务，支持外键

#### 1.3 MEMORY

数据存储在内存中

- 存储的文件个数：表结构
- 优点：增删改查速度都很快
- 缺点：重启后数据消失，容量有限

#### 1.4 相关语句

1. 查看配置项

   ```sql
   show variables;--查看所有配置项
   show variables like '%engine%';--查看类似engine相关的配置项
   ```

2. 创建表

   ```sql
   --创建一个MyISAM存储方式的表
   create table student(id int,name char(4)) engine=MyISAM;
   ```

3. 查看表结构

   ```sql
   desc student;--查看这张表字段的基础信息
   show create table student;--查看表的所有相关信息，如引擎
   show create table student \g --查看表的相关信息，并格式化查询结果，结果更容易查看
   ```

#### 1.5  引擎总结

为什么要用innoDB？

- innoDB引擎支持事务，对含有支付业务的项目更安全。

- 支持行级锁能更好的处理并发问题。

### 2.数据类型

#### 2.1 数字类型

##### 2.1.1 整型 int

- 整型默认为有符号的

- 它所表示的数字的范围不被宽度约束，所以一般不会对int进行长度约束

- 它只能约束数字的显示宽度

  ```sql
  create table t1 (id1 int unsigned,id2 int); --创建t1表，id1字段为为无符号整型，id2为有符号整型
  ```

##### 2.1.2 小数 float

- 单精度小数

```sql
create table t1(f1 float(5,2)); --f1字段总长度为5，小数点后保留两位，自动四舍五入
```

#### 2.2 日期和时间类型

##### 2.2.1 年 year

- 只存储年份

  ```sql
  create table t3(y1 year);
  ```

##### 2.2.2 年月日 date

- 只包括年月日，存储格式为yyyy-mm-dd，可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

  ```sql
  create table t4(d1 date);
  insert into t4 values('2018-02-03');
  insert into t4 values(20180203);
  insert into t4 values(now()); --now()为一个函数，获取当前日期时间
  ```

##### 2.2.3 时分秒 time

- 只包括时分秒，存储格式为HH:mm:ss，可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

  ```sql
  create table t5(t1 time);
  insert into t5 values('12:12:54');
  insert into t5 values(121254);
  insert into t5 values(now());
  ```

##### 2.2.4 年月日时分秒 datetime

- 存储格式为yyyy-mm-dd HH:MM:SS,可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

  ```sql
  create table t6(dt1 datetime);
  insert into t6 values('2018-03-30 12:25:30');
  insert into t6 values(20180330122530);
  insert into t6 values(now());
  ```

##### 2.2.4 时间戳类型 timestamp

- 存储格式为yyyy-mm-dd HH:MM:SS,可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

- 默认值为 CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP，每次更新时，该类型的字段都会更新为当前时间。添加一条数据时，默认值也为当前时间

  ```sql
  create table t7(ts1 timestamp);
  insert into t7 values(null) --默认值为当前时间
  ```

#### 2.3 字符串类型

##### 2.3.1 定长的单位 char

- 定长的单位，不足会自动在后面补空格，查询时会把后面的空格去掉。如果插入时末尾本身就带有空格，也会被去掉。

- 浪费空间，存取效率相对较高。

  ```sql
  create table t8(c1 char(10));
  insert into t8 values('alex');
  ```

##### 2.3.2 变长的单位 varchar

- 节省空间，存取效率相对较低。

  ```sql
  create table t9(vc1 varchar(10));
  insert into t9 values('alex');
  ```

##### 2.3.3 总结

- 手机号、身份证号长度不变的，用char
- 用户名、密码等限制位数区间的也用char
- 对于评论，微博，说说等，使用varchar

#### 2.4 enum和set

##### 2.4.1 枚举 enum

- 单选，在表定义时，会定义好选择范围，插入数据时若数据不再范围内，则无法插入

  ```sql
  create table t10(gender enum('male','female'));
  insert into t10 values('male')
  ```

##### 2.4.2 集合 set

- 多选，在表定义时，会定义好选择范围，插入时会自动去重，以及去掉不存在的值

  ```sql
  create table t13 (hobby set('抽烟','喝酒','烫头'));
  insert into t13 values('抽烟,喝酒,洗脚,抽烟');
  
  ```

  











