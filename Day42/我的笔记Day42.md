# Day42

## 今日内容

1. HTML定义
2. 标签
3. 标签分类

## 内容详细

### 1.HTML定义

html语言是一种超文本标记语言。

html特征：

- 对换行和空格不敏感
- 对空白折叠

### 2.标签

标签叫做标记，有两种标签：

- 双闭合标签，如：`<html></html>`
- 单闭合标签，如：`<meta charset="utf-8">`

#### 2.1 head标签

head标签内的内容，对于用户来说是不可见的，它不会在网页中显示。

- meta标签，存放网站基本元信息。可以有多个meta标签。

  ```html
  <meta charset="utf-8">
  ```

- title 网站的标题

- link 链接css样式表

  ```html
  <link rel="stylesheet" href="css/my.css">
  ```

- script 链接js文件，或者内部写js代码

  ```html
  <script src="js/my.js"></script>
  ```

- style 直接将css样式写入html，内嵌样式

  ```html
  <style>
      .pwd{
          color:red;
      }
  </style>
  ```

#### 2.2 body标签

1. 标题标签

   h1~h6  一级标题~六级标题

2. 段落标签

   ```html
   <p>
       1234
   </p>
   ```

3. 超链接标签

   a标签    anchor锚点

   ```html
   <a href="" target=""></a>
   ```

   a标签的属性

   - href 要链接的地址

     - 为空时表示一个空连接，代表自身

     - `#top`代表回到顶部，我们可以定义一个空标签，来进行定位

       ```html
       <h6 id="top"></h6>
       <a href="#top">回到顶部</a>
       ```

   - target 在哪里打开页面

     - _self 当前页面（默认值）
     - _blank 新的空白页

   - title 鼠标悬浮在标签上时现实的文字

4. 图片标签

   `<img></img>`

   属性：

   - src 图片的地址

   - alt 图片加载失败时，显示的文字

   - 可以把图片包裹在a标签中，点击图片完成跳转

     ```html
     <a><img></img></a>
     ```

5. 换行标签

   `<br></br>`

6. 分隔线标签

   `<hr></hr>`

7. 加粗标签

   `<b></b>`    已废弃 

   `<strong></strong>`

8. 斜体标签

   `<em></em>`

   `<i></i>`   html5中的标签

9. 列表标签

   - 有序列表  ol (order list)  内部包裹li标签

     ```html
     <ol type="">
     	<li>111</li>
     	<li>222</li>
     	<li>333</li>
     	<li>444</li>
     	<li>555</li>
     </ol>
     ```
     有序列表属性type,改变序号种类

     - 默认为1，表示阿拉伯数字

   - 无序列表 ul (unorder list) 内部包裹li标签

     ```html
     <ul type="square">
     	<li>aaa</li>
     	<li>bbb</li>
     	<li>ccc</li>
     	<li>ddd</li>
     	<li>eee</li>
     </ul>
     ```

     有序列表属性type,改变列表项前端样式

     - circle 空心圆点
     - square 方块

10. 表格标签

    - table标签
      - tr标签，表示一行
        - th标签，表示表头，会加粗
        - td标签，表示表格数据

    ```html
    <table border="1px" cellspacing="0">
     	<tr>
     	    <th>id</th>
     	    <th>姓名</th>
     	    <th>年龄</th>
     	</tr>
     	<tr>
     	    <td>1</td>
     	    <td>alex</td>
     	    <td>18</td>
     	</tr>
     	<tr>
     	    <td>2</td>
     	    <td>wusir</td>
     	    <td>18</td>
     	</tr>
    </table>
    ```

    table属性：

    - border 设置表格边框
    - cellspacing 设置表格单元格间隔

11. 表单标签

    form标签用于和后台服务器进行交互数据。

    属性：

    - action属性，后台交互的服务器程序地址
    - method属性，提交数据的方式，一般为get或post
    - enctype属性，默认为application/x-www-form-urlencoded，当要上传文件时，需要修改为multipart/form-data

    表单内的标签：

    - input标签，输入框

      input的属性：

      - value属性，提交的数据的键值对中的值
      - name属性，提交的数据的键值对中的键
      - type属性，修改输入框的种类
        - text 文本输入框
        - password 密码输入框
        - submit 提交按钮，会提交表单。默认按钮上的文字为提交，可通过input的value属性修改
        - radio 单选框，几个单选框起相同的name，表示为一组，互斥。添加checked属性表示默认选中
        - checkbox 多选框，添加checked属性表示默认选中
        - datetime-local 日期框
        - file 上传文件选择框

    - label标签，和input输入框进行关联。label的for属性的值和input的id值相同，两个标签会产生关联，点击label标签，输入框也会聚焦

      ```html
      <form action="" method="" enctype="application/x-www-form-urlencoded">
          <label for="user">用户名</label>
          <input type="text" id="user" name="username">
      </form>
      ```

      

    - select标签，下拉列表，内部包裹option标签表示下拉选项。默认选中selected。可以通过设置multiple属性多选下拉列表。

      ```html
      <form action="" method="" enctype="application/x-www-form-urlencoded">
          <select name="gender" id="">
              <option value="male">男</option>
              <option value="female">女</option>
          </select>
      </form>
      ```

      

    - textarea标签，多行文本框

12. span标签，用来包裹文字。
13. div标签，盒子标签。把网页分割成不同的独立的逻辑区域。
14. &nbsp 表示一个空格
15. &copy

### 3.标签分类

- 行内标签
  - 行内标签：在一行内显示，不能设置宽高，默认是内容的宽高。
    - strong,b,i,em,span,lable,a
  - 行内块标签：在一行内显示，可以设置宽高。
    - img,input,td,th
- 块级标签:单独占一行，可以设置宽高。
  - h1~h6,p,ol,ul,li,table,tr,form

