#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第十九天作业
王凯旋
'''

# 1.简述编写类和执行类中方法的流程。
'''
class Base:
    def func(self):
        print(123)



首先实例化一个该类的对象 对象名 = 类名()
通过对象名调用方法  对象名.方法名()
'''

# 2.简述面向对象三大特性?
'''
封装性：1.对多个函数进行分类，放入不同的类中 
2. 对于函数重复调用的数据封装到对象中
继承性：对多个类来说重复使用的函数，可以放到基类中，避免重复编写
多态性：Python语言本身就体现了多态性（鸭子模型）
'''

# 3.将以下函数改成类的方式并调用 :
'''
def func(a1):   
    print(a1) 
'''

'''
class Base:
    def func(self,a1):
        print(a1)

b = Base()
b.func('alex')
'''

# 4.面向对象中的self指的是什么?
'''
调用该方法的对象
'''

# 5.以下代码体现 向对象的 么特点?
'''
class Person:
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender


obj = Person('武沛齐', 18, '男')
'''

'''
封装性，将数据封装到对象中
'''

# 6.以下代码体现 向对象的 么特点?
'''
class Message:
    def email(self):
        """
        发送邮件
        :return:
        """
        pass

    def msg(self):
        """
        发送短信
        :return:
        """
        pass

    def wechat(self):
        """
        发送微信
        :return:
        """
        pass
'''

'''
封装性，将多个函数封装到
'''

# 7.看代码写结果
'''
class Foo:
    def func(self):
        print('foo.func')

obj = Foo()
result = obj.func()
print(result)
'''

'''
结果：foo.func
     None
'''

# 8.定义个类，其中有计算周长和面积的方法(圆的半径通过参数传递到初始化方法)。
'''
import math
class Circle:
    def __init__(self,r):
        self.radius = r

    def calc_perimeter(self):
        return 2 * math.pi * self.radius

    def calc_area(self):
        return math.pi * self.radius * self.radius

obj = Circle(3)
print(obj.calc_area())
print(obj.calc_perimeter())
'''

# 9.面向对象中为什么要有继承?
'''
多个类中若有重复的方法，可以放到一个基类中，减少代码编写量
'''

# 10.Python继承时，查找成员的顺序遵循 么规则?
'''
首先在对象自己的类中查找，
自己类中找不到再去父类中查找
'''

# 11.看代码写结果
'''
class Base1:
    def f1(self):
        print('base1.f1')

    def f2(self):
        print('base1.f2')

    def f3(self):
        print('base1.f3')
        self.f1()


class Base2:
    def f1(self):
        print('base2.f1')


class Foo(Base1, Base2):
    def f0(self):
        print('foo.f0')
        self.f3()


obj = Foo()
obj.f0()
'''

'''
结果：foo.f0
     base1.f3
     base1.f1
'''

# 12.看代码写结果:
'''
class Base:
    def f1(self):
        print('base.f1')

    def f3(self):
        self.f1()
        print('base.f3')


class Foo(Base):
    def f1(self):
        print('foo.f1')

    def f2(self):
        print('foo.f2')
        self.f3()


obj = Foo()
obj.f2()
'''

'''
结果：foo.f2
     foo.f1
     base.f3
'''

# 13.补充代码实现
'''
# 需求
1. while循环提示 户输 : 户名、密码、邮箱(正则满 邮箱格式)
2. 为每个 户创建 个对象，并添加到 表中。
3. 当 表中的添加 3个对象后，跳出循环并以此循环打印所有 户的姓名和邮箱
'''
'''
import re
class User:
    def __init__(self,name,pwd,email):
        self.name = name
        self.pwd = pwd
        self.email = email

user_list = []
while True:
    user = input('请输入用户名:')
    pwd = input('请输入密码:')
    email = input('请输入邮箱:')
    ret = re.match('^[0-9a-zA-Z_]{0,19}@[0-9a-zA-Z]{1,13}\.[com,cn,net]{1,3}$',email)
    if not ret:
        print('邮箱格式不正确，请重新输入')
        continue
    obj = User(user,pwd,email)
    user_list.append(obj)
    if len(user_list) == 3:
        break
for item in user_list:
    print('用户名：%s,密码：%s,邮箱：%s'%(item.name,item.pwd,item.email,))
'''


# 14.补充代码:实现 户注册和登录。
'''
class User:
    def __init__(self, name, pwd):
        self.name = name
        self.pwd = pwd


class Account:
    def __init__(self):
        # 用户列表，数据格式：[user对象，user对象，user对象]
        self.user_list = []

    def login(self):
        """
        用户登录，输入用户名和密码然后去self.user_list中校验用户合法性
        :return:
        """
        while True:
            print('用户登录'.center(16,'*'))
            name = input('请输入用户名(输入N/n退出)：')
            if name.upper() == 'N':
                return
            pwd = input('请输入密码：')
            for item in self.user_list:
                if name == item.name and pwd == item.pwd:
                    print('登录成功')
                    return
            else:
                print('登录失败')
                continue

    def register(self):
        """
        用户注册，没注册一个用户就创建一个user对象，然后添加到self.user_list中，表示注册成功。
        :return:
        """
        while True:
            print('用户注册'.center(16,'*'))
            name = input('请输入用户名(输入N/n退出)：')
            if name.upper() == 'N':
                return
            pwd = input('请输入密码：')
            u = User(name,pwd)
            self.user_list.append(u)


    def run(self):
        """
        主程序
        :return:
        """
        dic = {'1': self.register,'2':self.login}
        while True:
            print('请选择'.center(15,'*'))
            print('1.用户注册\n2.用户登录')
            index = input('请输入（N/n退出）:')
            if index.upper() == 'N':
                return
            if not dic.get(index):
                print('输入有误，请重新输入')
                continue
            dic.get(index)()


if __name__ == '__main__':
    obj = Account()
    obj.run()
'''
