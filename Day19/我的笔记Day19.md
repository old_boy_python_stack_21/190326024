# Day19

## 今日内容

1. 面向对象基本用法
2. 面向对象好处和应用场景
3. 面向对象三大特性

## 内容详细

### 1.面向对象的基本形式

#### 1.1 基本格式

```python
#定义类
class Account:
    def func(self):
        print(123)

#调用类中的方法
a = Account() #创建（实例化）了一个Account类的对象
a.func() #使用对象调用类中的方法
```

应用场景：以后遇到很多函数，要对函数进行归类和划分（封装）

#### 1.2 对象的作用

self参数：哪个对象调用的方法，self就是哪个对象

对象的作用：存储一些值，供自己以后使用

```python
class Person:
    def func(self):
        print('我叫%s，今年%s岁，性别%s'%(self.name,self.age,self.gender))

p = Person()
p.name = 'alex'
p.age = 18
p.gender = '男'
p.func()

#__init__() 初始化方法（构造方法）
class Person:
    def __init__(self,name,age,gender): #可以加参数
        self.name = name
        self.age = age
        self.gender = gender
        
    def func(self):
        print('我叫%s，今年%s岁，性别%s'%(self.name,self.age,self.gender))
        
p = Person('张三',18,'男') #实例化时，会调用__init__()方法
p.func()
p1 = Person('李四',19,'男') 
p1.func()  #将数据放入__init__()方法中，可以更方便的为每个对象保存自己的数据
```

### 2.面向对象三大特性

#### 2.1 封装性

- 将函数归类放到一个类中
- 函数中有反复使用的值，可以放到对象中

#### 2.2 继承性

```python
#基类
class Base:
    def func1():
        print('f1')

#派生类
class Foo(Base):  
    def func2():
        print('f2')
        
f = Foo()
f.func2() #首先在对象自己的类中查找，找不到就去父类中查找
f.func1()
```

什么时候会用到继承？

- 多个类中有公共方法时，可以将它们放到基类中，避免重复编写。

继承关系中查找方法的顺序：

- self是到底是谁
- self是由哪个类创建的，就在哪个类中查找，找不到就去父类中查找

```python
# 示例一
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1() #base.f1
obj.f2() #foo.f2

# 示例二
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
        
obj = Foo() 
obj.f2() #base.f1  foo.f2

# 示例三
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
	def f1(self):
        print('foo.f1')
        
obj = Foo()
obj.f2()#foo.f1  foo.f2

# 示例四
class Base:
    def f1(self):
        self.f2()
        print('base.f1')
	def f2(self):
        print('base.f2')
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1() #foo.f2  base.f1
```

对于多继承，从左到右依次查找

```python
class BaseServer:
    def serve_forever(self, poll_interval=0.5):
        self._handle_request_noblock()
	def _handle_request_noblock(self):
        self.process_request(request, client_address)
        
	def process_request(self, request, client_address):
        pass
    
class TCPServer(BaseServer):
    pass

class ThreadingMixIn:
    def process_request(self, request, client_address):
        pass
    
class ThreadingTCPServer(ThreadingMixIn, TCPServer): 
    pass

obj = ThreadingTCPServer()
obj.serve_forever()
```

#### 2.3 多态性（鸭子模型）

```python
def func(arg):
    v = arg[-1]
    print(v)
```

面试题：什么是鸭子模型

答：对于函数而言，Python对参数的类型不会限制，那么在传入参数时就可以是任意类型，在函数中如果有如：arg[-1],那么就是对传入类型的限制（类型必须有索引）。这就是鸭子模型，类似于上述函数，我们认为只要是能嘎嘎叫的就是鸭子（只要有索引就是我们要的参数类型）

## 总结

1. 面向对象三大特性
   - 封装
   - 继承
   - 多态

2. 面向对象的格式和关键字
   - 类
   - 对象
   - 方法

3. 什么时候用面向对象
   - 业务比较复杂，可以用面向对象进行分类
   - 想要封装有限个数据