# Day20

## 今日内容

1. 成员
2. 成员修饰符

## 内容详细

### 1.成员

#### 1.1 类变量 和 实例变量(属于对象)

写在类中与方法同级别，可以通过类名.变量，对象.变量调用

```python
class Base:
    x = 1

obj = Base()
print(obj.name) #打印实例变量
print(Base.name) #报错，类名无法访问实例变量
print(obj.x) #对象调用类变量
print(Base.x) #类名调用类变量
```

只能修改或设置自己的值，不能修改其他值

```python
class Base:
    x = 1

obj = Base()
obj.y = 123 #为对象obj添加了一个y变量
obj.x = 666 #为对象obj添加了一个x变量
print(Base.x) #1
Base.x = 666 #修改类变量x
print(Base.x) #666
```

```python
class Base:
    x = 1
    
class Foo(Base):
    pass
print(Base.x)#1
print(Foo.x)#1
Base.x = 666
print(Base.x)#666
print(Foo.x)#666
Foo.x = 999
print(Base.x)#666
print(Foo.x)#999
```

#### 1.2 绑定方法

定义：至少有一个self参数

调用：先创建对象，由对象调用

```python
class Base:
    def func(self):
        pass
obj = Base()
obj.func()
```

#### 1.3 静态方法

定义：

- 由装饰器@staticmethod装饰
- 参数任意

调用：

- 可以由类名或对象名（不推荐）调用

```python
class Base:
    @staticmethod
    def func():
		pass
Base.func()
```

#### 1.4 类方法

定义：

- 由装饰器@classmethod装饰
- 至少一个参数

调用：

- 可以由类名或对象名（不推荐）调用

```python
class Base:
    @classmetnod
    def func(cls): #cls类名
        pass
Base.func()
```

面试题：@staticmethod和@classmethod的区别？

一个是静态方法，一个是类方法

定义：

- 类方法：用@classmethod做装饰器，且至少一个参数
- 静态方法：用@staticmethod做装饰器，且参数无限制

调用：

- 类名调用，对象也可调用。

#### 1.5 属性

定义：

- 用@property装饰器
- 只有一个self参数

调用：

- 对象.func   不加括号

```python
class Base:
    @property
    def func(self):
        return 123
 
obj = Base()
obj.func
```

### 2.成员修饰符

公有：任何地方都可以访问

私有：只有类内部可以访问

```python
class Foo:
    def __init__(self,name):
        self.__name = name #__name为私有实例变量
    def func(self):
        print(self.__name)
        
obj = Foo('alex')
obj.__name #外部访问不到（继承也无法访问）
obj.func() #内部可以访问
```

可以通过obj._Foo.__x强制访问

### 3.小补充

py2和py3的区别

```python
class Foo:
	pass

class Foo(object):
	pass
# 在python3中这俩的写法是一样，因为所有的类默认都会继承object类，全部都是新式类。
# 如果在python2中这样定义，则称其为：经典类
class Foo:
	pass
# 如果在python2中这样定义，则称其为：新式类
class Foo(object):
	pass
```

## 总结

1. 数据封装
2. 继承关系的查找
3. 成员
   - 类成员
     - 类变量
     - 绑定方法
     - 静态方法
     - 类方法
     - 属性
   - 对象成员
     - 实例变量





#### 



