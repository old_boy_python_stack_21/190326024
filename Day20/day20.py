#!/usr/bin/env python
# -*- coding:utf-8 -*-
'''
第二十天作业
王凯旋
'''

# 1.简述面向对象三大特性并用代码表示。
'''
#封装：将同一类的函数封装到一个类中，将反复使用的数据封装到对象中
class File:
    def __init__(self,path):
        self.path = path

    def read(self):
        print(self.path)

    def write(self):
        print(self.path)

obj = File('123')
obj.read()

#继承：将重复的功能放到基类中，减少代码编写量
class Animal:
    def eat(self):
        pass
class Dog(Animal):
    def call(self):
        pass

obj = Dog()
obj.eat()

#多态：鸭子模型
def func(arg):
    v = arg.read()
    print(v)
'''

# 2.什么是鸭子模型？
'''
def func(arg):
    v = arg.read()
    print(v)

#对于func函数，python没有限制参数的类型，可以传入任意类型，但这个参数必须拥有read方法。这就是鸭子模型，我们认为只要是会
#嘎嘎叫的就是鸭子（只要有read方法就是我们要的参数类型）
'''

# 3.列举面向对象中的类成员和对象成员。
'''
类成员：
类变量
绑定方法
静态方法
类方法
属性
对象成员：
实例变量
'''

# 4.@classmethod和@staticmethod的区别?
'''
一个是类方法，一个是静态方法
定义：
类方法：用@classmethod装饰器装饰，且至少有一个参数
静态方法：用@staticmethod装饰器装饰，参数任意
调用：
用类名调用，也可以用对象调用
'''

# 5.Python中双下滑 __ 有什么作用？
'''
定义私有成员
'''

# 6.看代码写结果
'''
class Base:
    x = 1


obj = Base()

print(obj.x)
obj.y = 123
print(obj.y)
obj.x = 123
print(obj.x)
print(Base.x)
'''

'''
结果：1 123 123 1
'''

# 7.看代码写结果
'''
class Parent:
    x = 1


class Child1(Parent):
    pass


class Child2(Parent):
    pass


print(Parent.x, Child1.x, Child2.x)
Child2.x = 2
print(Parent.x, Child1.x, Child2.x)
Child1.x = 3
print(Parent.x, Child1.x, Child2.x)
'''

'''
结果：1 1 1
     1 1 2
     1 3 2
'''

# 8.看代码写结果
'''
class Foo(object):
    n1 = '武沛齐'
    n2 = '金老板'
    def __init__(self):
        self.n1 = '女神'

obj = Foo()
print(obj.n1)
print(obj.n2)
'''

'''
结果：女神 金老板
'''

# 9.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''


class Foo(object):
    n1 = '武沛齐'

    def __init__(self, name):
        self.n2 = name


obj = Foo('太白')
print(obj.n1)  # 武沛齐
print(obj.n2)  # 太白

print(Foo.n1)  # 武沛齐
print(Foo.n2)  # 报错
'''

# 10.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):
    a1 = 1
    __a2 = 2

    def __init__(self, num):
        self.num = num
        self.__salary = 1000

    def show_data(self):
        print(self.num + self.a1)


obj = Foo(666)

print(obj.num) #666
print(obj.a1) #1
print(obj.__salary) #报错
print(obj.__a2) #报错
print(Foo.a1) #1
print(Foo.__a2) #报错
'''

# 11.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):
    a1 = 1

    def __init__(self, num):
        self.num = num

    def show_data(self):
        print(self.num + self.a1)


obj1 = Foo(666)
obj2 = Foo(999)
print(obj1.num) #666
print(obj1.a1) #1

obj1.num = 18
obj1.a1 = 99

print(obj1.num) #18
print(obj1.a1) #99

print(obj2.a1) #1
print(obj2.num) #999
print(obj2.num) #999
print(Foo.a1) #1
print(obj1.a1) #99
'''

# 12.看代码写结果，注意返回值。
'''
class Foo(object):

    def f1(self):
        return 999

    def f2(self):
        v = self.f1()
        print('f2')
        return v

    def f3(self):
        print('f3')
        return self.f2()

    def run(self):
        result = self.f3()
        print(result)


obj = Foo()
v1 = obj.run()
print(v1)
'''

'''
结果：
f3
f2
999
None
'''

# 13.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):

    def f1(self):
        print('f1')

    @staticmethod
    def f2():
        print('f2')


obj = Foo()
obj.f1() #f1
obj.f2() #f2

Foo.f1() #报错
Foo.f2() #f2
'''

# 14.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):

    def f1(self):
        print('f1')

    @classmethod
    def f2(cls):
        print('f2')


obj = Foo()
obj.f1() #f1
obj.f2() #f2

Foo.f1() #报错
Foo.f2() #f2
'''

# 15.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Foo(object):

    def f1(self):
        print('f1')
        self.f2()
        self.f3()

    @classmethod
    def f2(cls):
        print('f2')

    @staticmethod
    def f3():
        print('f3')


obj = Foo()
obj.f1()
'''

'''
结果：
f1
f2
f3
'''

# 16.看代码写结果【如果有错误，则标注错误即可，并且假设程序报错可以继续执行】
'''
class Base(object):
    @classmethod
    def f2(cls):
          print('f2')

    @staticmethod
    def f3():
          print('f3')

class Foo(object):
    def f1(self):
        print('f1')
        self.f2()
        self.f3()

obj = Foo()
obj.f1()
'''

'''
结果：
f1
报错
'''

# 17.看代码写结果
'''
class Foo(object):
    def __init__(self, num):
        self.num = num


v1 = [Foo for i in range(10)]
v2 = [Foo(5) for i in range(10)]
v3 = [Foo(i) for i in range(10)]

print(v1) #[Foo,Foo,Foo,Foo,Foo,Foo,Foo,Foo,Foo,Foo]
print(v2) #列表中10个Foo对象
print(v3) #列表中10个Foo对象
'''

# 18.看代码写结果
'''
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    print(item.num)
'''

'''
1
2
3
'''

# 19.看代码写结果
'''
class StarkConfig(object):

    def __init__(self, num):
        self.num = num

    def changelist(self, request):
        print(self.num, request)


config_obj_list = [StarkConfig(1), StarkConfig(2), StarkConfig(3)]
for item in config_obj_list:
    item.changelist(666)
'''

'''
结果：
1 666
2 666
3 666
'''

# 20.看代码写结果
'''
class Department(object):
    def __init__(self, title):
        self.title = title


class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p3 = Person('安安', 19, d2)

print(p1.name) #武沛齐
print(p2.age) #18
print(p3.depart) # d2对象
print(p3.depart.title) #销售部
'''

# 21.看代码写结果
'''
class Department(object):
    def __init__(self, title):
        self.title = title


class Person(object):
    def __init__(self, name, age, depart):
        self.name = name
        self.age = age
        self.depart = depart

    def message(self):
        msg = "我是%s,年龄%s,属于%s" % (self.name, self.age, self.depart.title)
        print(msg)


d1 = Department('人事部')
d2 = Department('销售部')

p1 = Person('武沛齐', 18, d1)
p2 = Person('alex', 18, d1)
p1.message() #我是武沛齐，年龄18，属于人事部
p2.message() #我是alex，年龄18，属于人事部
'''

# 22.编写类完成以下的嵌套关系
'''
角色：学校、课程、班级
要求：
	1. 创建北京、上海、深圳三所学校。
	2. 创建课程
		北京有三种课程：Linux、Python、Go
		上海有两种课程：Linux、Python
		深圳有一种课程：Python
	3. 创建班级(班级包含：班级名称、开班时间、结课时间、班级人数)
		北京Python开设：21期、22期
		北京Linux开设：2期、3期
		北京Go开设：1期、2期
		上海Python开设：1期、2期
		上海Linux开设：2期
		深圳Python开设：1期、2期
'''

'''


class School:
    def __init__(self, name,courses):
        self.name = name
        self.courses = courses


class Course:
    def __init__(self, name,classrooms):
        self.name = name
        self.classrooms = classrooms


class ClassRoom:
    def __init__(self, name, start_date, end_date, person_count):
        self.name = name
        self.start_date = start_date
        self.end_date = end_date
        self.person_count = person_count

obj1 = ClassRoom('21期','123','123',50)
obj2 = ClassRoom('22期','123','123',51)
obj3 = ClassRoom('2期','123','123',55)
obj4 = ClassRoom('3期','123','123',52)
obj5 = ClassRoom('1期','123','123',56)
obj6 = ClassRoom('2期','123','123',57)
obj7 = ClassRoom('1期','123','123',53)
obj8 = ClassRoom('2期','123','123',54)
obj9 = ClassRoom('2期','123','123',60)
obj10 = ClassRoom('1期','123','123',60)
obj11 = ClassRoom('2期','123','123',61)

beijin = School('北京',[Course('Python',[obj1,obj2]),Course('Linux',[obj3,obj4]),Course('Go',[obj5,obj6])])
shanghai = School('上海',[Course('Python',[obj7,obj8]),Course('Linux',[obj9])])
shenzhen = School('深圳',[Course('Python',[obj10,obj11])])

dic = {'1':beijin,'2':shanghai,'3':shenzhen}
def run():
    while True:
        print('选课系统'.center(16,'*'))
        for k,v in dic.items():
            print('%s.%s'%(k,v.name,))
        index = input('请选择校区(N退出)：')
        if index.upper() == 'N':
            return
        select_course(dic.get(index).courses)
def select_course(arg):
    while True:
        print('课程选择'.center(16,'*'))
        for i in range(len(arg)):
            print('%s.%s'%(i+1,arg[i].name,))
        index = input('请选择课程(N退出)：')
        if index.upper() == 'N':
            return
        select_class(arg[int(index)-1].classrooms)

def select_class(arg):
    while True:
        print('班级选择'.center(16, '*'))
        for i in range(len(arg)):
            print('%s.%s' % (i + 1, arg[i].name,))
        index = input('请选择班级(N退出)：')
        if index.upper() == 'N':
            return
        num = int(index) - 1
        print('班级名称：%s,开课时间：%s,结课时间%s,人数：%s'%(arg[num].name,arg[num].start_date,arg[num].end_date,arg[num].person_count))

run()
'''
