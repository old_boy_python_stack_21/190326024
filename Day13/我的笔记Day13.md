# Day13

## 今日内容

1. 装饰器
2. 推导式

## 内容详细

### 1.装饰器

#### 1.1 原理示例

```python
def func():
    print(1)
    
def bar():
    print(2)
    
bar = func
bar()  #将func函数的地址赋值给了bar  结果：1
```

```python
def func(arg):
    def inner():
        print(arg)
    return inner
v1 = func(1)
v2 = func(2)
v1() #1
v2() #2
```

```python
def func(arg):
    def inner():
        return arg()
    return inner
def index():
    print(123)
    return 666
v1 = func(index) #将inner函数地址赋给v1
result = v1() #执行inner函数，内部执行index函数，打印123，并将返回值赋给result
print(result)
```

```python
def func(arg):
    def inner():
        return arg()
    return inner
def index():
    print(123)
    return 666
index = func(index) #将inner函数地址赋给index,此时index变量不再指向原来函数地址
result = index() #执行index也就是inner函数，由于闭包，内部参数依旧执行index原地址，打印123，并将返回值赋给result
print(result) #666
```

```python
def func(arg):
    def inner():
        print('before')
        v = arg()
        print('after')
        return v
    return inner
def index():
    print(123)
    return 666
index = func(index) #将inner函数地址赋给index,此时index变量不再指向原来函数地址
result = index() #执行index也就是inner函数，由于闭包，内部参数依旧执行index原地址，先打印before,再执行原函数打印123，将返回值赋给v，然后打印after，最后返回v
print(result) #666
```

```python
def func(arg):
    def inner():
        print('before')
        v = arg()
        print('after')
        return v
    return inner

@func #第一步将index函数作为参数传入func函数 v = func(index)。第二部将func函数返回值赋值给index index = v
def index():
    print(123)
    return 666

index() 
```



#### 1.2 装饰器目的

在不改变原函数内部代码的情况下，在函数执行前和函数执行后自定义一些功能。

#### 1.3 装饰器应用场景

- 大多数函数要增加相同的功能

#### 1.3 装饰器格式

```python
# 装饰器的编写
def func(arg):
    def inner(*args,**kwargs):
        return arg(*args,**kwargs)
    return inner

# 装饰器的使用
@func
def index():
    print(123)
    
@func
def base(a):
    return a+100        
```

### 2.推导式

#### 2.1 列表推导式

目的：更加便捷的生成一个列表

```python
v1 = [i for i in 'alex'] #['a','l','e','x']
v2 = [66 if i > 5 else 77 for i in range(10)] #[77,77,77,77,77,77,66,66,66,66]
v3 = [i for i in range(10) if i > 5] #[6,7,8,9]  条件成立添加到列表
```

基本格式

```python
v1 = [i for i in 可迭代对象]
v2 = [i for i in 可迭代对象 if 条件] #条件成立添加到列表中
```

面试题

```python
def num():
    return [lambda x:x*i for i in range(4)]
print([m(2) for m in num()]) #[6,6,6,6]
```



#### 2.2 集合推导式

基本格式

```python
v1 = {i for i in 可迭代对象}
v2 = {i for i in 可迭代对象 if 条件} #条件成立添加到列表中
```

#### 2.3 字典推导式

基本格式

```python
v1 = {'k'+str(i):i for i in 可迭代对象}
v2 = {'k'+str(i):i for i in 可迭代对象 if 条件} #条件成立添加到列表中
```

### 3.模块

#### 3.1 time模块

```python
import time
v = time.time() #获取时间戳（秒）
time.sleep(2) #休眠2秒
```



## 总结

1. 装饰器
   - 编写格式
   - 使用格式
2. 推导式
   - 列表推导式
   - 集合推导式
   - 字典推导式
3. time模块
   - time.time()
   - time.sleep(2)