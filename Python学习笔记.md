# Python学习笔记

## 一、计算机基础

### 1.1 硬件

​	主板、CPU、硬盘、显卡、网卡、内存...

### 1.2 操作系统

- windows 图形界面好
  - window 7
  - window 10
- linux  开源免费
  - centos(公司线上使用版本)
  - redhat
  - ubuntu
- mac 办公方便

### 1.3 解释器和编译器

语言开发者编写的一种软件，将用户写的代码转换成二进制，交给计算机执行

#### 1.3.1 解释型语言和编译型语言的区别

- 解释型语言：将编写好的代码交给解释器，解释器解释一句就执行语句。如：Python、PHP
- 编译型语言：将编写好的代码交给编译器编译成新的文件，然后交给计算机执行。如：C/C++、Java

### 1.4 软件

应用程序

### 1.5 进制

- 二进制：文件存储，网络传输都是二进制
- 八进制：
- 十进制：人来进行使用一般情况下计算机可以获取10进制，然后再内部会自动转换成二进制并操作。
- 十六进制：多用来表示复杂的东西。\x

## 二、Python入门

### 2.1 环境

#### 2.1.1 解释器环境

​	python2、python3

#### 2.1.2 代码开发工具（IDE）

​	PyCharm

### 2.2 编码

- ascii:一个字节表示一个字符
- unicode：万国码可以表示所有字符
  - ecs2:2个字节表示一个字符
  - ecs4:4个字节表示一个字符
- utf-8:Unicode的压缩版，3个字节表示一个中文字符
- gbk:亚洲字符编码，两个字节表示一个字符
- gb2312:2个字节表示一个字符

### 2.3 变量

2.3.1 为什么要定义变量？

​	方便以后代码调用

2.3.2 变量命名规范

- 只能包括数字、字母以及下划线。
- 不能以数字开头
- 不能是python的关键字
- 建议：见名知意，多个单词用下划线隔开

### 2.4 条件判断

```python
if 1>2:
    print()
elif 1<2:
    print()
else:
	print()
```

### 2.5 while循环

```python
while True:
    print()
```

- while
- break:结束当前循环
- continue:结束本次循环，回到循环条件判断处
- pass:跳过
- while  else:当while的条件不满足时，进入else。break掉的循环不会进入else。

### 2.6 运算符

- 算数运算符

  ```python
  +,-,*,/,%,**,//
  ```

- 比较运算符

  ```python
  ==,>=,<=,!=,>,<
  ```

- 赋值运算符

  ```python
  =,+=,-=,*=,/=,**=,//=,%=
  ```

- 逻辑运算符

  - not

  - and

    ```python
    #当第一值为假，取第一值
    #当第一值为真，取第二值
    #有多个判断时，按照上述规则，从左到右依次比较
    1 and 2
    ```

  - or

    ```python
    #当第一值为假，取第二值
    #当第一值为真，取第一值
    #有多个判断时，按照上述规则，从左到右依次比较
    1 or 2
    ```

  优先级：not>and>or

- 成员运算符

  - in 
  - not in

### 2.7 注释

```python
#单行注释

'''
多行注释
'''
```

### 2.8 单位

```python 
1 byte = 8 bit
1 kb = 1024 byte
1 Mb = 1024 kb
...
```

### 2.9 range()

```python
#产生一组数字（高仿列表）
value = range(0,9) #不包括9
```



### 2.10 py2和py3的区别

- 类型

  - py2:unicode类型（以unicode编码），str类型（其他编码）	

    ```python
    v = u"alex" #定义了unicode类型的变量
    v2 = "alex" #定义str类型的变量
    ```

  - py3：str类型(unicode编码) ，bytes类型（其他编码）

    ```python
    v1 = 'alex' #定义了一个str类型的变量，在内存中unicode编码
    v2 = b'alex' #定义了一个bytes类型变量
    ```

- 字典的keys,values,items

  - py2：返回列表
  - py3：返回迭代器

- map和filter

  - py2：返回列表
  - py3：返回迭代器

- 默认解释器编码

  - py2:ascii
  - py3:utf-8

- 输入
  - py2: raw_input()
  - py3: input()

- 输出
  - py2: print ""
  - py3: print()

- 整数相除
  - py2: 只保留整数位
  - py3: 全部保留

- 整型
  - py2: int/long
  - py3:只有int

- range()和xrange()
  - py2
    - range()  立即在内存中生成所有元素
    - xrange()  得到一个生成器，在循环的时候边循环边创建，节省内存
  - py3
    - range() 得到一个生成器，在循环的时候边循环边创建，节省内存

- 创建包
  - py2:文件夹下面必须有`__init__.py`文件才能算包
  - py3:不必有`__init__.py`文件

- 内置函数变化，如
  - py2：reduce是内置函数
  - py3：reduce被放入functools模块中，需导入模块后使用

- 类定义

  ```python
  class Foo:
  	pass
  
  class Foo(object):
  	pass
  # 在python3中这俩的写法是一样，因为所有的类默认都会继承object类，全部都是新式类。
  # 如果在python2中这样定义，则称其为：经典类
  class Foo:
  	pass
  # 如果在python2中这样定义，则称其为：新式类
  class Foo(object):
  	pass
  ```

  

### 2.11 三元运算符（三目运算符）

前面 if 表达式 else 后面

```python
#表达式为True取前面的值，否则取后面的值
v =  a if a > b else b
```

### 2.12 推导式

#### 2.12.1 列表推导式

目的：更加便捷的生成一个列表

```python
v1 = [i for i in 'alex'] #['a','l','e','x']
v2 = [66 if i > 5 else 77 for i in range(10)] #[77,77,77,77,77,77,66,66,66,66]
v3 = [i for i in range(10) if i > 5] #[6,7,8,9]  条件成立添加到列表
```

基本格式

```python
v1 = [i for i in 可迭代对象]
v2 = [i for i in 可迭代对象 if 条件] #条件成立添加到列表中
```

面试题

```python
def num():
    return [lambda x:x*i for i in range(4)]
print([m(2) for m in num()]) #[6,6,6,6]
```

#### 2.12.2 集合推导式

基本格式

```python
v1 = {i for i in 可迭代对象}
v2 = {i for i in 可迭代对象 if 条件} #条件成立添加到列表中
```

#### 2.12.3 字典推导式

基本格式

```python
v1 = {'k'+str(i):i for i in 可迭代对象}
v2 = {'k'+str(i):i for i in 可迭代对象 if 条件} #条件成立添加到列表中
```

#### 2.12.4 生成器推导式

```python
#基本格式
v = (i for i in range(10))
for item in v:
    print(v)
    
# 示例一
# def func():
#     result = []
#     for i in range(10):
#         result.append(i)
#     return result
# v1 = func()
# for item in v1:
#    print(item)

# 示例二
# def func():
#     for i in range(10):
#         def f():
#             return i
#         yield f
#
# v1 = func()
# for item in v1:
#     print(item())

# 示例三：
v1 = [i for i in range(10)] # 列表推导式，立即循环创建所有元素。
v2 = (lambda :i for i in range(10))
for item in v2:
    print(item())
```

面试题：请比较 [i for i in range(10)] 和 (i for i in range(10)) 的区别？

## 三、数据类型

### 3.1 整型（int）

### 3.2 布尔类型（bool）

0, "", [], (), {}, set(), None转换成为布尔值时为False,其他全部为True.

### 3.3  字符串（str）

- upper() 转大写

  ```python
  #upper()转换大写
  v1 = "alex"
  v2 = v1.upper()
  print(v2) #ALEX
  ```

- lower() 转小写

  ```python
  #lower()转换小写
  v1 = "ALEX"
  v2 = v1.upper()
  print(v2)  #alex
  ```

- isdecimal() 是否为十进制数

  ```python
  v1 = "12345"
  print(v1.isdecimal()) #True
  ```

- strip() 去空格

  ```python
  v1 = " alex "
  print(v1.strip()) #alex
  ```

- replace() 替换

  ```python
  v1 = "aaaa111"
  print(v1.replace("111","bbb")) #aaaabbb
  ```

- split() 分割

  ```python
  v1 = "aaaa|bbbb"
  print(v1.split("|")) #['aaaa', 'bbbb']
  ```

- format() 格式化

  ```python
  v1 = "他叫{0}，今年{1}岁"
  print(v1.format("张三",18)) #他叫张三，今年18岁
  ```

- startswith() 以...开头

  ```python
  #startswith()
  v1 = "abc"
  print(v1.startswith("a"))#True
  ```

- endswith()  以...结尾

  ```python
  #endswith()
  v1 = "abc"
  print(v1.endswith("a"))#False
  ```

- encode()  编码

  ```python
  v1 = "你好"
  print(v1.encode("utf-8"))# b'\xe4\xbd\xa0\xe5\xa5\xbd'
  ```

- join() 连接

  ```python
  v1 = "你好"
  print("_".join(v1))# 你_好
  ```

- 字符串格式化

  ```python
  v = '姓名是：%(name)s,年龄：%(age)s'%{'name':'alex','age':19}
  print(v) #姓名是：alex,年龄：19
  
  v = '姓名是：{0},年龄：{1}'.format('alex',19)
  print(v) #姓名是：alex,年龄：19
  
  v = '姓名是：{name},年龄：{age}'.format(name='alex',age=19)
  print(v) #姓名是：alex,年龄：19
  ```

### 3.4 列表（list）

- append()  追加

  ```python
  v1 = ["111","222","333","444","555"]
  v1.append("666")
  print(v1)# ['111', '222', '333', '444', '555', '666']
  ```

- insert() 插入

  ```python
  v1 = ["111","222","333","444","555"]
  v1.insert(1,"666")
  print(v1)# ['111', '666', '222', '333', '444', '555']
  ```

- extend()  扩展

  ```python
  v1 = ["111","222","333","444","555"]
  v1.extend(['777','888'])
  print(v1) #['111', '222', '333', '444', '555', '777', '888']
  ```

- pop() 删除

  ```python
  v1 = ["111","222","333","444","555"]
  v1.pop(1)
  print(v1) #['111', '333', '444', '555']
  ```

- remove() 移除

  ```python
  v1 = ["111","222","333","444","555"]
  v1.remove("333")
  print(v1)#['111', '222', '444', '555']
  ```

- clear() 清空

  ```python
  v1 = ["111","222","333","444","555"]
  v1.clear()
  print(v1) #[]
  ```

- reverse() 反转

  ```python
  v1 = ["111","222","333","444","555"]
  v1.reverse()
  print(v1)#['555', '444', '333', '222', '111']
  ```

- sort() 排序

  ```python
  v1 = [1,3,2,6,4]
  v1.sort(reverse=False)
  print(v1)#[1, 2, 3, 4, 6]
  ```

### 3.5 元组（tuple）

​	无

### 3.6 字典（dict）

- keys() 获取所有键

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.keys():
      print(i)
  #name
  #age
  ```

- values() 获取所有值

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.values():
      print(i)
  #alex
  #18
  ```

- items() 获取所有键值对

  ```python
  v1 = {"name":"alex","age":18}
  for i in v1.items():
      print(i)
  
  #('name', 'alex')
  #('age', 18)
  ```

- get() 根据键获取值

  ```python
  v1 = {"name":"alex","age":18}
  print(v1.get("name"))#alex
  ```

- update() 更新

  ```python
  v1 = {"name":"alex","age":18}
  v1.update({"gender":"男"})
  print(v1)# {'name': 'alex', 'age': 18, 'gender': '男'}
  ```

- pop() 根据键删除

  ```python
  v1 = {"name":"alex","age":18}
  v1.pop("name")
  print(v1)# {'age': 18}
  ```

- 有序字典

  ```python
  from collections import OrderedDict
  info = OrderedDict()
  info['k1'] = 1
  info['k2'] = 2
  ```

### 3.7 集合（set）

集合元素为可哈希类型（不可变），但集合本身不可哈希。

- add() 添加

  ```python
  v1 = {1,2,3,4}
  v1.add(5)
  print(v1)# {1, 2, 3, 4, 5}
  ```

- discard() 删除

  ```python
  v1 = {1,2,3,4}
  v1.discard(4)
  print(v1)#{1, 2, 3}
  ```

- update() 批量添加

  ```python
  v1 = {1,2,3,4}
  v1.update((6,7,8))
  print(v1)#{1, 2, 3, 4, 6, 7, 8}
  ```

- intersection() 交集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.intersection(v2)
  print(v3)#{3, 4}
  ```

- union() 并集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.union(v2)
  print(v3)#{1, 2, 3, 4, 5, 6}
  ```

- difference() 差集

  ```python
  v1 = {1,2,3,4}
  v2 = {3,4,5,6}
  v3 = v1.difference(v2)
  print(v3)# {1, 2}
  ```

### 3.8 公共功能

- len() 获取长度

  - str
  - list
  - tuple
  - dict
  - set

- 索引

  - str
  - list
  - tuple

- 切片

  - str
  - list
  - tuple

- 步长

  - str
  - list
  - tuple

- for循环

  - str
  - list
  - tuple
  - dict
  - set

- 删除

  ```python
  del v[1]
  ```

  - list
  - dict (根据键删除)

- 修改

  - list
  - dict (根据键删除)

### 3.9 内存及深浅拷贝

#### 3.9.1 内存

- 为变量重新赋值时，计算机会为变量重新开辟内存。
- 修改变量内部元素时，不会重新开辟内存。

python的小数据池：

- 常用数字：-5~256，重新赋值时，不会开辟内存
- 字符串：只有带特殊字符，且*num，num>1，重新赋值会开辟内存

#### 3.9.2 深浅拷贝

```python
import copy
copy.copy() #浅拷贝 只拷贝第一层，里面元素地址相同
copy.deepcopy() #深拷贝 寻找所有的可变类型，拷贝
```

- 对于int，bool，str深浅拷贝相同
- 对于list，dict，set
  - 当无嵌套时，深浅拷贝相同。最外层地址不同
  - 当有嵌套时，深拷贝的内部嵌套地址不再相同

- 对于元组
  - 无嵌套，深浅拷贝元组的最外层地址相同
  - 当嵌套有可变类型时，深拷贝的地址不再相同

#### 3.9.3 type()

获取变量的类型

#### 3.9.4 id()

获取变量的内存地址

#### 3.9.5 == 和 is

- == 比较两个变量的值是否相等
- is 比较两个变量的内存是否相同

## 四、文件操作

### 4.1 文件操作步骤

- 打开文件

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  ```

- 操作文件

  ```python
  f.read()
  ```

- 关闭文件

  ```python
  f.close()
  ```

### 4.2 文件打开模式

读和写都会在光标位置开始。

- r/w/a
  - r：只读，文件不存在报错
  - w：只写，文件不存在会创建文件，在文件打开时会清空文件
  - a：追加，文件不存在会创建
- r+/w+/a+
  - r+：可读可写，打开文件光标在文件开头，写入会覆盖光标后面的字符。
  - w+：可读可写，打开文件时会清空文件，可通过移动光标来读取。
  - a+：可读可写，打开文件时光标在文件末尾，可通过移动光标读取，但只要写入光标就会移动到末尾。
- rb/wb/ab ：以二进制打开，不能指定编码，否则报错。多用于图片、视频、音频的读取。
- r+b/w+b/a+b

### 4.3 文件操作方法

- read() 

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  f.read()#将文件的所有内容一次读入内存
  f.read(1)#读取一个字符
  ```

- readlines()

  ```python
  f = open("test.txt",mode="r",encoding="utf-8")
  lst = f.readlines()#将文件的所有内容一次读入内存,并按行放入列表中
  ```

- seek() 移动光标

  ```python
  seek(0) #将光标移动到开头
  seek(1) #将光标向后移动一个字节
  ```

- tell()  获取光标位置

- flush() 将内存中的数据强制写入硬盘文件

- 修改特别大的文件

  ```python
  with open("test.txt",mode="r",encoding="utf-8") as f1,open("test1.txt",mode="r",encoding="utf-8") as f2:
      for line in f1:
          content = line.replace("123","456")
          f2.write(content)
  ```

  

### 4.4 自动释放文件句柄

- with ... as....

  ```python
  with open("test.txt",mode="r",encoding="utf-8") as f:
      f.read
  #待缩进内的代码执行完成后，文件句柄会自动被释放
  ```

## 五、函数

截止目前，都是面向过程编程（可读性差/可重用性差）

对函数编程：

- 本质：将N行代码那到别处起个名字，以后通过名字就可以找到执行这段代码。
- 场景：
  - 代码重复执行
  - 代码量超过一屏，可以通过函数对代码进行分割。

函数执行：

- 函数和函数的执行不会相互影响，函数内部执行相互之间不会混乱
- 函数的销毁条件
  - 函数执行完成
  - 函数内部数据无人使用

### 5.1 基本结构

```python
def 函数名():
    #函数内容

#函数调用
函数名()
```

注意：函数如果不调用，将永远不会执行。

### 5.2 参数

#### 5.2.1 形参和实参

```python
def get_sum(a,b)  #这里ab为形参
	pass

v1 = 3
v2 = 4
get_sum(v1,v2)#这里的v1和v2为实参
```

#### 5.2.2 参数基本知识

- 任意类型
- 任意个数

#### 5.2.3 位置传参（调用函数时）

```python
def func(a1,a2,a3)：
	print(a1,a2,a3)
func(1,2,3)#按参数位置传递，一一对应
```

#### 5.2.4 关键字传参（调用函数时）

```python
def func(a1,a2,a3)：
	print(a1,a2,a3)
func(a1=1,a3=2,a2=3)#按参数名赋值
```

- 位置传参必须在关键字传参之前

  ```python
  def func(a1,a2,a3)：
  	print(a1,a2,a3)
  func(1,a3=2,a2=3)#正确
  func(a1=1,a2=2,3)#报错
  ```

#### 5.2.5 默认参数（定义函数时）

```python
def func(a1,a2,a3=3)：  #非默认参数必须在默认参数前
	print(a1,a2,a3)

func(a1=1,a2=2)#默认参数可以不传，不传的话使用默认值
```

默认参数的默认值慎用可变类型

```python
def func(data,value=[]):   #不推荐
    pass

def func(data,value=None): #推荐
    if not value:
        value = []
```

```python
def func(data,value=[]): #value在函数加载时就被定义出来 
    value.append(data)
    return value

v1 = func(1) 
v2 = func(1,[11,22,33])
v3 = func(2)
print(v1,v2,v3)  #[1,2]  [11,22,33,1] [1,2]
```

#### 5.2.6 万能参数（定义函数时）

- 第一种

  ```python
  def func(*args): #不支持关键字传参，只能用位置传参，可以传入任意个位置参数，会转换成元组
  	print(args)
  
  func(1,2,3,4,5) #转换成元组(1,2,3,4,5)
  func((1,2,3,4,5))#会转换成元组((1,2,3,4,5),)
  func(*(1,2,3,4,5))#会循环形成元组(1,2,3,4,5)， 也可以是列表、集合
  ```

  ```python
  def func(a1,*args,a2):
      print(a1,args,a2)
  func(1,2,3,4,5,a2=10)#会将1传给a1,2,3,4,5传给args,a2必须用关键字传参	
  ```

- 第二种

  ```python
  def func(**kwargs): #不支持位置传参，只支持关键字传参，可以传入任意个关键字参数，会转换成字典
      print(kwargs)
      
  func(k1=123,k2=456)#转换为{'k1':123,'k2':456}
  func(**{'k1':123,'k2':456}) #将字典复制给kwargs
  ```

#### 5.2.7 综合应用

```python
def func(*args,**kwargs):
    print(args,kwargs)
    
func(1,2,3,4,k1='alex',k2=123)#(1,2,3,4)  {'k1':'alex','k2':123}
```

#### 5.2.8参数重点

```python
def func(a1,a2):
    pass

def func(a1,a2=None):
    pass

def func(*args,**kwargs):
	pass
```

### 5.3 返回值

- 在函数里，遇到return后后面的代码不再执行
- 不写的话，默认返回None

```python
def get_sum(a,b):
	return a+b

get_sum(2,3)
```

### 5.4 作用域

1. 在python中：
   - py文件：全局作用域
   - 函数：局部作用域，一个函数一个作用域

2. 作用域内的数据归自己所有，别人访问不到。局部作用域可以访问父级作用域的数据。

3. 作用域中数据查找规则：首先在自己的作用域内查找，自己的作用域内不存在就去父级作用域查找，直到全局作用域。

4. 子作用域只能找到父级作用域内的变量，默认不能为父级作用域的变量重新赋值。但可以修改可变类型（列表，字典，集合）内部的值。

5. 可以使用global关键字为全局作用域的变量重新赋值

   ```python
   name = 'oldboy'
   def func():
       global name
       name = 'alex'
   func()
   print(name) #alex
   ```

6. 可以使用nonlocal关键字为父级的变量重新赋值

   ```python
   name = 'oldboy'
   def func():
       name = 'alex'
       def func1():
   		nonlocal name
           name = "wusir"
       func1()
       print(name)
   func() #打印wusir
   ```

### 5.5 函数高级知识

#### 5.5.1 函数赋值

- 函数名当做变量使用

- 函数名可以放入集合中，也可当做字典的键

  ```python
  def func():
      return 123
  v = func
  print(v()) #123
  ```

  ```python
  def func():
      print(123)
  func_list = [func,func,func]
  func_list[0]() #123
  func_list[1]() #123
  func_list[2]() #123
  ```

#### 5.5.2 函数作参数传递

```python
def func(arg):
	arg()
def func1():
    print(123)
func(func1) #123
```

#### 5.5.3 函数作返回值

```python
def func()
	print(123)
def bar():
    return func
result = bar()
result() #123
```

```python
name = "oldboy"
def func():
	print(name)

def bar():
    return func

result = bar()
result() #oldboy
```

```python
def func()
	def inner():
        print(123)
    return inner

result = func()
result() #123
```

```python
name = 'oldboy'
def func():
    name = 'eric'
    def inner():
        print(name)
    return inner
v = func()
v() #eric
```

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

#### 5.5.4 闭包

为函数开辟一块内存空间（内部变量供自己使用），为它以后执行时提供数据。

```python
name = 'oldboy'
def func(name)
	def inner()
    	print(name)
    return inner
v1 = func('alex') #{name:alex,inner} 闭包：为函数开辟一块内存空间(内部变量供自己使用)，为它以后执行提供数据
v2 = func('eric') #{name:eric,inner}
v1() #alex
v2() #eric
```

练习题

```python
name = 'oldboy'
def func():
    print(name)
def bar()
	name = 'alex'
    return func
v = bar()
v() #oldboy
```

```python
name = 'oldboy'
def func():
	name = 'alex'
    def inner():
        print(name)
    return inner
v = func()
v() #alex     
#注意：看函数是在哪里定义的
```

面试题

```python
info = []

def func(name):
	def inner():
		print(name)
    return inner

for item in range(10):
    info.append(func(item)

info[0]() #0 闭包{name：0 inner}
info[2]() #2 闭包{name：2 inner}
info[4]() #4 闭包{name：4 inner}
```

闭包应用场景：装饰器、SQLAlchemy源码

注意：闭包：封装值+内层函数使用

```python
#不是闭包
def func(name):
    def inner():
        return 123
    return inner

#是闭包
def func(name):
    def inner():
        return name
    return inner
```

#### 5.5.5 高阶函数

- 把函数当做参数传递
- 把函数当做返回值返回

### 5.6 lambda表达式(匿名函数)

为解决简单函数的情况，如：

```python
def func(a1,a2):
	return a1+a2
#等同于
v = lambda a1,a2:a1+a2
v(1,2)#3
```

```python
lst = []
func = lambda a1:lst.append(a1)
print(func(666)) #None
print(lst)#[666]
```

- lambda表达式的几种形式

  ```python
  func1 = lambda : 100
  
  func2 = lambda a1,a2:a1+a2
  
  func3 = lambda *args,**kwargs: print(args)
  
  DATA = 100
  func4 = lambda a1: a1+DATA  #只有一行代码，不能自己创建变量，只能去父级寻找
  ```

  

### 5.7 内置函数

#### 5.7.1 类型强转

- int()
- bool()
- str()
- list()
- tuple()
- dict()
- set()

#### 5.7.2 输入输出

- print()
- input()

#### 5.7.3 数学相关

- abs() 取绝对值

- max()  取最大值

  ```python
  print(max([11,22,66,754,78]))#754
  print(max(2,3))#3
  ```

- min()  取最小值

- sum()  求和

  ```python
  print(sum([1,2,3,4]))#10
  ```

- float()   转浮点类型

- divmod()  取两数相除的商和余数(可用于分页)

  ```python
  print(divmod(10,3)) #(3,1)
  print(divmod(3.5,2)) #(1.0,1.5)
  ```

- pow() 求指数

  ```python
  print(pow(2,3)) #2**3 = 8
  ```

  

- round()  取保留小数位

  取距离自身最近的整数，如果最后一位为5

  - 未指定第二个参数（保留位数），则取距离最近的偶数

    ```python
    print(round(1.5))#2
    print(round(2.5))#2
    ```

  - 若指定了第二个参数，若取舍位前一位为奇数，则直接舍弃取舍位；若为偶数则向上取舍

    ```python
    print(round(2.675,2))#2.67
    print(round(2.665,2))#2.67
    ```

#### 5.7.4 进制转换

- bin()   将十进制转换成二进制

  ```python
  print(bin(15))  #0b1111
  ```

- oct()   将十进制转换成八进制

  ```python
  print(oct(15))  #0o17
  ```

- hex()  将十进制转换成十六进制

  ```python
  print(hex(15)) #0xf
  ```

- int()    将其他进制转换成十进制

  ```python
  v1 = '0b11111111'
  print(int(v1,base=2)) #255
  
  v2 = '77'
  print(int(v2,base=8)) #63
  
  v3 = 'ff'
  print(int(v3,base=16))#255
  ```

  - base默认值为10

#### 5.7.5 编码相关

- chr()  将十进制整数转换成unicode编码的字符

  ```python
  v = 65
  print(chr(v)) #A
  ```

- ord() 将字符转换成Unicode编码对应的十进制整数

  ```python
  v = 'a'
  print(ord(v))#97
  ```

#### 5.7.6 高级内置函数

- map(函数,可迭代对象)    映射    输入元素数量 = 输出元素数量 ，且一一对应。

  ```python
  def func(a):
      return a+100
  
  lst = [1,2,3,4]
  result = map(func,lst)  #内部会遍历lst,将每个元素传给函数，然后将返回值放入新列表。不会影响原列表
  print(result)  # [101,102,103,104] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

  ```python
  #计算两个列表对应元素相加
  lst1 = [1,2,3,4,5]
  lst2 = [10,9,8,7,6]
  result = map(lambda x,y:x+y,lst1,lst2)
  print(list(result)) #[11, 11, 11, 11, 11]
  ```

- fliter(函数,可迭代对象)    筛选    输入元素数量 >= 输出元素数量

  ```python
  def func(a):
      return a > 3
  lst = [1,2,3,4,5]
  result = filter(func,lst) #内部会遍历lst,将每个元素传给函数，然后根据函数返回的真假确定是否添加到新列表。不会影响原列表
  print(result)  # [4,5] py2直接返回列表
  print(list(result)) #py3返回一个可迭代对象，需要强转一下
  ```

- reduce(函数,可迭代对象)    多对一  （py3把该函数放入functools模块中，需导入模块后使用）

  ```python
  import functools
  def func(x,y):       #函数参数必须为两个
      return x + y
  lst = [1,2,3,4,5]
  result = functools.reduce(func,lst)#内部会遍历lst，第一次循环时传入前两个元素，后面每次循环传入上次循环函数的返回值和下一个元素
  print(result) #15
  ```

#### 5.7.7 其他

- len()
- open()
- range()
- id()
- type()

#### 5.7.8 isinstance(obj,class)

查看obj是否是class或class的基类的实例

```python
class Base(object):
    pass

class Foo(Base):
    pass

obj = Foo()

print(type(obj) == Base) #False
print(isinstance(obj,Base)) #True
```

#### 5.7.9 issubclass(class,class)  

查看一个类是否是另一个类的子类

```python
class Base(object):
    pass

class Foo(Base):
    pass

class Bar(Foo):
    pass

print(issubclass(Bar,Base))#True
```

### 5.8 装饰器

#### 5.8.1 原理示例

```python
def func():
    print(1)
    
def bar():
    print(2)
    
bar = func
bar()  #将func函数的地址赋值给了bar  结果：1
```

```python
def func(arg):
    def inner():
        print(arg)
    return inner
v1 = func(1)
v2 = func(2)
v1() #1
v2() #2
```

```python
def func(arg):
    def inner():
        return arg()
    return inner
def index():
    print(123)
    return 666
v1 = func(index) #将inner函数地址赋给v1
result = v1() #执行inner函数，内部执行index函数，打印123，并将返回值赋给result
print(result)
```

```python
def func(arg):
    def inner():
        return arg()
    return inner
def index():
    print(123)
    return 666
index = func(index) #将inner函数地址赋给index,此时index变量不再指向原来函数地址
result = index() #执行index也就是inner函数，由于闭包，内部参数依旧执行index原地址，打印123，并将返回值赋给result
print(result) #666
```

```python
def func(arg):
    def inner():
        print('before')
        v = arg()
        print('after')
        return v
    return inner
def index():
    print(123)
    return 666
index = func(index) #将inner函数地址赋给index,此时index变量不再指向原来函数地址
result = index() #执行index也就是inner函数，由于闭包，内部参数依旧执行index原地址，先打印before,再执行原函数打印123，将返回值赋给v，然后打印after，最后返回v
print(result) #666
```

```python
def func(arg):
    def inner():
        print('before')
        v = arg()
        print('after')
        return v
    return inner

@func #第一步将index函数作为参数传入func函数 v = func(index)。第二部将func函数返回值赋值给index index = v
def index():
    print(123)
    return 666

index() 
```

#### 5.8.2 装饰器目的

在不改变原函数内部代码的情况下，在函数执行前和函数执行后自定义一些功能。

#### 5.8.3 装饰器应用场景

- 大多数函数要增加相同的功能

#### 5.8.4 装饰器格式

```python
# 装饰器的编写
def func(arg):
    def inner(*args,**kwargs):
        return arg(*args,**kwargs)
    return inner

# 装饰器的使用
@func
def index():
    print(123)
    
@func
def base(a):
    return a+100  
```

#### 5.8.5 带参数的装饰器

应用：flask框架 + django缓存 + 面试题

```python
####################普通装饰器###################
def func(arg):
    def inner(*args,**kwargs):
        print('before')
        data = arg(*args,**kwargs)
        print('after')
        return data
    return inner

#第一步执行 v1 = func(index)
#第二步 index = v1
@func
def index():
    print(123)
    
####################带参数的装饰器###################
def base(value):
    def func(arg):
        def inner(*args,**kwargs):
            print(value)
            data = arg(*args,**kwargs)
            print('after')
            return data
        return inner
    return func

#第一步 执行v1 = base(5)
#第二步 ret = v1(index)
#第三步 index = ret
@base(5)
def index():
    print(123)
```



### 5.9 迭代器

1. 自己不会写迭代器，只会用。

2. 迭代器：对可迭代对象（序列）中的元素进行逐一获取。

3. 列表转换成迭代器

   ```python
   lst = [11,22,33,44]
   v1= iter(lst)
   v2 = lst.__iter__()
   print(v2.__next__())
   print(v2.__next__())
   print(v2.__next__())
   print(v2.__next__()) #反复调用__next__()方法来获取每一个值，直到报错：StopIteration
   ```

4. 判断一个对象是否是迭代器：内部是否有`__next__()`方法。

   ```python
   lst = [11,22,33,44]
   for i in lst:   #1.内部将lst转换成迭代器，内部反复执行迭代器的__next__()方法，直到报错结束
       print(i)
   ```

5. 可迭代对象
   - 具有`__iter__()`方法，且`__iter__()` 方法返回一个迭代器或生成器

   - 或者可以被for循环

   - 如何将一个对象变为可迭代对象？

     在其类内部实现`__iter__()`方法，且返回一个迭代器或生成器

     ```python
     #方式一
     class Foo(object):
         def __iter__(self):
             return iter([1,2,3,4])
         
     #方式二
     class Foo(object):
         def __iter__(self):
             yield 1
             yield 2
             yield 4
     ```

     

### 5.10 生成器

```python
#函数
def func1():
    return 123
func()

#生成器函数（内部是否包含yield）
def func2():
	yield 1
    yield 2
    yield 3
    
v1 = func2()  #函数内部代码不会执行，返回一个生成器对象

#生成器可以被for循环，一旦开始循环，函数内部代码就会开始执行
for i in v1:
    print(i) #遇到yield就停止返回yield后的值
    		 #下一次继续从上次结束的位置开始，遇到yield就停止返回
        	 #直到生成器函数执行完毕终止循环
```

总结：

- 函数中如果存在yield，那么该函数就是一个生成器函数
- 执行生成器函数会返回一个生成器，只有for循环生成器的时候生成器函数内部的代码才会执行
- 每次循环都会获取yield返回的值，直到函数执行完成

```python
CURSOR = 0
FLAG = False

def func():
    global CURSOR
    global FLAG
    while True:
        f = open('goods.txt',mode='r',encoding='utf-8')
        f.seek(CURSOR)
        content_list = []
        for i in range(2):
            line = f.readline().strip()
            if not line:
                FLAG = True
            content_list.append(line)
        CURSOR = f.tell()
        f.close()

        for item in content_list:
            yield item
        if FLAG:
            return

v = func()
for item in v:
    print(item)
```

生成器：函数内部有yield就是生成器函数，调用该函数返回生成器，for循环生成器函数内部代码才会执行

- 生成器用来生成数据，也可以迭代(被for循环)，并且内部也有`__next__()` 方法
- 生成器是一种特殊的迭代器，和可迭代对象

yield from

```python
def base():
    yield 11
    yield 22

def func():
    yield 1
    yield from base()
    yield 3

for item in func():
    print(item)     #1 11 22 3
```

生成器的send方法会将参数赋给上一次执行的yield的变量，然后在执行下一次yield

可以通过close()方法手动关闭生成器

注：

- 取过一次就没有了

- 惰性运算：不取不执行(list,`__next__`,for循环)

  ```python
  def demo():
       for i in range(4):
          yield i
  g=demo()
  g1=(i for i in g)
  g2=(i for i in g1)
  print(list(g1))#[0,1,2,3]
  print(list(g2))#[]
  ```

  ```python
  def add(n,i):
      return n+i
  def test():
      for i in range(4):
          yield i
  g=test()
  for n in [1,10]:
      g=(add(n,i) for i in g)
  print(list(g))#[20,21,22,23]
  ```

### 5.11 递归函数

递归函数：函数自己调用自己，内存无法释放，效率低。python递归调用默认最大次数为1000次。

```python
def func():
    print(123)
    func()
func()
```



## 六、模块

### 6.1 基本概念

- 内置模块

  python内部提供的功能

- 第三方模块

  需要下载、安装、使用

  pip install 要安装的模块名称

- 自定义模块

- 常用模块：json，time，datetime

- 模块的概念：

  - 模块是一个py文件或者一个文件夹（包），方便以后其他py文件调用。

- 对于包的定义：
  - py2：在文件夹下必须加上  __ init  __ .py文件。
  - py3：不必要加上__ init  __ .py文件。

- 导入模块
  - 导入模块时会将模块中的所有值加载到内存。
  - 导入模块的几种方式

  ```python
  import 模块
  from 模块 import 函数，函数
  from 模块 import *  #导入模块内所有函数
  from 模块 import 函数 as 别名 #起别名
  ```

- 导入包中的文件

  导入包相当于执行了`__init__.py`文件

  直接只导入一个包，包内的模块默认是不能用的

  ```python
  import 包  #内部模块不能使用
  ```

  

  ```python
  from 包 import 文件
  from xxx.xx import x 
  
  #导入的是包下面的__init__.py文件
  import 包
  from 包 import * 
  ```

- __ file __ :命令行执行脚本时，传入的该脚本路径参数。

- 注意：文件和文件夹的命名不能是导入的模块名称相同，否则就会直接在当前目录中查找。

### 6.2 随机数 random

```python
import random
v = random.randint(1,3)
print(v) #随机生成一个1~3的整数
v1 = random.choice([1,2,3,4,5]) #随机选取一个
print(v1) #1
v2 = random.sample([1,2,3,4,5],3) #随机选取多个
print(v2) #[2, 5, 3]
v3 = random.uniform(1,5) #随机获取一个小数
print(v3) #2.906728453875117
lst = [1,2,3,4,5]
random.shuffle(lst) #打乱顺序
print(lst) #[1, 4, 5, 3, 2]
```

可以和内置函数chr()内置函数搭配使用生成随机验证码

```python
import random
def func(count):
    lst = []
    for i in range(0,count):
        val = random.randint(65,90)
        lst.append(chr(val))
    return ''.join(lst)
result = func(4)
print(result)
```

### 6.3 摘要算法模块  hashlib

#### 6.3.1 md5算法

- 加密

```python
import hashlib

def func(pwd):
    key = "asdw3221" #秘钥
    obj = hashlib.md5(key.encode("utf-8")) #在内存中字符串编码是unicode。在py3中需要转换成utf-8编码。否则报错
    obj.update(pwd.encode('utf-8')) #加密字符串
    result = obj.hexdigest() #得到密文
    return result

print(func('123'))
```

- md5校验文件一致性

```python
import hashlib

obj = hashlib.md5()
obj.update('123'.encode('utf-8'))  #可以一次update完，也可以分多次update，结果相同
print(obj.hexdigest()) #比较最终的结果是否相同验证文件一致性
```

#### 6.3.2 sha算法 

用法和md5相同，通常使用sha1

### 6.4 密码不显示   getpass

只有在终端中运行有效果

```python
import getpass

pwd = getpass.getpass('请输入密码：')
print(pwd)
```

### 6.5 时间  time

#### 6.5.1 关于时间

1. UTC/GMT  世界时间
2. 本地时间

#### 6.5.2 time.time() 获取时间戳（秒）

```python
import time

v = time.time() #获取时间戳（秒）
time.sleep(2) #休眠2秒
```

#### 6.5.3 time.sleep() 休眠

```python
import time

time.sleep(2) #休眠2秒
```

#### 6.5.4 time.timezone 获取和格林威治时间的差值

```python
import time

v = time.timezone
print(v) #-28800
```

### 6.6 datetime

#### 6.6.1 datetime.now() 获取本地当前时间   得到时间类型

```python
from datetime import datetime

v = datetime.now()
print(v,type(v)) #2019-04-18 16:59:34.829613 <class 'datetime.datetime'>
```

#### 6.6.2 datetime.utcnow() 获取UTC当前时间

```python
from datetime import datetime

v = datetime.utcnow()
print(v,type(v))#2019-04-18 09:00:16.636312 <class 'datetime.datetime'>
```

#### 6.6.3 获取其他时区时间

```python
from datetime import datetime,timezone,timedelta
tz = timezone(timedelta(hours=7)) #东七区的时间   -7为西七区的时间
date = datetime.now(tz)
print(date)
```

#### 6.6.4 时间增加或减少

```python
from datetime import datetime,timedelta
v1 = datetime.now()#2019-04-18 17:07:35.080733
print(v1)
v2 = v1 + timedelta(days=10)
print(v2)#2019-04-28 17:07:35.080733
```

#### 6.6.5 datetime转成字符串（strftime(格式)）

```python
from datetime import datetime

v1 = datetime.now()
print(v1,type(v1)) #2019-04-18 17:13:16.579031 <class 'datetime.datetime'>
v2 = v1.strftime('%Y-%m-%d %H:%M:%S')
print(v2,type(v2))#2019-04-18 17:13:16 <class 'str'>
```

#### 6.6.6 字符串转成datetime  (strptime(字符串，格式))

```python
from datetime import datetime

v1 = '2018-12-14'
v2 = datetime.strptime(v1,'%Y-%m-%d')
print(v2)#2018-12-14 00:00:00
```

#### 6.6.7 时间戳转datetime  (fromtimestamp(时间戳))

```python
import time
from datetime import datetime

v1 = time.time()
date = datetime.fromtimestamp(v1)
print(date) #2019-04-18 17:20:42.438591
```

#### 6.6.8 datetime转时间戳  (timestamp())

```python
from datetime import datetime

v1 = datetime.now()
date = v1.timestamp()
print(date)#1555579302.702781
```

### 6.7 python解释器相关 sys

- sys.getrefcount() 获取变量被引用的次数

  ```python
  import sys
  value = []
  print(sys.getrefcount(value)) #2
  ```

- sys.getrecursionlimit()   获取python默认递归最大次数

  ```python
  import sys
  print(sys.getrecursionlimit()) #1000
  ```

- sys.stdout.write()    输出（不换行）

  ```python
  import sys
  sys.stdout.write("123")
  sys.stdout.write("123") #123123
  ```

  注：\r  将光标移动到本行的开头，可用于打印进度百分比

  ```python
  import time
  
  for i in range(1, 101):
      msg = "%s%%" % i
      print(msg, end='')
      time.sleep(0.1)
  ```

- sys.argv   获取执行脚本传入的参数

  ```python
  # C:\Python36\python36.exe D:/code/s21day14/7.模块传参.py D:/test
  # sys.argv = [D:/code/s21day14/7.模块传参.py, D:/test]
  #print(sys.argv[1])  #D:/test
  ```

- sys.path  默认python在导入模块时会按照sys.path中的路径逐个查找。可以通过sys.path.append()添加一个路径

  ```python
  for i in sys.path:
      print(i)
  #E:\code\day10   当前执行文件所在的路径
  #C:\Python36\python36.zip
  #C:\Python36\DLLs
  #C:\Python36\lib
  #C:\Python36
  #C:\Python36\lib\site-packages
  ```

- sys.exit()  退出程序

- sys.module   存储了当前程序用到的所有模块

### 6.8 操作系统相关 os

- os.path.exists()    判断路径是否存在

  ```python
  import os
  print(os.path.exists('data.py')) #false
  ```

- os.stat(文件).st_size   获取文件大小

  ```python
  import os
  print(os.stat('goods.txt').st_size)
  ```

- os.path.abspath()  获取文件的绝对路径

  ```python
  import os
  print(os.path.abspath('goods.txt')) #E:\code\day10\goods.txt
  ```

- os.path.dirname()  获取路径的上级目录

  ```python
  import os
  v = r'E:\code\day10\goods.txt'  #r转义
  print(os.path.dirname(v)) #E:\code\day10
  ```

- os.path.join(*args)  将路径拼接起来

  ```python
  import os
  
  v = os.path.join('E:\code\day10', '123', '34')
  print(v) #E:\code\day10\123\34
  ```

- os.listdir()   获取目录下的所有文件，只能获取当前层

  ```python
  import os
  
  v = os.listdir('E:\code\day10')
  print(v) #['.idea', '123', 'day10.py', 'day11.py', 'day12.py', 'day13.py', 'goods.txt', 'test.py', 'test1.py', '__pycache__', '面试题.py']
  ```

- os.walk()  获取目录下的所有文件，所有层，返回生成器

  - for循环生成器 每次循环得到三个元素，第一个元素：正在查看的目录  第二个元素：此目录下的文件夹

    第三个元素：此目录下的文件

  ```python
  result = os.walk(r'D:\code\s21day14')
  for a,b,c in result:
  # a,正在查看的目录 b,此目录下的文件夹 c,此目录下的文件
  	for item in c:
  		path = os.path.join(a,item)
  		print(path)
  ```

- os.mkdir()  在当前工作路径下创建目录，只有一层

  ```python
  import os
  os.mkdir('aaa')
  ```

- os.makedirs()  在当前工作目录下创建目录及子目录，如果存在会报错

  ```python
  import os
  os.makedirs(r'dd\bb\cc')
  ```

- os.rename()  重命名

  ```python
  import os
  os.rename('test1.py','test2.py')
  os.rename('aaa','bbb')
  ```

### 6.9 shutil

#### 6.9.1 shutil.rmtree() 

删除目录下的所有文件和目录

```python
import shutil

shutil.rmtree('E:/code/day10')
```

#### 6.9.2 shutil.move() 重命名

```python
import shutil

shutil.move('ass','aaa')
shutil.move('aaa.txt','abc.txt')
```

#### 6.9.3 shutil.make_archive(压缩包名称,压缩的格式,要压缩的文件(夹))  

压缩文件或文件夹

```python
import shutil

shutil.make_archive('abc','zip','aaa')
```

#### 6.9.4 shutil.unpack_archive(压缩包名称,解压路径(默认当前路径),格式)  解压

```python
import shutil

shutil.unpack_archive('abc.zip',extract_dir=r'E:\test',format='zip')
```

### 6.10 json

#### 6.9.1 json基本知识

- json是一个特殊的字符串。
- 序列化：对象转换成json。
- 反序列化：json转换成对象
- json格式要求
  - 只包含int,str,list,dict,bool,tuple(会转换成列表)
  - 最外层必须是列表或字典
  - 如果包含字符串必须是双引号
- json的优缺点
  - 优点：所有语言通用
  - 缺点：只支持基本的数据类型（int,bool,str,list,tuple(会转换成list),dict）

#### 6.9.2 json.dumps() 序列化

```python
import json

lst = [1,2,3,{'k1':'v1'},(1,2,3),'alex',True]
print(json.dumps(lst))#[1, 2, 3, {"k1": "v1"}, [1, 2, 3], "alex", true]

#保留中文
import json
lst = ['alex',123,'李杰'] 
print(json.dumps(lst))#["alex", 123, "\u674e\u6770"]  中文默认序列化为unicode编码

print(json.dumps(lst,ensure_ascii=False))#["alex", 123, "李杰"]
```

#### 6.9.3 json.loads() 反序列化

```python
import json

val = '["alex",{"k2":2,"k1":44},true]'
print(json.loads(val))#['alex', {'k2': 2, 'k1': 44}, True]
```

#### 6.9.4 json.dump(obj,文件句柄,ensure_ascii = False) 

将对象序列化后写入文件

```python
import json

lst = ['alex', 123, '李杰']
f = open('abc.txt', mode='w', encoding='utf-8')
json.dump(lst, f, ensure_ascii=False)
f.close()
```

#### 6.9.5 json.load(文件内容)  

将文件内容反序列化成对象

```python
import json

f = open('abc.txt', mode='r', encoding='utf-8')
content = f.read()
data = json.load(content)
f.close()
print(data)
```

### 6.11 pickle

#### 6.11.1 pickle基本知识

- pickle优缺点
  - 优点：可以序列化几乎所有类型（socket除外）
  - 缺点：只有python自己认识

#### 6.11.2 pickle.dumps() 

将对象序列化成字节类型

#### 6.11.3 pickle.loads()

将字节类型反序列化成对象

```python
import pickle

lst = ['alex',123,'李杰']
v1 = pickle.dumps(lst)
print(v1) #b'\x80\x03]q\x00(X\x04\x00\x00\x00alexq\x01K{X\x06\x00\x00\x00\xe6\x9d\x8e\xe6\x9d\xb0q\x02e.'
data = pickle.loads(v1)
print(data)#['alex', 123, '李杰']

#操作函数
import pickle

def func():
    print(123)

v1 = pickle.dumps(func)
print(v1) #b'\x80\x03c__main__\nfunc\nq\x00.'
data = pickle.loads(v1)
data()#123
```

#### 6.11.4 pickle.dump()

将对象序列化成bytes并写入文件

```python
import pickle

def func():
    print(123)

f = open('abc.txt',mode='wb')
pickle.dump(func,f)
f.close()
```

#### 6.11.5 pickle.load()

读取文件内容，反序列化成对象

```python
import pickle

def func():
    print(123)

f = open('abc.txt',mode='rb')
data = pickle.load(f)
data()
f.close()
```

### 6.12 importlib模块

根据字符串的形式导入一个字符串

```python
import importlib
redis = importlib.import_module('utils.tests')
print(redis.X)


path = 'utils.tests.func'

v1,v2 = path.rsplit('.',maxsplit=1)
test = importlib.import_module(v1)
getattr(test,v2)()
```

### 6.13 logging模块

目的：记录日志

用户：记录用户的一些操作

程序员：

- 用于统计
- 用来做故障排除的debug
- 用来记录错误完成代码优化

应用场景：对异常处理捕获到的内容，通过logging模块写入到日志文件中。

#### 6.13.1 基本使用

```python
import logging
logging.basicConfig(filename='x.log',level=logging.ERROR)#使用系统默认编码
#level登记  一般写ERROR
'''
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
'''
logging.error('错误',exc_info=True)# 只有大于等于配置的登记才能写入日志文件,exc_info为True保留堆栈信息
```

#### 6.13.2 内部原理

```python
import logging
#涉及3个对象FileHandler、Formatter、Logger
file_handler = logging.FileHandler('x1.log',mode='a',encoding='utf-8')#创建FileHandler对象
fmt_handler = logging.Formatter(fmt='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s') #创建Formatter对象
file_handler.setFormatter(fmt_handler)#把Formatter对象添加到FileHandler对象中

logger = logging.Logger('xxx',level=logging.ERROR)#创建Logger对象，第一个参数为logger对象名称，即Formatter格式化时的name
logger.addHandler(file_handler)#把FileHandler对象添加到Logger对象中


logger.error('错误',exc_info=True)
```

#### 6.13.3 通过logger对象实现日志

```python
#创建logger对象
logger = logging.getLogger('mylog') 
#设置日志等级
logger.setLevel(logging.ERROR)
#创建文件操作符
fh = logging.FileHandler(filename='log.log',mode='a',encoding='utf-8')
#创建屏幕操作符
sh = logging.StreamHandler()
#创建格式化对象
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(module)s: %(message)s")
#将格式化对象绑定到文件操作符
fh.setFormatter(formatter)
#将格式化对象绑定到屏幕操作符
sh.setFormatter(formatter)
#将文件操作符添加到logger对象
logger.addHandler(fh)
#将屏幕操作符添加到logger对象
logger.addHandler(sh)

logger.error('123')#2019-04-29 15:19:25,594 - mylog - ERROR - test: 123
```



#### 6.13.4 推荐使用格式

- 写入到一个日志文件中

  ```python
  import logging
  file_handler = logging.FileHandler('x1.log',mode='a',encoding='utf-8')
  
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      level=logging.ERROR,
      datefmt='%Y-%m-%d %H:%M:%S %p',#日期时间格式化
      handlers=[file_handler,]
  )
  
  
  logging.error('错误',exc_info=True)
  ```

- 日志分割（按时间）

  ```python
  import logging
  from logging import handlers
  file_handler = handlers.TimedRotatingFileHandler(filename='x2.log',when='h',interval=1,encoding='utf-8')#when参数单位为小时，interval参数按几个小时来分割
  
  logging.basicConfig(
      format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',
      level=logging.ERROR,
      datefmt='%Y-%m-%d %H:%M:%S %p',
      handlers=[file_handler,]
  )
  
  try:
      int('wwqe')
  except Exception as e:
      logging.error(str(e),exc_info=True)
  ```

### 6.14 collections模块

#### 6.14.1 OrderedDict 有序字典

```python
from collections import OrderedDict

dic = OrderedDict([('a',1),('b',2),('c',3)])
for k,v in dic.items():
    print(k,v) #a 1   b 2    c 3
```

#### 6.14.2 namedtuple  可命名元组

创建一个类这个类没有方法，所有属性都不能修改

```python
from collections import namedtuple

Course = namedtuple('Course',['name','price','teacher'])
python = Course('python',19800,'alex')
print(python) #Course(name='python', price=19800, teacher='alex')
print(python.name) #python 
print(python.teacher) #alex
```

#### 6.14.3 deque 双端队列

#### 6.14.4 defaultDict 默认字典

可以给字典的value设置一个默认值

### 6.15 re模块

#### 6.15.1 转义符

正则表达式的转义符在python的字符串中也有转义的作用，但正则表达式中的转义符和字符串中的转义符并没有关系，且还容易产生冲突。为了避免冲突我们所有的正则都以在工具中的测试结果为结果，然后只需要在正则和待匹配的字符串前面加r即可。

```python
import re
ret = re.search(r'\\n',r'\n')
print(ret.group())
```

#### 6.15.2 模块方法

1. findall()

   ```python
   import re
   ret = re.findall('\d','alex83') #第一个参数正则表达式，第二个参数待匹配的字符串，返回一个列表，未匹配到返回空列表
   print(ret) #['8','3']
   ```

2. search()

   ```python
   import re
   ret = re.search('\d','alex83')#若匹配到则返回一个对象，否则返回None,只取第一个符合条件的项
   print(ret) #<_sre.SRE_Match object; span=(4, 5), match='8'>
   if ret:
   	print(ret.group())#8
   ```

3. match()

   ```python
   #只能从头开始匹配，相当于search的正则表达式加上一个^
   import re
   ret = re.match('\d','1alex83')#若匹配到则返回一个对象，否则返回None,只取第一个符合条件的项
   print(ret) #<_sre.SRE_Match object; span=(4, 5), match='8'>
   if ret:
   	print(ret.group())#8
   ```

4. finditer()

   ```python
   import re
   ret = re.finditer('\d','alex83') #返回一个迭代器
   for i in ret: #循环迭代器返回一个对象
       print(i.group())
   ```

5. compite()

   将正则表达式提前编译好，后面重复调用该正则规则时可以节省时间和空间。

   ```python
   import re
   ret = re.compile('\d')
   res = ret.findall('alex83')
   print(res)
   
   res = ret.search('alex83')
   print(res.group())
   
   res = ret.finditer('alex83')
   for i in res:
       print(i.group())
   ```

   compile的flags参数：

   ```python
   flags = re.S  #.可以匹配任意字符
   ```

6. split()

   ```python
   import re
   ret = re.split('\d+','alex83wusir74taibai40') #以正则表达式匹配的内容进行分割
   print(ret)#['alex', 'wusir', 'taibai', '']
   
   #split 默认保留分组中的内容
   ret = re.split('(\d+)','alex83wusir74taibai40')
   print(ret)#['alex', '83', 'wusir', '74', 'taibai', '40', '']
   ```

7. sub()和subn()

   ```python
   import re
   ret = re.sub('\d','D','alex83wusir74taibai40',3) #替换，默认全部替换，返回替换后的字符
   print(ret)# alexDDwusirD4taibai40
   
   ret = re.subn('\d','D','alex83wusir74taibai40',3)#替换，默认全部替换，返回替换后的字符和替换了几个字符
   print(ret)# ('alexDDwusirD4taibai40', 3)
   ```

#### 6.15.3 分组

无分组

```python
import re
s = '<h1>wahaha</h1>'
ret = re.search('<\w+>.*?</\w+>',s)
print(ret.group())
```

分组

```python
import re
s = '<h1>wahaha</h1>'
ret = re.search('<(\w+)>(.*?)</\w+>',s)
print(ret.group(0)) #<h1>wahaha</h1>  等同于ret.group()默认参数为0，表示取整个匹配项
print(ret.group(1)) #h1  取第一个分组内的内容
print(ret.group(2)) #wahaha 取第二个分组内的内容
```

对于findall()遇到正则表达式中的分组，会优先显示分组中的内容

```python
import re

ret = re.findall('\d+(\.\d+)?','1.234+2')
print(ret) #['.234', ''] 优先显示分组中的内容，所以2不会显示
```

取消分组优先显示 ?:

```python
import re

ret = re.findall('\d+(?:\.\d+)?','1.234+2')
print(ret)
```

应用：取字符串里的所有整数

```python
import re

ret = re.findall('\d+\.\d+|(\d+)','1-2*(60+(-40.35/5)-(-4*3))')#首先把整数和小数都取出来，优先显示整数
print(ret) #['1', '2', '60', '', '5', '4', '3']
ret.remove('') #移除''
print(ret)#['1', '2', '60', '5', '4', '3']
```

#### 6.15.4 命名分组

（?P<名字>正则表达式）

```python
import re

s = '<h1>wahaha</h1>'
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</\w+>',s)
print(ret.group('tag'))
print(ret.group('cont'))
```

#### 6.15.5 引用分组

这个组的内容必须和引用的分组的内容完全一致

```python
import re
s = '<h1>wahaha</h1>'
s1 = '<h1>wahaha</h2>'
ret = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s)
ret1 = re.search('<(?P<tag>\w+)>(?P<cont>.*?)</(?P=tag)>',s1)
print(ret.group())#<h1>wahaha</h1>
print(ret1.group())#报错
```

#### 6.15.6 正则的？都能做什么？

- ? 量词，0个或一个

- 用于量词后，非贪婪匹配
- ?P<> 用于分组命名
- ?P=   用于引用分组
- ?： 取消分组优先显示

#### 6.15.7 爬虫小例子

```python
import re
import json
import requests


def get_req(url):
    ret = requests.get(url)
    return ret.text


def get_con(ret, content):
    res = ret.finditer(content)
    for i in res:
        yield {
            'id':i.group('id'),
            'title':i.group('title'),
            'score':i.group('score'),
            'comment':i.group('comment')
        }




def write_file():
    with open('moviefile',mode='w',encoding='utf-8') as f:
        while True:
            dic = yield
            f.write(dic+'\n')


pattern = '<div class="item">.*?<em class="">(?P<id>\d+)</em>.*?<span class="title">(?P<title>\w+)' \
          '</span>.*?<span class="rating_num".*?>(?P<score>.*?)</span>.*?<span>(?P<comment>\d+)人评价'

res = re.compile(pattern, flags=re.S)

num = 0

f = write_file()

next(f)

for i in range(10):
    num = 25 * i
    con = get_req('https://movie.douban.com/top250?start=%s&filter=' % num)
    gc = get_con(res, con)
    for j in gc:
        f.send(json.dumps(j,ensure_ascii=False)#生成器的send方法会将参数赋给上一次执行的yield的变量，然后在执行下一次yield
        
f.close()#手动关闭生成器
```

### 6.16正则表达式

#### 6.16.1 定义

正则表达式是一种匹配字符串的规则

#### 6.16.2 应用场景

- 匹配字符串
  - 电话号码
  - 身份证
  - IP地址

- 表单验证
  - 验证用户输入的信息是否正确

- 爬虫
  - 从网页源码中获取一些链接，重要数据

#### 6.16.3 正则规则（元字符，量词）

1. 第一匹配规则：是哪一个字符，就匹配字符串中的哪个字符

2. 第二匹配规则：字符组[字符1字符2]，一个字符组就代表匹配一个字符，只要这个字符出现在字符组中，那就说明这个字符符合规则。

   ```python
   #[0-9][0-9]匹配一个两位数
   #[A-z]必须根据ascii码从小到大指定范围。
   #[a-zA-Z]匹配所有大小写字母
   ```

##### 6.16.3.1 元字符

```python
# \d: [0-9] 表示所有数字（digit）
# \w: 表示大小写字母，数字，下划线（word）
# \s: 表示空白，即空格，制表符，换行符（space）
# \t: 表示制表符（tab）
# \n: 表示换行符（next）
# \D: 表示除数字以外的所有字符
# \W: 表示除大小写字母，数字，下划线以外的所有字符
# \S: 表示除空白以外的所有字符      例：[\d\D]匹配所有字符
# .: 表示除换行符之外的所有字符
# []: 字符组，只要在中括号内的字符都是符合规则的字符 例：[\d] \d [0-9] 表示的都是匹配一个数字
# [^]: 非字符组，只要不在中括号内的字符都是符合规则的字符
# ^: 表示字符串的开始
# $: 表示字符串的结尾
#例：
# abc abc abc  ^a 只能匹配到第一个a   $c 只能匹配到最后一个c  
# ^abc$ 无匹配项 ：^和$之间有多长，匹配到的字符串就有多长

# |: 表示或   注：abc|ab如果两个规则有重合，则长的在前短的在后
# (): 表示分组，给一部分正则分组， 如：www\.(baidu|luffy)\.com,|的作用域就变小了
```

##### 6.16.3.2 量词

```python
# {n}: 表示只能出现n次
# {n,}: 表示至少出现n次
# {n,m}: 表示至少出现n次，至多出现m次
# ?: 匹配0次或1次，表示可有可无但又只能有一个
# +: 匹配一次或多次
# *: 匹配0次或多次，表示可有可无但有可以有多个
#例：表示一个小数或整数
#\d+(\.\d+)?
```

##### 6.16.3.3 贪婪匹配和非贪婪匹配

- 贪婪匹配（默认）

  总是在符合量词条件的范围内尽量多的匹配

- 非贪婪匹配

  总是匹配符合量词条件范围内尽量小的字符串

在**量词**后面加一个?，就变成了非贪婪匹配。

常用：

```python
#元字符 量词 ？x    表示按照元字符规则在量词范围内匹配，一旦遇到x就停止
#例： .*?x  匹配除换行符外的所有字符遇到x停止
```

带有特殊意义的元字符和量词到了字符组或非字符组内，有些会取消他的特殊意义。如：`[()+*.]`

[-],-的位置决定了他的意义，写在字符组的第一个位置或最后一个位置就表示一个普通的横杠，写在字符组的其他任何位置都表示一个范围

### 6.17 struct模块

```python
import struct

ret = struct.pack('i',10000) #i代表int，得到4个字节
print(ret)#b"\x10'\x00\x00"

res = struct.unpack('i',ret)
print(res)#(10000,)
```

### 6.18 socketserver模块

详见 8.7.2

### 6.19 hmac模块

```python
import hmac,os

secret_key = '123456'
random_msg = os.urandom(16) #得到16位随机字节串
obj = hmac.new(secret_key.encode('utf-8'),random_msg)
print(obj.digest())# 得到加密后的字节串 b': \x98`\xa6\xdb\xcdF\\ \xae\xbfR\x0e\x1a\xc1'
```

## 七、面向对象

- 什么是类：具有相同属性和方法的一类事物。
- 什么是对象或实例：一个具有具体属性和动作的具体个体。

### 7.1面向对象的基本形式

#### 7.1.1 基本格式

```python
#定义类
class Account:
    def func(self):
        print(123)

#调用类中的方法
a = Account() #创建（实例化）了一个Account类的对象
a.func() #使用对象调用类中的方法
```

应用场景：以后遇到很多函数，要对函数进行归类和划分（封装）

#### 7.1.2 对象的作用

self参数：哪个对象调用的方法，self就是哪个对象

对象的作用：存储一些值，供自己以后使用

```python
class Person:
    def func(self):
        print('我叫%s，今年%s岁，性别%s'%(self.name,self.age,self.gender))

p = Person()
p.name = 'alex'
p.age = 18
p.gender = '男'
p.func()

#__init__() 初始化方法（构造方法）
class Person:
    def __init__(self,name,age,gender): #可以加参数
        self.name = name
        self.age = age
        self.gender = gender
        
    def func(self):
        print('我叫%s，今年%s岁，性别%s'%(self.name,self.age,self.gender))
        
p = Person('张三',18,'男') #实例化时，会调用__init__()方法
p.func()
p1 = Person('李四',19,'男') 
p1.func()  #将数据放入__init__()方法中，可以更方便的为每个对象保存自己的数据
```

#### 7.1.3 应用场景

- 业务功能复杂时，可以用面向对象进行分类
- 想要封装有限个数据时

### 7.2 面向对象三大特性

#### 7.2.1 封装性

广义：将数据和方法放入类中

- 将函数归类放到一个类中
- 函数中有反复使用的值，可以放到对象中

狭义：私有成员，不允许外部访问

#### 7.2.2 继承性

```python
#基类
class Base:
    def func1():
        print('f1')

#派生类
class Foo(Base):  
    def func2():
        print('f2')
        
f = Foo()
f.func2() #首先在对象自己的类中查找，找不到就去父类中查找
f.func1()
```

什么时候会用到继承？

- 多个类中有公共方法时，可以将它们放到基类中，避免重复编写。

继承关系中查找方法的顺序：

- self是到底是谁
- self是由哪个类创建的，就在哪个类中查找，找不到就去父类中查找

```python
# 示例一
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1() #base.f1
obj.f2() #foo.f2

# 示例二
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
        
obj = Foo() 
obj.f2() #base.f1  foo.f2

# 示例三
class Base:
    def f1(self):
        print('base.f1')
        
class Foo(Base):
    def f2(self):
        self.f1()
        print('foo.f2')
	def f1(self):
        print('foo.f1')
        
obj = Foo()
obj.f2()#foo.f1  foo.f2

# 示例四
class Base:
    def f1(self):
        self.f2()
        print('base.f1')
	def f2(self):
        print('base.f2')
class Foo(Base):
    def f2(self):
        print('foo.f2')
        
obj = Foo()
obj.f1() #foo.f2  base.f1
```

对于多继承，从左到右依次查找

```python
class BaseServer:
    def serve_forever(self, poll_interval=0.5):
        self._handle_request_noblock()
	def _handle_request_noblock(self):
        self.process_request(request, client_address)
        
	def process_request(self, request, client_address):
        pass
    
class TCPServer(BaseServer):
    pass

class ThreadingMixIn:
    def process_request(self, request, client_address):
        pass
    
class ThreadingTCPServer(ThreadingMixIn, TCPServer): 
    pass

obj = ThreadingTCPServer()
obj.serve_forever()
```

多继承查找方式：

- py2：深度优先
- py3：广度优先   同过C3算法计算查找顺序

#### 7.2.3 多态性(鸭子模型)

```python
def func(arg):
    v = arg[-1]
    print(v)
```

面试题：什么是鸭子模型

答：对于函数而言，Python对参数的类型不会限制，那么在传入参数时就可以是任意类型，在函数中如果有如：arg[-1],那么就是对传入类型的限制（类型必须有索引）。这就是鸭子模型，类似于上述函数，我们认为只要是能嘎嘎叫的就是鸭子（只要有索引就是我们要的参数类型）

### 7.3 成员

#### 7.3.1 类变量 和 实例变量(属于对象)

写在类中与方法同级别，可以通过类名.变量，对象.变量调用

```python
class Base:
    x = 1

obj = Base()
print(obj.name) #打印实例变量
print(Base.name) #报错，类名无法访问实例变量
print(obj.x) #对象调用类变量
print(Base.x) #类名调用类变量
```

只能修改或设置自己的值，不能修改其他值

```python
class Base:
    x = 1

obj = Base()
obj.y = 123 #为对象obj添加了一个y变量
obj.x = 666 #为对象obj添加了一个x变量
print(Base.x) #1
Base.x = 666 #修改类变量x
print(Base.x) #666
```

```python
class Base:
    x = 1
    
class Foo(Base):
    pass
print(Base.x)#1
print(Foo.x)#1
Base.x = 666
print(Base.x)#666
print(Foo.x)#666
Foo.x = 999
print(Base.x)#666
print(Foo.x)#999
```

#### 7.3.2 绑定方法

定义：至少有一个self参数

调用：先创建对象，由对象调用

```python
class Base:
    def func(self):
        pass
obj = Base()
obj.func()
```

#### 7.3.3 静态方法

定义：

- 由装饰器@staticmethod装饰
- 参数任意

调用：

- 可以由类名或对象名（不推荐）调用

```python
class Base:
    @staticmethod
    def func():
		pass
Base.func()
```

#### 7.3.4 类方法

定义：

- 由装饰器@classmethod装饰
- 至少一个参数

调用：

- 可以由类名或对象名（不推荐）调用

```python
class Base:
    @classmetnod
    def func(cls): #cls类名
        pass
Base.func()
```

面试题：@staticmethod和@classmethod的区别？

一个是静态方法，一个是类方法

定义：

- 类方法：用@classmethod做装饰器，且至少一个参数
- 静态方法：用@staticmethod做装饰器，且参数无限制

调用：

- 类名调用，对象也可调用。

#### 7.3.5 属性

定义：

- 用@property装饰器
- 只有一个self参数

调用：

- 对象.func   不加括号

```python
class Base:
    @property
    def func(self):
        return 123
 
obj = Base()
obj.func
```

### 7.4 成员修饰符

公有：任何地方都可以访问

私有：只有类内部可以访问

```python
class Foo:
    def __init__(self,name):
        self.__name = name #__name为私有实例变量
    def func(self):
        print(self.__name)
        
obj = Foo('alex')
obj.__name #外部访问不到（继承也无法访问）
obj.func() #内部可以访问
```

可以通过obj._Foo.__x强制访问

### 7.5 特殊成员

#### 7.5.1 `__init__(self)`

初始化函数,为对象的变量赋值

```python
class Foo(object):
    def __init__(self,name):
        self.name = name
        
obj = Foo('alex')
```

#### 7.5.2 `__new__(cls,*args,**kwargs)`

构造方法，在实例化一个对象时会有两步`obj = Foo('alex')`

- 第一步调用`__new__()`方法
- 第二步调用`__init__()`方法初始化变量

```python
class Foo(object):
    def __init__(self, name):
        self.name = name

    def __new__(cls, *args, **kwargs):
        print(args[0])
        return object.__new__(cls) #返回什么obj就是什么

obj = Foo('alex')
print(obj.name)
```

若类内部无`__new__()`方法，则会调用object的`__new__()`方法

#### 7.5.3 `__call__(self)`

对象后面加括号会执行`__call__()`方法

```python
class Foo(object):
    def __call__(self, *args, **kwargs):
        print('执行了__call__方法')

Foo()()
```

#### 7.5.4 `__getitem__(self,item)`

```python
class Foo(object):
    def __getitem__(self, item):
        return item

obj = Foo()
print(obj['1']) #1
```

#### 7.5.5 `__setitem__(self,key,value)`

```python
class Foo(object):
    def __setitem__(self, key, value):
        print(key,value)

obj = Foo()
obj['k1'] = 123 #k1 123
```

#### 7.5.6 `__delitem__(self,key)`

```python
class Foo(object):
    def __delitem__(self, key):
        print(key)
        
obj = Foo()
del obj['k1']  #k1
```

#### 7.5.7 `__str__(self)`

打印对象时执行，打印内容为返回值

```python
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def __str__(self):
        return '姓名：%s,年龄：%s'%(self.name,self.age,)


obj = Foo('alex',18)
print(obj) #姓名：alex,年龄：18
```

#### 7.5.8 `__dict__`

寻找对象的所有变量，并转换成字典

```python
class Foo(object):
    def __init__(self,name,age):
        self.name = name
        self.age = age


obj = Foo('alex',18)
print(obj.__dict__) #{'name': 'alex', 'age': 18}
```

#### 7.5.9 上下文管理

```python
class Foo(object):

    def __enter__(self):
        print('开始')
        return Foo()

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('结束')


with Foo() as obj:   #with后面的Foo()会执行__enter__()方法,并将返回值赋给obj
    print(123)       #缩进代码执行完成后会执行__exit__()方法
                     #开始
        			 #123
            		 #结束
```

#### 7.5.10 对象相加

```python
class Foo(object):
    def __init__(self, name):
        self.name = name

    def __add__(self, other):
        return '%s + %s'%(self.name,other.name,)

    def __sub__(self, other):
        return '%s - %s' % (self.name, other.name,)

    def __mul__(self, other):
        return '%s * %s' % (self.name, other.name,)

    def __divmod__(self, other):
        return '%s / %s' % (self.name, other.name,)

    def __mod__(self, other):
        return '%s %% %s' % (self.name, other.name,)

obj1 = Foo('alex')
obj2 = Foo('eric')
print(obj1 + obj2) #调用obj1对象的类的__add__(self,other)方法，将obj1赋给self,obj2赋给other，返回值是表达式结果
print(obj1 - obj2)
print(obj1 * obj2)
print(divmod(obj1,obj2))
print(obj1 % obj2)
```

7.5.11 `__iter__(self)`

```python
#将对象变成可迭代对象
class Foo(object):
    def __iter__():
        return iter([1,2,3,4])
    
obj = Foo()
```

### 7.6 super

super().func() 根据self对象所属的类的继承关系,按照顺序挨个查找func()函数，找到第一个开始执行，后面不再找。

```python
class Base(object):
	def func(self):
		print('base.func')
		return 123
class Foo(Base):
	def func(self):
		v1 = super().func()
		print('foo.func',v1)
obj = Foo()
obj.func()
# super().func() 去父类中找func方法并执行
```

```python
class Bar(object):
	def func(self):
		print('bar.func')
		return 123
    
class Base(Bar):
	pass

class Foo(Base):
	def func(self):
		v1 = super().func()
		print('foo.func',v1)
obj = Foo()
obj.func()
# super().func() 根据类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

```python
class Base(object): # Base -> object
	def func(self):
		super().func()
		print('base.func')

class Bar(object):
	def func(self):
		print('bar.func')
class Foo(Base,Bar): # Foo -> Base -> Bar
	pass
obj = Foo()
obj.func()
# super().func() 根据self对象所属类的继承关系，按照顺序挨个找func方法并执行(找到第一个就不在找了)
```

### 7.7 异常处理

#### 7.7.1 基本格式

```python
def func():
    try:
        pass
    except IndexError as e: 
        pass
    except ValueError as e:
        pass
    except Exception as e: #e为Exception类的一个对象
        pass
    finally:   #finally内的代码，上面遇到return也会被执行
        pass
```

#### 7.7.2 抛出异常(raise)

```python
def func():
	result = True
	try:
		with open('x.log',mode='r',encoding='utf-8') as f:
			data = f.read()
			if 'alex' not in data:
			raise Exception()
	except Exception as e:
		result = False
	return result
```

#### 7.7.3 自定义异常

```python
class MyException(Exception):
    pass

def func():
    try:
        raise MyException()
    except MyException as e:
        print(e)
```

### 7.8 约束

是一种规范，约束子类必须实现某方法

```python
class Base(object):

    def send(self):
        raise NotImplementedError()  #基类约束所有的子类必须实现send方法


class Bar(Base):

    def send(self):  #子类实现send方法
        print('Bar的send方法') 


class Foo(Base):

    def send(self):   #子类实现send方法
        print('Foo的send方法')
```

### 7.9 反射

以字符串的形式去某个对象中操作他的成员

通过本文件来获取本文件的任意变量

```python
getattr(sys.module[__name__],'变量名')
```

#### 7.9.1 getattr(对象,'成员')

以字符串的形式去对象中获取对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')
v = getattr(obj,'name') #等同于obj.name
print(v)
```

#### 7.9.2 setattr(对象,'成员',值)

以字符串的形式设置对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')
setattr(obj,'name',123) #等同于obj.name = 123
print(getattr(obj,'name'))
```

#### 7.9.3 hasattr(对象,'成员')

以字符串的形式判断对象中是否包含某成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')

if hasattr(obj,'name'):
    print(getattr(obj, 'name'))
```

#### 7.9.4 delattr(对象,'成员')

以字符串形式删除对象的成员

```python
class Foo(object):

    def __init__(self,name):
        self.name = name

obj = Foo('alex')

delattr(obj,'name')
```

#### 7.9.5 python中一切皆对象

因此以后想要通过字符串形式操作内部成员，均可以通过反射来实现

- py文件（模块）
- 包
- 类
- 对象

```python
import utils.tests
print(getattr(utils.tests,'X'))
```

通过本文件来获取本文件的任意变量

```python
getattr(sys.module[__name__],'变量名')
```

sys.module 存储了当前程序用到的所有模块

### 7.10 单例模式

无论实例化多少次使用的都是第一次创建的那个对象

```python
class Singleton(object):
    __instance = None
    def __new__(cls,*args,**kwargs):
        if not cls.__instance:
            cls.__instance = object.__new__(cls)
        return cls.instance

obj1 = Singleton()
obj2 = Singleton()

print(obj1 is obj2) #True
```

多次导入，不会重新加载。模块在第一次导入时，会加载到内存中，其他文件再次导入时不会重新加载。

通过模块的导入特性也可以实现单例模式。

```python
#test.py文件
class Singleton(object):
    pass

OBJ = Singleton()

#其他文件
import test
obj = test.OBJ
```

多线程下的单例模式

```python
import time
from threading import Lock,Thread

class Singleton(object):
    __instance = None
    lock = Lock()

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __new__(cls, *args, **kwargs):
        with cls.lock:
            if not cls.__instance:
                time.sleep(0.1)
                cls.__instance = super().__new__(cls)
        return cls.__instance

def func():
    obj = Singleton('wusir', 18)
    print(obj)

for i in range(10):
    Thread(target=func).start()
```

### 7.11 项目目录结构

#### 7.11.1 对于一个脚本

只有一个文件，在文件内导入模块编写代码即可。

#### 7.11.2 对于但可执行文件

- config文件夹  存放配置文件
- db文件夹  存放数据文件
- lib文件夹  存放公共功能，类库
- src文件夹 存放业务相关代码
- log文件夹  存放日志文件
- app.py  可执行文件

#### 7.11.3 对于多可执行文件

- config文件夹  存放配置文件

  - setting.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    import os
    
    BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    LOG_FILE_PATH = os.path.join(BASE_PATH,'log','debug.log')
    LOG_WHEN = 'd'
    LOG_INTERVAL = 1
    ```

- db文件夹  存放数据文件

- lib文件夹  存放公共功能，类库

  - log.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    import logging
    from logging import handlers
    from config import settings
    def get_logger():
        file_handler = handlers.TimedRotatingFileHandler(filename=settings.LOG_FILE_PATH,
                                                         when=settings.LOG_WHEN,			                                                                  interval=settings.LOG_INTERVAL, 
                                                         encoding='utf-8')
        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(module)s： %(message)s',
            level=logging.ERROR,
            handlers=[file_handler,]
        )
    
        return logging
    
    logger = get_logger()
    ```

- src文件夹 存放业务相关代码

  - run.py

    ```python
    #!/usr/bin/env python
    # -*- coding:utf-8 -*-
    
    from src import account
    def start():
        dic = {'1':account.login,'2':account.reg}
        print('1.登录\n2.注册')
    
        index = input('请选择')
        dic.get(index)()
    ```

  - account.py

    ```python
    from lib.log import logger
    def login():
        print('登录')
        logger.error('登录错误',exc_info=1)
    
    def reg():
        print('注册')
        logger.error('登录错误', exc_info=1)
    ```

- log文件夹  存放日志文件

- bin文件夹  存放可执行文件

  - student.py

    ```python
    import os
    import sys
    
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(PROJECT_PATH)
    from src import run
    
    if __name__ == '__main__':
        run.start()
    ```

  - teacher.py

    ```python
    import os
    import sys
    
    PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.append(PROJECT_PATH)
    from src import run
    
    if __name__ == '__main__':
        run.start()
    ```


### 7.12 新式类和经典类

- 新式类：
  - 支持super
  - 广度优先
  - 有mro方法
- 经典类：
  - 不支持super
  - 深度优先
  - 没有mro方法

继承顺序可以通过mro方法获得

```python
class A(object):
    def func(self):
        print('A')

class B(A):
    def func(self):
        print('B')

class C(A):
    def func(self):
        print('C')

class D(B,C):
    def func(self):
        print('D')

print(D.mro())#[<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>]
```

super的查找顺序和mro方法得到的顺序相同

面试题：

```python
class A(object):
    def __init__(self):
        self.__func()   #私有成员会变式成_A__func()
        
    def func(self):
        print('A')
        
class B(object):
    def func(self):
        print('B')
        
B().func() #打印A
```



## 八、网络编程

### 8.1 网络基础

#### 8.1.1 网络应用开发架构

- C/S架构：client-server
- B/S架构：browser-server

- B/S架构是特殊的C/S架构

#### 8.1.2 网络基础概念

- 网卡：一个实际存在的计算机硬件
- mac地址：网卡在出厂时设置好的一个地址，全球唯一。普通交换机只认识mac地址。
- 协议：server和client得到的内容都是二进制，所以每一位代表什么内容，对于计算机来说必须先约定好，再按照约定进行发送和解析。
- ip地址
  - IPv4协议: 4位的点分十进制，32位2进制表示。范围：0.0.0.0-255.255.255.255
  - IPv6协议：8位的冒分十六进制，128位二进制表示。范围：0:0:0:0:0:0:0:0-FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF:FFFF

- 公网ip：每个IP地址想要被所有人访问到，那么这个ip地址必须是你申请的。
- 内网ip：
  - 192.168.0.0-192.168.255.255
  - 172.16.0.0-172.31.255.255
  - 10.0.0.0-10.255.255.255

- 交换机负责进行局域网内通信，路由器负责局域网间通信。
- 交换机：广播、单播、组播
- arp协议：地址解析协议。已知一个机器的ip地址，获取该机器的mac地址（会用到广播、单播）
- 网关IP：是一个局域网的网络出口，访问局域网之外的区域都需要经过路由器和网关。
- 网段：指的一个ip地址段。
- 子网掩码：用来计算两台机器是否在同一网段内。将两个ip转换成二进制，分别和子网掩码按位与，得到的结果相同则在同一网段内。
- 端口：0-65535。自己写的程序一般设置8000以后的端口。

### 8.2 TCP协议

tcp协议是一种全双工通信，是基于连接的协议。

#### 8.2.1 建立和断开连接

建立连接：三次握手，只能由客户端发起连接请求

断开连接：四次挥手，双方都可以发起断开请求。因为客户端或服务端主动发起断开请求后，对方同意后但对方可能还有信息要发送，所以断开连接请求中间的通过和请求断开连接不能合并。

![](E:\Python\2019-05-07\day28\TCP.png)

#### 8.2.2 tcp特点

- 面向连接，
- 流式的，
- 可靠的，慢
- 全双工通信

在建立连接后：

- 发送的每一条信息都有回执
- 为了保证数据的完整性，还有重传机制

IO操作(in/out)：输入和输出是相对与内存来说的。

- write/send 输出
- read/recv 输入

#### 8.2.3 应用场景

文件的上传下载（发邮件、网盘、缓存电影）

### 8.3 UDP协议

#### 8.3.1 udp特点

- 无连接速度快
- 传输不可靠，可能会丢消息
- 能够传递的长度有限，和数据传递设备的设置有关（一般1500byte）

补充：是一个面向数据报，不可靠，快的，能完成一对一，一对多，多对多的高效通讯协议。（视频的在线观看，即时聊天）

#### 8.3.2 应用场景

即时通讯类（QQ、微信、飞秋的聊天系统）

### 8.4 osi七层模型

应用层、表示层、会话层、传输层、网络层、数据链路层、物理层。

有时把应用层、表示层、会话层统一称为应用层。所以有了以下osi五层模型

| 层号 |    名称    |              协议               |        物理设备        |
| :--: | :--------: | :-----------------------------: | :--------------------: |
|  5   |   应用层   | http,https,ftp,smtp(python代码) |                        |
|  4   |   传输层   |          tcp,udp,端口           | 四层路由器、四层交换机 |
|  3   |   网络层   |            ipv4,ipv6            |   三层交换机、路由器   |
|  2   | 数据链路层 |        mac地址，arp协议         |    网卡、二层交换机    |
|  1   |   物理层   |             二进制              |                        |

### 8.5 socket实现TCP、UDP

什么是socket?

socket(套接字)是工作在应用层和传输层之间的抽象层，帮助我们完成所有信息的组织和拼接。socket对程序员来说已经是网络操作的底层了。

#### 8.5.1 socket实现TCP

```python
#服务端
import socket

sk = socket.socket()#默认tcp协议（type=socket.SOCK_STREAM）
sk.bind(('192.168.12.39',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        recv_msg = conn.recv(1024)
        if recv_msg.decode().upper() == 'Q':
            conn.close()
            break
        print(recv_msg.decode('utf-8'))
        send_msg = input('请输入：')
        conn.send(send_msg.encode('utf-8'))
        if send_msg.upper() == 'Q':
            conn.close()
            break

sk.close()
```

```python
#客户端
import socket

sk = socket.socket()#默认tcp协议（type=socket.SOCK_STREAM）
sk.connect(('192.168.12.39',8066))
while True:
    send_msg = input('请输入：')
    sk.send(send_msg.encode('utf-8'))
    if send_msg.upper() == 'Q':
        break
    recv_msg = sk.recv(1024)
    if recv_msg.decode().upper() == 'Q':
        break
    print(recv_msg.decode('utf-8'))
sk.close()
```

#### 8.5.2 socket实现UDP

```python
#服务端
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)#udp协议
sk.bind(('127.0.0.1',8066))

while True:
    msg,addr = sk.recvfrom(1024)
    print(msg.decode('utf-8'))
    send_msg = input('>>>').encode('utf-8')
    sk.sendto(send_msg,addr)

sk.close()
```

```python
#客户端
import socket

sk = socket.socket(type=socket.SOCK_DGRAM)#udp协议
while True:
    send_msg = input('>>>')
    if send_msg.upper() == 'Q':
        sk.close()
        break
    sk.sendto(send_msg.encode('utf-8'),('127.0.0.1',8066))
    recv_msg = sk.recv(1024)
    print(recv_msg.decode('utf-8'))
```

### 8.6 粘包

#### 8.6.1 tcp粘包问题

tcp传输特点

- 无边界
- 流式传输

##### 8.6.1.1 什么是粘包

同时执行多条命令之后，得到的结果很可能只有一部分，在执行其他命令的时候又接收到之前执行的另外一部分结果，这种现象就是黏包。

##### 8.6.1.2 tcp的分包机制

当发送端缓冲区的长度大于网卡的MTU时，tcp会将这次发送的数据拆成几个数据包发送出去。 
MTU是Maximum Transmission Unit的缩写。意思是网络上传送的最大数据包。MTU的单位是字节。 大部分网络设备的MTU都是1500。如果本机的MTU比网关的MTU大，大的数据包就会被拆开来传送，这样会产生很多数据包碎片，增加丢包率，降低网络速度。

##### 8.6.1.3 tcp粘包原因

- 发送方的缓存机制
  - 发送端需要等缓冲区满才发送出去，造成粘包（发送数据时间间隔很短，数据了很小，会合到一起，产生粘包）
- 接收方的缓存机制
  - 接收方不及时接收缓冲区的包，造成多个包接收（客户端发送了一段数据，服务端只收了一小部分，服务端下次再收的时候还是从缓冲区拿上次遗留的数据，产生粘包） 

##### 8.6.1.4 总结

黏包现象只发生在tcp协议中：

1.从表面上看，黏包问题主要是因为发送方和接收方的缓存机制、tcp协议面向流通信的特点。

2.实际上，**主要还是因为接收方不知道消息之间的界限，不知道一次性提取多少字节的数据所造成的**

#### 8.6.2 解决粘包问题

##### 8.6.2.1 解决粘包问题

借助struct模块，我们知道长度数字可以被转换成一个标准大小的4字节数字。因此可以利用这个特点来预先发送数据长度。

我们还可以把报头做成字典，字典里包含将要发送的真实数据的详细信息，然后json序列化，然后用struck将序列化后的数据长度打包成4个字节（4个字节足够用了）

```python
#服务端
import struct,json,socket

sk = socket.socket(type=socket.SOCK_STREAM)
sk.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)#防止连接关闭后，端口没有马上被释放。可以复用端口
sk.bind(('127.0.0.1',8066))
sk.listen()
while True:
    conn,addr = sk.accept()
    while True:
        head_length = conn.recv(4) #接收包头长度
        head_length = struct.unpack('i',head_length)[0]
        body_length = json.loads(conn.recv(head_length).decode('utf-8'))['body_length']

        recv_length = 0
        recv_info = b''
        while recv_length < body_length:
            recv_info += conn.recv(body_length-recv_length)# 循环接收包未接收完的数据
            recv_length = len(recv_info)

        print(recv_info.decode('utf-8'))
    
    conn.close()
sk.close()
```

```python
#客户端
import json,struct,socket

sk = socket.socket(type=socket.SOCK_STREAM)
sk.connect(('127.0.0.1',8066))
while True:
    inp = input('>>>')
    if not inp:
        continue
    body_info = inp.encode('utf-8')
    head = {'body_length':len(body_info)}
    head_info = json.dumps(head).encode('utf-8')
    head_length = struct.pack('i',len(head_info))
    sk.send(head_length)
    sk.send(head_info)
    sk.send(body_info)
sk.close()
```

### 8.7 并发问题

#### 8.7.1 IO模型

1. 阻塞IO模型

2. 非阻塞IO模型

   非阻塞模式解决了多个客户端同时连接的问题。但提高了CPU的占用率，耗费了大量时间在异常处理。

   ```python
   #服务端
   import socket
   import time
   
   lst = [] #连接客户端列表
   del_lst = []#关闭连接的客户端列表
   
   sk = socket.socket()
   sk.setblocking(False)#设置socket为非阻塞模式，默认True
   sk.bind(('127.0.0.1',8066))
   sk.listen()
   while True:
       try:
           conn,addr = sk.accept()#非阻塞，无客户端连接会报错
           lst.append(conn)
       except BlockingIOError:
           for i in lst:
               try:
                   msg = i.recv(1024).decode('utf-8')#非阻塞，客户端无消息发送来，会报错
                   if not msg:  #在window操作系统下，客户端关闭后仍然会接收空消息
                       del_lst.append(i)
                       continue
                   print('>>>',msg)
                   i.send(msg.upper().encode('utf-8'))
                   time.sleep(1)
               except BlockingIOError:pass
           for i in del_lst:
               i.close()
               lst.remove(i) #移除掉关闭的客户端
           del_lst.clear()
   
   sk.close()
   ```

   ```python
   #客户端
   import time
   import socket
   sk = socket.socket()
   sk.connect(('127.0.0.1',8066))
   for i in range(5):
       sk.send(b'abc')
       msg = sk.recv(1024).decode('utf-8')
       print(msg)
       time.sleep(1)
   sk.close()
   ```

3. 事件驱动IO模型

4. IO多路复用模型

5. 异步IO模型

#### 8.7.2 socketserver模块

socketserver模块底层调用了socket模块，解决了客户端的并发连接问题。

```python
#服务端
import time
import socketserver

class Myserver(socketserver.BaseRequestHandler):
    def handle(self):
        #print(self.request) #conn对象
        while True:
            msg = self.request.recv(1024).decode('utf-8')
            if not msg: #客户端连接关闭后，msg为空
                self.request.close()
                break
            print('>>>',msg)
            self.request.send(msg.upper().encode('utf-8'))
            time.sleep(1)

sk = socketserver.ThreadingTCPServer(('127.0.0.1',8066),Myserver)
sk.serve_forever()#启动服务端，有客户端连接，会执行handle方法
```

```python
#客户端
import time
import socket

sk = socket.socket()
sk.connect(('127.0.0.1',8066))
for i in range(5):
    sk.send('sss'.encode('utf-8'))
    msg = sk.recv(1024).decode('utf-8')
    print(msg)
    time.sleep(1)
sk.close()
```

### 8.8 验证客户端的合法性

- 如果客户端是给客户用的，那么就没有必要验证客户端的合法性，只需要登录验证即可。

- 如果客户端是提供给机器使用的话，就需要验证客户端的合法性

  ```python
  #服务端
  import socket,os,hashlib
  
  sk = socket.socket()
  sk.bind(('127.0.0.1',8066))
  sk.listen()
  
  conn,addr = sk.accept()
  key = '123456' #和客户端使用相同的秘钥
  s = os.urandom(16) #服务端随机生成字符串，并发送给客户端
  conn.send(s)
  msg = conn.recv(1024).decode('utf-8') #接收客户端发来的验证串，比较是否和服务端一致
  obj = hashlib.md5(key.encode('utf-8'))
  obj.update(s)
  info = obj.hexdigest()
  if msg == info:
      conn.send('验证成功'.encode('utf-8'))
  else:
      conn.send('验证失败'.encode('utf-8'))
  conn.close()
  sk.close()
  ```

  ```python
  #客户端
  import socket,hashlib
  
  sk = socket.socket()
  sk.connect(('127.0.0.1',8066))
  
  msg = sk.recv(16)
  key = '123456'
  obj = hashlib.md5(key.encode('utf-8'))
  obj.update(msg)
  info = obj.hexdigest()
  sk.send(info.encode('utf-8'))
  msg = sk.recv(1024)
  print(msg.decode('utf-8'))
  sk.close()
  ```

  

## 九、并发编程

### 9.1 基础知识

#### 9.1.1 操作系统发展史

##### 9.1.1.1 人机矛盾

- cpu利用率低

##### 9.1.1.2 磁带存储+批处理

- 降低数据的读取时间
- 提高cpu利用率

##### 9.1.1.3 多道操作系统

- 数据隔离
- 时空复用
- 能够在一个任务遇到io操作时，主动把cpu让出来给其他任务使用。
- 任务切换过程也占用时间（操作系统来进行切换）

##### 9.1.1.4 分时操作系统

给时间分片，让多个任务轮流使用cpu

- 时间分片
  - cpu的轮转
  - 每个程序分配一个时间片
- 切换占用了时间，降低了cpu的利用率
- 提高了用户体验

##### 9.1.1.5 分时操作系统+多道操作系统

多个程序一起在计算机中执行

- 一个程序如果遇到了io操作，就切出去让出cpu
- 一个程序没有遇到io操作，但时间片到了，就让出cpu

补充：python中的分布式框架celery

#### 9.1.2 进程

##### 9.1.2.1 定义

运行中的程序。

##### 9.1.2.2 程序和进程的区别

程序：是一个文件。

进程：这个文件被cpu运行起来了

##### 9.1.2.3 基础知识

- 进程是计算机中最小的资源分配单位
- 进程在操作系统中的唯一标识符：pid
- 操作系统调度进程的算法
  - 短作业优先算法
  - 先来先服务算法
  - 时间片轮转算法
  - 多级反馈算法

##### 9.1.2.4 并发和并行区别

并行：两个程序，两个cpu，每个程序分别占用一个cpu，自己执行自己的。

并发：多个程序，一个cpu，多个程序交替在一个cpu上执行，看起来在同时执行，但实际上仍然是串行。

#### 9.1.3 同步和异步

同步：一个动作执行时，要想执行下一个动作，必须将上一个动作停止。

异步：一个动作执行的同时，也可以执行另一个动作

#### 9.1.4 阻塞和非阻塞

阻塞：cpu不工作

非阻塞：cpu工作

同步阻塞：如conn.revc()

同步非阻塞：调用一个func()函数，函数内没有io操作

异步非阻塞：把func()函数(无io操作)放到其他任务里去执行，自己执行自己的任务。

异步阻塞：

#### 9.1.5 进程的三状态图

![](E:\Python\2019-05-10\day31\进程三状态图.png)



### 9.2 进程

- 进程是计算机资源分配的最小单位（负责圈资源）。
- 进程的数据是隔离的
- 进程的创建，销毁，切换开销大

#### 9.2.1 基础知识

1. 子进程

   在父进程中创建子进程。在pycharm中启动的所有py程序都是pycharm的子进程。

2. 获取进程pid

   ```python
   import os
   os.getpid()#获取当前进程的pid
   os.getppid()#获取父进程的pid
   ```

3. 主进程的结束逻辑

	- 主进程的代码结束
	- 所有的子进程的代码结束
	- 主进程回收子进程资源
	- 主进程结束

4. 主进程如何知道子进程结束？

   基于网络、文件。

#### 9.2.2 Process类

Process类在multiprocessing包中。

```python
import os
import time
from multiprocessing import Process
def func():
    print('start',os.getpid())
    time.sleep(1)
    print('end',os.getpid())

if __name__ == '__main__':
    p = Process(target=func)
    p.start()#异步调用开启进程的方法，但并不等待这个进程真的开启(异步非阻塞)
    print('main:',os.getpid())
```

不同操作系统创建进程的方式不同

- windows操作系统开启进程的代码
  - 实际上新的子进程需要通过import父进程的代码来完成数据的导入工作。
  - 所以有一些内容我们只希望在父进程中完成，就写在`if __name__ == '__main__':`中。
- ios、linux操作系统创建进程 fork, 不执行代码,直接执行调用的func函数

join放法：阻塞父线程，直到子进程结束。

例：等10个子进程全部执行完毕

```python
import time
from multiprocessing import Process

def func(i):
    time.sleep(1)
    print(i)

if __name__ == '__main__':
    p_l = []
    for i in range(10):
        p = Process(target=func,args=(i,))
        p.start()
        p_l.append(p)

    for i in p_l:
        i.join()
    print('执行完毕')
```

进程对象和进程之间的关系？`p = Process(target=函数名,args=(参数,))`

- 进程对象和进程之间没有直接的关系，进程对象只是存储了一些和进程相关的内容，此时此刻操作系统还没有接到开启操作进程的指令
- ·`p.start()`向操作系统发送一个开启子进程的指令，发送完之后父进程继续执行自己的任务（异步非阻塞）

join() 同步阻塞，主进程阻塞，等待子进程执行结束。

#### 9.2.3 守护进程

守护进程在父进程的**代码结束**时也跟着结束。

通过设置daemon参数为True设置一个线程为守护线程

```python
import time
from multiprocessing import Process

def func():
    while True:
        print('is_alive')
        time.sleep(0.5)

if __name__ == '__main__':
    p = Process(target=func)
    p.daemon = True
    p.start()
    time.sleep(2)
    #结果：
    #is_alive
    #is_alive
    #is_alive
    #is_alive
```

```python
import time
from multiprocessing import Process

def func():
    while True:
        print('is_alive')
        time.sleep(0.5)

def func1():
    for i in range(5):
        print('123')
        time.sleep(1)

if __name__ == '__main__':
    p = Process(target=func)
    p1 = Process(target=func1)
    p.daemon = True
    p.start()
    p1.start()
    time.sleep(2)
    #结果：
    #is_alive
    #123
    #is_alive
    #is_alive
    #123
    #is_alive
    #123
    #123
    #123
```

应用场景：

- 生产者消费者模型
- 和守护线程做对比

Process类的其他方法：

- is_alive() 判断进程是否活着
- terminate() 强制结束进程（异步非阻塞）

#### 9.2.4 面向对象方式开启进程

```python
import os
import time
from multiprocessing import Process

class Myprocess(Process):
    def __init__(self,name,age):
        super().__init__()
        self.name = name
        self.age = age


    def run(self):
        while True:
            print(os.getpid(),'姓名:%s,年龄:%s'%(self.name,self.age,))
            time.sleep(0.5)


if __name__ == '__main__':
    mp = Myprocess('alex',18)
    mp.daemon = True
    mp.start()
    time.sleep(5)
```

#### 9.2.5 锁的概念

如果在一个并发的场景下，涉及到某部分所有进程共享的数据资源需要修改，那就需要加锁来维护数据安全。

```python
import json
import time
from multiprocessing import Process,Lock

def buy_ticket(i,lock):
    lock.acquire()#加锁
    time.sleep(0.02)
    with open('ticket.txt',mode='r',encoding='utf-8') as f:
        dic = json.load(f)
        count = dic['count']
    if count > 0:
        print('%s买到票了'%(i,))
        count -= 1
    else:
        print('%s没买到票'%(i,))
    time.sleep(0.02)
    with open('ticket.txt',mode='w',encoding='utf-8') as f:
        dic['count'] = count
        json.dump(dic,f)
    lock.release()#解锁


if __name__ == '__main__':
    lock = Lock()#在父进程中创建锁对象，用参数传到子进程中，以确保所有的子进程都使用一把锁
    for i in range(10):
        p = Process(target=buy_ticket,args=(i,lock))
        p.start()

#可以通过 with lock： 代替lock.acquire()和lock.release()。好处：with内部实现了异常处理，不会形成死锁。
import json
import time
from multiprocessing import Process,Lock

def buy_ticket(i,lock):
    with lock:
        time.sleep(0.02)
        with open('ticket.txt',mode='r',encoding='utf-8') as f:
            dic = json.load(f)
            count = dic['count']
        if count > 0:
            print('%s买到票了'%(i,))
            count -= 1
        else:
            print('%s没买到票'%(i,))
        time.sleep(0.02)
        with open('ticket.txt',mode='w',encoding='utf-8') as f:
            dic['count'] = count
            json.dump(dic,f)


if __name__ == '__main__':
    lock = Lock()#在父进程中创建锁对象，用参数传到子进程中，以确保所有的子进程都使用一把锁
    for i in range(10):
        p = Process(target=buy_ticket,args=(i,lock))
        p.start()

```

加锁保证了数据的安全，但也降低了执行效率。

在数据安全的基础上，才考虑效率问题。

应用场景：

- 共享数据资源（文件，数据库），且对资源进行修改删除操作。

#### 9.2.6 进程间的通信IPC(inter process communication)

##### 9.2.6.1 Queue

- 基于文件家族的socket实现
- 写文件基于pickle
- 基于Lock,天生数据安全

```python
from multiprocessing import Process,Queue

def func(queue):
    queue.put(1) #向队列中推入数据
    queue.put(2)
    queue.put(3)

if __name__ == '__main__':
    queue =  Queue()
    p = Process(target=func,args=(queue,))
    p.start()
    print(queue.get())#从队列中取出数据，先进先出，和推入的顺序一样，不会混乱
    print(queue.get())
    print(queue.get())
    print(queue.get())#当队列中无数据时会阻塞，直到另一个进程在向其中推入一个数据
```

```python
from multiprocessing import Process,Queue

def func(queue):
    queue.put(1)
    queue.put(2)
    queue.put(3)
    queue.put(4)
    queue.put(5) #当队列满了，再向其中推入数据时会阻塞，直到其他进程取走数据


if __name__ == '__main__':
    queue =  Queue(4) #可以设置队列的大小
    p = Process(target=func,args=(queue,))
    p.start()
```

put_nowait()和get_nowait()。当队列满了或队列空了不会阻塞，会报错(queue.Full，queue.Emply)。put_nowait()报错会丢失最后推入导致报错的数据。

```python
from multiprocessing import Process,Queue
import queue

def func(qu):
    try:
        qu.put_nowait(1)
        qu.put_nowait(2)
        qu.put_nowait(3)
        qu.put_nowait(4)
        qu.put_nowait(5)
    except queue.Full:
        pass

if __name__ == '__main__':
    qu =  Queue(4)
    p = Process(target=func,args=(qu,))
    p.start()
    print(qu.get())#1
    print(qu.get())#2
    print(qu.get())#3
    print(qu.get())#4
    print(qu.get())#阻塞，5丢失了
```

```python
from multiprocessing import Process,Queue
import queue,time

def func(qu):
    try:
        qu.put_nowait(1)
        qu.put_nowait(2)
        qu.put_nowait(3)
        qu.put_nowait(4)
    except queue.Full:
        pass

if __name__ == '__main__':
    qu =  Queue()
    p = Process(target=func,args=(qu,))
    p.start()
    time.sleep(2)
    try:
        print(qu.get_nowait())
        print(qu.get_nowait())
        print(qu.get_nowait())
        print(qu.get_nowait())
        print(qu.get_nowait())
    except queue.Empty:
        pass
```

##### 9.2.6.2 Pipe管道

- 基于文件家族的socket,pickle,天生不安全。

```python
from multiprocessing import Pipe
pip = Pipe()
pip.send()
pip.recv()
```

Queue相当于Pipe加上异常处理。

##### 9.2.6.2 第三方软件

除了内置的Queue和Pipe外（基于文件），还有第三方的工具/软件（基于网络）提供给了我们IPC机制。

- redis
- memcache
- kafka
- rabbitmq

使用第三方工具的好处：

- 集群（高可用）
- 并发需求
- 断电保存数据
- 解耦

#### 9.2.7 生产者消费者模型

##### 9.2.7.1 程序解耦

把写在一起的大的功能分开成多个小的功能处理。

优点：修改、复用方便，代码的可读性高。

##### 9.2.7.2 生产者消费者模型

生产者消费者模型利用了程序解耦的思想，将获取(生产)数据和处理数据分开，提高程序效率。

生产者（生产数据）--> 中间容器 -->消费者（处理数据）

生产者消费者模型小示例：

```python
import time
import random
from multiprocessing import Process, Queue


def producer(q, name):
    for i in range(10):
        time.sleep(random.random())
        goods = '产品%s' % (i,)
        s = '生产者%s生产了%s' % (name, goods,)
        q.put(goods)
        print(s)


def consumer(q, name):
    while True:
        time.sleep(random.random())
        goods = q.get()
        if not goods: break
        print('消费者%s购买了%s' % (name, goods,))


def cp(c_count, p_count):
    p_list = []
    q = Queue(10)
    for i in range(p_count):
        p = Process(target=producer, args=(q, i))
        p.start()
        p_list.append(p)
    for i in range(c_count):
        Process(target=consumer, args=(q, i)).start()
    for i in p_list:
        i.join()
    for i in range(c_count):
        q.put(None)


if __name__ == '__main__':
    cp(2, 3)
```

爬虫实例：

```python
import re
import requests
from multiprocessing import Process,Queue

def producer(q,url):
    response = requests.get(url)
    q.put(response.text)


def consumer(q):
    while True:
        s = q.get()
        if not s: break
        com = re.compile(
            '<div class="item">.*?<div class="pic">.*?<em .*?>(?P<id>\d+).*?<span class="title">(?P<title>.*?)</span>'
            '.*?<span class="rating_num" .*?>(?P<rating_num>.*?)</span>.*?<span>(?P<comment_num>.*?)评价</span>', re.S)
        ret = com.finditer(s)
        for i in ret:
            print({
                "id": i.group("id"),
                "title": i.group("title"),
                "rating_num": i.group("rating_num"),
                "comment_num": i.group("comment_num")}
            )


if __name__ == '__main__':
    count = 0
    q = Queue(10)
    p_list = []
    for i in range(10):
        url = 'https://movie.douban.com/top250?start=%s&filter=' % count
        count += 25
        p = Process(target=producer, args=(q, url,))
        p.start()
        p_list.append(p)
    Process(target=consumer, args=(q,)).start()
    for i in p_list:
        i.join()
    q.put(None)
```

进程

- 一个进程就是一个生产者
- 一个进程就是一个消费者

队列：中间容器

##### 9.2.7.3 JoinableQueue

原理：

![](E:\Python\08并发编程\day34\JoinableQueue原理.png)

```python
import time
import random
from multiprocessing import Process, Queue,JoinableQueue


def producer(q, name):
    for i in range(10):
        time.sleep(random.random())
        goods = '产品%s' % (i,)
        s = '生产者%s生产了%s' % (name, goods,)
        q.put(goods)
        print(s)
    q.join()

def consumer(q, name):
    while True:
        time.sleep(random.random())
        goods = q.get()
        print('消费者%s购买了%s' % (name, goods,))
        q.task_done()


def cp(c_count, p_count):
    p_list = []
    q = JoinableQueue(10)
    for i in range(p_count):
        p = Process(target=producer, args=(q, i))
        p_list.append(p)
        p.start()
    for i in range(c_count):
        c = Process(target=consumer, args=(q, i))
        c.daemon = True
        c.start()
    for i in p_list:
        i.join()

if __name__ == '__main__':
    cp(2, 3)
```

#### 9.2.8 进程之间共享数据

```python
from multiprocessing import Manager,Process,Lock

def func(dic,lock):
    with lock:
        dic['count'] -= 1

if __name__ == '__main__':
    # m = Manager()
    with Manager() as m:
        l = Lock()
        dic = m.dict({'count':100})
        p_l = []
        for i in range(100):
            p = Process(target=func,args=(dic,l))
            p.start()
            p_l.append(p)
        for p in p_l:p.join()
        print(dic)
```

### 9.3 线程

#### 9.3.1 初识线程

1. 一个进程中的多个线程可以利用多核，但Cpython解释器不能实现多线程利用多核。
2. 锁：全局解释器锁GIL(global interpreter lock)
   - 保证了整个python程序中同一时刻只能有一个线程被CPU执行
   - 原因：Cpython解释器中特殊的垃圾回收机制

3. pypy解释器有锁，Jpython无锁。
4. GIL锁导致了线程不能并行，可以并发。
   - 使用多线程并不影响高IO型的操作
   - 对高计算型的程序有效率上的影响（遇到高计算可以使用多进程+多线程，或者分布式）

#### 9.3.2 Thread类

##### 9.3.2.1 启动线程

multiprocessing是完全仿照threading的类写的。所以它们的操作类似

```python
import os
from threading import Thread

def func():
    print(os.getpid())#

t = Thread(target=func)
t.start()
print(os.getpid())#两个pid相同，它们属于同一进程
```

```python
#创建多个线程
from threading import Thread

def func(i):
    print('thread',i)


for i in range(10):
    t = Thread(target=func,args=(i,))
    t.start()
```

##### 9.3.2.2 join方法

阻塞，直到子线程执行结束

主线程等待所有子线程结束后才结束，主线程结束后，主进程也就结束了。

##### 9.3.2.3 面向对象方式启动线程

```python
from threading import Thread

class MyThread(Thread):
    def __init__(self,count):
        self.count = count
        super().__init__()

    def run(self):
        print('thread',self.count)
        print(self.ident) #ident线程id


for i in range(10):
    t = MyThread(i)
    t.start()
```

##### 9.3.2.4 线程中的其他方法

```python
from threading import Thread,enumerate,current_thread,active_count

def func():
    print(current_thread().ident) #current_thread()获取当前线程对象

t = Thread(target=func)
t.start()

print(enumerate()) #获取当前活着的线程对象，得到列表
print(active_count())#获取当前活着的线程数，等价于len(enumerate())
```

注：在线程中，不能在主线程结束一个子线程（没有terminate方法）

##### 9.3.2.5 守护线程

守护线程一直等到所有非守护线程都结束后才结束，除了守护主线程的代码之外，也会守护子线程。

```python
import time
from threading import Thread

def func1():
    while True:
        time.sleep(0.5)
        print('thread1')

def func2():
    for i in range(5):
        time.sleep(1)
        print('thread2')


t1 = Thread(target=func1)
t2 = Thread(target=func2)
t1.daemon = True
t1.start()
t2.start()
time.sleep(3)
#结果
'''
thread1
thread2
thread1
thread1
thread2
thread1
thread1
thread2
thread1
thread1
thread2
thread1
thread1
thread2
'''
```

#### 9.3.3 锁

即使是线程，即使有GIL锁，也会出现数据不安全的问题。

如：+=，-=，*=，/=之类的先运算后赋值的操作。

发生数据安全的问题的条件：

- 操作的是全局变量
- 做了以下操作：+=，-+，*=，/=，还包括lst[0]+=1,dict['key'] +=1等。

线程加锁也会影响程序的执行效率，但保证了数据的安全。

##### 9.3.3.1 互斥锁

互斥锁是锁的一种，在同一个线程中，不能连续acquire多次。

```python
from threading import Lock
lock = Lock()
lock.acquire()
print('*'*20)  #***********************
lock.acquire() #阻塞，等待lock.release()
print('-'*20)
```

##### 9.3.3.2 死锁现象

```python
import time
from threading import Thread,Lock

def eat1(noodle_lock,fork_lock,name):
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    fork_lock.acquire()
    print('%s拿到了叉子'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下了面条'%(name))
    fork_lock.release()
    print('%s放下了叉子'%(name))

def eat2(noodle_lock,fork_lock,name):
    fork_lock.acquire()
    print('%s拿到了叉子' % (name))
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下了叉子' % (name))
    noodle_lock.release()
    print('%s放下了面条'%(name))

user_list = ['alex','wusir','taibai','yuan']
noodle_lock = Lock()
fork_lock = Lock()
Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[0])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[1])).start()
Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[2])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[3])).start()

#alex拿到了面条
#alex拿到了叉子
#alex吃了口面条
#alex放下了面条
#taibai拿到了面条
#alex放下了叉子
#wusir拿到了叉子
```

死锁现象是如何发生的？

- 有多把锁（一把以上）
- 多把锁交替使用

解决办法：

- 使用递归锁（解决死锁问题的根本原因：将多把互斥锁变成一把递归锁）
  - 使用递归锁可以快速解决死锁现象，但是效率差

- 优化代码
  - 通过代码优化还是用互斥锁解决死锁现象，执行效率高，但解决死锁现象的效率低（尤其代码复杂的情况）

通过代码优化解决死锁现象

```python
import time
from threading import Thread,Lock

def eat1(lock,name):
    lock.acquire()
    print('%s拿到了面条' % (name))
    print('%s拿到了叉子'%(name))
    print('%s吃了口面条' % (name))
    time.sleep(0.1)
    print('%s放下了面条'%(name))
    print('%s放下了叉子'%(name))
    lock.release()

def eat2(lock,name):
    lock.acquire()
    print('%s拿到了叉子' % (name))
    print('%s拿到了面条'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    print('%s放下了叉子' % (name))
    print('%s放下了面条' % (name))
    lock.release()

user_list = ['alex','wusir','taibai','yuan']
lock = Lock()
Thread(target=eat1,args=(lock,user_list[0])).start()
Thread(target=eat2,args=(lock,user_list[1])).start()
Thread(target=eat1,args=(lock,user_list[2])).start()
Thread(target=eat2,args=(lock,user_list[3])).start()
```

##### 9.3.3.3  递归锁

在同一个线程中，可以连续acquire多次，不会阻塞。

优点：在同一个线程中多次acquire多次，不会阻塞，但也要release同样的次数。

缺点：占用了更多地资源

递归锁也会发生死锁现象，多把递归锁交替使用也会产生死锁现象。

```python
import time
from threading import Thread,RLock

def eat1(noodle_lock,fork_lock,name):
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    fork_lock.acquire()
    print('%s拿到了叉子'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    noodle_lock.release()
    print('%s放下了面条'%(name))
    fork_lock.release()
    print('%s放下了叉子'%(name))

def eat2(noodle_lock,fork_lock,name):
    fork_lock.acquire()
    print('%s拿到了叉子' % (name))
    noodle_lock.acquire()
    print('%s拿到了面条'%(name))
    print('%s吃了口面条'%(name))
    time.sleep(0.1)
    fork_lock.release()
    print('%s放下了叉子' % (name))
    noodle_lock.release()
    print('%s放下了面条'%(name))

user_list = ['alex','wusir','taibai','yuan']
noodle_lock = fork_lock = RLock()#定义了同一把递归锁

Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[0])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[1])).start()
Thread(target=eat1,args=(noodle_lock,fork_lock,user_list[2])).start()
Thread(target=eat2,args=(noodle_lock,fork_lock,user_list[3])).start()
```

#### 9.3.4 线程队列

queue模块，不能进行进程间通信，可以进行线程间通信。

##### 9.3.4.1 先进先出队列

通常用于用户请求，写一个server，所有的用户的请求放到队列中。先来先服务思想。

```python
from queue import Queue

q = Queue()
q.put(1)
q.put(2)
q.put(3)
print(q.get())#1
print(q.get())#2
print(q.get())#3

```

##### 9.3.4.2 后进先出队列(lifo)

last in first out 栈，多用于算法。

```python
from queue import LifoQueue

q = LifoQueue()
q.put(1)
q.put(2)
q.put(3)
print(q.get())#3
print(q.get())#2
print(q.get())#1
```

##### 9.3.4.3 优先级队列

多用于自动排序

```python
from queue import PriorityQueue

q = PriorityQueue()
q.put((10,'alex'))
q.put((4,'wusir'))
q.put((100,'taibai'))
print(q.get()) #(4,'wusir')
print(q.get()) #(10,'alex')
print(q.get()) #(100,'taibai')
```

#### 9.3.5 池

预先开启固定个数的进程数/线程数，当任务来了，直接提交给已经开好的进程/线程去执行就可以了。

优点：

- 节省了进程/线程开启，关闭，切换的时间
- 减轻了操作系统的调度负担

##### 9.3.5.1 进程池

```python
import random,os,time
from concurrent.futures import ProcessPoolExecutor

def func():
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())

if __name__ == '__main__':
    pool = ProcessPoolExecutor(5)
    for i in range(10):
        pool.submit(func)
    pool.shutdown() #关闭池，不能再继续提交任务，并且会阻塞，直到所有已经提交的任务全部执行完成
    print('main')
```

```python
#参数和返回值
import random,os,time
from concurrent.futures import ProcessPoolExecutor

def func(i):
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
    return '%s+%s'%(os.getpid(),i,)

if __name__ == '__main__':
    pool = ProcessPoolExecutor(5)
    ret_list = []
    for i in range(10):
        ret = pool.submit(func,i) #返回一个对象
        ret_list.append(ret)
    for ret in ret_list:
        print(ret.result()) #调用result()方法等到返回值，会阻塞直到返回结果
    print('main')
```

##### 9.3.5.2 线程池

用法和进程池类似

```python
import random,os,time
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
    return '%s+%s'%(os.getpid(),i,)


pool = ThreadPoolExecutor(20)
ret_list = []
for i in range(10):
    ret = pool.submit(func,i) #返回一个对象
    ret_list.append(ret)
for ret in ret_list:
    print(ret.result()) #调用result()方法等到返回值，会阻塞直到返回结果
print('main')
```

池的map方法：

```python
import random,os,time
from concurrent.futures import ThreadPoolExecutor

def func(i):
    print('start',os.getpid())
    time.sleep(random.randint(1,3))
    print('end',os.getpid())
    return '%s+%s'%(os.getpid(),i,)


pool = ThreadPoolExecutor(20)
ret = pool.map(func,range(10)) #将可迭代对象的每个元素当做参数传入函数中，返回一个生成器，生成器内部是函数返回值
pool.shutdown()
print('main')
for i in ret:
    print(i)
```

池的回调函数add_down_callback()

```python
import random,os,time,requests
from concurrent.futures import ThreadPoolExecutor

def func(url):
    res = requests.get(url)
    return {'url':url,'content':res.text}

def func2(ret):
    dic = ret.result()
    print(dic['url'])

url_list = [
    'http://www.baidu.com',
    'http://www.cnblogs.com',
    'http://www.douban.com',
    'http://www.tencent.com',
    'http://www.cnblogs.com/Eva-J/articles/8306047.html',
    'http://www.cnblogs.com/Eva-J/articles/7206498.html',
]

pool = ThreadPoolExecutor(20)
for url in url_list:
    ret = pool.submit(func,url)
    ret.add_done_callback(func2)#绑定回调函数，哪个任务先执行完，哪个就先调用回调函数，提高了效率
```

#### 9.3.6 协程

##### 9.3.6.1 概念

1. 用户级别的，由我们自己写的python代码来控制切换，并且是操作系统不可见的。
2. 在Cpython解释器下，由于多线程本身不能利用多核，所以即便开启了多线程，也只能轮流在一个CPU上行。协程如果把所有任务的IO操作都规避掉，只剩下要使用CPU的操作，就意味着协程就可以做到提高CPU利用率的效果。
3. 多线程和协程都不能利用多核，只能在一个cpu上来回切换。
4. 线程和协程对比：
   - 线程：切换需要操作系统完成，开销大，操作系统控制切换的过程不可控，给操作系统的压力大。但操作系统对IO操作感知更加敏感。
   - 协程：切换由python代码完成，开销小，用户控制切换过程可控，完全不会增加操作系统的压力。但用户级别对IO操作的感知灵敏度较低。

##### 9.3.6.2 切换问题

1. 协程：能够在一个线程下的多个任务之间来回切换，那么每个任务都是一个协程。(只有在一个协程遇到io操作时，才会切换到另一个协程)

2. 两种切换方式

   - 原生python完成

     - 使用yield
     - asyncio模块（内置模块，原生靠近底层，使用不方便）

   - C语言完成的python模块

     - greenlet模块（只能完成任务切换，不能解决IO操作问题）

       ```python
       import time
       from greenlet import greenlet
       
       def eat():
           print('start eat')
           time.sleep(0.5)
           g2.switch() #切换到sleep任务
           print('end eat')
       
       def sleep():
           print('start sleep')
           time.sleep(0.5) #并不能解决io阻塞
           print('end sleep')
           g1.switch() # 若想eat继续执行，还要切回去
       
       g1 = greenlet(eat)
       g2 = greenlet(sleep)
       g1.switch() #相当于一个开关
       ```

     - gevent模块（第三方模块，使用方便）

##### 9.3.6.3 gevent模块

基于greenlet进行切换。

```python
import time
import gevent
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def eat():
    print('start eat')
    time.sleep(0.5)
    print('end eat')

def sleep():
    print('start sleep')
    time.sleep(1)
    print('end sleep')

g1 = gevent.spawn(eat) #创建一个协程任务
g2 = gevent.spawn(sleep)
g1.join() # 阻塞直到g1协程执行完成，若无阻塞则协程任务g1和g2不会执行(有阻塞才会任务切换)
g2.join()
```

带参数的协程

```python
import time
import gevent
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def eat(name):
    print('%s start eat'%(name,))
    time.sleep(0.5)
    print('%s end eat'%(name,))

def sleep(name):
    print('%s start sleep'%(name,))
    time.sleep(1)
    print('%s end sleep'%(name,))

g1 = gevent.spawn(eat,'wusir') #创建一个协程任务
g2 = gevent.spawn(sleep,'alex')
gevent.joinall([g1,g2]) #阻塞直到两个协程任务都执行完成
```

带返回值的协程

```python
import time
import gevent
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def eat(name):
    print('%s start eat'%(name,))
    time.sleep(0.5)
    print('%s end eat'%(name,))
    return name

def sleep(name):
    print('%s start sleep'%(name,))
    time.sleep(1)
    print('%s end sleep'%(name,))
    return name

g1 = gevent.spawn(eat,'wusir') #创建一个协程任务
g2 = gevent.spawn(sleep,'alex')
gevent.joinall([g1,g2]) #阻塞直到两个协程任务都执行完成
print(g1.value,g2.value) #wusir alex
```

协程实现并发socket

```python
#服务端
import gevent
import socket
from gevent import monkey
monkey.patch_all() #gevent将几乎所有的阻塞方法都重写了一遍，调用patch_all后，阻塞方法便不再是原来的阻塞方法了

def func(conn):
    while True:
        msg = conn.recv(1024).decode('utf-8')
        conn.send(msg.upper().encode('utf-8'))

sk = socket.socket()
sk.bind(('127.0.0.1',8066))
sk.listen()

while True:
    conn,addr = sk.accept()
    gt = gevent.spawn(func,conn)

#多线程客户端
import socket,time
from threading import Thread

def func():
    sk = socket.socket()
    sk.connect(('127.0.0.1',8066))
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)


for i in range(10):
    Thread(target=func).start()
 
#协程客户端
import socket,time
import gevent
from gevent import monkey
monkey.patch_all()

def func():
    sk = socket.socket()
    sk.connect(('127.0.0.1',8066))
    while True:
        sk.send(b'hello')
        msg = sk.recv(1024).decode('utf-8')
        print(msg)
        time.sleep(0.5)

g_list = []
for i in range(10):
    g = gevent.spawn(func)
    g_list.append(g)
gevent.joinall(g_list)
```

##### 9.3.6.4 asyncio模块

基于yield进行切换。

创建一个任务

```python
import asyncio

async def func(): #定义了一个协程函数
    print('start func')
    await asyncio.sleep(0.5)  #await关键字必须放在协程函数中 阻塞
    print('end func')

loop = asyncio.get_event_loop() #创建一个事件循环
loop.run_until_complete(func()) #把func函数放入事件循环中执行(函数需要带括号) 阻塞
```

创建多个任务

```python
import asyncio

async def func(): #定义了一个协程函数
    print('start func')
    await asyncio.sleep(0.5)  #await关键字必须放在协程函数中 阻塞
    print('end func')

loop = asyncio.get_event_loop() #创建一个事件循环
obj = asyncio.wait([func(),func(),func()])#创建一个对象，内部封装几个任务
loop.run_until_complete(obj) #把对象放入事件循环中执行 阻塞
```

启动多个任务且有返回值

```python
import asyncio

async def func(): #定义了一个协程函数
    print('start func')
    await asyncio.sleep(0.5)  #await关键字必须放在协程函数中 阻塞
    print('end func')
    return 123

loop = asyncio.get_event_loop() #创建一个事件循环
t1 = loop.create_task(func()) #创建任务
t2 = loop.create_task(func())
task_list = [t1,t2]
obj = asyncio.wait(task_list)
loop.run_until_complete(obj) #把对象放入事件循环中执行 阻塞
for i in task_list:
    print(i.result()) #会阻塞

```

先执行完的先去结果

```python
import asyncio
async def demo(i):   # 协程方法
    print('start')
    await asyncio.sleep(10-i)  # 阻塞
    print('end')
    return i,123

async def main():
    task_l = []
    for i in range(10):
        task = asyncio.ensure_future(demo(i))
        task_l.append(task)
    for ret in asyncio.as_completed(task_l):
        res = await ret
        print(res)

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
```

- await 阻塞 协程函数这里要切换出去，还能保证一会儿再切回来
- await 必须写在async函数里，async函数是协程函数
- loop 事件循环,所有的协程的执行 调度 都离不开这个loop

## 十、数据库

### 10.1 基础知识

#### 10.1.1 数据库概念

1. C/S架构的，操作数据文件的一个管理软件

- 帮助我们解决并发问题
- 能够帮助我们用更简单更快速的方式完成数据的增删改查
- 能够给我们提供一些容错、高可用的机制
- 权限的认证

2. 数据库管理系统（DBMS），专门用来管理数据文件，帮助用户更简洁的操作数据的软件。
   - 关系型数据库
     - sql server
     - oracle 收费、比较严谨、安全性比较高
       - 国企、事业单位
       - 银行、金融行业
     - mysql 开源的
       - 小公司
       - 互联网公司
     - sqlite
   - 非关系型数据库
     - redis
     - mongodb
3. 数据库管理员（DBA）

#### 10.1.2 数据库的安装

1. 解压安装包，将my-default.ini文件复制一份，重命名为my.ini，修改my.ini配置文件内容

   ```python
   [mysql] #客户端的配置
   # 设置mysql客户端默认字符集
   default-character-set=utf8 
   
   [mysqld]   #服务端的配置
   #设置3306端口
   port = 3306 
   # 设置mysql的安装目录
   basedir=D:\mysql\mysql-5.6.44-winx64 
   # 设置mysql数据库的数据的存放目录
   datadir=D:\mysql\mysql-5.6.44-winx64\data 
   # 允许最大连接数
   max_connections=200
   # 服务端使用的字符集默认为8比特编码的latin1字符集
   character-set-server=utf8
   # 创建新表时将使用的默认存储引擎
   default-storage-engine=INNODB
   ```

2. 添加环境变量，将安装文件夹下的bin文件夹路径添加到Path环境变量。

3. 以管理员身份运行cmd,分别执行如下指令

   ```python
   mysqld install #安装mysql服务
   net start mysql #启动mysql服务
   mysql -u root -p #登录数据库，用户为root，后面让输入密码，直接回车即可。
   
   net stop mysql #mysql服务没有没有重启命令，只能关闭再重新打开
   
   ```

#### 10.1.3 存储引擎

##### 10.1.3.1 MyISAM

mysql 5.5及以下的版本的默认存储方式。

- 存储文件个数：表结构，表中的数据，索引
- 支持表级锁
- 不支持行级锁，不支持事务，不支持外键

##### 10.1.3.2  InnoDB

mysql 5.6及以上版本默认的存储方式

- 存储文件个数：表结构，表中的数据

- 支持行级锁和表级锁，默认为行级锁
- 支持事务，支持外键

##### 10.1.3.3 MEMORY

数据存储在内存中

- 存储的文件个数：表结构
- 优点：增删改查速度都很快
- 缺点：重启后数据消失，容量有限

##### 10.1.3.4 相关语句

1. 查看配置项

   ```sql
   show variables;--查看所有配置项
   show variables like '%engine%';--查看类似engine相关的配置项
   ```

2. 创建表

   ```sql
   --创建一个MyISAM存储方式的表
   create table student(id int,name char(4)) engine=MyISAM;
   ```

3. 查看表结构

   ```sql
   desc student;--查看这张表字段的基础信息
   show create table student;--查看表的所有相关信息，如引擎
   show create table student \g --查看表的相关信息，并格式化查询结果，结果更容易查看
   ```

##### 10.1.3.5  引擎总结

为什么要用innoDB？

- innoDB引擎支持事务，对含有支付业务的项目更安全。

- 支持行级锁能更好的处理并发问题。

### 10.2 数据库语句

数据库语句分为三种：

- 数据库定义语句(DDL)：创建库、创建表等。
- 数据库操纵语句(DML)：数据的增删改查。
- 数据库控制语句(DCL)：数据库权限的控制。

1. 登录数据库

   数据库在安装完成之后，有一个最高权限的用户root。

   ```sql
   --登录本地数据库
   mysql -uroot -p  --mysql -u用户名 -p
   
   --登录远程数据库
   mysql -uroot -h192.168.12.39 -p  --mysql -u用户名 -h远程IP -p
   ```

2. 查看当前用户

   ```sql
   select user();
   ```

3. 设置密码

   ```sql
   set password = password();
   ```

4. 创建用户

   ```sql
   --创建用户wkx，密码为123，只有ip在192.168.12段的ip可以登录该用户
   create user 'wkx'@'192.168.12.%' identified by '123';
   --创建用户wkx,任何ip都可以访问（只要可以访问到服务器），不设置密码
   create user 'wkx'@'%';
   ```

5. 授权

   ```sql
   --设置wkx的权限为所有数据库所有权限
   grant all on * to 'wkx'@'192.168.12.%'; 
   --设置wkx的权限为test数据库下的所有操作权限
   grant all on test.* 'wkx'@'192.168.12.%';
   --设置wkx的权限为test数据库下的查询权限
   grant select on test.* to 'wkx'@'192.168.12.%';
   --设置权限的同时也可以创建用户
   grant all on test.* to 'wkx'@'192.168.12.%' identified by '123';
   ```

6. 查看数据库

   ```sql
   show databases;
   ```

7. 创建数据库

   ```sql
   --创建数据库test
   create database test;
   ```

8. 查看当前使用的数据库

   ```sql
   select database();
   ```

9. 切换数据库

   ```sql
   use test; -- use 数据库  切换到test数据库
   ```

10. 查看当前数据库的表

    ```sql
    show tables ;
    ```

11. 创建表

    ```sql
    --创建student表，表内有两个字段id(int型)和name(char类型最大长度为4)
    create table student(id int, name char(4));
    ```

12. 删除表

    ```sql
    drop table student; --删除student表
    ```

13. 查看表结构

    ```sql
    desc student; --查看student表的表结构
    ```

14. 添加数据

    ```sql
    insert into student values(1,'wusir');
    insert into student (id,name)values(1,'wusir');
    ```

15. 查询数据

    ```python
    select * from student;
    ```

16. 修改数据

    ```sql
    update student set name='alex' where id=1;
    ```

17. 删除数据

    ```sql
    delete from student where id=1;
    ```

### 10.3 数据类型

#### 10.3.1 数字类型

##### 10.3.1.1 整型 int

- 整型默认为有符号的

- 它所表示的数字的范围不被宽度约束，所以一般不会对int进行长度约束

- 它只能约束数字的显示宽度

  ```sql
  create table t1 (id1 int unsigned,id2 int); --创建t1表，id1字段为为无符号整型，id2为有符号整型
  ```

##### 10.3.1.2 小数 float

- 单精度小数

```sql
create table t1(f1 float(5,2)); --f1字段总长度为5，小数点后保留两位，自动四舍五入
```

#### 10.3.2 日期和时间类型

##### 10.3.2.1 年 year

- 只存储年份

  ```sql
  create table t3(y1 year);
  ```

##### 10.3.2.2 年月日 date

- 只包括年月日，存储格式为yyyy-mm-dd，可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

  ```sql
  create table t4(d1 date);
  insert into t4 values('2018-02-03');
  insert into t4 values(20180203);
  insert into t4 values(now()); --now()为一个函数，获取当前日期时间
  ```

##### 10.3.2.3 时分秒 time

- 只包括时分秒，存储格式为HH:mm:ss，可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

  ```sql
  create table t5(t1 time);
  insert into t5 values('12:12:54');
  insert into t5 values(121254);
  insert into t5 values(now());
  ```

##### 10.3.2.4 年月日时分秒 datetime

- 存储格式为yyyy-mm-dd HH:MM:SS,可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

  ```sql
  create table t6(dt1 datetime);
  insert into t6 values('2018-03-30 12:25:30');
  insert into t6 values(20180330122530);
  insert into t6 values(now());
  ```

##### 10.3.2.4 时间戳类型 timestamp

- 存储格式为yyyy-mm-dd HH:MM:SS,可以以字符串或数字的形式输入，内部会自动转换格式。但位数不能少。

- 默认值为 CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP，每次更新时，该类型的字段都会更新为当前时间。添加一条数据时，默认值也为当前时间

  ```sql
  create table t7(ts1 timestamp);
  insert into t7 values(null) --默认值为当前时间
  ```

#### 10.3.3 字符串类型

##### 10.3.3.1 定长的单位 char

- 定长的单位，不足会自动在后面补空格，查询时会把后面的空格去掉。如果插入时末尾本身就带有空格，也会被去掉。

- 浪费空间，存取效率相对较高。

  ```sql
  create table t8(c1 char(10));
  insert into t8 values('alex');
  ```

##### 10.3.3.2 变长的单位 varchar

- 节省空间，存取效率相对较低。

  ```sql
  create table t9(vc1 varchar(10));
  insert into t9 values('alex');
  ```

##### 10.3.3.3 总结

- 手机号、身份证号长度不变的，用char
- 用户名、密码等限制位数区间的也用char
- 对于评论，微博，说说等，使用varchar

#### 10.3.4 enum和set

##### 10.3.4.1 枚举 enum

- 单选，在表定义时，会定义好选择范围，插入数据时若数据不再范围内，则无法插入

  ```sql
  create table t10(gender enum('male','female'));
  insert into t10 values('male')
  ```

##### 10.3.4.2 集合 set

- 多选，在表定义时，会定义好选择范围，插入时会自动去重，以及去掉不存在的值

  ```sql
  create table t13 (hobby set('抽烟','喝酒','烫头'));
  insert into t13 values('抽烟,喝酒,洗脚,抽烟');
  ```

### 10.4 约束

#### 10.4.1 无符号 unsigned

```sql
create table t1(id int unsigned);
```

#### 10.4.2 不为空 not null

```sql
create table t2(id int not null);
```

#### 10.4.3 默认值 default

```sql
 create table t3(id int not null,age int default 18,
gender enum('male','female') default 'male');
```

#### 10.4.4 唯一 unique

设置某一个字段不能重复

```sql
create table t4(id int not null unique,name char(4)); 

--联合唯一
create table t4(id int not null,name char(4) default 'alex',unique(id,name));--id和name联合唯一
```

#### 10.4.5 自增 auto_increment

设置某一个int类型的字段 自动增加,auto_increment自带not null效果,设置条件int，unique

```sql
create table t5 (id int auto_increment unique,name char(4));
```

#### 10.4.6 主键 primary key

- 设置某一个字段非空且不能重复

- 约束力相当于 not null + unique

- 一张表只能由一个主键

- 你指定的第一个非空且唯一的字段会被定义成主键

  ```sql
  create table t6 (id int not null unique,name char(4) not null unique);--你指定的第一个非空且唯一的字段会被定义成主键
  
  create table t7(id int primary key,name char(4));
  
  --联合主键
  create table t8(id int,ip char(15),server char(10),port int,primary key(ip,port));--两个组合唯一
  ```

#### 10.4.7 外键 foreign key

- 涉及到两张表

  ```sql
  --员工表
  create table staff(id int primary key, age int default 18, gender enum('male','female') default 'male', post_id int,foreign key (post_id) references post(pid));
      
  --部门表
  create table post(pid int primary key,name char(5));
  
  
  --级联删除和级联更新（操作的都是从表）
  create table staff2(
  id  int primary key auto_increment,
  age int,
  gender  enum('male','female'),
  post_id int,
  foreign key(post_id) references post(pid) on update cascade on delete set null
  )--删除时，将外键置为空
  
  create table staff2(
  id  int primary key auto_increment,
  age int,
  gender  enum('male','female'),
  post_id int,
  foreign key(post_id) references post(pid) on update cascade on delete cascade
  )--级联更新删除
  
  
  update post set pid=2 where pid = 1;--级联更新会把staff的外键一起更新，更新staff的外键值，若post中不存在，则报错
  delete from post where pid = 1;--会把主表的外键变为null
  ```

### 10.5 修改表结构

#### 10.5.1 添加字段

```sql
--alter table 表名 add 添加字段
alter table staff add(name char(4),sno int);

--alter table 表名 add 字段名 数据类型(宽度) 约束 first/after name
```

#### 10.5.2 删除字段

```sql
--alter table 表名 drop 删除字段
alter table staff drop name;
```

#### 10.5.3 修改字段

- 修改已经存在的字段  的类型 宽度 约束

```sql
--alter table 表名 modify 修改已经存在的字段  的类型 宽度 约束
alter table staff modify sno char(4);
```

- 修改已经存在的字段  的类型 宽度 约束 和 字段名字

```sql
--alter table 表名 change 修改已经存在的字段  的类型 宽度 约束 和 字段名字
alter table staff change nnno name char(4);
```

```sql
--alter table 表名 modify age int not null after id;
--alter table 表名 modify age int not null first;
```

### 10.6 表关系

book 书：id,name,price,author_id

author 作者：aid,name,birthday,gender

#### 10.6.1 一对多

```sql
--作者表（一）
create table author(
aid int primary key,
name char(12) not null,
birthday date,
gender enum('male','female') default 'male'
)

--书表（多）
create table book(
id int primary key,
name char(12) not null,
price float(5,2),
author_id int,
foreign key(author_id) references author(aid)
)
```

#### 10.6.2 一对一

```sql
--作者表（一）
create table author(
aid int primary key,
name char(12) not null,
birthday date,
gender enum('male','female') default 'male'
)

--书表（一）
create table book(
id int primary key,
name char(12) not null,
price float(5,2),
author_id int unique,
foreign key(author_id) references author(aid)
)
```

#### 10.6.3 多对多

```sql
--作者表（多）
create table author(
aid int primary key,
name char(12) not null,
birthday date,
gender enum('male','female') default 'male'
)

--书表（多）
create table book(
id int primary key,
name char(12) not null,
price float(5,2)
)

--中间表，存储两表关系
create table book_author(
id int primary key,
bid int not null,
author_id int not null,
foreign key(bid) references book(id),
foreign key(author_key) references author(bid)
)
```

### 10.7 单表查询

#### 10.7.1 where 

##### 10.7.1.1 比较运算符

```sql
-- > < = >= <= != <>
select * from 表名 where 字段 > 值;
```

##### 10.7.1.2 范围筛选

1. 多选一   in

   ```sql
   select * from 表名 where 字段名 in (值1,值2,值3,...);
   ```

2. 范围筛选 between   and

   - 表示一个范围

   ```sql
   select * from 表名 where 字段名 between 值1 and 值2;
   ```

3. 模糊查询 like

   - 通配符 %，匹配任意长度的任意字符

     ```sql
     select * from 表名 where 字段名 like 'a%';--匹配所有以a开头的行
     ```

   - 通配符 _，匹配一个字符长度的任意字符

     ```sql
     select * from 表名 where 字段名 like 'a_';--匹配所有以a开头，且长度为2的行
     ```

4. 正则匹配

   - 更加细粒度的匹配的时候

     ```sql
     select * from 表名 where 字段名 regexp '正则表达式';
     -- 查找所有以j开头，长度为5 后四位为小写字母的用户名
     # select * from employee where emp_name regexp '^j[a-z]{5}'
     ```

##### 10.7.1.3 逻辑运算

- and，or，not

  ```sql
  -- and 筛选满足所有条件的项
  select * from 表名 where 字段名1 = 值1 and 字段名2 = 值2;
  -- or 筛选满足任意一个条件的项
  select * from 表名 where 字段名1 = 值1 or 字段名2 = 值2;
  -- not 不满足条件的项
  select * from 表名 where 字段名 not in (值1,值2,...);
  ```

##### 10.7.1.4 身份运算符

- is， null永远不等于null

  ```sql
  select * from 表名 where 字段名 is null;
  ```

#### 10.7.2 group by 分组

```sql
--会把group by后面的这个字段，也就是post字段中的每一个不同的项都保留下来，有为post列去重的效果。并且把值是这一项的所有行归为一组。
select * from employee group by post;
```

- 聚合：把多行的同一个字段进行一些统计，最终得到一个结果。

  - count(字段)   统计这个字段有多少项。
  - sum(字段)   统计这个字段对应的数值的和
  - avg(字段)   统计这个字段对应的数值的平均值
  - min(字段)  统计这个字段对应数值的最小值
  - max(字段)  统计这个字段对应数值的最大值
  - group_concat(字段)  分组聚合

- 分组聚合

  - 求各个部门的人数

    ```sql
    select post,count(*) from employee group by post;
    ```

  - 求公司里 男生 和女生的人数

    ```sql
    select sex,count(*) from employee group by sex;
    ```

  - 求各部门的平均薪资

    ```sql
    select post,avg(salary) from employee group by post;
    ```

  - 求各部门的平均年龄

    ```sql
    select post,avg(age) from employee group by post;
    ```

  - 求各部门年龄最小的

    ```sql
    select post,min(age) from employee group by post;
    ```

  - 求各部门年龄最大的

    ```sql
    select post,max(age) from employee group by post;
    ```

  - 求最晚入职的

    ```sql
    select max(hire_date) from employee;
    ```

  - 求各部门最晚入职的

    ```sql
    select post,max(hire_date) from employee group by post;
    ```

  - 求各部门的所有人员

    ```sql
    select post,group_concat(emp_name) from employee group by post;
    ```

#### 10.7.3 having

- 用于过滤，后面可以跟聚合函数（where不可以），一般和group by搭配使用。

  - 求部门人数大于3的部门

    ```sql
    select post,count(*) from employee group by post having count(*) > 3;
    ```

#### 10.7.4 order by

- 根据某些字段排序

  ```sql
  select * from 表名 order by 字段1;--根据字段1排序，默认升序
  select * from 表名 order by 字段1 desc;--降序排序
  select * from 表名 order by 字段1,字段2 desc;--先根据字段1排序，得到结果再根据字段2排序
  ```

#### 10.7.5 limit

- limit n 取前n个 ，等价于 limit 0,n

  ```sql
  select * from 表名 limit 5;--取查询结果的前五条数据
  ```

- 分页 limit m,n 从m+1开始取n条数据,等价于limit n offset m

  ```sql
  select * from 表名 limit 5,5;--从第六条开始取5条数据
  ```

#### 10.7.6 sql语句执行顺序

```sql
select 字段1,字段2,... from 表名
where 条件
group by 字段1
having 条件
order by 字段2
limit n
-- 1.from 表名
-- 2.where 条件
-- 3.group by 字段1
-- 4.having 条件
-- 5.select 字段1,字段2
-- 6.order by 字段2
-- 7.limit n
```

### 10.8 多表查询

#### 10.8.1 连表查询

##### 10.8.1.1 内连接 inner join

- 两张表条件不匹配的项不会出现再结果中

  ```sql
  select * from emp inner join department as dep on emp.dep_id = dep.id;
  ```

##### 10.8.1.2 外连接

- 左外连接 left join：永远显示全量的左表中的数据

  ```sql
  select * from emp left join department as dep on emp.dep_id = dep.id;
  ```

- 右外连接 right join：永远显示全量的右表中的数据

  ```sql
  select * from emp right join department as dep on emp.dep_id = dep.id;
  ```

- 全外连接 left join union right join

  ```sql
  select * from emp right join department as dep on emp.dep_id = dep.id 
  union 
  select * from emp left join department as dep on emp.dep_id = dep.id;
  ```

- 常用 inner join 和 left join

- 连接语法

  ```sql
  select 字段 from 表1 xxx join 表2 on 表1.字段 = 表2.字段;
  ```

#### 10.8.2 子查询

```sql
select * from emp where dep_id = (select id from department where name = '技术'); 
```

- sql查询优化，既可以用连表查询实现又可以用子查询实现的查询，尽量使用连表查询，连表查询效率高。

#### 10.8.3 练习

1. 查询平均年龄在25岁以上的部门名

   ```sql
   -- 连表查询
   select dep.name,avg(age) from emp inner join department as dep on emp.dep_id = dep.id group by dep.name having avg(age) > 25;
   --子查询
   select name from department where id in (select dep_id from emp group by dep_id having avg(age) > 25);
   ```

2. 查询大于部门内平均年龄的员工名、年龄

   ```sql
   -- 连表查询+子查询
   select emp.name,age from emp inner join (select dep_id,avg(age) as avg_age from emp group by dep_id) as dep on emp.dep_id = dep.dep_id where emp.age > dep.avg_age;
   ```

### 10.9 索引

#### 10.9.1 定义

一个建立在存储表阶段的一个存储结构，能在查询的时候加快速度。（相当于目录）

重要性：

- 在实际场景中读写的比例大概10:1，所以要加快读的速度。

#### 10.9.2 索引的原理

##### 10.9.2.1 磁盘预读原理 block

每次委托操作系统去硬盘读取内容时，操作系统为减少io操作，总是多读出来一部分数据到内存中，以便于下次读取。

- 读硬盘的io操作的时间非常长，比CPU执行指令的时间长很多。
- 尽量的减少io次数才是读写数据的主要要解决的问题。

##### 10.9.2.2 数据库的存储方式

1. 存储结构-树
2. 平衡树 balance tree   --b树
3. 在b树的基础上进行改良，b+树。
   - 分支节点和根节点都不再存储实际的数据
     - 让分支和根节点能存储更多的索引的信息
     - 降低了树的高度
     - 所有的实际数据都存储在叶子节点中
   - 在叶子节点之间加入了双向的链式结构
     - 方便在查询中范围条件查找
4. mysql当中的所有的b+树索引的高度都基本控制在3层
   - io操作的次数非常稳定
   - 有利于通过范围查询
5. 树的高度会影响索引的效率
   - 尽量选择短的列创建索引
   - 对区分度高的列创建索引，重复率超过了10%那么就不适合做索引。

##### 10.9.2.3 索引的种类

- 聚集索引：数据直接存储在树结构的叶子节点
- 辅助索引：数据不直接存储在树中

在innodb中既有聚集索引，又有辅助索引。

在mysiam中，只有辅助索引，没有聚集索引。

- primary key  主键   聚集索引    约束的作用：非空 + 唯一
- unique  自带索引  辅助索引   约束的作用：唯一
- index 辅助索引 没有约束作用

##### 10.9.2.4 创建索引语法

```sql
create index 索引名字 on 表(字段);
drop index 索引名字 on 表名字;
```

```sql
select * from 表名 where id = xxxx;
--在id字段没有索引的时候，效率低
--在id字段有索引的之后，效率高
```

#### 10.9.3 索引不生效

- 要查询的数据范围大

  - < , >,>=,<=,!=

  - between  and

    ```sql
    select * from 表 order by age limit 1000000,5;--越到后面效率越低，每次都要把前面的数据全查出来，再从1000001条开始取5条
    
    select * from 表 where id between 1000000 and 1000005;--效率快，根据索引只取中间需要的5条数据
    ```

  - like

    - 结果的范围大，索引不生效
    - 如果xxx% 索引生效，%xxx索引不生效

- 如果一列内容的区分度不高，索引不生效

- 索引不能在条件中参与计算，否则不生效

  ```sql
  select * from 表 where id * 10 = 1000000;--不生效，数据库内部会将每个树结构的索引值乘以10 后，再与1000000比较。
  ```

- 对两列内容进行条件查询

  - and 条件两端的内容，优先选择一个有索引的，并且树形结构更好的，来进行查询。and条件需要所有条件都满足，先完成范围小速度快的（带索引的）以减小后面的压力

    ```sql
    select * from s1 where id = 1000000 and email = 'eva1000000@oldboy';
    ```

  - or 条件不会进行优化，只是根据条件从左到右依次筛选。带or的想要命中索引，这些条件的列必须都是索引列。

- 联合索引

  ```sql
  create index ind_mix on s1(id,email);
  ```

  - 在联合索引中如果使用了or条件，则索引就不能生效

    ```sql
    select * from s1 where id = 1000000 or email = 'eva1000000@oldboy';
    ```

  - 最左前缀原则：在联合索引中，条件必须含有在创建索引的时候的第一个索引列，索引才会命中

    ```sql
    select * from s1 where id = 1000000; --可以命中
    select * from s1 where email = 'eva1000000@oldboy'; --不能命中
    ```

  - 在整个条件中，从开始出现模糊匹配,大范围查询的那一刻，索引就失效了。

    ```sql
    select * from s1 where id > 1000000 and email = 'eva1000000@oldboy'; --不生效
    select * from s1 where id = 1000000 and email like 'eva%'; --生效
    ```

#### 10.9.4 应用场景

##### 10.9.4.1 联合索引

- 只对a 或者abc条件进行筛选，而不会对b,c进行单列的条件筛选，可以创建联合索引。

  ```sql
  create index ind_abc on s1(a,b,c);
  ```

##### 10.9.4.2 单列索引

- 选择一个区分度高的列建立索引
- 使用or来连接多个条件
  - 在满足上述条件的基础上，对or相关的所有列分别创建索引

#### 10.9.5  其他名词

##### 10.9.5.1 覆盖索引

如果我们使用索引作为条件查询，查询完毕后，不需要回表查，那么这就是覆盖索引。

```sql
select id from s1 where id = 1000000;
select count(id) from s1 where id >1000000;
```

##### 10.9.5.2 合并索引

对两个字段分别创建索引，由于sql的条件，使两个索引同时生效了，那么这时候和两个索引就成为了合并索引。

##### 10.9.5.3 执行计划

如果你想在执行sql之前就知道sql语句的执行情况，那么可以使用执行计划。

```sql
explain sql语句;
```

### 10.10 数据库的备份

```sql
mysqldump -uroot -p123 数据库名 > 保存的文件路径 --只备份数据库中的表结构和数据
mysqldump -uroot -p123 database 数据库名 > 保存的文件路径 --备份整个数据库


--数据库还原
source 文件路径
```

### 10.11 事务和锁

```sql
begin;  -- 开启事务
select * from emp where id = 1 for update;  -- 查询id值，for update添加行锁；
update emp set salary=10000 where id = 1; -- 完成更新
commit; -- 提交事务
```

### 10.12 sql注入和pymysql

#### 10.12.1 pymysql模块

```python
import pymysql
#连接数据库
conn = pymysql.connect(host='127.0.0.1',user='root',password='123',database='day40')
cur = conn,cursor() #数据库操作符 游标。可以加参数cursor = pymysql.cursors.DictCursor 查询到的结果会按字典形式返回

# 添加数据
cur.execute('insert into employee(emp_name,sex,age,hire_date) '
             'values ("郭凯丰","male",40,20190808)')
cur.execute('delete from employee where id = 18')
conn.commit()#提交，将内存中的修改写入数据库文件
conn.close() #关闭连接

#查询数据
cur.execute('select * from employee '
            'where id > 10')
ret = cur.fetchone() #取结果集中的一条数据
print(ret['emp_name']) 
ret = cur.fetchmany(5) #取结果集中的5条数据，前面取过一条，再取只能接着取
ret = cur.fetchall() #取出所有结果
print(ret)
conn.close()
```

#### 10.12.2 sql注入

示例：

```sql
--用户名和密码到数据库里查询数据
--如果能查到数据 说明用户名和密码正确
--如果查不到，说明用户名和密码不对
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = '%s' and password = '%s'"%(username,password)
print(sql)
-- 注释掉--之后的sql语句
select * from userinfo where name = 'alex' ;-- and password = '792164987034';
select * from userinfo where name = 219879 or 1=1 ;-- and password = 792164987034;
select * from userinfo where name = '219879' or 1=1 ;-- and password = '792164987034';
```

```python
import pymysql

conn = pymysql.connect(host = '127.0.0.1',user = 'root',
                       password = '123',database='day41')
cur = conn.cursor()
username = input('user >>>')
password = input('passwd >>>')
sql = "select * from userinfo where name = %s and password = %s"
cur.execute(sql,(username,password))#让pymysql模块帮助我们做sql语句拼接
print(cur.fetchone())
cur.close()
conn.close()
```

### 10.13 数据库的优化

#### 10.13.1 索引优化

防止出现索引不命中的情况：

- 条件的范围过大
- 使用了or
- like '%xx'

- 没有符合最左前缀原则
- 索引列区分度不高
- 索引列参与计算
- 在条件中使用函数
- 索引类型和sql条件中类型不匹配

#### 10.13.2 sql优化

- 使用索引
- 尽量把条件范围都写在where里
- 尽量使用连表查询代替子查询

- 使用具体字段代替*

#### 10.13.3 数据库的优化

- sql的优化
- 创建表时把固定长度的字段放前面
- 分库分表
- 数据库集群，读写分离

## 十一、前端开发

### 11.1 Html

#### 11.1.1 Html定义

html语言是一种超文本标记语言。

html特征：

- 对换行和空格不敏感
- 对空白折叠

#### 11.1.2 Html标签

标签叫做标记，有两种标签：

- 双闭合标签，如：`<html></html>`
- 单闭合标签，如：`<meta charset="utf-8">`

##### 11.1.2.1 head标签

head标签内的内容，对于用户来说是不可见的，它不会在网页中显示。

- meta标签，存放网站基本元信息。可以有多个meta标签。

  ```html
  <meta charset="utf-8">
  ```

- title 网站的标题

- link 链接css样式表

  ```html
  <link rel="stylesheet" href="css/my.css">
  ```

- script 链接js文件，或者内部写js代码

  ```html
  <script src="js/my.js"></script>
  ```

- style 直接将css样式写入html，内嵌样式

  ```html
  <style>
      .pwd{
          color:red;
      }
  </style>
  ```

##### 11.1.2.2 body标签

1. 标题标签

   h1~h6  一级标题~六级标题

2. 段落标签

   ```html
   <p>
       1234
   </p>
   ```

3. 超链接标签

   a标签    anchor锚点

   ```html
   <a href="" target=""></a>
   ```

   a标签的属性

   - href 要链接的地址

     - 为空时表示一个空连接，代表自身

     - `#top`代表回到顶部，我们可以定义一个空标签，来进行定位

       ```html
       <h6 id="top"></h6>
       <a href="#top">回到顶部</a>
       ```

   - target 在哪里打开页面

     - _self 当前页面（默认值）
     - _blank 新的空白页

   - title 鼠标悬浮在标签上时现实的文字

4. 图片标签

   `<img></img>`

   属性：

   - src 图片的地址

   - alt 图片加载失败时，显示的文字

   - 可以把图片包裹在a标签中，点击图片完成跳转

     ```html
     <a><img></img></a>
     ```

5. 换行标签

   `<br></br>`

6. 分隔线标签

   `<hr></hr>`

7. 加粗标签

   `<b></b>`    已废弃 

   `<strong></strong>`

8. 斜体标签

   `<em></em>`

   `<i></i>`   html5中的标签

9. 列表标签

   - 有序列表  ol (order list)  内部包裹li标签

     ```html
     <ol type="">
     	<li>111</li>
     	<li>222</li>
     	<li>333</li>
     	<li>444</li>
     	<li>555</li>
     </ol>
     ```

     有序列表属性type,改变序号种类

     - 默认为1，表示阿拉伯数字

   - 无序列表 ul (unorder list) 内部包裹li标签

     ```html
     <ul type="square">
     	<li>aaa</li>
     	<li>bbb</li>
     	<li>ccc</li>
     	<li>ddd</li>
     	<li>eee</li>
     </ul>
     ```

     有序列表属性type,改变列表项前端样式

     - circle 空心圆点
     - square 方块

   - 自定义列表dl  内部包含dt标题和dd内容标签

     ```html
     <!DOCTYPE html>
     <html lang="en">
     <head>
         <meta charset="UTF-8">
         <title>Title</title>
         <style>
         </style>
     </head>
     <body>
         <dl>
             <dt>标题</dt>
             <dd>内容1</dd>
             <dd>内容2</dd>
             <dd>内容3</dd>
         </dl>
     </body>
     </html>
     ```

10. 表格标签

    - table标签
      - tr标签，表示一行
        - th标签，表示表头，会加粗
        - td标签，表示表格数据

    ```html
    <table border="1px" cellspacing="0">
     	<tr>
     	    <th>id</th>
     	    <th>姓名</th>
     	    <th>年龄</th>
     	</tr>
     	<tr>
     	    <td>1</td>
     	    <td>alex</td>
     	    <td>18</td>
     	</tr>
     	<tr>
     	    <td>2</td>
     	    <td>wusir</td>
     	    <td>18</td>
     	</tr>
    </table>
    ```

    table属性：

    - border 设置表格边框
    - cellspacing 设置表格单元格间隔

11. 表单标签

    form标签用于和后台服务器进行交互数据。

    属性：

    - action属性，后台交互的服务器程序地址
    - method属性，提交数据的方式，一般为get或post
    - enctype属性，默认为application/x-www-form-urlencoded，当要上传文件时，需要修改为multipart/form-data

    表单内的标签：

    - input标签，输入框

      input的属性：

      - value属性，提交的数据的键值对中的值
      - name属性，提交的数据的键值对中的键
      - type属性，修改输入框的种类
        - text 文本输入框
        - password 密码输入框
        - submit 提交按钮，会提交表单。默认按钮上的文字为提交，可通过input的value属性修改
        - radio 单选框，几个单选框起相同的name，表示为一组，互斥。添加checked属性表示默认选中
        - checkbox 多选框，添加checked属性表示默认选中
        - datetime-local 日期框
        - file 上传文件选择框

    - label标签，和input输入框进行关联。label的for属性的值和input的id值相同，两个标签会产生关联，点击label标签，输入框也会聚焦

      ```html
      <form action="" method="" enctype="application/x-www-form-urlencoded">
          <label for="user">用户名</label>
          <input type="text" id="user" name="username">
      </form>
      ```

      

    - select标签，下拉列表，内部包裹option标签表示下拉选项。默认选中selected。可以通过设置multiple属性多选下拉列表。

      ```html
      <form action="" method="" enctype="application/x-www-form-urlencoded">
          <select name="gender" id="">
              <option value="male">男</option>
              <option value="female">女</option>
          </select>
      </form>
      ```

      

    - textarea标签，多行文本框

12. span标签，在不影响文本正常显示的情况下，单独设置对应的样式。

13. div标签，盒子标签。把网页分割成不同的独立的逻辑区域。

14. &nbsp 表示一个空格

15. &copy

#### 11.1.3 标签分类

- 行内标签
  - 行内标签：在一行内显示，不能设置宽高，默认是内容的宽高。
    - strong,b,i,em,span,lable,a
  - 行内块标签：在一行内显示，可以设置宽高。
    - img,input,td,th
- 块级标签:单独占一行，可以设置宽高。
  - h1~h6,p,ol,ul,li,table,tr,form

### 11.2 CSS

#### 11.2.1 CSS的引入方式

##### 11.2.1.1 行内样式

```html
<div style="color:red;">
    
</div>
```

##### 11.2.1.2 内嵌样式

```html
<head>
    <style>
        div{
            color:red;
        }
    </style>
</head>
```

##### 11.2.1.3 外接样式

```html
<head>
    <link href="css/index.css" rel="stylesheet">
</head>
```

##### 11.2.1.4 优先级

行内>内嵌>外接。

#### 11.2.2 CSS选择器

##### 11.2.2.1 基础选择器

- id选择器

  ```css
  #index{
  	color:red;
  }
  ```

- 类选择器

  一个标签可以设置多个类名

  ```html
  <div class="index box"><div>
  ```

  ```css
  .index{
      color:red;
  }
  .box{
      font-size: 14px;
  }
  ```

- 标签选择器

  ```css
  div{
      color:red;
  }
  ```

- 属性选择器

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
      <style>
          input[type='text']{
              background-color: red;
          }
          input[type='checkbox']{
  
          }
          input[type='submit']{
  
          }
      </style>
  </head>
  <body>
  <form action="">
      <input type="text">
      <input type="password">
      <input type="radio">
      <input type="checkbox">
      <input type="file">
  </form>
  </body>
  </html>
  ```

##### 12.2.2.2 高级选择器

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        /*后代选择器*/
        /*#box p{*/
            /*color:red;*/
        /*}*/
        /*子代选择器*/
        /*#box>p{*/
            /*color: yellow;*/
        /*}*/
        /*组合选择器*/
        /*重置样式*/
        p,ul,ol,body{
            margin:0;
            padding: 0;
        }
        input,textarea{
            border:none;
            outline: 0;
        }
        span{
            color:red;
        }
        .active{
            font-size: 20px;
        }
        span.active{
            background-color: black;
        }
    </style>
</head>
<body>
    <div id="box">
        <div>
            <div>
                <div>
                    <p>
                        mjj
                    </p>
                </div>
            </div>
        </div>
        <p>
            wusir
        </p>
    </div>
    <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
    </ul>
    <span class="active">女神</span>
</body>
</html>
```

##### 12.2.2.3 伪类选择器

对于a标签，有“爱恨准则”，LoVe HAte。

- L:  link  未链接之前
- V：visited 链接之后
- H：hover 鼠标悬浮上之后 （hover可用于任何标签）
- A：active  鼠标点中之后

对于a标签，如果想要设置a标签的样式，要作用于a标签上，继承不起作用。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        a:link{
            color: aquamarine;
        }
        a:visited{
            color: chartreuse;
        }
        a:hover{
            color: yellow;
        }
        a:active{
            color: darkgreen;
        }
    </style>
</head>
<body>
    <a href="#">百度一下</a>
</body>
</html>
```

利用hover属性实现一些动态效果

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        #box{
            width: 200px;
            height:200px;
            background-color: red;
        }
        #box:hover div{
            width: 200px;
            height: 100px;
            background-color: yellow;
        }
        a{
            text-decoration: none;
        }
        a:hover{
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div id="box">
        <div></div>
    </div>

    <a href="">百度一下</a>
</body>
</html>
```

##### 12.2.2.4 伪元素选择器

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        /*可以省略一个冒号*/
        p:first-letter{
            color: red;
        }
        p:before{
            content: '123';
        }
        /*解决浮动布局常用的一种方法*/
        p:after{
             /*通过伪元素添加的内容为行内元素*/
            content: '@';
        }
    </style>
</head>
<body>
    <p>好好看，好好学</p>
</body>
</html>
```

##### 12.2.2.5 通配符选择器

将html文档中的所有标签都选中，包括head中的标签，效率较低（不推荐使用）。

```css
*{
    color:red;
}
```

补充：

```css
/*css3中的一种高级选择器。取第一个span*/
span:nth-child(1){
    color:red;
}
```

#### 12.2.3 层叠型和权重

##### 12.2.3.1 继承性

- css样式中，有哪些属性可以被继承？
  - color，line-height，text-xxx(如text-align)，font-xxx(如font-size)
- 为什么要有继承？
  - 减少代码量

##### 12.2.3.2 权重的计算

- 行内样式权重最大为1000，优先级最高。
- 标签选择器权重为1
- 类选择器权重为10
- id选择器权重为100

计算方法：

通过数一个样式中各个选择器的个数来计算，永不进位。

| id选择器 | 类选择器 | 标签选择器 |
| -------- | -------- | ---------- |
| 数量*100 | 数量*10  | 数量*1     |

权重比较总结：

- 对于两个选中了的标签 

  - 数选择器的数量，谁的大谁的优先级高，如果一样大，后面的覆盖前面的。

- 一个是继承来的，另一个是选中了标签

  - 选中标签的属性的优先级大于继承来的属性，它们是没有可比性的

- 同时继承来的属性

  - 谁描述的离标签近，谁的优先级高
  - 描述的一样进，这时才回归到数选择器的数量

- 属性后面加 `!important` 强制最大权重，但是大不过行内样式。

  ```css
  box1 .wrap2 .wrap3{
              color: greenyellow !important;
  }
  ```

#### 12.2.4 CSS盒子模型

![](E:\Python\10前端\CSS盒子模型.png)

##### 12.2.4.1 边框 border

边框三要素：粗细，线性样式，颜色。

```css
/*整体设置4个方向的粗细，线性样式，颜色*/
div{
    border:1px,solid,red;
}
```

设置粗细 border-width

- border-left-width  设置左侧边框
- border-right-width
- border-top-width
- border-bottom-width

```css
div{
    border-width:2px 3px; /*设置上下，左右*/
    border-width:1px 3px 2px;/*设置上，左右，下边框*/
    border-width:1px,2px,3px,4px;/*设置上，右，下，左（顺时针）*/
}
```

设置线性样式  border-style

- border-left-style  设置左边框线性样式 
- 。。。

线性样式：

- solid 实线
- dotted 点状线
- dashed  虚线
- double 双线

```css
div{
    border-style:solid;
}
```

设置颜色  border-color

- border-left-color  设置左边框线性样式
- 。。。

```css
div{
    border-color: transparent;
}
```

transparent  透明色

##### 12.2.4.2 内边距 padding

- padding-left
- padding-right
- padding-top
- padding-bottom

##### 12.2.4.3 外边距 margin

1. 水平方向，两个盒子的距离为各自所设定的外边距之和。
2. 垂直方向，会出现外边距的合并（塌陷现象），以大的外边距值为基准。设置盒子内部的元素的外边距时，会改变父盒子的位置上。

```css
/*盒子水平居中*/
div{
    margin-left:auto;
    margin-right:auto;
}
```

#### 12.2.5 常用排版格式

##### 12.2.5.1 字体属性

1. font-family  设置字体

   ```css
   /*从左向右解析，如果前面的字体系统没有安装，则一次向后查找*/
   body{
       font-family:'微软雅黑','宋体','楷体';
   }
   ```

2. font-size 设置字体大小

   ```css
   body{
       font-size:20px;
   }
   ```

   单位：

   - px 绝对单位，也叫固定单位

   - em 相对单位，相对于当前盒子的font-size。只需要动态改变字体大小，就可以实现响应式布局。

     ```css
     div{
         font-size:20px;
         width:10em; /*相当于200px*/
         height:10em;
     }
     ```

   - rem 相对单位，相对于根元素

     ```css
     /*font-size可以被继承，所以相对font-size容易设置*/
     html{
     	font-size:20px;
     }
     div{
        width:10rem; /*相当于200px*/
        height:10rem;
     }
     ```

3. font-style 字体样式

   - normal 普通字体
   - italic 斜体

   ```css
   div{
       font-style:italic;
   }
   ```

4. font-weight  设置字体的粗细 （范围100~900）

   - normal  普通字体     400
   - bold  加粗   700
   - bolder  更粗  900
   - lighter   比普通更细    100

   ```css
   div{
       font-weight:bold;
   }
   ```

##### 12.2.5.2 文本属性

1. text-decoration 

   - none 什么都不设置
   - underline 下划线
   - line-through 删除线
   - overline 上划线

   ```css
   div{
       text-decoration:underline;
   }
   ```

2. text-indent  设置缩进

   若字体大小为16px，则32px为缩进两个字符

   ```css
   div{
       font-size:16px;
       text-indent:32px;
   }
   
   .box{
   	font-size:16px;
       text-indent:2em;
   }
   ```

3. line-height 行高、行间距

   ```css
   div{
       line-height:20px;
   }
   ```

4. letter-spacing  文字间距（针对中文）

   ```css
   div{
       letter-spacing:2px;
   }
   ```

5. word-spacing   字母间距（针对英文）

   ```css
   div{
       word-spacing:2px;
   }
   ```

6. text-align 文本对齐

   - center居中
   - left 靠左
   - right 靠右
   - justify(仅限于英文文本)两端对齐；。‘、配【

   ```css
   div{
       text-align:center;
   }
   ```

#### 12.2.6 浮动

浮动作为布局方案，实现了元素并排效果。

##### 12.2.6.1 浮动的现象

- 脱离了标准文档流，不在页面上占位置（脱标）。
- 文字环绕，设置浮动的初衷。
- 贴边现象，给盒子设置了浮动之后，找浮动盒子的边贴靠，如果找不到浮动盒子的边，就找父元素的边贴靠。
- 若没有为设置盒子的宽度，给盒子设置浮动后盒子宽度会收缩，大小为内容宽度。

##### 12.2.6.2 浮动带来的问题

若父元素未设置固定高度，则浮动元素不能撑起父元素的高度

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father{
            width: 800px;
            border: 1px solid #000;
        }

        .child1{
            width: 200px;
            height: 200px;
            background-color: red;
            float: left;
        }
        .child2{
            width: 200px;
            height: 200px;
            background-color: green;
            float: left;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

##### 12.2.6.3 清除浮动方法

1. 为父元素设置固定高度。

   - 缺点：不灵活，后期不易维护。
   - 应用场景：固定的导航栏上。

2. 内墙法

   给最后一个浮动元素的后面添加一个空的块级标签，并设置该标签的样式属性clear：both;

   缺点：一个网页会有很多浮动，每次都加空的块级标签会冗余，消耗带宽。

   解决方案：利用伪元素选择器。

   标准写法：

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <style>
           .father{
               width: 800px;
               border: 1px solid #000;
           }
   
           .child1{
               width: 200px;
               height: 200px;
               background-color: red;
               float: left;
           }
           .child2{
               width: 200px;
               height: 200px;
               background-color: green;
               float: left;
           }
           /*标准写法*/
           .clearfix::after{
               content: '.';
               display: block;
               clear: both;
               visibility: hidden;
               height: 0;
           }
   
       </style>
   </head>
   <body>
       <div class="father clearfix">
           <div class="child1"></div>
           <div class="child2"></div>
       </div>
   </body>
   </html>
   ```

   简便写法：

   ```css
   .clearfix::after{
       content:'';
       display:block;
       clear:both;
   }
   ```

3. 通过overflow属性

   overflow:设置超出盒子部分的状态。

   - visible  默认值，超出部分可见。
   - hidden 超出部分隐藏。
   - scroll   使用滚动条。
   - inherit   可继承，设置为该项后，当前元素会继承父元素的overflow属性

   设置overflow属性为hidden可以清除浮动。

   一旦设置了一个box盒子的overflow为visible之外的任意值，那么该box会变成一个BFC（Block Format Context）区域，形成的BFC区域有它自己的规则，计算BFC的高度时，浮动的元素也参与计算。

   **注意：使用overflow清除浮动时应该注意，flowover本身的特性。**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father{
            width: 800px;
            border: 1px solid #000;
            overflow: hidden;
        }

        .child1{
            width: 200px;
            height: 200px;
            background-color: red;
            float: left;
        }
        .child2{
            width: 200px;
            height: 200px;
            background-color: green;
            float: left;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

**补充：BFC区域**

1. BFC定义

   BFC(Block formatting context)直译为”块级格式化上下文”。它是一个独立的渲染区域，只有Block-level box参与， 它规定了内部的Block-level Box如何布局，并且与这个区域外部毫不相干。

2. BFC的一些布局规则

   ```
   1.内部的Box会在垂直方向，一个接一个地放置。
   2.Box垂直方向的距离由margin决定。属于同一个BFC的两个相邻Box的margin会发生重叠
   3.每个元素的margin box的左边， 与包含块border box的左边相接触(对于从左往右的格式化，否则相反)。即使存在浮动也是如此。
   4.BFC的区域不会与float 元素重叠。
   5.BFC就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
   6.计算BFC的高度时，浮动元素也参与计算
   ```

3. 哪些元素会生成BFC

   ```
   1.根元素
   2.float属性不为none
   3.position为absolute或fixed
   4.display为inline-block
   5.overflow不为visible
   ```

**注：要浮动一起浮动，有浮动清除浮动。**

#### 12.2.7 定位

position  定位

- static  默认值，静态的
- relative 相对定位
- absolute  绝对定位
- fixed  固定定位

只要设置了position属性为除了static之外的其他值，就可以设置该元素的top,bottom,left,right属性值为该元素定位。

##### 12.2.7.1 relative 相对定位

参考点：以原来的盒子为参考点。

特征：

- 设置相对定位与标准文档流下的盒子没有任何区别，但是会覆盖标准文档流元素。
- 修改相对位置后，盒子原来的位置还会被占，影响页面布局

应用场景：一般用来做“子绝父相”布局方案的参考。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father{
            width: 800px;
            border: 1px solid #000;
        }

        .child1{
            width: 200px;
            height: 200px;
            background-color: red;
            position: relative;
            top: 20px;
            left: 30px;
        }
        .child2{
            width: 200px;
            height: 200px;
            background-color: green;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

##### 12.2.7.2 absolute 绝对定位

参考点：

- 如果单独设置一个盒子为绝对定位，以top来描述，它的参考点是以body的(0,0)为参考点。
- 如果单独设置一个盒子为绝对定位，以bottom来描述，它的参考点为浏览器的左下角。
- "子绝父相"，若父元素设置了相对定位，子元素设置了绝对定位，它会以最近的设置了相对定位的父辈元素的左上角为参考点。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .father {
            width: 800px;
            border: 1px solid #000;
            position: relative;
        }

        .child1 {
            width: 200px;
            height: 200px;
            background-color: red;
            position: absolute;
            top: 20px;
            left: 20px;
        }

        .child2 {
            width: 200px;
            height: 200px;
            background-color: green;
        }

    </style>
</head>
<body>
    <div class="father">
        <div class="child1"></div>
        <div class="child2"></div>
    </div>
</body>
</html>
```

绝对定位特征：

- 脱离标准流，不占位置
- 会压盖标准流元素
- 子绝父相

##### 12.2.7.3 固定定位

特征：

- 脱标
- 位置固定不变
- 提高层级，压盖

参考点：以浏览器的左上角为参考点。

```html
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 500px;
            height: 2000px;
            border: 1px solid #000;
        }

        .fix{
            width: 50px;
            height: 200px;
            background-color: yellow;
            position: fixed;
            top:260px;
            right: 0;
        }
    </style>
</head>
<body>
    <div class="box"></div>
    <div class="fix"></div>
</body>
</html>
```

#### 12.2.8 z-index

只是用于定位的元素（相对定位，绝对定位，固定定位），z-index越大表示显示层级越高。

#### 12.2.9 background背景图

- background-image  设置图片背景

  ```css
  div{
      width: 200px;
      height: 200px;
      border: 1px solid #000;
      background-image: url("图片地址");
  }
  ```

- background-repeat  设置背景图平铺样式

  - repeat  默认，平铺。
  - repeat-x  横向平铺。
  - repeat-y  纵向平铺。
  - no-repeat 不平铺。

- background-position  设置背景图位置

  - 第一个值为横向坐标，第二个为纵向坐标，默认为0,0。
  - 横向还有三个写好的参数：left(靠左)，center(居中)，right(靠右)。
  - 纵向三个参数：top(靠上)，center(居中)，bottom(靠下)。

- background   综合属性

  ```css
  .box {
      width: 200px;
      height: 200px;
      border: 1px solid #000;
      background: url("图片地址") no-repeat center top;
  }
  ```

#### 12.2.10 border-radius  设置边框圆角

```css
/*值越大，圆角越大，50%为整个圆*/
.box {
    width: 200px;
    height: 200px;
    background-color: red;
    border-radius: 50%;
}
```

#### 12.2.11 box-shadow 盒子阴影

box-shadow属性值：

- 第一个参数 水平距离
- 第二个，垂直距离
- 第三个，模糊距离
- 第四个，模糊颜色
- 第五个，阴影样式（默认为外阴影），inset设置为内阴影。

```css
.box {
    width: 200px;
    height: 200px;
    margin: 0 auto;
    background-color: red;
    box-shadow: 2px 3px 10px yellow;
}
```

#### 12.2.11 水平和垂直

##### 12.2.11.1 行内元素水平垂直居中

1. 设置text-align属性为center，行高等于容器高度。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 200px;
            height: 200px;
            border: 1px solid #000;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
    </style>
</head>
    <body>
        <div class="box">
           <span>mjj</span>
        </div>
    </body>
</html>
```

2. 设置元素display属性为table-cell，vertical-align属性为middle，text-align属性为center。(了解)

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        .box{
            width: 200px;
            height: 200px;
            border: 1px solid #000;
            display: table-cell;
            vertical-align: middle;
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="box">
       <span>mjj</span>
    </div>
</body>
</html>
```

##### 12.2.11.2 块级元素水平垂直居中

1. 方式一

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <style>
           .box{
               position: relative;
               width: 300px;
               height: 300px;
               border: 1px solid #000;
               margin: auto 0;
           }
           .box div{
               position: absolute;
               width: 100px;
               height: 100px;
               background-color: green;
               left: 0;
               right: 0;
               top:0;
               bottom: 0;
               margin: auto;
           }
       </style>
   </head>
   <body>
       <div class="box">
          <div></div>
       </div>
   </body>
   </html>
   ```

   2.方式2

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <style>
           .father{
               width: 500px;
               height: 300px;
               background-color: red;
               position: relative;
           }
           .child{
               /*如何让一个绝对定位的垂直居中： top设置50%，margin-top设置当前盒子的一半，并且是负*/
               position: absolute;
               width: 100px;
               height: 140px;
               background-color: green;
               /*父元素的50%*/
               left: 50%;
               margin-left: -50px;
               top: 50%;
               margin-top: -70px;
   
           }
       </style>
   </head>
   <body>
       <div class="father">
           <div class="child">我是个居中的盒子</div>
       </div>
   </body>
   </html>
   ```

#### 12.2.12 其他

1. 行内标签设置了浮动就可以设置宽高。

2. vertical-align  调整文本垂直方向的位置

   - center
   - top
   - bottom
   - 也可以设置数值

   同样可以设置input标签的位置，默认为center。

3. list-style  

   - none 清除ul的样式

4. curcor属性设置为pointer，可以使鼠标悬浮到元素上时变成小手。

5. display属性

    - inline 设置为行内

    - block 设置为块级元素

    - inline-block 设置为行内块

    - none  不显示

  注：行内元素尽量不要包裹块级元素。

### 12.3 java script

#### 12.3.1 js的引入方式

##### 12.3.1.1 内部js

只要放入Html文档中即可，一般放在head中或body中。

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript">
        //js代码
    </script>
</head>
<body>
</body>
</html>
```

##### 12.3.1.2 外部js

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script type="text/javascript" src="js/index.js"></script>
</head>
<body>
</body>
</html>
```

##### 12.3.1.3 js的注释

```js
//单行注释
/*
多行注释
*/
```

#### 12.3.2 js的变量

##### 12.3.2.1 通过var关键字声明变量

```js
//声明并初始化变量
var a =12;
//声明变量
var b;
//变量赋值
b = 12;
```

##### 12.3.2.2 变量命名规范

- 必须以字母，下划线或者$符开头。
- 驼峰命名法
- 不能是js中的关键字he保留字

#### 12.3.3 js的数据类型

##### 12.3.3.1 基本数据类型

1. number 数值类型

   ```js
   var a = 12;
   var b =1.234;
   var c = -1.2;
   alert(a);
   alert(typeof a);//typeof 获取变量的类型 
   var d = 7/0; //Infinity 无限大的
   ```

2. string 字符串类型

   字符串可以用双引号也可以用单引号

   ```js
   var a = '123';
   var b = "I'm MJJ";
   var c = "I'm \"MJJ\"";//转义
   ```

3. boolean 布尔类型

   - 0 假 false
   - 1 真 true

   ```js
   var c = 3 < 4;
   alert(c); // true
   ```

4. undefined 未定义

   当一个变量只是声明，但是没有赋值时，这时值为undefined,类型也为undefined。

   ```js
   var a;
   alert(a); //undefined
   alert(typeof a);//undefined
   ```

5. null 空

   ```js
   var a = null;
   alert(a); //null
   alert(typeof a) //object
   ```

##### 12.3.3.2 引用数据类型

1. object 类型

   我们看到的大多数的像Array、String、Number等这些类型的，他们统统属于对象，而且，object也是ECMAScript中使用最多的一个类型。并且每个对象都有属性和方法。

   - **对象的属性**：它是属于这个对象的某个变量。比如字符串的长度、数组的长度和索引、图像的宽高等
   - **对象的方法**：只有某个特定属性才能调用的函数。表单的提交、时间的获取等。

   创建对象:

   方式一：使用new操作符后跟Object构造函数

   ```js
   //创建对象
   var person = new Object();//等价于 var person = {};
   //给对象添加name和age属性
   person.name = 'jack';
   person.age = 28;
   //给对象添加fav的方法
   person.fav = function(){
       console.log('泡妹子');
   }
   ```

   方式二：使用**对象字面量**表示法。

   ```js
   var person = {
       name : 'jack',
       age : 28,
       fav : function(){
           console.log(123);
       }   
   }
   ```

   对象属性的调用和修改

   ```js
   //调用
   var a = person.name;
   var b = person['name'];
   //修改
   person.name = '123';
   person['name'] = '123';
   ```

   遍历对象

   ```js
   var person = {
       name:"mjj",
       age:18
   }
   
   for(var key in person){
       person[key];
   }
   ```

   

2. Array 数组类型

   ```js
   var a = [1,2,'123',4];//可以放置不同类型的数据
   var b = [1,2,3,['1','2']];//可以嵌套
   console.log(a[2])//取值  console.log通过浏览器的控制台打印
   a[1] = 10; //修改值
   console.log(typeof a)//object  数组属于object类型
   //获取数组长度
   a.length; //4
   ```

3. Function 方法

#### 12.3.4 运算符

##### 12.3.4.1 算数运算符

```
+,-,*,/,%
```

##### 12.3.4.2 赋值运算符

```
=,++,--,+=,-=,*=,/=
```

##### 12.3.4.3 逻辑元素符

```
&&,||,!
```

#### 12.3.5 数值和字符串的转换

##### 12.3.5.1 数值转字符串

```js
//方式一 隐式转换
var a = 123;
var b = a + '';
//方式二 方法转换
var a = 123;
var c = a.toString();
//方式三
var a = 123;
var b = String(a);
```

##### 12.3.5.2 字符串转数值

```js
//Number
var a = '123';
var b = Number(a);

//转换错误
var a = '123asd';
var b = Number(a);//得到结果NaN not a Number 数值运算错误
alert(typeof b); // number 

//parseInt
var a = '123asd';
var b = parseInt(a); //123

//parseFloat
var a = 1234.333;
var b = parseFloat(a);//1234.333

```

#### 12.3.6 流程控制

##### 12.3.6.1 条件判断

```js
var score = 200;
if(score > 200){
    console.log('大于200');
}else if(score == 200){
    console.log('等于200');
}else{
    console.log('小于200');
}
// == 和 ===
var a = 2;
var b = '2';
console.log(a == b);//比较的是值
console.log(a === b); //比较是值和数据类型

//三元运算符
if 1>2 ? 'true' :'false'
```

##### 12.3.6.2 switch语句

```js
var weather = prompt('请输入今天天气');
switch(weather){
    case '晴天':
        console.log('123');
        break;
    case '阴天':
        console.log('456');
         break;
    default:
        console.log('789');
         break;
}
```

#### 12.3.7 循环

##### 12.3.7.1 for循环

```js
for(var i=0;i<10;i++){
    console.log(i);
}
```

##### 12.3.7.2 while循环

```js
var a = 1;
while(a<100){
    console.log(a);
    a++;
}
```

##### 12.3.7.3 do while循环

先执行再判断，至少执行一次。

```js
var a = 0;
do{
    console.log(a);
    a++;
}while(a<100);
```

#### 12.3.8 js中的常用对象

##### 12.3.8.1 String 字符串

1. charAt和charCodeAt

   ```js
   var str = 'hello world';
   var b = str.charAt(3);
   console.log(b); //返回位于索引3处的字符
   
   var C = str.charCodeAt(3);
   console.log(C);//返回位于索引3处的字符编码
   ```

2. concat方法

   字符串拼接

   ```js
   var stringValue = "hello ";
   var result = stringValue.concat("world", "!");
   alert(result); //"hello world!" 
   
   //另一种拼接方式
   var name = 'alex';
   var age = 73;
   var str = `我叫${name},年龄是${age}`;
   alert(str);
   ```

3. 字符串截取

   - slice 参数：截取开始的位置上 ，截取结束的位置（不包括）

     ```js
     ar stringValue = "hello world";
     alert(stringValue.slice(3));//"lo world"
     alert(stringValue.slice(3, 7));//"lo w"
     ```

   - substring 参数：截取开始的位置上 ，截取结束的位置（不包括）

     ```js
     ar stringValue = "hello world";
     alert(stringValue.substring(3));//"lo world"
     alert(stringValue.substring(3,7));//"lo w"
     ```

   - substr 参数：截取开始的位置，截取的字符个数

     ```js
     ar stringValue = "hello world";
     alert(stringValue.substr(3));//"lo world"
     alert(stringValue.substr(3, 7));//"lo worl"
     ```

4. indexOf和lastIndexOf

   `indexOf()`方法从字符串的开头向后搜索子字符串，而 `lastIndexOf()`方法 是从字符串的末尾向前搜索子字符串 

   ```js
   var stringValue = "hello world";
   alert(stringValue.indexOf("o"));             //4
   alert(stringValue.lastIndexOf("o"));         //7
   alert(stringValue.indexOf("o", 6));         //7
   alert(stringValue.lastIndexOf("o", 6));     //4
   ```

5. trim方法

   去掉字符串两边的空格。

   ```js
   var stringValue = "   hello world   ";
   var trimmedStringValue = stringValue.trim();//hello world
   ```

6. 转大小写

   ```js
   //转大写
   var str = 'abc';
   console.log(str.toUpperCase());
   
   //转小写
   var str = 'ABC';
   console.log(str.toLowerCase());
   ```

##### 12.3.8.2 Array数组

1. isArray() 判断是否为一个数组

   ```js
   var ary = [1,2,3,4];
   console.log(Array.isArray(ary));
   ```

2. join连接方法

   ```js
   var colors = ['red','blue','green'];
   colors.join('||'); //red||blue||green
   ```

3. 栈方法

   - push方法：可以接收任意数量的参数，把它们逐个添加到数组末尾，并返回修改后数组的长度。

     ```js
     var ary = [1,2,3,4];
     var len = ary.push('23',44,66);
     console.log(len)
     ```

   - pop方法：从数组末尾移除最后一项，减少数组的 length 值，然后返回移除的项 

     ```js
     var ary = [1,2,3,4];
     var val = ary.pop();
     console.log(val)
     
     ```

4. 队列方法

   - unshift方法：在数组第一个位置插入
   - shift方法：在数组的第一个位置删除

5. concat方法

   - 参数为一个或多个数组，则该方法会将这些数组中每一项都添加到结果数组中。

   - 参数不为数组，则只是被简单地添加到结果数组的末尾

     ```js
     var ary = [1,2,3,4];
     var a = ary.concat({'123':123});//[1,2,3,4,{'123':123}]
     var b = ary.concat([5,6],[7,8]);//[1,2,3,4,5,6,7,8]
     ```

6. slice方法

   对数组进行切片。

   - 一个参数的情况下，slice()方法会返回从该参数指定位置开始到当前数组默认的所有项
   - 两个参数的情况下，该方法返回起始和结束位置之间的项——但不包括结束位置的项。

   ```js
   var ary = [1,2,3,4];
   var b = ary.slice(1); //[2,3,4]
   var c = ary.slice(1,3);//[2,3]
   var d = ary.slice(-2,-1);//使用数组长度分别和两个参数相加，得到结果再切片
   ```

7. splice方法

   - **删除**：可以删除任意数量的项，只需指定2个参数：要删除的第一项的位置和要删除的个数。例如`splice(0,2)`会删除数组中的前两项

     ```js
     var ary = [1,2,3,4,5,6];
     var b = ary.splice(1,2);
     console.log(b);// 结果为返回的值[2,3]
     ```

   - **插入**：可以向指定位置插入任意数量的项，只需提供3个参数：**起始位置**、**0（要删除的个数）**和**要插入的项**。如果要插入多个项，可以再传入第四、第五、以至任意多个项。例如，`splice(2,0,'red','green')`会从当前数组的位置2开始插入字符串`'red'`和`'green'`。

     ```js
     var ary = [1,2,3,4,5,6];
     ary.splice(1,0,'green','red');
     console.log(ary);//[1, "green", "red", 2, 3, 4, 5, 6]
     ```

   - **替换**：可以向指定位置插入任意数量的项，且同时删除任意数量的项，只需指定 3 个参数:**起始位置**、**要删除的项数**和**要插入的任意数量的项**。插入的项数不必与删除的项数相等。例如，`splice (2,1,"green")`会删除当前数组位置 2 的项，然后再从位置 2 开始插入字符串`"green"`。 

     ```js
     var ary = [1,2,3,4,5,6];
     var b = ary.splice(2,1,'green');
     console.log(ary);//[1,2,'red',4,5,6]
     console.log(b); //[3]返回值为删除的值
     ```

8. indexof和lastIndexOf

   indexOf()方法从数组的开头(位置 0)开始向后查找，lastIndexOf()方法则从数组的末尾开始向前查找。

   两个方法都返回要查找的那项在数组中的位置，或者在没找到的情况下返回-1。

```js
var numbers = [1,2,3,4,5,4,3,2,1];
alert(numbers.indexOf(4)); //3
alert(numbers.lastIndexOf(4));//5

//可以查找对象
var person = {name:"mjj"};
var people = [{name:'mjj'}];
var morePeople = [person];
alert(people.indexOf(person)); //-1
alert(morePeople.indexOf(person));// 0
```

9. forEach方法

它只是对数组中的每一项运行传入的函数。这个方法没有返回值。

```js
var numbers = [1,2,3,4,5,4,3,2,1];
numbers.forEach(function(item, index, array){
});//数组的项  数组的索引  数组
```

##### 12.3.8.3 date日期

1. 创建日期对象

   ```js
   var now = new Date();
   console.log(now);//Mon Jun 03 2019 21:15:52 GMT+0800
   ```

2. toLocaleString 格式化

   ```js
   var now = new Date();
   var s = now.toLocaleString();
   console.log(s);// 2019/6/3 下午9:15:52
   ```

3. getDate方法

   根据本地时间返回指定日期对象的月份中的第几天(1-31)

   ```js
   var now = new Date();
   console.log(now.getDate());
   ```

4. getMonth方法

   根据本地时间返回指定日期对象的月份(0-11)

   ```js
   var now = new Date();
   console.log(now.getMonth());
   ```

5. getFullYear方法

   根据本地时间返回指定日期对象的年份

   ```js
   var now = new Date();
   console.log(now.getFullYear());
   ```

6. getDay方法

   根据本地时间返回指定的日期对象的星期中的第几天(0-6) ,0代表星期日

   ```js
   var now = new Date();
   console.log(now.getDay());
   ```

7. getHours方法

   根据本地时间返回指定的日期对象的小时(0-23)

   ```js
   var now = new Date();
   console.log(now.getHours());
   ```

8. 根据本地时间返回指定的日期对象的分钟(0-59)

   ```js
   var now = new Date();
   console.log(now.getMinutes());
   ```

9. 根据本地时间返回指定的日期对象的秒数(0-59)

   ```js
   var now = new Date();
   console.log(now.getSeconds());
   ```

##### 12.3.8.4 Math数学对象

1. 舍入方法

   - `Math.ceil()`执行向上舍入，即它总是将数值向上舍入为最接近的整数;
   - `Math.floor()`执行向下舍入，即它总是将数值向下舍入为最接近的整数;  
   - `Math.round()`执行标准舍入，即它总是将数值四舍五入为最接近的整数(这也是我们在数学课上学到的舍入规则)。 

   ```js
   var num = 25.7;
   var num2 = 25.2;
   alert(Math.ceil(num));//26
   alert(Math.floor(num));//25
   alert(Math.round(num));//26
   alert(Math.round(num2));//25
   ```

2. random方法

   返回大于等于 0 小于 1 的一个随机数 

   ```js
   //获取min到max的范围的整数
   function random(lower, upper) {
       return Math.floor(Math.random() * (upper - lower)) + lower;
   }
   ```

3. 最大值和最小值

   - max方法，取最大值

     ```js
     var a = Math.max(1,4,7,23,56);
     console.log(a);
     ```

   - min方法，取最小值

     ```js
     var a = Math.min(1,4,7,23,56);
     console.log(a);
     ```

   - apply()方法，第一个参数：传入一个对象改变调用apply方法的函数内this的指向，第二个参数：传入一个数组apply方法会将数组中的每个元素传入调用apply方法的函数

     ```js
     //可以利用apply传入任意数组，比较数组内元素的大小
     var a = Math.max.apply(null,[4,52,1,45,99]);
     console.log(a);
     ```

#### 12.3.9 函数

1. 普通函数

   ```js
   function fn(){
       
   }
   fn();
   ```

2. 函数表达式

   ```js
   var fn = function(){
       //这是一个匿名函数
   }
   ```

3. 自执行函数

   ```js
   ;(function(){
       //内部的this一定指向window
   })(); //为了防止之前有语句未写分号，所以先加个分号
   ```

**注：全局作用域下，函数作用域，自执行函数的this都指向window**

```js
var obj = {name:'mjj'};
function fn(){
    console.log(this.name)
}
fn();//是空值，因为函数内部的this指向了window
fn.call(obj);//打印mjj,call方法将函数内部的this改成了传入的对象
```

#### 12.3.10 BOM模型

浏览器对象模型，核心对象是window对象。

##### 12.3.10.1 BOM模型的对话框

###### 1 弹出框

```js
window.alert('mjj');//window可以省略不写
```

###### 2 提示框

```js
var ret = window.confirm('您确实要删除吗？');
console.log(ret);//点击确定按钮返回true,点击取消按钮返回false
```

###### 3 输入框

```js
var val = window.prompt('请输入姓名');
console.log(val);//返回输入的值
```

##### 12.3.10.2 定时方法

###### 1 设置延时 setTimeout

```js
//window.setTimeout(回调函数,延迟的时间)  非阻塞
window.setTimeout(fn,2000);
function fn(){
    console.log(111);
}
console.log(222);//先打印222，后打印111
```

###### 2 定时器 setInterval

```js
//window.setInterval(回调函数,执行间隔)
num = 0;
window.setInterval(function(){
    console.log(num);
    num++;
},1000);

//停止定时器
num = 0;
var timer = window.setInterval(func,1000);
function func(){
    console.log(num);
    num++;
    if(num === 5){
        window.clearInterval(timer);
    }
}
```

##### 12.3.10.3 location对象

location对象是window对象的一个属性。

```js
//通过console.dir(对象)查看对象的所有属性和方法
console.dir(window.location);
//reload方法 刷新整个网页（一般用来做测试）
window.location.reload();
//局部刷新，ajax技术：在不重载页面的情况下，对网页进行操作。
```

#### 12.3.11 DOM模型

文档对象模型。

javascript中的对象分为三种：

- 用户自定义对象：用户自己创建的对象。
- 内建对象：Array,Math,Function
- 宿主对象：window,document

DOM中的节点分类：

```html
<!--元素节点-->
<p></p>  
<!--属性节点-->
<p title=''></p>
<!--文本节点-->
<p>.....</p>
```

##### 12.3.11.1 获取元素节点

- getElementById 根据元素id获取元素节点

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <div id="box">123</div>
      <script type="text/javascript">
          var box = document.getElementById('box');
          console.log(box);
      </script>
  </body>
  </html>
  ```

- getElementsByTagName 根据元素名获取元素节点，返回一个伪数组。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
      </ul>
      <script type="text/javascript">
          var lis = document.getElementsByTagName('li');
          console.log(lis);
      </script>
  </body>
  </html>
  ```

- getElementsByClassName 通过类名获取元素节点,返回一个伪数组。

  ```html
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <title>Title</title>
  </head>
  <body>
      <ul>
          <li class="active">1</li>
          <li>2</li>
          <li class="active">3</li>
          <li>4</li>
      </ul>
      <script type="text/javascript">
          var lis = document.getElementsByClassName('active');
          console.log(lis);
      </script>
  </body>
  </html>
  ```

##### 12.3.11.2 事件

###### 1 单击事件

```js
var box = document.getElementById('box');
box.onclick = function(){
    console.log('点击');
}
```

###### 2 鼠标悬浮事件

```js
var box = document.getElementById('box');
box.onmouseover = function(){
    console.log('悬浮');
}
```

###### 3 鼠标离开事件

```js
var box = document.getElementById('box');
box.onmouseout = function(){
    console.log('离开');
}
```

##### 12.3.11.3 对样式进行操作

```js
var box = document.getElementById('box');
box.style.backgroundColor = 'red'//css中所有的带有-的属性在js中都改为驼峰命名法
```

##### 12.3.11.4 对属性进行设置

- 方式一

  ```js
  //getAttribute(name) 获取属性值
  var box = document.getElementById('box');
  var val = box.getAttribute('class');
  
  //setAttribute(name,value) 设置属性值
  var box = document.getElementById('box');
  box.setAttribute('class','a1 a2'); //会覆盖原来的属性值
  ```

- 方式二

  ```js
  //获取属性值
  var box = document.getElementById('box');
  var val =  box.className;//获取class属性值
  //设置属性值
  var box = document.getElementById('box');
  box.abc = '123';//自定义属性
  box.className += ' active'//多添加一个属性 
  ```

  **注：如果是自定义的属性，想要在文档上可以看到需要使用setAttribute,否则只能在对象属性中看到。**

##### 12.3.11.5 节点的创建、添加和删除

```js
var li = document.createElement('li');//创建一个节点
var ul = document.getElementById('box');
li.innerText = '123'; //只设置节点文本
li.innerHtml = '<a>123</a>'; //设置节点内html
ul.appendChild(li);//添加元素
ul.removeChild(li);//删除元素

//在一个节点前添加一个新节点insertBefore(新节点,旧节点)
ul.insertBefore(li2,li1);
```

##### 12.3.11.6 遍历数据操作页面

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <ul id="box">
    </ul>
    <script type="text/javascript">
        var data = [
            {id:'1',name:'小米5',price:99},
            {id:'2',name:'小米6',price:199},
            {id:'3',name:'小米7',price:299},
            {id:'4',name:'小米8',price:399}
        ];
        var ul = document.getElementById('box');
        for(var i=0;i<data.length;i++){
            var li = document.createElement('li');
            li.innerHTML = `<p>${data[i].name}</p><p>价格:${data[i].price}</p>`;
            ul.appendChild(li);
        }
    </script>
</body>
</html>
```

##### 12.3.11.7 轮播图实现

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<div id="box">
			<img src="images/1.jpg" alt="" id="imgBox">
		</div>
		<button id="prev">上一张</button>
		<button id="next">下一张</button>

		<script type="text/javascript">
			var imgBox = document.getElementById('imgBox');
			var next = document.getElementById('next');
			var prev = document.getElementById('prev');
			var num = 1;
			next.onclick = function (){
				nextImg();
			};
			prev.onclick = function () {
                prevImg();
            };
			function prevImg() {
			    if(num === 1){
			        num = 4
                }
			    imgBox.src = `images/${num}.jpg`;
                num--;
            }
			function nextImg(){
				num++;
				if(num === 5){
					num = 1;
				}
				imgBox.src = `images/${num}.jpg`;
			}
			setInterval(nextImg,3000);
		</script>
	</body>
</html>
```

### 12.4 jQuery

#### 12.4.1 简介

jQuery是一个快速，小巧，功能丰富的JavaScript库。

它通过易于使用的API在大量浏览器中运行，使得Html文档遍历和操作，事件处理，动画和AJAX变得更加简单。通过多功能性和可扩展性的结合，jQuery改变了数百万人编写JavaScript的方式。

操作：获取节点元素对象，属性操作，样式操作，类名，节点的创建，删除，添加和替换。

核心：write less,do more

#### 12.4.2 基础选择器

##### 12.4.2.1 基础选择器

- id选择器

  ```js
  $('#box')//得到一个jquery对象
  ```

- 类选择器

  ```js
  $('.box')
  ```

- 标签选择器

  ```js
  $('div')
  ```

- 后代选择器

  ```js
  $('#box .active')
  ```

- 子代选择器

  ```js
  $('#id>.active')
  ```

- 属性选择器

  ```js
  $('input[type=submit]')
  ```

##### 12.4.2.2 jquery对象和js对象的相互转换

- jquery对象转化js对象

  ```js
  $('#id')[0]//通过对jquery对象加索引，取到的每一个元素就是js对象
  ```

- js对象转化成jquery对象

  ```js
  var div = document.getElementById('box');
  $(div)//使用$可以将js对象转换成jquery对象
  ```

#### 12.4.3 事件

- 单击事件

  ```js
  //自动遍历所有选择的节点元素，为它们绑定事件
  $('.box').click(function(){
      console.log($(this));//回调函数内部的this指向js节点元素本身
  })
  ```

- 鼠标悬浮事件

  ```js
  $('.box').mouseover(function(){
      console.log('悬浮');
  })
  ```

- 鼠标离开事件

  ```js
  $('.box').mouseout(function(){
      console.log('离开');
  })
  ```

- 聚焦事件

  ```js
  $('input').focus(function(){
       console.log('聚焦');
  })
  ```

- 失焦事件

  ```js
  $('input').blur(function(){
       console.log('失焦');
  })
  ```

- 内容改变事件

  可用于单选，复选，下拉列表，input输入框内容的改变

  ```js
  $('input').change(function(){
       console.log($(this).val());//通过val()方法获取input的value值。通过text()方法和html()方法获取双闭合标签内部的文本或html
  })
  ```

12.4.4 设置样式

```js
//设置单个样式
$(this).css('color,red')
$(this).css('font-size,12px')
//设置多个样式
$(this).css({'color':'red','font-size':12})
//获取样式
var ret = $(this).css('color')
```

#### 12.4.5 设置类

```js
//为节点元素添加类
$(this).addClass('active')
//为节点元素移除类
$(this).removeClass('active')
//开关，有就删除，没有就添加
$(this).toggleClass('active')
```

#### 12.4.6 操作值

- 操作文本值

  ```js
  //获取值
  var val = $('div').text();
  //设置值
  $('div').text('123');
  ```

- 操作html值

  ```js
  //获取值
  var val = $('div').html();
  //设置值
  $('div').html('123');
  ```

- 操作input值

  ```js
  //获取值
  var val = $('div').val();
  //设置值
  $('div').html('123');
  ```

#### 12.4.7 基本过滤选择器

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<ul>
			<li class="a">
				<a href="#">
					mjj
				</a>
			</li>
			<li class="b">wusir</li>
			<li class="c">alex</li>
			<li class="d">sb</li>
		</ul>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
		 	console.log($('ul li:eq(1)'));//过滤出索引等于1的li标签
		 	console.log($('ul li:gt(1)'));//过滤出索引大于1的li标签
			console.log($('ul li:lt(1)'));//过滤出索引小于1的li标签
            console.log($('ul li:odd'));//过滤出所有索引为奇数的li标签
            console.log($('ul li:even'));//过滤出所有索引为偶数的li标签
			console.log($('ul li:first'));//过滤出第一个li标签
			console.log($('ul li:last'));//过滤出最后一个li标签
		</script>
	</body>
</html>
```

#### 12.4.8 筛选选择器

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<ul>
			<li id="first" class="a">
				<a href="#">
					mjj
				</a>
			</li>
			<li class="b">wusir</li>
			<li class="c">alex</li>
			<li class="d">sb</li>
            <a href=""></a>
		</ul>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			// 筛选方法
			// 即选择儿子又选择孙子....
			console.log($('ul').find('a'));//后代筛选器
			// 只选择亲儿子
			console.log($('ul').children());//子代选择器
			console.log($('li a').parent());//父代选择器
            console.log($('ul #first').siblings('li'));//兄弟选择器(不包括自己)，选择li的所有兄弟li节点元素，
		</script>
	</body>
</html>
```

**补充：**

```js
console.log(document);//获取文档对象
console.log(document.body);//获取body对象
console.log(document.documentElement);//获取html对象
```

#### 12.4.9 动画

##### 12.4.9.1 显示和隐藏

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<style>
			#box{
				width: 200px;
				height: 200px;
				background-color: red;
				display: none;
			}
		</style>
	</head>
	<body>
		<button id="btn">显示</button>
		<div id="box"></div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$('#btn').click(function(){
                if($(this).text() === '显示'){
				 	$('#box').show(1000,function(){
						$('#btn').text('隐藏');
					});//显示动画，回调函数在动画执行完成后会执行
				}else{
					$('#box').hide(1000,function(){
						$('#btn').text('显示');
					});//显示动画，回调函数在动画执行完成后会执行

				}
				//$('#box').stop().toggle(2000);//toggle相当于一个开关在显示和隐藏之间来回切换，但快速切换，动画响应不及时会产生bug，所以在每次切换前调用stop方法结束上次未结束的动画效果。
			})
		</script>
	</body>
</html>
```

##### 12.4.9.2 滑入和滑出

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<style>
			#box{
				width: 200px;
				height: 200px;
				background-color: red;
				display: none;
			}
		</style>
	</head>
	<body>
		<button id="btn">显示</button>
		<div id="box"></div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$('#btn').click(function(){
                if($(this).text() === '显示'){
				 	$('#box').slideDown(1000,function(){
						$('#btn').text('隐藏');
					});

				}else{

					$('#box').slideUp(1000,function(){
						$('#btn').text('显示');
					});

				}

				//$('#box').stop().slideToggle(2000);

			})
		</script>
	</body>
</html>
```

##### 12.4.9.3 淡入和淡出

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
		<style>
			#box{
				width: 200px;
				height: 200px;
				background-color: red;
				display: none;
			}
		</style>
	</head>
	<body>
		<button id="btn">显示</button>
		<div id="box"></div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">

			$('#btn').click(function(){
                if($(this).text() === '显示'){
				 	$('#box').fadeIn(1000,function(){
						$('#btn').text('隐藏');
					});

				}else{

					$('#box').fadeOut(1000,function(){
						$('#btn').text('显示');
					});

				}

				//$('#box').stop().fadeToggle(2000);

			})
		</script>
	</body>
</html>
```

##### 12.4.9.4 自定义动画

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <style>
        div {
            position: absolute;
            left: 20px;
            top: 30px;
            width: 100px;
            height: 100px;
            background-color: green;
        }
    </style>
    <script src="js/jquery.js"></script>
    <script>
        jQuery(function () {
            $("button").click(function () {
                var json = {"width": 500, "height": 500, "left": 300, "top": 300, "border-radius": 100};
                var json2 = {
                    "width": 100,
                    "height": 100,
                    "left": 100,
                    "top": 100,
                    "border-radius": 100,
                    "background-color": "red"//颜色对动画不起作用，需要使用插件
                };

                //自定义动画
                $("div").animate(json, 5000, function () {
                    $("div").animate(json2, 1000, function () {
                        alert("动画执行完毕！");
                    });
                });//第一个参数为动画执行完整后的样式，第二个参数为执行时间，第三个参数为动画执行完成后的回调函数

            })
        })
    </script>
</head>
<body>
<button>自定义动画</button>
<div></div>
</body>
</html>
```

#### 12.4.10 文档加载完成事件

当文档未加载完成之前，对内部的元素进行操作，绑定事件是不生效的，因为节点还不存在。所以对节点元素的操作要放在文档加载完成后执行。

- jQuery中的文档加载完成事件，事件不会覆盖。

  ```js
  //完整
  $(document).ready(function(){
      console.log($('div'));
  })
  
  //简写
  $(function(){
      console.log($('div'));
  })
  ```

- js中的文档加载完成事件，js中的事件有覆盖效果，后面再写前面的就不生效了。

  ```js
  window.onload = function(){
      console.log($('div'));
  }
  ```

#### 12.4.11 对属性的操作

##### 12.4.11.1 attr和removeAttr方法

操作的标签上的属性。

- 设置或读取属性 attr

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<ul>
  			<li class="a">mjj</li>
  			<li class="b">wusir</li>
  			<li class="c">alex</li>
  			<li class="d">sb</li>
  		</ul>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  		 	$('ul li').attr('index',1);//设置所有匹配节点元素的属性和值
              $('ul li').attr({'index':1,'b':2});//设置多个属性
              console.log($('ul li').attr('index'));//这能获取匹配到的第一个节点元素的属性值
  		</script>
  	</body>
  </html>
  ```

- 移除属性 removeAttr

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<ul>
  			<li index="1" class="a">mjj</li>
  			<li index="2" class="b">wusir</li>
  			<li index="3" class="c">alex</li>
  			<li index="4" class="d">sb</li>
  		</ul>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  		 	$('ul li:odd').removeAttr('index');//为每一个匹配的节点元素移除一个属性
              $('ul li:odd').removeAttr('index active');//为每一个匹配的节点元素移除多个属性
  		</script>
  	</body>
  </html>
  ```

##### 12.4.11.2 prop和removeProp方法

操作的是节点元素对象的属性

- 设置或读取属性 prop

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  			$(function(){
  				console.log($('input[type=radio]').eq(0).prop('checked'));
  			})
  		</script>
  	</head>
  	<body>
  		<p id="box">mjj</p>
  		<input type="radio" name='sex' checked='checked'>男
  		<input type="radio" name='sex'>女
  		
  	</body>
  </html>
  ```

- 移除属性 removeProp

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  			$(function(){
  				console.log($('input[type=radio]').eq(0).removeProp('checked'));
  			})
  		</script>
  	</head>
  	<body>
  		<p id="box">mjj</p>
  		<input type="radio" name='sex' checked='checked'>男
  		<input type="radio" name='sex'>女
  		
  	</body>
  </html>
  ```

#### 12.4.12 文档操作

- 父.append(子)    向父元素内的后面追加元素，返回父元素对象

  ```js
  //参数为js对象
  var obj = document.createElement('h3');
  obj.innerText = 'MJJ';
  $('#box').append(obj);//添加一个js对象，若js节点元素在文档中已存在，则为改变节点元素位置
  
  //参数为jQuery对象
  $('#box').append($('h4'));//若参数为jQuery对象，则为改变节点元素位置
  
  //参数为html
  $('box').append('<h4>mjj</h4>');
  
  //参数为文本
  $('box').append('mjj');
  ```

- 子.appendTo(父)  将子元素追加到父元素内的后面，返回子元素对象。用起来更方便可以链式编程在后面继续调用方法，操作子元素。

  ```js
  $('<a href="#">百度一下</a>').appendTo('#box').css('color','yellow');
  ```

- 父.prepend(子)  向父元素内的前面追加元素，返回父元素对象

  ```js
  $('#box').prepend('<p>hello world</p>');
  ```

- 子.prependTo(父)  将子元素追加到父元素内的前面，返回子元素对象。用起来更方便可以链式编程在后面继续调用方法，操作子元素。

  ```js
  $('<p>hello world</p>').prependTo('#box');
  ```

- 元素1.before(元素2)  将元素2添加到元素1的前边

  ```js
  $('h2').before('<p>hello world</p>');
  ```

- 元素1.insertBefore(元素2)  将元素1添加到元素2的前边

  ```js
  $('<h3>女神</h3>').insertBefore('h2');
  ```

- 元素1.after(元素2)  将元素2添加到元素1的后边

  ```js
  $('h2').after('<p>hello world</p>');
  
  ```

- 元素1.insertAfter(元素2)  将元素1添加到元素2的后边

  ```js
  $('<h3>女神</h3>').insertAfter('h2');
  ```

- 被替换的元素.replaceWith(要替换成的元素) 用提供的内容替换集合中所有匹配的元素并且返回被删除元素的集合。 

  ```js
  <div class="container">
    <div class="inner first">Hello</div>
    <div class="inner second">And</div>
    <div class="inner third">Goodbye</div>
  </div>
  
  $('div.inner').replaceWith('<h2>New heading</h2>');
  
  <div class="container">
    <h2>New heading</h2>
    <h2>New heading</h2>
    <h2>New heading</h2>
  </div>
  ```

- 要替换成的元素.replaceAll(被替换的元素)   返回要替换的元素

  ```js
  <div class="container">
    <div class="inner first">Hello</div>
    <div class="inner second">And</div>
    <div class="inner third">Goodbye</div>
  </div>
  
  $('<h2>New heading</h2>').replaceAll('.inner');
  
  <div class="container">
    <h2>New heading</h2>
    <h2>New heading</h2>
    <h2>New heading</h2>
  </div>
  ```

- 元素.remove(选择器) 移除匹配的元素，元素内部绑定的事件也会被移除。返回移除的元素对象。

  ```js
  <div class="container">
    <div class="hello">Hello</div>
    <div class="goodbye">Goodbye</div>
  </div>
  //remove()参数为空
  $('.hello').remove();
  //结果
  <div class="container">
    <div class="goodbye">Goodbye</div>
  </div>
  
  //参数不为空
  $('div').remove('.hello');
  //结果
  <div class="container">
    <div class="goodbye">Goodbye</div>
  </div>
  ```

- 元素.detach(选择器)  删除所有匹配的元素，事件会被保留。返回删除的元素对象。

  ```js
  <p class="hello">Hello</p> how are <p>you?</p>
  
  $("p").detach(".hello");
  
  how are <p>you?</p>
  ```

- 匹配元素.empty()  移除所有匹配的元素节点的所有子节点，无参数，内部嵌套的元素也会被移除。

  ```js
  <div class="container">
    <div class="hello">Hello</div>
    <div class="goodbye">Goodbye</div>
  </div>
  
  $('.hello').empty();
  
  <div class="container">
    <div class="hello"></div>
    <div class="goodbye">Goodbye</div>
  </div>
  ```

#### 12.4.13 事件

##### 12.4.13.1 鼠标键盘事件

- 单击事件click和双击事件dblclick

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  	<button>点击</button>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
              //解决一次双击击事件会执行两次单击事件问题
              //解决思路：我们在单击事件中添加一个延时，若在延时所在的时长内没有做其他操作，则执行单击事件，若在延时的时长内，再次点击控件，则取消延时程序。这样就可以在一定程度上解决这个冲突。当然这个延时的时长设置是需要斟酌的，若太长，则单击事件有种缓慢的感觉，若太短，则起不到分离单击事件和双击事件的效果。
  			var times = null;
  			$('button').click(function () {
  				// 取消上次延时未执行的方法
  				clearTimeout(times);
  				//执行延时
  				times = setTimeout(function(){
  				console.log('单击了');
  			},300);
  			});
  
  			$('button').dblclick(function () {
  				clearTimeout(times);
  				console.log('双击了')
  			});
  		</script>
  	</body>
  </html>
  ```

  

- mouseover  鼠标悬浮事件，鼠标穿过父级元素和子元素都会调用方法

  ```js
  $('#box').mouseover(function(){
      console.log('移入进来了');
  
      $('#child').slideDown(1000);
  })
  ```

- mouseout    鼠标离开事件，鼠标穿过父级元素和子元素都会调用方法

  ```js
  $('#box').mouseout(function(){
      console.log('移除了');
  
      $('#child').slideUp(1000);
  })
  ```

- mouseenter  鼠标悬浮事件，鼠标穿过父级元素会调用方法

  ```js
  $('#box').mouseenter(function(){
      console.log('进来了');
  
      $('#child').stop().slideDown(1000);
  })
  ```

- mouseleave  鼠标离开事件，鼠标穿过父级元素会调用方法

  ```js
  $('#box').mouseleave(function(){
      console.log('离开了');
  
      $('#child').stop().slideUp(1000);
  })
  ```

- hover   鼠标悬浮和离开事件，接收两个回调函数分别对应悬浮和离开动作要做的操作

  ```js
  $('#box').hover(function(){
      $('#child').stop().slideDown(1000);
  },function(){
      $('#child').stop().slideUp(1000);
  })
  ```

- focus  聚焦事件

  ```js
  //默认文档加载聚焦行为
  $('input[type=text]').focus();
  
  
  $('input[type=text]').focus(function(){
      //聚焦
      console.log('聚焦了');
  })
  ```

- blur  失去焦点事件

  ```js
  $('input[type=text]').blur(function(){
      //失焦
      console.log('失去焦点了');
  })
  ```

- keydown  当键盘的键按下时触发

  ```js
  //e为事件对象，内部封装了大量事件相关的信息
  $('input[type=text]').keydown(function(e){
      console.log(e.keyCode);
      switch (e.keyCode){ //通过事件对象的keyCode属性，获取按下了哪个键（对应的ascii码）
          case 8:
              $(this).val(' ')
              break;
          default:
              break;
      }	
  })
  ```

##### 12.4.13.2 表单事件

- change  可用于单选，复选，下拉列表，input输入框内容的改变

  ```js
  $('input').change(function(){
       console.log($(this).val());//通过val()方法获取input的value值。通过text()方法和html()方法获取双闭合标签内部的文本或html
  })
  ```

- submit  点击提交按钮式触发

  ```html
  <!DOCTYPE html>
  <html>
  	<head>
  		<meta charset="utf-8">
  		<title></title>
  	</head>
  	<body>
  		<a href="javascript:;">百度游戏啊</a>//还可以通过该方式阻止a标签的连接
  		<form action="javascript:void(0)"> //也可以通过该方式阻止表单的默认提交方式
  			<input type="text" placeholder="用户名">
  			<input type="password" placeholder="密码">
  			<input type="submit">
  		</form>
  		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
  		<script type="text/javascript">
  			$('form').submit(function(e){
  				e.preventDefault();//通过事件对象的preventDefault方法，阻止表单的默认提交action方式。
  				console.log('11111');
  			})
  		</script>
  	</body>
  </html>
  ```

- select   当选中时触发

  ```html
  <form>
    <input id="target" type="text" value="Hello there" />
  </form>
  <div id="other">
    Trigger the handler
  </div>
  <script>
      $('#target').select(function() {
        alert('Handler for .select() called.');
      });
  </script>
  ```

#### 12.4.14 Ajax

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<div id="box">
			
		</div>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script type="text/javascript">
			$(function(){
				// 获取首页的数据
				$.ajax({
					url:'https://api.apeland.cn/api/banner/',
					methods:'get',
					success:function(res){ //请求成功的回调，res包括了服务器返回的数据
						console.log(res);
						if(res.code === 0){
							var cover = res.data[0].cover;
							var name = res.data[0].name;
							console.log(cover);
							$('#box').append(`<img src=${cover} alt=${name}>`)
						}
					},
					error:function(err){
						console.log(err);
					}
				})
			})
		</script>
	</body>
</html>
```

## 十二、Django框架

### 12.1 web框架本质

#### 12.1.1 本质

实现socket服务端。

#### 12.1.2 Http协议

超文本传输协议（英文：Hyper Text Transfer Protocol，HTTP）是一种用于分布式、协作式和超媒体信息系统的应用层协议。

- 工作在应用层
- 双工通信

#### 12.1.3 Http请求方法

- GET
- POST
- HEAD
- PUT
- DELETE
- TRACE
- OPTIONS
- CONNECT

#### 12.1.4 Http状态码

- 1xx：消息，请求已被服务器接收，继续处理。
- 2xx：成功，请求已成功被服务器接收、理解、返回。
- 3xx：重定向，需要后续操作才能完成这一请求。
- 4xx：请求错误，请求含有词法错误或无法被执行。
- 5xx：服务器错误，服务器在处理一个正确请求时发生错误。

#### 12.1.5 URL

超文本传输协议（HTTP）的统一资源定位符将从因特网获取信息的五个基本元素包括在一个简单的地址中：

- 传送协议
- 层级URL标记符号（为//，固定不变）
- 服务器（通常为域名，有时为IP地址）
- 端口号（以数字方式表示，若为Http的默认值":80"可以省略）
- 路径 （以"/"字符区别路径中的每一个目录名称）
- 查询（GET模式的窗体参数，以"?"字符为起点，每个参数以"&"隔开，再以"="分开参数名称和数据，通常以UTF-8的URL编码，避开字符冲突的问题）
- 片段 以"#"字符为起点

```
以http://www.luffycity.com:80/news/index.html?id=250&page=1 为例, 其中：

http，是协议；
www.luffycity.com，是服务器；
80，是服务器上的网络端口号；
/news/index.html，是路径；
?id=250&page=1，是查询。
```

#### 12.1.6 HTTP格式

1. 请求格式

   ![](E:\Python\11Django\http请求格式.jpg)

2. 响应格式

   ![](E:\Python\11Django\http响应格式.jpg)

#### 12.1.7 web框架的功能

1. socket收发消息 wsgireg   uwsgi
2. 根据不同的路径返回不同的页面
3. 返回动态页面（字符串的替换）jinja2

各web框架实现的功能：

- django：2,3
- flask：2
- tornado：1,2,3

### 12.2 django的下载安装使用

#### 12.2.1 下载安装

1. 命令行

   ```
   pip3 install django==1.11.21 -i https://pypi.tuna.tsinghua.edu.cn/simple
   ```

2. pycharm

   ![](E:\Python\11Django\django下载安装.png)

#### 12.2.2 创建项目

1. 命令行：

   找一个文件夹存放项目文件：

   打开终端：

   django-admin startproject 项目名称

   项目目录:

![](E:\Python\11Django\django项目目录.png)

2. pycharm

   ![](E:\Python\11Django\pycharm创建django项目.png)

#### 12.2.3 启动项目

1.命令行

切换到项目的根目录下  manage.py

`python36 manage.py runserver `  ——默认 <http://127.0.0.1:8000/>

`python36 manage.py runserver 80 `   在80端口启动

`python36 manage.py runserver 0.0.0.0:80`   0.0.0.0:80  指定IP和端口

通过修改配置文件settings.py

```
ALLOWED_HOSTS = ['*']  //允许所有ip连接
```

2.pycharm

点绿三角启动 可配置

![](E:\Python\11Django\项目配置.png)

![](E:\Python\11Django\python修改ip和端口号.png)

#### 12.2.4 简单使用

urls.py文件

```python
from django.conf.urls import url
from django.contrib import admin
from django.shortcuts import render,HttpResponse //引入模块

def index(request): 页面函数
    #return HttpResponse(b'hello world')
    return render(request,'index.html')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^index/', index),
]
```

### 12.3 Django的配置

#### 12.3.1 模板配置

在settings.py文件中配置模板文件夹路径

```python
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR,'templates')], #配置模板文件夹路径
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
```

#### 12.3.2 调试post请求配置

为解决在调试发送post请求时发生的403错误，在settings.py文件的中间件配置项中做如下修改：

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',   #注释掉该项
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
```

#### 12.3.3 静态文件配置

静态文件指本地的一些图片，字体，js，css等文件。

配置静态文件

```pyhton
STATIC_URL = '/static/' #别名
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'), #配置静态文件夹的名称
    os.path.join(BASE_DIR, 'static1'),
    os.path.join(BASE_DIR, 'static2'),
]
```

静态文件的使用

```python
<link rel="stylesheet" href="/static/css/login.css">   # 别名开头,django会在配置的文件夹内寻找资源，找到后不再向后
```

#### 12.3.4 app

每次都把页面对应的函数写在urls.py文件中，会很冗余。所以创建app来存放一类功能类似的页面函数。使项目结构更清晰合理。

##### 12.3.4.1 创建app

1. 命令行创建

   ```python
   python36 manage.py startapp app名称
   ```

2. pycharm创建

   tools-->run manage.py task

   ```python
   startapp app名称
   ```

##### 12.3.4.2 注册app

在settings文件中

```python
INSTALLED_APPS = [
    ...
    'app01',
    'app01.apps.App01Config', #推荐写法
]
```

##### 12.3.4.3 app目录结构

```python
app
 -- migrations  #ORM数据库迁移记录文件夹
 -- __init__.py  
 -- admin.py  #Django后台管理工具
 -- apps.py   #app相关
 -- models.py #ORM相关
 -- tests.py  
 -- views.py  #写函数的文件
```

#### 12.3.5 数据库配置

在settings.py文件中进行如下配置

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'studentManage',
        'HOST': '127.0.0.1',
        'PORT': 3306,
        'USER': 'root',
        'PASSWORD': '123'
    }
}
```

然后在与settings.py同级的init.py文件中，加上如下代码

```python
import pymysql
pymysql.install_as_MySQLdb()  #使用pymysql代替mysqldb来操作数据库
```

### 12.4 ORM

#### 12.4.1 简介

- 对象关系映射（Object Relational Mapping，简称ORM）模式是一种为了解决面向对象与关系数据库存在的互不匹配的现象的技术。
- 简单的说，ORM是通过使用描述对象和数据库之间映射的元数据，将程序中的对象自动持久化到关系数据库中。
- ORM在业务逻辑层和数据库层之间充当了桥梁的作用。

#### 12.4.2 ORM优缺点

优点：

- ORM解决的主要问题是对象和关系的映射。它通常将一个类和一张表一一对应，类的每个实例对应表中的一条记录，类的每个属性对应表中的每个字段。 
- ORM提供了对数据库的映射，不用直接编写SQL代码，只需操作对象就能对数据库操作数据。
- 让软件开发人员专注于业务逻辑的处理，提高了开发效率。

缺点：

- ORM的缺点是会在一定程度上牺牲程序的执行效率。
- ORM定义好的操作是可以完成的，一些复杂的查询操作是完成不了。
- 关系数据库相关技能退化...

#### 12.4.3 Django中使用Mysql数据库的流程

1. 创建一个MySQL数据库。

2. 在settings中配置，django连接Mysql数据库：

   ```python
   DATABASES = {
       'default':{
          'ENGINE':'django.db.backends.mysql', #引擎
           'NAME' : 'day53', #数据库名称
           'HOST' : '127.0.0.1', #ip地址
           'PORT' : 3306, #端口号
           'USER' : 'root', #用户名
           'PASSWORD' : '123' #密码
       }
   }
   ```

3. 在与settings同级目录下的init文件中写：

   ```python
   import pymysql
   pymysql.install_as_MySQLdb()
   ```

4. 创建表（在app下的models.py中写类）

   - 类----表
   - 对象---一行数据
   - 属性----字段值

   ```python
   class User(models.Model):
       username = models.CharField(max_length=32) #varchar(32)
       password = models.CharField(max_length=32)
   ```

5. 执行数据库迁移的命令

   ```python
   python manage.py makemigrations #检测每个注册的app下的models.py 记录models的变更记录，后面也可跟app名称，表示只检测该app下的models文件的变更记录
   python manage.py migrate #同步变更记录到数据中
   ```

#### 12.4.4 ORM操作

##### 12.4.4.1 DDL

创建表约束

```python
class User(models.Model):
    uid = models.AutoField(primary_key=True) #自增主键
    name = models.CharField(max_length=32,unique=True) #唯一
    gender = models.CharField(max_length=2,default='男') #默认值
```

```python
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE) #默认关联Publisher表的主键
    #pub = models.ForeignKey('Publisher',on_delete=models.CASCADE)  也可以通过字符串的方式创建 
```

on_delete  在django2.0 版本之后是必填的参数    1.11  之前的可以不填 

on_delete 的参数  

- models.CASCADE   级联删除

- models.SET()   设置指定的值

- models.SET_DEFAULT    设置默认值

  ```python
  pub = models.ForeignKey(Publisher,on_delete=models.SET_DEFAULT,default=0)
  ```

- models.SET_NULL 设置为空

**注意：**

数据库的表在创建时会在model中的外键字段后加上`_id`，在django代码中查询model的外键名得到的是一个关联的表对应类的对象，可以用`外键名_id`来获取数据库中外键的值。

```python
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE) #数据库中外键字段名为pub_id
    
    
book = models.Book.objects.get(bid=1) 
print(book.pub) #得到Publisher对象
print(book.pub_id) #得到外键值
print(book.pub.pid) #得到外键值
```

##### 12.4.4.2 DML

1. 查询数据

    ```python
    # 获取表中所有的数据
    ret = models.User.objects.all()  # QuerySet 对象列表  【对象】
    # 获取一个对象（有且唯一）
    obj = models.User.objects.get(username='alex')   # 获取不到或者获取到多个对象会报错
    # 获取满足条件的对象
    ret = models.User.objects.filter(username='alex1',password='dasb')  # QuerySet 对象列表
    ```

2. 添加数据

    ```python
    models.User.objects.create(name='alex',gender='女')
    ```

3. 删除数据

    ```python
    obj_list = models.User.objects.filter(gender='男') 
    obj_list.delete()#删除一条或多条数据

    obj = models.User.objects.get(pk=1) #pk代表主键字段
    obj.delete()#删除一条数据
    ```

4. 修改数据

    ```python
    obj = models.User.objects.get(uid=1)
    obj.name = 'wusir'
    obj.save()
    ```



### 12.5 模板

#### 12.5.1 view函数向模板发送数据

view函数：

```python
from django.shortcuts import render
from app01 import models

def user_list(request):
    obj_list = models.User.objects.all()
    return render(request,'user_list.html',{'obj_list':obj_list})#模板根据键取数据
```

#### 12.5.2 模板语法

##### 12.5.2.1 变量

{{    }}  内部为后台的变量

取变量内部成员用.来取

```python
#取列表的第一个元素
{{lst.0}}
#取字典键为name的值
{{dic.name}}
#取person的name属性
{{person.name}}
#取字典的keys方法
{{dic.keys}}
```

优先级：

- 在字典中查询
- 属性或者方法
- 数字索引

1. 过滤器filter

   语法：{{ 变量|filter_name:参数 }} 

   **注：冒号：两边不能有空格**

   - django自带过滤器

     1. - default  当变量不存在或者为空时显示的默认值

          ```
          {{value|default:'nothing'}}
          ```

          也可以在settings.py的TEMPLATES下的OPTIONS中添加如下配置，若项目中的模板变量不存在，则显示配置的值

          ```python
          TEMPLATES = [
              {
                  'BACKEND': 'django.template.backends.django.DjangoTemplates',
                  'DIRS': [os.path.join(BASE_DIR, 'templates')]
                  ,
                  'APP_DIRS': True,
                  'OPTIONS': {
                      'context_processors': [
                          'django.template.context_processors.debug',
                          'django.template.context_processors.request',
                          'django.contrib.auth.context_processors.auth',
                          'django.contrib.messages.context_processors.messages',
                      ],
                      'string_if_invalid':'找不到', #添加该项
                  },
              },
          ]
          ```

        - filesizeformat  转换成问见大小格式，最大单位PB

          ```python
          # size = 1*1024*1024*1024
          {{size|filesizeformat}}  #1.0GB
          ```

        - add  相当于+，可以用于数字之间、数字和字符串、字符串之间、列表之间等

          ```python
          #num = 12 
          {{ num|add:12 }} #24
          {{ num|add:'12' }} #24
          {{ '12'|add:'12' }} #24
          {{ '12'|add:'abc' }} #24
          
          #lst = [1,2,3,4]
          {{ lst|add:lst}} #[1,2,3,4,1,2,3,4]
          ```

        - lower 转小写

          ```python
          # s = ABC
          {{s|lower}} #abc
          ```

        - upper 转大写

          ```python
          # s = abc
          {{s|upper}} #ABC
          ```

        - title 首字母大写

          ```python
          # s = abc
          {{ s|title }} #Abc
          ```

        - length 返回变量的长度

          ```python
          # lst = [1,2,3,4]
          {{lst|length}} #4
          ```

        - slice 切片

          ```python
          # lst = [1,2,3,4,5]
          {{lst|slice:'0:2'}} #[1,2]
          {{lst|slice:'0:'}} #[1,2,3,4,5]
          {{lst|slice:'-2:0:-1'}} #[4,3,2]
          ```

        - join 拼接

          ```python
          #lst = [1,2,3,4,5]
          {{lst|join:','}} #1,2,3,4,5
          ```

        - date  格式化时间

          ```python
          {{now|date:'Y-m-d H:i:s'}} #2019-06-18 16:10:14 
          ```

          还可以在settings.py中做如下配置，使全局的日期格式化

          ```python
          USE_L10N = False
          2019-06-18 16:10:14 
          ```

        - safe 取消django对html,js代码的自动转义

          ```python
          # value = "<a href='#'>点我</a>"
          {{value|safe}}
          ```

   - 自定义过滤器

     - 在app下创建一个名为templatetags的python包

     - 在该包内创建py文件，文件名自定义(my_tag.py)

     - 在py文件中写：

       ```python
       from django import template
       register = template.Library() #register名不能更改
       ```

     - 写函数+装饰器

       ```python
       @register.filter
       def add_xx(value, arg):  # 最多有两个,第二个参数可以省略
           return '{}-{}'.format(value, arg)
       
       #可以通过装饰器为自定义模板取名字
       @register.filter(name='add1') #使用时通过该名字使用
       def add_xx(value, arg):  # 最多有两个,第二个参数可以省略
           return '{}-{}'.format(value, arg)
       ```

     - 使用

       ```python
       {% load my_tag %}
       {{'alex'|add_xx:'dasb'}} #alex-dasb
       ```

##### 12.5.2.2 标签

{% %}  写一些逻辑相关的代码  

1. for循环

   - forloop对象

     - forloop.counter 当前循环的索引,从1开始
     - forloop.counter0  当前循环的索引，从0开始
     - forloop.revcounter 当前循环的倒叙索引值，到1结束
     - forloop.revcounter0 当前循环的倒叙索引值，到0结束
     - forloop.first   是否为第一次循环
     - forloop.last    是否为最后一次循环
     - forloop.parentloop 父循环的forloop对象

   - for  empty   当循环为空时，执行empty

     ```python
     {% for i in book_list%}
     	{{i}}
     {% empty%}
     	没有相关数据
     {% endfor%}
     ```

2. 条件判断

   - 模板中不支持连续判断

       ```python
       #python中：5>4>1 等价于 5>4 and 4>1   结果：True
       #js中： 5>4>1 等价于 5>4 true  等价于 1>1  结果：false
       ```

   - 模板中不支持算数运算（可以使用过滤器代替）

     ```python
     {% if num|add:2 > 4 %}
     	xxxxxxx
     {% endif %}
     
     {% if num|divisibleby:2 %}
     	可以被2整除               
     {% else %}
         不能被2整除            
     {% endif %}
     ```

3. with  起别名

   ```html
   {% with 变量 as new %}
   	{{ new }}
   {% endwith %}
   ```

4. csrf_token

   csrf 跨站请求伪造。加上{% csrf_token %}后，django会在页面自动加上一个隐藏的input,然后提交post请求时会通过该input的value值来判断是不是本站的post请求。

   ```html
   <!--模板代码代码-->
   <form action="">
       {% csrf_token %}
       <label for="">用户名</label>
       <input type="text" name="name" id="">
       <input type="submit" name="name" id="">
   </form>
   
   <!--浏览器html代码-->
   <form action="">
       <input type="hidden" name="csrfmiddlewaretoken" value="urZC99wxQ26eF3u9hwIyRLbZYZqH6ukLqeWSvk8ixIiKuo6YCDw1wgRJnJDq0dCQ">
       <label for="">用户名</label>
       <input type="text" name="name" id="">
       <input type="submit" name="name" id="">
   </form>
   ```

5. 母版页

   将重复用到的内容写成一个母版页，独有的内容定义成block块。子页面在编写时只需要继承母版页，然后将自己的内容包裹在对应的block块中即可。

   母版页 base.html：

   ```html
   <!DOCTYPE html>
   <html lang="en">
   <head>
       <meta charset="UTF-8">
       <title>Title</title>
       <link rel="stylesheet" href="/static/css/dsb.css">
       {% block css %}
   
       {% endblock %}
   </head>
   <div class="container-fluid">
       <div class="row">
           <div class="col-sm-3 col-md-2 sidebar">
               <ul class="nav nav-sidebar">
                   <li class="{% block pub_active %}active{% endblock %}"><a 									href="/publisher_list/">出版社列表 <span
                           class="sr-only">(current)</span></a></li>
                   <li class="{% block book_active %}{% endblock %}"><a href="/book_list/">						书籍列表 <span class="sr-only">(current)</span></a>
                   </li>
               </ul>
           </div>
           <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
               {% block content %}
   
               {% endblock %}
           </div>
       </div>
   </div>
   </body>
   {% block js %}
   
   {% endblock %}
   </html>
   ```

   子版页

   ```html
   {% extends 'base.html' %} <!-- 继承母版页 -->
   {% block content %}  <!-- 会替代母版页中对应的块 -->
       <div class="panel panel-primary">
           <div class="panel-heading">
               <h3 class="panel-title">书籍列表</h3>
           </div>
           <div class="panel-body">
               <a class="btn btn-success btn-sm" href="/add_book/">添加</a>
               <table class="table table-striped table-hover">
                   <thead>
                   <tr>
                       <th>序号</th>
                       <th>ID</th>
                       <th>书名</th>
                       <th>出版社名称</th>
                       <th>操作</th>
                   </tr>
                   </thead>
                   <tbody>
                   {% for book in all_books %}
                       <tr>
                           <td>{{ forloop.counter }}</td>
                           <td>{{ book.pk }}</td>
                           <td>{{ book.title }}</td>
                           <td>{{ book.pub.name }}</td>
                           <td>
                               <a href="/del_book/?id={{ book.pk }}">删除</a>
                               <a href="/edit_book/?id={{ book.pk }}">编辑</a>
                           </td>
                       </tr>
                   {% empty %}
                       <td colspan="5" style="text-align: center">没有相关的数据</td>
                   {% endfor %}
                   </tbody>
               </table>
           </div>
       </div>
   {% endblock %}
   ```

   **注意：**

   - {% extends 'base.html' %} 要写在第一行前面不能有内容，有内容会显示
   - {% extends 'base.html' %}  的base.html要加上引号，不然会当做变量来处理。利用这一点可以根据从views返回的变量值，动态改变母版页
   - 把要显示的内容写在block块中
   - 可以定义js和css的块，使不同页面只引入自己的js和css

6. 组件

   把常用的，不改变的部分html代码片段，单独封装成一个组件（一个html文件），如导航栏。

   使用方法：

   ```python
   ...
   {% include ‘nav.html ’  %}
   ...
   ```

7. 静态文件

   当settings.py文件中的STATIC_URL改变后，原来的静态文件写法就需要把所有的地方都改一遍。通过下列两种方法，STATIC_URL改变后，引用也会动态改变。

   ```html
   {% load static %}
   
    <link rel="stylesheet" href="{% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}">
    <link rel="stylesheet" href="{% static '/css/dsb.css' %}">
    <link rel="stylesheet" href="{% get_static_prefix %}css/dsb.css">
   
   {% static '/plugins/bootstrap-3.3.7/css/bootstrap.css' %}  
   {% get_static_prefix %}   ——》 获取别名
   ```

8. simple_tag

   类似于过滤器，但simpletag是标签，可以传任意个参数。

   my_tags.py :

   ```python
   @register.simple_tag
   def join_str(*args, **kwargs):
       return '{} - {} '.format('*'.join(args), '$'.join(kwargs.values()))
   ```

   使用：

   ```html
   {% load my_tags %}
   {% join_str '1' '2' k1='3' k2='4' %}
   ```

9. inclusion_tag

   有点类似于组件，也是返回一段html代码段，但是inclusion_tag可以动态的改变代码段。

   流程如下：

   ![](E:\Python\11Django\day58\inclusion_tag.png)

   

### 12.6 CBV和FBV

#### 12.6.1 FBV

FBV：function based view   基于函数实现view逻辑代码

定义：

```python
from django.shortcuts import render,redirect
from appPublisher import models

def add_book(request):
    error = ''
    publisher_list = models.Publisher.objects.all().order_by('-pid')
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name,pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name,pub_id=publisher)
            return redirect('/book_list/')
    return render(request, 'book/add_book.html', {'error':error,'publisher_list':publisher_list})
```

使用：

```python
from django.conf.urls import url
from appPublisher import views

urlpatterns = [
    url(r'^add_book/', views.add_book),
]
```

#### 12.6.2 CBV

CBV：class based view  基于类实现的view代码

定义：

```python
from django.shortcuts import render,redirect
from appPublisher import models
from django.views import View

class AddBook(View):

    def get(self,request): #get请求逻辑代码
        error = ''
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        return render(request, 'book/add_book.html', {'error': error, 'publisher_list': publisher_list})

    def post(self,request):  #post请求逻辑代码
        error = ''
        publisher_list = models.Publisher.objects.all().order_by('-pid')
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name, pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name, pub_id=publisher)
            return redirect('/book_list/')
        return render(request, 'book/add_book.html', {'error': error, 'publisher_list': publisher_list})
```

使用：

```python
from django.conf.urls import url
from appPublisher import views

urlpatterns = [
    url(r'^add_book/', views.AddBook.as_view()),
]
```

#### 12.6.3 as_view的流程

1.项目启动 加载ur.py时，执行类.as_view()    ——》  view函数

2.请求到来的时候执行view函数：

1. 实例化类  ——》 self 

   ​	self.request = request 

   2. 执行self.dispatch(request, *args, **kwargs)

      1. 判断请求方式是否被允许：
      2. 允许    通过反射获取到对应请求方式的方法   ——》 handler
      3. 不允许   self.http_method_not_allowed  ——》handler
      4. 执行handler（request，*args,**kwargs）

       返回响应   —— 》 浏览器

#### 12.6.4 视图加装饰器

##### 12.6.4.1 FBV

直接加装饰器即可

```python
#定义装饰器
def timer(func):
    def inner(*args,**kwargs):
        start = time.time()
        ret = func(*args,**kwargs)
        print(time.time()-start)
        return ret
    return inner

@timer
def add_book(request):
    error = ''
    publisher_list = models.Publisher.objects.all().order_by('-pid')
    if request.method == 'POST':
        book_name = request.POST.get('book_name')
        publisher = request.POST.get('publisher')

        if models.Book.objects.filter(name=book_name,pub_id=publisher):
            error = '图书信息已存在'
        if not book_name:
            error = '图书名称不能为空'
        if not error:
            models.Book.objects.create(name=book_name,pub_id=publisher)
            return redirect('/book_list/')
    return render(request, 'book/add_book.html', {'error':error,'publisher_list':publisher_list})
```

##### 12.6.4.2 CBV

首先导入一个模块

```python
from django.utils.decorators import method_decorator
```

方式一：加在方法上，可以为特定的请求方法加装饰器

```python
@method_decorator(timer)
def get(self, request, *args, **kwargs):
    """处理get请求"""
```

方式二：加在自己重写的dispatch方法上，相当于给所有的请求方法都加装饰器

```python
@method_decorator(timer)
def dispatch(self, request, *args, **kwargs):
    ret = super().dispatch(request, *args, **kwargs)
    return ret
```

方式三：加在类上

```python
@method_decorator(timer,name='post')
@method_decorator(timer,name='get')
class AddPublisher(View):
    
 
@method_decorator(timer,name='dispatch')
class AddPublisher(View):
```

注意：方法也可以直接加装饰器。

直接加装饰器和使用method_decorator的区别：

- 直接使用装饰器

  ```python
  '''
  func   ——》 <function AddPublisher.get at 0x000001FC8C358598>
  
  args  ——》 (<app01.views.AddPublisher object at 0x000001FC8C432C50>, <WSGIRequest: GET '/add_publisher/'>)
  '''
  ```

- 使用method_decorator

  ```python
  '''
  func ——》 <function method_decorator.<locals>._dec.<locals>._wrapper.<locals>.bound_func at 0x0000015185F7C0D0>
  
  args ——》 (<WSGIRequest: GET '/add_publisher/'>,)
  '''
  ```

### 12.7 request和response对象

#### 12.7.1 request对象

1. 属性

   - request.method   请求方式

   - request.GET    url上携带的参数

   - request.POST   post请求提交的数据

   - request.path_info   URL路径  不包括ip和端口(域名)  不包含参数

   - request.body   请求体    b''

   - request.FILES    上传的文件

     html代码

     ```python
     <!DOCTYPE html>
     <html lang="en">
     <head>
         <meta charset="UTF-8">
         <meta http-equiv="x-ua-compatible" content="IE=edge">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <title>Title</title>
     </head>
     <body>
     
     <form action="" method="post" enctype="multipart/form-data"> <!--上传文件需要改为multipart/form-data-->
         {% csrf_token %}
         <input type="file"  name="f1">
         <button>上传</button>
     </form>
     ```

     后台代码：

     ```python
     def upload(request):
         if request.method == 'POST':
             # 获取文件
             f1 = request.FILES.get('f1')
             # 保存文件
             with open(f1.name, 'wb') as f:
                 for i in f1.chunks():
                     f.write(i)
         return render(request, 'upload.html')
     ```

   - request.META    请求头信息

   - request.cookie    cookie

   - request.session   session

2. 方法

   - request.get_full_path()   URL的路径   不包含ip和端口   包含参数
   - request.is_ajax()    判断是否为ajax请求

#### 12.7.2 response对象

- HttpResponse  

  ```python
  HttpResponse('字符串')
  ```

- render  返回完整页面

  ```python
  render(request,'模板的文件名',{k1:v1})
  ```

- redirect  重定向

  ```python
  redirect('重定向地址')  #请求头Localtion:地址
  ```

- JsonResponse   返回一个json对象

  ```python
  from django.http.response import JsonResponse
  
  #默认只能返回最外层为字段形式的json
  JsonResponse({})  #自动序列化为json串
  #若想响应字典对象，需要把safe参数设为False
  JsonResponse([],safe=False)
  ```

### 12.8 路由

#### 12.8.1 urlconf配置参数

1. 基本格式

   ```python
   '''
   from django.conf.urls import url
   urlpatterns = [
       url(正则表达式,views视图，参数，别名)
   ]
   '''
   urlpatterns = [
       url(r'^articles/2003/$', views.special_case_2003),
       url(r'^articles/([0-9]{4})/$', views.year_archive),
       url(r'^articles/([0-9]{4})/([0-9]{2})/$', views.month_archive),
       url(r'^articles/([0-9]{4})/([0-9]{2})/([0-9]+)/$', views.article_detail),
   ]
   ```

   参数说明:

   - 正则表达式：一个正则表达式字符串，用来匹配地址
   - views视图：一个可调用对象，通常为一个视图函数
   - 参数：可选的要传递给视图函数的默认参数（字典形式）
   - 别名：一个可选的name参数

    正则表达式注意事项：

   - urlpatterns中的元素按照书写的顺序从上往下逐一匹配正则表达式，一但匹配成功则不再继续。
   - 若要从url中捕获一个值只需要在它周围放置一对圆括号（分组匹配）
   - 不需要添加一个前导的反斜杠，因为每个url都有。如：'^demo',而不是'^/demo'
   - 每个正则表达式前面的'r'是可选的，但建议加上。取消转义

2. APPEND_SLASH配置

   ```python
   # 是否开启URL访问地址后面不为/跳转至带有/的路径的配置项
   APPEND_SLASH=True
   ```

   若该配置项为true，我们在地址栏输入末尾不带'/'的url地址，则django会重定向末尾带'/'的地址。

   默认没有 APPEND_SLASH 这个参数，但 Django 默认这个参数为 APPEND_SLASH = True。 其作用就是自动在网址结尾加'/'。

#### 12.8.2 url命名分组

##### 12.8.2.1 分组

分组会将括号内匹配到的 内容按照位置传参的方式，传入视图函数中

```python
#url代码
urlpatterns = [
    url(r'^demo/(/d+)/$', views.demo),
]

#视图函数代码
def demo(request,pk):
    pass
```

##### 12.8.2.2 命名分组

命名分组会将分组匹配到的内容，以命名为关键字按关键字传参的方式传到后台函数中

```python
#url代码
urlpatterns = [
    url(r'^demo/(?P<pk>/d+)/$', views.demo),
]

#视图函数代码  针对于url'/demo/12' 会按以下方式调用视图函数
# demo(pk='12')
def demo(request,pk):
    pass
```

**注意：urlconf中捕获的参数都是字符串格式。**

##### 12.8.2.3 为视图函数参数指定默认值

如下：两个url指向同一个视图函数。

```python
#urls.py
urlpatterns = [
    url(r'^demo/$', views.demo),
    url(r'^demo/(?P<pk>/d+)/$', views.demo),
]

#视图函数
def demo(request,pk=12):
    pass
```

##### 12.8.2.4 include其他urlconfs

- 在业务越来越复杂的情况下，项目下的urls.py也会越来越复杂。因此我们可以将不同业务的路由放到自己对应的app中。
- 在多人协作开发项目的过程中，为了避免多人操作urls.py文件的冲突问题。所以需要需要路由分发。

在对应的业务功能app中创建自己的urls.py文件，内部写上自己相关的路由配置。然后在项目下的urls.py中引用即可。

```python
#app01下的urls.py
from django.conf.urls import url
urlpatterns = [
    url(r'^demo/$', views.demo),
    url(r'^demo1/(?P<pk>/d+)/$', views.demo1),
]

#项目下的urls.py
from django.conf.urls import url,include
urlpatterns = [
    url(r'^app01/$',include('app01.urls')), #包含其他文件的urlconfs文件
]
```

#### 12.8.3 传递额外参数

`django.conf.urls.url()` 可以接收一个可选的第三个参数，它是一个字典，表示想要传递给视图函数的额外关键字参数。

```python
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^blog/(?P<year>[0-9]{4})/$', views.year_archive, {'foo': 'bar'}),
]
#对于/blog/2005/请求，将会这样调用视图函数：year_archive(year='2005',foo='bar')
```

**注：若命名分组和额外参数键相同，传入视图中的是额外参数。**

#### 12.8.4 命名url和url反向解析

为了提高项目的可扩展和易维护性。我们应该避免使用固定写死的url来发送请求。

Django 提供一个办法是让URL 映射是URL 设计唯一的地方。你填充你的URLconf，然后可以双向使用它：

- 根据用户/浏览器发起的URL 请求，它调用正确的Django 视图，并从URL 中提取它的参数需要的值。
- 根据Django 视图的标识和将要传递给它的参数的值，获取与之关联的URL。

通俗来讲，就是为urlconf起一个名称，当我们在视图函数中要重定向一个路由或者在html的a标签或表单提交请求的时，通过这个别名来设置路由地址。

```python
#urls.py
from django.conf.urls import url

urlpatterns = [
    url(r'^home/',views.func,name='home'),
    url(r'^demo/(\d+)/',views.func,name='demo'), #分组
    url(r'^demo/(?P<pk>\d+)/',views.func,name='index'), #命名分组
]

```

```html
<!--html文件-->
<a href="{% url 'home' %}">home</a> 
<a href="{% url 'demo' 12 %}">demo</a> <!--分组-->
<a href="{% url 'index' pk=12 %}">index</a> <!--命名分组-->
```

```python
#视图函数
#可以通过下列两种方式导入reverse
from django.shortcuts import reverse,redirect
from django.urls import reverse

def func(request):
    redirect(reverse('home'))
    redirect(reverse('demo',args=(12,))) #分组
    redirect(reverse('index',kwargs={'pk':12}))#命名分组
```

#### 12.8.5 命名空间模式

如果两个app下的urlconf的别名相同，可以通过命名空间来区分调用。

项目下的urls.py文件

```python
from django.conf.urls import url
urlpatterns = [
    url(r'^app01/',include('app01.urls')),
    url(r'^app02/',include('app02.urls')),
]
```

app01下的urls.py文件

```python
from django.conf.urls import url
urlpatterns = [
    url(r'book/(?P<pk>\d+)$',views.book,name='demo')
]
```

app02下的urls.py文件

```python
from django.conf.urls import url
urlpatterns = [
    url(r'author/(?P<pk>\d+)$',views.book,name='demo')
]
```

当调用时便无法确定调用哪一个路由了

```python
reverse('demo',kwargs={'pk':12})
```

```html
<a href="{% url 'demo' pk=12 %}">demo</a>
```

所以可以使用命名空间namespase解决该问题。修改项目下的urls.py文件如下：

```python
from django.conf.urls import url
urlpatterns = [
    url(r'^app01/',include('app01.urls',namespase = 'app01')),
    url(r'^app02/',include('app02.urls',namespase = 'app02')),
]
```

然后用以下方式调用：

```python
reverse('app01:demo',kwargs={'pk':12})
```

```html
<a href="{% url 'app02:demo' pk=12 %}">demo</a>
```

### 12.9 Django的ORM操作

#### 12.9.1 ORM字段和参数

##### 12.9.1.1 字段

- AutoField，自增字段，设置primary_key=True后代表主键字段。一张表里只能有一个AutoField字段。

  ```python
  id = models.AutoField(primary_key = True)
  ```

- IntegerField，整型

  ```
  age = models.IntegerField()
  ```

- DateField  日期类型    DateTimeField 日期时间类型

  ```python
  birth = models.DateField(auto_now_add = True)
  #auto_now_add  为True，新增数据时自动添加当前日期（UTC时间）
  #auto_now   为True，新增或修改数据时都会更新该字段
  ```

- DecimalField  十进制小数

  ```python
  price = models.DecimalField(max_digits=5,decimal_places=2)
  #max_digits  小数总长度
  #decimal_places  小数部分长度
  ```

- BooleanField  布尔值字段，只有0和1两个值

- TextField  文本字段

- 自定义字段

- 在models.py文件中添加类：

  ```python
  #定义一个Char类型字段
  class MyCharField(models.Field):
      """
      自定义的char类型的字段类
      """
      def __init__(self, max_length, *args, **kwargs):
          self.max_length = max_length
          super(MyCharField, self).__init__(max_length=max_length, *args, **kwargs)
   
      def db_type(self, connection):
          """
          限定生成数据库表的字段类型为char，长度为max_length指定的值
          """
          return 'char(%s)' % self.max_length
      
  #创建表，使用自定义字段   
  class Person(models.Model):
      name = MyCharField(max_length = 10)
  ```

##### 12.9.1.2 参数

- null 表示一个字段是否可以为空，默认为False
- db_column  数据库中字段的名称
- unique  是否唯一
- db_index  是否创建索引
- defalut  默认值
- verbose_name      Admin中显示的字段名称
- blank          Admin中是否允许用户输入为空
- editable            Admin中是否可以编辑
- help_text           Admin中该字段的提示信息
- choices             Admin中显示选择框的内容，用不变动的数据放在内存中从而避免跨表操作

#### 12.9.2 表的参数

```python
class Person(models.Model):
    name = models.CharField(max_length=12)
    age = models.IntegerField()
    
    #配置表信息
    class Meta:
        db_table = '表名'  #表名
        index_together = [('name','age')] #联合索引
        unique_together = [('name','age')] #联合唯一索引
        
```

#### 12.9.3 数据操作

则pyCharm中通过脚本文件操作数据

```python
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings") #可以按照manage.py中写

import django
django.setup()

from appPublisher import models
#数据操作
```

- 

- all()  获取所有数据

- get()  获取满足条件的一条数据，没有或多条数据会报错

- filter()  获取满足条件的所有数据

- exclude()  获取不满足条件的所有数据

- values()  拿到对象的所有字段和字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values()
  print(obj)
  #<QuerySet [{'pid': 2, 'name': '北京理工出版社', 'phone': '18836325896', 'addr': '北京市房山区良乡'}, {'pid': 5, 'name': '清华大学出版社', 'phone': '13399900888', 'addr': '北京市海淀区'}, {'pid': 7, 'name': '路飞出版社', 'phone': '13589456983', 'addr': 'ad请问'}]>
  obj_list = models.Publisher.objects.filter(name='出版社').values()
  ```

- values('字段名称',...)  拿到对象指定的字段和字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values('name')
  print(obj)
  #<QuerySet [{'name': '北京理工出版社'}, {'name': '清华大学出版社'}, {'name': '路飞出版社'}]>
  ```

- values_list()  拿到对象的所有字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values_list()
  print(obj)
  #<QuerySet [(2, '北京理工出版社', '18836325896', '北京市房山区良乡'), (5, '清华大学出版社', '13399900888', '北京市海淀区'), (7, '路飞出版社', '13589456983', 'ad请问')]>
  ```

- values_list('字段名称',...)   拿到对象的指定字段值

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj = models.Publisher.objects.values_list('name','phone')
  print(obj)
  #<QuerySet [('北京理工出版社', '18836325896'), ('清华大学出版社', '13399900888'), ('路飞出版社', '13589456983')]>
  ```

- order_by()  排序，默认升序。降序要在字段前面加负号。也可以按照多个字段排序

  ```python
  import os
  os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bookmanage.settings")
  
  import django
  django.setup()
  
  from appPublisher import models
  
  obj_list = models.Publisher.objects.all().order_by('-pk')
  obj_list = models.Publisher.objects.all().order_by('name','-pk')
  ```

- reverse()  反向排序，必须对已排序的QuerySet使用

  ```python
  ...
  obj_list = models.Publisher.objects.values().order_by('-pk').reverse()
  ```

- distinct()  去重，必须完全相同的内容才能去重

  ```python
  ...
  obj_list = models.Publisher.objects.values('name').distinct
  ```

- count()  计算结果集元素个数

  ```python
  num = models.Publisher.objects.all().count()
  ```

- first() 取查询结果集的第一个元素，没有则返回None

  ```python
  ...
  obj = models.Publisher.objects.values('name').first()
  ```

- last()  取查询结果集的最后一个元素，没有则返回None

- exists()  判断查询的数据是否存在

  ```python
  ...
  obj = models.Publisher.objects.filter(pk=1).exists()#True
  ```

#### 12.9.4 单表的双下划线

- gt 大于

  ```python
  ret = models.Person.objects.filter(pk__gt=1)
  ```

- lt 小于

  ```python
  ret = models.Person.objects.filter(pk__lt=1)
  ```

- gte 大于等于

- lte  小于等于

- range = [m,n]  范围 m=< 值<=n

  ```python
  ret = models.Person.objects.filter(pk__range=[2,5])
  ```

- in = [1,2,3]   在...中，成员判断

  ```python
  ret = models.Person.objects.filter(pk__in=[1,2,3])
  ```

- contains  包含，相当于like 区分大小写

  ```python
  ret = models.Person.objects.filter(name__contains='a')
  ```

- icontains  包含，不区分大小写

  ```python
  ret = models.Person.objects.filter(name__icontains='a')
  ```

- startswith   以...开头 区分大小写

  ```python
  ret = models.Person.objects.filter(name__startswith='a')
  ```

- istartswith   以...开头 不区分大小写

  ```python
  ret = models.Person.objects.filter(name__istartswith='a')
  ```

- endswith   以...结尾 区分大小写

- iendswith   以...结尾 不区分大小写

- year  筛选年

  ```python
  ret = models.Person.objects.filter(birth__year='2019')
  ```

- isnull   是否为空

  ```python
  ret = models.Person.objects.filter(name__isnull=True)
  ```

#### 12.9.5 外键操作（多对一）

##### 12.9.5.1 基于对象的查询

表关系(多对一)：

```python
#出版社（一）
class Publisher(models.Model):
    pid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32,unique=True)
    phone = models.CharField(max_length=11,unique=True)
    addr = models.CharField(max_length=50)

#书籍（多）
class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE)
```

- 正向查询

  ```python
  book_obj = models.Book.object.get(pk=1)
  book_obj.pub.pid
  ```

- 反向查询

  ```python
  pub_obj = models.Publisher.objects.get(pk=1)
  pub_obj.book_set.all()  #book_set多对一关系管理对象，可以在建立外键字段时设置related_name属性来设置管理对象名称
  ```

```python
class Book(models.Model):
    bid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=32)
    pub = models.ForeignKey(Publisher,on_delete=models.CASCADE,related_name='books')

#使用
pub_obj = models.Publisher.objects.get(pk=1)
pub_obj.books.all()
```

##### 12.9.5.2 基于字段查询

```python
#  查询老男孩出版的书
ret = models.Book.objects.filter(pub__name='老男孩出版社') 


# 查询出版菊花怪大战MJJ的出版社
# related_name='books' 指定了管理对象名
ret= models.Publisher.objects.filter(books__title='菊花怪大战MJJ')

#没有指定related_name   类名的小写 在对象查询和字段查询时使用
ret= models.Publisher.objects.filter(book__title='菊花怪大战MJJ')

#指定related_query_name='xxx' 在字段查询时使用优先级比related_name高
ret= models.Publisher.objects.filter(xxx__title='菊花怪大战MJJ')
```

#### 12.9.6 多对多查询

表关系

```python
class Book(models.Model):
    title = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=6, decimal_places=2)  # 9999.99
    sale = models.IntegerField()
    kucun = models.IntegerField()
    pub = models.ForeignKey(Publisher, null=True,
                            on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Author(models.Model):
    name = models.CharField(max_length=32, )
    books = models.ManyToManyField(Book)

    def __str__(self):
        return self.name
```

##### 12.9.6.1 基于对象查询

```python
#从author表查关联的book,多对多字段写在author类中，正向查询
author_obj = models.Author.objects.get(pk=1)
book_list = author_obj.books.all() #获取与该作者关联的所有书籍对象 books关系管理对象

#从book表查关联的author,多对多字段写在author类中，反向查询
#未设置related_name
book_obj = models.Book.objects.get(pk=1)
author_list = book_obj.author_set.all() #获取与该书籍关联的所有作者对象 author_set关系管理对象
#设置related_name = authors
book_obj = models.Book.objects.get(pk=1)
author_list = book_obj.authors.all() #获取与该书籍关联的所有作者对象 authors关系管理对象
```

##### 12.9.6.2 基于字段查询

```python
#查与书籍相关的作者
author_list = models.Author.objects.filter(books__title = '菊花怪大战MJJ')

#查询作者相关的书籍
#未设置related_name和related_query_name
book_list = models.Book.objects.filter(author__pk = 1)
#设置related_name='authors',未设置related_query_name
book_list = models.Book.objects.filter(authors__pk = 1)
#同时设置了related_name='authors'和related_query_name='xxx'(字段查询时使用的值)
book_list = models.Book.objects.filter(xxx__pk = 1)
```

##### 12.9.6.3 关系管理对象的方法

- all()  获取关联的所有对象

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.all()
  ```

- set() 设置多对多关系，会覆盖之前的数据

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.set([1,2,3]) #可以设置关联的主键id
  author_obj.books.set(models.Book.objects.filter(pk__in=[1,2,3])) #也可以设置对象集合
  ```

- add() 添加多对多数据，不会覆盖之前的数据

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.add(1,2) #不接受列表参数
  author_obj.books.add(*models.Book.objects.filter(pk__in=[1,2]))#列表前加*把列表拆开打散
  ```

- remove() 删除多对多关系 

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.remove(1,2) #不接受列表参数
  author_obj.books.remove(*models.Book.objects.filter(pk__in=[1,2]))#列表前加*把列表拆开打散
  ```

- clear()   清空多对多关系

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.clear()
  ```

- create() 创建对象并关联

  ```python
  author_obj = models.Author.objects.get(pk=1)
  author_obj.books.create(title='跟MJJ学前端',pub_id=1) #创建书籍对象（增加一条书籍数据）并关联作者
  ```

##### 12.9.6.4 多对一关系的方法

多对一关系中，关系管理对象的方法和多对多关系管理对象的方法有些差别

- 只能使用对象，不能使用id
- 要使用remove和clear方法，必须把外键设置为可为null

```python
pub_obj = models.Publisher.objects.get(pk=1)
#set
pub_obj.book_set.set(*models.Book.objects.filter(pk__in=[2,3]))#只能是对象
#add
pub_obj.book_set.add(*models.Book.objects.filter(pk__in=[2,3]))#只能是对象
#remove pub = models.ForeignKey(Publisher, null=True,on_delete=models.CASCADE)
pub_obj.book_set.remove(*models.Book.objects.filter(pk__in=[2,3]))#只能是对象
pub_obj.book_set.clear()
```

#### 12.9.7 分组聚合

##### 12.9.7.1 聚合

```python
from django.db.models import Max, Min, Avg, Sum, Count
ret = models.Book.objects.filter(pk__gt=3).aggregate(Max('price'),avg=Avg('price')) #聚合方法返回一个列表，是一个终止分句，后面不能继续使用ORM操作方法
```

##### 12.9.7.2 分组聚合

```python
from django.db.models import Max, Min, Avg, Sum, Count
# 统计每一本书的作者个数
ret = models.Book.objects.annotate(count = Count('author')) #根据书籍分组，通过作者聚合
# 统计出每个出版社的最便宜的书的价格
#方式一
ret = models.Publisher.objects.annotate(min=Min(book__price)).values()
#方式二
ret = models.Book.objects.values('pub_id').annotate(min=Min('price')).values('pub_id','min')#按照出版社id分组，后边的values只能取前面出现的字段
#统计不止一个作者的图书
ret = models.Book.objects.annotate(count=Count('author')).filter(count__gt=1)
# 查询各个作者出的书的总价格
ret = models.Author.objects.annotate(sum = Sum('books__price')).values()
```

#### 12.9.8 F和Q查询

##### 12.9.8.1 F查询

```python
from django.db.models import F
# 比较两个字段的值  查找销售量大于库存的书籍
ret = models.Book.objects.filter(sale__gt=F('kucun'))
```

更新数据的两种方式：

```python
#方式一
obj = models.Book.objects.get(pk=1)
obj.title = '123'
obj.save()
#方式二
models.Book.objects.filter(pk__in=[1,2,3]).update(title='123') #将所有满足条件的都更新
```

```python
from django.db.models import F
# 比较两个字段的值  将销量翻倍
ret = models.Book.objects.all().update(sale=F('sale')*2)
```

##### 12.9.8.2 Q查询

```python
from django.db.models import Q
#获取id大于3或小于2，且价格大于50的书籍
ret = models.Book.objects.filter(Q(Q(pk__gt=3) | Q(pk__lt=2)) & Q(price__gt=50))
# & 与  |或  ~非
```

Q的另一种使用方式：

```python
q = Q() #创建一个Q对象
q.connector = 'OR' #设置Q对象的子对象之间的关系为或
q.children.append(Q(qq__contains=query)) #添加子Q对象

Q(('qq__contains',query))#通过字符串的方式创建Q对象 
```

##### 12.9.9 事务

```python
from django.db import transaction
try: #异常捕捉要放到事务外边
    with transaction.atomic():
        #进行orm操作，若中间报错，则自动回滚
        models.Publisher.object.create(name='123')
        models.Publisher.object.create(name='123')
except Exception as e:
    print(e)
```

### 12.10 cookie

#### 12.10.1 定义

保存在浏览器本地的一组组键值对

#### 12.10.2 特点

- 是服务器让浏览器进行设置的
- 保存在浏览器本地
- 下次访问自动携带

#### 12.10.3 应用

- 登录验证
- 保存浏览习惯
- 简单的投票

**注：默认在浏览器关闭后cookie就失效了。**

#### 12.10.4 django使用cookie

1. 获取cookie

   ```python
   #方式一
   value = request.COOKIES['key']   #key不存在会报错
   value = request.COOKIES.get('key')  #key不存在返回None
   #方式二
   value = request.get_signed_cookie('key',salt='盐',default='',max_age='20') #获取设置时加密的cookie
   '''
   参数：
   salt:设置cookie时的盐
   default:获取不到返回的默认值
   max_age:过期时间
   '''
   ```

2. 设置cookie

   ```python
   #方式一
   response.set_cookie(key,value,...)
   #方式二
   response.set_signed_cookie(key,value,salt='加密盐',...)
   ```

   set_cookie()参数：

   - key, 键
   - value='', 值
   - max_age=None, 超时时间
   - expires=None, 超时时间(IE requires expires, so set it if hasn't been already.)
   - path='/', Cookie生效的路径，/ 表示根路径，特殊的：根路径的cookie可以被任何url的页面访问
   - domain=None, Cookie生效的域名
   - secure=False, https传输
   - httponly=False 只能http协议传输，无法被JavaScript获取（不是绝对，底层抓包可以获取到也可以被覆盖）

3. 删除cookie

   ```python
   response.delete_cookie("user")  # 删除用户浏览器上之前设置的user的cookie值
   #本质是把cookie的值设置为""，max_age=0,浏览器接收到后就使cookie失效
   ```

### 12.11 session

#### 12.11.1 定义

保存在服务器上的一组组键值对，必须依赖cookie使用。

#### 12.11.2 为什么要有session？

- cookie保存在浏览器本地，不安全
- 大小受限制

#### 12.11.3 session原理

设置session时，会生成一个sessionid并把它放到cookie中返回给浏览器，sessionid对应的数据存放在服务器上，下次请求会携带sessionid，然后找到服务器上对应的session，取出里面的值。

#### 12.11.4 django使用session

```python
#获取session
value = request.session['key']
value = request.session.get('key')

#设置session
request.session['key'] = value
request.session.setdefault('key',value) #存在则不设置

#删除某一个键值对
del request.session['key']
request.session.pop('key')

#获取会话session的key(sessionid)
request.session.session_key

#删除所有数据库中过期的session 
request.session.clear_expired()

#检查session的session_key在数据库中是否存在
request.session.exists("session_key")

#删除数据库中的数据，不会删除cookie
request.session.delete()

#删除数据库和cookie数据，一般在注销时使用
request.session.flush()

#设置过期时间
request.session.set_expiry(value)
'''
 * 如果value是个整数，session会在些秒数后失效。
 * 如果value是个datatime或timedelta，session就会在这个时间后失效。
 * 如果value是0,用户关闭浏览器session就会失效。
 * 如果value是None,session会依赖全局session失效策略。
'''
```

**注：默认两星期过期**

### 12.12 csrf校验

#### 12.12.1 csrf装饰器

- csrf_exempt装饰器      某个视图不需要进行csrf校验

  ```python
  from django.views import View
  from django.utils.decorators import method_decorator
  from django.shortcuts import render,HttpResponse
  from django.views.decorators.csrf import csrf_exempt,csrf_protect,ensure_csrf_cookie
  
  #在CBV中必须要加在CBV的dispatch方法上。
  @method_decorator(csrf_exempt,name='dispatch')
  class Index(View):
      def get(self,request,*args,**kwargs):
          return render(request, 'index.html')
  
      def post(self,request,*args,**kwargs):
          print(request.POST.get('user'))
          return render(request,'index.html')
      
  #加在FBV上
  @csrf_exempt
  def index(request):
      if request.method == 'POST':
          print(request.POST.get('user'))
      return render(request,'index.html')
  ```

- csrf_protect   某个视图需要进行csrf校验。

  ```python
  #csrf_protect  某个视图需要进行csrf校验。若没有在settings中设置'django.middleware.csrf.CsrfViewMiddleware' 全局校验，则可以使用该装饰器为单个视图函数设置校验
  
  #CBV
  class Index(View):
      def get(self,request,*args,**kwargs):
          return render(request, 'index.html')
  
      @method_decorator(csrf_protect)
      def post(self,request,*args,**kwargs):
          print(request.POST.get('user'))
          return render(request,'index.html')
  
  #FBV
  @csrf_protect
  def index(request):
      if request.method == 'POST':
          print(request.POST.get('user'))
      return render(request,'index.html')
  ```

- ensure_csrf_cookie  确保生成csrf的cookie，（不会在模板生成一个隐藏的模板标签（在post的数据中并没有csrfmiddlewaretoken））

  ```python
  #CBV
  class Index(View):
      @method_decorator(ensure_csrf_cookie)
      def get(self,request,*args,**kwargs):
          return render(request, 'index.html')
  
      def post(self,request,*args,**kwargs):
          print(request.POST.get('user'))
          return render(request,'index.html')
      
  #FBV
  @ensure_csrf_cookie
  def index(request):
      if request.method == 'POST':
          print(request.POST.get('user'))
      return render(request,'index.html')
  ```

#### 12.12.2 csrf功能

1.csrf中间件中执行process_request：

​	1. 从cookie中获取到csrftoken的值

```
2. csrftoken的值放入到 request.META
```

2.执行process_view

1. 查询视图函数是否使用csrf_exempt装饰器，使用了就不进行csrf的校验

2. 判断请求方式：

   1. 如果是GET', 'HEAD', 'OPTIONS', 'TRACE'

      不进行csrf校验

      2. 其他的请求方式（post，put）

      进行csrf校验：

      1.获取cookie中csrftoken的值

      获取csrfmiddlewaretoken的值

      ​	能获取到  ——》 request_csrf_token

      ​	获取不到   ——》 获取请求头中X-csrftoken的值 ——》request_csrf_token

      比较上述request_csrf_token和cookie中csrftoken的值，比较成功接收请求，比较不成功拒绝请求。

**注：使请求中含有csrftoken的cookie的两种方法：**

- 在setting.py文件的中间件项中放开'django.middleware.csrf.CsrfViewMiddleware'（全局csrf校验），并且在模板表单中添加{% csrf_token %}
- 为返回页面的视图函数添加ensure_csrf_cookie装饰器。

**注：校验生效的两种情况：**

- 把模板{% csrf_token %}标签生成的csrfmiddlewaretoken的值传到后台和csrftoken(cookie)作比较。比较通过，验证通过。 
- 未向后台传csrfmiddlewaretoken的值，但是在请求头中设置了X-csrftoken的值，csrftoken(cookie)和X-csrftoken的值作比较，比较通过，则通过验证。

### 12.13 Ajax

#### 12.13.1 定义

就是使用js的技术发请求和接收响应的。

#### 12.13.2 特点

- 异步
- 局部刷新
- 传输的数据量少

#### 12.13.3 jqery发ajax请求

```html
<script>
	$.ajax({
        url: '/calc/',
        type: 'post',
        data: {
            a: $("[name='i1']").val(),
            b: $("[name='i2']").val(),
            hobby: JSON.stringify(['唱', '跳', 'rap']),
        },
        success: function (res) {
            //json格式字符串反序列化
            //res = JSON.parse(res)
            //res.xxx
            $("[name='i3']").val(res)
        },
        error:function (error) {
            console.log(error)
        }
	})
</script>
```

#### 12.13.4 上传文件

前端代码：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form action="">
    <input type="file" id="f1">
    <button id="b1" type="button">上传</button>
</form>
<script src="/static/jquery.js"></script>
<script>
    $('#b1').click(function () {
       var formobj = new FormData();
        //formobj.append('file', $('#f1')[0].files[0]); //js对象的files[0]获取文件对象
        formobj.append('file',document.getElementById('f1').files[0])
        formobj.append('name', '123'); //添加一些其他数据
        $.ajax({
            url: '/upload/',
            type: 'post',
            processData: false,
            contentType: false,
            data: formobj,
            success: function (res) {
                console.log(res)
            },
        });
    })
</script>
</body>
</html>
```

后台视图函数代码

```python
def upload(request):
    print(request.FILES)
    name = request.POST.get('name')
    f1 = request.FILES.get('file')
    with open(f1.name,mode='wb') as f:
        for i in f1.chunks():
            f.write(i)
    return HttpResponse('ok')
```

#### 12.13.5 ajax通过csrf的校验

首先确保有cookie csrftoken：

- 添加装饰器ensure_csrf_cookie
- 设置全局csrf校验，添加标签{% csrf_token %}

然后确保有和cookie比较校验的csrfmiddlewaretoken或x-csrftoken：

- 在data中添加csrfmiddlewaretoken

  ```js
  data: {
      'csrfmiddlewaretoken':$('[name="csrfmiddlewaretoken"]').val(),
      a: $("[name='i1']").val(),
      b: $("[name='i2']").val(),
  },
  ```

- 在请求头中添加x-csrftoken

  ```js
  $.ajax({
              url: '/calc2/',
              type: 'post',
              headers: {
                  'x-csrftoken':$('[name="csrfmiddlewaretoken"]').val(),
              },
              data: {
                  a: $("[name='ii1']").val(),
                  b: $("[name='ii2']").val(),
              },
              success: function (res) {
                  $("[name='ii3']").val(res)
              },
          })
  ```

  

### 12.14 django的form组件

#### 12.14.1 form组件的功能

- 生产input标签
- 对提交的数据进行校验
- 提供错误提示

#### 12.14.2 form组件的定义

后台视图代码

```python
from django.shortcuts import render, HttpResponse
from django import forms
from app01 import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

#自定义验证
def checkname(value):
    # 通过校验规则 不做任何操作
    # 不通过校验规则   抛出异常
    if 'alex' in value:
        raise ValidationError('不符合社会主义核心价值观')

class RegForm(forms.Form):
    username = forms.CharField(
        label='用户名',
        min_length=6, #最小长度验证
        initial='张三', #默认值
        required=True , #是否为必须字段
        validators=[checkname],  #自定义验证 自动将值传入checkname函数
        error_messages={  #错误提示信息
            'required': '用户名是必填项', 
            'min_length': '用户名长度不能小于6位'
        }
    )
    pwd = forms.CharField(label='密码', widget=forms.PasswordInput) #widget html插件
    
    re_pwd = forms.CharField(label='确认密码', widget=forms.PasswordInput)
    
    gender = forms.ChoiceField(
        choices=((1, '男'), (2, '女')), #key value
        widget=forms.RadioSelect  #单选框  不设置为单选下拉列表
    )
    
    # hobby = forms.MultipleChoiceField(  #多选下拉框
    #     # choices=((1, "篮球"), (2, "足球"), (3, "双色球"),),
    #     choices=models.Hobby.objects.values_list('id', 'name'), 
    # )

    hobby = forms.ModelMultipleChoiceField(  #另一种多选下拉框
        # choices=((1, "篮球"), (2, "足球"), (3, "双色球"),),
        queryset=models.Hobby.objects.all(), #通过该方式，在对应的表类中添加__str__方法即可
    )

    phone = forms.CharField(
        validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')] #内置的校验器正则
    )

    def clean_username(self):
        # 局部钩子
        # 通过校验规则  必须返回当前字段的值
        # 不通过校验规则   抛出异常
        v = self.cleaned_data.get('username')
        if 'alex' in v:
            raise ValidationError('不符合社会主义核心价值观')
        else:
            return v

    def clean(self):
        # 全局钩子
        # 通过校验规则  必须返回当前所有字段的值
        # 不通过校验规则   抛出异常   模板通过'__all__.errors'显示错误信息
        pwd = self.cleaned_data.get('pwd')
        re_pwd = self.cleaned_data.get('re_pwd')
        if pwd == re_pwd:
            return self.cleaned_data
        else:
            self.add_error('re_pwd','两次密码不一致!!!!!') #为re_pwd添加错误信息，可以再模板中使用re_pwd来显示该错误信息
            raise ValidationError('两次密码不一致')

    # def __init__(self, *args, **kwargs):
    #     super(RegForm, self).__init__(*args, **kwargs)
    #     self.fields['hobby'].choices = models.Hobby.objects.values_list('id', 'name') #在类的初始化方法中设置选项，可以解决数据库改变而网页未改变的情况

def reg2(request):
    form_obj = RegForm()
    if request.method == 'POST':
        form_obj = RegForm(request.POST)
        if form_obj.is_valid():  # 对数据进行校验
            # 插入数据
            print(form_obj.cleaned_data)  # 通过校验的数据
            return HttpResponse('注册成功')

    return render(request, 'reg2.html', {'form_obj': form_obj})    

```

前端代码

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
</head>
<body>

<form action="" method="post" novalidate>
    {% csrf_token %}
    <p>
        <label for="{{ form_obj.username.id_for_label }}">{{ form_obj.username.label }}</label>
        {{ form_obj.username }}
        {{ form_obj.username.errors }}
    </p>

    <p>
        <label for="{{ form_obj.pwd.id_for_label }}">{{ form_obj.pwd.label }}</label>
        {{ form_obj.pwd }}
        {{ form_obj.pwd.errors.0 }}
    </p>

    <p>
        <label for="{{ form_obj.re_pwd.id_for_label }}">{{ form_obj.re_pwd.label }}</label>
        {{ form_obj.re_pwd }}
        {{ form_obj.re_pwd.errors.0 }}
    </p>
{##}
{#    <p>#}
{#        <label for="{{ form_obj.gender.id_for_label }}">{{ form_obj.gender.label }}</label>#}
{#        {{ form_obj.gender }}#}
{#        {{ form_obj.gender.errors.0 }}#}
{#    </p>#}
{##}
{#    <p>#}
{#        <label for="{{ form_obj.hobby.id_for_label }}">{{ form_obj.hobby.label }}</label>#}
{#        {{ form_obj.hobby }}#}
{#        {{ form_obj.hobby.errors.0 }}#}
{#    </p>#}
{##}
{#    <p>#}
{#        <label for="{{ form_obj.phone.id_for_label }}">{{ form_obj.phone.label }}</label>#}
{#        {{ form_obj.phone }}#}
{#        {{ form_obj.phone.errors.0 }}#}
{#    </p>#}

    {{ form_obj.errors }}
    <button>注册</button>
</form>
</body>
</html>
```

模板使用总结：

```
{{ form_obj.as_p }}    __>   生产一个个P标签  input  label
{{ form_obj.errors }}    ——》   form表单中所有字段的错误
{{ form_obj.username }}     ——》 一个字段对应的input框
{{ form_obj.username.label }}    ——》  该字段的中午提示
{{ form_obj.username.id_for_label }}    ——》 该字段input框的id
{{ form_obj.username.errors }}   ——》 该字段的所有的错误
{{ form_obj.username.errors.0 }}   ——》 该字段的第一个错误的错误
{{ form_obj.non_field_errors.0 }}  --》 __all__中的错误
```



#### 12.14.3 常用字段

```
CharField    
ChoiceField 
MultipleChoiceField
```

#### 12.14.4 字段参数

```
required=True,               是否允许为空
widget=None,                 HTML插件
label=None,                  用于生成Label标签或显示内容
initial=None,                初始值
error_messages=None,         错误信息 {'required': '不能为空', 'invalid': '格式错误'}  
validators=[],               自定义验证规则
disabled=False,              是否可以编辑
```

#### 12.14.5 验证

1. 内置验证

   ```
   required=True 必须字段
   min_length   最小长度
   max_length   最大长度
   ```

2. 自定义验证

   - 自己写函数

     ```python
     def checkname(value):
         # 通过校验规则 不做任何操作
         # 不通过校验规则   抛出异常
         if 'alex' in value:
             raise ValidationError('不符合社会主义核心价值观')
     ```

   - 使用内置校验器

     ```python
     from django.core.validators import RegexValidator
     phone = forms.CharField(
             validators=[RegexValidator(r'^1[3-9]\d{9}$', '手机号格式不正确')] #内置的校验器正则
         )
     ```

#### 12.14.6 钩子函数

1. 局部钩子

   ```
   def clean_字段名(self):
       # 局部钩子
       # 通过校验规则  必须返回当前字段的值
       # 不通过校验规则   抛出异常
       raise ValidationError('不符合社会主义核心价值观')
   ```

2. 全局钩子

   ```
   def clean(self):
       # 全局钩子
       # 通过校验规则  必须返回当前所有字段的值
       # 不通过校验规则   抛出异常   '__all__'
       pass
   ```

#### 12.14.7 is_valid流程

1.执行full_clean()的方法：

定义错误字典

定义存放清洗数据的字典

2.执行_clean_fields方法：

​	循环所有的字段

​	获取当前的值

​    对进行校验 （ 内置校验规则   自定义校验器）

1. 通过校验

   self.cleaned_data[name] = value 

   如果有局部钩子，执行进行校验：

   通过校验——》 self.cleaned_data[name] = value 

   不通过校验——》     self._errors  添加当前字段的错误 并且 self.cleaned_data中当前字段的值删除掉

2. 没有通过校验

   self._errors  添加当前字段的错误

3.执行全局钩子clean方法

#### 12.14.8 ModelForm组件

ModelForm组件对应model表，保存表单数据更方便

```python
from django import forms
from crm import models
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from comm.encrypt import encrypt_pwd

class RegForm(forms.ModelForm):
    password = forms.CharField(
        min_length=8,
        widget= forms.PasswordInput(attrs={'placeholder':'请输入密码','autocomplete':'off'})
    )
    re_password = forms.CharField(
        min_length=8,
        widget= forms.PasswordInput(attrs={'placeholder':'请再次输入密码','autocomplete':'off'})
    )
    mobile = forms.CharField(
        widget= forms.TextInput(attrs={'placeholder':'请输入手机号','autocomplete':'off'}),
        validators=[RegexValidator(r'^1[3-9]\d{9}$','手机号格式不正确')]
    )

    class Meta:

        model = models.UserProfile
        fields = '__all__'
        exclude = ['is_active']
        widgets = {
            'username': forms.EmailInput(attrs={'placeholder':'请输入邮箱地址','autocomplete':'off'}),
            'name': forms.TextInput(attrs={'placeholder':'请输入真实姓名','autocomplete':'off'}),
        }
    def clean(self):
        password = self.cleaned_data.get('password')
        re_password = self.cleaned_data.get('re_password')
        if password == re_password:
            if password:
                self.cleaned_data['password'] = encrypt_pwd(password)
            return self.cleaned_data
        else:
            self.add_error('re_password','两次密码输入不一致')
            raise ValidationError('两次密码输入不一致')
```

视图函数

```python
def reg(request):
    reg_obj = RegForm()
    if request.method == 'POST':
        reg_obj = RegForm(request.POST)
        if reg_obj.is_valid():
            reg_obj.save()
            return redirect('login')

    return render(request,'reg.html',{'reg_obj':reg_obj})
```

##### 12.14.8.1 ModelForm组件的循环生成

循环ModelForm对象得到的是经过处理的ModelForm对象的字段

```html
{% for field in form_obj %}
   <div class="form-group {% if field.errors %}has-error{% endif %}">
       <label style="{% if not field.field.required %}color: #999{% endif %}"
        for="{{ field.id_for_label }}" class="col-md-3 control-label">{{ field.label }}		   </label>
       <div class="col-md-7">{{ field }}</div>
            <span class="help-block">{{ field.errors.0 }}</span>
       </div>
{% endfor %}
```

后台为ModelForm循环设置属性

```python
class CustomerForm(forms.ModelForm):
    class Meta:
        model = models.Customer
        fields = "__all__"
        # widgets = {
            # 'qq': forms.TextInput(attrs={'class': 'form-control'}),
        # }
    
    #在初始化ModelForm对象时，循环为每个字段添加属性
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for filed in self.fields.values():
            if isinstance(filed,MultiSelectFormField):
                continue
            filed.widget.attrs['class'] = 'form-control'
```

##### 12.14.8.2 ModelForm组件编辑数据

```python
def oper_customer(request,pk=None):
    #查询要编辑的数据
    customer = models.Customer.objects.filter(pk=pk).first()
    #用要编辑的数据实例化ModelForm对象
    form_obj = CustomerForm(instance=customer)
    if request.method == 'POST':
        return_url = request.COOKIES.get('customer_url')
        #修改完成后，用修改后的数据和原始数据对象实例化ModelForm对象
        form_obj = CustomerForm(data=request.POST,instance=customer)
        if form_obj.is_valid():
            #保存ModelForm对象，完成数据修改
            form_obj.save()
            if return_url:
                return redirect(return_url)
            return redirect('my_customer')
    title = '编辑客户' if pk else '新增客户'
    return render(request, 'oper_customer.html', {'form_obj': form_obj, 'title':title})
```

### 12.15 django的中间件

#### 12.15.1 django中间件的定义

中间件是处理django的请求和响应的框架级别的钩子，本质就是一个类。

#### 12.15.2 自定义一个中间件

自己创建一个类继承MiddlewareMixin内部实现以下五个方法（不必全部实现）

- process_request(self,request)
- process_view(self, request, view_func, view_args, view_kwargs)
- process_template_response(self,request,response)
- process_exception(self, request, exception)
- process_response(self, request, response)

```python
from django.utils.deprecation import MiddlewareMixin
class MyMiddleware(MiddlewareMixin):
    process_request(self,request):
        pass
    ...
```

然后在settings文件中注册该中间件。（假设该类在项目下的app01下的mymds文件夹下的mymd.py文件中）

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
 'app01.mymds.mymd.MyMiddleware',
]
```

#### 12.15.3 中间件方法介绍

- process_request(self,request)

  执行时间：视图函数之前

  参数：request   —— 》 和视图函数中是同一个request对象

  执行顺序：按照注册的顺序  顺序执行

  返回值：

  ​		None ： 正常流程

  ​		HttpResponse： 后面的中间件的process_request、视图函数都不执行，直接执行当前中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

- process_response(self,request,response)

  执行时间：视图函数之后

  参数：

  ​	request   —— 》 和视图函数中是同一个request对象

  ​	response   ——》  返回给浏览器响应对象

  执行顺序：按照注册的顺序  倒叙执行

  返回值：

  ​		HttpResponse：必须返回response对象

- process_view(self,request,view_func,view_args,view_kwargs)

  执行时间：视图函数之前，process_request之后

  参数：

  ​		request   —— 》 和视图函数中是同一个request对象

  ​		view_func  ——》 视图函数

  ​		view_args   ——》 视图函数的位置参数

  ​		view_kwargs  ——》 视图函数的关键字参数

  执行顺序：按照注册的顺序  顺序执行

  返回值：

  ​		None ： 正常流程

  ​		HttpResponse： 后面的中间的process_view、视图函数都不执行，直接执行最后一个中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

- process_exception(self,request,exception)

  执行时间（触发条件）：视图层面有错时才执行

  参数：

  ​		request   —— 》 和视图函数中是同一个request对象

  ​		exception   ——》 错误对象

  执行顺序：按照注册的顺序  倒叙执行

  返回值：

  ​		None ： 交给下一个中间件取处理异常，都没有处理交由django处理异常

  ​		HttpResponse： 后面的中间的process_exception不执行，直接执行最后一个中间件中的process_response方法，倒叙执行之前的中间中process_response方法。

- process_template_response(self,request,response)

  执行时间（触发条件）：视图返回的是一个templateResponse对象

  参数：

  ​	request   —— 》 和视图函数中是同一个request对象

  ​	response   ——》  templateResponse对象

  执行顺序：按照注册的顺序  倒叙执行

  返回值：

  ​		HttpResponse：必须返回response对象

  注：render和TemplateResponse对象的区别：

  - render:返回的是一个渲染好的模板，不能更改

  - TemplateResponse:返回的是未渲染的模板，我们可以在process_template_response()方法中更改模板和渲染的数据

    ```python
    #view函数
    from django.shortcuts import render, HttpResponse
    from django.template.response import TemplateResponse
    
    
    def index(request):
        print('index')
        return TemplateResponse(request,'index.html',{'name':'alxe'})
    
    
    #process_template_response()方法
    def process_template_response(self, request, response):
            response.template_name = 'index1.html'
            response.context_data['name'] = 'peiqi'
    
            return response
    ```

    

#### 12.15.4 django请求的生命周期

![](E:\Python\11Django\day65-1\代码\asset\1561783661582.png)







## 十三、django项目

### 13.1 展示客户信息

#### 13.1.1 普通字段

对象.字段名

#### 13.1.2 有choice参数

- 对象.字段名    --》得到数据库中的数据

- `对象.get_字段名_display()`     --》得到数据库对应的中文提示

  ```python
  #Custome的class_type字段
  class_type_choices = (('fulltime', '脱产班',),
                        ('online', '网络班'),
                        ('weekend', '周末班',),)
  ```

  ```html
  <td>{{ customer.get_class_type_display }}</td>
  ```

#### 13.1.3 外键

- 对象.外键  ——》  外键对象   定义`__str__`
- 对象.外键.字段

#### 13.1.4 自定义方法

多对多：

```python
def show_class(self):
    return ' '.join([str(i) for i in self.class_list.all()])
```

自定义需求：

```python
    def get_status(self):
        dic = {
            'signed': "blue",
            'unregistered': "gray",
            'studying': 'green',
            'paid_in_full': "red"
        }
        return '<span style="color:white; background-color:{};padding: 5px; border-radius: 8%">{}</span>'.format(
            dic.get(self.status), self.get_status_display())
```

### 13.2 分页

分页功能封装成类

```python
import math

class Pagination:
    def __init__(self,page,total_count,show_page=11,pre_count=10):
        '''

        :param page: 当前页码
        :param total_count:数据总量
        :param show_page: 展示的页码个数
        :param pre_count: 每页展示的数据个数
        '''
        try:
            self.page = int(page)
            if self.page <= 0:
                self.page = 1
        except Exception:
            self.page = 1

        total_page = math.ceil(total_count / pre_count)
        half_page = show_page // 2
        if total_page <= show_page:
            start_page = 1
            end_page = total_page
        else:
            if self.page <= half_page:
                start_page = 1
                end_page = show_page
            elif self.page + half_page > total_page:
                start_page = total_page - show_page + 1
                end_page = total_page
            else:
                start_page = self.page - half_page
                end_page = self.page + half_page
        self.total_page = total_page
        self.start_page = start_page
        self.end_page = end_page
        self.start = (self.page - 1) * 10
        self.end = self.page * 10

    @property
    def page_html(self):
        page_list = []
        if self.page == 1:
            page_list.append(
                '<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>')
        else:
            page_list.append(
                '<li><a href="?page={}" aria-label="Previous"><span aria-hidden="true">上一页</span></a></li>'.format(
                    self.page - 1))
        for i in range(self.start_page, self.end_page + 1):
            if i == self.page:
                page_list.append('<li class="active"><a href="?page={}">{}</a></li>'.format(i, i))
            else:
                page_list.append('<li><a href="?page={}">{}</a></li>'.format(i, i))
        if self.page == self.total_page:
            page_list.append(
                '<li class="disabled"><a aria-label="Next"><span aria-hidden="true">下一页</span></a></li>')
        else:
            page_list.append(
                '<li><a href="?page={}" aria-label="Next"><span aria-hidden="true">下一页</span></a></li>'.format(
                    self.page + 1))
        return page_list
```

### 13.3 git操作

```python
#查看历史版本
git log
git reflog

#版本回退
git reset --hard 版本号

#克隆远程仓库
git clone https://gitee.com/old_boy_python_stack_21/teaching_plan.git  克隆远程仓库

#拉取远程仓库代码到本地仓库
git pull origin master
```

### 13.4 django后台管理（admin）

1. 创建超级用户

   ```
   python manage.py createsuperuser
   ```

2. 在app下的admin.py文件中注册要使用的model

   ```python
   from django.contrib import admin
   from crm import models
   
   admin.site.register(models.Customer)
   admin.site.register(models.Department)
   admin.site.register(models.Campus)
   admin.site.register(models.ClassList)
   ```

### 13.5 分页保留搜索条件

可以在模板中使用request对象

```html
{{ request}}
```

```python
from django.http.request import QueryDict

request.GET   #GET参数为<class 'django.http.request.QueryDict'>类型，类似字典'query': ['13'] 
request.GET.urlencode()   #对参数字典进行编码会对特殊字符编码（如&）  query=13&page=1
request.GET._mutable = True  # 默认QueryDict不可编辑  设置 _mutable属性为True可编辑
request.GET.copy()   # 深拷贝 可编辑
QueryDict(mutable=True)   # 可编辑
```

### 13.6 自己创建数据库model对象，限制choices

视图函数：

```python
def oper_course_record(request, pk=None, class_pk=None):
    course_record = models.CourseRecord(re_class_id=class_pk,
                                        recorder=request.user_obj) if class_pk else models.CourseRecord.objects.filter(
        pk=pk)
    form_obj = CourseRecordForm(instance=course_record)
    if request.method == 'POST':
        form_obj = CourseRecordForm(data=request.POST, instance=course_record)
        return_url = request.session.get('return_url')
        if form_obj.is_valid():
            form_obj.save()
            return redirect(return_url)
    title = '编辑信息' if pk else '添加信息'
    return render(request, 'teacher/oper_course_record.html', {'form_obj': form_obj, 'title': title})
```

modelform组件：

```python
class CourseRecordForm(BSModelForm):

    class Meta:
        model = models.CourseRecord
        fields = '__all__'

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['re_class'].choices =[(self.instance.re_class.pk,self.instance.re_class)]  #限制选项范围
        self.fields['teacher'].choices = [(i.pk,i) for i in self.instance.re_class.teachers.all()]
        self.fields['recorder'].choices = [(self.instance.recorder.pk,self.instance.recorder)]
```

### 13.7 事务和行级锁

#### 13.7.1 mySQL的事务和行级锁

```sql
begin； 开启事务

select * from t6 where f1 = 23.46 for update;   加行级锁

commit； 结束事务
```

#### 13.7.2 django中的事务和行级锁

```python
from django.db import transaction

try:
    with transaction.atomic():

       
        queryset = models.Customer.objects.filter(pk__in=pk, consultant=None).select_for_update() #行级锁

        queryset.update(consultant=self.request.user_obj)
   
except Exception as e:
	print(e)
```

### 13.8 py文件使用setting.py中自定义的配置

settings.py文件

```python
MAX_CUSTOMER_NUM = 3  # 配置写大写
```

使用：

```python
#方式一
from Aida_crm.settings import MAX_CUSTOMER_NUM
count = MAX_CUSTOMER_NUM

#方式二（推荐）
from django.conf import global_settings, settings
settings.MAX_CUSTOMER_NUM  #配置项必须大写
```

### 13.9 批量插入数据

```python
study_record_list = [] 
for student in students:      study_record_list.append(models.StudyRecord(course_record=course_record,student=student))  
models.StudyRecord.objects.bulk_create(study_record_list)
```

### 13.10 modelformset

用于生成多个form表单

视图函数：

```python
from django.forms import modelformset_factory

def study_record_list(request,course_pk):
    ModelFormSet = modelformset_factory(models.StudyRecord,StudyRecordForm,extra=0)#创建一个modelformset类  extra参数=0 :不存在数据时不会默认显示一条空数据（默认值为1）
    formset_obj = ModelFormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_pk))#传入要显示的数据
    if request.method == 'POST':
        formset_obj = ModelFormSet(queryset=models.StudyRecord.objects.filter(course_record_id=course_pk),data=request.POST)
        return_url = request.session.get('return_url')
        if formset_obj.is_valid():
            formset_obj.save()
            if return_url:
                return redirect(return_url)

    return render(request,'teacher/study_record_list.html',{'form_set_obj':formset_obj})
```

模板：

```html
<form action="" method="post">
                {% csrf_token %} 
                {{ form_set_obj.management_form }} <!--必写，管理-->
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>学生</th>
                        <th>考勤</th>
                        <th>成绩</th>
                        <th>作业批语</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% for form in form_set_obj %}
                        <tr>
                            {{ form.id }}  <!--必写，标识不同的form-->
                            <td>{{ forloop.counter }}</td>
                            <td>{{ form.instance.student }}</td><!--instance StudyRecord对象-->
                            <td>{{ form.attendance }}</td> 
                            <td>{{ form.score }}</td>
                            <td>{{ form.homework_note }}</td>
                            <td class="hidden">{{ form.student }}</td>
                            <td class="hidden">{{ form.course_record }}</td>
                        </tr>
                    {% endfor %}

{#                    {{ form_set_obj.errors }}#}


                    </tbody>
                </table>
                <button class="btn btn-primary">保存</button>
            </form>
```



## 附录 常见错误和单词

### 单词

| 单词        | 解释               | 备注           |
| ----------- | ------------------ | -------------- |
| preview     | 预览               |                |
| iterable    | 迭代               |                |
| refactor    | 重构               |                |
| indentation | 缩进               |                |
| generator   | 生成器             |                |
| recursion   | 递归               |                |
| priority    | 优先级             |                |
| aggregate   | 聚合               |                |
| annotate    | 注释               | django分组聚合 |
| transaction | 事务               |                |
| atomic      | 原子的，不可拆分的 |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |
|             |                    |                |



### 常见错误

